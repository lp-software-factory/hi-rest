<?php

use Phinx\Migration\AbstractMigration;

class InitInviteCardFamilyMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_family')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('locale_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('author_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('event_type_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('has_photo', 'boolean', [
                'null' => false,
            ])
            ->addColumn('style_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('card_size_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('locale_id', 'locale', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('author_id', 'profile', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('event_type_id', 'event_type', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('card_size_id', 'invite_card_size', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('style_id', 'invite_card_style', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}