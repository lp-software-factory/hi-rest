<?php
use Phinx\Migration\AbstractMigration;

class InitFontsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('font')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('files', 'json', [
                'null' => false,
            ])
            ->addColumn('definitions', 'json', [
                'null' => false,
            ])
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->create();
    }
}
