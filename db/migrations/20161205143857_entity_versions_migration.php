<?php

use Phinx\Migration\AbstractMigration;

class EntityVersionsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('account')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
                ])
            ->save();

        $this->table('reset_password_request')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('attachment')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('sign_up_request')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('company')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('company_profile')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('designer')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('designer_request')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('event_type')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('event_type_group')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('font')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('invite_card_size')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();


        $this->table('invite_card_color')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();


        $this->table('invite_card_family')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();


        $this->table('invite_card_gamma')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();


        $this->table('invite_card_style')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();


        $this->table('invite_card_template')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();


        $this->table('locale')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('mail')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('currency')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('price')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('oauth2_account')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('oauth2_provider')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('product')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('products_package')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('products_package_eq')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('profile')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

        $this->table('profile_companies')
            ->addColumn('version','string', [
                'null' => false,
                'limit' => 32,
                'default' => '1.0.0',
            ])
            ->save();

    }
}
