<?php

use Phinx\Migration\AbstractMigration;

class VersionMigration extends AbstractMigration
{
    public function change()
    {
        $tables = [
            'account',
            'reset_password_request',
            'attachment',
            'sign_up_request',
            'company',
            'company_profile',
            'designer',
            'designer_request',
            'envelope_backdrop',
            'envelope_color',
            'envelope_marks',
            'envelope_pattern',
            'envelope_texture',
            'event_type',
            'event_type_group',
            'invite_card_size',
            'invite_card_color',
            'invite_card_family',
            'invite_card_gamma',
            'invite_card_style',
            'invite_card_family',
            'invite_card_template',
            'invite_card_template_photo',
            'locale',
            'mail',
            'currency',
            'price',
            'oauth2_account',
            'oauth2_provider',
            'product',
            'products_package',
            'products_package_eq',
            'profile',
            'profile_companies',
            'font',
        ];

        foreach($tables as $table) {
            if(! $this->table($table)->hasColumn('version')) {
                $this->table($table)
                    ->addColumn('version', 'string', [
                        'limit' => 32,
                        'null' => false,
                        'default' => '1.0.0',
                    ])
                    ->save();
            }
        }
    }
}
