<?php

use Phinx\Migration\AbstractMigration;

class StyleIconMigratio extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_style')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('image', 'json', [
                'null' => false,
            ])
            ->save();
    }
}
