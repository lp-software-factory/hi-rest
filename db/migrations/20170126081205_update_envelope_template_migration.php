<?php

use Phinx\Migration\AbstractMigration;

class UpdateEnvelopeTemplateMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_template')
            ->dropForeignKey('envelope_backdrop_id')
            ->dropForeignKey('envelope_color_id')
            ->dropForeignKey('envelope_marks_id')
            ->dropForeignKey('envelope_pattern_id')
            ->dropForeignKey('envelope_texture_id')
            ->removeColumn('envelope_backdrop_id')
            ->removeColumn('envelope_color_id')
            ->removeColumn('envelope_marks_id')
            ->removeColumn('envelope_pattern_id')
            ->removeColumn('envelope_texture_id')
            ->addColumn('envelope_backdrop_definition_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('envelope_texture_definition_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('envelope_pattern_definition_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('envelope_mark_definition_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('envelope_backdrop_definition_id', 'envelope_backdrop_definition', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('envelope_texture_definition_id', 'envelope_texture_definition', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('envelope_pattern_definition_id', 'envelope_pattern_definition', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('envelope_mark_definition_id', 'envelope_mark_definition', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save()
        ;
    }
}
