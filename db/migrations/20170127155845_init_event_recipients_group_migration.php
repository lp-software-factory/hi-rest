<?php

use Phinx\Migration\AbstractMigration;

class InitEventRecipientsGroupMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_recipients_group')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('title', 'string', [
                'null' => false
            ])
            ->addColumn('owner_profile_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('owner_profile_id', 'profile', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->create();
    }
}
