<?php

use Phinx\Migration\AbstractMigration;

class RmSvgMapMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->dropForeignKey('svg_map')
            ->removeColumn('svg_map')
            ->save();
    }
}
