<?php

use Phinx\Migration\AbstractMigration;

class EnvelopeTexturePreviewMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_texture')
            ->addColumn('preview_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('preview_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->save();
    }
}
