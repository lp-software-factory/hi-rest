<?php

use Phinx\Migration\AbstractMigration;

class EventMailingAnalyticsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_analytics')
            ->removeColumn('event_id')
            ->addColumn('event_mailing_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('event_mailing_id', 'event_mailing', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save();
    }
}
