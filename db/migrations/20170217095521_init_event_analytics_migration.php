<?php

use Phinx\Migration\AbstractMigration;

class InitEventAnalyticsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_analytics')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('event_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('records', 'json', [
                'null' => false,
                'default' => '[]',
            ])
            ->addForeignKey('event_id', 'event', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->create();

        $this->table('event')
            ->removeColumn('event_mailing_options_id')
            ->removeColumn('event_recipients_id')
            ->save();


        $this->table('event_mailing_options')
            ->addColumn('event_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('event_id', 'event', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save();

        $this->table('event_recipients')
            ->addColumn('event_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('event_id', 'event', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save();
    }
}
