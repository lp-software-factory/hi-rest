<?php

use Phinx\Migration\AbstractMigration;

class InitPricingMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('currency')
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('title', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('code', 'string', [
                'limit' => 8
            ])
            ->addColumn('sign', 'string', [
                'limit' => 127
            ])
            ->addIndex('code', ['unique' => true])
            ->create();

        $this->table('price')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('currency_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('amount', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('currency_id', 'currency', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();

        $this->table('product')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('title', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('base_price_id', 'integer', [
                'null' => false
            ])
            ->addIndex('sid', ['unique' => true])
            ->addForeignKey('base_price_id', 'price', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade'
            ])
            ->create();

        $this->table('products_package')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('title', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'default' => '{}',
                'null' => false
            ])
            ->addColumn('base_price_id', 'integer', [
                'null' => false,
            ])
            ->addIndex('sid', ['unique' => true])
            ->addForeignKey('base_price_id', 'price', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade'
            ])
            ->create();

        $this->table('products_package_eq', [
            'id' => false,
            'primary_key' => [
                'products_package_id',
                'product_id'
            ]
        ])
            ->addColumn('products_package_id', 'integer')
            ->addColumn('product_id', 'integer')
            ->addForeignKey('products_package_id', 'products_package', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('product_id', 'product', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
