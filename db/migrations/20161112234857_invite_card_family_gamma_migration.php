<?php

use Phinx\Migration\AbstractMigration;

class InviteCardFamilyGammaMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_family')
            ->addColumn('gamma_id', 'integer', [
                'null' => false,
            ])

            ->addForeignKey('gamma_id', 'invite_card_gamma', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save();
    }
}
