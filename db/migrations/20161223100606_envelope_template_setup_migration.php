<?php

use Phinx\Migration\AbstractMigration;

class EnvelopeTemplateSetupMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_template')
            ->addColumn('title', 'json', [
                'null' => false,
            ])
            ->addColumn('description', 'json', [
                'null' => false,
            ])
            ->addColumn('envelope_backdrop_id', 'integer', [
                'null' => false
            ])
            ->addColumn('envelope_backdrop_attachment_id', 'integer', [
                'null' => false
            ])
            ->addColumn('envelope_marks_id', 'integer', [
                'null' => false
            ])
            ->addColumn('envelope_marks_attachment_id', 'integer', [
                'null' => false
            ])
            ->addColumn('envelope_pattern_id', 'integer', [
                'null' => false
            ])
            ->addColumn('envelope_texture_id', 'integer', [
                'null' => false
            ])
            ->addColumn('envelope_color_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('envelope_backdrop_id', 'envelope_backdrop', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_backdrop_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_marks_id', 'envelope_marks', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_marks_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_pattern_id', 'envelope_pattern', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_texture_id', 'envelope_texture', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_color_id', 'envelope_color', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->save()
        ;
    }
}
