<?php

use Phinx\Migration\AbstractMigration;

class CardSizeIconMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_size')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('image', 'json', [
                'null' => false,
            ])
            ->save();
    }
}
