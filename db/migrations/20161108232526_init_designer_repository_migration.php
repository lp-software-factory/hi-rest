<?php

use Phinx\Migration\AbstractMigration;

class InitDesignerRepositoryMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('designer')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('profile_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('designer_request_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('profile_id', 'profile', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('designer_request_id', 'designer_request', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
