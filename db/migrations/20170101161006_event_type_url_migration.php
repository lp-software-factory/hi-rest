<?php

use Phinx\Migration\AbstractMigration;

class EventTypeUrlMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_type')
            ->addColumn('url', 'string', [
                'null' => false,
                'limit' => 32,
            ])
            ->save();

        $this->table('event_type_group')
            ->addColumn('url', 'string', [
                'null' => false,
                'limit' => 32,
            ])
            ->save();
    }
}
