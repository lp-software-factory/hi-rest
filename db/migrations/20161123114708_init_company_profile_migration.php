<?php

use Phinx\Migration\AbstractMigration;

class InitCompanyProfileMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('company_profile')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('company_id', 'integer', [
                'null' => false,
                'limit' => 256,
            ])
            ->addColumn('ogrn', 'string', [
                'null' => false,
                'limit' => 15,
                'default' => '',
            ])
            ->addColumn('who_gives', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('inn', 'string', [
                'null' => false,
                'limit' => 12,
                'default' => '',
            ])
            ->addColumn('kpp', 'string', [
                'null' => false,
                'limit' => 3,
                'default' => '',
            ])
            ->addColumn('bank_bik', 'string', [
                'null' => false,
                'limit' => 3,
                'default' => '',
            ])
            ->addColumn('bank_correspondent_account', 'string', [
                'null' => false,
                'limit' => 20,
                'default' => '',
            ])
            ->addColumn('giro', 'string', [
                'null' => false,
                'limit' => 20,
                'default' => '',
            ])
            ->addColumn('okpo', 'string', [
                'null' => false,
                'limit' => 10,
                'default' => '',
            ])
            ->addColumn('okato', 'string', [
                'null' => false,
                'limit' => 11,
                'default' => '',
            ])
            ->addColumn('okved', 'string', [
                'null' => false,
                'limit' => 20,
                'default' => '',
            ])
            ->addColumn('legal_address', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('real_address', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('director_general', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('accounting_general', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('phone', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('fax', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addColumn('email', 'string', [
                'null' => false,
                'limit' => 512,
                'default' => '',
            ])
            ->addForeignKey('company_id', 'company', 'id', [
                'update' =>'cascade',
                'delete' => 'cascade',
            ])
            ->addIndex(['sid','inn', 'ogrn'], ['unique' => true] )
            ->create();
    }
}
