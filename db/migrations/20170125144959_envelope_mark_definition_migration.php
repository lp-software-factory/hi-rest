<?php

use Phinx\Migration\AbstractMigration;

class EnvelopeMarkDefinitionMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_mark_definition')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false
            ])
            ->addColumn('owner_envelope_marks_id', 'integer', [
                'null' => false
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('attachment_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('owner_envelope_marks_id', 'envelope_marks', 'id', [
                'delete' => 'restrict',
                'update' => 'cascade'
            ])
            ->addForeignKey('attachment_id', 'attachment', 'id', [
                'delete' => 'restrict',
                'update' => 'cascade'
            ])
            ->create();

        $this->table('envelope_marks')
            ->removeColumn('attachment_ids')
            ->save();
    }
}
