<?php

use Phinx\Migration\AbstractMigration;

class InitOauth2Migration extends AbstractMigration
{
    public function change()
    {
        $this->table('oauth2_provider')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'null' => false,
            ])
            ->addColumn('handler', 'string', [
                'null' => false,
            ])
            ->addColumn('config', 'json', [
                'null' => false,
            ])
            ->addIndex('sid', ['unique' => true])
            ->addIndex('code', ['unique' => true])
            ->addIndex('handler', ['unique' => true])
            ->create();

        $this->table('oauth2_account')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('account_id', 'integer', [
                'null' => false
            ])
            ->addColumn('provider_id', 'integer', [
                'null' => false
            ])
            ->addColumn('refresh_token', 'string', [
                'null' => false
            ])
            ->addColumn('resource_owner_id', 'string', [
                'null' => false
            ])
            ->addIndex('sid', ['unique' => true])
            ->addForeignKey('account_id', 'account', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('provider_id', 'oauth2_provider', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->create();

    }
}
