<?php

use Phinx\Migration\AbstractMigration;

class HiDomainLanding extends AbstractMigration
{
    public function change()
    {
        $this->table('landing')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '[]',
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('is_selected', 'string', [
                'limit' => 256,
            ])
            ->addColumn('title', 'string', [
                'limit' => 32,
                'default' => 'landing'// а сколько должно быть?
            ])
            ->addColumn('description', 'string', [
                'limit' => 256,
                'default' => 'descrip'
            ])
            ->addColumn('definition', 'string', [
                'limit' => 256,
                'default' => '[]'
            ])
            ->addColumn('type', 'string', [
                'limit' => 256,
                'default' => 'celebrate'
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
