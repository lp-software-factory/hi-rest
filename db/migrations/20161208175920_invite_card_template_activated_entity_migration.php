<?php

use Phinx\Migration\AbstractMigration;

class InviteCardTemplateActivatedEntityMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_family')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->save();
    }
}
