<?php
use Phinx\Migration\AbstractMigration;

class CompanyInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('company')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('name', 'string', [
                'limit' => 256,
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
