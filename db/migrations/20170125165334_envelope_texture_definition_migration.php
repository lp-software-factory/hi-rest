<?php

use Phinx\Migration\AbstractMigration;

class EnvelopeTextureDefinitionMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_texture')
            ->removeColumn('definitions')
            ->save();

        $this->table('envelope_texture_definition')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false
            ])
            ->addColumn('owner_envelope_texture_id', 'integer', [
                'null' => false
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('preview_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('resources', 'json', [
                'null' => false,
                'default' => '{}',
            ])
            ->addForeignKey('owner_envelope_texture_id', 'envelope_texture', 'id', [
                'delete' => 'restrict',
                'update' => 'cascade'
            ])
            ->addForeignKey('preview_attachment_id', 'attachment', 'id', [
                'delete' => 'restrict',
                'update' => 'cascade'
            ])
            ->create();
    }
}
