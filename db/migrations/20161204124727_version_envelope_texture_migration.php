<?php

use Phinx\Migration\AbstractMigration;

class VersionEnvelopeTextureMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_texture')
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->save();
    }
}
