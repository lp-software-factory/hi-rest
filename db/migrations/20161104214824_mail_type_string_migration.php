<?php

use Phinx\Migration\AbstractMigration;

class MailTypeStringMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('mail')
            ->removeColumn('mail_type')
            ->addColumn('mail_type', 'string', [
                'limit' => 32,
                'null' => false,
            ])
            ->save();
    }
}
