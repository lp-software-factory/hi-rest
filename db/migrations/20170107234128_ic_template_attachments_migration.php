<?php

use Phinx\Migration\AbstractMigration;

class IcTemplateAttachmentsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->addColumn('attachment_ids', 'json', [
                'null' => false,
                'default' => '{}'
            ])
            ->save();
    }
}
