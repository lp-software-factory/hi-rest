<?php

use Phinx\Migration\AbstractMigration;

class InitStyleMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_style')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('position', 'integer', [
                'default' => 1,
                'null' => false
            ])
            ->addColumn('title', 'json')
            ->addColumn('description', 'json')
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
