<?php

use Phinx\Migration\AbstractMigration;

class AccountInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('account')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('app_access', 'json', [
                'default' => '{}',
            ])
            ->addColumn('email', 'string', [
                'limit' => 256,
            ])
            ->addColumn('phone', 'string', [
                'limit' => 32,
                'null' => true,
            ])
            ->addColumn('password', 'string', [
                'limit' => 127,
            ])
            ->addIndex('sid', ['unique' => true])
            ->addIndex('email', ['unique' => true])
            ->create();
    }
}
