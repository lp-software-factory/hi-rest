<?php

use Phinx\Migration\AbstractMigration;

class EventLandingJsonMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_landing_template')
            ->addColumn('json', 'json', [
                'null' => false,
            ])
            ->save();
    }
}
