<?php

use Phinx\Migration\AbstractMigration;

class InitColorMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_color')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('position', 'integer', [
                'default' => 1,
                'null' => false
            ])
            ->addColumn('title', 'json')
            ->addColumn('hex_code', 'string', [
                'limit' => 7
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
