<?php

use Phinx\Migration\AbstractMigration;

class LocaleInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('locale')
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('language', 'string', [
                'limit' => 2
            ])
            ->addColumn('region', 'string', [
                'limit' => 5
            ])
            ->addIndex(['language', 'region'], ['unique' => true])
            ->create();
    }
}
