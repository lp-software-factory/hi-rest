<?php

use Phinx\Migration\AbstractMigration;

class InitEventMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('title', 'string', [
                'null' => false,
                'limit' => 127,
                'default' => '""',
            ])
            ->addColumn('owner_id', 'integer', [
                'null' => false
            ])
            ->addColumn('original_solution_id', 'integer', [
                'null' => false
            ])
            ->addColumn('current_solution_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('original_solution_id', 'solution', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('current_solution_id', 'solution', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('owner_id', 'profile', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
