<?php

use Phinx\Migration\AbstractMigration;

class ProfileCompaniesInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('profile_companies')
            ->addColumn('profile_id', 'integer')
            ->addColumn('company_id', 'integer')
            ->addForeignKey('profile_id', 'profile', 'id', [
                'update' =>'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('company_id', 'company', 'id', [
                'update' =>'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
