<?php

use Phinx\Migration\AbstractMigration;

class ConfigEnvelopeMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_template')
            ->removeColumn('envelope_backdrop_attachment_id')
            ->removeColumn('envelope_marks_attachment_id')
            ->addColumn('config', 'json', [
                'null' => false,
                'default' => '{}'
            ])
            ->save();
    }
}
