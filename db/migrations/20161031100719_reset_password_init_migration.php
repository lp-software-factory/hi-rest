<?php

use Phinx\Migration\AbstractMigration;

class ResetPasswordInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('reset_password_request')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('accept_token', 'string', [
                'limit' => 32,
            ])
            ->addColumn('account_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('account_id', 'account', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
