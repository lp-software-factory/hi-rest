<?php

use Phinx\Migration\AbstractMigration;

class PositionPackageProductsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('products_package_eq')->drop();

        $this->table('products_package_eq')
            ->addColumn('products_package_id', 'integer')
            ->addColumn('product_id', 'integer')
            ->addColumn('position', 'integer')
            ->addForeignKey('products_package_id', 'products_package', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('product_id', 'product', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
