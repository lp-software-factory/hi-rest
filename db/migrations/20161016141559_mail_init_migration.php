<?php

use Phinx\Migration\AbstractMigration;

class MailInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('mail')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('mail_type', 'integer')
            ->addColumn('subject', 'string',  [
                'limit' => 256
            ])
            ->addColumn('from_address', 'string', [
                'limit' => 256
            ])
            ->addColumn('to_address', 'string', [
                'limit' => 256
            ])
            ->addColumn('content_type', 'string', [
                'limit' => 64
            ])
            ->addColumn('body', 'text')
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
