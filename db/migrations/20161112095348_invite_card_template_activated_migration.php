<?php

use Phinx\Migration\AbstractMigration;

class InviteCardTemplateActivatedMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->save();
    }
}
