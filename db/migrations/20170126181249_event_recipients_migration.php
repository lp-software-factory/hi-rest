<?php

use Phinx\Migration\AbstractMigration;

class EventRecipientsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_recipients')
            ->removeColumn('solution_component_user_specified')
            ->removeColumn('solution_component_owner')
            ->addColumn('contacts', 'json', [
                'null' => false,
                'default' => '[]',
            ])
            ->save();

        $this->table('event_mailing_options')
            ->removeColumn('solution_component_user_specified')
            ->removeColumn('solution_component_owner')
            ->addColumn('config', 'json', [
                'null' => false,
                'default' => '[]',
            ])
            ->save();

        $this->table('event')
            ->addColumn('event_recipients_id', 'integer', [
                'null' => false
            ])
            ->addColumn('event_mailing_options_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('event_recipients_id', 'event_recipients', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->addForeignKey('event_mailing_options_id', 'event_recipients', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->save();
    }
}
