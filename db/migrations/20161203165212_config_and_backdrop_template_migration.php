<?php

use Phinx\Migration\AbstractMigration;

class ConfigAndBackdropTemplateMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->addColumn('config', 'json', [
                'null' => false,
                'default' => '{}'
            ])
            ->addColumn('backdrop', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('backdrop', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save()
        ;
    }
}
