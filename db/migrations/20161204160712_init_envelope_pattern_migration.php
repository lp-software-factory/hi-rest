<?php

use Phinx\Migration\AbstractMigration;

class InitEnvelopePatternMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_pattern')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('preview_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('texture_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('definitions', 'json', [
                'null' => false,
                'default' => '{}',
            ])
            ->addForeignKey('preview_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->addForeignKey('texture_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->create();
    }
}
