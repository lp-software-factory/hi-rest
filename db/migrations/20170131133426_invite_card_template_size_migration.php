<?php

use Phinx\Migration\AbstractMigration;

class InviteCardTemplateSizeMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->addColumn('card_size_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('card_size_id', 'invite_card_size', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save();
    }
}
