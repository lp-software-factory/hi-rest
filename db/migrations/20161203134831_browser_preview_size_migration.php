<?php

use Phinx\Migration\AbstractMigration;

class BrowserPreviewSizeMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_size')
            ->addColumn('browser_preview_width', 'integer', [
                'null' => false,
            ])
            ->addColumn('browser_preview_height', 'integer', [
                'null' => false,
            ])
            ->save();
    }
}
