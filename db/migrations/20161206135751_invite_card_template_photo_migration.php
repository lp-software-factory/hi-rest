<?php

use Phinx\Migration\AbstractMigration;

class InviteCardTemplatePhotoMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template_photo')
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('placeholder', 'integer', [
                'null' => false
            ])
            ->addColumn('container_start', 'json', [
                'null' => false,
                'default' => json_encode(['x' => 0, 'y' => 0])
            ])
            ->addColumn('container_end', 'json', [
                'null' => false,
                'default' => json_encode(['x' => 0, 'y' => 0])
            ])
            ->addForeignKey('placeholder', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->create();
        ;

        $this->table('invite_card_template')
            ->addColumn('svg_map', 'integer', [
                'null' => false,
            ])
            ->addColumn('invite_card_template_photo_id', 'integer', [
                'null' => true,
            ])
            ->addForeignKey('svg_map', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->addForeignKey('invite_card_template_photo_id', 'invite_card_template_photo', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict'
            ])
            ->save();
    }
}
