<?php

use Phinx\Migration\AbstractMigration;

class InitEventTypeMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_type_group')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('position', 'integer', [
                'default' => 1,
                'null' => false
            ])
            ->addColumn('title', 'json')
            ->addColumn('description', 'json')
            ->addIndex('sid', ['unique' => true])
            ->create();

        $this->table('event_type')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('position', 'integer', [
                'default' => 1,
                'null' => false
            ])
            ->addColumn('event_type_group_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('title', 'json')
            ->addColumn('description', 'json')
            ->addForeignKey('event_type_group_id', 'event_type_group', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade'
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
