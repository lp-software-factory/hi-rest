<?php

use Phinx\Migration\AbstractMigration;

class EventFeaturesMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event')
            ->addColumn('event_features', 'json', [
                'null' => false,
                'default' => json_encode([
                    'envelope' => true,
                    'landing' => true,
                ])
            ])
            ->save()
        ;
    }
}
