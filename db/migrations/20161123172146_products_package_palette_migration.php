<?php

use Phinx\Migration\AbstractMigration;

class ProductsPackagePaletteMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('products_package')
            ->addColumn('palette', 'json', [
                'null' => false,
            ])
            ->save();
    }
}
