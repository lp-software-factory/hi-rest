<?php

use Phinx\Migration\AbstractMigration;

class MailingInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_mailing')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('event_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('is_perform_requested', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('date_perform_requested', 'datetime', [
                'null' => true,
            ])
            ->addColumn('is_performed', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('date_performed', 'datetime', [
                'null' => true,
            ])
            ->addForeignKey('event_id', 'event', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
