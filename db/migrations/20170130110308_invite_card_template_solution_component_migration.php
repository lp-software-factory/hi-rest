<?php

use Phinx\Migration\AbstractMigration;

class InviteCardTemplateSolutionComponentMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->addColumn('solution_component_owner', 'integer', [
                'null' => true,
            ])
            ->addColumn('solution_component_user_specified', 'boolean', [
                'null' => false,
            ])
            ->addForeignKey('solution_component_owner', 'profile', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->save();
    }
}
