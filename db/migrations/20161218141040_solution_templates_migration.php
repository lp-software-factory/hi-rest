<?php

use Phinx\Migration\AbstractMigration;

class SolutionTemplatesMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('solution_invite_card_templates')
            ->addColumn('solution_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('invite_card_template_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('solution_id', 'solution', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('invite_card_template_id', 'invite_card_template', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create()
        ;

        $this->table('solution_envelope_templates')
            ->addColumn('solution_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('envelope_template_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('solution_id', 'solution', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('envelope_template_id', 'envelope_template', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create()
        ;

        $this->table('event_landing_templates')
            ->addColumn('solution_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('event_landing_template_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('solution_id', 'solution', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('event_landing_template_id', 'event_landing_template', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create()
        ;

        $this->table('solution')
            ->addColumn('target_profile_id', 'integer', [
                'null' => true,
            ])
            ->addForeignKey('target_profile_id', 'profile', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->save();
    }
}
