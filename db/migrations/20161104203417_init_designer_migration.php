<?php

use Phinx\Migration\AbstractMigration;

class InitDesignerMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('designer_request')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('date_reviewed', 'datetime', [
                'null' => true,
            ])
            ->addColumn('form', 'json', [
                'null' => false,
            ])
            ->addColumn('is_reviewed', 'boolean', [
                'default' => false,
                'null' => false,
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
