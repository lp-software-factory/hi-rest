<?php

use Phinx\Migration\AbstractMigration;

class InitInviteCardTemplateMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_template')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('image', 'json', [
                'null' => false,
            ])
            ->addColumn('invite_card_family_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('invite_card_color_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('invite_card_family_id', 'invite_card_family', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->addForeignKey('invite_card_color_id', 'invite_card_color', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
