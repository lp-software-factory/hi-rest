<?php
use Phinx\Migration\AbstractMigration;

class AuthInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('sign_up_request')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('accept_token', 'string', [
                'limit' => 32,
            ])
            ->addColumn('email', 'string', [
                'limit' => 127,
            ])
            ->addColumn('password', 'string', [
                'limit' => 256,
            ])
            ->addColumn('first_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('last_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('is_company_profile', 'boolean')
            ->addColumn('company_name', 'string', [
                'limit' => 256,
                'null' => true,
            ])
            ->addColumn('phone', 'string', [
                'limit' => 32,
                'null' => true,
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
