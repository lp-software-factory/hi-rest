<?php

use Phinx\Migration\AbstractMigration;

class InitEventMailingOptionsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_mailing_options')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('solution_component_owner', 'integer', [
                'null' => true,
            ])
            ->addColumn('solution_component_user_specified', 'boolean', [
                'null' => false,
            ])
            ->addForeignKey('solution_component_owner', 'profile', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
