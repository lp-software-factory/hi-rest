<?php

use Phinx\Migration\AbstractMigration;

class MailingSendResultsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event_mailing')
            ->addColumn('mandrill_results', 'json', [
                'default' => '[]',
                'null' => false
            ])
            ->save();
    }
}
