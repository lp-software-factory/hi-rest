<?php

use Phinx\Migration\AbstractMigration;

class InitDressCodeMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('dress_code')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('image_attachment_id', 'integer', [
                'null' => false
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('description_men', 'json', [
                'null' => false
            ])
            ->addColumn('description_women', 'json', [
                'null' => false
            ])
            ->addForeignKey('image_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->create();
    }
}
