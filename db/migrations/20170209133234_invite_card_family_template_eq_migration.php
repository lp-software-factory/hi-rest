<?php

use Phinx\Migration\AbstractMigration;

class InviteCardFamilyTemplateEqMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_family_templates_eq')
            ->addColumn('invite_card_template_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('invite_card_family_id', 'integer', [
                'null' => false,
            ])
            ->addForeignKey('invite_card_template_id', 'invite_card_template', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('invite_card_family_id', 'invite_card_family', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
