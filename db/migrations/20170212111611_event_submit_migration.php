<?php

use Phinx\Migration\AbstractMigration;

class EventSubmitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('event')
            ->addColumn('is_submit', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->addColumn('date_submit', 'datetime', [
                'null' => true,
            ])
            ->save();
    }
}
