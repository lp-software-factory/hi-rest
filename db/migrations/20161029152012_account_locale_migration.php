<?php

use Phinx\Migration\AbstractMigration;

class AccountLocaleMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('account')
            ->addColumn('locale_id', 'integer', [
                'null' => false
            ])
            ->addForeignKey('locale_id', 'locale', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->save();
    }
}
