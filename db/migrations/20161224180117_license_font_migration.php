<?php

use Phinx\Migration\AbstractMigration;

class LicenseFontMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('font')
            ->addColumn('preview_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('title', 'string', [
                'null' => false,
            ])
            ->addColumn('license', 'text', [
                'null' => false,
                'default' => '""'
            ])
            ->addColumn('font_face', 'text', [
                'null' => false,
                'default' => '""'
            ])
            ->addColumn('supports', 'integer', [
                'null' => false,
            ])
            ->removeColumn('definitions')
            ->addForeignKey('preview_attachment_id', 'attachment', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->save();
    }
}
