<?php

use Phinx\Migration\AbstractMigration;

class ProfileAvatarMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('profile')
            ->addColumn('image', 'json', [
                'null' => false,
                'default' => json_encode([
                    'uid' => '#NONE',
                    'is_auto_generated' => true,
                    'variants' => []
                ])
            ])
            ->save();
    }
}
