<?php

use Phinx\Migration\AbstractMigration;

class InitAddressBookContactMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('address_book_contact')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('owner_profile_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('first_name', 'string', [
                'null' => false,
            ])
            ->addColumn('last_name', 'string', [
                'null' => false,
            ])
            ->addColumn('middle_name', 'string', [
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'null' => false,
            ])
            ->addColumn('phone', 'string', [
                'null' => false,
            ])
            ->addForeignKey('owner_profile_id', 'profile', 'id', [
                'delete' => 'cascade',
                'update' => 'cascade',
            ])
            ->create();

        $this->table('address_book_contact_group_eq')
            ->addColumn('contact_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('contact_group_id', 'integer', [
                'null' => false,
            ])
            ->addIndex(['contact_id', 'contact_group_id'], ['unique' => true])
            ->addForeignKey('contact_id', 'address_book_contact', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addForeignKey('contact_group_id', 'event_recipients_group', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->create();
    }
}
