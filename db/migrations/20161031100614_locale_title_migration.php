<?php

use Phinx\Migration\AbstractMigration;

class LocaleTitleMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('locale')
            ->addColumn('title', 'json', [
                'default' => '{}',
                'null' => false,
            ])
            ->save();
    }
}
