<?php
use Phinx\Migration\AbstractMigration;

class ProfileInitMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('profile')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('account_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('first_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('last_name', 'string', [
                'limit' => 32,
            ])
            ->addColumn('middle_name', 'string', [
                'limit' => 32,
                'null' => true
            ])
            ->addForeignKey('account_id', 'account', 'id', [
                'update' => 'cascade',
                'delete' => 'cascade',
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
