<?php

use Phinx\Migration\AbstractMigration;

class InitAtachmentMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('attachment')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('date_attached_on', 'datetime', [
                'null' => true,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('title', 'string', [
                'null' => false
            ])
            ->addColumn('description', 'string', [
                'null' => false,
            ])
            ->addColumn('is_attached', 'boolean', [
                'default' => false
            ])
            ->addColumn('owner_id', 'integer', [
                'null' => true
            ])
            ->addColumn('owner_code', 'string', [
                'null' => true
            ])
            ->addIndex('sid', ['unique' => true])
            ->addIndex(['owner_id', 'owner_code'])
            ->create();
    }
}
