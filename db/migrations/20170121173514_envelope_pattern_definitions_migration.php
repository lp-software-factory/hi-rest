<?php

use Phinx\Migration\AbstractMigration;

class EnvelopePatternDefinitionsMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('envelope_pattern_definition')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('version', 'string', [
                'limit' => 32,
                'null' => false,
                'default' => '1.0.0',
            ])
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false
            ])
            ->addColumn('envelope_pattern_id', 'integer', [
                'null' => false
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('preview_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('texture_attachment_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('envelope_color_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('width', 'integer', [
                'null' => false,
            ])
            ->addColumn('height', 'integer', [
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addForeignKey('envelope_pattern_id', 'envelope_pattern', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('preview_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('texture_attachment_id', 'attachment', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->addForeignKey('envelope_color_id', 'envelope_color', 'id', [
                'update' => 'cascade',
                'delete' => 'restrict',
            ])
            ->create();


        $this->table('envelope_pattern')
            ->dropForeignKey('preview_attachment_id')
            ->dropForeignKey('texture_attachment_id')
            ->removeColumn('preview_attachment_id')
            ->removeColumn('texture_attachment_id');
    }
}
