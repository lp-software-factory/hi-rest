<?php

use Phinx\Migration\AbstractMigration;

class InitGammaMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_gamma')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('date_created_at', 'datetime', [
                'null' => false,
            ])
            ->addColumn('last_updated_on', 'datetime', [
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'null' => false,
                'default' => 1,
            ])
            ->addColumn('title', 'json', [
                'null' => false
            ])
            ->addColumn('description', 'json', [
                'null' => false
            ])
            ->addColumn('image', 'json', [
                'null' => false,
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
        
        $this->table('invite_card_gamma')
            ->addColumn('is_activated', 'boolean', [
                'null' => false,
                'default' => false,
            ])
            ->save();
    }
}
