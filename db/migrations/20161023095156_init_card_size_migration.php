<?php

use Phinx\Migration\AbstractMigration;

class InitCardSizeMigration extends AbstractMigration
{
    public function change()
    {
        $this->table('invite_card_size')
            ->addColumn('sid', 'string', [
                'null' => false,
                'limit' => 12,
            ])
            ->addColumn('metadata', 'json', [
                'default' => '{}',
            ])
            ->addColumn('position', 'integer', [
                'default' => 1,
                'null' => false
            ])
            ->addColumn('title', 'json')
            ->addColumn('description', 'json')
            ->addColumn('width', 'integer', [
                'null' => false,
            ])
            ->addColumn('height', 'integer', [
                'null' => false,
            ])
            ->addIndex('sid', ['unique' => true])
            ->create();
    }
}
