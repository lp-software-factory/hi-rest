STREAM.MD
=========

Файл содержит нераспределенные заметки, которые необходимо задокументировать.

- Использование `EntityRepository` запрещено; необходимо использовать `ProtectedEntityRepository`
- Чеклист: CASCADE, ON-UPDATE, ON-DELETE, DEFAULT, NULL
- Currency, Money, паттерн Money