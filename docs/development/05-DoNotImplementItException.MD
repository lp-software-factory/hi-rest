DoNotImplementItException
=========================

Данный эксепшен оставляется разработчиком для запрета имплементации определенных методов. Его семантический смысл: ни
в коем случаи не имплементируйте данный функционал.