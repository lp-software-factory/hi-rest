01-DOMAIN-BUNDLE-GUIDE.MD
=========================

Данный документ описывает пошаговую подробную инструкцию по созданию DOMAIN-баднла.


Основные компоненты бандла
--------------------------

Существует ряд категорий, к которому относятся модули (компоненты) бандла.

- `Entity` – Doctrine2-сущности, с котороми работает модель предметной области.
- `Repository` - репозитории для описанных в бандле сущностей. Являются инфраструктнурными  компонентами, чаще всего 
хранят методы по сохранению сущности (`saveTheme`, `saveUser`, `saveStyle`, ...), удалению сущности (`deleteTheme`, 
`deleteStyle`, ...), получению сущностей (`getById`, `getAll`, `listTheme(ListThemesCriteria $criteria)`, ...). 
Репозитории являются приватными компонентами бандла, т.е. *не допускается* использование репозиториев вне бандла, а
желательно и вне сервисов бандла тоже. **Все взаимодействие с репозиториями сущностей должен быть построено через 
сервисы, и все внешнее взаимодействие между бандлами должно быть пострено через сервисы.**
- `Service` – сервисы бандла, или *интерфейс бандла*. В сервисах хранится все то, что бандл дает в публичный доступ на 
использование как на свои нужды, так и на нужды других модулей/бандлов проекта.
- `Parameters` – в случаи, если есть какой-то набор параметров (чаще всего – создание сущности, обновление данных 
сущностей), и этот набор дублируется сквозь архитектурные слои (`Request` -> `Middleware` -> `Service` -> `Repository`), 
то этот набор параметров упаковается в объект специального класса (например, `CreateInviteCardParameters`, 
`SaveThemeParameters`) и уже передается сквозь слои только этот объект. Смотри паттерн: https://sourcemaking.com/refactoring/introduce-parameter-object
- `Exceptions` – исключение необходимо типизировать ("давать названия") только при необходимости. Все эксепшены бандла
должны находится в неймспейсе `{BUNDLE_NAMESPACE}\Exceptions`.

Дополнительные компоненты бандла
--------------------------------

- `Factory` – содержит различные фабрики.
- `Events` – если один бандл строит какое-то внешнее взаимодействие с другими бандлами система, то крайне рекомендуется
данное внешнее взаимодействие вынести в `SubscriptionScript`'ы. Пример: при принятии заявки на дизайнера должно 
отправиться письмо о положительном решении, при необходимости создать аккаунт для дизайнера, и дать роль `designer` 
данному аккаунту. Для того, чтобы сервис `DesignerService` не имел жесткой связи с `AccountService`, то все данное
взаимодействие вынесено в `SubscriptionScript`'ы, которые и соединяют логику данных бандлов и которую – при 
необходимости – достаточно легко удалить.
- `Criteria` – критерии выборки, по сути те же самые `Parameters` с несколько отдельной выделенной семантикой.
- `Config` – конфиги, выраженные в виде классов. Инстансы этих объектов зачастую конфигурируются статическими 
значениями и описываются отдельно в `container.config.php`