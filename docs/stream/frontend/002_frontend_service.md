**Frontend.exports**

На основном сайте, `hi-rest/frontend`, теперь также доступен сервис `FrontendService`. Помимо набора стандартных данных доступен еще дополнительный ключ `exports`, назначение которого состоит в передаче от шаблонизатора с бэкенда во фронт тех или иных данных. Например, на странице редактора событий `/event/{eventId}/editor` бэкенд передает  данные о текущем мероприятии и они доступны с помощью следующего кода:


```
constructor(
        private frontend: FrontendService,
    ) {
        frontend.notify.subscribe(response => {
            this.event = response.exports['event'];
        });
    }
```