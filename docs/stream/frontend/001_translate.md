**В hi-rest/frontend завезен перевод**

1. В директории `src/app/translations` находятся bootstrap-файлы для системы переводов. При разработке все русскоязычные
строки должны быть описаны в `ru.json`. Добавление в `en.json` нежелательно.

2. Для перевода в разметке используется следующая конструкция: `{{ "hi.some.key" | translate }}`

3. Библиотека поддерживает возможность использования подстановки переменных: [https://angular-translate.github.io/docs/#/guide/05_using-translate-directive]()

4. Использование в компонентах и сервисах:

```typescript
@Component({
    selector: 'hi-my-component',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        ITranslateService,
    ]
})
export class MyComponent
{
    constructor(
        private translate: ITranslateServiceInterface,
    ) {}
    
    // ...
    
    getWelcome(): string {
        return this.translation(['my.component.welcome']).then(translations => {
           this.message = translations['my.component.welcome'];
       })
    }
}
```