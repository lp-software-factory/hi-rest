container.config.php
====================

- Содержит конфигурацию DI-контейнера
- Содержит дополнительную конфигурацию. Каждый ключ должен начинаться с `config.hi.rest`
- Каждый файл обязан иметь следующий формат:

```php
<?php
namespace HappyInvite\REST;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
// Остальные use'ы

return [
    // Конфигурация
];
```

- __constructor: обязательные параметры