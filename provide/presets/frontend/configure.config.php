<?php
namespace HappyInvite;

define('APP_TIMER_START', time());
define('HAPPY_INVITE_CACHE_ENABLED', true);
define('HAPPY_INVITE_CACHE_TYPE', 'filesystem');