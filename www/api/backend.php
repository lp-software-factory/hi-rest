<?php
use Zend\Expressive\Router\FastRouteRouter;

define('HI_BOOTSTRAP_START_TIME', microtime(true));

$application = require_once __DIR__.'/../../src/backend/HappyInvite/REST/resources/bootstrap/bootstrap.php';

$RESTApiApplication = new \Zend\Expressive\Application(new FastRouteRouter(), null, new \Zend\Stratigility\NoopFinalHandler());
$RESTApiApplication->pipe('/backend/api', $application);
$RESTApiApplication->pipeRoutingMiddleware();
$RESTApiApplication->pipeDispatchMiddleware();

$RESTApiApplication->run();