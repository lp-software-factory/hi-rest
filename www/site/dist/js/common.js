$(document).ready(function() {

	// desktop
	function is_touch_device() {
		return (('ontouchstart' in window)
			|| (navigator.MaxTouchPoints > 0)
			|| (navigator.msMaxTouchPoints > 0));
	}
	if (is_touch_device()) {
		var body = $('body');
		body.addClass('no-desktop');
	}

	// main page slider
	(function () {
		var slider = $('.js-sl-main');
		if (slider.length) {
			slider.slick({
				dots: true,
				adaptiveHeight: true
			});
		};
	}());

	// contacts
	(function () {
		var contacts = $('.js-contacts'),
			btn      = contacts.find('.js-contacts-btn');
		setTimeout(function () {
			contacts.addClass('is-show-card is-show-back');
		}, 400);
		btn.on('click', function () {
			contacts.removeClass('is-show-back').addClass('is-success');
			setTimeout(function () {
				contacts.addClass('is-show-back');
			}, 5000);
		});
	}());

	// we
	(function () {
		var we    = $('.js-we'),
			btn   = we.find('.js-we-btn'),
			close = we.find('.js-we-close');
		btn.on('click', function () {
			var data = $(this).data('class');
			we.removeClass('is-first is-second');
			we.addClass(data);
			return false;
		});
		close.on('click', function () {
			we.removeClass('is-first is-second');
			return false;
		});
	}());

	// tabs with sliders
	(function () {
		var tabs = $('.js-tabs-sliders'),
			link = tabs.find('.js-tabs-link'),
			item = tabs.find('.js-tabs-item');
		link.on('click', function () {
			var _this   = $(this),
				index   = _this.index(),
				indexEl = item.eq(index);
			if (!_this.hasClass('is-active')) {
				link.removeClass('is-active');
				_this.addClass('is-active');
				item.hide();
				indexEl.fadeIn();
				if (!indexEl.hasClass('is-inited')) {
					indexEl.addClass('is-inited');
					indexEl.find('.js-sl').slick({
						slides: '.js-sl-item',
						slidesToScroll: 1,
						dots: true,
						variableWidth: true,
						centerMode: true,
						centerPadding: '0',
						responsive: [
							{
							breakpoint: 760,
							settings: {
								slidesToShow: 1
							}
						}]
					});
				};
			};
			return false;
		}).first().trigger('click');
	}());

	// tabs with table
	(function () {
		var tabs = $('.js-tabs-tables'),
			link = tabs.find('.js-tabs-link'),
			item = tabs.find('.js-tabs-item');
		link.on('click', function () {
			var _this   = $(this),
				index   = _this.index(),
				indexEl = item.eq(index);
			if (!_this.hasClass('is-active')) {
				link.removeClass('is-active');
				_this.addClass('is-active');
				item.hide();
				indexEl.fadeIn();
			};
			return false;
		});
	}());

	// main page slider review
	(function () {
		var slider = $('.js-sl-review');
		if (slider.length) {
			slider.slick({
				dots: true,
				adaptiveHeight: true
			});
		};
	}());

	// edit
	(function () {
		var btn  = $('.js-edit-btn');
		btn.on('click', function () {
			var _this = $(this),
				input = _this.prev(),
				field = _this.parents('.js-edit'),
				attr  = _this.attr('data-input');
			if (typeof attr !== typeof undefined && attr !== false) {
				var el = $('.' + attr); 
				if (!field.hasClass('is-active')) {
					el.removeAttr('readonly');
				}
				else {
					el.attr('readonly', 'true');
				};	
			}
			if (!field.hasClass('is-active')) {
				field.addClass('is-active');
				input.removeAttr('readonly');
			}
			else {
				field.removeClass('is-active');
				input.attr('readonly', 'true');
			};
 		});
	}());

	// reg form
	(function () {
		var reg        = $('.js-reg'),
			title      = reg.find('.js-reg-title'),
			switchWrap = reg.find('.js-reg-switch'),
			switchEl   = reg.find('.js-reg-switch-el'),
			item       = reg.find('.js-reg-item');
		title.on('click', function () {
			var _this = $(this),
				tab   = _this.data('tab');
			title.removeClass('is-active');
			_this.addClass('is-active');
			switchWrap.addClass('is-active');
			switchEl.removeClass('is-email is-social');
			switchEl.addClass('is-' + tab);
			item.removeClass('is-active');
			$('.js-reg-' + tab).addClass('is-active');
		});
	}());

	// settings
	(function () {
		var btn = $('.js-settings-btn');
		btn.on('click', function () {
			btn.toggleClass('is-active');
			btn.next().toggleClass('is-active');
		});
	}());


	// switch toggle
	(function () {
		var input = $('.js-switch-toggle');
		input.on('change', function () {
			var data = $(this).data('toggle');
			$('.' + data).toggle();
		});
	}());

	// 
	(function () {
		
	}());

	// form-tabs
	(function () {
		var tabs = $('.js-form-tabs');
		tabs.each(function () {
			var thisTabs = $(this), 
				nav      = thisTabs.find('.js-form-tabs-link'), 
				item     = thisTabs.find('.js-form-tabs-item'); 
			nav.on('click', function () {
				var thisNav  = $(this), 
					indexNav = thisNav.index(); 
				nav.removeClass('is-active'); 
				thisNav.addClass('is-active'); 
				item.hide(); 
				item.eq(indexNav).fadeIn(); 
				return false;
			});
		});
	}());

	// select
	(function () {
		var select = $('.js-select');
		select.each(function () {
			var _this      = $(this),
				head       = _this.find('.js-select-head'),
				value      = _this.find('.js-select-value'),
				list       = _this.find('.js-select-list'),
				input      = _this.find('.js-select-input'),
				option     = list.find('a'),
				hideEl;
			head.on('click', function () {
				_this.addClass('is-active');
				list.slideDown();
			});
			// common select
			option.on('click', function () {
				var text = $(this).text();
				_this.removeClass('is-active');
				list.slideUp();
				_this.addClass('is-selected');
				value.text(text);
				input.val(text);
				if ($(this).hasClass('js-select-hide')) {
					hideEl = $(this).data('hide');
					$('.' + hideEl).hide();
				}
				else {
					$('.' + hideEl).show();
				}
				return false;
			});
		});
	}());

	// select custom
	(function () {
		var select = $('.js-sel');
		select.each(function () {
			var _this      = $(this),
				headTitle  = _this.find('.js-sel-head-title'),
				headValue  = _this.find('.js-sel-head-value'),
				value      = _this.find('.js-sel-value'),
				title      = _this.find('.js-sel-title'),
				list       = _this.find('.js-sel-list'),
				input      = _this.find('.js-sel-input'),
				option     = list.find('a'),
				hideEl;
			headTitle.on('click', function () {
				_this.addClass('is-active');
				list.slideDown();
			});
			headValue.on('click', function () {
				_this.removeClass('is-active is-selected');
				headTitle.show();
				headValue.hide();
			});
			$('body').on('click', '.js-sel-list a', function () {
				var text = $(this).text();
				_this.removeClass('is-active');
				headTitle.hide();
				headValue.show();
				list.slideUp();
				_this.addClass('is-selected');
				value.text(text);
				input.val(text);
				if ($(this).hasClass('js-select-hide')) {
					hideEl = $(this).data('hide');
					$('.' + hideEl).hide();
				}
				else {
					$('.' + hideEl).show();
				}
				return false;
			});
		});
	}());

	// table
	(function () {
		var table = $('#table');
		if (table.length) {
			// checkboxes
			var checkAll = $('.js-dt-checkbox-all input'),
				check    = $('.js-dt-checkbox input');
			checkAll.on('change', function () {
				check.prop('checked', this.checked);
			});
			// datatables
			table.DataTable({
				'aoColumnDefs': [
					{
						'bSortable': false,
						'aTargets': [0, 3, 4]
					}
				],
				initComplete: function () {
					this.api().columns().every(function (i) {
						if (i == 3 || i == 4) {
							var column = this;
							var select = $('<select class="th-select-native"><option value=""></option></select>')
								.appendTo($(column.header()))
								.on('change', function () {
									var val = $.fn.dataTable.util.escapeRegex(
										$(this).val()
									);
									column
										.search( val ? '^'+val+'$' : '', true, false )
										.draw();
								});
							column.data().unique().sort().each( function ( d, j ) {
								select.append('<option value="'+d+'">'+d+'</option>');
								var dd       = select.prev(),
									ddHead   = dd.find('.js-dt-select-head'),
									ddValue  = dd.find('.js-dt-select-value'),
									ddList   = dd.find('.js-dt-select-list');
								ddList.append('<a class="select__item" href="#">' + d + '</a>');
								ddHead.on('click', function () {
									dd.addClass('is-active');
									ddList.slideDown();
								});
								ddList.find('a').on('click', function () {
									var _this = $(this),
										text  = _this.text(),
										index = _this.index();
									select.val(text).change();
									dd.removeClass('is-active');
									ddList.slideUp();
									dd.addClass('is-selected');
									ddValue.text(text);
									return false;
								});
							});
						}
					});
				}
			});
		};
	}());

	// news slider
	(function () {
		var slider = $('.js-sl-news');
		if (slider.length) {
				slider.slick({
			});
		};
	}());

	// redactir slider
	(function () {
		var slider = $('.js-sl-red');
		if (slider.length) {
				slider.slick({
					slidesToShow: 3,
					slidesToScroll: 1
			});
		};
	}());

	// function
	(function () {
		var btn   = $('.js-popup-btn'),
			popup = $('.js-popup'),
			close = $('.js-popup-close'),
			body  = $('body');
		btn.on('click', function () {
			var _this = $(this),
				el    = _this.data('popup');
			body.addClass('no-scroll');
			$('.' + el).fadeIn();
			return false;
		});
		close.on('click', function () {
			body.removeClass('no-scroll');
			popup.fadeOut();
			return false;
		});
	}());

	// show menu
	(function () {
		var actions = $('.js-actions'),
			list    = $('.js-actions-list'),
			link    = $('.js-actions-link');
		actions.on('click', function () {
			var _this = $(this);
			if (_this.hasClass('is-active')) {
				_this.removeClass('is-active');
				_this.next().slideUp();
			}
			else {
				actions.removeClass('is-active');
				_this.addClass('is-active');
				list.slideUp();
				_this.next().slideDown();
			}
			return false;
		});
		link.on('click', function () {
			list.slideUp();
		});
	}());

	// we
	(function () {
		var item = $('.js-we-item');
		item.on('click', function () {
			$(this).toggleClass('is-active');
			return false;
		});
	}());

	// accordeon
	(function () {
		var accord = $('.js-accord-item');
		accord.each(function () {
			var thisAccord = $(this),
				head = thisAccord.find('.js-accord-head'),
				inner = thisAccord.find('.js-accord-body');
			head.on('click', function () {
				thisAccord.toggleClass('is-active');
				inner.slideToggle();
			});
		});
	}());

	// tooltip
	(function () {
		var el = $('.js-tooltip');
		if (el.length) {
			el.tooltipster();
		};
	}());

	// isotop
	(function () {
		var grid = $('.js-filter');
		if (grid.length) {
			grid.isotope({
				itemSelector: '.js-filter-item',
				percentPosition: true,
				masonry: {
					columnWidth: '.js-filter-item'
				}
			})
		};
	}());

	// answer
	(function () {
		var answer     = $('.js-answer'),
			form       = answer.find('.js-answer-form'),
			thank      = answer.find('.js-answer-thank'),
			send       = answer.find('.js-answer-send'),
			el         = answer.find('.js-answer-el'),
			menu       = answer.find('.js-answer-menu'),
			footer     = answer.find('.js-answer-footer');
		function answerHeight () {
			var wndHeight    = $(window).height(),
				menuHeight   = menu.height(),
				footerHeight = footer.height(),
				elHeight     = wndHeight - menuHeight - footerHeight;
			el.css('min-height', elHeight);
		}
		answerHeight();
		send.on('click', function () {
			form.slideUp(600, function () {
				thank.slideDown(600);
				answerHeight();
			});
			return false;
		});
		$(window).resize(function () {
			answerHeight();
		});
	}());

	// rate
	(function () {
		var rate  = $('.js-rate');
		if (rate.length) {
			rate.each(function () {
				var _this = $(this),
					input = _this.find('.js-rate-input'),
					item  = _this.find('.js-rate-item');
				input.on('change', function () {
					var _this = $(this);
					if (_this.is(':checked')) {
						item.removeClass('is-active');
						_this.parents('.js-rate-item').addClass('is-active');
					};
				});
			});
		};
	}());
	
	// add group in select
	(function () {
		var add = $('.js-add-group'),
			input = add.find('.js-add-group-input'),
			item = add.find('.js-add-group-item'),
			list = add.find('.js-add-group-list'),
			btn  = add.find('.js-add-group-btn');
		btn.on('click', function () {
			var name  = input.val();
			if (name.length) {
				list.append('<a class="select__item" href="#">' + name + '</a>');
				input.val('');
			};
		})
	}());

	// order
	(function () {
		var btn = $('.js-order-btn'),
			wrap = $('.js-order-wrap');
		btn.on('click', function () {
			var _this = $(this),
				data  = _this.data('wrap');
			_this.addClass('is-active');
			wrap.slideUp();
			$('.' + data).slideDown();
		});
	}());

	// function
	(function () {
		var	info  = $('.js-info'),
			open  = $('.js-info-open');
		open.on('click', function () {
			info.show();
			return false;
		});
	}());

	// 
	// redactor
	// 

	// tabs
	(function () {
		var tabs = $('.js-ev-tabs'),
			btn  = tabs.find('.js-ev-tabs-btn'),
			wrap = tabs.find('.js-ev-tabs-wrap');
		btn.on('click', function () {
			var _this = $(this);
			if (_this.hasClass('is-active')) {
				btn.removeClass('is-active');
				wrap.hide();
			}
			else {
				btn.removeClass('is-active');
				wrap.hide();
				_this.addClass('is-active');
				_this.next().show();
			}
		});
	}());

	// date
	(function () {
		var dp      = $('.js-ev-dp'),
			ui      = dp.find('.js-ev-dp-ui'),
			wrap    = dp.find('.js-ev-dp-wrap'),
			value   = dp.find('.js-ev-dp-value'),
			dayEl   = dp.find('.js-ev-dp-day'),
			monthEl = dp.find('.js-ev-dp-month'),
			yearEl  = dp.find('.js-ev-dp-year');
		if (dp.length) {
			$.datepicker.regional['ru'] = { 
				closeText: 'Закрыть', 
				prevText: '&#x3c;', 
				nextText: '&#x3e;', 
				currentText: 'Сегодня', 
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'], 
				monthNamesShort: ['Янв','Фев','Мар','Апр','Мая','Июня','Июля','Авг','Сент','Окт','Нояб','Дек'],
				dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'], 
				dayNamesShort: ['Вск','Пнд','Втр','Срд','Чтв','Птн','Сбт'], 
				dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
			}; 
			$.datepicker.setDefaults($.datepicker.regional['ru']);
			ui.datepicker({
				firstDay: 1, 
				isRTL: false,
				minDate: '+1d',
				onSelect: function(dateText) {
					var date        = new Date(dateText),
						day         = date.getDate(),  
						monthNumber = date.getMonth(),
						month       = $.datepicker.regional['ru'].monthNamesShort[monthNumber],          
						year        = date.getFullYear();
		
					dayEl.text(day);
					monthEl.text(month);
					yearEl.text(year);

					wrap.hide();
					ui.hide();
					value.show();
				}
			});
			wrap.on('click', function () {
				ui.toggle();
			});
			value.on('click', function () {
				ui.toggle();
			});
		};
	}());

	// editor
	(function () {
		var editor = $('.js-ev-editor'),
			btn    = $('.js-ev-editor-btn');
		if (editor.length) {
			// 
			CKEDITOR.inline('editor-title', {
				customConfig: 'config-no-link.js'
			});
			CKEDITOR.instances['editor-title'].on('focus', function () {
				// this.setData('');
			});
			CKEDITOR.instances['editor-title'].on('blur', function () {
				// var defaultValue = this.element.$.defaultValue,
				// 	currentValue = this.getData();
				// if (currentValue == '') {
				// 	this.setData(defaultValue);
				// };
			});



			CKEDITOR.inline('editor-author', {
				customConfig: 'config-no-link.js'
			});
			CKEDITOR.inline('editor-invite', {
				customConfig: 'config-no-link.js'
			});
			CKEDITOR.inline('editor-programm-title', {
				customConfig: 'config-no-align.js'
			});
			CKEDITOR.inline('editor-guestlist', {
				customConfig: 'config-no-align.js'
			});
			CKEDITOR.inline('editor-wishlist', {
				customConfig: 'config-no-align.js'
			});
			CKEDITOR.inline('editor-gallery', {
				customConfig: 'config-no-align.js'
			});
			CKEDITOR.inline('editor-block-title', {
				customConfig: 'config-no-align.js'
			});
			CKEDITOR.inline('editor-dress-title', {
				customConfig: 'config-no-align.js'
			});
			CKEDITOR.inline('editor-programm-desc');
			CKEDITOR.inline('editor-block-text');
			CKEDITOR.inline('editor-dress-male');
			CKEDITOR.inline('editor-dress-female');

			// btn
			btn.on('click', function () {
				var _this = $(this);
				if (_this.hasClass('is-active')) {
					_this.removeClass('is-active');
					_this.prev().blur();
				}
				else {
					_this.addClass('is-active');
					_this.prev().focus();
				}
				return false;
			});
		};
	}());

	// bg
	(function () {
		var bg     = $('.js-ev-bg'),
			change = $('.js-ev-change');
		if (bg.length) {
			bg.minicolors({
				defaultValue: '#edce16',
				position: 'bottom right',
				change: function (value, opacity) {
					change.css({
						'background': '-webkit-linear-gradient(transparent, ' + value + ')',
						'background': 'linear-gradient(transparent, ' + value + ')'
					});
				}
			});
		};
	}());

	// check tabs toggle
	(function () {
		var check  = $('.js-ev-check-tabs-toggle'),
			change = $('.js-ev-change');
		check.change(function () {
			var data = $(this).data('el'),
				el   = $('.' + data);
			el.toggle();
			change.show();
			if (check.not(':checked').length == check.length) {
				change.hide();
			};
		});
	}());

	// check toggle
	(function () {
		var check     = $('.js-ev-check-toggle'),
			checkPage = $('.js-ev-check-toggle-page'),
			close     = $('.js-ev-check-close'),
			wrap      = $('.js-ev-wrap'),
			page      = $('.js-ev-page'),
			finish    = $('.js-ev-finish'),
			smile     = $('.js-ev-smile');
		check.on('click', function () {
			var _this = $(this),
				data  = _this.data('el'),
				el    = $('.' + data);
			el.toggle();
			if (!checkPage.is(':checked')) {
				checkPage.addClass('is-active');
				checkPage.trigger('click');
			}
		});
		checkPage.on('click', function () {
			page.toggle();
			smile.toggle();
			finish.toggle();
			if (checkPage.is(':checked')) {
				check.each(function () {
					var _this = $(this);
					if (!_this.is(':checked')) {
						_this.trigger('click');
					};
				});
			}
			else {
				check.each(function () {
					var _this = $(this);
					if (_this.is(':checked')) {
						_this.trigger('click');
					};
				});
			}
		});
		close.on('click', function () {
			var dataClose = $(this).data('el');
			check.each(function () {
				var _this     = $(this),
					dataCheck = _this.data('el');
				if (dataCheck == dataClose) {
					_this.trigger('click');
				};
			});
		});
	}());

	// mask
	(function () {
		var input = $('.js-ev-mask-time');
		if (input.length) {
			input.mask('00:00');
		};
	}());

	// tags
	(function () {
		var wrap   = $('.js-ev-select'),
			select = wrap.find('.js-ev-select-tags'),
			btn    = wrap.find('.js-ev-select-btn');
		if (select.length) {
			select.select2({
				tags: true
			});
			btn.on('click', function () {
				var _this = $(this);
				if (_this.hasClass('is-active')) {
					_this.removeClass('is-active');
					select.prop('disabled', true);
				}
				else {
					_this.addClass('is-active');
					select.prop('disabled', false);
				}
			});
		};
	}());

	// google map
	(function () {
		var btn = $('.js-ev-gmap-btn'),
			map = $('.js-ev-gmap');
		btn.on('click', function () {
			map.toggleClass('is-active');
			return false;
		});
	}());

	// function
	(function () {
		var btn = $('.js-btn'),
			table = $('.js-table');
		btn.on('click', function () {
			table.toggleClass('is-active');
			return false;
		});
	}());

	// reply pass
	(function () {
		var btn  = $('.js-reply-pass-btn'),
			wrap = $('.js-reply-pass'),
			done = $('.js-reply-pass-done');
		btn.on('click', function () {
			wrap.slideToggle();
		});
		done.on('click', function () {
			wrap.slideUp();
		});
	}());

	// delivery
	(function () {
		var input = $('.js-delivery-all'),
			text  = $('.js-delivery-text'),
			send  = $('.js-delivery-send');
		input.on('change', function () {
			text.toggle();
			send.toggleClass('is-show');
		});
	}());
});