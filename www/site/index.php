<?php
use Zend\Expressive\Router\FastRouteRouter;

define('HI_BOOTSTRAP_START_TIME', microtime(true));

$application = require_once __DIR__.'/../../src/backend/HappyInvite/MainSite/resources/bootstrap/bootstrap.php'; /** @var \Zend\Expressive\Application $application */
$application->run();