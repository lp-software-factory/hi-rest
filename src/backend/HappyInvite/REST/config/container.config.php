<?php
namespace HappyInvite\REST;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Mail\Config\MailConfig;
use HappyInvite\Platform\Bundles\APIDocs\Generator\Impl\BundlesAPIDocsGenerator;
use HappyInvite\Platform\Bundles\APIDocs\Service\APIDocsService;

return [
    APIDocsService::class => object()->constructorParameter('generator', get(BundlesAPIDocsGenerator::class)),
    MailConfig::class => object()->constructorParameter('config', get('config.hi.rest.mail')),
];