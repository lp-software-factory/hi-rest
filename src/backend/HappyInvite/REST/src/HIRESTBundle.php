<?php
namespace HappyInvite\REST;

use HappyInvite\REST\Bundle\RESTBundle;
use HappyInvite\REST\Bundles\Access\AccessRESTBundle;
use HappyInvite\REST\Bundles\Account\AccountRESTBundle;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\ContactRESTBundle;
use HappyInvite\REST\Bundles\Attachment\AttachmentRESTBundle;
use HappyInvite\REST\Bundles\Auth\AuthRESTBundle;
use HappyInvite\REST\Bundles\Avatar\AvatarRESTBundle;
use HappyInvite\REST\Bundles\Company\CompanyRESTBundle;
use HappyInvite\REST\Bundles\CompanyProfile\CompanyProfileRESTBundle;
use HappyInvite\REST\Bundles\Designer\DesignerRESTBundle;
use HappyInvite\REST\Bundles\DesignerRequests\DesignerRequestsRESTBundle;
use HappyInvite\REST\Bundles\DressCode\DressCodeRESTBundle;
use HappyInvite\REST\Bundles\Envelope\EnvelopeRESTBundle;
use HappyInvite\REST\Bundles\Event\EventRESTBundle;
use HappyInvite\REST\Bundles\EventType\EventTypeRESTBundle;
use HappyInvite\REST\Bundles\EventTypeGroup\EventTypeGroupRESTBundle;
use HappyInvite\REST\Bundles\Fonts\FontsRESTBundle;
use HappyInvite\REST\Bundles\Frontend\FrontendRESTBundle;
use HappyInvite\REST\Bundles\InviteCard\InviteCardRESTBundle;
use HappyInvite\REST\Bundles\Locale\LocaleRESTBundle;
use HappyInvite\REST\Bundles\Mail\MailRESTBundle;
use HappyInvite\REST\Bundles\Mandrill\MandrillRESTBundle;
use HappyInvite\REST\Bundles\Money\MoneyRESTBundle;
use HappyInvite\REST\Bundles\OAuth2Providers\OAuth2ProvidersRESTBundle;
use HappyInvite\REST\Bundles\Palette\PaletteRESTBundle;
use HappyInvite\REST\Bundles\PHPUnit\PHPUnitBundle;
use HappyInvite\REST\Bundles\Product\ProductRESTBundle;
use HappyInvite\REST\Bundles\Profile\ProfileRESTBundle;
use HappyInvite\REST\Bundles\Solution\SolutionRESTBundle;
use HappyInvite\REST\Bundles\Translation\TranslationRESTBundle;
use HappyInvite\REST\Bundles\Version\VersionRESTBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScriptInjectableBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\ErrorHandlerPipeInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\InjectSchemaServiceInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\NotFoundPipeInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\PipeMiddlewaresInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\RoutesInitScript;

final class HIRESTBundle extends RESTBundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        $bundles = [
            new PHPUnitBundle(),
            new AvatarRESTBundle(),
            new FrontendRESTBundle(),
            new CompanyRESTBundle(),
            new AccountRESTBundle(),
            new ProfileRESTBundle(),
            new AuthRESTBundle(),
            new VersionRESTBundle(),
            new AccessRESTBundle(),
            new MailRESTBundle(),
            new LocaleRESTBundle(),
            new EventTypeRESTBundle(),
            new EventTypeGroupRESTBundle(),
            new InviteCardRESTBundle(),
            new AttachmentRESTBundle(),
            new PaletteRESTBundle(),
            new ProductRESTBundle(),
            new MoneyRESTBundle(),
            new OAuth2ProvidersRESTBundle(),
            new DesignerRequestsRESTBundle(),
            new DesignerRESTBundle(),
            new CompanyProfileRESTBundle(),
            new FontsRESTBundle(),
            new EnvelopeRESTBundle(),
            new EventRESTBundle(),
            new SolutionRESTBundle(),
            new DressCodeRESTBundle(),
            new TranslationRESTBundle(),
            new ContactRESTBundle(),
            new MandrillRESTBundle(),
        ];

        foreach($bundles as $bundle) {
            if(! ($bundle instanceof RESTBundle)) {
                throw new \Exception(sprintf('Invalid bundle, should be extended from `%s`', RESTBundle::class));
            }
        }

        return $bundles;
    }

    public function getInitScripts(): array
    {
        return [
            new ErrorHandlerPipeInitScript(),
            new RoutesInitScript([
                'locale',
                'auth',
                'pipes',
                'default',
                'final',
            ]),
            new InjectSchemaServiceInitScript(),
            new PipeMiddlewaresInitScript(),
            new NotFoundPipeInitScript(),
        ];
    }
}