<?php
namespace HappyInvite\REST\Bundle;

use HappyInvite\Platform\Bundles\HIBundle;
use HappyInvite\Platform\Bundles\APIDocs\Bundle\APIDocsBundle;

abstract class RESTBundle extends HIBundle implements APIDocsBundle
{
    public function hasAPIDocs(): bool
    {
        return is_dir(sprintf('%s/../api-docs', $this->getDir()));
    }

    public function getAPIDocsDir(): string
    {
        return sprintf('%s/../api-docs', $this->getDir());
    }
}