<?php
namespace HappyInvite\REST\Exceptions;

final class FileNotUploadedException extends \Exception {}