<?php
namespace HappyInvite\REST\Command\Service;

use DI\Container;
use HappyInvite\REST\Command\Resolve\CommandResolverBuilder;

class CommandService
{
    /** @var Container */
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    public function createResolverBuilder(): CommandResolverBuilder {
        return new CommandResolverBuilder($this->container);
    }
}
