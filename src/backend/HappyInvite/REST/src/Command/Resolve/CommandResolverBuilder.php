<?php
namespace HappyInvite\REST\Command\Resolve;

use HappyInvite\REST\Command\Command;
use HappyInvite\REST\Command\Resolve\Resolvers\CallbackCommandResolver;
use HappyInvite\REST\Command\Resolve\Resolvers\CompositionCommandResolver;
use HappyInvite\REST\Command\Resolve\Resolvers\DirectCommandResolver;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface;

class CommandResolverBuilder
{
    /** @var CompositionCommandResolver */
    private $composition;

    /** @var Container */
    private $container;

    public function __construct(Container $container) {
        $this->composition = new CompositionCommandResolver();
        $this->container = $container;
    }

    public function attachDirect(string $command, string $commandClassName, string $allowedMethod = null, string $attr = 'command'): self {
        $this->composition->attachResolver(new DirectCommandResolver($command, $commandClassName, $allowedMethod, $attr));

        return $this;
    }

    public function attachCallable(Callable $callback, string $commandClassName): self {
        $this->composition->attachResolver(new CallbackCommandResolver($callback, $commandClassName));

        return $this;
    }

    public function getCommandResolver(): CompositionCommandResolver
    {
        return $this->composition;
    }

    public function resolve(ServerRequestInterface $request): Command
    {
        return $this->composition->resolve($request, $this->container);
    }
}