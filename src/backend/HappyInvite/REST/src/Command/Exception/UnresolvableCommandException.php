<?php
namespace HappyInvite\REST\Command\Exception;

class UnresolvableCommandException extends \Exception {}