<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware\Command;

use HappyInvite\Domain\Bundles\DressCode\Exceptions\DressCodeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteDressCodeCommand extends AbstractDressCodeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $dressCodeId = $request->getAttribute('dressCodeId');

        try {
            $this->service->deleteDressCode($dressCodeId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(DressCodeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}