<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\DressCode\Middleware\Command\CreateDressCodeCommand;
use HappyInvite\REST\Bundles\DressCode\Middleware\Command\DeleteDressCodeCommand;
use HappyInvite\REST\Bundles\DressCode\Middleware\Command\EditDressCodeCommand;
use HappyInvite\REST\Bundles\DressCode\Middleware\Command\GetDressCodeCommand;
use HappyInvite\REST\Bundles\DressCode\Middleware\Command\ListDressCodeCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class DressCodeMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateDressCodeCommand::class)
            ->attachDirect('edit', EditDressCodeCommand::class)
            ->attachDirect('delete', DeleteDressCodeCommand::class)
            ->attachDirect('get', GetDressCodeCommand::class)
            ->attachDirect('list', ListDressCodeCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}