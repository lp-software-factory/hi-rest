<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware\Command;

use HappyInvite\Domain\Bundles\DressCode\Exceptions\DressCodeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetDressCodeCommand extends AbstractDressCodeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $dressCodeId = $request->getAttribute('dressCodeId');

        try {
            $entity = $this->service->getById($dressCodeId);

            $responseBuilder
                ->setJSON([
                    'dress_code' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(DressCodeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}