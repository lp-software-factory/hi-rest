<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware\Command;

use HappyInvite\Domain\Bundles\DressCode\Formatter\DressCodeFormatter;
use HappyInvite\Domain\Bundles\DressCode\Service\DressCodeService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\DressCode\ParametersFactory\DressCodeParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractDressCodeCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var DressCodeParametersFactory */
    protected $parametersFactory;

    /** @var DressCodeFormatter */
    protected $formatter;

    /** @var DressCodeService */
    protected $service;

    public function __construct(
        AccessService $accessService,
        DressCodeParametersFactory $parametersFactory,
        DressCodeFormatter $formatter,
        DressCodeService $service
    ) {
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}