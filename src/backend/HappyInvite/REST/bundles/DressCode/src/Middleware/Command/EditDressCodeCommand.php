<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware\Command;

use HappyInvite\Domain\Bundles\DressCode\Exceptions\DressCodeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditDressCodeCommand extends AbstractDressCodeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $dressCodeId = $request->getAttribute('dressCodeId');

        try {
            $parameters = $this->parametersFactory->factoryEditParameters($dressCodeId, $request);

            $entity = $this->service->editDressCode($dressCodeId, $parameters);

            $responseBuilder
                ->setJSON([
                    'dress_code' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(DressCodeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}