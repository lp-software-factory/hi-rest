<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListDressCodeCommand extends AbstractDressCodeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $responseBuilder
            ->setJSON([
                'dress_codes' => $this->formatter->formatMany(
                    $this->service->getAll()
                )
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}