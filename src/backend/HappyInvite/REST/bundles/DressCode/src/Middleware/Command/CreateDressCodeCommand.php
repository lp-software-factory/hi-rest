<?php
namespace HappyInvite\REST\Bundles\DressCode\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateDressCodeCommand extends AbstractDressCodeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->parametersFactory->factoryCreateParameters($request);

        $entity = $this->service->createDressCode($parameters);

        $responseBuilder
            ->setJSON([
                'dress_code' => $this->formatter->formatOne($entity)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}