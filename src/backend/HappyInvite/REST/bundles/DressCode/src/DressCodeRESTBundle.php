<?php
namespace HappyInvite\REST\Bundles\DressCode;

use HappyInvite\REST\Bundle\RESTBundle;

final class DressCodeRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}