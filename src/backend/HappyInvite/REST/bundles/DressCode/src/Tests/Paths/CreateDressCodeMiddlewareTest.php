<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\DressCode\Tests\DressCodeMiddlewareTestCase;

final class CreateDressCodeMiddlewareTest extends DressCodeMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'image_attachment_id' => AttachmentFixture::$attachmentImage->getId(),
            'title' => [['region' => 'en_US', 'value' => 'Dress Code EF1']],
            'description' => [['region' => 'en_US', 'value' => 'Dress Code EF1 Description']],
            'description_men' => [['region' => 'en_US', 'value' => 'Dress Code EF1 Description (Men)']],
            'description_women' => [['region' => 'en_US', 'value' => 'Dress Code EF1 Description (Women)']],
        ];
    }

    public function test200()
    {
        $id = $this->requestDressCodeCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'dress_code' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'image' => [
                            'id' => $json['image_attachment_id'],
                        ],
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'description_men' => $json['description_men'],
                        'description_women' => $json['description_women'],
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['dress_code']['entity']['id'];
            });

        $this->requestDressCodeGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->requestDressCodeCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestDressCodeCreate($this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

}