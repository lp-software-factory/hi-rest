<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Paths;

use HappyInvite\REST\Bundles\DressCode\Tests\DressCodeMiddlewareTestCase;
use HappyInvite\REST\Bundles\DressCode\Tests\Fixture\DressCodeFixture;

final class GetDressCodeMiddlewareTest extends DressCodeMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new DressCodeFixture());

        $fixture = DressCodeFixture::$dressCodeEF1;

        $this->requestDressCodeGetById($fixture->getId())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'dress_code' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new DressCodeFixture());

        $this->requestDressCodeGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}