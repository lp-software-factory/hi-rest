<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\DressCode\Tests\DressCodeMiddlewareTestCase;
use HappyInvite\REST\Bundles\DressCode\Tests\Fixture\DressCodeFixture;

final class EditDressCodeMiddlewareTest extends DressCodeMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'image_attachment_id' => AttachmentFixture::$attachmentImage2->getId(),
            'title' => [['region' => 'en_US', 'value' => 'Dress Code EF1 ***']],
            'description' => [['region' => 'en_US', 'value' => 'Dress Code EF1 Description ***']],
            'description_men' => [['region' => 'en_US', 'value' => 'Dress Code EF1 Description (Men) ***']],
            'description_women' => [['region' => 'en_US', 'value' => 'Dress Code EF1 Description (Women) ***']],
        ];
    }

    public function test200()
    {
        $this->upFixture(new DressCodeFixture());

        $fixture = DressCodeFixture::$dressCodeEF1;

        $this->requestDressCodeEdit($fixture->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'dress_code' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                        'image' => [
                            'id' => $json['image_attachment_id'],
                        ],
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'description_men' => $json['description_men'],
                        'description_women' => $json['description_women'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new DressCodeFixture());

        $fixture = DressCodeFixture::$dressCodeEF1;

        $this->requestDressCodeEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestDressCodeEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new DressCodeFixture());

        $this->requestDressCodeEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}