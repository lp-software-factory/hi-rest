<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\DressCode\Tests\DressCodeMiddlewareTestCase;
use HappyInvite\REST\Bundles\DressCode\Tests\Fixture\DressCodeFixture;

final class DeleteDressCodeMiddlewareTest extends DressCodeMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new DressCodeFixture());

        $id = DressCodeFixture::$dressCodeEF1->getId();

        $this->requestDressCodeGetById($id)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestDressCodeDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestDressCodeGetById($id)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new DressCodeFixture());

        $id = DressCodeFixture::$dressCodeEF1->getId();

        $this->requestDressCodeDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestDressCodeDelete($id)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new DressCodeFixture());

        $this->requestDressCodeDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}