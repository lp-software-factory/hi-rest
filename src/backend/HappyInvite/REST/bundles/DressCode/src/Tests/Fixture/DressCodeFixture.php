<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\DressCode\Entity\DressCode;
use HappyInvite\Domain\Bundles\DressCode\Parameters\CreateDressCodeParameters;
use HappyInvite\Domain\Bundles\DressCode\Service\DressCodeService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class DressCodeFixture implements Fixture
{
    /** @var DressCode */
    public static $dressCodeEF1;

    /** @var DressCode */
    public static $dressCodeEF2;

    /** @var DressCode */
    public static $dressCodeEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(DressCodeService::class); /** @var DressCodeService $service */

        self::$dressCodeEF1 = $service->createDressCode(new CreateDressCodeParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF1']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF1 Description']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF1 Description (Men)']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF1 Description (Women)']])
        ));

        self::$dressCodeEF2 = $service->createDressCode(new CreateDressCodeParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF2']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF2 Description']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF2 Description (Men)']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF2 Description (Women)']])
        ));

        self::$dressCodeEF3 = $service->createDressCode(new CreateDressCodeParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF3']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF3 Description']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF3 Description (Men)']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Dress Code EF3 Description (Women)']])
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$dressCodeEF1,
            self::$dressCodeEF2,
            self::$dressCodeEF3,
        ];
    }
}