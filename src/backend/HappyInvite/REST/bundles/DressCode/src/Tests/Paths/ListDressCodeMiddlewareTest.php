<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Paths;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\DressCode\Entity\DressCode;
use HappyInvite\REST\Bundles\DressCode\Tests\DressCodeMiddlewareTestCase;
use HappyInvite\REST\Bundles\DressCode\Tests\Fixture\DressCodeFixture;

final class ListDressCodeMiddlewareTest extends DressCodeMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new DressCodeFixture());

        $ids = $this->requestDressCodeList()
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
            ])
            ->fetch(function(array $json) {
                $this->assertTrue(isset($json['dress_codes']) && is_array($json['dress_codes']));

                return array_map(function(array $input) {
                    return $input['entity']['id'];
                }, $json['dress_codes']);
            });

        $expected = Chain::create(DressCodeFixture::getOrderedList())
            ->map(function(DressCode $dressCode) {
                return $dressCode->getId();
            })
            ->array;

        $this->assertEquals($expected, $ids);
    }
}