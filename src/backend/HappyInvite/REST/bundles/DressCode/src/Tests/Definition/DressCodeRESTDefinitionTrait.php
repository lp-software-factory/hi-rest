<?php
namespace HappyInvite\REST\Bundles\DressCode\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait DressCodeRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestDressCodeCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/dress-code/create')
            ->setParameters($json);
    }

    protected function requestDressCodeEdit(int $dressCodeId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/dress-code/%d/edit', $dressCodeId))
            ->setParameters($json);
    }

    protected function requestDressCodeDelete(int $dressCodeId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/dress-code/%d/delete', $dressCodeId));
    }

    protected function requestDressCodeGetById(int $dressCodeId): RESTRequest
    {
        return $this->request('GET', sprintf('/dress-code/%d/get', $dressCodeId));
    }

    protected function requestDressCodeList(): RESTRequest
    {
        return $this->request('GET', '/dress-code/list');
    }
}