<?php
namespace HappyInvite\REST\Bundles\DressCode\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\DressCode\Parameters\CreateDressCodeParameters;
use HappyInvite\Domain\Bundles\DressCode\Parameters\EditDressCodeParameters;
use HappyInvite\REST\Bundles\DressCode\Request\CreateDressCodeRequest;
use HappyInvite\REST\Bundles\DressCode\Request\EditDressCodeRequest;
use Psr\Http\Message\ServerRequestInterface;

final class DressCodeParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function factoryCreateParameters(ServerRequestInterface $request): CreateDressCodeParameters
    {
        $json = (new CreateDressCodeRequest($request))->getParameters();

        return new CreateDressCodeParameters(
            $this->attachmentService->getById($json['image_attachment_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            new ImmutableLocalizedString($json['description_men']),
            new ImmutableLocalizedString($json['description_women'])
        );
    }

    public function factoryEditParameters(int $dressCodeId, ServerRequestInterface $request): EditDressCodeParameters
    {
        $json = (new EditDressCodeRequest($request))->getParameters();

        return new EditDressCodeParameters(
            $this->attachmentService->getById($json['image_attachment_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            new ImmutableLocalizedString($json['description_men']),
            new ImmutableLocalizedString($json['description_women'])
        );
    }
}