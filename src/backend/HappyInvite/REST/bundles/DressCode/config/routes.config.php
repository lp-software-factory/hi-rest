<?php
namespace HappyInvite\REST\Bundles\DressCode;

use HappyInvite\REST\Bundles\DressCode\Middleware\DressCodeMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/dress-code/{command:create}[/]',
                'middleware' => DressCodeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/dress-code/{dressCodeId}/{command:edit}[/]',
                'middleware' => DressCodeMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/dress-code/{dressCodeId}/{command:delete}[/]',
                'middleware' => DressCodeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/dress-code/{dressCodeId}/{command:get}[/]',
                'middleware' => DressCodeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/dress-code/{command:list}[/]',
                'middleware' => DressCodeMiddleware::class,
            ],
        ]
    ]
];