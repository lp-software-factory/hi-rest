<?php
namespace HappyInvite\REST\Bundles\Mandrill;

use HappyInvite\REST\Bundle\RESTBundle;

final class MandrillRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}