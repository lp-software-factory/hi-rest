<?php
namespace HappyInvite\REST\Bundles\Palette;

use HappyInvite\REST\Bundle\RESTBundle;

final class PaletteRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}