<?php
namespace HappyInvite\REST\Bundles\Event;

use HappyInvite\REST\Bundle\RESTBundle;
use HappyInvite\REST\Bundles\Event\Bundles\Event\EventRESTBundle as EventEntityRESTBundle;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\EventLandingTemplateRESTBundle;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailing\EventMailingRESTBundle;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\EventMailingOptionsRESTBundle;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\EventRecipientsRESTBundle;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\EventRecipientsGroupRESTBundle;

final class EventRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new EventEntityRESTBundle(),
            new EventLandingTemplateRESTBundle(),
            new EventMailingOptionsRESTBundle(),
            new EventRecipientsRESTBundle(),
            new EventRecipientsGroupRESTBundle(),
            new EventMailingRESTBundle(),
        ];
    }
}