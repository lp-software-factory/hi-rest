<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Exceptions\EventRecipientsNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditEventRecipientsCommand extends AbstractEventRecipientsCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $eventRecipientsId = $request->getAttribute('eventRecipientsId');

        try {
            $parameters = $this->parametersFactory->factoryEditParameters($eventRecipientsId, $request);

            $entity = $this->service->editEventRecipients($eventRecipientsId, $parameters);

            $responseBuilder
                ->setJSON([
                    'event_recipients' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventRecipientsNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(EventIsNotYoursException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }

        return $responseBuilder->build();
    }
}