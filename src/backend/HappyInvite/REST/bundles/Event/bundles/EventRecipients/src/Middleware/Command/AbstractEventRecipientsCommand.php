<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Formatter\EventRecipientsFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\ParametersFactory\EventRecipientsParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEventRecipientsCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var EventRecipientsParametersFactory */
    protected $parametersFactory;

    /** @var EventRecipientsFormatter */
    protected $formatter;

    /** @var EventRecipientsService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        EventRecipientsParametersFactory $parametersFactory,
        EventRecipientsFormatter $formatter,
        EventRecipientsService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}