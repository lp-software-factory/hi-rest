<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Tests\EventRecipientsMiddlewareTestCase;

final class GetEventRecipientsMiddlewareTest extends EventRecipientsMiddlewareTestCase
{
    public function test200Admin()
    {
        $fixture = EventFixture::$eventRecipients1;

        $this->requestEventRecipientsGetById($fixture->getId())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event_recipients' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test200Owner()
    {
        $fixture = EventFixture::$eventRecipients1;

        $this->requestEventRecipientsGetById($fixture->getId())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event_recipients' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $fixture = EventFixture::$eventRecipients1;

        $this->requestEventRecipientsGetById($fixture->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventRecipientsGetById($fixture->getId())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventRecipientsGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}