<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EventRecipientsRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEventRecipientsCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/event/recipients/create')
            ->setParameters($json);
    }

    protected function requestEventRecipientsEdit(int $eventRecipientsId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/recipients/%d/edit', $eventRecipientsId))
            ->setParameters($json);
    }

    protected function requestEventRecipientsDelete(int $eventRecipientsId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/event/recipients/%d/delete', $eventRecipientsId));
    }

    protected function requestEventRecipientsGetById(int $eventRecipientsId): RESTRequest
    {
        return $this->request('GET', sprintf('/event/recipients/%d/get', $eventRecipientsId));
    }
}