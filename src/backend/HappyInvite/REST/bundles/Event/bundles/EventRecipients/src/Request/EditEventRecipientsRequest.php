<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditEventRecipientsRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_EventRecipientsBundle_EditEventRecipientsRequest');
    }
}