<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command\CreateEventRecipientsCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command\DeleteEventRecipientsCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command\EditEventRecipientsCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command\GetEventRecipientsCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EventRecipientsMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('edit', EditEventRecipientsCommand::class)
            ->attachDirect('get', GetEventRecipientsCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}