<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Tests\Paths;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts\EventRecipientContact;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Tests\EventRecipientsMiddlewareTestCase;

final class EditEventRecipientsMiddlewareTest extends EventRecipientsMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'contacts' => [
                [
                    'sid' => GenerateRandomString::generate(SIDEntity::SID_LENGTH),
                    'email' => 'demo@example.com',
                    'first_name' => 'Foo',
                    'last_name' => 'Bar',
                    'middle_name' => 'Baz',
                ]
            ]
        ];
    }

    public function test200Admin()
    {
        $fixture = EventFixture::$eventRecipients1;

        $this->requestEventRecipientsEdit($fixture->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_recipients' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                        'contacts' => [
                            0 => [
                                'sid' => $json['contacts'][0]['sid'],
                                'email' => $json['contacts'][0]['email'],
                                'first_name' => $json['contacts'][0]['first_name'],
                                'last_name' => $json['contacts'][0]['last_name'],
                                'middle_name' => $json['contacts'][0]['middle_name'],
                            ]
                        ]
                    ]
                ]
            ]);
    }

    public function test200Owner()
    {
        $fixture = EventFixture::$eventRecipients1;

        $this->requestEventRecipientsEdit($fixture->getId(), $json = $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_recipients' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                        'contacts' => [
                            0 => [
                                'sid' => $json['contacts'][0]['sid'],
                                'email' => $json['contacts'][0]['email'],
                                'first_name' => $json['contacts'][0]['first_name'],
                                'last_name' => $json['contacts'][0]['last_name'],
                                'middle_name' => $json['contacts'][0]['middle_name'],
                            ]
                        ]
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $fixture = EventFixture::$eventRecipients1;

        $this->requestEventRecipientsEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventRecipientsEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventRecipientsEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}