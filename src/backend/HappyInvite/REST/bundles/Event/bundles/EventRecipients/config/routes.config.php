<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients;

use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\EventRecipientsMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'post',
                'path' => '/event/recipients/{eventRecipientsId}/{command:edit}[/]',
                'middleware' => EventRecipientsMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/recipients/{eventRecipientsId}/{command:get}[/]',
                'middleware' => EventRecipientsMiddleware::class,
            ],
        ]
    ]
];