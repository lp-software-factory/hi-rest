<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventRecipientsRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }
}