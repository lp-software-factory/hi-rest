<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\ParametersFactory;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Parameters\EditEventRecipientsParameters;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Request\EditEventRecipientsRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EventRecipientsParametersFactory
{
    public function factoryEditParameters(int $eventRecipientsId, ServerRequestInterface $request): EditEventRecipientsParameters
    {
        $json = (new EditEventRecipientsRequest($request))->getParameters();

        return new EditEventRecipientsParameters(
            $json['contacts']
        );
    }
}