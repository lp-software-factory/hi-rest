<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Exceptions\EventRecipientsNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEventRecipientsCommand extends AbstractEventRecipientsCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $eventRecipientsId = $request->getAttribute('eventRecipientsId');

        try {
            $entity = $this->service->getById($eventRecipientsId);

            if(! $this->accessService->isAdmin()) {
                $this->accessService->requireOwner($entity->getEvent()->getOwner()->getId());
            }

            $responseBuilder
                ->setJSON([
                    'event_recipients' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventRecipientsNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(EventIsNotYoursException|AccessDeniedException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }

        return $responseBuilder->build();
    }
}