<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests\EventMailingOptionsMiddlewareTestCase;

final class EditEventMailingOptionsMiddlewareTest extends EventMailingOptionsMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'config' => json_decode(file_get_contents(__DIR__.'/../Resources/sample-edit.json'), true),
        ];
    }

    public function test200Admin()
    {
        $fixture = EventFixture::$eventMailingOptions1;

        $this->requestEventMailingOptionsEdit($fixture->getId(), $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_mailing_options' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test200User()
    {
        $fixture = EventFixture::$eventMailingOptions1;

        $this->requestEventMailingOptionsEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_mailing_options' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $fixture = EventFixture::$eventMailingOptions1;

        $this->requestEventMailingOptionsEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventMailingOptionsEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventMailingOptionsEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}