<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserCFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Designer\Tests\Fixture\DesignersFixture;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture\DesignerRequestsFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture\EnvelopeMarksFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;
use HappyInvite\REST\Bundles\Solution\Tests\Fixture\SolutionFixture;

abstract class EventMailingOptionsMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new UserAFixture(),
            new UserBFixture(),
            new UserCFixture(),
            new AttachmentFixture(),
            new EventTypeGroupsFixture(),
            new EventTypesFixture(),
            new DesignerRequestsFixture(),
            new DesignersFixture(),
            new InviteCardColorFixture(true),
            new InviteCardSizeFixture(),
            new InviteCardStyleFixture(),
            new InviteCardGammaFixture(),
            new InviteCardFamilyFixture(),
            new InviteCardTemplateFixture(),
            new EnvelopeBackdropFixture(),
            new EnvelopeBackdropDefinitionFixture(),
            new EnvelopeColorFixture(),
            new EnvelopePatternFixture(),
            new EnvelopePatternDefinitionFixture(),
            new EnvelopeTextureFixture(),
            new EnvelopeTextureDefinitionFixture(),
            new EnvelopeMarksFixture(),
            new EnvelopeMarkDefinitionFixture(),
            new EnvelopeTemplateFixture(),
            new EventLandingTemplateFixture(),
            new SolutionFixture(),
            new EventFixture(),
        ];
    }
}