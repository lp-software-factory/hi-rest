<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Formatter\EventMailingOptionsFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service\EventMailingOptionsService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\ParametersFactory\EventMailingOptionsParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEventMailingOptionsCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var EventMailingOptionsParametersFactory */
    protected $parametersFactory;

    /** @var EventMailingOptionsFormatter */
    protected $formatter;

    /** @var EventMailingOptionsService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        EventMailingOptionsParametersFactory $parametersFactory,
        EventMailingOptionsFormatter $formatter,
        EventMailingOptionsService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}