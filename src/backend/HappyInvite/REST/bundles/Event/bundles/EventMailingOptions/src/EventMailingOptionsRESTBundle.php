<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventMailingOptionsRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}