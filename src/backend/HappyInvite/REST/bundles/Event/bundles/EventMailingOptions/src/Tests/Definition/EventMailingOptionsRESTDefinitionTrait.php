<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EventMailingOptionsRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEventMailingOptionsEdit(int $eventMailingOptionsId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/mailing/options/%d/edit', $eventMailingOptionsId))
            ->setParameters($json);
    }

    protected function requestEventMailingOptionsGetById(int $eventMailingOptionsId): RESTRequest
    {
        return $this->request('GET', sprintf('/event/mailing/options/%d/get', $eventMailingOptionsId));
    }
}