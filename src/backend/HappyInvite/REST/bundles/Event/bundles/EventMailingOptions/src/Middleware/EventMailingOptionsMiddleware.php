<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware\Command\EditEventMailingOptionsCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware\Command\GetEventMailingOptionsCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EventMailingOptionsMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('edit', EditEventMailingOptionsCommand::class)
            ->attachDirect('get', GetEventMailingOptionsCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}