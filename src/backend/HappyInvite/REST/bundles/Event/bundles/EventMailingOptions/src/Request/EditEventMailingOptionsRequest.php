<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditEventMailingOptionsRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_EventMailingOptionsBundle_EditEventMailingOptionsRequest');
    }
}