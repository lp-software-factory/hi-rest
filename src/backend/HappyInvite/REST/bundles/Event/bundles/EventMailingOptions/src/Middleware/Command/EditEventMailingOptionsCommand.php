<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Exceptions\EventMailingOptionsNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditEventMailingOptionsCommand extends AbstractEventMailingOptionsCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $eventMailingOptionsId = $request->getAttribute('eventMailingOptionsId');

        try {
            $parameters = $this->parametersFactory->factoryEditParameters($eventMailingOptionsId, $request);

            $entity = $this->service->editEventMailingOptions($eventMailingOptionsId, $parameters);

            $responseBuilder
                ->setJSON([
                    'event_mailing_options' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventMailingOptionsNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(EventIsNotYoursException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }

        return $responseBuilder->build();
    }
}