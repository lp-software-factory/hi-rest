<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\ParametersFactory;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\EventMailingOptionsConfigDataMapper;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Parameters\EditEventMailingOptionsParameters;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Request\EditEventMailingOptionsRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EventMailingOptionsParametersFactory
{
    /** @var EventMailingOptionsConfigDataMapper */
    private $dataMapper;

    public function __construct(EventMailingOptionsConfigDataMapper $dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    public function factoryEditParameters(int $eventMailingOptionsId, ServerRequestInterface $request): EditEventMailingOptionsParameters
    {
        $json = (new EditEventMailingOptionsRequest($request))->getParameters();

        return new EditEventMailingOptionsParameters(
            $this->dataMapper->createConfigFromArray($json['config'])
        );
    }
}