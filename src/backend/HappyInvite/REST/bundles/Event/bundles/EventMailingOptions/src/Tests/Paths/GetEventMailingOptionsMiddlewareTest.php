<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests\EventMailingOptionsMiddlewareTestCase;

final class GetEventMailingOptionsMiddlewareTest extends EventMailingOptionsMiddlewareTestCase
{
    public function test200Admin()
    {
        $fixture = EventFixture::$eventMailingOptions1;

        $this->requestEventMailingOptionsGetById($fixture->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event_mailing_options' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test200Owner()
    {
        $fixture = EventFixture::$eventMailingOptions1;

        $this->requestEventMailingOptionsGetById($fixture->getId())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event_mailing_options' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }


    public function test403()
    {
        $fixture = EventFixture::$eventMailingOptions1;

        $this->requestEventMailingOptionsGetById($fixture->getId())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventMailingOptionsGetById($fixture->getId())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventMailingOptionsGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}