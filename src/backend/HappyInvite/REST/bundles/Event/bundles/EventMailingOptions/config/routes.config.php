<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions;

use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware\EventMailingOptionsMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'post',
                'path' => '/event/mailing/options/{eventMailingOptionsId}/{command:edit}[/]',
                'middleware' => EventMailingOptionsMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/mailing/options/{eventMailingOptionsId}/{command:get}[/]',
                'middleware' => EventMailingOptionsMiddleware::class,
            ],
        ]
    ]
];