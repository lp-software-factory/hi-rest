<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Exceptions\EventMailingOptionsNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEventMailingOptionsCommand extends AbstractEventMailingOptionsCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $eventMailingOptionsId = $request->getAttribute('eventMailingOptionsId');

        try {
            $entity = $this->service->getById($eventMailingOptionsId);

            if(! $this->accessService->isAdmin()) {
                $this->accessService->requireOwner($entity->getEvent()->getOwner()->getId());
            }

            $responseBuilder
                ->setJSON([
                    'event_mailing_options' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventMailingOptionsNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(EventIsNotYoursException|AccessDeniedException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }

        return $responseBuilder->build();
    }
}