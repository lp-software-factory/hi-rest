<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\EventLandingTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;

final class DeleteEventLandingTemplateMiddlewareTest extends EventLandingTemplateMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $id = EventLandingTemplateFixture::$eventLandingTemplateEF1->getId();

        $this->requestEventLandingTemplateGetById($id)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEventLandingTemplateDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventLandingTemplateGetById($id)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $id = EventLandingTemplateFixture::$eventLandingTemplateEF1->getId();

        $this->requestEventLandingTemplateDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventLandingTemplateDelete($id)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $this->requestEventLandingTemplateDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}