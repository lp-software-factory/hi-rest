<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Exceptions\EventLandingTemplateNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEventLandingTemplateCommand extends AbstractEventLandingTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $eventLandingTemplateId = $request->getAttribute('eventLandingTemplateId');

        try {
            $template = $this->service->getById($eventLandingTemplateId);

            if(! $this->accessService->isAdmin()) {
                if($template->isSolutionUserSpecified() /* Don't! It's pretty strange behaviour if we hot this case: && $template->hasSolutionComponentOwner() */) {
                    $this->accessService->requireOwner($template->getSolutionComponentOwner()->getId());
                }else{
                    throw new AccessDeniedException(sprintf("You're not allowed to edit event template `%s`", $eventLandingTemplateId));
                }
            }

            $this->service->deleteEventLandingTemplate($eventLandingTemplateId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(EventLandingTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}