<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate;

use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\EventLandingTemplateMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/event/landing/template/{command:create}[/]',
                'middleware' => EventLandingTemplateMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/landing/template/{eventLandingTemplateId}/{command:edit}[/]',
                'middleware' => EventLandingTemplateMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/event/landing/template/{eventLandingTemplateId}/{command:delete}[/]',
                'middleware' => EventLandingTemplateMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/landing/template/{eventLandingTemplateId}/{command:get}[/]',
                'middleware' => EventLandingTemplateMiddleware::class,
            ],
        ]
    ]
];