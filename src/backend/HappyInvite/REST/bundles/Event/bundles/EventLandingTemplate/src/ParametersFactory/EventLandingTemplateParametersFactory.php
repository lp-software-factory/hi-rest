<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\ParametersFactory;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters\CreateEventLandingTemplateParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters\EditEventLandingTemplateParameters;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Request\CreateEventLandingTemplateRequest;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Request\EditEventLandingTemplateRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EventLandingTemplateParametersFactory
{
    public function factoryCreateParameters(ServerRequestInterface $request): CreateEventLandingTemplateParameters
    {
        $json = (new CreateEventLandingTemplateRequest($request))->getParameters();

        return new CreateEventLandingTemplateParameters(
            $json['json']
        );
    }

    public function factoryEditParameters(int $eventLandingTemplateId, ServerRequestInterface $request): EditEventLandingTemplateParameters
    {
        $json = (new EditEventLandingTemplateRequest($request))->getParameters();

        return new EditEventLandingTemplateParameters(
            $json['json']
        );
    }
}