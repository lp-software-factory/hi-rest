<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\EventLandingTemplateMiddlewareTestCase;

final class CreateEventLandingTemplateMiddlewareTest extends EventLandingTemplateMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'json' => ['version' => '1.0.0', 'foo' => 'bar', 'n' => 4]
        ];
    }

    public function test200()
    {
        $id = $this->requestEventLandingTemplateCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_landing_template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'json' => $json['json'],
                        'solution' => [
                            'is_user_specified' => false,
                            'owner' => [
                                'has' => false,
                            ]
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['event_landing_template']['entity']['id'];
            });

        $this->requestEventLandingTemplateGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test200UserSpecified()
    {
        $id = $this->requestEventLandingTemplateCreate($json = $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_landing_template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'json' => $json['json'],
                        'solution' => [
                            'is_user_specified' => true,
                            'owner' => [
                                'has' => true,
                                'profile' => [
                                    'id' => UserAccountFixture::getProfile()->getId(),
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['event_landing_template']['entity']['id'];
            });

        $this->requestEventLandingTemplateGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->requestEventLandingTemplateCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
    }
}