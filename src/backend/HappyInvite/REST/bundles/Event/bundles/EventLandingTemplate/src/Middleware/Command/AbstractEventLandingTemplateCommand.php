<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Formatter\EventLandingTemplateFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service\EventLandingTemplateService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\ParametersFactory\EventLandingTemplateParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEventLandingTemplateCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var EventLandingTemplateParametersFactory */
    protected $parametersFactory;

    /** @var EventLandingTemplateFormatter */
    protected $formatter;

    /** @var EventLandingTemplateService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        EventLandingTemplateParametersFactory $parametersFactory,
        EventLandingTemplateFormatter $formatter,
        EventLandingTemplateService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}