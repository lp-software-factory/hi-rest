<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventLandingTemplateRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }
}