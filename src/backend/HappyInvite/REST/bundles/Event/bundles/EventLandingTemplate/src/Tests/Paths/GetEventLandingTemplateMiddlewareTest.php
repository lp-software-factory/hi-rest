<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\EventLandingTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;

final class GetEventLandingTemplateMiddlewareTest extends EventLandingTemplateMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $fixture = EventLandingTemplateFixture::$eventLandingTemplateEF1;

        $this->requestEventLandingTemplateGetById($fixture->getId())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event_landing_template' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $this->requestEventLandingTemplateGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}