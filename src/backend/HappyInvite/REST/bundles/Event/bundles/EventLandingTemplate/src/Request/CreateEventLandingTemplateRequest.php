<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEventLandingTemplateRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_EventLandingTemplateBundle_CreateEventLandingTemplateRequest');
    }
}