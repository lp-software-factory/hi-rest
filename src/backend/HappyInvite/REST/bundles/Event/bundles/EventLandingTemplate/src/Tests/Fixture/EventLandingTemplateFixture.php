<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters\CreateEventLandingTemplateParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service\EventLandingTemplateService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EventLandingTemplateFixture implements Fixture
{
    /** @var EventLandingTemplate */
    public static $eventLandingTemplateEF1;

    /** @var EventLandingTemplate */
    public static $eventLandingTemplateEF2;

    /** @var EventLandingTemplate */
    public static $eventLandingTemplateEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EventLandingTemplateService::class); /** @var EventLandingTemplateService $service */

        self::$eventLandingTemplateEF1 = $service->createEventLandingTemplate(new CreateEventLandingTemplateParameters(
            ['version' => '1.0.0', 'foo' => 'bar', 'n' => 1]
        ));

        self::$eventLandingTemplateEF2 = $service->createEventLandingTemplate(new CreateEventLandingTemplateParameters(
            ['version' => '1.0.0', 'foo' => 'bar', 'n' => 2]
        ));

        self::$eventLandingTemplateEF3 = $service->createEventLandingTemplate(new CreateEventLandingTemplateParameters(
            ['version' => '1.0.0', 'foo' => 'bar', 'n' => 3]
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$eventLandingTemplateEF1,
            self::$eventLandingTemplateEF2,
            self::$eventLandingTemplateEF3,
        ];
    }
}