<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EventLandingTemplateRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEventLandingTemplateCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/event/landing/template/create')
            ->setParameters($json);
    }

    protected function requestEventLandingTemplateEdit(int $eventLandingTemplateId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/landing/template/%d/edit', $eventLandingTemplateId))
            ->setParameters($json);
    }

    protected function requestEventLandingTemplateDelete(int $eventLandingTemplateId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/event/landing/template/%d/delete', $eventLandingTemplateId));
    }

    protected function requestEventLandingTemplateGetById(int $eventLandingTemplateId): RESTRequest
    {
        return $this->request('GET', sprintf('/event/landing/template/%d/get', $eventLandingTemplateId));
    }
}