<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Exceptions\EventLandingTemplateNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEventLandingTemplateCommand extends AbstractEventLandingTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $eventLandingTemplateId = $request->getAttribute('eventLandingTemplateId');

        try {
            $entity = $this->service->getById($eventLandingTemplateId);

            $responseBuilder
                ->setJSON([
                    'event_landing_template' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventLandingTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}