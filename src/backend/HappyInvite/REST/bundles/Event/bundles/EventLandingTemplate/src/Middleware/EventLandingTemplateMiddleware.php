<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command\CreateEventLandingTemplateCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command\DeleteEventLandingTemplateCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command\EditEventLandingTemplateCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command\GetEventLandingTemplateCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EventLandingTemplateMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEventLandingTemplateCommand::class)
            ->attachDirect('edit', EditEventLandingTemplateCommand::class)
            ->attachDirect('delete', DeleteEventLandingTemplateCommand::class)
            ->attachDirect('get', GetEventLandingTemplateCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}