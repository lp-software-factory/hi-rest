<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEventLandingTemplateCommand extends AbstractEventLandingTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $parameters = $this->parametersFactory->factoryCreateParameters($request);

        $entity = $this->service->createEventLandingTemplate($parameters);

        if(! $this->accessService->isAdmin()) {
            $this->service->markAsUserSpecified($entity->getId(), $this->authToken->getProfile());
        }

        $responseBuilder
            ->setJSON([
                'event_landing_template' => $this->formatter->formatOne($entity)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}