<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\EventLandingTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;

final class EditEventLandingTemplateMiddlewareTest extends EventLandingTemplateMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'json' => ['version' => '1.0.0', 'foo' => 'bar*', 'n' => 99]
        ];
    }

    public function test200()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $fixture = EventLandingTemplateFixture::$eventLandingTemplateEF1;

        $this->requestEventLandingTemplateEdit($fixture->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_landing_template' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                        'json' => $json['json']
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $fixture = EventLandingTemplateFixture::$eventLandingTemplateEF1;

        $this->requestEventLandingTemplateEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventLandingTemplateEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventLandingTemplateFixture());

        $this->requestEventLandingTemplateEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}