<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters\CreateEventRecipientsGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Service\EventRecipientsGroupService;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EventRecipientsGroupFixture implements Fixture
{
    /** @var EventRecipientsGroup */
    public static $eventRecipientsGroupEF1;

    /** @var EventRecipientsGroup */
    public static $eventRecipientsGroupEF2;

    /** @var EventRecipientsGroup */
    public static $eventRecipientsGroupEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EventRecipientsGroupService::class); /** @var EventRecipientsGroupService $service */

        self::$eventRecipientsGroupEF1 = $service->create(new CreateEventRecipientsGroupParameters(
            UserAFixture::getProfile(),
            'Contact 1'
        ));

        self::$eventRecipientsGroupEF2 = $service->create(new CreateEventRecipientsGroupParameters(
            UserAFixture::getProfile(),
            'Contact 2'
        ));

        self::$eventRecipientsGroupEF3 = $service->create(new CreateEventRecipientsGroupParameters(
            UserAFixture::getProfile(),
            'Contact 3'
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$eventRecipientsGroupEF1,
            self::$eventRecipientsGroupEF2,
            self::$eventRecipientsGroupEF3,
        ];
    }
}