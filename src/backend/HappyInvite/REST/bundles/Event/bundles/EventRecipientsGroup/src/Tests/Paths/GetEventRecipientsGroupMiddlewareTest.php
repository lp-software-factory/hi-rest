<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\EventRecipientsGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;

final class GetEventRecipientsGroupMiddlewareTest extends EventRecipientsGroupMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $fixture = EventRecipientsGroupFixture::$eventRecipientsGroupEF1;

        $this->requestEventRecipientsGroupGetById($fixture->getId())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event_recipients_group' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $this->requestEventRecipientsGroupGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}