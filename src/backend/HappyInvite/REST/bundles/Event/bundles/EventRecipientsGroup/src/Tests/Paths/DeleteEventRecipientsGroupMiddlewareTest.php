<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\EventRecipientsGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;

final class DeleteEventRecipientsGroupMiddlewareTest extends EventRecipientsGroupMiddlewareTestCase
{
    public function test200Admin()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $id = EventRecipientsGroupFixture::$eventRecipientsGroupEF1->getId();

        $this->requestEventRecipientsGroupGetById($id)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEventRecipientsGroupDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventRecipientsGroupGetById($id)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200Owner()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $id = EventRecipientsGroupFixture::$eventRecipientsGroupEF1->getId();

        $this->requestEventRecipientsGroupGetById($id)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEventRecipientsGroupDelete($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventRecipientsGroupGetById($id)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $id = EventRecipientsGroupFixture::$eventRecipientsGroupEF1->getId();

        $this->requestEventRecipientsGroupDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventRecipientsGroupDelete($id)
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $this->requestEventRecipientsGroupDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}