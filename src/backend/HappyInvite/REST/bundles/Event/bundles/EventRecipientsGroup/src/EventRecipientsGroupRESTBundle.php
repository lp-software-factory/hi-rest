<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventRecipientsGroupRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}