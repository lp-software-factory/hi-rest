<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\EventRecipientsGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;

final class ListRecipientsGroupMiddlewareTest extends EventRecipientsGroupMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $this->requestEventRecipientsList()
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $this->requestEventRecipientsList(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectAuthError();
    }
}