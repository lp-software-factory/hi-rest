<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\EventRecipientsGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;

final class SetTitleEventRecipientsGroupMiddlewareTest extends EventRecipientsGroupMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'title' => 'Demo *'
        ];
    }

    public function test200Admin()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $fixture = EventRecipientsGroupFixture::$eventRecipientsGroupEF1;

        $this->requestEventRecipientsGroupSetTitle($fixture->getId(), $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_recipients_group' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test200Owner()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $fixture = EventRecipientsGroupFixture::$eventRecipientsGroupEF1;

        $this->requestEventRecipientsGroupSetTitle($fixture->getId(), $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_recipients_group' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $fixture = EventRecipientsGroupFixture::$eventRecipientsGroupEF1;

        $this->requestEventRecipientsGroupSetTitle($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventRecipientsGroupSetTitle($fixture->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventRecipientsGroupFixture());

        $this->requestEventRecipientsGroupSetTitle(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}