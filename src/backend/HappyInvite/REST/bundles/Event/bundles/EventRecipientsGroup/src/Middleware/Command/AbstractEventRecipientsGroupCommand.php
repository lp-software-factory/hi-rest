<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Formatter\EventRecipientsGroupFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Service\EventRecipientsGroupService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\ParametersFactory\EventRecipientsGroupParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEventRecipientsGroupCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var EventRecipientsGroupParametersFactory */
    protected $parametersFactory;

    /** @var EventRecipientsGroupFormatter */
    protected $formatter;

    /** @var EventRecipientsGroupService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        EventRecipientsGroupParametersFactory $parametersFactory,
        EventRecipientsGroupFormatter $formatter,
        EventRecipientsGroupService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}