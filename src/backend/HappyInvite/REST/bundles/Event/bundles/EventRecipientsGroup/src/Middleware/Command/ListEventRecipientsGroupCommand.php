<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListEventRecipientsGroupCommand extends AbstractEventRecipientsGroupCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $entities = $this->service->getAllByOwner($this->authToken->getProfile());

        $responseBuilder
            ->setJSON([
                'event_recipients_groups' => $this->formatter->formatMany($entities)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}