<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEventRecipientsGroupCommand extends AbstractEventRecipientsGroupCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $parameters = $this->parametersFactory->factoryCreateParameters($request);

        $entity = $this->service->create($parameters);

        $responseBuilder
            ->setJSON([
                'event_recipients_group' => $this->formatter->formatOne($entity)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}