<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupIsNotYoursException;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command\CreateEventRecipientsGroupCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command\DeleteEventRecipientsGroupCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command\ListEventRecipientsGroupCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command\SetTitleEventRecipientsGroupCommand;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command\GetEventRecipientsGroupCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EventRecipientsGroupMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEventRecipientsGroupCommand::class)
            ->attachDirect('set-title', SetTitleEventRecipientsGroupCommand::class)
            ->attachDirect('delete', DeleteEventRecipientsGroupCommand::class)
            ->attachDirect('get', GetEventRecipientsGroupCommand::class)
            ->attachDirect('list', ListEventRecipientsGroupCommand::class)
            ->resolve($request);

        try {
            return $resolver->__invoke($request, $responseBuilder);
        }catch(EventRecipientsGroupIsNotYoursException $e) {
            return $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed()
                ->build();
        }
    }
}