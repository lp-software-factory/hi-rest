<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup;

use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\EventRecipientsGroupMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/event/recipients/group/{command:create}[/]',
                'middleware' => EventRecipientsGroupMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/recipients/group/{eventRecipientsGroupId}/{command:set-title}[/]',
                'middleware' => EventRecipientsGroupMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/event/recipients/group/{eventRecipientsGroupId}/{command:delete}[/]',
                'middleware' => EventRecipientsGroupMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/recipients/group/{eventRecipientsGroupId}/{command:get}[/]',
                'middleware' => EventRecipientsGroupMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/recipients/group/{command:list}[/]',
                'middleware' => EventRecipientsGroupMiddleware::class,
            ],
        ]
    ]
];