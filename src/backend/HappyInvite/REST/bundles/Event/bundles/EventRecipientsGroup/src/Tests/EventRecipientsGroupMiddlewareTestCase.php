<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserCFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class EventRecipientsGroupMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
            new UserAccountFixture(),
            new UserAFixture(),
            new UserBFixture(),
            new UserCFixture(),
        ];
    }

    protected function requestEventRecipientsGroupCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/event/recipients/group/create')
            ->setParameters($json);
    }

    protected function requestEventRecipientsGroupSetTitle(int $eventRecipientsGroupId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/recipients/group/%d/set-title', $eventRecipientsGroupId))
            ->setParameters($json);
    }

    protected function requestEventRecipientsGroupDelete(int $eventRecipientsGroupId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/event/recipients/group/%d/delete', $eventRecipientsGroupId));
    }

    protected function requestEventRecipientsGroupGetById(int $eventRecipientsGroupId): RESTRequest
    {
        return $this->request('GET', sprintf('/event/recipients/group/%d/get', $eventRecipientsGroupId));
    }

    protected function requestEventRecipientsList(): RESTRequest
    {
        return $this->request('GET', sprintf('/event/recipients/group/list'));
    }
}