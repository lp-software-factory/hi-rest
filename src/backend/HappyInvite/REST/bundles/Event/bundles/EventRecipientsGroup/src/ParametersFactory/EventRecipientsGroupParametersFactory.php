<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\ParametersFactory;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters\CreateEventRecipientsGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters\SetTitleEventRecipientsGroupParameters;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Request\CreateEventRecipientsGroupRequest;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Request\EditEventRecipientsGroupRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EventRecipientsGroupParametersFactory
{
    /** @var AuthToken */
    private $authToken;

    public function __construct(AuthToken $authToken)
    {
        $this->authToken = $authToken;
    }

    public function factoryCreateParameters(ServerRequestInterface $request): CreateEventRecipientsGroupParameters
    {
        $json = (new CreateEventRecipientsGroupRequest($request))->getParameters();

        return new CreateEventRecipientsGroupParameters(
            $this->authToken->getProfile(), $json['title']
        );
    }

    public function factorySetTitleParameters(int $eventRecipientsGroupId, ServerRequestInterface $request): SetTitleEventRecipientsGroupParameters
    {
        $json = (new EditEventRecipientsGroupRequest($request))->getParameters();

        return new SetTitleEventRecipientsGroupParameters($json['title']);
    }
}