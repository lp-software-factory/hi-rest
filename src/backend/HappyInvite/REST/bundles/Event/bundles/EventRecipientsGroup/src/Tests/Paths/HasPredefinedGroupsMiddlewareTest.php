<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Paths;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Subscriptions\DefaultEventRecipientsEventScript;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\EventRecipientsGroupMiddlewareTestCase;

final class HasPredefinedGroupsMiddlewareTest extends EventRecipientsGroupMiddlewareTestCase
{
    public function testHasPredefinedGroups()
    {
        $actual = $this->requestEventRecipientsList()
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['title'];
                }, $json['event_recipients_groups']);
            });

        $this->assertEquals(DefaultEventRecipientsEventScript::PREDEFINED_GROUPS, $actual);
    }
}