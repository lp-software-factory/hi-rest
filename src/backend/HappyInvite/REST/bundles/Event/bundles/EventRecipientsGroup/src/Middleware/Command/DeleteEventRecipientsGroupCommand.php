<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEventRecipientsGroupCommand extends AbstractEventRecipientsGroupCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $eventRecipientsGroupId = $request->getAttribute('eventRecipientsGroupId');

        try {
            $this->service->delete($eventRecipientsGroupId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(EventRecipientsGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}