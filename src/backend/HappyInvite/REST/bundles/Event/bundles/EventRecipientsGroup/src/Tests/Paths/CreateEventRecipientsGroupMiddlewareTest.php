<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\EventRecipientsGroupMiddlewareTestCase;

final class CreateEventRecipientsGroupMiddlewareTest extends EventRecipientsGroupMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'title' => 'Demo'
        ];
    }

    public function test200Admin()
    {
        $id = $this->requestEventRecipientsGroupCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_recipients_group' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'position' => 1,
                        'owner_profile_id' => AdminAccountFixture::getProfile()->getId(),
                        'title' => $json['title'],
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['event_recipients_group']['entity']['id'];
            });

        $this->requestEventRecipientsGroupGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test200User()
    {
        $id = $this->requestEventRecipientsGroupCreate($json = $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_recipients_group' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'position' => 1,
                        'owner_profile_id' => UserAFixture::getProfile()->getId(),
                        'title' => $json['title'],
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['event_recipients_group']['entity']['id'];
            });

        $this->requestEventRecipientsGroupGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->requestEventRecipientsGroupCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
    }

}