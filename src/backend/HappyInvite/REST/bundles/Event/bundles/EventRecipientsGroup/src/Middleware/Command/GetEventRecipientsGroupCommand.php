<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEventRecipientsGroupCommand extends AbstractEventRecipientsGroupCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $eventRecipientsGroupId = $request->getAttribute('eventRecipientsGroupId');

        try {
            $entity = $this->service->getById($eventRecipientsGroupId);

            $responseBuilder
                ->setJSON([
                    'event_recipients_group' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventRecipientsGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}