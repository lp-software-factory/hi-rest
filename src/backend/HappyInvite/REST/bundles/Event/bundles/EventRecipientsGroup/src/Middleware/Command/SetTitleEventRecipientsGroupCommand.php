<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetTitleEventRecipientsGroupCommand extends AbstractEventRecipientsGroupCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $eventRecipientsGroupId = $request->getAttribute('eventRecipientsGroupId');

        try {
            $parameters = $this->parametersFactory->factorySetTitleParameters($eventRecipientsGroupId, $request);

            $entity = $this->service->setTitle($eventRecipientsGroupId, $parameters);

            $responseBuilder
                ->setJSON([
                    'event_recipients_group' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventRecipientsGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}