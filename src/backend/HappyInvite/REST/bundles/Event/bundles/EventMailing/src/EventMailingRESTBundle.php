<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\EventMailing;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventMailingRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}