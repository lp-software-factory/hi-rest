<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\CreateEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Entity\EventMailingOptions;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service\EventMailingOptionsService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Entity\EventRecipients;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserCFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use HappyInvite\REST\Bundles\Solution\Tests\Fixture\SolutionFixture;
use Zend\Expressive\Application;

final class EventFixture implements Fixture
{
    /** @var Event */
    public static $event1;

    /** @var Event */
    public static $event2;

    /** @var Event */
    public static $event3;

    /** @var EventRecipients */
    public static $eventRecipients1;

    /** @var EventRecipients */
    public static $eventRecipients2;

    /** @var EventRecipients */
    public static $eventRecipients3;

    /** @var EventMailingOptions */
    public static $eventMailingOptions1;

    /** @var EventMailingOptions */
    public static $eventMailingOptions2;

    /** @var EventMailingOptions */
    public static $eventMailingOptions3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EventService::class); /** @var EventService $service */
        $eventRecipientsService = $app->getContainer()->get(EventRecipientsService::class); /** @var EventRecipientsService $eventRecipientsService */
        $eventMailingOptionsService = $app->getContainer()->get(EventMailingOptionsService::class); /** @var EventMailingOptionsService $eventMailingOptionsService */

        self::$event1 = $service->createEvent(new CreateEventParameters(
            UserAFixture::getProfile(),
            SolutionFixture::$solutionEF1
        ));

        self::$event2 = $service->createEvent(new CreateEventParameters(
            UserBFixture::getProfile(),
            SolutionFixture::$solutionEF2
        ));

        self::$event3 = $service->createEvent(new CreateEventParameters(
            UserCFixture::getProfile(),
            SolutionFixture::$solutionEF3
        ));

        self::$eventRecipients1 = $eventRecipientsService->getByEventId(self::$event1->getId());
        self::$eventRecipients2 = $eventRecipientsService->getByEventId(self::$event2->getId());
        self::$eventRecipients3 = $eventRecipientsService->getByEventId(self::$event3->getId());

        self::$eventMailingOptions1 = $eventMailingOptionsService->getByEventId(self::$event1->getId());
        self::$eventMailingOptions2 = $eventMailingOptionsService->getByEventId(self::$event2->getId());
        self::$eventMailingOptions3 = $eventMailingOptionsService->getByEventId(self::$event3->getId());
    }

    public static function getOrderedList(): array
    {
        return [
            self::$event1,
            self::$event2,
            self::$event3,
        ];
    }
}