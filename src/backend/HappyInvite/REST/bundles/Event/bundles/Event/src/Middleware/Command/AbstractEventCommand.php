<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Formatter\EventFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Event\Bundles\Event\ParametersFactory\EventParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEventCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var EventParametersFactory */
    protected $parametersFactory;

    /** @var EventFormatter */
    protected $formatter;

    /** @var EventService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        EventParametersFactory $parametersFactory,
        EventFormatter $formatter,
        EventService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}