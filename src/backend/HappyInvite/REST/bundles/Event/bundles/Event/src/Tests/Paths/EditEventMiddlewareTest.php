<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\EventMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;

final class EditEventMiddlewareTest extends EventMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'title' => 'My Event'
        ];
    }

    public function test200User()
    {
        $this->upFixture(new EventFixture());

        $event = EventFixture::$event1;

        $this->requestEventEdit($event->getId(), $json = $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event' => [
                    'entity' => [
                        'id' => $event->getId(),
                        'title' => $json['title'],
                    ]
                ]
            ]);
    }

    public function test200Admin()
    {
        $this->upFixture(new EventFixture());

        $event = EventFixture::$event1;

        $this->requestEventEdit($event->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event' => [
                    'entity' => [
                        'id' => $event->getId(),
                        'title' => $json['title'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EventFixture());

        $fixture = EventFixture::$event1;

        $this->requestEventEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventFixture());

        $this->requestEventEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEventEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}