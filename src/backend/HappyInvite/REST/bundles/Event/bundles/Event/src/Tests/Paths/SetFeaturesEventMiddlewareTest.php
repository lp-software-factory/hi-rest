<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\EventMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;

final class SetFeaturesEventMiddlewareTest extends EventMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return ['envelope' => true, 'landing' => true];
    }

    private function getSets(): array
    {
        return [
            [
                'envelope' => false,
                'landing' => false,
            ],
            [
                'envelope' => true,
                'landing' => false,
            ],
            [
                'envelope' => false,
                'landing' => true,
            ],
            [
                'envelope' => true,
                'landing' => true,
            ],
            [
                'envelope' => false,
                'landing' => false,
            ],            [
                'envelope' => true,
                'landing' => true,
            ],
        ];
    }

    public function test200User()
    {

        $this->upFixture(new EventFixture());

        $event = EventFixture::$event1;

        foreach($this->getSets() as $json) {
            $this->requestEventSetFeatures($event->getId(), $json)
                ->auth(UserAFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(200)
                ->expectJSONContentType()
                ->expectJSONBody([
                    'success' => true,
                    'event' => [
                        'entity' => [
                            'id' => $event->getId(),
                            'event_features' => [
                                'envelope' => $json['envelope'],
                                'landing' => $json['landing'],
                            ],
                        ]
                    ]
                ]);
        }
    }

    public function test200Admin()
    {
        $this->upFixture(new EventFixture());

        $event = EventFixture::$event1;

        foreach($this->getSets() as $json) {
            $this->requestEventSetFeatures($event->getId(), $json)
                ->auth(AdminAccountFixture::getJWT())
                ->__invoke()
                ->expectStatusCode(200)
                ->expectJSONContentType()
                ->expectJSONBody([
                    'success' => true,
                    'event' => [
                        'entity' => [
                            'id' => $event->getId(),
                            'event_features' => [
                                'envelope' => $json['envelope'],
                                'landing' => $json['landing'],
                            ],
                        ]
                    ]
                ]);
        }
    }

    public function test403()
    {
        $this->upFixture(new EventFixture());

        $fixture = EventFixture::$event1;

        $this->requestEventSetFeatures($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventSetFeatures($fixture->getId(), $this->getTestJSON())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventFixture());

        $this->requestEventSetFeatures(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEventSetFeatures(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}