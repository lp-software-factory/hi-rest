<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\EventMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;

final class GetEventMiddlewareTest extends EventMiddlewareTestCase
{
    public function test200User()
    {
        $this->upFixture(new EventFixture());

        $fixture = EventFixture::$event1;

        $this->requestEventGetById($fixture->getId())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test200Admin()
    {
        $this->upFixture(new EventFixture());

        $fixture = EventFixture::$event1;

        $this->requestEventGetById($fixture->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'event' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EventFixture());

        $fixture = EventFixture::$event1;

        $this->requestEventGetById($fixture->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventGetById($fixture->getId())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventFixture());

        $this->requestEventGetById(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEventGetById(self::NOT_FOUND_ID)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}