<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EventRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEventCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/event/create')
            ->setParameters($json);
    }

    protected function requestEventEdit(int $eventId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/%d/edit', $eventId))
            ->setParameters($json);
    }

    protected function requestEventSubmit(int $eventId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/%d/submit', $eventId))
            ->setParameters($json);
    }

    protected function requestEventTest(int $eventId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/%d/test', $eventId))
            ->setParameters($json);
    }

    protected function requestEventSetTitle(int $eventId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/%d/set-title', $eventId))
            ->setParameters($json);
    }

    protected function requestEventSetFeatures(int $eventId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/event/%d/set-features', $eventId))
            ->setParameters($json);
    }

    protected function requestEventDelete(int $eventId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/event/%d/delete', $eventId));
    }

    protected function requestEventGetById(int $eventId): RESTRequest
    {
        return $this->request('GET', sprintf('/event/%d/get', $eventId));
    }

    protected function requestEventList(): RESTRequest
    {
        return $this->request('GET', sprintf('/event/list'));
    }
}