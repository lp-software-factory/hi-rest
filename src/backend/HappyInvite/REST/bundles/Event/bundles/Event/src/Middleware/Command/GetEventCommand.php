<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEventCommand extends AbstractEventCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $eventId = $request->getAttribute('eventId');

        try {
            $event = $this->service->getById($eventId);

            if(! $this->accessService->isAdmin()) {
                $this->accessService->requireOwner($event->getOwner()->getId());
            }

            $responseBuilder
                ->setJSON([
                    'event' => $this->formatter->formatOne($event)
                ])
                ->setStatusSuccess();
        }catch(EventNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}