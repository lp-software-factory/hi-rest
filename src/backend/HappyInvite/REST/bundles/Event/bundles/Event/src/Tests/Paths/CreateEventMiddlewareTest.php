<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\EventMiddlewareTestCase;
use HappyInvite\REST\Bundles\Solution\Tests\Fixture\SolutionFixture;

final class CreateEventMiddlewareTest extends EventMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'solution_id' => SolutionFixture::$solutionEF1->getId(),
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $id = $this->requestEventCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'owner' => [
                            'id' => UserAccountFixture::getProfile()->getId(),
                        ],
                        'event_mailing_options' => [
                            'entity' => [
                                'id' => $this->expectId(),
                            ]
                        ],
                        'event_recipients' => [
                            'entity' => [
                                'id' => $this->expectId(),
                            ]
                        ],
                        'is_submit' => false,
                        'date_submit' => $this->expectUndefined(),
                        'solution' => [
                            'original' => [
                                'entity' => [
                                    'id' => $json['solution_id'],
                                    'target' => [
                                        'type' => 'global',
                                        'profile' => $this->expectUndefined(),
                                    ],
                                    'invite_card_templates' => [
                                        0 => [
                                            'entity' => [
                                                'solution' => [
                                                    'is_user_specified' => false,
                                                    'owner' => [
                                                        'has' => false,
                                                        'profile' => $this->expectUndefined(),
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    'envelope_templates' => [
                                        0 => [
                                            'entity' => [
                                                'solution' => [
                                                    'is_user_specified' => false,
                                                    'owner' => [
                                                        'has' => false,
                                                        'profile' => $this->expectUndefined(),
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    'landing_templates' => [
                                        0 => [
                                            'entity' => [
                                                'solution' => [
                                                    'is_user_specified' => false,
                                                    'owner' => [
                                                        'has' => false,
                                                        'profile' => $this->expectUndefined(),
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                ]
                            ],
                            'current' => [
                                'entity' => [
                                    'id' => $this->expectId(),
                                    'target' => [
                                        'type' => 'profile',
                                        'profile' => [
                                            'id' => UserAccountFixture::getProfile()->getId(),
                                        ]
                                    ],
                                    'invite_card_templates' => [
                                        0 => [
                                            'entity' => [
                                                'solution' => [
                                                    'is_user_specified' => false,
                                                    'owner' => [
                                                        'has' => true,
                                                        'profile' => [
                                                            'id' => UserAccountFixture::getProfile()->getId(),
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    'envelope_templates' => [
                                        0 => [
                                            'entity' => [
                                                'solution' => [
                                                    'is_user_specified' => false,
                                                    'owner' => [
                                                        'has' => true,
                                                        'profile' => [
                                                            'id' => UserAccountFixture::getProfile()->getId(),
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                    'landing_templates' => [
                                        0 => [
                                            'entity' => [
                                                'solution' => [
                                                    'is_user_specified' => false,
                                                    'owner' => [
                                                        'has' => true,
                                                        'profile' => [
                                                            'id' => UserAccountFixture::getProfile()->getId(),
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->expect(function($input) use ($json) {
                $this->assertNotEquals($json['solution_id'], $input['event']['entity']['solution']['current']['entity']['id']);

                $this->assertNotEquals(
                    $input['event']['entity']['solution']['current']['entity']['id'],
                    $input['event']['entity']['solution']['original']['entity']['id']
                );

                $this->assertNotEquals(
                    $input['event']['entity']['solution']['current']['entity']['invite_card_templates'][0]['entity']['id'],
                    $input['event']['entity']['solution']['original']['entity']['invite_card_templates'][0]['entity']['id']
                );
            })
            ->fetch(function (array $json) {
                return $json['event']['entity']['id'];
            });

        $this->requestEventGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test200DoNotCreateDuplicatesOfTemplatesInFamily()
    {
        $json = $this->getTestJSON();

        $result = $this->requestEventCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return [
                    'familyId' => $json['event']['entity']['solution']['original']['entity']['invite_card_templates'][0]['entity']['family_id'],
                    'original' => $json['event']['entity']['solution']['original']['entity']['invite_card_templates'][0]['entity']['id'],
                    'current' => $json['event']['entity']['solution']['current']['entity']['invite_card_templates'][0]['entity']['id'],
                ];
            })
        ;

        $existedTemplateIdsInFamily = $this->getTemplateIdsOfFamily($result['familyId']);

        $this->assertTrue(in_array($result['original'], $existedTemplateIdsInFamily, true));
        $this->assertFalse(in_array($result['current'], $existedTemplateIdsInFamily, true));
    }

    public function test403()
    {
        $this->requestEventCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $json = [
            'solution_id' => self::NOT_FOUND_ID,
        ];

        $id = $this->requestEventCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}