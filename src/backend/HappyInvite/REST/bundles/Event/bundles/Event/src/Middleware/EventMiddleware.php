<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\CreateEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\DeleteEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\EditEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\GetEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\ListEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\SetFeaturesEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\SetTitleEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\SubmitEventCommand;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command\TestEventCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EventMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEventCommand::class)
            ->attachDirect('edit', EditEventCommand::class)
            ->attachDirect('submit', SubmitEventCommand::class)
            ->attachDirect('test', TestEventCommand::class)
            ->attachDirect('set-title', SetTitleEventCommand::class)
            ->attachDirect('set-features', SetFeaturesEventCommand::class)
            ->attachDirect('delete', DeleteEventCommand::class)
            ->attachDirect('get', GetEventCommand::class)
            ->attachDirect('list', ListEventCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}