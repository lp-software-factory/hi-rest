<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListEventCommand extends AbstractEventCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->accessService->requireProtectedAccess();

            $events = $this->service->getByOwner($this->authToken->getProfile());

            $responseBuilder
                ->setJSON([
                    'events' => $this->formatter->formatList($events),
                ])
                ->setStatusSuccess();
        }catch(EventNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}