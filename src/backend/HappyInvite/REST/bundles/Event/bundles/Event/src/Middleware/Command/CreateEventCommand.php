<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command;

use HappyInvite\Domain\Bundles\Solution\Exceptions\SolutionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEventCommand extends AbstractEventCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            $parameters = $this->parametersFactory->factoryCreateParameters($request);

            $entity = $this->service->createEvent($parameters);

            $responseBuilder
                ->setJSON([
                    'event' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(SolutionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}