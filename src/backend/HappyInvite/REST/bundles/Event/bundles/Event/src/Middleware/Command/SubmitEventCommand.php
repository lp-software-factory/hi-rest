<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SubmitEventCommand extends AbstractEventCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $eventId = $request->getAttribute('eventId');

        try {
            $event = $this->service->getById($eventId);

            if(! $this->accessService->isAdmin()) {
                $this->accessService->requireOwner($event->getOwner()->getId());
            }

            $parameters = $this->parametersFactory->factorySubmitParameters($eventId, $request);

            $entity = $this->service->submitEvent($eventId, $parameters);

            $responseBuilder
                ->setJSON([
                    'event' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EventNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}