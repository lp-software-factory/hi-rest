<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\EventMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Fixture\EventFixture;

final class DeleteEventMiddlewareTest extends EventMiddlewareTestCase
{
    public function test200Admin()
    {
        $this->upFixture(new EventFixture());

        $id = EventFixture::$event1->getId();

        $this->requestEventGetById($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEventDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventGetById($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200User()
    {
        $this->upFixture(new EventFixture());

        $id = EventFixture::$event1->getId();

        $this->requestEventGetById($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEventDelete($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventGetById($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EventFixture());

        $id = EventFixture::$event1->getId();

        $this->requestEventDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventDelete($id)
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventFixture());

        $this->requestEventDelete(self::NOT_FOUND_ID)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEventDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}