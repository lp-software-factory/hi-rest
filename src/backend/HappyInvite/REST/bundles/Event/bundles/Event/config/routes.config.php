<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event;

use HappyInvite\REST\Bundles\Event\Bundles\Event\Middleware\EventMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/event/{command:create}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/{eventId}/{command:edit}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/{eventId}/{command:submit}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/{eventId}/{command:test}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/{eventId}/{command:set-title}[/]',
                'middleware' => EventMiddleware::class,
            ],

            [
                'method' => 'post',
                'path' => '/event/{eventId}/{command:set-features}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/event/{eventId}/{command:delete}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/{eventId}/{command:get}[/]',
                'middleware' => EventMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/{command:list}[/]',
                'middleware' => EventMiddleware::class,
            ],
        ]
    ]
];