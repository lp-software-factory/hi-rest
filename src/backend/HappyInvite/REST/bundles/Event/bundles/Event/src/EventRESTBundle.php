<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }
}