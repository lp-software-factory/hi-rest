<?php
namespace HappyInvite\REST\Bundles\Event\Bundles\Event\ParametersFactory;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\CreateEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\EditEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\SubmitEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\TestEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Type\EventFeatures;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Request\CreateEventRequest;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Request\EditEventRequest;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Request\SetFeaturesEventRequest;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Request\SubmitEventRequest;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Request\TestEventRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EventParametersFactory
{
    /** @var AuthToken */
    private $authToken;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(AuthToken $authToken, SolutionService $solutionService)
    {
        $this->authToken = $authToken;
        $this->solutionService = $solutionService;
    }

    public function factoryCreateParameters(ServerRequestInterface $request): CreateEventParameters
    {
        $json = (new CreateEventRequest($request))->getParameters();

        return new CreateEventParameters($this->authToken->getProfile(), $this->solutionService->getById($json['solution_id']));
    }

    public function factoryEditParameters(int $eventId, ServerRequestInterface $request): EditEventParameters
    {
        $json = (new EditEventRequest($request))->getParameters();

        return new EditEventParameters($json['title']);
    }

    public function factorySubmitParameters(int $eventId, ServerRequestInterface $request): SubmitEventParameters
    {
        $json = (new SubmitEventRequest($request))->getParameters();

        return new SubmitEventParameters();
    }

    public function factoryTestParameters(int $eventId, ServerRequestInterface $request): TestEventParameters
    {
        $json = (new TestEventRequest($request))->getParameters();

        return new TestEventParameters();
    }

    public function factorySetFeatures(int $eventId, ServerRequestInterface $request): EventFeatures
    {
        $json = (new SetFeaturesEventRequest($request))->getParameters();

        return new EventFeatures([
            'envelope' => $json['envelope'],
            'landing' => $json['landing']
        ]);
    }
}