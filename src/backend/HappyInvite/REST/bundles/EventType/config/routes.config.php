<?php
namespace HappyInvite\REST\Bundles\EventType;

use HappyInvite\REST\Bundles\EventType\Middleware\EventTypeMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/event/type/entity/{command:create}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/type/entity/{id}/{command:get}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/type/entity/{command:all}/of-group/{groupId}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/type/entity/{id}/{command:edit}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/event/type/entity/{id}/{command:delete}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/type/entity/{id}/move/{command:up}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/type/entity/{id}/move/{command:down}[/]',
                'middleware' => EventTypeMiddleware::class,
            ],
        ],
    ],
];