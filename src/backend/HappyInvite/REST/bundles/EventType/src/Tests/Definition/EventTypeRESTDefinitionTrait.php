<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EventTypeRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEventTypeCreate(array $json): RESTRequest
    {
        return $this->request('put', '/event/type/entity/create')
            ->setParameters($json);
    }

    protected function requestEventTypeEdit(int $id, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/event/type/entity/%d/edit', $id))
            ->setParameters($json);
    }

    protected function requestEventTypeGet(int $id): RESTRequest
    {
        return $this->request('get', sprintf('/event/type/entity/%d/get', $id));
    }

    protected function requestEventTypeGetAll(int $groupId): RESTRequest
    {
        return $this->request('get', sprintf('/event/type/entity/all/of-group/%d', $groupId));
    }

    protected function requestEventTypeDelete(int $id): RESTRequest
    {
        return $this->request('delete', sprintf('/event/type/entity/%d/delete', $id));
    }

    protected function requestEventTypeMoveUp(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/event/type/entity/%d/move/up', $id));
    }

    protected function requestEventTypeMoveDown(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/event/type/entity/%d/move/down', $id));
    }
}