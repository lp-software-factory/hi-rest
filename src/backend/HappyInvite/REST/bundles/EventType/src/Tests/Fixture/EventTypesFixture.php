<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Fixture;

use Cocur\Chain\Chain;
use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Parameters\CreateEventTypeParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EventTypesFixture implements Fixture
{
    private static $etWedding1;
    private static $etWedding2;
    private static $etBirthday1;
    private static $etBirthday2;
    private static $etBirthday3;
    private static $etBusiness1;

    public function up(Application $app, EntityManager $em)
    {
        /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService $service */
        $service = $app->getContainer()->get(EventTypeService::class);

        self::$etWedding1 = $service->createEventType(new CreateEventTypeParameters(
            'wedding_et1',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Wedding ET 1']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Wedding ET 1 Description']]),
            EventTypeGroupsFixture::getETGWedding()->getId()
        ));

        self::$etWedding2 = $service->createEventType(new CreateEventTypeParameters(
            'wedding_et2',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Wedding ET 2']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Wedding ET 2 Description']]),
            EventTypeGroupsFixture::getETGWedding()->getId()
        ));

        self::$etBirthday1 = $service->createEventType(new CreateEventTypeParameters(
            'birthday_et1',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday ET 1']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday ET 1 Description']]),
            EventTypeGroupsFixture::getETGBirthday()->getId()
        ));

        self::$etBirthday2 = $service->createEventType(new CreateEventTypeParameters(
            'birthday_et2',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday ET 2']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday ET 2 Description']]),
            EventTypeGroupsFixture::getETGBirthday()->getId()
        ));

        self::$etBirthday3 = $service->createEventType(new CreateEventTypeParameters(
            'birthday_et3',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday ET 3']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday ET 3 Description']]),
            EventTypeGroupsFixture::getETGBirthday()->getId()
        ));

        self::$etBusiness1 = $service->createEventType(new CreateEventTypeParameters(
            'business_et1',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Business ET 1']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Business ET 1 Description']]),
            EventTypeGroupsFixture::getETGBusiness()->getId()
        ));
    }

    public static function getETWedding1(): EventType
    {
        return self::$etWedding1;
    }

    public static function getETWedding2(): EventType
    {
        return self::$etWedding2;
    }

    public static function getETBirthday1(): EventType
    {
        return self::$etBirthday1;
    }

    public static function getETBirthday2(): EventType
    {
        return self::$etBirthday2;
    }

    public static function getETBirthday3(): EventType
    {
        return self::$etBirthday3;
    }

    public static function getETBusiness1(): EventType
    {
        return self::$etBusiness1;
    }

    public static function getWeddingIds(): array
    {
        return Chain::create([self::$etWedding2, self::$etWedding1])
            ->map(function(EventType $et) {
                return $et->getId();
            })
            ->sort()
            ->array;
    }

    public static function getBirthdayIds(): array
    {
        return Chain::create([self::$etBirthday1, self::$etBirthday2, self::$etBirthday3])
            ->map(function(EventType $et) {
                return $et->getId();
            })
            ->sort()
            ->array;
    }

    public static function getBusinessIds(): array
    {
        return Chain::create([self::$etBusiness1])
            ->map(function(EventType $et) {
                return $et->getId();
            })
            ->sort()
            ->array;
    }
}