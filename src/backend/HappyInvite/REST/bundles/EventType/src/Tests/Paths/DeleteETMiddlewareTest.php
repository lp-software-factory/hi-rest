<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventType\Tests\EventTypeMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class DeleteETMiddlewareTest extends EventTypeMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new EventTypesFixture());

        $et = EventTypesFixture::getETBirthday1();

        $this->requestEventTypeDelete($et->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeDelete($et->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventTypesFixture());

        $this->requestEventTypeDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypesFixture());

        $etId = EventTypesFixture::getETBirthday1()->getId();

        $this->requestEventTypeGet($etId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEventTypeDelete($etId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventTypeGet($etId)
            ->__invoke()
            ->expectStatusCode(404);
    }
}