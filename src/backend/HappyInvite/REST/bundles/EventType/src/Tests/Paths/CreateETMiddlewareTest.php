<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventType\Tests\EventTypeMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class CreateETMiddlewareTest extends EventTypeMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "url" => 'demo_et',
            "title" => [["region" => "en_GB", "value" => "Demo ET"]],
            "description" => [["region" => "en_GB", "value" => "Demo ET Description"]],
            "group_id" => EventTypeGroupsFixture::getETGWedding()->getId(),
        ];
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEventTypeCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEventTypeCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                    'group' => [
                        'id' => EventTypeGroupsFixture::getETGWedding()->getId(),
                        'title' => EventTypeGroupsFixture::getETGWedding()->getTitle()->toJSON(),
                        'description' => EventTypeGroupsFixture::getETGWedding()->getDescription()->toJSON(),
                    ],
                ],
            ]);
    }
}