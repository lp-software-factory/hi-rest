<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Paths;

use Cocur\Chain\Chain;
use HappyInvite\REST\Bundles\EventType\Tests\EventTypeMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class GetByGroupETMiddlewareTest extends EventTypeMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EventTypesFixture());

        $etg = EventTypeGroupsFixture::getETGWedding();

        $this->requestEventTypeGetAll($etg->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_types' => function($input) {
                    $this->assertTrue(is_array($input));
                    $this->assertEquals(count(EventTypesFixture::getWeddingIds()), count($input));

                    $this->assertEquals(
                        EventTypesFixture::getWeddingIds(),
                        Chain::create($input)
                            ->map(function($json) {
                                $this->assertTrue(isset($json['id']) && is_int($json['id']));

                                return $json['id'];
                            })
                            ->sort()
                            ->array
                    );
                }
            ]);
    }
}