<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventType\Tests\EventTypeMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class EditETMiddlewareTest extends EventTypeMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "url" => 'demo_et__',
            "title" => [["region" => "en_GB", "value" => "* Demo ET"]],
            "description" => [
                ["region" => "en_GB", "value" => "* EN Demo ET Description"],
                ["region" => "ru_RU", "value" => "* RU Demo ET Description"],
            ],
            "group_id" => EventTypeGroupsFixture::getETGWedding()->getId(),
        ];
    }

    public function test403()
    {
        $this->upFixture(new EventTypesFixture());

        $et = EventTypesFixture::getETBirthday1();
        $json = $this->getTestJSON();

        $this->requestEventTypeEdit($et->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeEdit($et->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EventTypesFixture());

        $json = $this->getTestJSON();

        $this->requestEventTypeEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypesFixture());

        $et = EventTypesFixture::getETBirthday1();
        $json = $this->getTestJSON();

        $this->requestEventTypeEdit($et->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type' => [
                    'id' => $et->getId(),
                    'sid' => $this->expectString(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                    'group' => [
                        'id' => EventTypeGroupsFixture::getETGWedding()->getId(),
                        'title' => EventTypeGroupsFixture::getETGWedding()->getTitle()->toJSON(),
                        'description' => EventTypeGroupsFixture::getETGWedding()->getDescription()->toJSON(),
                    ],
                ],
            ]);
    }
}