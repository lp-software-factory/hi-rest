<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventType\Tests\EventTypeMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;

final class MoveDownETMiddlewareTest extends EventTypeMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new EventTypesFixture());

        $et = EventTypesFixture::getETBirthday1();

        $this->requestEventTypeMoveDown($et->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeMoveDown($et->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventTypeMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypesFixture());

        $this->assertPosition(1, EventTypesFixture::getETBirthday1()->getId());
        $this->assertPosition(2, EventTypesFixture::getETBirthday2()->getId());
        $this->assertPosition(3, EventTypesFixture::getETBirthday3()->getId());

        $this->requestEventTypeMoveDown(EventTypesFixture::getETBirthday1()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertPosition(1, EventTypesFixture::getETBirthday2()->getId());
        $this->assertPosition(2, EventTypesFixture::getETBirthday1()->getId());
        $this->assertPosition(3, EventTypesFixture::getETBirthday3()->getId());

        $this->requestEventTypeMoveDown(EventTypesFixture::getETBirthday1()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, EventTypesFixture::getETBirthday2()->getId());
        $this->assertPosition(2, EventTypesFixture::getETBirthday3()->getId());
        $this->assertPosition(3, EventTypesFixture::getETBirthday1()->getId());

        $this->requestEventTypeMoveDown(EventTypesFixture::getETBirthday1()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, EventTypesFixture::getETBirthday2()->getId());
        $this->assertPosition(2, EventTypesFixture::getETBirthday3()->getId());
        $this->assertPosition(3, EventTypesFixture::getETBirthday1()->getId());
    }
}