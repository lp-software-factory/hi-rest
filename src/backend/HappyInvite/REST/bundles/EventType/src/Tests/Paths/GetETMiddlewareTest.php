<?php
namespace HappyInvite\REST\Bundles\EventType\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventType\Tests\EventTypeMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class GetETMiddlewareTest extends EventTypeMiddlewareTestCase
{
    public function test404()
    {
        $this->upFixture(new EventTypesFixture());

        $this->requestEventTypeGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypesFixture());

        $et = EventTypesFixture::getETBirthday1();

        $this->requestEventTypeGet($et->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type' => $et->toJSON(),
            ]);
    }
}