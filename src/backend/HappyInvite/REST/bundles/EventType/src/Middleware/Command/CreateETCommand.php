<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware\Command;

use HappyInvite\REST\Bundles\EventType\Request\CreateEventTypeRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateETCommand extends AbstractETCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = (new CreateEventTypeRequest($request))->getParameters();
        $eventType = $this->eventTypeService->createEventType($parameters);

        $responseBuilder
            ->setJSON([
                'event_type' => $eventType->toJSON(),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}