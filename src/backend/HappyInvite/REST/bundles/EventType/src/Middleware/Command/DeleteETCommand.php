<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Exceptions\EventTypeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteETCommand extends AbstractETCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $eventTypeId = $request->getAttribute('id');

            $this->eventTypeService->deleteEventType($eventTypeId);

            $responseBuilder->setStatusSuccess();
        }catch(EventTypeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}