<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware;

use HappyInvite\REST\Bundles\EventType\Middleware\Command\CreateETCommand;
use HappyInvite\REST\Bundles\EventType\Middleware\Command\DeleteETCommand;
use HappyInvite\REST\Bundles\EventType\Middleware\Command\EditETCommand;
use HappyInvite\REST\Bundles\EventType\Middleware\Command\GetAllETCommand;
use HappyInvite\REST\Bundles\EventType\Middleware\Command\GetByIdETCommand;
use HappyInvite\REST\Bundles\EventType\Middleware\Command\MoveDownETCommand;
use HappyInvite\REST\Bundles\EventType\Middleware\Command\MoveUpETCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class EventTypeMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateETCommand::class)
            ->attachDirect('edit', EditETCommand::class)
            ->attachDirect('delete', DeleteETCommand::class)
            ->attachDirect('get', GetByIdETCommand::class)
            ->attachDirect('all', GetAllETCommand::class)
            ->attachDirect('up', MoveUpETCommand::class)
            ->attachDirect('down', MoveDownETCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}