<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Exceptions\EventTypeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class MoveDownETCommand extends AbstractETCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $eventTypeId = $request->getAttribute('id');
            $newPosition = $this->eventTypeService->moveDownEventType($eventTypeId);

            $responseBuilder
                ->setJSON([
                    'position' => $newPosition,
                ])
                ->setStatusSuccess();
        }catch(EventTypeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}