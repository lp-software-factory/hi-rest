<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Exceptions\EventTypeGroupNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllETCommand extends AbstractETCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $groupId = $request->getAttribute('groupId');

        try {
            $responseBuilder
                ->setJSON([
                    'event_types' => array_map(function(EventType $eventType) {
                        return $eventType->toJSON();
                    }, $this->eventTypeService->getEventTypesByGroup($groupId))
                ])
                ->setStatusSuccess();
        }catch(EventTypeGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}