<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractETCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EventTypeService */
    protected $eventTypeService;

    public function __construct(AccessService $accessService, EventTypeService $eventTypeService)
    {
        $this->accessService = $accessService;
        $this->eventTypeService = $eventTypeService;
    }
}