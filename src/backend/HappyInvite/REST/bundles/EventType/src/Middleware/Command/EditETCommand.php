<?php
namespace HappyInvite\REST\Bundles\EventType\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Exceptions\EventTypeNotFoundException;
use HappyInvite\REST\Bundles\EventType\Request\EditEventTypeRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditETCommand extends AbstractETCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $eventTypeId = $request->getAttribute('id');
            $parameters = (new EditEventTypeRequest($request))->getParameters();

            $eventType = $this->eventTypeService->editEventType($eventTypeId, $parameters);

            $responseBuilder
                ->setJSON([
                    'event_type' => $eventType->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(EventTypeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}