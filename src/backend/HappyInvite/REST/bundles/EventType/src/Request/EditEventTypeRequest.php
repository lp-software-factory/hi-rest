<?php
namespace HappyInvite\REST\Bundles\EventType\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Parameters\EditEventTypeParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditEventTypeRequest extends SchemaRequest
{
    public function getParameters(): EditEventTypeParameters
    {
        $data = $this->getData();

        return new EditEventTypeParameters(
            $data['url'],
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description']),
            $data['group_id']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EventTypeBundle_EditEventTypeRequest');
    }
}