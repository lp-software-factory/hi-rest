<?php
namespace HappyInvite\REST\Bundles\EventType;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventTypeRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}