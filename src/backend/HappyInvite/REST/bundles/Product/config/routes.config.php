<?php
namespace HappyInvite\REST\Bundles\Product;

use HappyInvite\REST\Bundles\Product\Middleware\ProductMiddleware;
use HappyInvite\REST\Bundles\Product\Middleware\ProductsPackageMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/product/{command:create}[/]',
                'middleware' => ProductMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/product/{productId}/{command:delete}[/]',
                'middleware' => ProductMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/product/{productId}/{command:edit}[/]',
                'middleware' => ProductMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/product/{productId}/{command:get}[/]',
                'middleware' => ProductMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/product/{command:list}[/]',
                'middleware' => ProductMiddleware::class,
            ],
            [
                'method' => 'put',
                'path' => '/product/package/{command:create}[/]',
                'middleware' => ProductsPackageMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/product/package/{productsPackageId}/{command:edit}[/]',
                'middleware' => ProductsPackageMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/product/package/{productsPackageId}/{command:set-products}[/]',
                'middleware' => ProductsPackageMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/product/package/{productsPackageId}/{command:delete}[/]',
                'middleware' => ProductsPackageMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/product/package/{productsPackageId}/{command:get}[/]',
                'middleware' => ProductsPackageMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/product/package/{command:list}[/]',
                'middleware' => ProductsPackageMiddleware::class,
            ],
        ]
    ]
];