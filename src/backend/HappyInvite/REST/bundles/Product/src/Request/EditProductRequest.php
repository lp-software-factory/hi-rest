<?php
namespace HappyInvite\REST\Bundles\Product\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Product\Parameters\EditProductParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditProductRequest extends SchemaRequest
{
    public function getParameters(): EditProductParameters
    {
        $data = $this->getData();

        return new EditProductParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description']),
            (bool) $data['is_activated'],
            $data['base_price']['currency'],
            $data['base_price']['amount']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_PricingBundle_EditProductRequest');
    }
}