<?php
namespace HappyInvite\REST\Bundles\Product\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Product\Parameters\CreateProductParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateProductRequest extends SchemaRequest
{
    public function getParameters(): CreateProductParameters
    {
        $data = $this->getData();

        return new CreateProductParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description']),
            $data['base_price']['currency'],
            $data['base_price']['amount']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_PricingBundle_CreateProductRequest');
    }
}