<?php
namespace HappyInvite\REST\Bundles\Product\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SetProductsRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        $data = $this->getData();

        return $data['product_ids'];
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_PricingBundle_SetProductsRequest');
    }
}