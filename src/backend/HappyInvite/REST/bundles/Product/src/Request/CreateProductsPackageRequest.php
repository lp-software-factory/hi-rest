<?php
namespace HappyInvite\REST\Bundles\Product\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Palette\Entity\Palette;
use HappyInvite\Domain\Bundles\Product\Parameters\CreateProductsPackageParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateProductsPackageRequest extends SchemaRequest
{
    public function getParameters(): CreateProductsPackageParameters
    {
        $data = $this->getData();

        return new CreateProductsPackageParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description']),
            Palette::createFromJSON($data['palette']),
            $data['base_price']['currency'],
            $data['base_price']['amount']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_PricingBundle_CreateProductsPackageRequest');
    }
}