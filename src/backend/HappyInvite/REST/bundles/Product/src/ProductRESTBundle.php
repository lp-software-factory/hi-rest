<?php
namespace HappyInvite\REST\Bundles\Product;

use HappyInvite\REST\Bundle\RESTBundle;

final class ProductRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}