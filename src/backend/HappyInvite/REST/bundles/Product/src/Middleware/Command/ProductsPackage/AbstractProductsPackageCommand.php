<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\ProductsPackage;

use HappyInvite\Domain\Bundles\Product\Service\ProductsPackageService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractProductsPackageCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var ProductsPackageService */
    protected $productsPackageService;

    public function __construct(AccessService $accessService, ProductsPackageService $productsPackageService)
    {
        $this->accessService = $accessService;
        $this->productsPackageService = $productsPackageService;
    }
}