<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\ProductsPackage;

use HappyInvite\Domain\Bundles\Product\Exceptions\ProductsPackageNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetProductsPackageCommand extends AbstractProductsPackageCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $package = $this->productsPackageService->getById($request->getAttribute('productsPackageId'));

            $responseBuilder
                ->setJSON([
                    'products_package' => $package->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(ProductsPackageNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}