<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\ProductsPackage;

use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListProductsPackageCommand extends AbstractProductsPackageCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $responseBuilder
            ->setJSON([
                'product_packages' => array_map(function(ProductsPackage $package) {
                    return $package->toJSON();
                }, $this->productsPackageService->getAll())
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}