<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\Product;

use HappyInvite\Domain\Bundles\Product\Exceptions\ProductNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteProductCommand extends AbstractProductCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->productService->deleteProduct($request->getAttribute('productId'));

            $responseBuilder->setStatusSuccess();
        }catch(ProductNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}