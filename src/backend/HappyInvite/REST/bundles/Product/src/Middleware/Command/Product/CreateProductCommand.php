<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\Product;

use HappyInvite\REST\Bundles\Product\Request\CreateProductRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateProductCommand extends AbstractProductCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = (new CreateProductRequest($request))->getParameters();
        $product = $this->productService->createProduct($parameters);

        $responseBuilder
            ->setJSON([
                'product' => $product->toJSON(),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}