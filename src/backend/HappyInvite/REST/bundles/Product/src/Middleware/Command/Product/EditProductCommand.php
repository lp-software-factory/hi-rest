<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\Product;

use HappyInvite\Domain\Bundles\Product\Exceptions\ProductNotFoundException;
use HappyInvite\REST\Bundles\Product\Request\EditProductRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditProductCommand extends AbstractProductCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new EditProductRequest($request))->getParameters();
            $product = $this->productService->editProduct($request->getAttribute('productId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'product' => $product->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(ProductNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}