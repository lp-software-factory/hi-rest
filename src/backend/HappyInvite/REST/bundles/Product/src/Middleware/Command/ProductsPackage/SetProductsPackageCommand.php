<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\ProductsPackage;

use HappyInvite\Domain\Bundles\Product\Exceptions\ProductNotFoundException;
use HappyInvite\Domain\Bundles\Product\Exceptions\ProductsPackageNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Product\Request\SetProductsRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetProductsPackageCommand extends AbstractProductsPackageCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $ids = (new SetProductsRequest($request))->getParameters();

            $package = $this->productsPackageService->setProducts(
                $request->getAttribute('productsPackageId'), $ids
            );

            $responseBuilder
                ->setJSON([
                    'products_package' => $package->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(ProductsPackageNotFoundException | ProductNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}