<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\Product;

use HappyInvite\Domain\Bundles\Product\Service\ProductService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractProductCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var ProductService */
    protected $productService;

    public function __construct(AccessService $accessService, ProductService $productService)
    {
        $this->accessService = $accessService;
        $this->productService = $productService;
    }
}