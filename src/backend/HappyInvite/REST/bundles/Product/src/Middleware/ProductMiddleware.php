<?php
namespace HappyInvite\REST\Bundles\Product\Middleware;

use HappyInvite\REST\Bundles\Product\Middleware\Command\Product\CreateProductCommand;
use HappyInvite\REST\Bundles\Product\Middleware\Command\Product\DeleteProductCommand;
use HappyInvite\REST\Bundles\Product\Middleware\Command\Product\EditProductCommand;
use HappyInvite\REST\Bundles\Product\Middleware\Command\Product\GetProductCommand;
use HappyInvite\REST\Bundles\Product\Middleware\Command\Product\ListProductCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class ProductMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateProductCommand::class)
            ->attachDirect('delete', DeleteProductCommand::class)
            ->attachDirect('edit', EditProductCommand::class)
            ->attachDirect('list', ListProductCommand::class)
            ->attachDirect('get', GetProductCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}