<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\ProductsPackage;

use HappyInvite\Domain\Bundles\Product\Exceptions\ProductsPackageNotFoundException;
use HappyInvite\REST\Bundles\Product\Request\EditProductsPackageRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditProductsPackageCommand extends AbstractProductsPackageCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new EditProductsPackageRequest($request))->getParameters();
            $product = $this->productsPackageService->editProductsPackage($request->getAttribute('productsPackageId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'products_package' => $product->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(ProductsPackageNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}