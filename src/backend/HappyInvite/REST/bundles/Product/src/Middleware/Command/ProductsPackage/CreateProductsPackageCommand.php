<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\ProductsPackage;

use HappyInvite\REST\Bundles\Product\Request\CreateProductsPackageRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateProductsPackageCommand extends AbstractProductsPackageCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = (new CreateProductsPackageRequest($request))->getParameters();
        $product = $this->productsPackageService->createProductsPackage($parameters);

        $responseBuilder
            ->setJSON([
                'products_package' => $product->toJSON(),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}