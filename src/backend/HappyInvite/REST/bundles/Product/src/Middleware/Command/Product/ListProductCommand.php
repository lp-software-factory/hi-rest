<?php
namespace HappyInvite\REST\Bundles\Product\Middleware\Command\Product;

use HappyInvite\Domain\Bundles\Product\Entity\Product;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListProductCommand extends AbstractProductCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $responseBuilder
            ->setJSON([
                'products' => array_map(function(Product $product) {
                    return $product->toJSON();
                }, $this->productService->getAll())
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}