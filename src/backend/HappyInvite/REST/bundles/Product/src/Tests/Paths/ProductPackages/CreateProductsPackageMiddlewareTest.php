<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\ProductPackages;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductsPackageMiddlewareTestCase;

final class CreateProductsPackageMiddlewareTest extends ProductsPackageMiddlewareTestCase
{
    public function test200()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "Demo Product Description"]],
            "base_price" => [
                'amount' => 1400,
                'currency' => 'RUB'
            ],
            "palette" => $this->getPalettesService()->getRandomPalette()->toJSON()
        ];

        $this->requestProductPackagesCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                    'base_price' => [
                        'currency' => [
                            'code' => $json['base_price']['currency']
                        ],
                        'amount' => $json['base_price']['amount']
                    ],
                    'palette' => $json['palette']
                ]
            ])
            ->expect(function(array $json) {
                $this->assertEquals(0, count($json['products_package']['products']));
            })
        ;
    }

    public function test403()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "Demo Product Description"]],
            "base_price" => [
                'amount' => 1400,
                'currency' => 'RUB'
            ],
            "palette" => $this->getPalettesService()->getRandomPalette()->toJSON()
        ];

        $this->requestProductPackagesCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestProductPackagesCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}