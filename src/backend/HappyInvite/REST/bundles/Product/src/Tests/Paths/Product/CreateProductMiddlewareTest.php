<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\Product;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductMiddlewareTestCase;

final class CreateProductMiddlewareTest extends ProductMiddlewareTestCase
{
    public function test403()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "Demo Product Description"]],
            "base_price" => [
                'amount' => 1400,
                'currency' => 'RUB'
            ],
        ];

        $this->requestProductCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestProductCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "Demo Product Description"]],
            "base_price" => [
                'amount' => 1400,
                'currency' => 'RUB'
            ],
        ];

        $this->requestProductCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'product' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'title' =>  $json['title'],
                    'description' => $json['description'],
                    'base_price' => [
                        'id' => $this->expectId(),
                        'currency' => [
                            'code' => $json['base_price']['currency']
                        ],
                        'amount' => $json['base_price']['amount']
                    ],
                    'is_activated' => false,
                ]
            ]);
    }
}