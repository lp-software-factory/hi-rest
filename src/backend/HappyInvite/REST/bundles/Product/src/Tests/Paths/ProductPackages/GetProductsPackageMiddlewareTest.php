<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\ProductPackages;

use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductsPackageFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductsPackageMiddlewareTestCase;

final class GetProductsPackageMiddlewareTest extends ProductsPackageMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;

        $this->requestProductPackagesGet($package->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => $package->toJSON(),
            ]);
    }

    public function test404()
    {
        $this->upFixture(new ProductsPackageFixture());

        $this->requestProductPackagesGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}