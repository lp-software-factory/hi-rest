<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\ProductPackages;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductsPackageFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductsPackageMiddlewareTestCase;

final class EditProductsPackageMiddlewareTest extends ProductsPackageMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;

        $json = [
            "title" => [["region" => "en_GB", "value" => "* Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "* Demo Product Description"]],
            "base_price" => [
                'amount' => rand(0, 10000),
                'currency' => 'RUB'
            ],
            "palette" => $this->getPalettesService()->getRandomPalette()->toJSON(),
            'is_activated' => true
        ];

        $this->requestProductPackagesEdit($package->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => [
                    'id' => $package->getId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                    'base_price' => [
                        'currency' => [
                            'code' => $json['base_price']['currency']
                        ],
                        'amount' => $json['base_price']['amount']
                    ],
                    'palette' => $json['palette'],
                    'is_activated' => true,
                ]
            ])
            ->expect(function(array $json) {
                $this->assertEquals(0, count($json['products_package']['products']));
            })
        ;
    }

    public function test403()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;

        $json = [
            "title" => [["region" => "en_GB", "value" => "* Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "* Demo Product Description"]],
            "base_price" => [
                'amount' => rand(0, 10000),
                'currency' => 'RUB'
            ],
            "palette" => $this->getPalettesService()->getRandomPalette()->toJSON(),
            'is_activated' => true
        ];

        $this->requestProductPackagesEdit($package->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestProductPackagesEdit($package->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new ProductsPackageFixture());

        $json = [
            "title" => [["region" => "en_GB", "value" => "* Demo Product"]],
            "description" => [["region" => "en_GB", "value" => "* Demo Product Description"]],
            "base_price" => [
                'amount' => rand(0, 10000),
                'currency' => 'RUB'
            ],
            "palette" => $this->getPalettesService()->getRandomPalette()->toJSON(),
            'is_activated' => true
        ];

        $this->requestProductPackagesEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}