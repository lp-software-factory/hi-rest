<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait ProductPackagesRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestProductPackagesCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/product/package/create')
            ->setParameters($json);
    }

    protected function requestProductPackagesEdit(int $productsPackageId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/product/package/%d/edit', $productsPackageId))
            ->setParameters($json);
    }

    protected function requestProductPackagesDelete(int $productsPackageId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/product/package/%d/delete', $productsPackageId));
    }

    protected function requestProductPackagesGet(int $productsPackageId): RESTRequest
    {
        return $this->request('GET', sprintf('/product/package/%d/get', $productsPackageId));
    }

    protected function requestSetProducts(int $productsPackageId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/product/package/%d/set-products', $productsPackageId))
            ->setParameters($json);
    }

    protected function requestProductPackagesList(): RESTRequest
    {
        return $this->request('GET', '/product/package/list');
    }
}