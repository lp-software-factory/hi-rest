<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\ProductPackages;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductFixture;
use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductsPackageFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductsPackageMiddlewareTestCase;

final class SetProductsPackageMiddlewareTest extends ProductsPackageMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;
        $productIds = [
            ProductFixture::getProductA()->getId(),
            ProductFixture::getProductC()->getId(),
        ];

        $this->requestSetProducts($package->getId(), ['product_ids' => $productIds])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => [
                    'id' => $package->getId(),
                ]
            ])
            ->expect(function(array $json) use ($productIds) {
                $this->assertTrue(isset($json['products_package']['products']));
                $this->assertTrue(is_array($json['products_package']['products']));
                $this->assertEquals(2, count($json['products_package']['products']));
                $this->assertEquals($productIds, array_map(function(array $input) {
                    return $input['id'];
                }, $json['products_package']['products']));
            });
    }

    public function test200OrderMatters()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;
        $productIds = [
            ProductFixture::getProductC()->getId(),
            ProductFixture::getProductA()->getId(),
            ProductFixture::getProductB()->getId(),
        ];

        $this->requestSetProducts($package->getId(), ['product_ids' => $productIds])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => [
                    'id' => $package->getId(),
                ]
            ])
            ->expect(function(array $json) use ($productIds) {
                $this->assertTrue(isset($json['products_package']['products']));
                $this->assertTrue(is_array($json['products_package']['products']));
                $this->assertEquals(3, count($json['products_package']['products']));
                $this->assertEquals($productIds, array_map(function(array $input) {
                    return $input['id'];
                }, $json['products_package']['products']));
            });

        $productIds = [
            ProductFixture::getProductA()->getId(),
            ProductFixture::getProductB()->getId(),
        ];

        $this->requestSetProducts($package->getId(), ['product_ids' => $productIds])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => [
                    'id' => $package->getId(),
                ]
            ])
            ->expect(function(array $json) use ($productIds) {
                $this->assertTrue(isset($json['products_package']['products']));
                $this->assertTrue(is_array($json['products_package']['products']));
                $this->assertEquals(2, count($json['products_package']['products']));
                $this->assertEquals($productIds, array_map(function(array $input) {
                    return $input['id'];
                }, $json['products_package']['products']));
            });

        $productIds = [
            ProductFixture::getProductB()->getId(),
            ProductFixture::getProductA()->getId(),
        ];

        $this->requestSetProducts($package->getId(), ['product_ids' => $productIds])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'products_package' => [
                    'id' => $package->getId(),
                ]
            ])
            ->expect(function(array $json) use ($productIds) {
                $this->assertTrue(isset($json['products_package']['products']));
                $this->assertTrue(is_array($json['products_package']['products']));
                $this->assertEquals(2, count($json['products_package']['products']));
                $this->assertEquals($productIds, array_map(function(array $input) {
                    return $input['id'];
                }, $json['products_package']['products']));
            });
    }

    public function test403()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;
        $productIds = [
            ProductFixture::getProductC()->getId(),
            ProductFixture::getProductA()->getId(),
            ProductFixture::getProductB()->getId(),
        ];

        $this->requestSetProducts($package->getId(), ['product_ids' => $productIds])
            ->__invoke()
            ->expectAuthError();

        $this->requestSetProducts($package->getId(), ['product_ids' => $productIds])
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new ProductsPackageFixture());

        $productIds = [
            ProductFixture::getProductC()->getId(),
            ProductFixture::getProductA()->getId(),
            ProductFixture::getProductB()->getId(),
        ];

        $this->requestSetProducts(self::NOT_FOUND_ID, ['product_ids' => $productIds])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}