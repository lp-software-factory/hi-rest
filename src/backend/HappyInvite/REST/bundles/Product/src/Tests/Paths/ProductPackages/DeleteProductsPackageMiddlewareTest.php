<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\ProductPackages;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductsPackageFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductsPackageMiddlewareTestCase;

final class DeleteProductsPackageMiddlewareTest extends ProductsPackageMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;
        $packageId = $package->getId();

        $this->requestProductPackagesGet($packageId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestProductPackagesDelete($packageId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        $this->requestProductPackagesGet($packageId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new ProductsPackageFixture());

        $package = ProductsPackageFixture::$packageA;
        $packageId = $package->getId();

        $this->requestProductPackagesDelete($packageId)
            ->__invoke()
            ->expectAuthError();

        $this->requestProductPackagesDelete($packageId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new ProductsPackageFixture());

        $this->requestProductPackagesDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}