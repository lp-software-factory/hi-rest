<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait ProductRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestProductCreate(array $json): RESTRequest
    {
        return $this->request('put', '/product/create')
            ->setParameters($json);
    }

    protected function requestProductGet(int $productId): RESTRequest
    {
        return $this->request('get', sprintf('/product/%d/get', $productId));
    }

    protected function requestProductEdit(int $productId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/product/%d/edit', $productId))
            ->setParameters($json);
    }

    protected function requestProductDelete(int $productId): RESTRequest
    {
        return $this->request('delete', sprintf('/product/%d/delete', $productId));
    }

    protected function requestProductList(): RESTRequest
    {
        return $this->request('get', '/product/list');
    }
}