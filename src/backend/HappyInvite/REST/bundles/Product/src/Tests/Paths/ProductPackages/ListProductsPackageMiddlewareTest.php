<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\ProductPackages;

use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage;
use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductsPackageFixture;
use HappyInvite\REST\Bundles\Product\Tests\ProductsPackageMiddlewareTestCase;

final class ListProductsPackageMiddlewareTest extends ProductsPackageMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new ProductsPackageFixture());

        $this->requestProductPackagesList()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'product_packages' => array_map(function(ProductsPackage $package) {
                    return $package->toJSON();
                }, ProductsPackageFixture::getOrderedList())
            ]);
    }
}