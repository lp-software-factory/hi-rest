<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Palette\Service\PaletteService;
use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage;
use HappyInvite\Domain\Bundles\Product\Parameters\CreateProductsPackageParameters;
use HappyInvite\Domain\Bundles\Product\Service\ProductsPackageService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class ProductsPackageFixture implements Fixture
{
    /** @var ProductsPackage */
    public static $packageA;

    /** @var ProductsPackage */
    public static $packageB;

    /** @var ProductsPackage */
    public static $packageC;

    public function up(Application $app, EntityManager $em)
    {
        $palettes = $app->getContainer()->get(PaletteService::class); /** @var PaletteService $palettes */
        $service = $app->getContainer()->get(ProductsPackageService::class); /** @var ProductsPackageService $service */

        self::$packageA = $service->createProductsPackage(new CreateProductsPackageParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Package A']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Package A Description']]),
            $palettes->getRandomPalette(),
            'USD',
            100
        ));

        self::$packageB = $service->createProductsPackage(new CreateProductsPackageParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Package B']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Package B Description']]),
            $palettes->getRandomPalette(),
            'USD',
            400
        ));

        self::$packageC = $service->createProductsPackage(new CreateProductsPackageParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Package C']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Package C Description']]),
            $palettes->getRandomPalette(),
            'RUB',
            29999
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$packageA,
            self::$packageB,
            self::$packageC,
        ];
    }
}