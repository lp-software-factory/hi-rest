<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Paths\Product;

use HappyInvite\REST\Bundles\Product\Tests\ProductMiddlewareTestCase;

final class GetProductMiddlewareTest extends ProductMiddlewareTestCase
{
    public function test404()
    {
        $this->requestProductGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {

    }
}