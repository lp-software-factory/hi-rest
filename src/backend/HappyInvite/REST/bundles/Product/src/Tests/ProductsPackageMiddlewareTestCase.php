<?php
namespace HappyInvite\REST\Bundles\Product\Tests;

use HappyInvite\Domain\Bundles\Palette\Service\PaletteService;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\Money\Tests\Fixture\CurrencyFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;
use HappyInvite\REST\Bundles\Product\Tests\Fixture\ProductFixture;

abstract class ProductsPackageMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new CurrencyFixture(),
            new ProductFixture(),
        ];
    }

    protected function getPalettesService(): PaletteService
    {
        return self::$app->getContainer()->get(PaletteService::class);
    }
}