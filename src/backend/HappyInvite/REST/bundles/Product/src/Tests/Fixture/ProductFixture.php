<?php
namespace HappyInvite\REST\Bundles\Product\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Product\Entity\Product;
use HappyInvite\Domain\Bundles\Product\Parameters\CreateProductParameters;
use HappyInvite\Domain\Bundles\Product\Service\ProductService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class ProductFixture implements Fixture
{
    /** @var Product */
    private static $productA;

    /** @var Product */
    private static $productB;

    /** @var Product */
    private static $productC;

    public function up(Application $app, EntityManager $em)
    {
        /** @var ProductService $service */
        $service = $app->getContainer()->get(ProductService::class);

        self::$productA = $service->createProduct(new CreateProductParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Product A']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Product A Description']]),
            'USD',
            100
        ));

        self::$productB = $service->createProduct(new CreateProductParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Product B']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Product B Description']]),
            'USD',
            1400
        ));

        self::$productC = $service->createProduct(new CreateProductParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Product C']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Product C Description']]),
            'RUB',
            15250
        ));
    }

    public static function getProductA(): Product
    {
        return self::$productA;
    }

    public static function getProductB(): Product
    {
        return self::$productB;
    }

    public static function getProductC(): Product
    {
        return self::$productC;
    }
}