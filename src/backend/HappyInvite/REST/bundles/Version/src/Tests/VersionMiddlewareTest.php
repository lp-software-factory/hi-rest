<?php
namespace HappyInvite\REST\Bundles\Version\Tests;

use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

final class VersionMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [];
    }

    public function testCurrentVersion200()
    {
        $this->requestVersionCurrent()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'version' => $this->expectString(),
            ]);
    }
}