<?php
namespace HappyInvite\REST\Bundles\Version\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CurrentVersionCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        return $responseBuilder
            ->setJSON([
                'version' => $this->versionService->getCurrentVersion(),
            ])
            ->setStatusSuccess()
            ->build();
    }
}