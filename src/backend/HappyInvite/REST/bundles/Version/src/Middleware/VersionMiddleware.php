<?php
namespace HappyInvite\REST\Bundles\Version\Middleware;

use HappyInvite\REST\Bundles\Version\Middleware\Command\CurrentVersionCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class VersionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('current', CurrentVersionCommand::class)
            ->resolve($request)
        ;

        return $resolver->__invoke($request, new HIResponseBuilder($response));
    }
}