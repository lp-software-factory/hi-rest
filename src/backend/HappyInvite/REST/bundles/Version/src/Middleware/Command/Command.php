<?php
namespace HappyInvite\REST\Bundles\Version\Middleware\Command;

use HappyInvite\Domain\Bundles\Version\Service\VersionService;

abstract class Command implements \HappyInvite\REST\Command\Command
{
    /** @var VersionService */
    protected $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }
}