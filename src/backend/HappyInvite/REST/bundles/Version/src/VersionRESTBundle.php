<?php
namespace HappyInvite\REST\Bundles\Version;

use HappyInvite\REST\Bundle\RESTBundle;

final class VersionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}