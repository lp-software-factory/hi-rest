<?php
namespace HappyInvite\REST\Bundles\Version;

use HappyInvite\REST\Bundles\Version\Middleware\VersionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'group' => 'default',
                'method' => 'get',
                'path' => '/version/{command:current}[/]',
                'middleware' => VersionMiddleware::class,
            ],
        ]
    ]
];