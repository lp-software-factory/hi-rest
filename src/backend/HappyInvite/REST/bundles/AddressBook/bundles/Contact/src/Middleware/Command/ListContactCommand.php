<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListContactCommand extends AbstractContactCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $entities = $this->service->getAllByOwner($this->authToken->getProfile());

        $responseBuilder
            ->setJSON([
                'contacts' => $this->formatter->formatMany($entities)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}