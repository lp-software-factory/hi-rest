<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\ContactMiddlewareTestCase;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Fixture\ContactFixture;

final class ListRecipientsGroupMiddlewareTest extends ContactMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new ContactFixture());

        $this->requestAddressBookContactList()
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
            ]);
    }

    public function test403()
    {
        $this->upFixture(new ContactFixture());

        $this->requestAddressBookContactList(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectAuthError();
    }
}