<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateContactCommand extends AbstractContactCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $parameters = $this->parametersFactory->factoryCreateParameters($request);

        $entity = $this->service->create($parameters);

        $responseBuilder
            ->setJSON([
                'contact' => $this->formatter->formatOne($entity)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}