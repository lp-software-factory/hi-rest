<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserCFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class ContactMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
            new UserAccountFixture(),
            new UserAFixture(),
            new UserBFixture(),
            new UserCFixture(),
            new EventRecipientsGroupFixture(),
        ];
    }
}