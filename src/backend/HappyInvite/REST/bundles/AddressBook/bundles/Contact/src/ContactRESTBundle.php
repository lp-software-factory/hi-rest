<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact;

use HappyInvite\REST\Bundle\RESTBundle;

final class ContactRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}