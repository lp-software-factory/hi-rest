<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\ContactMiddlewareTestCase;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Fixture\ContactFixture;

final class DeleteContactMiddlewareTest extends ContactMiddlewareTestCase
{
    public function test200Admin()
    {
        $this->upFixture(new ContactFixture());

        $id = ContactFixture::$contactEF1->getId();

        $this->requestAddressBookContactGetById($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestAddressBookContactDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestAddressBookContactGetById($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200Owner()
    {
        $this->upFixture(new ContactFixture());

        $id = ContactFixture::$contactEF1->getId();

        $this->requestAddressBookContactGetById($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestAddressBookContactDelete($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestAddressBookContactGetById($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new ContactFixture());

        $id = ContactFixture::$contactEF1->getId();

        $this->requestAddressBookContactDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestAddressBookContactDelete($id)
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestAddressBookContactGetById($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test404()
    {
        $this->upFixture(new ContactFixture());

        $this->requestAddressBookContactDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}