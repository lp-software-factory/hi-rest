<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions\ContactNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetContactCommand extends AbstractContactCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $contactId = $request->getAttribute('contactId');

        try {
            $entity = $this->service->getByOwnerAndId($contactId);

            $responseBuilder
                ->setJSON([
                    'contact' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(ContactNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}