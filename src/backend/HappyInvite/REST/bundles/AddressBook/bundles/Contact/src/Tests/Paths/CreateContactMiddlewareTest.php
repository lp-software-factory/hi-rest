<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\ContactMiddlewareTestCase;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;

final class CreateContactMiddlewareTest extends ContactMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'first_name' => 'First',
            'last_name' => 'Last',
            'middle_name' => 'Middle',
            'email' => 'demo@example.com',
            'phone' => '+7 800 000 000',
            'group_ids' => [
                EventRecipientsGroupFixture::$eventRecipientsGroupEF1->getId(),
                EventRecipientsGroupFixture::$eventRecipientsGroupEF2->getId(),
                EventRecipientsGroupFixture::$eventRecipientsGroupEF3->getId(),
            ]
        ];
    }

    public function test200Admin()
    {
        $id = $this->requestAddressBookContactCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'contact' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'owner_profile_id' => AdminAccountFixture::getProfile()->getId(),
                        'first_name' => $json['first_name'],
                        'last_name' => $json['last_name'],
                        'middle_name' => $json['middle_name'],
                        'email' => $json['email'],
                        'phone' => $json['phone'],
                        'group_ids' => $json['group_ids'],
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['contact']['entity']['id'];
            });

        $this->requestAddressBookContactGetById($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test200User()
    {
        $id = $this->requestAddressBookContactCreate($json = $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'contact' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'owner_profile_id' => UserAFixture::getProfile()->getId(),
                        'first_name' => $json['first_name'],
                        'last_name' => $json['last_name'],
                        'middle_name' => $json['middle_name'],
                        'email' => $json['email'],
                        'phone' => $json['phone'],
                        'group_ids' => $json['group_ids'],
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['contact']['entity']['id'];
            });

        $this->requestAddressBookContactGetById($id)
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->requestAddressBookContactCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
    }

}