<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions\ContactIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupIsNotYoursException;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command\CreateContactCommand;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command\DeleteContactCommand;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command\EditContactCommand;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command\ListContactCommand;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command\GetContactCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class ContactMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateContactCommand::class)
            ->attachDirect('delete', DeleteContactCommand::class)
            ->attachDirect('edit', EditContactCommand::class)
            ->attachDirect('get', GetContactCommand::class)
            ->attachDirect('list', ListContactCommand::class)
            ->resolve($request);

        try {
            return $resolver->__invoke($request, $responseBuilder);
        }catch(ContactIsNotYoursException|EventRecipientsGroupIsNotYoursException $e) {
            return $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed()
                ->build();
        }
    }
}