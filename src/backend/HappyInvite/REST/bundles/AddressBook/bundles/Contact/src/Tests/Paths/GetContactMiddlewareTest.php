<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserBFixture;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\ContactMiddlewareTestCase;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Fixture\ContactFixture;

final class GetContactMiddlewareTest extends ContactMiddlewareTestCase
{
    public function test200Owner()
    {
        $this->upFixture(new ContactFixture());

        $fixture = ContactFixture::$contactEF1;

        $this->requestAddressBookContactGetById($fixture->getId())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'contact' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test200Admin()
    {
        $this->upFixture(new ContactFixture());

        $fixture = ContactFixture::$contactEF1;

        $this->requestAddressBookContactGetById($fixture->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'contact' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new ContactFixture());

        $fixture = ContactFixture::$contactEF1;

        $this->requestAddressBookContactGetById($fixture->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestAddressBookContactGetById($fixture->getId())
            ->auth(UserBFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new ContactFixture());

        $this->requestAddressBookContactGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}