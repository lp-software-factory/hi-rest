<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\ParametersFactory;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Parameters\EditContactParameters;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Parameters\CreateContactParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Service\EventRecipientsGroupService;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Request\CreateContactRequest;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Request\EditContactRequest;
use Psr\Http\Message\ServerRequestInterface;

final class ContactParametersFactory
{
    /** @var AuthToken */
    private $authToken;

    /** @var EventRecipientsGroupService */
    private $eventRecipientsGroupService;

    public function __construct(AuthToken $authToken, EventRecipientsGroupService $eventRecipientsGroupService)
    {
        $this->authToken = $authToken;
        $this->eventRecipientsGroupService = $eventRecipientsGroupService;
    }

    public function factoryCreateParameters(ServerRequestInterface $request): CreateContactParameters
    {
        $json = (new CreateContactRequest($request))->getParameters();

        return new CreateContactParameters(
            $this->authToken->getProfile(),
            (string) $json['first_name'],
            (string) $json['last_name'],
            (string) $json['middle_name'],
            (string) $json['email'],
            (string) $json['phone'],
            array_map(function(int $id) {
                return $this->eventRecipientsGroupService->getByIdAdnOwner($id);
            }, $json['group_ids'])
        );
    }

    public function factoryEditParameters(int $contactId, ServerRequestInterface $request): EditContactParameters
    {
        $json = (new EditContactRequest($request))->getParameters();

        return new EditContactParameters(
            $this->authToken->getProfile(),
            (string) $json['first_name'],
            (string) $json['last_name'],
            (string) $json['middle_name'],
            (string) $json['email'],
            (string) $json['phone'],
            array_map(function(int $id) {
                return $this->eventRecipientsGroupService->getByIdAdnOwner($id);
            }, $json['group_ids'])
        );
    }

}