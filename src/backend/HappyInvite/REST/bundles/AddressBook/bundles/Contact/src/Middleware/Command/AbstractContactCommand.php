<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Formatter\ContactFormatter;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Service\ContactService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\ParametersFactory\ContactParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractContactCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var ContactParametersFactory */
    protected $parametersFactory;

    /** @var ContactFormatter */
    protected $formatter;

    /** @var ContactService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        ContactParametersFactory $parametersFactory,
        ContactFormatter $formatter,
        ContactService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}