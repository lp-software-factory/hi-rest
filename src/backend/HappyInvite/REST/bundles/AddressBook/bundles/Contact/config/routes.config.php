<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact;

use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Middleware\ContactMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/address-book/contact/{command:create}[/]',
                'middleware' => ContactMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/address-book/contact/{contactId}/{command:edit}[/]',
                'middleware' => ContactMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/address-book/contact/{contactId}/{command:delete}[/]',
                'middleware' => ContactMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/address-book/contact/{contactId}/{command:get}[/]',
                'middleware' => ContactMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/address-book/contact/{command:list}[/]',
                'middleware' => ContactMiddleware::class,
            ],
        ]
    ]
];