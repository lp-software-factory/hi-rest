<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Parameters\CreateContactParameters;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Service\ContactService;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class ContactFixture implements Fixture
{
    /** @var Contact */
    public static $contactEF1;

    /** @var Contact */
    public static $contactEF2;

    /** @var Contact */
    public static $contactEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(ContactService::class); /** @var ContactService $service */

        self::$contactEF1 = $service->create(new CreateContactParameters(
            UserAFixture::getProfile(),
            'Riven',
            'Wind',
            'Blades',
            'riven@best.na',
            '8-800-RIVEN',
            [
                EventRecipientsGroupFixture::$eventRecipientsGroupEF1,
                EventRecipientsGroupFixture::$eventRecipientsGroupEF2,
            ]
        ));

        self::$contactEF2 = $service->create(new CreateContactParameters(
            UserAFixture::getProfile(),
            'Luku',
            'Fae',
            'Sorceress',
            'tastes@pur.pl',
            '',
            [
                EventRecipientsGroupFixture::$eventRecipientsGroupEF3,
            ]
        ));

        self::$contactEF3 = $service->create(new CreateContactParameters(
            UserAFixture::getProfile(),
            'Singed',
            'Rylai',
            'Mask',
            'science-experiments@zaun.com',
            '+654-800-400050-00',
            []
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$contactEF1,
            self::$contactEF2,
            self::$contactEF3,
        ];
    }
}