<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait AddressBookContactRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestAddressBookContactCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/address-book/contact/create')
            ->setParameters($json);
    }

    protected function requestAddressBookContactEdit(int $contactId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/address-book/contact/%d/edit', $contactId))
            ->setParameters($json);
    }

    protected function requestAddressBookContactDelete(int $contactId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/address-book/contact/%d/delete', $contactId));
    }

    protected function requestAddressBookContactGetById(int $contactId): RESTRequest
    {
        return $this->request('GET', sprintf('/address-book/contact/%d/get', $contactId));
    }

    protected function requestAddressBookContactList(): RESTRequest
    {
        return $this->request('GET', sprintf('/address-book/contact/list'));
    }
}