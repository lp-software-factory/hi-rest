<?php
namespace HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\MoreUsers\UserAFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\ContactMiddlewareTestCase;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Fixture\ContactFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipientsGroup\Tests\Fixture\EventRecipientsGroupFixture;

final class EditContactMiddlewareTest extends ContactMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'first_name' => '*First',
            'last_name' => '*Last',
            'middle_name' => '*Middle',
            'email' => '*demo@example.com',
            'phone' => '+7 801 000 000',
            'group_ids' => [
                EventRecipientsGroupFixture::$eventRecipientsGroupEF1->getId(),
            ]
        ];
    }

    public function test200Admin()
    {
        $this->upFixture(new ContactFixture());

        $fixture = ContactFixture::$contactEF1;

        $this->requestAddressBookContactEdit($fixture->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'contact' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                        'first_name' => $json['first_name'],
                        'last_name' => $json['last_name'],
                        'middle_name' => $json['middle_name'],
                        'email' => $json['email'],
                        'phone' => $json['phone'],
                        'group_ids' => $json['group_ids'],
                    ]
                ]
            ]);
    }

    public function test200Owner()
    {
        $this->upFixture(new ContactFixture());

        $fixture = ContactFixture::$contactEF1;

        $this->requestAddressBookContactEdit($fixture->getId(), $json = $this->getTestJSON())
            ->auth(UserAFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'contact' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                        'first_name' => $json['first_name'],
                        'last_name' => $json['last_name'],
                        'middle_name' => $json['middle_name'],
                        'email' => $json['email'],
                        'phone' => $json['phone'],
                        'group_ids' => $json['group_ids'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new ContactFixture());

        $fixture = ContactFixture::$contactEF1;

        $this->requestAddressBookContactEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestAddressBookContactEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new ContactFixture());

        $this->requestAddressBookContactEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}