<?php
namespace HappyInvite\Domain\Bundles\AddressBook;

use HappyInvite\REST\Bundle\RESTBundle;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\ContactRESTBundle;

final class AddressBookRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new ContactRESTBundle(),
        ];
    }
}