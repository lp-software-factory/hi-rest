<?php
namespace HappyInvite\REST\Bundles\Solution;

use HappyInvite\REST\Bundles\Solution\Middleware\SolutionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/solution/{command:create}[/]',
                'middleware' => SolutionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/solution/{solutionId}/{command:edit}[/]',
                'middleware' => SolutionMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/solution/{solutionId}/{command:delete}[/]',
                'middleware' => SolutionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/solution/{solutionId}/{command:get}[/]',
                'middleware' => SolutionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/solution/{command:repository}/{source}[/]',
                'middleware' => SolutionMiddleware::class,
            ],
        ]
    ]
];