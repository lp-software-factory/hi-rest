<?php
namespace HappyInvite\REST\Bundles\Solution\ParametersFactory;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service\EnvelopeTemplateService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service\EventLandingTemplateService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use HappyInvite\Domain\Bundles\Solution\Parameters\CreateSolutionParameters;
use HappyInvite\Domain\Bundles\Solution\Parameters\EditSolutionParameters;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\REST\Bundles\Solution\Request\CreateSolutionRequest;
use HappyInvite\REST\Bundles\Solution\Request\EditSolutionRequest;
use HappyInvite\REST\Bundles\Solution\Request\RepositorySolutionRequest;
use Psr\Http\Message\ServerRequestInterface;

final class SolutionParametersFactory
{
    /** @var ProfileService */
    private $profileService;

    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var EnvelopeTemplateService */
    private $envelopeTemplateService;

    /** @var EventLandingTemplateService */
    private $eventLandingTemplateService;

    public function __construct(
        ProfileService $profileService,
        InviteCardTemplateService $inviteCardTemplateService,
        EnvelopeTemplateService $envelopeTemplateService,
        EventLandingTemplateService $eventLandingTemplateService
    ) {
        $this->profileService = $profileService;
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->envelopeTemplateService = $envelopeTemplateService;
        $this->eventLandingTemplateService = $eventLandingTemplateService;
    }

    public function factoryRepositoryQuery(ServerRequestInterface $request): array
    {
        return (new RepositorySolutionRequest($request))->getParameters();
    }

    public function factoryCreateParameters(ServerRequestInterface $request): CreateSolutionParameters
    {
        $json = (new CreateSolutionRequest($request))->getParameters();

        $parameters = new CreateSolutionParameters(
            $this->inviteCardTemplateService->getByIds(new IdsCriteria($json['invite_card_template_ids'])),
            $this->envelopeTemplateService->getByIds(new IdsCriteria($json['envelope_template_ids'])),
            $this->eventLandingTemplateService->getByIds(new IdsCriteria($json['event_landing_template_ids']))
        );

        if(isset($json['target'])) {
            $parameters->assignTo($this->profileService->getProfileById($json['target']));
        }

        return $parameters;
    }

    public function factoryEditParameters(int $solutionId, ServerRequestInterface $request): EditSolutionParameters
    {
        $json = (new EditSolutionRequest($request))->getParameters();

        $parameters = new EditSolutionParameters(
            $this->inviteCardTemplateService->getByIds(new IdsCriteria($json['invite_card_template_ids'])),
            $this->envelopeTemplateService->getByIds(new IdsCriteria($json['envelope_template_ids'])),
            $this->eventLandingTemplateService->getByIds(new IdsCriteria($json['event_landing_template_ids']))
        );

        if(isset($json['target'])) {
            $parameters->assignTo($this->profileService->getProfileById($json['target']));
        }

        return $parameters;
    }
}