<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware\Command;

use HappyInvite\Domain\Bundles\Solution\Exceptions\SolutionNotFoundException;
use HappyInvite\Domain\Bundles\SolutionIndex\Facade\SolutionIndexQueryFacade;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class RepositorySolutionCommand extends AbstractSolutionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $source = $request->getAttribute('source');

        try {
            $jsonRequest = $this->parametersFactory->factoryRepositoryQuery($request);

            $response = $this->solutionIndexQueryFacade->query($source, $jsonRequest['query'], [
                SolutionIndexQueryFacade::OPTION_FOR_AS_RESPONSE200 => true,
                SolutionIndexQueryFacade::OPTION_FORMAT_MAP_FILTER => isset($jsonRequest['filter']) ? $jsonRequest['filter'] : false,
            ]);

            $responseBuilder
                ->setJSON($response)
                ->setStatusSuccess();
        }catch(SolutionNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}