<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Solution\Middleware\Command\CreateSolutionCommand;
use HappyInvite\REST\Bundles\Solution\Middleware\Command\DeleteSolutionCommand;
use HappyInvite\REST\Bundles\Solution\Middleware\Command\EditSolutionCommand;
use HappyInvite\REST\Bundles\Solution\Middleware\Command\GetSolutionCommand;
use HappyInvite\REST\Bundles\Solution\Middleware\Command\RepositorySolutionCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class SolutionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateSolutionCommand::class)
            ->attachDirect('edit', EditSolutionCommand::class)
            ->attachDirect('delete', DeleteSolutionCommand::class)
            ->attachDirect('repository', RepositorySolutionCommand::class)
            ->attachDirect('get', GetSolutionCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}