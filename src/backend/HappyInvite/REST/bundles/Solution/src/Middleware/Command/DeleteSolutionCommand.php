<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware\Command;

use HappyInvite\Domain\Bundles\Solution\Exceptions\SolutionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteSolutionCommand extends AbstractSolutionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $solutionId = $request->getAttribute('solutionId');

        try {
            $solution = $this->service->getById($solutionId);

            $this->service->deleteSolution($solutionId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(SolutionNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}