<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware\Command;

use HappyInvite\Domain\Bundles\Solution\Exceptions\SolutionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetSolutionCommand extends AbstractSolutionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $solutionId = $request->getAttribute('solutionId');

        try {
            $entity = $this->service->getById($solutionId);

            $responseBuilder
                ->setJSON([
                    'solution' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(SolutionNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}