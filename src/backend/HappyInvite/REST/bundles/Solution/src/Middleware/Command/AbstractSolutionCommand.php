<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionFormatter;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatter;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionRepositoryService;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Facade\SolutionIndexQueryFacade;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Solution\ParametersFactory\SolutionParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractSolutionCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var SolutionParametersFactory */
    protected $parametersFactory;

    /** @var SolutionFormatter */
    protected $formatter;

    /** @var SolutionService */
    protected $service;

    /** @var SolutionIndexQueryFacade */
    protected $solutionIndexQueryFacade;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        SolutionParametersFactory $parametersFactory,
        SolutionFormatter $formatter,
        SolutionService $service,
        SolutionIndexQueryFacade $solutionIndexQueryFacade
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
        $this->solutionIndexQueryFacade = $solutionIndexQueryFacade;
    }
}