<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateSolutionCommand extends AbstractSolutionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->parametersFactory->factoryCreateParameters($request);

        $entity = $this->service->createSolution($parameters);

        $responseBuilder
            ->setJSON([
                'solution' => $this->formatter->formatOne($entity)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}