<?php
namespace HappyInvite\REST\Bundles\Solution\Middleware\Command;

use HappyInvite\Domain\Bundles\Solution\Exceptions\SolutionNotFoundException;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditSolutionCommand extends AbstractSolutionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $solutionId = $request->getAttribute('solutionId');

        try {
            $solution = $this->service->getById($solutionId);

            if(! $this->accessService->isAdmin()) {
                if($solution->isSolutionUserSpecified() /* Don't! It's pretty strange behaviour if we hot this case: && $solution->hasSolutionComponentOwner() */) {
                    $this->accessService->requireOwner($solution->getSolutionComponentOwner()->getId());
                }else{
                    throw new AccessDeniedException(sprintf("You're not allowed to edit solution `%s`", $solutionId));
                }
            }

            $parameters = $this->parametersFactory->factoryEditParameters($solutionId, $request);

            $entity = $this->service->editSolution($solutionId, $parameters);

            $responseBuilder
                ->setJSON([
                    'solution' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(SolutionNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}