<?php
namespace HappyInvite\REST\Bundles\Solution;

use HappyInvite\REST\Bundle\RESTBundle;

final class SolutionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }
}