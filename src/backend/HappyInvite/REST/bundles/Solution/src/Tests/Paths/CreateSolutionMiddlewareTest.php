<?php
namespace HappyInvite\REST\Bundles\Solution\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;
use HappyInvite\REST\Bundles\Solution\Tests\SolutionMiddlewareTestCase;

final class CreateSolutionMiddlewareTest extends SolutionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'invite_card_template_ids' => [InviteCardTemplateFixture::$templateRed->getId()],
            'envelope_template_ids' => [EnvelopeTemplateFixture::$envelopeTemplateEF1->getId()],
            'event_landing_template_ids' => [EventLandingTemplateFixture::$eventLandingTemplateEF1->getId()],
        ];
    }

    public function test200Global()
    {
        $json = $this->getTestJSON();

        $id = $this->requestSolutionCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'solution' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'solution' => [
                            'is_user_specified' => false,
                            'owner' => [
                                'has' => false,
                            ]
                        ],
                        'invite_card_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['invite_card_template_ids'][0],
                                ]
                            ]
                        ],
                        'envelope_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['envelope_template_ids'][0],
                                ]
                            ]
                        ],
                        'landing_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['event_landing_template_ids'][0],
                                ]
                            ]
                        ],
                        'target' => [
                            'type' => 'global',
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['solution']['entity']['id'];
            });

        $this->requestSolutionGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test200Target()
    {
        $json = array_merge($this->getTestJSON(), [
            'target' => UserAccountFixture::getProfile()->getId(),
        ]);

        $id = $this->requestSolutionCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'solution' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'solution' => [
                            'is_user_specified' => false,
                            'owner' => [
                                'has' => false,
                            ]
                        ],
                        'target' => [
                            'type' => 'profile',
                            'profile' => [
                                'id' => $json['target']
                            ],
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['solution']['entity']['id'];
            });

        $this->requestSolutionGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->requestSolutionCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestSolutionCreate($this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}