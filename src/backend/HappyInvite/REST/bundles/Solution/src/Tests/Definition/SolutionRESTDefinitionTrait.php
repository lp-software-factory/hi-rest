<?php
namespace HappyInvite\REST\Bundles\Solution\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait SolutionRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestSolutionCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/solution/create')
            ->setParameters($json);
    }

    protected function requestSolutionEdit(int $solutionId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/solution/%d/edit', $solutionId))
            ->setParameters($json);
    }

    protected function requestSolutionDelete(int $solutionId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/solution/%d/delete', $solutionId));
    }

    protected function requestSolutionGetById(int $solutionId): RESTRequest
    {
        return $this->request('GET', sprintf('/solution/%d/get', $solutionId));
    }
}