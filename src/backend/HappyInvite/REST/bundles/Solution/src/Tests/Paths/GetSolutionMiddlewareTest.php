<?php
namespace HappyInvite\REST\Bundles\Solution\Tests\Paths;

use HappyInvite\REST\Bundles\Solution\Tests\SolutionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Solution\Tests\Fixture\SolutionFixture;

final class GetSolutionMiddlewareTest extends SolutionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new SolutionFixture());

        $fixture = SolutionFixture::$solutionEF1;

        $this->requestSolutionGetById($fixture->getId())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'solution' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new SolutionFixture());

        $this->requestSolutionGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}