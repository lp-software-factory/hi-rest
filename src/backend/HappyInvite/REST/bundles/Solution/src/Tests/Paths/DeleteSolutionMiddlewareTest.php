<?php
namespace HappyInvite\REST\Bundles\Solution\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Solution\Tests\SolutionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Solution\Tests\Fixture\SolutionFixture;

final class DeleteSolutionMiddlewareTest extends SolutionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new SolutionFixture());

        $id = SolutionFixture::$solutionEF1->getId();

        $this->requestSolutionGetById($id)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestSolutionDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestSolutionGetById($id)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new SolutionFixture());

        $id = SolutionFixture::$solutionEF1->getId();

        $this->requestSolutionDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestSolutionDelete($id)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new SolutionFixture());

        $this->requestSolutionDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}