<?php
namespace HappyInvite\REST\Bundles\Solution\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;
use HappyInvite\REST\Bundles\Solution\Tests\SolutionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Solution\Tests\Fixture\SolutionFixture;

final class EditSolutionMiddlewareTest extends SolutionMiddlewareTestCase
{
    private function getTestCreateJSON(): array
    {
        return [
            'invite_card_template_ids' => [InviteCardTemplateFixture::$templateRed->getId()],
            'envelope_template_ids' => [EnvelopeTemplateFixture::$envelopeTemplateEF1->getId()],
            'event_landing_template_ids' => [EventLandingTemplateFixture::$eventLandingTemplateEF1->getId()],
        ];
    }

    private function getTestEditJSON(): array
    {
        return [
            'invite_card_template_ids' => [InviteCardTemplateFixture::$templateBlue->getId()],
            'envelope_template_ids' => [EnvelopeTemplateFixture::$envelopeTemplateEF2->getId(), EnvelopeTemplateFixture::$envelopeTemplateEF3->getId()],
            'event_landing_template_ids' => [EventLandingTemplateFixture::$eventLandingTemplateEF2->getId()],
        ];
    }

    public function test200Global()
    {
        $this->upFixture(new SolutionFixture());

        $json = $this->getTestCreateJSON();

        $id = $this->requestSolutionCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'solution' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'solution' => [
                            'is_user_specified' => false,
                            'owner' => [
                                'has' => false,
                            ]
                        ],
                        'invite_card_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['invite_card_template_ids'][0],
                                ]
                            ]
                        ],
                        'envelope_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['envelope_template_ids'][0],
                                ]
                            ]
                        ],
                        'landing_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['event_landing_template_ids'][0],
                                ]
                            ]
                        ],
                        'target' => [
                            'type' => 'global',
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['solution']['entity']['id'];
            });

        $json = $this->getTestEditJSON();

        $this->requestSolutionEdit($id, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'solution' => [
                    'entity' => [
                        'id' => $id,
                        'invite_card_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['invite_card_template_ids'][0],
                                ]
                            ]
                        ],
                        'envelope_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['envelope_template_ids'][0],
                                ]
                            ],
                            1 => [
                                'entity' => [
                                    'id' => $json['envelope_template_ids'][1],
                                ]
                            ]
                        ],
                        'landing_templates' => [
                            0 => [
                                'entity' => [
                                    'id' => $json['event_landing_template_ids'][0],
                                ]
                            ]
                        ],
                        'target' => [
                            'type' => 'global',
                        ]
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new SolutionFixture());

        $fixture = SolutionFixture::$solutionEF1;

        $this->requestSolutionEdit($fixture->getId(), $this->getTestEditJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestSolutionEdit($fixture->getId(), $this->getTestEditJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new SolutionFixture());

        $this->requestSolutionEdit(self::NOT_FOUND_ID, $this->getTestEditJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}