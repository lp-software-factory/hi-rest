<?php
namespace HappyInvite\REST\Bundles\Solution\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Parameters\CreateSolutionParameters;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Fixture\EventLandingTemplateFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class SolutionFixture implements Fixture
{
    /** @var Solution */
    public static $solutionEF1;

    /** @var Solution */
    public static $solutionEF2;

    /** @var Solution */
    public static $solutionEF3;

    /** @var Solution */
    public static $solutionET3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(SolutionService::class); /** @var SolutionService $service */

        self::$solutionEF1 = $service->createSolution(new CreateSolutionParameters(
            [InviteCardTemplateFixture::$templateBlue],
            [EnvelopeTemplateFixture::$envelopeTemplateEF1],
            [EventLandingTemplateFixture::$eventLandingTemplateEF1]
        ));

        self::$solutionEF2 = $service->createSolution(new CreateSolutionParameters(
            [InviteCardTemplateFixture::$templateRed],
            [EnvelopeTemplateFixture::$envelopeTemplateEF2],
            [EventLandingTemplateFixture::$eventLandingTemplateEF2]
        ));

        self::$solutionEF3 = $service->createSolution(new CreateSolutionParameters(
            [InviteCardTemplateFixture::$templateGreen],
            [EnvelopeTemplateFixture::$envelopeTemplateEF3],
            [EventLandingTemplateFixture::$eventLandingTemplateEF3]
        ));

        /** @noinspection PhpParamsInspection */
        self::$solutionET3 = $service->createSolution((new CreateSolutionParameters(
            [InviteCardTemplateFixture::$templateGreen],
            [EnvelopeTemplateFixture::$envelopeTemplateEF3],
            [EventLandingTemplateFixture::$eventLandingTemplateEF3]
        ))->assignTo(UserAccountFixture::getProfile()));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$solutionEF1,
            self::$solutionEF2,
            self::$solutionEF3,
            self::$solutionET3,
        ];
    }
}