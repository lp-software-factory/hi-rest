<?php
namespace HappyInvite\REST\Bundles\PHPUnit;

use HappyInvite\REST\Bundle\RESTBundle;
use HappyInvite\REST\Bundles\PHPUnit\InitScripts\PHPUnitInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScriptInjectableBundle;

class PHPUnitBundle extends RESTBundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getInitScripts(): array
    {
        return [
            new PHPUnitInitScript(),
        ];
    }
}