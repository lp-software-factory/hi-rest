<?php
namespace HappyInvite\REST\Bundles\PHPUnit\RESTRequest;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Result
{
    /**
     * HTTP-запрос, который был отправлен
     * @var ServerRequestInterface
     */
    private $httpRequest;

    /**
     * HTTP-ответ, полученный от бэкенда
     * @var ResponseInterface
     */
    private $httpResponse;

    /**
     * JSON Body
     * @var array
     */
    private $content;

    public function __construct(ServerRequestInterface $httpRequest, ResponseInterface $httpResponse, array $content)
    {
        $this->httpRequest = $httpRequest;
        $this->httpResponse = $httpResponse;
        $this->content = $content;
    }

    public function getHttpRequest(): ServerRequestInterface
    {
        return $this->httpRequest;
    }

    public function getHttpResponse(): ResponseInterface
    {
        return $this->httpResponse;
    }

    public function getContent(): array
    {
        return $this->content;
    }
}