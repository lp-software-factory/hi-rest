<?php
namespace HappyInvite\REST\Bundles\PHPUnit\TestCase;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\DoctrineORM\Service\DoctrineORMTransactionService;
use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\Domain\Bundles\Mandrill\Service\MandrillService;
use HappyInvite\REST\Bundles\Account\Tests\Definition\AccountRESTDefinitionTrait;
use HappyInvite\REST\Bundles\AddressBook\Bundles\Contact\Tests\Definition\AddressBookContactRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Attachment\Tests\Definition\AttachmentRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Auth\Tests\REST\Definition\AuthRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Company\Tests\Definition\CompanyRESTDefinitionTrait;
use HappyInvite\REST\Bundles\CompanyProfile\Tests\Definition\CompanyProfileRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Designer\Tests\Definition\DesignerRESTDefinitionTrait;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Definition\DesignerRequestsRESTDefinition;
use HappyInvite\REST\Bundles\DressCode\Tests\Definition\DressCodeRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Definition\EnvelopeBackdroprRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Definition\EnvelopeBackdropDefinitionRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Definition\EnvelopeColorRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Definition\EnvelopeMarkDefinitionRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Definition\EnvelopeMarksRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Definition\EnvelopePatternRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Definition\EnvelopePatternDefinitionRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Definition\EnvelopeTemplateRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Definition\EnvelopeTextureRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Definition\EnvelopeTextureDefinitionRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Event\Bundles\Event\Tests\Definition\EventRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Event\Bundles\EventLandingTemplate\Tests\Definition\EventLandingTemplateRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Event\Bundles\EventMailingOptions\Tests\Definition\EventMailingOptionsRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Event\Bundles\EventRecipients\Tests\Definition\EventRecipientsRESTDefinitionTrait;
use HappyInvite\REST\Bundles\EventType\Tests\Definition\EventTypeRESTDefinitionTrait;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Definition\EventTypeGroupRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Fonts\Tests\Definition\FontsRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Frontend\Tests\Definition\FrontendRESTDefinitionTrait;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Definition\InviteCardSizeRESTDefinitionTrait;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Definition\InviteCardColorRESTDefinitionTrait;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Definition\InviteCardFamilyRESTDefinitionTrait;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Definition\InviteCardGammaRESTDefinitionTrait;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Definition\InviteCardStyleRESTDefinitionTrait;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Definition\InviteCardTemplateRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Landing\Tests\Definition\LandingRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Locale\Tests\Definition\LocaleRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Money\Tests\Definition\MoneyRESTDefinitionTrait;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\Definition\OAuth2ProvidersRESTDefinitionTrait;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use HappyInvite\REST\Bundles\Product\Tests\Definition\ProductPackagesRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Product\Tests\Definition\ProductRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Solution\Tests\Definition\SolutionRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Translation\Tests\Definition\TranslationRESTDefinitionTrait;
use HappyInvite\REST\Bundles\Version\Tests\Definition\VersionRESTDefinitionTrait;

abstract class HappyInviteMiddlewareTestCase extends MiddlewareTestCase
{
    use AccountRESTDefinitionTrait, FrontendRESTDefinitionTrait, AddressBookContactRESTDefinitionTrait,
        AttachmentRESTDefinitionTrait, AuthRESTDefinitionTrait, CompanyRESTDefinitionTrait,
        CompanyProfileRESTDefinitionTrait, DesignerRESTDefinitionTrait, DesignerRequestsRESTDefinition,
        DressCodeRESTDefinitionTrait, EnvelopeBackdroprRESTDefinitionTrait, EnvelopeBackdropDefinitionRESTDefinitionTrait,
        EnvelopeColorRESTDefinitionTrait, EnvelopeMarkDefinitionRESTDefinitionTrait, EnvelopeMarksRESTDefinitionTrait,
        EnvelopePatternRESTDefinitionTrait, EnvelopePatternDefinitionRESTDefinitionTrait, EnvelopeTemplateRESTDefinitionTrait,
        EnvelopeTextureRESTDefinitionTrait, EnvelopeTextureDefinitionRESTDefinitionTrait, EventRESTDefinitionTrait,
        EventLandingTemplateRESTDefinitionTrait, EventMailingOptionsRESTDefinitionTrait, EventRecipientsRESTDefinitionTrait,
        EventTypeRESTDefinitionTrait, EventTypeGroupRESTDefinitionTrait, FontsRESTDefinitionTrait, InviteCardSizeRESTDefinitionTrait,
        InviteCardColorRESTDefinitionTrait, InviteCardFamilyRESTDefinitionTrait, InviteCardGammaRESTDefinitionTrait,
        InviteCardStyleRESTDefinitionTrait, InviteCardTemplateRESTDefinitionTrait, LandingRESTDefinitionTrait,
        LocaleRESTDefinitionTrait, MoneyRESTDefinitionTrait, OAuth2ProvidersRESTDefinitionTrait, ProductRESTDefinitionTrait,
        ProductPackagesRESTDefinitionTrait, SolutionRESTDefinitionTrait, TranslationRESTDefinitionTrait, VersionRESTDefinitionTrait;

    /**
     * Фикстуры
     * Возвращает массив фикстур, которые применяются к каждому юнит-тесту
     *
     * Массив должен быть пустым, либо содержать набор объектов классов,
     *  имплементирующие интерфейс ZEA2\Platform\Bundles\PHPUnit\Fixtures
     *
     * Каждая фикстура содержит в себе статическую переменную, в которой хранятся объект/записи, созданные ей
     *
     * @return array
     */
    protected abstract function getFixtures(): array;

    /**
     * При выполнении каждого юнит-теста происходят следующие действия:
     *
     *  1. Стартует транзакция
     *  2. Применяются фикстуры
     *  3. Инжектится SchemaService в SchemaParams
     *
     * @throws \DI\NotFoundException
     */
    protected function setUp()
    {
        $authToken = $this->container()->get(AuthToken::class); /** @var AuthToken $authToken */
        $authToken->signOut();

        $transactionService = $this->container()->get(DoctrineORMTransactionService::class); /** @var DoctrineORMTransactionService $transactionService */
        $transactionService->beginTransaction();

        $mandrillService = $this->container()->get(MandrillService::class); /** @var MandrillService $mandrillService */
        $mandrillService->enableSyncMode();

        $spool = $this->container()->get(PHPUnitMemorySpool::class); /** @var PHPUnitMemorySpool $spool */
        $spool->clear();

        $app = $this->app();
        $em = $this->container()->get(EntityManager::class);

        array_map(function(Fixture $fixture) use ($app, $em) {
            $this->upFixture($fixture);
        }, $this->getFixtures());
    }

    /**
     * После завершения каждого юнит-теста происходит роллбак транзакции и удаляются MongoDB-записи
     *
     * @throws \DI\NotFoundException
     */
    protected function tearDown()
    {
        $transactionService = $this->container()->get(DoctrineORMTransactionService::class); /** @var DoctrineORMTransactionService $transactionService */
        $transactionService->rollback();
    }

    /**
     * Поднимает указанную фикстуру
     *
     * @param Fixture $fixture
     * @return HappyInviteMiddlewareTestCase
     */
    protected function upFixture(Fixture $fixture): self
    {
        $app = $this->app();
        $em = $this->container()->get(EntityManager::class);
        $fixture->up($app, $em);

        return $this;
    }

    /**
     * Поднимает все фикстуры
     *
     * @param Fixture[] $fixtures
     * @return HappyInviteMiddlewareTestCase
     */
    protected function upFixtures(array $fixtures): self
    {
        foreach($fixtures as $fixture) {
            $this->upFixture($fixture);
        }

        return $this;
    }
}