<?php
namespace HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\Traits;

use HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectArray;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectDate;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectId;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectImageCollection;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectString;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations\ExpectUndefined;

trait AllExpectationsTrait
{
    public function expectId():  ExpectId
    {
        return new ExpectId();
    }

    public function expectString(): ExpectString
    {
        return new ExpectString();
    }

    public function expectDate(): ExpectDate
    {
        return new ExpectDate();
    }

    public function expectUndefined(): ExpectUndefined
    {
        return new ExpectUndefined();
    }

    public function expectImageCollection(): ExpectImageCollection
    {
        return new ExpectImageCollection();
    }

    public function expectArray(): ExpectArray
    {
        return new ExpectArray();
    }
}