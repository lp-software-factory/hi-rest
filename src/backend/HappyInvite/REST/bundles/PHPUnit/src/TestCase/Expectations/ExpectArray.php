<?php
namespace HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations;

class ExpectArray implements Expectation
{
    public function __toString(): string
    {
        return "{{ARRAY}}";
    }

    public function expect(ExpectationParams $params)
    {
        $case = $params->getCase();
        $key = $params->getKey();
        $actual = $params->getActual();

        $case->assertArrayHasKey($key, $actual);
        $case->assertTrue(is_array($actual[$key]));
    }
}