<?php
namespace HappyInvite\REST\Bundles\PHPUnit\TestCase\Expectations;

interface Expectation
{
    public function __toString(): string;
    public function expect(ExpectationParams $params);
}