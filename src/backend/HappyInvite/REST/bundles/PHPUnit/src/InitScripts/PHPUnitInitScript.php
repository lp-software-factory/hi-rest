<?php
namespace HappyInvite\REST\Bundles\PHPUnit\InitScripts;

use HappyInvite\REST\Bundles\PHPUnit\TestCase\MiddlewareTestCase;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class PHPUnitInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        MiddlewareTestCase::$app = $application;
    }
}