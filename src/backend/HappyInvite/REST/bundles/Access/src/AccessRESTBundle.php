<?php
namespace HappyInvite\REST\Bundles\Access;

use HappyInvite\REST\Bundle\RESTBundle;

final class AccessRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}