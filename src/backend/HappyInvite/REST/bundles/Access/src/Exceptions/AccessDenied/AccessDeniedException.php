<?php
namespace HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied;

use HappyInvite\Platform\Bundles\ZE\StatusExceptions\StatusException403;

class AccessDeniedException extends \Exception implements StatusException403 {}