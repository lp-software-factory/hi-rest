<?php
namespace HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied;

final class RouteIsProtectedException extends AccessDeniedException {}