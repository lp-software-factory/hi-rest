<?php
namespace HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied;

final class RouteIsAdminProtectedException extends AccessDeniedException {}