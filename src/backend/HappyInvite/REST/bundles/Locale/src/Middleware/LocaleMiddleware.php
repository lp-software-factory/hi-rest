<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware;

use HappyInvite\REST\Bundles\Locale\Middleware\Command\CreateLocaleCommand;
use HappyInvite\REST\Bundles\Locale\Middleware\Command\DeleteLocaleCommand;
use HappyInvite\REST\Bundles\Locale\Middleware\Command\GetLocaleCommand;
use HappyInvite\REST\Bundles\Locale\Middleware\Command\ListLocalesCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class LocaleMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateLocaleCommand::class)
            ->attachDirect('delete', DeleteLocaleCommand::class)
            ->attachDirect('get', GetLocaleCommand::class)
            ->attachDirect('list', ListLocalesCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}