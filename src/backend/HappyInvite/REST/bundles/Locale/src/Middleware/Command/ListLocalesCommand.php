<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware\Command;

use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListLocalesCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $responseBuilder
            ->setJSON([
                'locales' => array_map(function(Locale $locale) {
                    return $locale->toJSON();
                }, $this->localeService->getAllLocales())
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}