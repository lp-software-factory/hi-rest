<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware;

use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class CurrentLocalePipe implements MiddlewareInterface
{
    /** @var LocaleService */
    private $localeService;

    public function __construct(LocaleService $localeService)
    {
        $this->localeService = $localeService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        if($request->hasHeader('Accept-Language')) {
            $acceptLanguage = $request->getHeader('Accept-Language')[0];

            $language = strtolower(substr($acceptLanguage, 0, 2));
            $region = strtoupper(str_replace('-', '_', $acceptLanguage));

            if($this->localeService->hasLocale($language, $region)) {
                $this->localeService->setCurrentLocale(
                    $this->localeService->getLocaleByRegion($region)
                );
            }
        }

        return $out($request, $response);
    }
}