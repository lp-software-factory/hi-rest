<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware\Command;

use HappyInvite\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $locale = $this->localeService->getLocaleById($request->getAttribute('localeId'));

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        } catch(LocaleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}