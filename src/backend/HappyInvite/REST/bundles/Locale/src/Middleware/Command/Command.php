<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware\Command;

use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;

abstract class Command implements \HappyInvite\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var LocaleService */
    protected $localeService;

    public function __construct(AccessService $accessService, LocaleService $localeService)
    {
        $this->accessService = $accessService;
        $this->localeService = $localeService;
    }
}