<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware\Command;

use HappyInvite\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->localeService->deleteLocale($request->getAttribute('localeId'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(LocaleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}