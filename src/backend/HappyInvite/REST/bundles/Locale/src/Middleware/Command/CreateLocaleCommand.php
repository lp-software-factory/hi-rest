<?php
namespace HappyInvite\REST\Bundles\Locale\Middleware\Command;

use HappyInvite\Domain\Bundles\Locale\Exceptions\DuplicateLocaleException;
use HappyInvite\REST\Bundles\Locale\Request\CreateLocaleRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateLocaleCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new CreateLocaleRequest($request))->getParameters();
            $locale = $this->localeService->createLocale($parameters);

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DuplicateLocaleException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}