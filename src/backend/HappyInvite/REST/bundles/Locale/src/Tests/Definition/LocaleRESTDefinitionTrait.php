<?php
namespace HappyInvite\REST\Bundles\Locale\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait LocaleRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestLocaleCreate(array $json): RESTRequest
    {
        return $this->request('put', '/locale/create')
            ->setParameters($json);
    }

    protected function requestLocaleDelete(int $localeId): RESTRequest
    {
        return $this->request('delete', sprintf('/locale/%d/delete', $localeId));
    }

    protected function requestLocaleGet(int $localeId): RESTRequest
    {
        return $this->request('get', sprintf('/locale/%d/get', $localeId));
    }

    protected function requestLocaleList(): RESTRequest
    {
        return $this->request('get', '/locale/list');
    }
}