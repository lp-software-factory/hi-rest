<?php
namespace HappyInvite\REST\Bundles\Locale\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class LocaleFixture implements Fixture
{
    private static $LOCALE_EN;
    private static $LOCALE_RU;

    public function up(Application $app, EntityManager $em)
    {
        $localeService = $app->getContainer()->get(LocaleService::class); /** @var LocaleService $localeService */

        self::$LOCALE_EN = $localeService->createLocale(new CreateLocaleParameters('en', 'en_GB'));
        self::$LOCALE_RU = $localeService->createLocale(new CreateLocaleParameters('ru', 'ru_RU'));

        $localeService->setCurrentLocale(self::$LOCALE_RU);
    }

    public static function getLocaleEN(): Locale
    {
        return self::$LOCALE_EN;
    }

    public static function getLocaleRU(): Locale
    {
        return self::$LOCALE_RU;
    }
}
