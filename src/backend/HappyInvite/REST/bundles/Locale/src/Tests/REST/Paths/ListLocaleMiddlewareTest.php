<?php
namespace HappyInvite\REST\Bundles\Locale\Tests\REST\Paths;

use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\Locale\Tests\LocaleMiddlewareTestCase;

final class ListLocaleMiddlewareTest extends LocaleMiddlewareTestCase
{
    public function test200()
    {
        $this->requestLocaleList()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locales' => [
                    LocaleFixture::getLocaleEN()->toJSON(),
                    LocaleFixture::getLocaleRU()->toJSON(),
                ]
            ])
            ->expect(function(array $json) {
                return count($json['locales']) === 2;
            });
    }
}