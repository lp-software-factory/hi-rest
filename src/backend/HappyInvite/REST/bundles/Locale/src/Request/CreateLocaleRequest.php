<?php
namespace HappyInvite\REST\Bundles\Locale\Request;

use HappyInvite\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateLocaleRequest extends SchemaRequest
{
    public function getParameters(): CreateLocaleParameters
    {
        $data = $this->getData();

        return new CreateLocaleParameters(
            $data['language'],
            $data['region']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_LocaleBundle_CreateLocaleRequest');
    }
}