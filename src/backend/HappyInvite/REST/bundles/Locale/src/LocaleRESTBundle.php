<?php
namespace HappyInvite\REST\Bundles\Locale;

use HappyInvite\REST\Bundle\RESTBundle;

final class LocaleRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}