<?php
namespace HappyInvite\REST\Bundles\Locale;

use HappyInvite\REST\Bundles\Locale\Middleware\CurrentLocalePipe;
use HappyInvite\REST\Bundles\Locale\Middleware\LocaleMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'pipe',
                'path' => '/',
                'group' => 'locale',
                'middleware' => CurrentLocalePipe::class,
            ],
            [
                'method' => 'put',
                'path' => '/locale/{command:create}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/locale/{localeId}/{command:delete}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/locale/{localeId}/{command:get}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/locale/{command:list}[/]',
                'middleware' => LocaleMiddleware::class,
            ],
        ],
    ],
];