<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Company\Tests\CompanyMiddlewareTest;
use HappyInvite\REST\Bundles\Company\Tests\Fixtures\CompaniesFixture;

final class EditCompanyMiddlewareTest extends CompanyMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new CompaniesFixture());

        $json = [
            'name' => '* new name'
        ];

        $this->requestCompanyEdit(CompaniesFixture::get(1)->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestCompanyEdit(CompaniesFixture::get(1)->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new CompaniesFixture());

        $json = [
            'name' => '* new name'
        ];

        $this->requestCompanyEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new CompaniesFixture());

        $company = CompaniesFixture::get(1);
        $json = [
            'name' => '* new name'
        ];

        $this->requestCompanyEdit($company->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);


        $this->requestCompanyGetById($company->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'company' => [
                    'id' => $company->getId(),
                    'name' => $json['name'],
                ]
            ])
        ;
    }
}