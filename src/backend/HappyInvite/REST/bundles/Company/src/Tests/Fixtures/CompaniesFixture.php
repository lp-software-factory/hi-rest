<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Fixtures;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Parameters\CreateCompanyParameters;
use HappyInvite\Domain\Bundles\Company\Service\CompanyService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class CompaniesFixture implements Fixture
{
    private static $companies = [
    ];

    public function up(Application $app, EntityManager $em)
    {
        self::$companies = [];

        $service = $app->getContainer()->get(CompanyService::class);

        foreach(['Company 1', 'Company 2', 'Company 3', 'Company 4', 'Company 5'] as $index => $company) {
            self::$companies[$index+1] = $service->createCompany(new CreateCompanyParameters($company));
        }
    }

    public static function get(int $index): Company
    {
        return self::$companies[$index];
    }

    public static function getAll(): array
    {
        return self::$companies;
    }
}