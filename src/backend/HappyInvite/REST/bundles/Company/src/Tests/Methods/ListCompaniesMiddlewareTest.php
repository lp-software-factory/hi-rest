<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Company\Tests\CompanyMiddlewareTest;
use HappyInvite\REST\Bundles\Company\Tests\Fixtures\CompaniesFixture;

final class ListCompaniesMiddlewareTest extends CompanyMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyList(0, 5)
            ->__invoke()
            ->expectAuthError();

        $this->requestCompanyList(0, 5)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyList(0, 5)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 0,
                        'limit' => 5,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'companies' => [
                    ['id' => CompaniesFixture::get(5)->getId()],
                    ['id' => CompaniesFixture::get(4)->getId()],
                    ['id' => CompaniesFixture::get(3)->getId()],
                    ['id' => CompaniesFixture::get(2)->getId()],
                    ['id' => CompaniesFixture::get(1)->getId()],
                ],
                'total' => 5,
            ])
            ->expect(function(array $json) {
                $this->assertEquals(5, count($json['companies']));
            })
        ;

        $this->requestCompanyList(2, 3)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 2,
                        'limit' => 3,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'companies' => [
                    ['id' => CompaniesFixture::get(3)->getId()],
                    ['id' => CompaniesFixture::get(2)->getId()],
                    ['id' => CompaniesFixture::get(1)->getId()],
                ],
                'total' => 5
            ])
            ->expect(function(array $json) {
                $this->assertEquals(3, count($json['companies']));
            })
        ;

        $this->requestCompanyList(1, 1)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 1,
                        'limit' => 1,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'companies' => [
                    ['id' => CompaniesFixture::get(4)->getId()],
                ],
                'total' => 5
            ])
            ->expect(function(array $json) {
                $this->assertEquals(1, count($json['companies']));
            })
        ;
    }

    public function test200Sort()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyList(0, 5, ['sort' => 'id', 'order' => 'asc'])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 0,
                        'limit' => 5,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'asc'
                    ]
                ],
                'companies' => [
                    ['id' => CompaniesFixture::get(1)->getId()],
                    ['id' => CompaniesFixture::get(2)->getId()],
                    ['id' => CompaniesFixture::get(3)->getId()],
                    ['id' => CompaniesFixture::get(4)->getId()],
                    ['id' => CompaniesFixture::get(5)->getId()],
                ],
                'total' => 5
            ])
            ->expect(function(array $json) {
                $this->assertEquals(5, count($json['companies']));
            })
        ;

        $this->requestCompanyList(0, 5, ['sort' => 'id', 'order' => 'desc'])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 0,
                        'limit' => 5,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'companies' => [
                    ['id' => CompaniesFixture::get(5)->getId()],
                    ['id' => CompaniesFixture::get(4)->getId()],
                    ['id' => CompaniesFixture::get(3)->getId()],
                    ['id' => CompaniesFixture::get(2)->getId()],
                    ['id' => CompaniesFixture::get(1)->getId()],
                ],
                'total' => 5
            ])
            ->expect(function(array $json) {
                $this->assertEquals(5, count($json['companies']));
            })
        ;
    }
}