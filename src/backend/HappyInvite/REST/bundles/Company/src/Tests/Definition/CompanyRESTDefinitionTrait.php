<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait CompanyRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestCompanyCreate(array $json): RESTRequest
    {
        return $this->request('put', '/company/create')
            ->setParameters($json);
    }

    protected function requestCompanyDelete(int $companyId): RESTRequest
    {
        return $this->request('delete', sprintf('/company/%d/delete', $companyId));
    }

    protected function requestCompanyEdit(int $companyId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/company/%d/edit', $companyId))
            ->setParameters($json);
    }

    protected function requestCompanyGetById(int $companyId): RESTRequest
    {
        return $this->request('get', sprintf('/company/%d/get', $companyId));
    }

    protected function requestCompanyGetByIds(array $companyIds): RESTRequest
    {
        return $this->request('get', sprintf('/company/%s/get-many', join(',', $companyIds)));
    }

    protected function requestCompanyList(int $offset, int $limit, array $qp = null): RESTRequest
    {
        return $this->request('get', sprintf('/company/list/offset/%d/limit/%d', $offset, $limit), $qp);
    }
}