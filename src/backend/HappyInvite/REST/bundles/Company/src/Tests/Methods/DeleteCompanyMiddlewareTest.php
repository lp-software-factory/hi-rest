<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Company\Tests\CompanyMiddlewareTest;
use HappyInvite\REST\Bundles\Company\Tests\Fixtures\CompaniesFixture;

final class DeleteCompanyMiddlewareTest extends CompanyMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyDelete(CompaniesFixture::get(1)->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestCompanyDelete(CompaniesFixture::get(1)->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new CompaniesFixture());

        $company = CompaniesFixture::get(1);
        $companyId = $company->getId();

        $this->requestCompanyGetById($companyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'company' => [
                    'id' => $companyId,
                ]
            ])
        ;

        $this->requestCompanyDelete($companyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestCompanyGetById($companyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError()
        ;
    }
}