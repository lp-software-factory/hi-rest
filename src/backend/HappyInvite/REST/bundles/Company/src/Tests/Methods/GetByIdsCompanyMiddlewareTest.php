<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Company\Tests\CompanyMiddlewareTest;
use HappyInvite\REST\Bundles\Company\Tests\Fixtures\CompaniesFixture;

final class GetByIdsCompanyMiddlewareTest extends CompanyMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyGetByIds([
            CompaniesFixture::get(1)->getId(),
            CompaniesFixture::get(2)->getId(),
            CompaniesFixture::get(3)->getId(),
        ])
            ->__invoke()
            ->expectAuthError();

        $this->requestCompanyGetByIds([
            CompaniesFixture::get(1)->getId(),
            CompaniesFixture::get(2)->getId(),
            CompaniesFixture::get(3)->getId(),
        ])
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyGetByIds([self::NOT_FOUND_ID])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestCompanyGetByIds([CompaniesFixture::get(1)->getId(), self::NOT_FOUND_ID])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestCompanyGetByIds([self::NOT_FOUND_ID, CompaniesFixture::get(1)->getId()])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200Single()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyGetByIds([
            CompaniesFixture::get(1)->getId(),
        ])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'companies' => [
                    ['id' => CompaniesFixture::get(1)->getId()],
                ]
            ])
        ;
    }

    public function test200Many()
    {
        $this->upFixture(new CompaniesFixture());

        $this->requestCompanyGetByIds([
            CompaniesFixture::get(1)->getId(),
            CompaniesFixture::get(2)->getId(),
            CompaniesFixture::get(3)->getId(),
        ])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'companies' => [
                    ['id' => CompaniesFixture::get(1)->getId()],
                    ['id' => CompaniesFixture::get(2)->getId()],
                    ['id' => CompaniesFixture::get(3)->getId()],
                ]
            ])
        ;
    }
}