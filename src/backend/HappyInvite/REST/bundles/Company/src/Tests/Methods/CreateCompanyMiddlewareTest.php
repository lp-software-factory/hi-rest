<?php
namespace HappyInvite\REST\Bundles\Company\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Company\Tests\CompanyMiddlewareTest;

final class CreateCompanyMiddlewareTest extends CompanyMiddlewareTest
{
    public function test403()
    {
        $this->requestCompanyCreate(['name' => 'Demo Company'])
            ->__invoke()
            ->expectAuthError();

        $this->requestCompanyCreate(['name' => 'Demo Company'])
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = ['name' => 'Demo Company'];

        $this->requestCompanyCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'company' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'name' => $json['name'],
                ]
            ]);
    }
}