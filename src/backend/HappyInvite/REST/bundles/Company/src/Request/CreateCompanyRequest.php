<?php
namespace HappyInvite\REST\Bundles\Company\Request;

use HappyInvite\Domain\Bundles\Company\Parameters\CreateCompanyParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateCompanyRequest extends SchemaRequest
{
    public function getParameters(): CreateCompanyParameters
    {
        $json = $this->getData();

        return new CreateCompanyParameters($json['name']);
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_CompanyBundle_CreateCompanyRequest');
    }
}