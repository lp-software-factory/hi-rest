<?php
namespace HappyInvite\REST\Bundles\Company\Request;

use HappyInvite\Domain\Bundles\Company\Parameters\EditCompanyParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditCompanyRequest extends SchemaRequest
{
    public function getParameters(): EditCompanyParameters
    {
        $json = $this->getData();

        return new EditCompanyParameters($json['name']);
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_CompanyBundle_EditCompanyRequest');
    }
}