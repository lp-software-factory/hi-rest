<?php
namespace HappyInvite\REST\Bundles\Company;

use HappyInvite\REST\Bundle\RESTBundle;

final class CompanyRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}