<?php
namespace HappyInvite\REST\Bundles\Company\Middleware\Command;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Exception\CompaniesNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetByIdsCompaniesCommand extends Command
{
    public const MAX_ENTITIES = 1000;

    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $ids = Chain::create(explode(',', $request->getAttribute('companyIds')))
                ->filter(function($input) {
                    return is_numeric($input);
                })
                ->map(function($input) {
                    return (int) $input;
                })
                ->array;

            $result = $this->companyService->getCompaniesByIds(
                new IdsCriteria($ids, self::MAX_ENTITIES)
            );

            $responseBuilder
                ->setJSON([
                    'companies' => array_map(function(Company $company) {
                        return $company->toJSON();
                    }, $result)
                ])
                ->setStatusSuccess();
        }catch(CompaniesNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}