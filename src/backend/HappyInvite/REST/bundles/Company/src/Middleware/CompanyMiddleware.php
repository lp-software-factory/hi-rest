<?php
namespace HappyInvite\REST\Bundles\Company\Middleware;

use HappyInvite\REST\Bundles\Company\Middleware\Command\CreateCompanyCommand;
use HappyInvite\REST\Bundles\Company\Middleware\Command\DeleteCompanyCommand;
use HappyInvite\REST\Bundles\Company\Middleware\Command\EditCompanyCommand;
use HappyInvite\REST\Bundles\Company\Middleware\Command\GetByIdCompanyCommand;
use HappyInvite\REST\Bundles\Company\Middleware\Command\GetByIdsCompaniesCommand;
use HappyInvite\REST\Bundles\Company\Middleware\Command\ListCompaniesCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class CompanyMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateCompanyCommand::class)
            ->attachDirect('delete', DeleteCompanyCommand::class)
            ->attachDirect('edit', EditCompanyCommand::class)
            ->attachDirect('get', GetByIdCompanyCommand::class)
            ->attachDirect('get-many', GetByIdsCompaniesCommand::class)
            ->attachDirect('list', ListCompaniesCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}