<?php
namespace HappyInvite\REST\Bundles\Company\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Company\Service\CompanyService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;

abstract class Command implements \HappyInvite\REST\Command\Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var CompanyService */
    protected $companyService;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        CompanyService $companyService
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->companyService = $companyService;
    }

}