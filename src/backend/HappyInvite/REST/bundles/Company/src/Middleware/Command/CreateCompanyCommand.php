<?php
namespace HappyInvite\REST\Bundles\Company\Middleware\Command;

use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use HappyInvite\REST\Bundles\Company\Request\CreateCompanyRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateCompanyCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = (new CreateCompanyRequest($request))->getParameters();
        $company = $this->companyService->createCompany($parameters);

        $responseBuilder
            ->setJSON([
                'company' => $company->toJSON(),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}