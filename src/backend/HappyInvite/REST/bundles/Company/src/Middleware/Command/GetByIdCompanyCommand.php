<?php
namespace HappyInvite\REST\Bundles\Company\Middleware\Command;

use HappyInvite\Domain\Bundles\Company\Exception\CompanyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetByIdCompanyCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $company = $this->companyService->getCompanyById($request->getAttribute('companyId'));

            $responseBuilder
                ->setJSON([
                    'company' => $company->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(CompanyNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}