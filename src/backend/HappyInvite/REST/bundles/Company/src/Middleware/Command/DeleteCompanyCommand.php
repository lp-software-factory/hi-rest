<?php
namespace HappyInvite\REST\Bundles\Company\Middleware\Command;

use HappyInvite\Domain\Bundles\Company\Exception\CompanyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteCompanyCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->companyService->deleteCompany($request->getAttribute('companyId'));

            $responseBuilder->setStatusSuccess();
        }catch(CompanyNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}