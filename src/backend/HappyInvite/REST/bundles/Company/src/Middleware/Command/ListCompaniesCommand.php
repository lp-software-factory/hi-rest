<?php
namespace HappyInvite\REST\Bundles\Company\Middleware\Command;

use HappyInvite\Domain\Bundles\Company\Criteria\ListCompaniesCriteria;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListCompaniesCommand extends Command
{
    const MAX_ENTITIES = 1000;

    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $result = $this->companyService->listCompanies(
            $criteria = $this->fetchCriteria($request)
        );

        return $responseBuilder
            ->setJSON([
                'companies' => array_map(function(Company $company) {
                    return $company->toJSON();
                }, $result),
                'criteria' => $criteria->toJSON(),
                'total' => $this->companyService->countCompanies($criteria),
            ])
            ->setStatusSuccess()
            ->build();
    }

    private function fetchCriteria(ServerRequestInterface $request): ListCompaniesCriteria
    {
        $qp = $request->getQueryParams();

        $seekOffset = $request->getAttribute('offset');
        $seekLimit = $request->getAttribute('limit');

        $orderByField = $qp['sort'] ?? ListCompaniesCriteria::ORDER_BY_DEFAULT_FIELD;
        $orderByDirection = $qp['order'] ?? ListCompaniesCriteria::ORDER_BY_DEFAULT_DIRECTION;

        return new ListCompaniesCriteria(
            new SeekCriteria(self::MAX_ENTITIES, $seekOffset, $seekLimit),
            new SortCriteria($orderByField,  $orderByDirection)
        );
    }
}