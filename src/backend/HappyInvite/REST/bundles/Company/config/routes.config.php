<?php
namespace HappyInvite\REST\Bundles\Company;

use HappyInvite\REST\Bundles\Company\Middleware\CompanyMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/company/{command:create}[/]',
                'middleware' => CompanyMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/company/{companyId}/{command:delete}[/]',
                'middleware' => CompanyMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/company/{companyId}/{command:edit}[/]',
                'middleware' => CompanyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/company/{companyId}/{command:get}[/]',
                'middleware' => CompanyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/company/{companyIds}/{command:get-many}[/]',
                'middleware' => CompanyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/company/{command:list}/offset/{offset}/limit/{limit}[/]',
                'middleware' => CompanyMiddleware::class,
            ],
        ],
    ],
];