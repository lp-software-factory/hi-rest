<?php
namespace HappyInvite\REST\Bundles\Auth\Request;

use HappyInvite\Domain\Bundles\Auth\Parameters\SignUpParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SignUpRequest extends SchemaRequest
{
    public function getParameters(): SignUpParameters
    {
        $data = $this->getData();

        return new SignUpParameters(
            $data['email'],
            $data['password'],
            $data['first_name'],
            $data['last_name'],
            $data['phone']['has'] ? $data['phone']['value'] : null,
            $data['company']['has'],
            $data['company']['has'] ? $data['company']['value'] : null
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_AuthBundle_Request_SignUpRequest');
    }
}