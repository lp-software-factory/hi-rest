<?php
namespace HappyInvite\REST\Bundles\Auth\Request;

use HappyInvite\Domain\Bundles\Auth\Parameters\SignInParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SignInRequest extends SchemaRequest
{
    public function getParameters(): SignInParameters
    {
        $data = $this->getData();

        return new SignInParameters($data['email'], $data['password']);
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_AuthBundle_Request_SignInRequest');
    }
}