<?php
namespace HappyInvite\REST\Bundles\Auth\Exceptions;

final class UnknownProviderTypeException extends \Exception {}