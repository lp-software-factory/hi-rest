<?php
namespace HappyInvite\REST\Bundles\Auth;

use HappyInvite\REST\Bundle\RESTBundle;

final class AuthRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}