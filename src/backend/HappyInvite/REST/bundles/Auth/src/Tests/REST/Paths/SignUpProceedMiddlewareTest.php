<?php
namespace HappyInvite\REST\Bundles\Auth\Tests\REST\Paths;

use HappyInvite\Domain\Bundles\Auth\Entity\SignUpRequest;
use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Auth\Tests\AuthMiddlewareTestCase;

final class SignUpProceedMiddlewareTest extends AuthMiddlewareTestCase
{
    public function test200()
    {
        $json = [
            'email' => sprintf('%s@example.com', GenerateRandomString::generate(8)),
            'password' => '1234',
            'first_name' => "Root",
            'last_name' => "Admin",
            'phone' => [
                'has' => false,
            ],
            'company' => [
                'has' => true,
                'value' => 'My Company',
            ]
        ];

        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $this->assertEquals(0, $spool->count());

        $this->requestAuthSignUp($json)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        $this->assertEquals(1, $spool->count());

        preg_match('/token=([a-zA-Z0-90]+)/', $spool->getLastMessage()->getBody(), $matches);

        $this->assertTrue(is_string($token = $matches[1]));
        $this->assertTrue(strlen($token) === SignUpRequest::SIGN_UP_TOKEN_LENGTH);

        $token = $this->requestAuthSignUpProceed($token)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => [
                        'id' => $this->expectId(),
                        'email' => $json['email'],
                    ],
                    'profile' => [
                        'id' => $this->expectId(),
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['token'];
            });

        $this->requestAuthSignIn($json['email'], $json['password'])
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => $token['account'],
                    'profile' => $token['profile'],
                ]
            ]);
    }
}