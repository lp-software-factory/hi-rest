<?php
namespace HappyInvite\REST\Bundles\Auth\Tests\REST\Paths;

use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Auth\Tests\AuthMiddlewareTestCase;

final class SignUpMiddlewareTest extends AuthMiddlewareTestCase
{
    public function test409()
    {
        $json = [
            'email' => AdminAccountFixture::$EMAIL,
            'password' => '1234',
            'first_name' => "Root",
            'last_name' => "Admin",
            'phone' => [
                'has' => false,
            ],
            'company' => [
                'has' => true,
                'value' => 'My Company',
            ]
        ];

        $this->requestAuthSignUp($json)
            ->__invoke()
            ->expectStatusCode(409)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => false,
                'error' => $this->expectString(),
            ]);
    }

    public function test200()
    {
        $json = [
            'email' => sprintf('%s@example.com', GenerateRandomString::generate(8)),
            'password' => '1234',
            'first_name' => "Root",
            'last_name' => "Admin",
            'phone' => [
                'has' => false,
            ],
            'company' => [
                'has' => true,
                'value' => 'My Company',
            ]
        ];

        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $this->assertEquals(0, $spool->count());

        $this->requestAuthSignUp($json)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        $this->assertEquals(1, $spool->count());
    }

    public function test200ICanDoMoreThanOneSignUpRequests()
    {
        $json = [
            'email' => sprintf('%s@example.com', GenerateRandomString::generate(8)),
            'password' => '1234',
            'first_name' => "Root",
            'last_name' => "Admin",
            'phone' => [
                'has' => false,
            ],
            'company' => [
                'has' => true,
                'value' => 'My Company',
            ]
        ];

        for($i = 0; $i < 3; $i++) {
            $this->requestAuthSignUp($json)
                ->__invoke()
                ->expectStatusCode(200)
                ->expectJSONContentType()
                ->expectJSONBody([
                    'success' => true
                ]);
        }
    }
}