<?php
namespace HappyInvite\REST\Bundles\Auth\Tests\REST\Paths;

use HappyInvite\Domain\Bundles\Auth\Service\JWTTokenService;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Auth\Tests\AuthMiddlewareTestCase;

final class SignInMiddlewareTest extends AuthMiddlewareTestCase
{
    public function test403()
    {
        $invalidPasswords = [
            '',
            GenerateRandomString::generate(3),
            GenerateRandomString::generate(12),
            GenerateRandomString::generate(256),
        ];

        foreach($invalidPasswords as $invalid) {
            $this->requestAuthSignIn(AdminAccountFixture::$EMAIL, $invalid)
                ->__invoke()
                ->expectAuthError();

            $this->requestAuthSignIn(UserAccountFixture::$EMAIL, $invalid)
                ->__invoke()
                ->expectAuthError();
        }
    }

    public function test404()
    {
        $this->requestAuthSignIn('demo@example.com', '1234')
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200User()
    {
        $jwtTokenService = self::$app->getContainer()->get(JWTTokenService::class); /** @var JWTTokenService $jwtTokenService */

        $jwt = $this->requestAuthSignIn(UserAccountFixture::$EMAIL, UserAccountFixture::$PASS)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => UserAccountFixture::getAccount()->toJSON(),
                    'profile' => UserAccountFixture::getProfile()->toJSON(),
                ]
            ])
            ->fetch(function(array $json) {
                return $json['token']['jwt'];
            })
        ;

        $jwtToken = $jwtTokenService->decodeJWT($jwt);

        $this->assertEquals(UserAccountFixture::getAccount()->getId(), $jwtToken['data']['accountId']);
        $this->assertEquals(UserAccountFixture::getProfile()->getId(), $jwtToken['data']['profileId']);
    }

    public function test200Admin()
    {
        $jwtTokenService = self::$app->getContainer()->get(JWTTokenService::class); /** @var JWTTokenService $jwtTokenService */

        $jwt = $this->requestAuthSignIn(AdminAccountFixture::$EMAIL, AdminAccountFixture::$PASS)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => AdminAccountFixture::getAccount()->toJSON(),
                    'profile' => AdminAccountFixture::getProfile()->toJSON(),
                ]
            ])
            ->fetch(function(array $json) {
                return $json['token']['jwt'];
            })
        ;

        $jwtToken = $jwtTokenService->decodeJWT($jwt);

        $this->assertEquals(AdminAccountFixture::getAccount()->getId(), $jwtToken['data']['accountId']);
        $this->assertEquals(AdminAccountFixture::getProfile()->getId(), $jwtToken['data']['profileId']);
    }
}