<?php
namespace HappyInvite\REST\Bundles\Auth\Tests\REST\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait AuthRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestAuthCurrent(): RESTRequest
    {
        return $this->request('get', '/auth/current');
    }

    protected function requestAuthSignIn(string $email, string $password): RESTRequest
    {
        return $this->request('post', '/auth/sign-in')
            ->setParameters([
                'email' => $email,
                'password' => $password,
            ]);
    }

    protected function requestAuthSignUp(array $json): RESTRequest
    {
        return $this->request('post', '/auth/sign-up')
            ->setParameters($json);
    }

    protected function requestAuthSignUpProceed(string $token): RESTRequest
    {
        return $this->request('get', '/auth/sign-up-proceed', ['token' => $token]);
    }

    protected function requestAuthSignOut(): RESTRequest
    {
        return $this->request('delete', '/auth/sign-out');
    }
}