<?php
namespace HappyInvite\REST\Bundles\Auth\Tests\REST\Paths;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Auth\Tests\AuthMiddlewareTestCase;

final class SignOutMiddlewareTest extends AuthMiddlewareTestCase
{
    public function test200()
    {
        $jwt = $this->requestAuthSignIn(UserAccountFixture::$EMAIL, UserAccountFixture::$PASS)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => UserAccountFixture::getAccount()->toJSON(),
                    'profile' => UserAccountFixture::getProfile()->toJSON(),
                ]
            ])
            ->fetch(function(array $json) {
                return $json['token']['jwt'];
            })
        ;

        $this->requestAuthSignOut()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);
    }
}