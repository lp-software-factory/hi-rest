<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Auth\Service\AuthService;
use HappyInvite\Domain\Bundles\OAuth2\Factory\LeagueProviderFactory;
use HappyInvite\Domain\Bundles\OAuth2\Factory\OAuth2RequestFactory;
use HappyInvite\Domain\Bundles\OAuth2\Service\OAuth2Service;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\Domain\Bundles\Auth\Formatter\AuthTokenFormatter;

abstract class Command implements \HappyInvite\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var AuthService */
    protected $authService;

    /** @var AuthToken */
    protected $authToken;

    /** @var \HappyInvite\Domain\Bundles\Auth\Formatter\AuthTokenFormatter */
    protected $authTokenFormatter;

    /** @var LeagueProviderFactory */
    protected $leagueProviderFactory;

    /** @var OAuth2RequestFactory */
    protected $ouath2RequestFactory;

    /** @var OAuth2Service */
    protected $oauth2Service;

    public function __construct(
        AccessService $accessService,
        AuthService $authService,
        AuthToken $authToken,
        AuthTokenFormatter $authTokenFormatter,
        LeagueProviderFactory $leagueProviderFactory,
        OAuth2RequestFactory $ouath2RequestFactory,
        OAuth2Service $OAuth2Service
    ) {
        $this->accessService = $accessService;
        $this->authService = $authService;
        $this->authToken = $authToken;
        $this->authTokenFormatter = $authTokenFormatter;
        $this->leagueProviderFactory = $leagueProviderFactory;
        $this->ouath2RequestFactory = $ouath2RequestFactory;
        $this->oauth2Service = $OAuth2Service;
    }
}