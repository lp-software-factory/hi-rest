<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CurrentCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        if($this->authToken->isSignedIn()) {
            $responseBuilder
                ->setJSON([
                    'token' => $this->authToken->toJSON()
                ])
                ->setStatusSuccess();
        }else{
            $responseBuilder
                ->setError("You're not signed in.")
                ->setStatusNotAllowed();
        }

        return $responseBuilder->build();
    }
}