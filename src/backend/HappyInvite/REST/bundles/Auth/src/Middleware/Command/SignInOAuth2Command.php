<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignInOAuth2Command extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $league = $this->oauth2Service->getOAuth2LeagueProvider($request->getAttribute('provider'));

        if(isset($_GET['code'])) {
            $this->preventCSRFAttack();

            try {
                $this->authService->signInOauth2($league, $_GET['code']);

                header('Location: /browser');
                exit();
            }catch(AccountNotFoundException $e) {
                $responseBuilder
                    ->setStatusNotFound()
                    ->setError($e);
            }
        }else{
            $authorizationUrl = $league->getAuthorizationUrl();
            $_SESSION['oauth2state'] = $league->getState();
            header('Location: ' . $authorizationUrl);
            exit;
        }

        return $responseBuilder->build();
    }

    private function preventCSRFAttack()
    {
        $hasState = !empty($_GET['state']);
        $isSameClient = !empty($_GET['state']) && ($_GET['state'] === $_SESSION['oauth2state']);

        if (!($hasState && $isSameClient)) {
            throw new \Exception('Invalid state. Looks like a CSRF attack so gtfo.');
        }
    }
}