<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Domain\Bundles\Account\Exceptions\DuplicateAccountException;
use HappyInvite\Domain\Bundles\Auth\Exceptions\InvalidAcceptTokenException;
use HappyInvite\Domain\Bundles\Auth\Exceptions\SignUpRequestNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignUpProceedCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $token = $request->getQueryParams()['token'];

            $authToken = $this->authService->acceptSignUpRequest($token);

            $responseBuilder
                ->setJSON([
                    'token' => $this->authTokenFormatter->format($authToken),
                ])
                ->setStatusSuccess();
        }catch(SignUpRequestNotFoundException | InvalidAcceptTokenException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatus(422);
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }catch(DuplicateAccountException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}