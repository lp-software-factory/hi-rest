<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignOutCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->authService->signOut();

        return $responseBuilder
            ->setStatusSuccess()
            ->build();
    }
}