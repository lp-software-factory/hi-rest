<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Domain\Bundles\Auth\Exceptions\JWTTokenFailException;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Profile\Exceptions\ProfileNotFoundException;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class ProtectedMiddleware implements MiddlewareInterface
{
    /** @var AuthToken */
    private $authToken;

    public function __construct(AuthToken $authToken)
    {
        $this->authToken = $authToken;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        try {
            if ($request->hasHeader('Authorization') && strlen($request->getHeader('Authorization')[0])) {
                $this->authToken->auth($request->getHeader('Authorization')[0]);
            } else if(isset($_SESSION[AuthToken::SESSION_KEY_JWT]) && is_string($_SESSION[AuthToken::SESSION_KEY_JWT])) {
                $this->authToken->auth($_SESSION[AuthToken::SESSION_KEY_JWT]);
            }

            return $out($request, $response);
        }catch(AccountNotFoundException | ProfileNotFoundException $e) {
            unset($_SESSION[AuthToken::SESSION_KEY_JWT]);

            $responseBuilder = new HIResponseBuilder($response);

            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();

            return $responseBuilder->build();
        }catch(JWTTokenFailException $e) {
            unset($_SESSION[AuthToken::SESSION_KEY_JWT]);

            $responseBuilder = new HIResponseBuilder($response);

            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();

            return $responseBuilder->build();
        }
    }
}