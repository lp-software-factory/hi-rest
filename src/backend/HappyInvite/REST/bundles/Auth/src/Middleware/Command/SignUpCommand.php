<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Domain\Bundles\Account\Exceptions\DuplicateAccountException;
use HappyInvite\REST\Bundles\Auth\Request\SignUpRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignUpCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $request = (new SignUpRequest($request))->getParameters();

            $this->authService->signUp($request);

            $responseBuilder
                ->setStatusSuccess();
        }catch(DuplicateAccountException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}