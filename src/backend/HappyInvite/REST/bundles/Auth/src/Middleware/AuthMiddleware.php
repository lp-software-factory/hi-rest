<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware;

use HappyInvite\REST\Bundles\Auth\Middleware\Command\CurrentCommand;
use HappyInvite\REST\Bundles\Auth\Middleware\Command\SignInCommand;
use HappyInvite\REST\Bundles\Auth\Middleware\Command\SignInOAuth2Command;
use HappyInvite\REST\Bundles\Auth\Middleware\Command\SignOutCommand;
use HappyInvite\REST\Bundles\Auth\Middleware\Command\SignUpCommand;
use HappyInvite\REST\Bundles\Auth\Middleware\Command\SignUpProceedCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class AuthMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    /**
     * AuthMiddleware constructor.
     * @param CommandService $commandService
     */
    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('current', CurrentCommand::class)
            ->attachDirect('oauth2', SignInOAuth2Command::class)
            ->attachDirect('sign-in', SignInCommand::class)
            ->attachDirect('sign-up', SignUpCommand::class)
            ->attachDirect('sign-up-proceed', SignUpProceedCommand::class)
            ->attachDirect('sign-out', SignOutCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}