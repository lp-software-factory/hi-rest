<?php
namespace HappyInvite\REST\Bundles\Auth\Middleware\Command;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Domain\Bundles\Auth\Exceptions\InvalidPasswordException;
use HappyInvite\REST\Bundles\Auth\Request\SignInRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SignInCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $request = (new SignInRequest($request))->getParameters();

            $authToken = $this->authService->signIn($request);

            $responseBuilder
                ->setJSON([
                    'token' => $authToken->toJSON()
                ])
                ->setStatusSuccess();
        }catch(InvalidPasswordException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}