<?php
namespace HappyInvite\REST\Bundles\Auth;

use HappyInvite\REST\Bundles\Auth\Middleware\AuthMiddleware;
use HappyInvite\REST\Bundles\Auth\Middleware\ProtectedMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'pipe',
                'path' => '/',
                'group' => 'auth',
                'middleware' => ProtectedMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/auth/{command:current}[/]',
                'middleware' => AuthMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/auth/{command:sign-in}[/]',
                'middleware' => AuthMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/auth/{command:oauth2}/{provider}[/]',
                'middleware' => AuthMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/auth/{command:sign-up}[/]',
                'middleware' => AuthMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/auth/{command:sign-up-proceed}[/]',
                'middleware' => AuthMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/auth/{command:sign-out}[/]',
                'middleware' => AuthMiddleware::class,
            ],
        ],
    ],
];