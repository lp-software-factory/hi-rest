<?php
namespace HappyInvite\REST\Bundles\Frontend\Middleware;

use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

class
FrontendMiddleware implements MiddlewareInterface
{
    /** @var FrontendService */
    private $frontendService;

    public function __construct(FrontendService $frontendService)
    {
        $this->frontendService = $frontendService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $tags = explode(',', urldecode($request->getAttribute('tags')));

        if(in_array('*', $tags)) {
            $filter = new FrontendService\NoneFilter();
        }else{
            $filter = new FrontendService\IncludeFilter();
            $filter->includeTags($tags);
        }

        $response->getBody()->write(json_encode([
                'success' => true
            ] + $this->frontendService->fetch($filter), JSON_UNESCAPED_SLASHES));

        return $response
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
        ;
    }
}