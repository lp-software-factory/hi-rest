<?php
namespace HappyInvite\REST\Bundles\Frontend\Tests;

use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;

final class FrontendMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
        ];
    }

    public function test200()
    {
        $this->request('get', '/frontend/*/')
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => [
                    'current' => [
                        'id' => $this->expectId(),
                        'language' =>  $this->expectString(),
                        'region' =>  $this->expectString(),
                    ],
                    'available' => function($input) {
                        foreach($input as $locale) {
                            $this->assertTrue(is_array($locale));
                            $this->assertTrue(isset($locale['region']) && is_string($locale['region']));
                            $this->assertTrue(isset($locale['language']) && is_string($locale['language']));
                        }
                    }
                ],
                'auth' => [
                    'jwt' => AdminAccountFixture::getJWT(),
                    'account' => AdminAccountFixture::getAccount()->toJSON(),
                    'profile' => AdminAccountFixture::getProfile()->toJSON(),
                ]
            ]);
    }
}