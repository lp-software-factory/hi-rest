<?php
namespace HappyInvite\REST\Bundles\Frontend\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait FrontendRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestFrontend(): RESTRequest
    {
        return $this->request('get', '/frontend/*/');
    }
}