<?php
namespace HappyInvite\REST\Bundles\Frontend;

use HappyInvite\REST\Bundle\RESTBundle;

final class FrontendRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}