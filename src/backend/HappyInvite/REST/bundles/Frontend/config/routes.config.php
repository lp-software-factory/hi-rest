<?php
namespace HappyInvite\REST\Bundles\Frontend;

use HappyInvite\REST\Bundles\Frontend\Middleware\FrontendMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/frontend/{tags}[/]',
                'middleware' => FrontendMiddleware::class,
            ]
        ],
    ],
];