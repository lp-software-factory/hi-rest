<?php
namespace HappyInvite\REST\Bundles\Attachment;

use HappyInvite\REST\Bundles\Attachment\Middleware\AttachmentMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'post',
                'path' => '/attachment/{command:upload}[/]',
                'middleware' => AttachmentMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/attachment/{command:link}[/]',
                'middleware' => AttachmentMiddleware::class,
            ],
        ],
    ],
];