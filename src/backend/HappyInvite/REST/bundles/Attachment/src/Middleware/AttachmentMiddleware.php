<?php
namespace HappyInvite\REST\Bundles\Attachment\Middleware;

use HappyInvite\REST\Bundles\Attachment\Middleware\Command\LinkCommand;
use HappyInvite\REST\Bundles\Attachment\Middleware\Command\UploadCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

class AttachmentMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('upload', UploadCommand::class)
            ->attachDirect('link', LinkCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}