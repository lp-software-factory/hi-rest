<?php
namespace HappyInvite\REST\Bundles\Attachment\Middleware\Command;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Attachment\Service\FetchResourceService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;

abstract class Command implements \HappyInvite\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var AttachmentService */
    protected $attachmentService;

    /** @var FetchResourceService */
    protected $fetchResourceService;

    public function __construct(
        AccessService $accessService,
        AttachmentService $attachmentService,
        FetchResourceService $fetchResourceService
    ) {
        $this->accessService = $accessService;
        $this->attachmentService = $attachmentService;
        $this->fetchResourceService = $fetchResourceService;
    }
}