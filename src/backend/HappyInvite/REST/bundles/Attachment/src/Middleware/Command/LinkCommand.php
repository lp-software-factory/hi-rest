<?php
namespace HappyInvite\REST\Bundles\Attachment\Middleware\Command;

use HappyInvite\Domain\Bundles\Attachment\Exception\InvalidURLException;
use HappyInvite\Domain\Bundles\Attachment\Exception\NotFoundException;
use HappyInvite\Domain\Bundles\Attachment\Source\ExternalSource;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class LinkCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            $url = $request->getQueryParams()['url'] ?? '';
            $source = new ExternalSource($url);

            if(strpos($url, 'http') === false) {
                $url = 'http://' . $url;
            }

            $result = $this->fetchResourceService->fetchResource($url);
            $entity = $this->attachmentService->linkAttachment($url, '', '', $result, $source);

            $responseBuilder
                ->setStatusSuccess()
                ->setJSON([
                    'entity' => $entity->toJSON(),
                ]);
        } catch(InvalidURLException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusBadRequest();
        } catch(NotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}