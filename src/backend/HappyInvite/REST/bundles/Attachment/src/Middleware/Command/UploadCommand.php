<?php
namespace HappyInvite\REST\Bundles\Attachment\Middleware\Command;

use HappyInvite\Domain\Bundles\Attachment\Exception\AttachmentFactoryException;
use HappyInvite\Domain\Bundles\Attachment\Exception\FileTooBigException;
use HappyInvite\Domain\Bundles\Attachment\Exception\FileTooSmallException;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Attachment\Exceptions\FileNotUploadedException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\UploadedFile;

class UploadCommand extends Command
{
    const AUTO_GENERATE_FILE_NAME_LENGTH = 8;

    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            /** @var UploadedFile $file */
            $file = $request->getUploadedFiles()["file"];
            $filename = $file->getClientFilename();

            if(! strlen($filename)) {
                $filename = GenerateRandomString::generate(self::AUTO_GENERATE_FILE_NAME_LENGTH);
            }

            if($file->getError() !== 0) {
                throw new FileNotUploadedException('Failed to upload file');
            }

            $entity = $this->attachmentService->uploadAttachment(
                $file->getStream()->getMetadata('uri'),
                $filename
            );

            $responseBuilder
                ->setStatusSuccess()
                ->setJSON([
                    'entity' => $entity->toJSON(),
                ]);
        } catch(FileTooBigException  $e) {
            $responseBuilder
                ->setStatus(409)
                ->setError($e);
        } catch(FileTooSmallException $e) {
            $responseBuilder
                ->setStatus(409)
                ->setError($e);
        } catch(AttachmentFactoryException $e) {
            $responseBuilder
                ->setStatusBadRequest()
                ->setError($e);
        }

        return $responseBuilder->build();
    }
}