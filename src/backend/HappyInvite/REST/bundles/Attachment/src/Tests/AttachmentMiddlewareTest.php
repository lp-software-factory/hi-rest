<?php
namespace HappyInvite\REST\Bundles\Attachment\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;
use Zend\Diactoros\UploadedFile;

/**
 * @backupGlobals disabled
 */
class AttachmentMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
        ];
    }

    public function testUpload403()
    {
        $localFileName = __DIR__ . '/Resources/grid-example.png';
        $uploadFile = new UploadedFile($localFileName, filesize($localFileName), 0);

        $this->requestAttachmentUpload($uploadFile)
            ->__invoke()
            ->expectAuthError();
    }

    public function testUpload200()
    {
        $localFileName = __DIR__ . '/Resources/grid-example.png';
        $uploadFile = new UploadedFile($localFileName, filesize($localFileName), 0);

        return $this->requestAttachmentUpload($uploadFile)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
            ->expectJSONBody([
                'entity' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectString(),
                    'link' => [
                        'url' => $this->expectString(),
                        'resource' => $this->expectString(),
                        'source' => [
                            'source' => 'local',
                            'public_path' => $this->expectString(),
                            'storage_path' => $this->expectString(),
                        ],
                    ],
                    'is_attached' => false,
                ],
            ]);
    }
}