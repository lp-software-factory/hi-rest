<?php
namespace HappyInvite\REST\Bundles\Attachment\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Diactoros\UploadedFile;
use Zend\Expressive\Application;

final class AttachmentFixture implements Fixture
{
    /** @var Attachment */
    public static $attachmentImage;

    /** @var Attachment */
    public static $attachmentImage2;

    /** @var Attachment */
    public static $attachmentImage3;

    /** @var Attachment */
    public static $attachmentVideo;

    /** @var Attachment */
    public static $attachmentLink;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(AttachmentService::class); /** @var AttachmentService $service */

        self::$attachmentImage = $service->uploadAttachment(
            $this->getLocalFile()->getStream()->getMetadata('uri'),
            'image.png'
        );

        self::$attachmentImage2 = $service->uploadAttachment(
            $this->getLocalFile()->getStream()->getMetadata('uri'),
            'image.png'
        );

        self::$attachmentImage3 = $service->uploadAttachment(
            $this->getLocalFile()->getStream()->getMetadata('uri'),
            'image.png'
        );

        self::$attachmentVideo = $service->uploadAttachment(
            $this->getLocalFile()->getStream()->getMetadata('uri'),
            'video.mp4'
        );

        self::$attachmentLink = $service->uploadAttachment(
            $this->getLocalFile()->getStream()->getMetadata('uri'),
            'link.url'
        );
    }

    private function getLocalFile(): UploadedFile
    {
        $localFileName = __DIR__ . '/../Resources/grid-example.png';

        return new UploadedFile($localFileName, filesize($localFileName), 0);
    }
}