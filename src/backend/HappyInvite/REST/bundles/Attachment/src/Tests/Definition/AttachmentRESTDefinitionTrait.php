<?php
namespace HappyInvite\REST\Bundles\Attachment\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use Zend\Diactoros\UploadedFile;

trait AttachmentRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestAttachmentUpload(UploadedFile $localFile): RESTRequest
    {
        return $this
            ->request('POST', '/attachment/upload')
            ->setUploadedFiles([
                'file' => $localFile,
            ]);
    }
}