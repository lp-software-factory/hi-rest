<?php
namespace HappyInvite\REST\Bundles\Attachment\Exceptions;

final class FileNotUploadedException extends \Exception {}