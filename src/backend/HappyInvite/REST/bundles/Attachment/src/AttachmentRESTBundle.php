<?php
namespace HappyInvite\REST\Bundles\Attachment;

use HappyInvite\REST\Bundle\RESTBundle;

final class AttachmentRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}