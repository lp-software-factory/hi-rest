<?php
namespace HappyInvite\REST\Bundles\Avatar;

use HappyInvite\REST\Bundle\RESTBundle;

final class AvatarRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}