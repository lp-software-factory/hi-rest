<?php
namespace HappyInvite\REST\Bundles\Landing;

use HappyInvite\REST\Bundles\Landing\Middleware\LandingMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/landing/{command:create}[/]',
                'middleware' => LandingMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/landing/{landingId}/{command:delete}[/]',
                'middleware' => LandingMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/landing/{landingId}/{command:edit}[/]',
                'middleware' => LandingMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/landing/type/{type}/{command:get-type}[/]',
                'middleware' => LandingMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/landing/{landingIds}/{command:get}[/]',
                'middleware' => LandingMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/landing/{landingIds}/{command:select}[/]',
                'middleware' => LandingMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/landing/{command:list}/offset/{offset}/limit/{limit}[/]',
                'middleware' => LandingMiddleware::class,
            ],
        ],
    ],
];