<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Landing\Tests\Fixtures\LandingsFixture;
use HappyInvite\REST\Bundles\Landing\Tests\LandingMiddlewareTestCase;

final class GetByTypeLandingMiddlewareTest extends LandingMiddlewareTestCase
{
   public function test404()
    {
        $this->upFixture(new LandingsFixture());

        $this->requestLandingGetByType(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new LandingsFixture());

        $langing = LandingsFixture::get(1);
       // $landingType = $langing->getType();
        $landingType = 'first';

        $this->requestLandingGetByType($landingType)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'landing' => [
                    'type' => $this->$landingType,
                ]
            ])
        ;
    }

    public function test409()
    {
        $this->upFixture(new LandingsFixture());

        $landing = LandingsFixture::get(1);

        $this->requestLandingGetByType($landing->getType())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
          //  ->dump()
            ->expectStatusCode(409)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => false,
                'error' => $this->expectString(),
            ])
        ;
    }

}