<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Methods;


use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Landing\Tests\Fixtures\LandingsFixture;
use HappyInvite\REST\Bundles\Landing\Tests\LandingMiddlewareTestCase;


final class ListLandingsMiddlewareTest extends LandingMiddlewareTestCase
{

    public function test403()
    {
        $this->upFixture(new LandingsFixture());

        $this->requestLandingList(0, 5)
            ->__invoke()
         //   ->dump()
            ->expectAuthError();

        $this->requestLandingList(0, 5)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
          //  ->dump()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new LandingsFixture());

        $this->requestLandingList(0, 5)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
         //   ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 0,
                        'limit' => 5,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'landings' => [
                    ['id' => LandingsFixture::get(5)->getId()],
                    ['id' => LandingsFixture::get(4)->getId()],
                    ['id' => LandingsFixture::get(3)->getId()],
                    ['id' => LandingsFixture::get(2)->getId()],
                    ['id' => LandingsFixture::get(1)->getId()],
                ],
                'total' => 10,
            ])
            ->expect(function(array $json) {
                $this->assertEquals(5, count($json['landings']));
            })
        ;

        $this->requestLandingList(2, 3)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
          //  ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 2,
                        'limit' => 3,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'landings' => [
                    ['id' => LandingsFixture::get(3)->getId()],
                    ['id' => LandingsFixture::get(2)->getId()],
                    ['id' => LandingsFixture::get(1)->getId()],
                ],
                'total' => 10
            ])
            ->expect(function(array $json) {
                $this->assertEquals(3, count($json['landings']));
            })
        ;

        $this->requestLandingList(1, 1)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
        //    ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 1,
                        'limit' => 1,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'landings' => [
                    ['id' => LandingsFixture::get(4)->getId()],
                ],
                'total' => 10
            ])
            ->expect(function(array $json) {
                $this->assertEquals(1, count($json['landings']));
            })
        ;
    }

    public function test200Sort()
    {
        $this->upFixture(new LandingsFixture());

        $this->requestLandingList(0, 5, ['sort' => 'id', 'order' => 'asc'])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
         //   ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 0,
                        'limit' => 5,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'asc'
                    ]
                ],
                'landings' => [
                    ['id' => LandingsFixture::get(1)->getId()],
                    ['id' => LandingsFixture::get(2)->getId()],
                    ['id' => LandingsFixture::get(3)->getId()],
                    ['id' => LandingsFixture::get(4)->getId()],
                    ['id' => LandingsFixture::get(5)->getId()],
                ],
                'total' => 5
            ])
            ->expect(function(array $json) {
                $this->assertEquals(5, count($json['landings']));
            })
        ;

        $this->requestLandingList(0, 5, ['sort' => 'id', 'order' => 'desc'])
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
          //  ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => [
                    'seek' => [
                        'max_limit' => 1000,
                        'offset' => 0,
                        'limit' => 5,
                    ],
                    'sort' => [
                        'field' => 'id',
                        'direction' => 'desc'
                    ]
                ],
                'landings' => [
                    ['id' => LandingsFixture::get(5)->getId()],
                    ['id' => LandingsFixture::get(4)->getId()],
                    ['id' => LandingsFixture::get(3)->getId()],
                    ['id' => LandingsFixture::get(2)->getId()],
                    ['id' => LandingsFixture::get(1)->getId()],
                ],
                'total' => 10
            ])
            ->expect(function(array $json) {
                $this->assertEquals(5, count($json['landings']));
            })
        ;
    }

}