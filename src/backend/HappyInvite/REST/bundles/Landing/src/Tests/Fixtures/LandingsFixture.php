<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Fixtures;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Landing\Entity\Landing;
use HappyInvite\Domain\Bundles\Landing\Parameters\CreateLandingParameters;
use HappyInvite\Domain\Bundles\Landing\Service\LandingService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class LandingsFixture implements Fixture
{

    private static $landings;

    public function up(Application $app, EntityManager $em)
    {
       self::$landings = [];

        /** @var LandingService $service */
        $service = $app->getContainer()->get(LandingService::class);

        $title = 'demo';
        $description = 'test';
        $definition = 'test';
        $type = 'first';



        for($index = 0; $index < 5; $index++) {
            self::$landings[$index+1] = $service->createLanding(new CreateLandingParameters(
                $title,
                $description,
                $definition,
                $type
            ));
        }

        // var_dump(self::$landings);die();

    }

    public static function get(int $index): Landing
    {
        return self::$landings[$index];
    }

    public static function getAll(): array
    {
        return self::$landings;
    }
}