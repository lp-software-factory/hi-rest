<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait LandingRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestLandingCreate(array $json): RESTRequest
    {
        return $this->request('put', '/landing/create')
            ->setParameters($json);
    }

    protected function requestLandingDelete(int $landingId): RESTRequest
    {
        return $this->request('delete', sprintf('/landing/%d/delete', $landingId));
    }

    protected function requestLandingEdit(int $landingId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/landing/%d/edit', $landingId))
            ->setParameters($json);
    }

    protected function requestLandingGetById(int $landingId): RESTRequest
    {
        return $this->request('get', sprintf('/landing/%d/get', $landingId));
    }

    protected function requestLandingGetByType(string $type): RESTRequest
    {
        return $this->request('get', sprintf('/landing/type/%d/get', $type));
    }

    protected function requestLandingSelect(int $landingId): RESTRequest
    {
        return $this->request('get', sprintf('/landing/%d/select', $landingId));
    }
}