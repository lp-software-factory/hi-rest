<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Landing\Tests\Fixtures\LandingsFixture;
use HappyInvite\REST\Bundles\Landing\Tests\LandingMiddlewareTestCase;


final class EditCompanyMiddlewareTest extends LandingMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new LandingsFixture());

        $json = [
            'title' => 'retitle landing',
            'description' => 'rewrite description',
            'definition' => 'rewrite definition',
            'type' => 'new type'
        ];

        $this->requestLandingEdit(LandingsFixture::get(1)->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestLandingEdit(LandingsFixture::get(1)->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new LandingsFixture());

        $json = [
            'title' => 'retitle landing',
            'description' => 'rewrite description',
            'definition' => 'rewrite definition',
            'type' => 'new type'
        ];

        $this->requestLandingEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new LandingsFixture());

        $landing = LandingsFixture::get(1);
        $json = [
            'title' => 'retitle landing',
            'description' => 'rewrite description',
            'definition' => 'rewrite definition',
            'type' => 'new type'
        ];

        $this->requestLandingEdit($landing->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);


        $this->requestLandingGetById($landing->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'landing' => [
                    'id' => $landing->getId(),
                    'title' => $json['title'],
                    'description' => $json['description'],
                    'definition' => $json['definition'],
                    'type' => $json['type']
                ]
            ])
        ;
    }

}