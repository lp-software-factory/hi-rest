<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Landing\Tests\LandingMiddlewareTestCase;


final class CreateCompanyMiddlewareTest extends LandingMiddlewareTestCase
{
    public function test403()
    {
        $this->requestLandingCreate([
            'title' => 'Demo title',
            'description' => 'Demo description',
            'definition' => 'Demo definition json array',
            'type'=> 'Standart'
        ])
            ->__invoke()
           // ->dump()
            ->expectAuthError();

        $this->requestLandingCreate([
            'title' => 'Demo title',
            'description' => 'Demo description',
            'definition' => 'Demo definition json array',
            'type'=> 'Standart'
        ])
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
          //->dump()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            'title' => 'Demo title',
            'description' => 'Demo description',
            'definition' => 'Demo definition json array',
            'type'=> 'Standart'
            ];
        $this->requestLandingCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            //->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'landing' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'title' => $json['title'],
                    'description' => $json['description'],
                    'definition' => $json['definition'],
                    'type' => $json['type'],
                ]
            ]);

    }
}