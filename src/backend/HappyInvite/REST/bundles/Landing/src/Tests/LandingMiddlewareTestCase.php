<?php
namespace HappyInvite\REST\Bundles\Landing\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Landing\Tests\Fixtures\LandingsFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class LandingMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
            new UserAccountFixture(),
            new LandingsFixture()
        ];
    }

    protected function requestLandingList(int $offset, int $limit, array $qp = null): RESTRequest
    {
        return $this->request('get', sprintf('/landing/list/offset/%d/limit/%d', $offset, $limit), $qp);
    }
}