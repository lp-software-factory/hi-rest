<?php
namespace HappyInvite\REST\Bundles\Landing\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Landing\Tests\Fixtures\LandingsFixture;
use HappyInvite\REST\Bundles\Landing\Tests\LandingMiddlewareTestCase;

final class SelectLandingMiddlewareTest extends LandingMiddlewareTestCase
{

    public function test403()
    {
        $this->upFixture(new LandingsFixture());

        $this->requestLandingSelect(LandingsFixture::get(1)->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestLandingSelect(LandingsFixture::get(1)->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new LandingsFixture());

        $this->requestLandingSelect(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
         //   ->dump()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new LandingsFixture());

        $landing = LandingsFixture::get(1);
      //  $select = $landing->isSelected();
        $landingId = $landing->getId();
      //  print_r($landing);
        $this->requestLandingSelect($landingId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->dump()

            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'landing' => [
                    'id' => $landingId,
                    ]
            ]);

        $this->requestLandingGetById($landingId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
         //   ->dump()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'landing' => [
                    'id' => $landingId,
                 //   'isSelected' => $select
                ]
            ])
        ;


/*
        $this->requestGetLandingById($landingId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->dump()
            ->expectNotFoundError()
        ;
*/
    }

}
