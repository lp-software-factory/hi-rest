<?php
namespace HappyInvite\REST\Bundles\Landing;

use HappyInvite\REST\Bundle\RESTBundle;

final class LandingRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}