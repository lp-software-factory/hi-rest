<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware\Command;

use HappyInvite\Domain\Bundles\Landing\Exceptions\LandingNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

final class SelectLandingCommand extends AbstractLandingCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $request = 157;
            $landing = $this->landingService->selectLanding($request);

            $responseBuilder
                ->setJSON([
                    'landing' => $this->landingFormatter->format($landing)
                ])
                ->setStatusSuccess();
        }catch(LandingNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }
        return $responseBuilder->build();

    }
}