<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware\Command;

use HappyInvite\Domain\Bundles\Landing\Criteria\ListLandingsCriteria;
use HappyInvite\Domain\Bundles\Landing\Entity\Landing;
use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;

final class ListLandingsCommand extends AbstractLandingCommand
{
    const MAX_ENTITIES = 1000;

    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $result = $this->landingService->listLandings(
            $criteria = $this->fetchCriteria($request)
        );

        return $responseBuilder
            ->setJSON([
                'landings' => array_map(function(Landing $landing) {
                    return $landing->toJSON();
                }, $result),
                'criteria' => $criteria->toJSON(),
                'total' => $this->landingService->countLandings($criteria),
            ])
            ->setStatusSuccess()
            ->build();
    }

    private function fetchCriteria(ServerRequestInterface $request): ListLandingsCriteria
    {
        $qp = $request->getQueryParams();

        $seekOffset = $request->getAttribute('offset');
        $seekLimit = $request->getAttribute('limit');

        $orderByField = $qp['sort'] ?? ListLandingsCriteria::ORDER_BY_DEFAULT_FIELD;
        $orderByDirection = $qp['order'] ?? ListLandingsCriteria::ORDER_BY_DEFAULT_DIRECTION;

        return new ListLandingsCriteria(
            new SeekCriteria(self::MAX_ENTITIES, $seekOffset, $seekLimit),
            new SortCriteria($orderByField,  $orderByDirection)
        );
    }
}