<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware\Command;

use HappyInvite\Domain\Bundles\Landing\Exceptions\LandingNotFoundException;
use HappyInvite\Domain\Bundles\Landing\Parameters\EditLandingParameters;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Landing\Request\EditLandingRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditLandingCommand extends AbstractLandingCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = $this->landingParameters->editLandingParameters($request);

            $landing = $this->landingService->editLanding($request->getAttribute('landingId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'landing' => $this->landingFormatter->format($landing)
                ])
                ->setStatusSuccess();
        }catch(LandingNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}