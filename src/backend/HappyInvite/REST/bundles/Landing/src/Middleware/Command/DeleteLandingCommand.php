<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware\Command;


use HappyInvite\Domain\Bundles\Landing\Exceptions\LandingNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

final class DeleteLandingCommand extends AbstractLandingCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->landingService->deleteLanding($request->getAttribute('landingId'));

            $responseBuilder->setStatusSuccess();
        }catch(LandingNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}