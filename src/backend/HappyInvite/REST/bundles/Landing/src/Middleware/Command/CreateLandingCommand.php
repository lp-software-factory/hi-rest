<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Landing\Request\CreateLandingRequest;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

final class CreateLandingCommand extends AbstractLandingCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->landingParameters->createLandingParameters($request);

        $landing = $this->landingService->createLanding($parameters);

        $responseBuilder
            ->setJSON([
                'landing' => $this->landingFormatter->format($landing)
                ])
                ->setStatusSuccess();

        return $responseBuilder->build();

    }
}
