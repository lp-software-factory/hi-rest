<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware\Command;

use HappyInvite\Domain\Bundles\Landing\Factory\Doctrine\LandingParametersFactory;
use HappyInvite\Domain\Bundles\Landing\Formatter\LandingFormatter;
use HappyInvite\Domain\Bundles\Landing\Service\LandingService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;


abstract class AbstractLandingCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var  LandingService */
    protected $landingService;

    /** @var  LandingFormatter */
    protected $landingFormatter;

    /** @var  LandingParametersFactory */
    protected $landingParameters;

    public function __construct
    (
        AccessService $accessService,
        LandingService $landingService,
        LandingFormatter $landingFormatter,
        LandingParametersFactory $landingParameters
    )
    {
        $this->accessService = $accessService;
        $this->landingService = $landingService;
        $this->landingFormatter = $landingFormatter;
        $this->landingParameters = $landingParameters;
    }
}