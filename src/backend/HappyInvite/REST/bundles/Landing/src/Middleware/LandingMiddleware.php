<?php
namespace HappyInvite\REST\Bundles\Landing\Middleware;

use HappyInvite\REST\Bundles\Landing\Middleware\Command\CreateLandingCommand;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\DeleteLandingCommand;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\EditLandingCommand;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\GetByIdLandingCommand;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\GetByTypeLandingCommand;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\ListLandingsCommand;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\SelectLandingCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


final class LandingMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateLandingCommand::class)
            ->attachDirect('edit', EditLandingCommand::class)
            ->attachDirect('select', SelectLandingCommand::class)
            ->attachDirect('delete', DeleteLandingCommand::class)
            ->attachDirect('get', GetByTypeLandingCommand::class)
            ->attachDirect('list', ListLandingsCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}
