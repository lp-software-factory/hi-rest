<?php
namespace HappyInvite\REST\Bundles\Landing\Request;

use HappyInvite\Domain\Bundles\Landing\Parameters\CreateLandingParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateLandingRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        $json = $this->getData();

        return $json;
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_LandingBundle_CreateLandingRequest');
    }
}