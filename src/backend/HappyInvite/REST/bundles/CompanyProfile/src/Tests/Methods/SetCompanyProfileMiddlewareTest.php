<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\CompanyProfile\Tests\CompanyProfileMiddlewareTest;
use HappyInvite\REST\Bundles\CompanyProfile\Tests\Fixtures\CompanyProfilesFixture;

final class SetCompanyProfileMiddlewareTest extends CompanyProfileMiddlewareTest
{
    public function test403()
    {
        {
            $this->upFixture(new CompanyProfilesFixture());
            $json = [
                'company' => '',
                'inn' => '4164566551',
                'kpp' => '34213563',
                'ogrn' => '124124',
                'who_gives' => 'kekeke',
                'bank_bik' => '444',
                'giro' => '241241',
                'bank_correspondent_account' => '2341254125',
                'okpo' => '12412',
                'okato' => '24124',
                'okved' => '2141241',
                'legal_address' => 'Pushkina str.',
                'real_address' => 'Kolotushkina str.',
                'director_general' => 'Who know',
                'accounting_general' => 'Anyone',
                'phone' => '224466',
                'fax' => '23156',
                'email' => 'thismail@you.com',
            ];

            $this->requestCompanyProfileSet(CompanyProfilesFixture::get(1)->getId(), $json)
                ->__invoke()
                ->expectAuthError();

            $this->requestCompanyProfileSet(CompanyProfilesFixture::get(1)->getId(), $json)
                ->auth(UserAccountFixture::getJWT())
                ->__invoke()
                ->expectAuthError();
        }
    }

    public function test404()
    {
        $this->upFixture(new CompanyProfilesFixture());
        $json = [];

        $this->requestCompanyProfileSet(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200NoProfile()
    {
        $this->upFixture(new CompanyProfilesFixture());

        $companyProfile = CompanyProfilesFixture::get(5);
        $companyProfileById = $companyProfile->getId();
        $json = [
            'company' => '',
            'inn' => '4164566551',
            'kpp' => '34213563',
            'ogrn' => '124124',
            'who_gives' => 'kekeke',
            'bank_bik' => '444',
            'giro' => '241241',
            'bank_correspondent_account' => '2341254125',
            'okpo' => '12412',
            'okato' => '24124',
            'okved' => '2141241',
            'legal_address' => 'Pushkina str.',
            'real_address' => 'Kolotushkina str.',
            'director_general' => 'Who know',
            'accounting_general' => 'Anyone',
            'phone' => '224466',
            'fax' => '23156',
            'email' => 'thismail@you.com',
        ];

        $this->requestCompanyProfileSet($companyProfile->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'companyProfile' => [
                    'id' => $companyProfileById,
                    'company' => $json['company'],
                    'inn' => $json['inn'],
                    'kpp' => $json['kpp'],
                    'ogrn' => $json['ogrn'],
                    'who_gives' => $json['who_gives'],
                    'bank_bik' => $json['bank_bik'],
                    'giro' => $json['giro'],
                    'bank_correspondent_account' => $json['bank_correspondent_account'],
                    'okpo' => $json['okpo'],
                    'okato' => $json['okato'],
                    'okved' => $json['okved'],
                    'legal_address' => $json['legal_address'],
                    'real_address' => $json['real_address'],
                    'director_general' => $json['director_general'],
                    'accounting_general' => $json['accounting_general'],
                    'phone' => $json['phone'],
                    'fax' => $json['fax'],
                    'email' => $json['email'],

                ],
            ]);
    }

    public function test200HasProfile()
    {
        $this->upFixture(new CompanyProfilesFixture());

        $companyProfile = CompanyProfilesFixture::get(1);
        $companyProfileById = $companyProfile->getId();
        $json = [
            'company' => '',
            'inn' => '4164566551',
            'kpp' => '34213563',
            'ogrn' => '124124',
            'who_gives' => 'kekeke',
            'bank_bik' => '444',
            'giro' => '241241',
            'bank_correspondent_account' => '2341254125',
            'okpo' => '12412',
            'okato' => '24124',
            'okved' => '2141241',
            'legal_address' => 'Pushkina str.',
            'real_address' => 'Kolotushkina str.',
            'director_general' => 'Who know',
            'accounting_general' => 'Anyone',
            'phone' => '224466',
            'fax' => '23156',
            'email' => 'thismail@you.com',
        ];

        $this->requestCompanyProfileSet($companyProfile->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'companyProfile' => [
                    'id' => $companyProfileById,
                    'company' => $json['company'],
                    'inn' => $json['inn'],
                    'kpp' => $json['kpp'],
                    'ogrn' => $json['ogrn'],
                    'who_gives' => $json['who_gives'],
                    'bank_bik' => $json['bank_bik'],
                    'giro' => $json['giro'],
                    'bank_correspondent_account' => $json['bank_correspondent_account'],
                    'okpo' => $json['okpo'],
                    'okato' => $json['okato'],
                    'okved' => $json['okved'],
                    'legal_address' => $json['legal_address'],
                    'real_address' => $json['real_address'],
                    'director_general' => $json['director_general'],
                    'accounting_general' => $json['accounting_general'],
                    'phone' => $json['phone'],
                    'fax' => $json['fax'],
                    'email' => $json['email'],

                ],

            ]);
    }
}