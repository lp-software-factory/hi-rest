<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Tests\Fixtures;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\CompanyProfile\Entity\CompanyProfile;
use HappyInvite\Domain\Bundles\CompanyProfile\Parameters\CompanyProfileParameters;
use HappyInvite\Domain\Bundles\CompanyProfile\Service\CompanyProfileService;
use HappyInvite\REST\Bundles\Company\Tests\Fixtures\CompaniesFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class CompanyProfilesFixture implements Fixture
{
    private static $companyProfiles = [];

    public function up(Application $app, EntityManager $em)
    {
        self::$companyProfiles = [];

        /** @var CompanyProfileService $service */
        $service = $app->getContainer()->get(CompanyProfileService::class);
        $inn = 000;
        $ogrn = 10;
        $kpp = 200;
        $who_gives = 78567;
        $bank_bik = 231;
        $giro = 1231561;
        $bank_correspondent_account = 125125616;
        $okpo = 21451256;
        $okato = 12415125;
        $okved = 1241254;
        $legal_address = 2415;
        $real_address = 125125;
        $director_general = 12415;
        $accounting_general = 124124;
        $phone = 1241254;
        $fax = 1241254;
        $email = 124151;

        foreach (CompaniesFixture::getAll() as $index => $company) {
            self::$companyProfiles[$index] = $service->createCompanyProfile(new CompanyProfileParameters(
                $company,
                $inn,
                $kpp,
                $ogrn,
                $who_gives,
                $bank_bik,
                $giro,
                $bank_correspondent_account,
                $okpo,
                $okato,
                $okved,
                $legal_address,
                $real_address,
                $director_general,
                $accounting_general,
                $phone,
                $fax,
                $email
            ));
        }
    }

    public static function get(int $index): CompanyProfile
    {
        return self::$companyProfiles[$index];
    }
}