<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Tests\Methods;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\CompanyProfile\Tests\CompanyProfileMiddlewareTest;
use HappyInvite\REST\Bundles\CompanyProfile\Tests\Fixtures\CompanyProfilesFixture;

final class GetCompanyProfileMiddlewareTest extends CompanyProfileMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new CompanyProfilesFixture());
        $companyProfile = CompanyProfilesFixture::get(1);

        $this->requestCompanyProfileGet($companyProfile->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestCompanyProfileGet($companyProfile->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new CompanyProfilesFixture());

        $this->requestCompanyProfileGet(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new CompanyProfilesFixture());

        $companyProfile = CompanyProfilesFixture::get(1);

        $this->requestCompanyProfileGet($companyProfile->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);
    }
}