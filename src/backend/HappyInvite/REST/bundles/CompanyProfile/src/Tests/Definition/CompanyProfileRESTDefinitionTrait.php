<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait CompanyProfileRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestCompanyProfileSet (int $companyId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/company_profile/%d/set', $companyId))
            ->setParameters($json);
    }

    protected function requestCompanyProfileGet (int $companyId): RESTRequest
    {
        return $this->request('get', sprintf('/company_profile/%d/get', $companyId));
    }
}