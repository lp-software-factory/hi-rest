<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Company\Tests\Fixtures\CompaniesFixture;
use HappyInvite\REST\Bundles\CompanyProfile\Tests\Fixtures\CompanyProfilesFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class CompanyProfileMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
            new UserAccountFixture(),
            new CompaniesFixture(),
            new CompanyProfilesFixture()
        ];
    }
}