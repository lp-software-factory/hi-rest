<?php
namespace HappyInvite\REST\Bundles\CompanyProfile;

use HappyInvite\REST\Bundle\RESTBundle;

final class CompanyProfileRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}