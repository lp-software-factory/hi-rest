<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Middleware;

use HappyInvite\REST\Bundles\CompanyProfile\Middleware\Command\GetCompanyProfileCommand;
use HappyInvite\REST\Bundles\CompanyProfile\Middleware\Command\SetCompanyProfileCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class CompanyProfileMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('set', SetCompanyProfileCommand::class)
            ->attachDirect('get', GetCompanyProfileCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}