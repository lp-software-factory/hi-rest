<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Middleware\Command;

use HappyInvite\Domain\Bundles\CompanyProfile\Factory\Doctrine\CompanyProfileParametersFactory;
use HappyInvite\Domain\Bundles\CompanyProflie\Exceptions\CompanyProfileNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\CompanyProfile\Request\SetCompanyProfileRequest;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetCompanyProfileCommand extends AbstractCompanyProfileCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = $this->parametersFactory->createCompanyProfileParameters(array($request)
            );

            $companyProfile = $this->companyProfileService->editCompanyProfile($request->getAttribute('companyId'), $parameters);

            if ($this->companyService->hasCompanyWithId(false)) {
                $this->companyProfileService->createCompanyProfile($parameters);
            } else {
                $this->companyProfileService->editCompanyProfile($parameters->getCompany()->getId(), $parameters);
            }

            $responseBuilder
                ->setJSON([
                    'company_profile' => $companyProfile->toJSON(),
                ])
                ->setStatusSuccess();
        } catch (CompanyProfileNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }

}


