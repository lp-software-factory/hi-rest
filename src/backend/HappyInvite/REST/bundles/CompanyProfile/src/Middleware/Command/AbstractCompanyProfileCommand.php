<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Middleware\Command;

use HappyInvite\Domain\Bundles\Company\Service\CompanyService;
use HappyInvite\Domain\Bundles\CompanyProfile\Service\CompanyProfileService;
use HappyInvite\Domain\Bundles\CompanyProfile\Service\CompanyProfileValidationService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractCompanyProfileCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var CompanyProfileService */
    protected $companyProfileService;

    /** @var CompanyProfileValidationService */
    protected $companyProfileValidationService;

    /** @var CompanyService */
    protected $companyService;

    /** @var CompanyProfileParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        CompanyProfileService $companyProfileService,
        CompanyProfileValidationService $companyProfileValidationService,
        CompanyService $companyService,
        CompanyProfileParametersFactory $companyProfileParametersFactory
    )
    {
        $this->accessService = $accessService;
        $this->companyProfileService = $companyProfileService;
        $this->companyProfileValidationService = $companyProfileValidationService;
        $this->companyService = $companyService;
        $this->parametersFactory = $companyProfileParametersFactory;
    }
}
