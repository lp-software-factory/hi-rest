<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Middleware\Command;

use HappyInvite\Domain\Bundles\CompanyProflie\Exceptions\CompanyProfileNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetCompanyProfileCommand extends AbstractCompanyProfileCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $companyProfile = $this->companyProfileService->getCompanyProfileByCompanyId($request->getAttribute('companyId'));

            $responseBuilder
                ->setJSON([
                    'company_profile' => $companyProfile->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(CompanyProfileNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}