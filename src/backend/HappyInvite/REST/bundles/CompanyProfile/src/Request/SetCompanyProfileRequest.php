<?php
namespace HappyInvite\REST\Bundles\CompanyProfile\Request;


use HappyInvite\Domain\Bundles\CompanyProfile\Entity\CompanyProfile;
use HappyInvite\Domain\Bundles\CompanyProfile\Factory\Doctrine\CompanyProfileParametersFactory;
use HappyInvite\Domain\Bundles\CompanyProfile\Parameters\CompanyProfileParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;

final class SetCompanyProfileRequest extends SchemaRequest
{

    public function getParameters(): array
    {
        $json = $this->getData();
        return $json;
/*
        return (new CompanyProfileParameters(
            $json['company'],
            $json['inn'],
            $json['kpp'],
            $json['ogrn'],
            $json['who_gives'],
            $json['bank_bik'],
            $json['giro'],
            $json['bank_correspondent_account'],
            $json['okpo'],
            $json['okato'],
            $json['okved'],
            $json['legal_address'],
            $json['real_address'],
            $json['director_general'],
            $json['accounting_general'],
            $json['phone'],
            $json['fax'],
            $json['email']
        ));
*/
    }

protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_CompanyProfileRESTBundle_CompanyProfileRequest');
    }
}
