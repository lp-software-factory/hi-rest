<?php
namespace HappyInvite\REST\Bundles\Money\Request;

use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Domain\Bundles\Money\Criteria\ListPriceCriteria;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class ListPriceRequest extends SchemaRequest
{
    public function getParameters(): ListPriceCriteria
    {
        $data = $this->getData();

        return new ListPriceCriteria(
            new SeekCriteria(ListPriceCriteria::$MAX_LIMIT, $data['seek']['offset'], $data['seek']['limit']),
            new SortCriteria($data['order']['field'], $data['order']['direction'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_MoneyBundle_ListPriceRequest');
    }
}