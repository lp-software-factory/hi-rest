<?php
namespace HappyInvite\REST\Bundles\Money\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Parameters\CreateCurrencyParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateCurrencyRequest extends SchemaRequest
{
    public function getParameters(): CreateCurrencyParameters
    {
        $data = $this->getData();

        return new CreateCurrencyParameters(
            $data['code'],
            $data['sign'],
            new ImmutableLocalizedString($data['title'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_MoneyBundle_CreateCurrencyRequest');
    }
}