<?php
namespace HappyInvite\REST\Bundles\Money\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Parameters\EditCurrencyParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditCurrencyRequest extends SchemaRequest
{
    public function getParameters(): EditCurrencyParameters
    {
        $data = $this->getData();

        return new EditCurrencyParameters(
            $data['code'],
            $data['sign'],
            new ImmutableLocalizedString($data['title'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_MoneyBundle_EditCurrencyRequest');
    }
}