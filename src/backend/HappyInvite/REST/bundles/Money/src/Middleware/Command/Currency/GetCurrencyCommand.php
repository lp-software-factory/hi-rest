<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Currency;

use HappyInvite\Domain\Bundles\Money\Exceptions\CurrencyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetCurrencyCommand extends AbstractCurrencyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $currency = $this->currencyService->getById($request->getAttribute('currencyId'));

            $responseBuilder
                ->setJSON([
                    'currency' => $currency->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(CurrencyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}