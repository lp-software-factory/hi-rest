<?php
namespace HappyInvite\REST\Bundles\Money\Middleware;

use HappyInvite\REST\Bundles\Money\Middleware\Command\Currency\CreateCurrencyCommand;
use HappyInvite\REST\Bundles\Money\Middleware\Command\Currency\DeleteCurrencyCommand;
use HappyInvite\REST\Bundles\Money\Middleware\Command\Currency\EditCurrencyCommand;
use HappyInvite\REST\Bundles\Money\Middleware\Command\Currency\GetCurrencyCommand;
use HappyInvite\REST\Bundles\Money\Middleware\Command\Currency\ListCurrencyCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class CurrencyMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateCurrencyCommand::class)
            ->attachDirect('delete', DeleteCurrencyCommand::class)
            ->attachDirect('get', GetCurrencyCommand::class)
            ->attachDirect('list', ListCurrencyCommand::class)
            ->attachDirect('edit', EditCurrencyCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}