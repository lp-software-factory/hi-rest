<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Currency;

use HappyInvite\Domain\Bundles\Money\Exceptions\DuplicateCurrencyCodeException;
use HappyInvite\REST\Bundles\Money\Request\CreateCurrencyRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateCurrencyCommand extends AbstractCurrencyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new CreateCurrencyRequest($request))->getParameters();

            $currency = $this->currencyService->createCurrency($parameters);

            $responseBuilder
                ->setJSON([
                    'currency' => $currency->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DuplicateCurrencyCodeException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}