<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Currency;

use HappyInvite\Domain\Bundles\Money\Service\CurrencyService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractCurrencyCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var CurrencyService */
    protected $currencyService;

    public function __construct(AccessService $accessService, CurrencyService $currencyService)
    {
        $this->accessService = $accessService;
        $this->currencyService = $currencyService;
    }
}