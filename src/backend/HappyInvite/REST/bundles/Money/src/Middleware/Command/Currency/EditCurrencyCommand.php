<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Currency;

use HappyInvite\Domain\Bundles\Money\Exceptions\CurrencyNotFoundException;
use HappyInvite\Domain\Bundles\Money\Exceptions\DuplicateCurrencyCodeException;
use HappyInvite\REST\Bundles\Money\Request\EditCurrencyRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditCurrencyCommand extends AbstractCurrencyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new EditCurrencyRequest($request))->getParameters();

            $currency = $this->currencyService->editCurrency($request->getAttribute('currencyId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'currency' => $currency->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(CurrencyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(DuplicateCurrencyCodeException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}