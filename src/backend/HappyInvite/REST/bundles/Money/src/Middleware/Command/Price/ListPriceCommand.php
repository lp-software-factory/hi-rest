<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Price;

use HappyInvite\Domain\Bundles\Money\Entity\Price;
use HappyInvite\REST\Bundles\Money\Request\ListPriceRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListPriceCommand extends AbstractPriceCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $criteria = (new ListPriceRequest($request))->getParameters();
        $prices = $this->priceService->getPrices($criteria);

        $responseBuilder
            ->setJSON([
                'prices' => array_map(function(Price $price) {
                    return $price->toJSON();
                }, $prices)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}