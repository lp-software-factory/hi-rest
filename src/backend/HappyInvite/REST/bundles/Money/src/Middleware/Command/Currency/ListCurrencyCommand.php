<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Currency;

use HappyInvite\Domain\Bundles\Money\Entity\Currency;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListCurrencyCommand extends AbstractCurrencyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $responseBuilder
            ->setJSON([
                'currencies' => array_map(function(Currency $currency) {
                    return $currency->toJSON();
                }, $this->currencyService->getAll())
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}