<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Price;

use HappyInvite\Domain\Bundles\Money\Service\PriceService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractPriceCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var PriceService */
    protected $priceService;

    public function __construct(AccessService $accessService, PriceService $priceService)
    {
        $this->accessService = $accessService;
        $this->priceService = $priceService;
    }
}