<?php
namespace HappyInvite\REST\Bundles\Money\Middleware\Command\Currency;

use HappyInvite\Domain\Bundles\Money\Exceptions\CurrencyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteCurrencyCommand extends AbstractCurrencyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->currencyService->deleteCurrency($request->getAttribute('currencyId'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(CurrencyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}