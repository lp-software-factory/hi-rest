<?php
namespace HappyInvite\REST\Bundles\Money;

use HappyInvite\REST\Bundle\RESTBundle;

final class MoneyRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}