<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Paths\Currency;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Money\Tests\Fixture\CurrencyFixture;
use HappyInvite\REST\Bundles\Money\Tests\MoneyMiddlewareTestCase;

final class EditCurrencyMiddlewareTest extends MoneyMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();
        $json = [
            'code' => 'USD_M',
            'sign' => '$$',
            'title' => [['region' => 'en_US', 'value' => '* USD Currency']]
        ];

        $this->requestCurrencyEdit($currency->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestCurrencyEdit($currency->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new CurrencyFixture());

        $json = [
            'code' => 'USD_M',
            'sign' => '$$',
            'title' => [['region' => 'en_US', 'value' => '* USD Currency']]
        ];

        $this->requestCurrencyEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test409()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();
        $json = [
            'code' => CurrencyFixture::getCurrencyRUB()->getCode(),
            'sign' => '$$',
            'title' => [['region' => 'en_US', 'value' => '* USD Currency']]
        ];

        $this->requestCurrencyEdit($currency->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => false,
                'error' => $this->expectString(),
            ])
        ;
    }

    public function test200DifferenceCode()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();
        $json = [
            'code' => 'USD_M',
            'sign' => '$$',
            'title' => [['region' => 'en_US', 'value' => '* USD Currency']]
        ];

        $this->requestCurrencyEdit($currency->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'currency' => [
                    'id' => $this->expectId(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'title' => $json['title'],
                    'sign' => $json['sign'],
                    'code' => $json['code']
                ]
            ])
        ;
    }

    public function test200SameCode()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();
        $json = [
            'code' => CurrencyFixture::getCurrencyUSD()->getCode(),
            'sign' => '$$',
            'title' => [['region' => 'en_US', 'value' => '* USD Currency']]
        ];

        $this->requestCurrencyEdit($currency->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'currency' => [
                    'id' => $this->expectId(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'title' => $json['title'],
                    'sign' => $json['sign'],
                    'code' => $json['code']
                ]
            ])
        ;
    }
}