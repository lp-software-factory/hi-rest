<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Paths\Price;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Money\Tests\MoneyMiddlewareTestCase;

final class ListPriceMiddlewareTest extends MoneyMiddlewareTestCase
{
    public function test403()
    {
        $criteria = [
            'seek' => [
                'offset' => 0,
                'limit' => 100
            ],
            'order' => [
                'field' => 'id',
                'direction' => 'desc'
            ]
        ];

        $this->requestPriceList($criteria)
            ->__invoke()
            ->expectAuthError();

        $this->requestPriceList($criteria)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $criteria = [
            'seek' => [
                'offset' => 0,
                'limit' => 100
            ],
            'order' => [
                'field' => 'id',
                'direction' => 'desc'
            ]
        ];

        $this->requestPriceList($criteria)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType();
    }
}