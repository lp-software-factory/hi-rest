<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait MoneyRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestCurrencyCreate(array $json): RESTRequest
    {
        return $this->request('put', '/money/currency/create')
            ->setParameters($json);
    }

    protected function requestCurrencyList(): RESTRequest
    {
        return $this->request('get', '/money/currency/list');
    }

    protected function requestCurrencyEdit(int $currencyId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/money/currency/%d/edit', $currencyId))
            ->setParameters($json);
    }

    protected function requestCurrencyGet(int $currencyId): RESTRequest
    {
        return $this->request('get', sprintf('/money/currency/%d/get', $currencyId));
    }

    protected function requestCurrencyDelete(int $currencyId): RESTRequest
    {
        return $this->request('delete', sprintf('/money/currency/%d/delete', $currencyId));
    }

    protected function requestPriceList(array $json): RESTRequest
    {
        return $this->request('post', '/money/price/list')
            ->setParameters($json);
    }
}