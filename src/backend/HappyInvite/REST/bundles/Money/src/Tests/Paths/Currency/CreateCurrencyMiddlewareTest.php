<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Paths\Currency;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Money\Tests\Fixture\CurrencyFixture;
use HappyInvite\REST\Bundles\Money\Tests\MoneyMiddlewareTestCase;

final class CreateCurrencyMiddlewareTest extends MoneyMiddlewareTestCase
{
    public function test403()
    {
        $json = [
            'code' => 'USD',
            'sign' => '$',
            'title' => [['region' => 'en_US', 'value' => 'USD Currency']]
        ];

        $this->requestCurrencyCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestCurrencyCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test409()
    {
        $this->upFixture(new CurrencyFixture());

        $json = [
            'code' => CurrencyFixture::getCurrencyUSD()->getCode(),
            'sign' => '$',
            'title' => [['region' => 'en_US', 'value' => 'USD Currency']]
        ];

        $this->requestCurrencyCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(409)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => false,
                'error' => $this->expectString(),
            ])
        ;
    }

    public function test200()
    {
        $json = [
            'code' => 'USD',
            'sign' => '$',
            'title' => [['region' => 'en_US', 'value' => 'USD Currency']]
        ];

        $this->requestCurrencyCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'currency' => [
                    'id' => $this->expectId(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'title' => $json['title'],
                    'sign' => $json['sign'],
                    'code' => $json['code']
                ]
            ])
        ;
    }
}