<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Paths\Currency;

use HappyInvite\REST\Bundles\Money\Tests\Fixture\CurrencyFixture;
use HappyInvite\REST\Bundles\Money\Tests\MoneyMiddlewareTestCase;

final class ListCurrencyMiddlewareTest extends MoneyMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();

        $this->requestCurrencyList()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'currencies' => [
                    CurrencyFixture::getCurrencyUSD()->toJSON(),
                    CurrencyFixture::getCurrencyRUB()->toJSON(),
                ],
            ]);
    }
}