<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Paths\Currency;

use HappyInvite\REST\Bundles\Money\Tests\Fixture\CurrencyFixture;
use HappyInvite\REST\Bundles\Money\Tests\MoneyMiddlewareTestCase;

final class GetCurrencyMiddlewareTest extends MoneyMiddlewareTestCase
{
    public function test404()
    {
        $this->requestCurrencyGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();

        $this->requestCurrencyGet($currency->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'currency' => $currency->toJSON(),
            ]);
    }
}