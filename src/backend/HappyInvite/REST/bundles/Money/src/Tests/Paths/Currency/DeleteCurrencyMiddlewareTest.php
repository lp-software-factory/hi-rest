<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Paths\Currency;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Money\Tests\Fixture\CurrencyFixture;
use HappyInvite\REST\Bundles\Money\Tests\MoneyMiddlewareTestCase;

final class DeleteCurrencyMiddlewareTest extends MoneyMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();

        $this->requestCurrencyDelete($currency->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestCurrencyDelete($currency->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new CurrencyFixture());

        $this->requestCurrencyDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new CurrencyFixture());

        $currency = CurrencyFixture::getCurrencyUSD();
        $currencyId = $currency->getId();

        $this->requestCurrencyGet($currencyId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestCurrencyDelete($currencyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestCurrencyGet($currencyId)
            ->__invoke()
            ->expectStatusCode(404);
    }
}