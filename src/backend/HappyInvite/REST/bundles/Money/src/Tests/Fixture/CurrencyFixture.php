<?php
namespace HappyInvite\REST\Bundles\Money\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;
use HappyInvite\Domain\Bundles\Money\Parameters\CreateCurrencyParameters;
use HappyInvite\Domain\Bundles\Money\Service\CurrencyService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class CurrencyFixture implements Fixture
{
    /** @var Currency */
    private static $currencyRUB;

    /** @var Currency */
    private static $currencyUSD;

    public function up(Application $app, EntityManager $em)
    {
        /** @var CurrencyService $service */
        $service = $app->getContainer()->get(CurrencyService::class);

        self::$currencyUSD = $service->createCurrency(new CreateCurrencyParameters(
            'USD', '$', new ImmutableLocalizedString([
                ['region' => 'en_US', 'value' => 'USD Currency']
            ])
        ));

        self::$currencyRUB = $service->createCurrency(new CreateCurrencyParameters(
            'RUB', '₽', new ImmutableLocalizedString([
                ['region' => 'en_US', 'value' => 'RUB Currency']
            ])
        ));
    }

    public static function getCurrencyRUB(): Currency
    {
        return self::$currencyRUB;
    }

    public static function getCurrencyUSD(): Currency
    {
        return self::$currencyUSD;
    }
}