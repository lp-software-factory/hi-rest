<?php
namespace HappyInvite\REST\Bundles\Money;

use HappyInvite\REST\Bundles\Money\Middleware\CurrencyMiddleware;
use HappyInvite\REST\Bundles\Money\Middleware\PriceMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/money/currency/{command:create}[/]',
                'middleware' => CurrencyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/money/currency/{command:list}[/]',
                'middleware' => CurrencyMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/money/currency/{currencyId}/{command:edit}[/]',
                'middleware' => CurrencyMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/money/currency/{currencyId}/{command:delete}[/]',
                'middleware' => CurrencyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/money/currency/{currencyId}/{command:get}[/]',
                'middleware' => CurrencyMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/money/price/{command:list}[/]',
                'middleware' => PriceMiddleware::class,
            ],
        ],
    ],
];