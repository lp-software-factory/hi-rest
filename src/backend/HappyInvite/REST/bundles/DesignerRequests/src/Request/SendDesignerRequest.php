<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Request;

use HappyInvite\Domain\Bundles\Designer\Form\DesignerRequestForm;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SendDesignerRequest extends SchemaRequest
{
    public function getParameters(): DesignerRequestForm
    {
        $form = $this->getData()['form'];

        return new DesignerRequestForm(
            $form['first_name'],
            $form['last_name'],
            $form['email'],
            $form['phone'],
            $form['link_portfolio'],
            $form['description'],
            $form['are_terms_accepted']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_DesignerBundle_SendDesignerRequest');
    }
}