<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Request;

use HappyInvite\Domain\Bundles\Designer\Criteria\ListDesignersCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class ListDesignersRequest extends SchemaRequest
{
    public function getParameters(): ListDesignersCriteria
    {
        $criteria = $this->getData()['criteria'];

        return new ListDesignersCriteria(
            new SeekCriteria(ListDesignersCriteria::MAX_LIMIT, $criteria['seek']['offset'], $criteria['seek']['limit']),
            new SortCriteria($criteria['sort']['field'], $criteria['sort']['direction'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_DesignerBundle_ListDesignersRequest');
    }
}