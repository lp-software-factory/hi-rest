<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Tests\Paths;

use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\DesignerRequestsMiddlewareTest;

final class SendDesignerRequestMiddlewareTest extends DesignerRequestsMiddlewareTest
{
    public function test409()
    {
        $json = [
            'form' => [
                'first_name' => 'Foo',
                'last_name' => 'Bar',
                'email' => 'foo@bar.com',
                'phone' => '+7 800 9000000',
                'link_portfolio' => 'https://example.com/designer',
                'description' => "I'm designer",
                'are_terms_accepted' => false,
            ],
        ];

        $this->requestDesignerRequestsSend($json)
            ->__invoke()
            ->expectStatusCode(409)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => false,
                'error' => $this->expectString()
            ]);
    }

    public function test200()
    {
        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $spool->clear();

        $this->assertEquals(0, $spool->count());

        $json = [
            'form' => [
                'first_name' => 'Foo',
                'last_name' => 'Bar',
                'email' => 'foo@bar.com',
                'phone' => '+7 800 9000000',
                'link_portfolio' => 'https://example.com/designer',
                'description' => "I'm designer",
                'are_terms_accepted' => true,
            ],
        ];

        $this->requestDesignerRequestsSend($json)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'designer_request' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'is_reviewed' => false,
                    'is_accepted' => false,
                    'is_declined' => false,
                    'form' => $json['form'],
                ]
            ]);

        $this->assertEquals(1, $spool->count());
    }
}