<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Tests\Paths;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\DesignerRequestsMiddlewareTest;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture\DesignerRequestsFixture;

final class ListDesignerRequestsMiddlewareTest extends DesignerRequestsMiddlewareTest
{
    public function test403()
    {
        $json = [
            'criteria' => [
                'seek' => [
                    'limit' => 100,
                    'offset' => 0,
                ],
                'sort' => [
                    'field' => 'id',
                    'direction' => 'asc'
                ]
            ]
        ];

        $this->requestDesignerRequestsList($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestDesignerRequestsList($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $json = [
            'criteria' => [
                'seek' => [
                    'limit' => 100,
                    'offset' => 0,
                ],
                'sort' => [
                    'field' => 'id',
                    'direction' => 'asc'
                ]
            ]
        ];

        $expected = Chain::create($fixture->getOrderedRequests())
            ->sort(function(DesignerRequest $a, DesignerRequest $b) {
                return $a->getId() > $b->getId();
            })
            ->map(function(DesignerRequest $request) {
                return $request->toJSON();
            })
            ->array;

        $this->requestDesignerRequestsList($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => $json['criteria'],
                'total' => $fixture->getCountRequests(),
                'designer_requests' => $expected,
            ]);
    }

    public function test200IdDesc()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $json = [
            'criteria' => [
                'seek' => [
                    'limit' => 100,
                    'offset' => 0,
                ],
                'sort' => [
                    'field' => 'id',
                    'direction' => 'desc'
                ]
            ]
        ];

        $expected = Chain::create($fixture->getOrderedRequests())
            ->sort(function(DesignerRequest $a, DesignerRequest $b) {
                return $a->getId() < $b->getId();
            })
            ->map(function(DesignerRequest $request) {
                return $request->toJSON();
            })
            ->array;

        $this->requestDesignerRequestsList($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => $json['criteria'],
                'total' => $fixture->getCountRequests(),
                'designer_requests' => $expected,
            ]);
    }

    public function test200Limit()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $json = [
            'criteria' => [
                'seek' => [
                    'limit' => 3,
                    'offset' => 1,
                ],
                'sort' => [
                    'field' => 'id',
                    'direction' => 'desc'
                ]
            ]
        ];

        $expected = Chain::create($fixture->getOrderedRequests())
            ->sort(function(DesignerRequest $a, DesignerRequest $b) {
                return $a->getId() < $b->getId();
            })
            ->map(function(DesignerRequest $request) {
                return $request->toJSON();
            });

        $expected->slice(1, 3);

        $this->requestDesignerRequestsList($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'criteria' => $json['criteria'],
                'total' => $fixture->getCountRequests(),
                'designer_requests' => $expected->array,
            ]);
    }
}