<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Tests\Paths;

use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\DesignerRequestsMiddlewareTest;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture\DesignerRequestsFixture;

final class DeclineDesignerRequestMiddlewareTest extends DesignerRequestsMiddlewareTest
{
    public function test403()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $this->requestDesignerRequestsDecline($fixture->getRequest(1)->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestDesignerRequestsDecline($fixture->getRequest(1)->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $this->requestDesignerRequestsDecline(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $request = $fixture->getRequest(1);


        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $spool->clear();

        $this->assertEquals(0, $spool->count());

        $this->requestDesignerRequestsDecline($request->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'designer_request' => $request->toJSON()
            ])
            ->expectJSONBody([
                'designer_request' => [
                    'is_reviewed' => true,
                    'is_accepted' => false,
                    'is_declined' => true,
                    'date_reviewed' => $this->expectDate(),
                ]
            ]);

        $this->assertEquals(1, $spool->count());
    }
}