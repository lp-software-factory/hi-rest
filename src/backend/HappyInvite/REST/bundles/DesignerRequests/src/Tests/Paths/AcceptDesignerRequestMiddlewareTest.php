<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Tests\Paths;

use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\DesignerRequestsMiddlewareTest;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture\DesignerRequestsFixture;

final class AcceptDesignerRequestMiddlewareTest extends DesignerRequestsMiddlewareTest
{
    public function test403()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $this->requestDesignerRequestsAccept($fixture->getRequest(1)->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestDesignerRequestsAccept($fixture->getRequest(1)->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture($fixture = new DesignerRequestsFixture());

        $this->requestDesignerRequestsAccept(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200NewAccount()
    {
        $json = [
            'form' => [
                'first_name' => 'Foo',
                'last_name' => 'Bar',
                'email' => 'foo@bar.com',
                'phone' => '+7 800 9000000',
                'link_portfolio' => 'https://example.com/designer',
                'description' => "I'm designer",
                'are_terms_accepted' => true,
            ],
        ];

        $designerRequestId = $this->requestDesignerRequestsSend($json)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'designer_request' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'is_reviewed' => false,
                    'is_accepted' => false,
                    'is_declined' => false,
                    'form' => $json['form'],
                ]
            ])
            ->fetch(function(array $json) {
                return $json['designer_request']['id'];
            });

        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $spool->clear();

        $this->assertEquals(0, $spool->count());

        $this->requestDesignerRequestsAccept($designerRequestId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'designer_request' => [
                    'id' => $designerRequestId,
                ]
            ])
            ->expectJSONBody([
                'designer_request' => [
                    'is_reviewed' => true,
                    'is_accepted' => true,
                    'is_declined' => false,
                    'date_reviewed' => $this->expectDate(),
                ]
            ]);

        $this->assertEquals(2, $spool->count());
    }

    public function test200ExistingAccount()
    {
        $json = [
            'form' => [
                'first_name' => 'Foo',
                'last_name' => 'Bar',
                'email' => UserAccountFixture::getAccount()->getEmail(),
                'phone' => '+7 800 9000000',
                'link_portfolio' => 'https://example.com/designer',
                'description' => "I'm designer",
                'are_terms_accepted' => true,
            ],
        ];

        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $spool->clear();

        $this->assertEquals(0, $spool->count());

        $designerRequestId = $this->requestDesignerRequestsSend($json)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'designer_request' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'date_created_at' => $this->expectDate(),
                    'last_updated_on' => $this->expectDate(),
                    'is_reviewed' => false,
                    'is_accepted' => false,
                    'is_declined' => false,
                    'form' => $json['form'],
                ]
            ])
            ->fetch(function(array $json) {
                return $json['designer_request']['id'];
            });

        $this->assertEquals(1, $spool->count());

        $this->requestDesignerRequestsAccept($designerRequestId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'designer_request' => [
                    'id' => $designerRequestId,
                ]
            ])
            ->expectJSONBody([
                'designer_request' => [
                    'is_reviewed' => true,
                    'is_accepted' => true,
                    'is_declined' => false,
                    'date_reviewed' => $this->expectDate(),
                ]
            ]);

        $this->assertEquals(2, $spool->count());
    }
}