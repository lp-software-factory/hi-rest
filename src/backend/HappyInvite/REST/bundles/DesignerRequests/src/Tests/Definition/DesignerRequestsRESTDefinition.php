<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait DesignerRequestsRESTDefinition
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestDesignerRequestsSend(array $json): RESTRequest
    {
        return $this->request('put', '/designer-requests/send')
            ->setParameters($json);
    }

    protected function requestDesignerRequestsList(array $json): RESTRequest
    {
        return $this->request('post', '/designer-requests/list')
            ->setParameters($json);
    }

    protected function requestDesignerRequestsAccept(int $designerRequestId): RESTRequest
    {
        return $this->request('post', sprintf('/designer-requests/%s/accept', $designerRequestId));
    }

    protected function requestDesignerRequestsDecline(int $designerRequestId): RESTRequest
    {
        return $this->request('post', sprintf('/designer-requests/%s/decline', $designerRequestId));
    }
}