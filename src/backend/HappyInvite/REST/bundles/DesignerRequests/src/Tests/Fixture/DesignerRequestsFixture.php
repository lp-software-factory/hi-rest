<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Form\DesignerRequestForm;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class DesignerRequestsFixture implements Fixture
{
    /** @var DesignerRequest[] */
    private static $requests = [];

    public function up(Application $app, EntityManager $em)
    {
        self::$requests = [];

        $service = $app->getContainer()->get(DesignerFormService::class); /** @var DesignerFormService $service */

        for($i = 0; $i <= 5; $i++) {
            self::$requests[] = $service->sendDesignerRequestForm(new DesignerRequestForm(
                'Foo',
                'Bar',
                sprintf('%s@example.com', GenerateRandomString::generate(5)),
                '+79008000000',
                sprintf('http://foo.bar/%s/link/to/portfolio', GenerateRandomString::generate(5)),
                'TRIGGERED',
                true
            ));
        }
    }

    public static function getRequest(int $index): DesignerRequest
    {
        return self::$requests[$index - 1];
    }

    public static function getOrderedRequests(): array
    {
        return self::$requests;
    }

    public static function getCountRequests(): int
    {
        return count(self::$requests);
    }
}