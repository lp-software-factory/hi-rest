<?php
namespace HappyInvite\REST\Bundles\DesignerRequests;

use HappyInvite\REST\Bundle\RESTBundle;

final class DesignerRequestsRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}