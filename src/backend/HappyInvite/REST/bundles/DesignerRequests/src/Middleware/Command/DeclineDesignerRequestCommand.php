<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerRequestNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeclineDesignerRequestCommand extends AbstractDesignerCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $designerRequest = $this->designerFormService->declineDesignerRequest($request->getAttribute('designerRequestId'));

            $responseBuilder
                ->setJSON([
                    'designer_request' => $designerRequest->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DesignerRequestNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}