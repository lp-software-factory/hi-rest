<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerRequestNotFoundException;
use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerTermsAreNotAcceptedException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class AcceptDesignerRequestCommand extends AbstractDesignerCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $designerRequest = $this->designerFormService->acceptDesignerRequest($request->getAttribute('designerRequestId'));

            $responseBuilder
                ->setJSON([
                    'designer_request' => $designerRequest->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DesignerRequestNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(DesignerTermsAreNotAcceptedException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}