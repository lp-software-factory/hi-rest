<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Middleware;

use HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command\AcceptDesignerRequestCommand;
use HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command\DeclineDesignerRequestCommand;
use HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command\ListDesignersCommand;
use HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command\SendDesignerFormCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class DesignerRequestsMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('send', SendDesignerFormCommand::class)
            ->attachDirect('list', ListDesignersCommand::class)
            ->attachDirect('accept', AcceptDesignerRequestCommand::class)
            ->attachDirect('decline', DeclineDesignerRequestCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}