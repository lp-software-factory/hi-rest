<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\REST\Bundles\DesignerRequests\Request\ListDesignersRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListDesignersCommand extends AbstractDesignerCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $criteria = (new ListDesignersRequest($request))->getParameters();
        $entities = $this->designerFormService->listDesignerRequests($criteria);
        $total = $this->designerFormService->countDesignerRequests($criteria);

        $responseBuilder
            ->setJSON([
                'designer_requests' => array_map(function(DesignerRequest $designerRequest) {
                    return $designerRequest->toJSON();
                }, $entities),
                'criteria'  => $criteria->toJSON(),
                'total' => $total,
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}