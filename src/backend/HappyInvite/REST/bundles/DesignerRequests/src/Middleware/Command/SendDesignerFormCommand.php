<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerDuplicateEmailException;
use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerTermsAreNotAcceptedException;
use HappyInvite\REST\Bundles\DesignerRequests\Request\SendDesignerRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SendDesignerFormCommand extends AbstractDesignerCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $form = (new SendDesignerRequest($request))->getParameters();

            $request = $this->designerFormService->sendDesignerRequestForm($form);

            $responseBuilder
                ->setJSON([
                    'designer_request' => $request->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DesignerTermsAreNotAcceptedException | DesignerDuplicateEmailException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}