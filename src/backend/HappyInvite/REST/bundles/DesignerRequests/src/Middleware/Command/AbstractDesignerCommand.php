<?php
namespace HappyInvite\REST\Bundles\DesignerRequests\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractDesignerCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var DesignerFormService */
    protected $designerFormService;

    public function __construct(AccessService $accessService, DesignerFormService $designerFormService)
    {
        $this->accessService = $accessService;
        $this->designerFormService = $designerFormService;
    }
}