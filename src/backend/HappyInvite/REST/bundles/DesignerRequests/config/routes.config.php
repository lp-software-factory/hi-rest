<?php
namespace HappyInvite\REST\Bundles\DesignerRequests;

use HappyInvite\REST\Bundles\DesignerRequests\Middleware\DesignerRequestsMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/designer-requests/{command:send}[/]',
                'middleware' => DesignerRequestsMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/designer-requests/{command:list}[/]',
                'middleware' => DesignerRequestsMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/designer-requests/{designerRequestId}/{command:accept}[/]',
                'middleware' => DesignerRequestsMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/designer-requests/{designerRequestId}/{command:decline}[/]',
                'middleware' => DesignerRequestsMiddleware::class,
            ],
        ],
    ],
];