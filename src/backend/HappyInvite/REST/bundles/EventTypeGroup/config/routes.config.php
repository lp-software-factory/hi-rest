<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup;

use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\EventTypeGroupMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/event/type/groups/{command:create}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/type/groups/{id}/{command:edit}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/event/type/groups/{id}/{command:delete}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/type/groups/{id}/move/{command:up}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/event/type/groups/{id}/move/{command:down}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/type/groups/{command:all}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/type/groups/{id}/{command:get}[/]',
                'middleware' => EventTypeGroupMiddleware::class,
            ],
        ],
    ],
];