<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Exceptions\EventTypeGroupNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetByIdETGCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $eventTypeGroupId = $request->getAttribute('id');

            $eventTypeGroup = $this->eventTypeGroupService->getEventTypeGroupById($eventTypeGroupId);

            $responseBuilder
                ->setJSON([
                    'event_type_group' => $this->eventTypeGroupFormatter->formatOne($eventTypeGroup),
                ])
                ->setStatusSuccess();
        }catch(EventTypeGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}