<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Formatter\EventTypeGroupFormatter;

abstract class Command implements \HappyInvite\REST\Command\Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService */
    protected $eventTypeGroupService;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Formatter\EventTypeGroupFormatter */
    protected $eventTypeGroupFormatter;

    public function __construct(
        AccessService $accessService,
        EventTypeGroupService $eventTypeGroupService,
        EventTypeGroupFormatter $formatter
    ) {
        $this->accessService = $accessService;
        $this->eventTypeGroupService = $eventTypeGroupService;
        $this->eventTypeGroupFormatter = $formatter;
    }

}