<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Entity\EventTypeGroup;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllETGCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $groups = $this->eventTypeGroupService->getAllEventTypeGroups();

        $responseBuilder
            ->setJSON([
                'event_type_groups' => $this->eventTypeGroupFormatter->formatMany($groups),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}