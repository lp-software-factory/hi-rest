<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command;

use HappyInvite\REST\Bundles\EventTypeGroup\Request\CreateEventTypeGroupRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateETGCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = (new CreateEventTypeGroupRequest($request))->getParameters();
        $eventTypeGroup = $this->eventTypeGroupService->createEventTypeGroup($parameters);

        $responseBuilder
            ->setJSON([
                'event_type_group' => $this->eventTypeGroupFormatter->formatOne($eventTypeGroup),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}