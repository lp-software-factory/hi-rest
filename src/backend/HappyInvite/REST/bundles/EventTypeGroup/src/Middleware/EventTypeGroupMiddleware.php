<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Middleware;

use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\CreateETGCommand;
use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\DeleteETGCommand;
use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\EditETGCommand;
use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\GetAllETGCommand;
use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\GetByIdETGCommand;
use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\MoveDownETGCommand;
use HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command\MoveUpETGCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class EventTypeGroupMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateETGCommand::class)
            ->attachDirect('edit', EditETGCommand::class)
            ->attachDirect('delete', DeleteETGCommand::class)
            ->attachDirect('get', GetByIdETGCommand::class)
            ->attachDirect('all', GetAllETGCommand::class)
            ->attachDirect('up', MoveUpETGCommand::class)
            ->attachDirect('down', MoveDownETGCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}