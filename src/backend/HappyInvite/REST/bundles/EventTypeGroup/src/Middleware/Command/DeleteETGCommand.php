<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Middleware\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Exceptions\EventTypeGroupNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteETGCommand extends Command
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $eventTypeGroupId = $request->getAttribute('id');

            $this->eventTypeGroupService->deleteEventTypeGroup($eventTypeGroupId);

            $responseBuilder->setStatusSuccess();
        }catch(EventTypeGroupNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}