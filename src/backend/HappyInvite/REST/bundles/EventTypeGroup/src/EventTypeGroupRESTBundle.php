<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup;

use HappyInvite\REST\Bundle\RESTBundle;

final class EventTypeGroupRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}