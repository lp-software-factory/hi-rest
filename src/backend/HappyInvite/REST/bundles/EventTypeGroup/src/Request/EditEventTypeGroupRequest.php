<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters\EditEventTypeGroupParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditEventTypeGroupRequest extends SchemaRequest
{
    public function getParameters(): EditEventTypeGroupParameters
    {
        $data = $this->getData();

        return new EditEventTypeGroupParameters(
            $data['url'],
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EventTypeGroupBundle_EditEventTypeGroupRequest');
    }
}