<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters\CreateEventTypeGroupParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEventTypeGroupRequest extends SchemaRequest
{
    public function getParameters(): CreateEventTypeGroupParameters
    {
        $data = $this->getData();

        return new CreateEventTypeGroupParameters(
            $data['url'],
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EventTypeGroupBundle_CreateEventTypeGroupRequest');
    }
}