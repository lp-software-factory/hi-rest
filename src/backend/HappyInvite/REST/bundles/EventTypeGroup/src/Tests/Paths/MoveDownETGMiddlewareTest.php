<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\EventTypeGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class MoveDownETGMiddlewareTest extends EventTypeGroupMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $etg = EventTypeGroupsFixture::getETGWedding();

        $this->requestEventTypeGroupMoveDown($etg->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeGroupMoveDown($etg->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventTypeGroupMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $this->assertPosition(1, EventTypeGroupsFixture::getETGWedding()->getId());
        $this->assertPosition(2, EventTypeGroupsFixture::getETGBirthday()->getId());
        $this->assertPosition(3, EventTypeGroupsFixture::getETGBusiness()->getId());

        $this->requestEventTypeGroupMoveDown(EventTypeGroupsFixture::getETGBusiness()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3,
            ]);

        $this->assertPosition(1, EventTypeGroupsFixture::getETGWedding()->getId());
        $this->assertPosition(2, EventTypeGroupsFixture::getETGBirthday()->getId());
        $this->assertPosition(3, EventTypeGroupsFixture::getETGBusiness()->getId());

        $this->requestEventTypeGroupMoveDown(EventTypeGroupsFixture::getETGBirthday()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3,
            ]);

        $this->assertPosition(1, EventTypeGroupsFixture::getETGWedding()->getId());
        $this->assertPosition(2, EventTypeGroupsFixture::getETGBusiness()->getId());
        $this->assertPosition(3, EventTypeGroupsFixture::getETGBirthday()->getId());

        $this->requestEventTypeGroupMoveDown(EventTypeGroupsFixture::getETGBirthday()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3,
            ]);

        $this->assertPosition(1, EventTypeGroupsFixture::getETGWedding()->getId());
        $this->assertPosition(2, EventTypeGroupsFixture::getETGBusiness()->getId());
        $this->assertPosition(3, EventTypeGroupsFixture::getETGBirthday()->getId());

        $this->requestEventTypeGroupMoveDown(EventTypeGroupsFixture::getETGWedding()->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2,
            ]);

        $this->assertPosition(1, EventTypeGroupsFixture::getETGBusiness()->getId());
        $this->assertPosition(2, EventTypeGroupsFixture::getETGWedding()->getId());
        $this->assertPosition(3, EventTypeGroupsFixture::getETGBirthday()->getId());
    }
}