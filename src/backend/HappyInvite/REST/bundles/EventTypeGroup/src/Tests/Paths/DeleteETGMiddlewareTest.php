<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\EventTypeGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class DeleteETGMiddlewareTest extends EventTypeGroupMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $etg = EventTypeGroupsFixture::getETGBirthday();

        $this->requestEventTypeGroupDelete($etg->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeGroupDelete($etg->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestEventTypeGroupDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $etgId = EventTypeGroupsFixture::getETGBirthday()->getId();

        $this->requestEventTypeGroupGet($etgId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type_group' => [
                    'id' => $etgId,
                ]
            ]);

        $this->requestEventTypeGroupDelete($etgId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEventTypeGroupGet($etgId)
            ->__invoke()
            ->expectNotFoundError();
    }
}