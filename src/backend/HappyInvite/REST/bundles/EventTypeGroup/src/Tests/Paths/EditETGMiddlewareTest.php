<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\EventTypeGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class EditETGMiddlewareTest extends EventTypeGroupMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'url' => 'birthday_',
            'title' => [
                ['region' => 'en_GB', 'value' => '* Birthday']
            ],
            'description' => [
                ['region' => 'en_GB', 'value' => '* Birthday events']
            ],
        ];
    }

    public function test403()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $etg = EventTypeGroupsFixture::getETGBirthday();
        $json = $this->getTestJSON();

        $this->requestEventTypeGroupEdit($etg->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeGroupCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $json = $this->getTestJSON();

        $this->requestEventTypeGroupEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $etg = EventTypeGroupsFixture::getETGBirthday();
        $json = $this->getTestJSON();

        $this->requestEventTypeGroupEdit($etg->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type_group' => [
                    'id' => $etg->getId(),
                    'sid' => $etg->getSID(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                ]
            ])
        ;

        $this->requestEventTypeGroupGet($etg->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type_group' => [
                    'id' => $etg->getId(),
                    'sid' => $etg->getSID(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                ]
            ]);
    }
}