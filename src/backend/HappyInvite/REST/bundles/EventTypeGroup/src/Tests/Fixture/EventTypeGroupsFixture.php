<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters\CreateEventTypeGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EventTypeGroupsFixture implements Fixture
{
    private static $etgWedding;
    private static $etgBirthday;
    private static $etgBusiness;

    public function up(Application $app, EntityManager $em)
    {
        /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService $etgService */
        $etgService = $app->getContainer()->get(EventTypeGroupService::class);

        self::$etgWedding = $etgService->createEventTypeGroup(new CreateEventTypeGroupParameters(
            'wedding',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Wedding']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Wedding events']])
        ));

        self::$etgBirthday = $etgService->createEventTypeGroup(new CreateEventTypeGroupParameters(
            'birthday',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Birthday events']])
        ));

        self::$etgBusiness = $etgService->createEventTypeGroup(new CreateEventTypeGroupParameters(
            'business',
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Business']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Business']])
        ));
    }

    public static function getETGWedding(): EventTypeGroup
    {
        return self::$etgWedding;
    }

    public static function getETGBirthday(): EventTypeGroup
    {
        return self::$etgBirthday;
    }

    public static function getETGBusiness(): EventTypeGroup
    {
        return self::$etgBusiness;
    }
}