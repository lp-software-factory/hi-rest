<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EventTypeGroupRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEventTypeGroupCreate(array $json): RESTRequest
    {
        return $this->request('put', '/event/type/groups/create')
            ->setParameters($json);
    }

    protected function requestEventTypeGroupEdit(int $id, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/event/type/groups/%d/edit', $id))
            ->setParameters($json);
    }

    protected function requestEventTypeGroupDelete(int $id): RESTRequest
    {
        return $this->request('delete', sprintf('/event/type/groups/%d/delete', $id));
    }

    protected function requestEventTypeGroupGet(int $id): RESTRequest
    {
        return $this->request('get', sprintf('/event/type/groups/%d/get', $id));
    }

    protected function requestEventTypeGroupGetAll(): RESTRequest
    {
        return $this->request('get', '/event/type/groups/all');
    }

    protected function requestEventTypeGroupMoveUp(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/event/type/groups/%d/move/up', $id));
    }

    protected function requestEventTypeGroupMoveDown(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/event/type/groups/%d/move/down', $id));
    }
}