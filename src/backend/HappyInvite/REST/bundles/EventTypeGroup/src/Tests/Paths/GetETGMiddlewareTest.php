<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Paths;

use HappyInvite\REST\Bundles\EventTypeGroup\Tests\EventTypeGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class GetETGMiddlewareTest extends EventTypeGroupMiddlewareTestCase
{
    public function test404()
    {
        $this->requestEventTypeGroupGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $etg = EventTypeGroupsFixture::getETGBirthday();

        $this->requestEventTypeGroupGet($etg->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type_group' => $etg->toJSON(),
            ]);
    }
}