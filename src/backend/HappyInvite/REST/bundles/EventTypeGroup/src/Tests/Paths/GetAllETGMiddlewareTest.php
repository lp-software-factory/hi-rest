<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Paths;

use HappyInvite\REST\Bundles\EventTypeGroup\Tests\EventTypeGroupMiddlewareTestCase;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;

final class GetAllETGMiddlewareTest extends EventTypeGroupMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EventTypeGroupsFixture());

        $this->requestEventTypeGroupGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type_groups' => function(array $input) {
                    foreach($input as $json) {
                        $this->assertTrue(is_array($json));
                        $this->assertTrue(isset($json['id']));
                    }
                }
            ]);
    }
}