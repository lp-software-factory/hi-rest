<?php
namespace HappyInvite\REST\Bundles\EventTypeGroup\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\EventTypeGroupMiddlewareTestCase;

final class CreateETGMiddlewareTest extends EventTypeGroupMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'url' => 'birthday',
            'title' => [
                ['region' => 'en_GB', 'value' => 'Birthday']
            ],
            'description' => [
                ['region' => 'en_GB', 'value' => 'Birthday events']
            ],
        ];
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEventTypeGroupCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestEventTypeGroupCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEventTypeGroupCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'event_type_group' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'description' => $json['description'],
                ]
            ])
        ;
    }
}