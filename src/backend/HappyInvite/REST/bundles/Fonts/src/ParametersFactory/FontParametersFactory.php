<?php
namespace HappyInvite\REST\Bundles\Fonts\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Fonts\Parameters\FontParameters;
use HappyInvite\REST\Bundles\Fonts\Request\FontRequest;
use Psr\Http\Message\ServerRequestInterface;

final class FontParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function factoryCreateFontParameters(ServerRequestInterface $request): FontParameters
    {
        return $this->factoryFontParameters($request);
    }

    public function factoryEditFontParameters(ServerRequestInterface $request): FontParameters
    {
        return $this->factoryFontParameters($request);
    }

    private function factoryFontParameters(ServerRequestInterface $request): FontParameters
    {
        $json = (new FontRequest($request))->getParameters();

        return new FontParameters(
            $this->attachmentService->getById($json['preview']),
            $json['title'],
            $json['supports'],
            $json['font_face'],
            $json['license'],
            $json['files'],
            $json['is_activated']
        );
    }
}