<?php
namespace HappyInvite\REST\Bundles\Fonts\Tests\Paths;

use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Fonts\Tests\FontsMiddlewareTestCase;

final class CreateFontMiddlewareTest extends FontsMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'title' => 'Lucida Console',
            'supports' => Font::MODE_NORMAL | Font::MODE_BOLD,
            'font_face' => '@font-face {}',
            'license' => 'Some license here',
            'preview' => AttachmentFixture::$attachmentImage->getId(),
            'files' => [
                AttachmentFixture::$attachmentImage->getId(),
                AttachmentFixture::$attachmentVideo->getId(),
            ],
            'is_activated' => true,
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestFontCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'font' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'title' => $json['title'],
                        'font_face' => $json['font_face'],
                        'license' => $json['license'],
                        'preview' => [
                            'id' => $json['preview'],
                        ],
                        'files' => [
                            [
                                'id' => $json['files'][0]
                            ],
                            [
                                'id' => $json['files'][1]
                            ],
                        ],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestFontCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestFontCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}