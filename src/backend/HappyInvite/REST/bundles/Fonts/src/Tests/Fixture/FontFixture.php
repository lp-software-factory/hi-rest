<?php
namespace HappyInvite\REST\Bundles\Fonts\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\Domain\Bundles\Fonts\Parameters\FontParameters;
use HappyInvite\Domain\Bundles\Fonts\Service\FontService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class FontFixture implements Fixture
{
    /** @var Font */
    public static $fontArial;

    /** @var Font */
    public static $fontSans;

    /** @var Font */
    public static $fontRoman;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(FontService::class); /** @var FontService $service */

        self::$fontArial = $service->createFont(new FontParameters(
            AttachmentFixture::$attachmentImage,
            'Arial',
            (Font::MODE_NORMAL | Font::MODE_BOLD | Font::MODE_BOLD_ITALIC | Font::MODE_ITALIC),
            '{ font-family: Arial; font-weight: normal; }',
            'Some license here',
            [AttachmentFixture::$attachmentImage->getId()]
        , true));

        self::$fontSans = $service->createFont(new FontParameters(
            AttachmentFixture::$attachmentImage,
            'PT Sans',
            (Font::MODE_NORMAL | Font::MODE_BOLD | Font::MODE_BOLD_ITALIC | Font::MODE_ITALIC),
            '{ font-family: "PT Sans"; font-weight: normal; }',
            'Some license here',
            [AttachmentFixture::$attachmentImage->getId()]
        , true));

        self::$fontRoman = $service->createFont(new FontParameters(
            AttachmentFixture::$attachmentImage,
            'Times New Roman',
            (Font::MODE_NORMAL | Font::MODE_BOLD),
            '{ font-family: "Times New Roman"; font-weight: normal; }',
            'Some license here',
            [AttachmentFixture::$attachmentImage->getId()]
        , true));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$fontArial,
            self::$fontSans,
            self::$fontRoman,
        ];
    }
}