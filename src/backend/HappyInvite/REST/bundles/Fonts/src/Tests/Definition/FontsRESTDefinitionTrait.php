<?php
namespace HappyInvite\REST\Bundles\Fonts\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait FontsRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestFontCreate(array $json): RESTRequest
    {
        return $this->request('put', '/fonts/create')
            ->setParameters($json);
    }

    protected function requestFontDelete(int $fontId): RESTRequest
    {
        return $this->request('delete', sprintf('/fonts/%d/delete', $fontId));
    }

    protected function requestFontEdit(int $fontId, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/fonts/%d/edit', $fontId))
            ->setParameters($json);
    }

    protected function requestFontGet(int $fontId): RESTRequest
    {
        return $this->request('get', sprintf('/fonts/%s/get', $fontId));
    }

    protected function requestFontGetAll(): RESTRequest
    {
        return $this->request('get', sprintf('/fonts/get-all'));
    }
}