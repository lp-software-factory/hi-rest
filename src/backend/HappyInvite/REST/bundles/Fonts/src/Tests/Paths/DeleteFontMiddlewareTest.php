<?php
namespace HappyInvite\REST\Bundles\Fonts\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Fonts\Tests\Fixture\FontFixture;
use HappyInvite\REST\Bundles\Fonts\Tests\FontsMiddlewareTestCase;

final class DeleteFontMiddlewareTest extends FontsMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new FontFixture());

        $font = FontFixture::$fontArial;
        $fontId = $font->getId();

        $this->requestFontGet($fontId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestFontDelete($fontId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        $this->requestFontGet($fontId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new FontFixture());

        $font = FontFixture::$fontArial;
        $fontId = $font->getId();

        $this->requestFontDelete($fontId)
            ->__invoke()
            ->expectAuthError();

        $this->requestFontDelete($fontId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new FontFixture());

        $this->requestFontDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}