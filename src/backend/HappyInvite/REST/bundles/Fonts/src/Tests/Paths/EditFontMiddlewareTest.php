<?php
namespace HappyInvite\REST\Bundles\Fonts\Tests\Paths;

use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Fonts\Tests\Fixture\FontFixture;
use HappyInvite\REST\Bundles\Fonts\Tests\FontsMiddlewareTestCase;

final class EditFontMiddlewareTest extends FontsMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'title' => 'Lucida Console',
            'supports' => Font::MODE_NORMAL | Font::MODE_BOLD,
            'font_face' => '@font-face {}',
            'license' => 'Some license here',
            'preview' => AttachmentFixture::$attachmentImage->getId(),
            'files' => [
                AttachmentFixture::$attachmentImage->getId(),
                AttachmentFixture::$attachmentVideo->getId(),
            ],
            'is_activated' => true,
        ];
    }

    public function test200()
    {
        $this->upFixture(new FontFixture());

        $font = FontFixture::$fontArial;

        $this->requestFontEdit($font->getId(), $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'font' => [
                    'entity' => [
                        'id' => $font->getId(),
                        'sid' => $font->getSID(),
                        'title' => $json['title'],
                        'font_face' => $json['font_face'],
                        'license' => $json['license'],
                        'preview' => [
                            'id' => $json['preview'],
                        ],
                        'files' => [
                            [
                                'id' => $json['files'][0]
                            ],
                            [
                                'id' => $json['files'][1]
                            ],
                        ],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new FontFixture());

        $font = FontFixture::$fontArial;

        $this->requestFontEdit($font->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestFontEdit($font->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new FontFixture());

        $this->requestFontEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}