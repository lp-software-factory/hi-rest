<?php
namespace HappyInvite\REST\Bundles\Fonts\Tests\Paths;

use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\REST\Bundles\Fonts\Tests\Fixture\FontFixture;
use HappyInvite\REST\Bundles\Fonts\Tests\FontsMiddlewareTestCase;

final class GetAllFontMiddlewareTest extends FontsMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new FontFixture());

        $ids = array_map(function(Font $font) {
            return $font->getId();
        }, FontFixture::getOrderedList());

        $compareIds = $this->requestFontGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $input) {
                    return $input['entity']['id'];
                }, $json['fonts']);
            });

        $this->assertEquals($ids, $compareIds);
    }
}