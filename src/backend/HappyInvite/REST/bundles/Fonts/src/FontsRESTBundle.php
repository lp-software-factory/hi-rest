<?php
namespace HappyInvite\REST\Bundles\Fonts;

use HappyInvite\REST\Bundle\RESTBundle;

final class FontsRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}