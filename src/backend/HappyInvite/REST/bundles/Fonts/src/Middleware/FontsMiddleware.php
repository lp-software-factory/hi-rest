<?php
namespace HappyInvite\REST\Bundles\Fonts\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font\CreateFontCommand;
use HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font\DeleteFontCommand;
use HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font\EditFontCommand;
use HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font\GetAllFontCommand;
use HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font\GetByIdFontCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class FontsMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateFontCommand::class)
            ->attachDirect('delete', DeleteFontCommand::class)
            ->attachDirect('edit', EditFontCommand::class)
            ->attachDirect('get', GetByIdFontCommand::class)
            ->attachDirect('get-all', GetAllFontCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}