<?php
namespace HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllFontCommand extends AbstractFontCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $fonts = $this->fontService->getAll();

        $responseBuilder
            ->setJSON([
                'fonts' => $this->fontFormatter->formatMany($fonts),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}