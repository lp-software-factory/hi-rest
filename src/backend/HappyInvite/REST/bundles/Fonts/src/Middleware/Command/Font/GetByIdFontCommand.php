<?php
namespace HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font;

use HappyInvite\Domain\Bundles\Fonts\Exceptions\FontNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetByIdFontCommand extends AbstractFontCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $fontId = $request->getAttribute('fontId');

            $font = $this->fontService->getFontById($fontId);

            $responseBuilder
                ->setJSON([
                    'font' => $this->fontFormatter->formatOne($font),
                ])
                ->setStatusSuccess();
        }catch(FontNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}