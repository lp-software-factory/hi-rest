<?php
namespace HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Fonts\Request\FontRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateFontCommand extends AbstractFontCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $font = $this->fontService->createFont(
            $this->parametersFactory->factoryCreateFontParameters($request)
        );

        $responseBuilder
            ->setJSON([
                'font' => $this->fontFormatter->formatOne($font),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}