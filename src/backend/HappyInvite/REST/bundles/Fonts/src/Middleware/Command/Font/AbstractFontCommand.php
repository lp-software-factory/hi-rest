<?php
namespace HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font;

use HappyInvite\Domain\Bundles\Fonts\Service\FontService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\Domain\Bundles\Fonts\Formatter\FontFormatter;
use HappyInvite\REST\Bundles\Fonts\ParametersFactory\FontParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractFontCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var FontService */
    protected $fontService;

    /** @var FontFormatter */
    protected $fontFormatter;

    /** @var FontParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        FontService $fontService,
        FontFormatter $fontFormatter,
        FontParametersFactory $parametersFactory
    ) {
        $this->accessService = $accessService;
        $this->fontService = $fontService;
        $this->fontFormatter = $fontFormatter;
        $this->parametersFactory = $parametersFactory;
    }
}