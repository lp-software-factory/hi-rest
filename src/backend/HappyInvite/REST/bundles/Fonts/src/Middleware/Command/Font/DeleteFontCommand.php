<?php
namespace HappyInvite\REST\Bundles\Fonts\Middleware\Command\Font;

use HappyInvite\Domain\Bundles\Fonts\Exceptions\FontNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteFontCommand extends AbstractFontCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $fontId = $request->getAttribute('fontId');

            $this->fontService->deleteFont($fontId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(FontNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}