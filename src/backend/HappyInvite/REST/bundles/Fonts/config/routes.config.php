<?php
namespace HappyInvite\REST\Bundles\Fonts;

use HappyInvite\REST\Bundles\Fonts\Middleware\FontsMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/fonts/{command:create}[/]',
                'middleware' => FontsMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/fonts/{fontId}/{command:delete}[/]',
                'middleware' => FontsMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/fonts/{fontId}/{command:edit}[/]',
                'middleware' => FontsMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/fonts/{fontId}/{command:get}[/]',
                'middleware' => FontsMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/fonts/{command:get-all}[/]',
                'middleware' => FontsMiddleware::class,
            ],
        ],
    ],
];