<?php
namespace HappyInvite\REST\Bundles\Profile\Middleware\Command;

use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class AbstractProfileCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var ProfileService */
    protected $profileService;

    public function __construct(AccessService $accessService, ProfileService $profileService)
    {
        $this->accessService = $accessService;
        $this->profileService = $profileService;
    }
}