<?php
namespace HappyInvite\REST\Bundles\Profile\Middleware\Command;

use HappyInvite\Domain\Bundles\Profile\Exceptions\ProfileNotFoundException;
use HappyInvite\REST\Bundles\Profile\Request\ProfileImageUploadRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ProfileImageUploadCommand extends AbstractProfileCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $profileId = $request->getAttribute('profileId');

        $this->accessService->requireOwner($profileId);

        try {
            $image = $this->profileService->uploadProfileImage($profileId, (new ProfileImageUploadRequest($request))->getParameters());

            $responseBuilder
                ->setJSON([
                    'image' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch (ProfileNotFoundException $e) {
            $responseBuilder
                ->setStatusNotFound()
                ->setError($e);
        }

        return $responseBuilder->build();
    }
}