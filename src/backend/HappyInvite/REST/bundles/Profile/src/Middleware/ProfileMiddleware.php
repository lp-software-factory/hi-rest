<?php
namespace HappyInvite\REST\Bundles\Profile\Middleware;

use HappyInvite\REST\Bundles\Profile\Middleware\Command\ProfileImageGenerateCommand;
use HappyInvite\REST\Bundles\Profile\Middleware\Command\ProfileImageUploadCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class ProfileMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('image-upload', ProfileImageUploadCommand::class)
            ->attachDirect('image-regenerate', ProfileImageGenerateCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}