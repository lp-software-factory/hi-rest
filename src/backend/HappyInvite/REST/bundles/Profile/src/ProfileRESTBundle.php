<?php
namespace HappyInvite\REST\Bundles\Profile;

use HappyInvite\REST\Bundle\RESTBundle;

final class ProfileRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}