<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeBackdroprRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeBackdropCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/backdrop/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeBackdropEdit(int $envelopeBackdropId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/%d/edit', $envelopeBackdropId))
            ->setParameters($json);
    }

    protected function requestEnvelopeBackdropDelete(int $envelopeBackdropId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/backdrop/%d/delete', $envelopeBackdropId));
    }

    protected function requestEnvelopeBackdropMoveUp(int $envelopeBackdropId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/%d/move-up', $envelopeBackdropId));
    }

    protected function requestEnvelopeBackdropMoveDown(int $envelopeBackdropId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/%d/move-down', $envelopeBackdropId));
    }

    protected function requestEnvelopeBackdropActivate(int $envelopeBackdropId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/%d/activate', $envelopeBackdropId));
    }

    protected function requestEnvelopeBackdropDeactivate(int $envelopeBackdropId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/%d/deactivate', $envelopeBackdropId));
    }

    protected function requestEnvelopeBackdropGetById(int $envelopeBackdropId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/backdrop/%d/get', $envelopeBackdropId));
    }

    protected function requestEnvelopeBackdropGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/backdrop/get-all');
    }
}