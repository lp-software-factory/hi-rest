<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions\EnvelopeBackdropNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Request\EditEnvelopeBackdropRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditEnvelopeBackdropCommand extends AbstractEnvelopeBackdropCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeBackdropId = $request->getAttribute('envelopeBackdropId');

            $envelopeBackdrop = $this->service->editEnvelopeBackdrop(
                $envelopeBackdropId, $this->parametersFactory->factoryEditEnvelopeBackdropParameters($envelopeBackdropId, (new EditEnvelopeBackdropRequest($request))->getParameters())
            );

            $responseBuilder
                ->setJSON([
                    'envelope_backdrop' => $this->formatter->formatOne($envelopeBackdrop),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeBackdropNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}