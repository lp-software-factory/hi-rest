<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\EnvelopeBackdropMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;

final class DeleteEnvelopeBackdropMiddlewareTest extends EnvelopeBackdropMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $envelopeFixture = EnvelopeBackdropFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopeBackdropDelete($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopeBackdropGetById($envelopeFixtureId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeBackdropFixture());
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeBackdropFixture());
    }
}