<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\CreateEnvelopeBackdropParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeBackdropFixture implements Fixture
{
    /** @var EnvelopeBackdrop */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeBackdrop */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeBackdrop */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeBackdropService::class); /** @var EnvelopeBackdropService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeBackdrop(new CreateEnvelopeBackdropParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop 1 Description"]])
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopeBackdrop(new CreateEnvelopeBackdropParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop 2 Description"]])
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeBackdrop(new CreateEnvelopeBackdropParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop 3 Description"]])
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}