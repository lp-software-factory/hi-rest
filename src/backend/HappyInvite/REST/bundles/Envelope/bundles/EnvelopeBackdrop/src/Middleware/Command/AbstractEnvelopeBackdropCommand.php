<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Formatter\EnvelopeBackdropFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\ParametersFactory\EnvelopeBackdropParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeBackdropCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeBackdropService */
    protected $service;

    /** @var EnvelopeBackdropFormatter */
    protected $formatter;

    /** @var EnvelopeBackdropParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        EnvelopeBackdropService $service,
        EnvelopeBackdropFormatter $formatter,
        EnvelopeBackdropParametersFactory $parametersFactory
    ) {
        $this->accessService = $accessService;
        $this->service = $service;
        $this->formatter = $formatter;
        $this->parametersFactory = $parametersFactory;
    }
}