<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Request\CreateEnvelopeBackdropRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeBackdropCommand extends AbstractEnvelopeBackdropCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeBackdrop = $this->service->createEnvelopeBackdrop(
            $this->parametersFactory->factoryCreateEnvelopeBackdropParameters((new CreateEnvelopeBackdropRequest($request))->getParameters())
        );

        $responseBuilder
            ->setJSON([
                'envelope_backdrop' => $this->formatter->formatOne($envelopeBackdrop),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}