<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeBackdropCommand extends AbstractEnvelopeBackdropCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeBackdrops = $this->service->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_backdrops' => $this->formatter->formatMany($envelopeBackdrops),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}