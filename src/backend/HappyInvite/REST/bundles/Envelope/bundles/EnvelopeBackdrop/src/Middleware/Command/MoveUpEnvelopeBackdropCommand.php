<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions\EnvelopeBackdropNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class MoveUpEnvelopeBackdropCommand extends AbstractEnvelopeBackdropCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeBackdropId = $request->getAttribute('envelopeBackdropId');

            $newPosition = $this->service->moveUpEnvelopeBackdrop($envelopeBackdropId);

            $responseBuilder
                ->setJSON([
                    'position' => $newPosition,
                ])
                ->setStatusSuccess();
        }catch(EnvelopeBackdropNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}