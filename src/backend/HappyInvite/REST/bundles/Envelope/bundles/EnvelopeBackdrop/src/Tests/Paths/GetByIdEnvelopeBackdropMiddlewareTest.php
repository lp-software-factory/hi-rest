<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\EnvelopeBackdropMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;

final class GetByIdEnvelopeBackdropMiddlewareTest extends EnvelopeBackdropMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $envelopeFixture = EnvelopeBackdropFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_backdrop' => [
                    'entity' => [
                        'id' => $this->expectId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $this->requestEnvelopeBackdropGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}