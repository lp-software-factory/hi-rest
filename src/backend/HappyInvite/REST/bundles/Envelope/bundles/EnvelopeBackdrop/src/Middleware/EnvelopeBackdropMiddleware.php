<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\ActivateEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\CreateEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\DeactivateEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\DeleteEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\EditEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\GetAllEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\GetEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\MoveDownEnvelopeBackdropCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\Command\MoveUpEnvelopeBackdropCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeBackdropMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeBackdropCommand::class)
            ->attachDirect('edit', EditEnvelopeBackdropCommand::class)
            ->attachDirect('delete', DeleteEnvelopeBackdropCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeBackdropCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeBackdropCommand::class)
            ->attachDirect('activate', ActivateEnvelopeBackdropCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeBackdropCommand::class)
            ->attachDirect('get', GetEnvelopeBackdropCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeBackdropCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}