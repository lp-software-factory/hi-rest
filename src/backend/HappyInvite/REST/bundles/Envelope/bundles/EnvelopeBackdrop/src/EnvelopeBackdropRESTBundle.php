<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeBackdropRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}