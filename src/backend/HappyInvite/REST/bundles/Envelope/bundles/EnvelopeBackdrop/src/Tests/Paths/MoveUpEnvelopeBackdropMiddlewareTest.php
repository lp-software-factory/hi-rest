<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\EnvelopeBackdropMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;

final class MoveUpEnvelopeBackdropMiddlewareTest extends EnvelopeBackdropMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $this->assertEnvelopeBackdropPosition(1, EnvelopeBackdropFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropPosition(2, EnvelopeBackdropFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeBackdropPosition(3, EnvelopeBackdropFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeBackdropMoveUp(EnvelopeBackdropFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeBackdropPosition(1, EnvelopeBackdropFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropPosition(2, EnvelopeBackdropFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeBackdropPosition(3, EnvelopeBackdropFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeBackdropMoveUp(EnvelopeBackdropFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeBackdropPosition(1, EnvelopeBackdropFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeBackdropPosition(2, EnvelopeBackdropFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropPosition(3, EnvelopeBackdropFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeBackdropMoveUp(EnvelopeBackdropFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeBackdropPosition(1, EnvelopeBackdropFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeBackdropPosition(2, EnvelopeBackdropFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeBackdropPosition(3, EnvelopeBackdropFixture::$envelopeFixtureEF1->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $envelopeFixture = EnvelopeBackdropFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropMoveUp($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeBackdropMoveUp($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $this->requestEnvelopeBackdropMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}