<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Middleware\EnvelopeBackdropMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/backdrop/{command:create}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:edit}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:delete}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:activate}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:deactivate}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:move-up}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:move-down}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/backdrop/{envelopeBackdropId}/{command:get}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/backdrop/{command:get-all}[/]',
                'middleware' => EnvelopeBackdropMiddleware::class,
            ],
        ]
    ]
];