<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\EnvelopeBackdropMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;

final class EditEnvelopeBackdropMiddlewareTest extends EnvelopeBackdropMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "* My Envelope Backdrop"]],
            "description" => [["region" => "en_GB", "value" => "* My Envelope Backdrop Description"]],
            'preview_attachment_id' => AttachmentFixture::$attachmentImage->getId(),
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeBackdropFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_backdrop' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ],
                    ],
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeBackdropFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeBackdropEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeBackdropFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopeBackdropEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}