<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\EnvelopeBackdropMiddlewareTestCase;

final class CreateEnvelopeBackdropMiddlewareTest extends EnvelopeBackdropMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "My Envelope Backdrop"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Backdrop Description"]],
            "preview_attachment_id" => AttachmentFixture::$attachmentLink->getId(),
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeBackdropCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_backdrop' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ],
                        'position' => 1,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeBackdropCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeBackdropCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}