<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\CreateEnvelopeBackdropParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\EditEnvelopeBackdropParameters;

final class EnvelopeBackdropParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function factoryCreateEnvelopeBackdropParameters(array $json): CreateEnvelopeBackdropParameters
    {
        return new CreateEnvelopeBackdropParameters(
            $this->attachmentService->getById($json['preview_attachment_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }

    public function factoryEditEnvelopeBackdropParameters(int $id, array $json): EditEnvelopeBackdropParameters
    {
        return new EditEnvelopeBackdropParameters(
            $this->attachmentService->getById($json['preview_attachment_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }
}