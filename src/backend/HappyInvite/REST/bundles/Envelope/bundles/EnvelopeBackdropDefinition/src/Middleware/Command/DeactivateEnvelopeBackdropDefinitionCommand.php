<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions\EnvelopeBackdropDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateEnvelopeBackdropDefinitionCommand extends AbstractEnvelopeBackdropDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeBackdropDefinitionId = $request->getAttribute('envelopeBackdropDefinitionId');

            $envelopeBackdropDefinition = $this->service->deactivateEnvelopeBackdropDefinition($envelopeBackdropDefinitionId);

            $responseBuilder
                ->setJSON([
                    'envelope_backdrop_definition' => $this->formatter->formatOne($envelopeBackdropDefinition),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeBackdropDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}