<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions\EnvelopeBackdropDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEnvelopeBackdropDefinitionCommand extends AbstractEnvelopeBackdropDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeBackdropDefinitionId = $request->getAttribute('envelopeBackdropDefinitionId');

            $this->service->deleteEnvelopeBackdropDefinition($envelopeBackdropDefinitionId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(EnvelopeBackdropDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}