<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeBackdropDefinitionCommand extends AbstractEnvelopeBackdropDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeBackdropDefinitions = $this->service->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_backdrop_definitions' => $this->formatter->formatMany($envelopeBackdropDefinitions),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}