<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Formatter\EnvelopeBackdropDefinitionFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropDefinitionService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\ParametersFactory\EnvelopeBackdropDefinitionParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeBackdropDefinitionCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeBackdropDefinitionService */
    protected $service;

    /** @var EnvelopeBackdropDefinitionFormatter */
    protected $formatter;

    /** @var EnvelopeBackdropDefinitionParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        EnvelopeBackdropDefinitionService $service,
        EnvelopeBackdropDefinitionFormatter $formatter,
        EnvelopeBackdropDefinitionParametersFactory $parametersFactory
    ) {
        $this->accessService = $accessService;
        $this->service = $service;
        $this->formatter = $formatter;
        $this->parametersFactory = $parametersFactory;
    }
}