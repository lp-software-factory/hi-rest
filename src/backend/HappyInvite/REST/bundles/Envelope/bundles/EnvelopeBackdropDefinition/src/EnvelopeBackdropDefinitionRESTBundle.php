<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeBackdropDefinitionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}