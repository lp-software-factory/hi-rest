<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\ActivateEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\CreateEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\DeactivateEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\DeleteEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\EditEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\GetAllEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\GetEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\MoveDownEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command\MoveUpEnvelopeBackdropDefinitionCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeBackdropDefinitionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('edit', EditEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('delete', DeleteEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('activate', ActivateEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('get', GetEnvelopeBackdropDefinitionCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeBackdropDefinitionCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}