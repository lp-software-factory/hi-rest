<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\EnvelopeBackdropDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;

final class GetAllEnvelopeBackdropDefinitionMiddlewareTest extends EnvelopeBackdropDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $ids = $this->requestEnvelopeBackdropDefinitionGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_backdrop_definitions' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_backdrop_definitions']);
            });

        $this->assertEquals(array_map(function(EnvelopeBackdropDefinition $envelopeBackdropDefinition) {
            return $envelopeBackdropDefinition->getId();
        }, EnvelopeBackdropDefinitionFixture::getOrderedList()), $ids);
    }
}