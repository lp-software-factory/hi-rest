<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Request\CreateEnvelopeBackdropDefinitionRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeBackdropDefinitionCommand extends AbstractEnvelopeBackdropDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeBackdropDefinition = $this->service->createEnvelopeBackdropDefinition(
            $this->parametersFactory->factoryCreateEnvelopeBackdropDefinitionParameters((new CreateEnvelopeBackdropDefinitionRequest($request))->getParameters())
        );

        $responseBuilder
            ->setJSON([
                'envelope_backdrop_definition' => $this->formatter->formatOne($envelopeBackdropDefinition),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}