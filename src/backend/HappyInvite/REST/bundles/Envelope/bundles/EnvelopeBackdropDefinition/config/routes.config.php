<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Middleware\EnvelopeBackdropDefinitionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/backdrop/definition/{command:create}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:edit}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:delete}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:activate}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:deactivate}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:move-up}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:move-down}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/backdrop/definition/{envelopeBackdropDefinitionId}/{command:get}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/backdrop/definition/{command:get-all}[/]',
                'middleware' => EnvelopeBackdropDefinitionMiddleware::class,
            ],
        ]
    ]
];