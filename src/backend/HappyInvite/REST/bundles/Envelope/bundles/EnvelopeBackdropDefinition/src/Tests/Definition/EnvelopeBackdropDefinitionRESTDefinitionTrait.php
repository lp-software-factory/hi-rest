<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeBackdropDefinitionRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeBackdropDefinitionCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/backdrop/definition/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeBackdropDefinitionEdit(int $envelopeBackdropDefinitionId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/definition/%d/edit', $envelopeBackdropDefinitionId))
            ->setParameters($json);
    }

    protected function requestEnvelopeBackdropDefinitionDelete(int $envelopeBackdropDefinitionId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/backdrop/definition/%d/delete', $envelopeBackdropDefinitionId));
    }

    protected function requestEnvelopeBackdropDefinitionMoveUp(int $envelopeBackdropDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/definition/%d/move-up', $envelopeBackdropDefinitionId));
    }

    protected function requestEnvelopeBackdropDefinitionMoveDown(int $envelopeBackdropDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/definition/%d/move-down', $envelopeBackdropDefinitionId));
    }

    protected function requestEnvelopeBackdropDefinitionActivate(int $envelopeBackdropDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/definition/%d/activate', $envelopeBackdropDefinitionId));
    }

    protected function requestEnvelopeBackdropDefinitionDeactivate(int $envelopeBackdropDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/backdrop/definition/%d/deactivate', $envelopeBackdropDefinitionId));
    }

    protected function requestEnvelopeBackdropDefinitionGetById(int $envelopeBackdropDefinitionId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/backdrop/definition/%d/get', $envelopeBackdropDefinitionId));
    }

    protected function requestEnvelopeBackdropDefinitionGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/backdrop/definition/get-all');
    }
}