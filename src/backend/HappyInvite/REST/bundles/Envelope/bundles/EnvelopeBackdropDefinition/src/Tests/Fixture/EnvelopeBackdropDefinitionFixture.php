<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition\CreateEnvelopeBackdropDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropDefinitionService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeBackdropDefinitionFixture implements Fixture
{
    /** @var EnvelopeBackdropDefinition */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeBackdropDefinition */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeBackdropDefinition */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeBackdropDefinitionService::class); /** @var EnvelopeBackdropDefinitionService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeBackdropDefinition(new CreateEnvelopeBackdropDefinitionParameters(
            EnvelopeBackdropFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop Definition 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop Definition 1 Description"]]),
            AttachmentFixture::$attachmentImage,
            AttachmentFixture::$attachmentImage2
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopeBackdropDefinition(new CreateEnvelopeBackdropDefinitionParameters(
            EnvelopeBackdropFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop Definition 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop Definition 2 Description"]]),
            AttachmentFixture::$attachmentImage3,
            AttachmentFixture::$attachmentImage
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeBackdropDefinition(new CreateEnvelopeBackdropDefinitionParameters(
            EnvelopeBackdropFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop Definition 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Backdrop Definition 3 Description"]]),
            AttachmentFixture::$attachmentImage2,
            AttachmentFixture::$attachmentImage3
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}