<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\EnvelopeBackdropDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;

final class MoveDownEnvelopeBackdropDefinitionMiddlewareTest extends EnvelopeBackdropDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $this->assertEnvelopeBackdropDefinitionPosition(1, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(2, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(3, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeBackdropDefinitionMoveDown(EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopeBackdropDefinitionPosition(1, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(2, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(3, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeBackdropDefinitionMoveDown(EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopeBackdropDefinitionPosition(1, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(2, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(3, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2->getId());

        $this->requestEnvelopeBackdropDefinitionMoveDown(EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeBackdropDefinitionPosition(1, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(2, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeBackdropDefinitionPosition(3, EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $envelopeFixture = EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropDefinitionMoveDown($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeBackdropDefinitionMoveDown($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $this->requestEnvelopeBackdropDefinitionMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}