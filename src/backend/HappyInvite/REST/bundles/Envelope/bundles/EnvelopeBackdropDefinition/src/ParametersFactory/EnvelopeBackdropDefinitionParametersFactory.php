<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition\CreateEnvelopeBackdropDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition\EditEnvelopeBackdropDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropService;

final class EnvelopeBackdropDefinitionParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeBackdropService */
    private $envelopeBackdropService;

    public function __construct(AttachmentService $attachmentService, EnvelopeBackdropService $envelopeBackdropService)
    {
        $this->attachmentService = $attachmentService;
        $this->envelopeBackdropService = $envelopeBackdropService;
    }

    public function factoryCreateEnvelopeBackdropDefinitionParameters(array $json): CreateEnvelopeBackdropDefinitionParameters
    {
        return new CreateEnvelopeBackdropDefinitionParameters(
            $this->envelopeBackdropService->getEnvelopeBackdropById($json['owner_envelope_backdrop_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->attachmentService->getById($json['preview_attachment_id']),
            $this->attachmentService->getById($json['resource_attachment_id'])
        );
    }

    public function factoryEditEnvelopeBackdropDefinitionParameters(int $id, array $json): EditEnvelopeBackdropDefinitionParameters
    {
        return new EditEnvelopeBackdropDefinitionParameters(
            $this->envelopeBackdropService->getEnvelopeBackdropById($json['owner_envelope_backdrop_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->attachmentService->getById($json['preview_attachment_id']),
            $this->attachmentService->getById($json['resource_attachment_id'])
        );
    }
}