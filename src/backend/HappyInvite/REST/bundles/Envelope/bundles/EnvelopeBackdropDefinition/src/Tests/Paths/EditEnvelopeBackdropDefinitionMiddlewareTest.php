<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\Tests\Fixture\EnvelopeBackdropFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\EnvelopeBackdropDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;

final class EditEnvelopeBackdropDefinitionMiddlewareTest extends EnvelopeBackdropDefinitionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "* My Envelope Backdrop Definition"]],
            "description" => [["region" => "en_GB", "value" => "* My Envelope Backdrop Definition Description"]],
            'owner_envelope_backdrop_id' => EnvelopeBackdropFixture::$envelopeFixtureEF1->getId(),
            "preview_attachment_id" => AttachmentFixture::$attachmentLink->getId(),
            "resource_attachment_id" => AttachmentFixture::$attachmentLink->getId(),
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropDefinitionEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_backdrop_definition' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ],
                        'resource' => [
                            'id' => $json['resource_attachment_id']
                        ],
                        'owner_envelope_backdrop_id' => $json['owner_envelope_backdrop_id'],
                    ],
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeBackdropDefinitionEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeBackdropDefinitionEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeBackdropDefinitionFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopeBackdropDefinitionEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}