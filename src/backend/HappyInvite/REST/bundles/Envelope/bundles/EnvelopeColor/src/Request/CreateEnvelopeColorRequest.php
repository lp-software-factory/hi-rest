<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Parameters\CreateEnvelopeColorParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEnvelopeColorRequest extends SchemaRequest
{
    public function getParameters(): CreateEnvelopeColorParameters
    {
        $data = $this->getData();

        return new CreateEnvelopeColorParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description']),
            $data['hex_code']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EnvelopeColor_EnvelopeColorRequest');
    }
}