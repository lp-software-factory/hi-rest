<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\EnvelopeColorMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;

final class GetAllEnvelopeColorMiddlewareTest extends EnvelopeColorMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $ids = $this->requestEnvelopeColorGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_colors' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_colors']);
            });

        $this->assertEquals(array_map(function(EnvelopeColor $envelopeColor) {
            return $envelopeColor->getId();
        }, EnvelopeColorFixture::getOrderedList()), $ids);
    }
}