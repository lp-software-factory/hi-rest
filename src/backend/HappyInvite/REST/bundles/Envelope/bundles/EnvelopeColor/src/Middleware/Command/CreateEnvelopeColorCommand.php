<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Request\CreateEnvelopeColorRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeColorCommand extends AbstractEnvelopeColorCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeColor = $this->envelopeColorService->createEnvelopeColor(
            (new CreateEnvelopeColorRequest($request))->getParameters()
        );

        $responseBuilder
            ->setJSON([
                'envelope_color' => $this->envelopeColorFormatter->formatOne($envelopeColor),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}