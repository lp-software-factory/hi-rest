<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Exceptions\EnvelopeColorNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateEnvelopeColorCommand extends AbstractEnvelopeColorCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeColorId = $request->getAttribute('envelopeColorId');

            $envelopeColor = $this->envelopeColorService->deactivateEnvelopeColor($envelopeColorId);

            $responseBuilder
                ->setJSON([
                    'envelope_color' => $this->envelopeColorFormatter->formatOne($envelopeColor),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeColorNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}