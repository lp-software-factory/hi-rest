<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class EnvelopeColorMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new AttachmentFixture(),
        ];
    }

    protected function assertEnvelopeColorPosition(int $position, int $envelopeColorId)
    {
        $this->requestEnvelopeColorGetById($envelopeColorId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_color' => [
                    'entity' => [
                        'id' => $envelopeColorId,
                        'position' => $position,
                    ]
                ]
            ]);
    }
}