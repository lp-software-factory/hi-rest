<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\EnvelopeColorMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;

final class MoveUpEnvelopeColorMiddlewareTest extends EnvelopeColorMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $this->assertEnvelopeColorPosition(1, EnvelopeColorFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeColorPosition(2, EnvelopeColorFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeColorPosition(3, EnvelopeColorFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeColorMoveUp(EnvelopeColorFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeColorPosition(1, EnvelopeColorFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeColorPosition(2, EnvelopeColorFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeColorPosition(3, EnvelopeColorFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeColorMoveUp(EnvelopeColorFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeColorPosition(1, EnvelopeColorFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeColorPosition(2, EnvelopeColorFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeColorPosition(3, EnvelopeColorFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeColorMoveUp(EnvelopeColorFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeColorPosition(1, EnvelopeColorFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeColorPosition(2, EnvelopeColorFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeColorPosition(3, EnvelopeColorFixture::$envelopeFixtureEF1->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $envelopeFixture = EnvelopeColorFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeColorMoveUp($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeColorMoveUp($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $this->requestEnvelopeColorMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}