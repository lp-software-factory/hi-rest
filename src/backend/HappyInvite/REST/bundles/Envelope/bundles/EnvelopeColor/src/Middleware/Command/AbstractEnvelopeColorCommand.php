<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Formatter\EnvelopeColorFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service\EnvelopeColorService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeColorCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeColorService */
    protected $envelopeColorService;

    /** @var EnvelopeColorFormatter */
    protected $envelopeColorFormatter;

    public function __construct(
        AccessService $accessService,
        EnvelopeColorService $envelopeColorService,
        EnvelopeColorFormatter $envelopeColorFormatter
    ) {
        $this->accessService = $accessService;
        $this->envelopeColorService = $envelopeColorService;
        $this->envelopeColorFormatter = $envelopeColorFormatter;
    }
}