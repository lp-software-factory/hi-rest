<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Exceptions\EnvelopeColorNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEnvelopeColorCommand extends AbstractEnvelopeColorCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $envelopeColorId = $request->getAttribute('envelopeColorId');

            $envelopeColor = $this->envelopeColorService->getEnvelopeColorById($envelopeColorId);

            $responseBuilder
                ->setJSON([
                    'envelope_color' => $this->envelopeColorFormatter->formatOne($envelopeColor),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeColorNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}