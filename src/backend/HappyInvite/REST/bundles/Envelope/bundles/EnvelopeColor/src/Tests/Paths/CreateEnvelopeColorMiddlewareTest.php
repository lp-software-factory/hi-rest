<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\EnvelopeColorMiddlewareTestCase;

final class CreateEnvelopeColorMiddlewareTest extends EnvelopeColorMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "My Envelope Color"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Color Description"]],
            "hex_code" => "#ff0011"
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeColorCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_color' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'hex_code' => $json['hex_code']
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeColorCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeColorCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}