<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Exceptions\EnvelopeColorNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Request\EditEnvelopeColorRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditEnvelopeColorCommand extends AbstractEnvelopeColorCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeColorId = $request->getAttribute('envelopeColorId');

            $envelopeColor = $this->envelopeColorService->editEnvelopeColor(
                $envelopeColorId, (new EditEnvelopeColorRequest($request))->getParameters()
            );

            $responseBuilder
                ->setJSON([
                    'envelope_color' => $this->envelopeColorFormatter->formatOne($envelopeColor),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeColorNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}