<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeColorRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}