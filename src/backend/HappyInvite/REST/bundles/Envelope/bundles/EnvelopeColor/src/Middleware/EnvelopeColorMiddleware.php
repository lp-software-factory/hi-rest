<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\ActivateEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\CreateEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\DeactivateEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\DeleteEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\EditEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\GetAllEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\GetEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\MoveDownEnvelopeColorCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command\MoveUpEnvelopeColorCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeColorMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeColorCommand::class)
            ->attachDirect('edit', EditEnvelopeColorCommand::class)
            ->attachDirect('delete', DeleteEnvelopeColorCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeColorCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeColorCommand::class)
            ->attachDirect('activate', ActivateEnvelopeColorCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeColorCommand::class)
            ->attachDirect('get', GetEnvelopeColorCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeColorCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}