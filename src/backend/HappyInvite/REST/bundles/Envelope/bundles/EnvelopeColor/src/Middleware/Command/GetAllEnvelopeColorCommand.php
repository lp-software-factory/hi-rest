<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeColorCommand extends AbstractEnvelopeColorCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeColors = $this->envelopeColorService->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_colors' => $this->envelopeColorFormatter->formatMany($envelopeColors),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}