<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Middleware\EnvelopeColorMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/color/{command:create}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/color/{envelopeColorId}/{command:edit}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/color/{envelopeColorId}/{command:delete}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/color/{envelopeColorId}/{command:activate}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/color/{envelopeColorId}/{command:deactivate}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/color/{envelopeColorId}/{command:move-up}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/color/{envelopeColorId}/{command:move-down}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/color/{envelopeColorId}/{command:get}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/color/{command:get-all}[/]',
                'middleware' => EnvelopeColorMiddleware::class,
            ],
        ]
    ]
];