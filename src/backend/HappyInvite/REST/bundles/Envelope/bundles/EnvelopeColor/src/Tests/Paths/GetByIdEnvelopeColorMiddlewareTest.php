<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Paths;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\EnvelopeColorMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;

final class GetByIdEnvelopeColorMiddlewareTest extends EnvelopeColorMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $envelopeFixture = EnvelopeColorFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeColorGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_color' => [
                    'entity' => [
                        'id' => $this->expectId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $this->requestEnvelopeColorGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}