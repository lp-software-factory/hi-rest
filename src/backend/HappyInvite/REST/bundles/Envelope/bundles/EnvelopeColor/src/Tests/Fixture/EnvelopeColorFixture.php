<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Parameters\CreateEnvelopeColorParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service\EnvelopeColorService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeColorFixture implements Fixture
{
    /** @var EnvelopeColor */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeColor */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeColor */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeColorService::class); /** @var EnvelopeColorService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeColor(new CreateEnvelopeColorParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Color 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Color 1 Description"]]),
            '#ff00ff'
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopeColor(new CreateEnvelopeColorParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Color 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Color 2 Description"]]),
            '#00ff00'
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeColor(new CreateEnvelopeColorParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Color 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Color 3 Description"]]),
            '#ffffff'
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}