<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\EnvelopeColorMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;

final class DeleteEnvelopeColorMiddlewareTest extends EnvelopeColorMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeColorFixture());

        $envelopeFixture = EnvelopeColorFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeColorGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopeColorDelete($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopeColorGetById($envelopeFixtureId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeColorFixture());
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeColorFixture());
    }
}