<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeColorRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeColorCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/color/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeColorEdit(int $envelopeColorId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/color/%d/edit', $envelopeColorId))
            ->setParameters($json);
    }

    protected function requestEnvelopeColorDelete(int $envelopeColorId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/color/%d/delete', $envelopeColorId));
    }

    protected function requestEnvelopeColorMoveUp(int $envelopeColorId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/color/%d/move-up', $envelopeColorId));
    }

    protected function requestEnvelopeColorMoveDown(int $envelopeColorId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/color/%d/move-down', $envelopeColorId));
    }

    protected function requestEnvelopeColorActivate(int $envelopeColorId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/color/%d/activate', $envelopeColorId));
    }

    protected function requestEnvelopeColorDeactivate(int $envelopeColorId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/color/%d/deactivate', $envelopeColorId));
    }

    protected function requestEnvelopeColorGetById(int $envelopeColorId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/color/%d/get', $envelopeColorId));
    }

    protected function requestEnvelopeColorGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/color/get-all');
    }

}