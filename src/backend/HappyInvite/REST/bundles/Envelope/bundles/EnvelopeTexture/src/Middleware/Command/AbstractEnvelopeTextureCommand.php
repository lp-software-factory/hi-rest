<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Formatter\EnvelopeTextureFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\ParametersFactory\EnvelopeTextureParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeTextureCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeTextureService */
    protected $envelopeTextureService;

    /** @var EnvelopeTextureFormatter */
    protected $envelopeTextureFormatter;

    /** @var EnvelopeTextureParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        EnvelopeTextureService $envelopeTextureService,
        EnvelopeTextureFormatter $envelopeTextureFormatter,
        EnvelopeTextureParametersFactory $parametersFactory
    ) {
        $this->accessService = $accessService;
        $this->envelopeTextureService = $envelopeTextureService;
        $this->envelopeTextureFormatter = $envelopeTextureFormatter;
        $this->parametersFactory = $parametersFactory;
    }
}