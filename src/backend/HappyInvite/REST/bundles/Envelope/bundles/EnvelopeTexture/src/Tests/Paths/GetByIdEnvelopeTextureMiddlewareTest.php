<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\EnvelopeTextureMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;

final class GetByIdEnvelopeTextureMiddlewareTest extends EnvelopeTextureMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $envelopeFixture = EnvelopeTextureFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_texture' => [
                    'entity' => [
                        'id' => $this->expectId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $this->requestEnvelopeTextureGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}