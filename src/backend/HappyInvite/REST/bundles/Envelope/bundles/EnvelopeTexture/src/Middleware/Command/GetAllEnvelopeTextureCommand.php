<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeTextureCommand extends AbstractEnvelopeTextureCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeTextures = $this->envelopeTextureService->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_textures' => $this->envelopeTextureFormatter->formatMany($envelopeTextures),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}