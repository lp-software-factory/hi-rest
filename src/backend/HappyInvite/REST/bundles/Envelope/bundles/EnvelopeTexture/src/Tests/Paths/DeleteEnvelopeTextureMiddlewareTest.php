<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\EnvelopeTextureMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;

final class DeleteEnvelopeTextureMiddlewareTest extends EnvelopeTextureMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $envelopeFixture = EnvelopeTextureFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopeTextureDelete($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopeTextureGetById($envelopeFixtureId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTextureFixture());
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureFixture());
    }
}