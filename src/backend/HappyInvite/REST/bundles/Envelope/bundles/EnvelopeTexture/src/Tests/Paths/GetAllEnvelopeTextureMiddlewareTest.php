<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\EnvelopeTextureMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;

final class GetAllEnvelopeTextureMiddlewareTest extends EnvelopeTextureMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $ids = $this->requestEnvelopeTextureGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_textures' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_textures']);
            });

        $this->assertEquals(array_map(function(EnvelopeTexture $envelopeTexture) {
            return $envelopeTexture->getId();
        }, EnvelopeTextureFixture::getOrderedList()), $ids);
    }
}