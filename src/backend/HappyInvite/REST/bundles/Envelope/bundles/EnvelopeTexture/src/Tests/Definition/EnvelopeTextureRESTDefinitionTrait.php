<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeTextureRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeTextureCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/texture/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeTextureEdit(int $envelopeTextureId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/%d/edit', $envelopeTextureId))
            ->setParameters($json);
    }

    protected function requestEnvelopeTextureDelete(int $envelopeTextureId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/texture/%d/delete', $envelopeTextureId));
    }

    protected function requestEnvelopeTextureMoveUp(int $envelopeTextureId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/%d/move-up', $envelopeTextureId));
    }

    protected function requestEnvelopeTextureMoveDown(int $envelopeTextureId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/%d/move-down', $envelopeTextureId));
    }

    protected function requestEnvelopeTextureActivate(int $envelopeTextureId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/%d/activate', $envelopeTextureId));
    }

    protected function requestEnvelopeTextureDeactivate(int $envelopeTextureId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/%d/deactivate', $envelopeTextureId));
    }

    protected function requestEnvelopeTextureGetById(int $envelopeTextureId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/texture/%d/get', $envelopeTextureId));
    }

    protected function requestEnvelopeTextureGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/texture/get-all');
    }
}