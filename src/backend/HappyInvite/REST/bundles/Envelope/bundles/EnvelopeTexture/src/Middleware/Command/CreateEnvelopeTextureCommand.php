<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Request\CreateEnvelopeTextureRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeTextureCommand extends AbstractEnvelopeTextureCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeTexture = $this->envelopeTextureService->createEnvelopeTexture(
            $this->parametersFactory->factoryCreateEnvelopeTextureParameters($request)
        );

        $responseBuilder
            ->setJSON([
                'envelope_texture' => $this->envelopeTextureFormatter->formatOne($envelopeTexture),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}