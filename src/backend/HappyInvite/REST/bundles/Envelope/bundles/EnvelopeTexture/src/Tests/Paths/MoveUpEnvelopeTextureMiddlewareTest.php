<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\EnvelopeTextureMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;

final class MoveUpEnvelopeTextureMiddlewareTest extends EnvelopeTextureMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $this->assertEnvelopeTexturePosition(1, EnvelopeTextureFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTexturePosition(2, EnvelopeTextureFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeTexturePosition(3, EnvelopeTextureFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeTextureMoveUp(EnvelopeTextureFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeTexturePosition(1, EnvelopeTextureFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTexturePosition(2, EnvelopeTextureFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeTexturePosition(3, EnvelopeTextureFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeTextureMoveUp(EnvelopeTextureFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeTexturePosition(1, EnvelopeTextureFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeTexturePosition(2, EnvelopeTextureFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTexturePosition(3, EnvelopeTextureFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeTextureMoveUp(EnvelopeTextureFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeTexturePosition(1, EnvelopeTextureFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeTexturePosition(2, EnvelopeTextureFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeTexturePosition(3, EnvelopeTextureFixture::$envelopeFixtureEF1->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $envelopeFixture = EnvelopeTextureFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureMoveUp($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureMoveUp($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $this->requestEnvelopeTextureMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}