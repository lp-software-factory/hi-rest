<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeTextureRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}