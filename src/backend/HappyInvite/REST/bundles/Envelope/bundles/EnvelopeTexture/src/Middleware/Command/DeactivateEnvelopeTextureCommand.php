<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Exceptions\EnvelopeTextureNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateEnvelopeTextureCommand extends AbstractEnvelopeTextureCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeTextureId = $request->getAttribute('envelopeTextureId');

            $envelopeTexture = $this->envelopeTextureService->deactivateEnvelopeTexture($envelopeTextureId);

            $responseBuilder
                ->setJSON([
                    'envelope_texture' => $this->envelopeTextureFormatter->formatOne($envelopeTexture),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeTextureNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}