<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\EnvelopeTextureMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;

final class EditEnvelopeTextureMiddlewareTest extends EnvelopeTextureMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "preview_attachment_id" => AttachmentFixture::$attachmentVideo->getId(),
            "title" => [["region" => "en_GB", "value" => "* My Envelope Texture"]],
            "description" => [["region" => "en_GB", "value" => "* My Envelope Texture Description"]],
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeTextureFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_texture' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ]
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeTextureFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopeTextureEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}