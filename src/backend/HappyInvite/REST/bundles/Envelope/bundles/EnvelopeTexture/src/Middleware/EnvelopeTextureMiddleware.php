<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\ActivateEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\CreateEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\DeactivateEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\DeleteEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\EditEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\GetAllEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\GetEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\MoveDownEnvelopeTextureCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\Command\MoveUpEnvelopeTextureCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeTextureMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeTextureCommand::class)
            ->attachDirect('edit', EditEnvelopeTextureCommand::class)
            ->attachDirect('delete', DeleteEnvelopeTextureCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeTextureCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeTextureCommand::class)
            ->attachDirect('activate', ActivateEnvelopeTextureCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeTextureCommand::class)
            ->attachDirect('get', GetEnvelopeTextureCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeTextureCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}