<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class EnvelopeTextureMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new AttachmentFixture(),
        ];
    }

    protected function assertEnvelopeTexturePosition(int $position, int $envelopeTextureId)
    {
        $this->requestEnvelopeTextureGetById($envelopeTextureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_texture' => [
                    'entity' => [
                        'id' => $envelopeTextureId,
                        'position' => $position,
                    ]
                ]
            ]);
    }
}