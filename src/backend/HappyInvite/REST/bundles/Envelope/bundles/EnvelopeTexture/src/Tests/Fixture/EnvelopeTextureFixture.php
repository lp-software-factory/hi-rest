<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\CreateEnvelopeTextureParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeTextureFixture implements Fixture
{
    /** @var EnvelopeTexture */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeTexture */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeTexture */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeTextureService::class); /** @var EnvelopeTextureService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeTexture(new CreateEnvelopeTextureParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture 1 Description"]])
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopeTexture(new CreateEnvelopeTextureParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture 2 Description"]])
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeTexture(new CreateEnvelopeTextureParameters(
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture 3 Description"]])
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}