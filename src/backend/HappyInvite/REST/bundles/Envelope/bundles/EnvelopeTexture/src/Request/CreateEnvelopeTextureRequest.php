<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEnvelopeTextureRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EnvelopeTexture_EnvelopeTextureRequest');
    }
}