<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\EnvelopeTextureMiddlewareTestCase;

final class CreateEnvelopeTextureMiddlewareTest extends EnvelopeTextureMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "preview_attachment_id" => AttachmentFixture::$attachmentImage->getId(),
            "title" => [["region" => "en_GB", "value" => "My Envelope Texture"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Texture Description"]],
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeTextureCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_texture' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeTextureCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}