<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Middleware\EnvelopeTextureMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/texture/{command:create}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:edit}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:delete}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:activate}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:deactivate}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:move-up}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:move-down}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/texture/{envelopeTextureId}/{command:get}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/texture/{command:get-all}[/]',
                'middleware' => EnvelopeTextureMiddleware::class,
            ],
        ]
    ]
];