<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinitionResource;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\CreateEnvelopeTextureParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\EditEnvelopeTextureParameters;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Request\CreateEnvelopeTextureRequest;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Request\EditEnvelopeTextureRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EnvelopeTextureParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function factoryCreateEnvelopeTextureParameters(ServerRequestInterface $request): CreateEnvelopeTextureParameters
    {
        $json = (new CreateEnvelopeTextureRequest($request))->getParameters();

        return new CreateEnvelopeTextureParameters(
            $this->attachmentService->getById($json['preview_attachment_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }

    public function factoryEditEnvelopeTextureParameters(ServerRequestInterface $request): EditEnvelopeTextureParameters
    {
        $json = (new EditEnvelopeTextureRequest($request))->getParameters();

        return new EditEnvelopeTextureParameters(
            $this->attachmentService->getById($json['preview_attachment_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }
}