<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\EnvelopeMarkDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture\EnvelopeMarksFixture;

final class CreateEnvelopeMarkDefinitionMiddlewareTest extends EnvelopeMarkDefinitionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "owner_envelope_marks_id" => EnvelopeMarksFixture::$envelopeFixtureEF1->getId(),
            "title" => [["region" => "en_GB", "value" => "My Envelope Marks"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Marks Description"]],
            "attachment_id" => AttachmentFixture::$attachmentImage->getId(),
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeMarkDefinitionCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_mark_definition' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'owner_envelope_marks_id' => $json['owner_envelope_marks_id'],
                        'attachment' => [
                            'id' => $json['attachment_id']
                        ]
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeMarkDefinitionCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarkDefinitionCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}