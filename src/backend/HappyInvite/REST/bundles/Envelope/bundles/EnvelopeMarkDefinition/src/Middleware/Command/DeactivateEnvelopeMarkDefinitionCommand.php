<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions\EnvelopeMarkDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateEnvelopeMarkDefinitionCommand extends AbstractEnvelopeMarkDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeMarkDefinitionId = $request->getAttribute('envelopeMarkDefinitionId');

            $envelopeMarkDefinition = $this->service->deactivateEnvelopeMarkDefinition($envelopeMarkDefinitionId);

            $responseBuilder
                ->setJSON([
                    'envelope_mark_definition' => $this->formatter->formatOne($envelopeMarkDefinition),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeMarkDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}