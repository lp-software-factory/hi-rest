<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\EnvelopeMarkDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;

final class DeleteEnvelopeMarkDefinitionMiddlewareTest extends EnvelopeMarkDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $envelopeFixture = EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarkDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopeMarkDefinitionDelete($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopeMarkDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());
    }
}