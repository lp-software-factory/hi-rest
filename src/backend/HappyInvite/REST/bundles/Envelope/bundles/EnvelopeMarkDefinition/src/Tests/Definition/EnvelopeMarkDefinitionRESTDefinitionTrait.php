<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeMarkDefinitionRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeMarkDefinitionCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/marks/definition/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeMarkDefinitionEdit(int $envelopeMarkDefinitionId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/definition/%d/edit', $envelopeMarkDefinitionId))
            ->setParameters($json);
    }

    protected function requestEnvelopeMarkDefinitionDelete(int $envelopeMarkDefinitionId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/marks/definition/%d/delete', $envelopeMarkDefinitionId));
    }

    protected function requestEnvelopeMarkDefinitionMoveUp(int $envelopeMarkDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/definition/%d/move-up', $envelopeMarkDefinitionId));
    }

    protected function requestEnvelopeMarkDefinitionMoveDown(int $envelopeMarkDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/definition/%d/move-down', $envelopeMarkDefinitionId));
    }

    protected function requestEnvelopeMarkDefinitionActivate(int $envelopeMarkDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/definition/%d/activate', $envelopeMarkDefinitionId));
    }

    protected function requestEnvelopeMarkDefinitionDeactivate(int $envelopeMarkDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/definition/%d/deactivate', $envelopeMarkDefinitionId));
    }

    protected function requestEnvelopeMarkDefinitionGetById(int $envelopeMarkDefinitionId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/marks/definition/%d/get', $envelopeMarkDefinitionId));
    }

    protected function requestEnvelopeMarkDefinitionGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/marks/definition/get-all');
    }
}