<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeMarkDefinitionCommand extends AbstractEnvelopeMarkDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeMarkDefinitions = $this->service->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_mark_definition' => $this->formatter->formatMany($envelopeMarkDefinitions),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}