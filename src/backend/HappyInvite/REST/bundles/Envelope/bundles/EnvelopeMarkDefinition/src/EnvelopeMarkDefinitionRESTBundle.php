<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeMarkDefinitionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}