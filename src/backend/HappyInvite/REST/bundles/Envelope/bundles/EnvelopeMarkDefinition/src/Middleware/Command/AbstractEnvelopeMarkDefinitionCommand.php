<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarkDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Formatter\EnvelopeMarkDefinitionFormatter;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\ParametersFactory\EnvelopeMarkDefinitionParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeMarkDefinitionCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeMarkDefinitionService */
    protected $service;

    /** @var EnvelopeMarkDefinitionFormatter */
    protected $formatter;

    /** @var EnvelopeMarkDefinitionParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        EnvelopeMarkDefinitionService $envelopeMarkDefinitionService,
        EnvelopeMarkDefinitionFormatter $envelopeMarkDefinitionFormatter,
        EnvelopeMarkDefinitionParametersFactory $parametersFactory
    ) {
        $this->accessService = $accessService;
        $this->service = $envelopeMarkDefinitionService;
        $this->formatter = $envelopeMarkDefinitionFormatter;
        $this->parametersFactory = $parametersFactory;
    }
}