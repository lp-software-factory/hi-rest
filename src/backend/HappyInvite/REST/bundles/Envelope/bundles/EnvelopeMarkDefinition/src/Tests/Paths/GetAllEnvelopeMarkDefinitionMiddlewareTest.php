<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\EnvelopeMarkDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;

final class GetAllEnvelopeMarkDefinitionMiddlewareTest extends EnvelopeMarkDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $ids = $this->requestEnvelopeMarkDefinitionGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_mark_definition' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_mark_definition']);
            });

        $this->assertEquals(array_map(function(EnvelopeMarkDefinition $envelopeMarkDefinition) {
            return $envelopeMarkDefinition->getId();
        }, EnvelopeMarkDefinitionFixture::getOrderedList()), $ids);
    }
}