<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEnvelopeMarkDefinitionRequest extends SchemaRequest
{
    public function getParameters(): array 
    {
        $data = $this->getData();

        return $data;
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EnvelopeMarkDefinition_EnvelopeMarkDefinitionRequest');
    }
}