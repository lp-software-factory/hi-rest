<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarkDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition\CreateEnvelopeMarkDefinitionParameters;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture\EnvelopeMarksFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeMarkDefinitionFixture implements Fixture
{
    /** @var EnvelopeMarkDefinition */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeMarkDefinition */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeMarkDefinition */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeMarkDefinitionService::class); /** @var \HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarkDefinitionService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeMarkDefinition(new CreateEnvelopeMarkDefinitionParameters(
            EnvelopeMarksFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 1 Description"]]),
            AttachmentFixture::$attachmentImage
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopeMarkDefinition(new CreateEnvelopeMarkDefinitionParameters(
            EnvelopeMarksFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 2 Description"]]),
            AttachmentFixture::$attachmentImage2
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeMarkDefinition(new CreateEnvelopeMarkDefinitionParameters(
            EnvelopeMarksFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 3 Description"]]),
            AttachmentFixture::$attachmentImage3
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}