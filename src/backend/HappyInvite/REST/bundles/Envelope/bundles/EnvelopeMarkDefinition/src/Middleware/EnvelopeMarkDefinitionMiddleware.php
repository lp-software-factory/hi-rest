<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\ActivateEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\CreateEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\DeactivateEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\DeleteEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\EditEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\GetAllEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\GetEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\MoveDownEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command\MoveUpEnvelopeMarkDefinitionCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeMarkDefinitionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('edit', EditEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('delete', DeleteEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('activate', ActivateEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('get', GetEnvelopeMarkDefinitionCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeMarkDefinitionCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}