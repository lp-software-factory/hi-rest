<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\EnvelopeMarkDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;

final class MoveUpEnvelopeMarkDefinitionMiddlewareTest extends EnvelopeMarkDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $this->assertEnvelopeMarkDefinitionPosition(1, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarkDefinitionPosition(2, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeMarkDefinitionPosition(3, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeMarkDefinitionMoveUp(EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeMarkDefinitionPosition(1, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarkDefinitionPosition(2, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeMarkDefinitionPosition(3, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeMarkDefinitionMoveUp(EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopeMarkDefinitionPosition(1, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeMarkDefinitionPosition(2, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarkDefinitionPosition(3, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeMarkDefinitionMoveUp(EnvelopeMarkDefinitionFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeMarkDefinitionPosition(1, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeMarkDefinitionPosition(2, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeMarkDefinitionPosition(3, EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $envelopeFixture = EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarkDefinitionMoveUp($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarkDefinitionMoveUp($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $this->requestEnvelopeMarkDefinitionMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}