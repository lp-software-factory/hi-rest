<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Request\CreateEnvelopeMarkDefinitionRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeMarkDefinitionCommand extends AbstractEnvelopeMarkDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeMarkDefinition = $this->service->createEnvelopeMarkDefinition(
            $this->parametersFactory->factoryCreateParameters((new CreateEnvelopeMarkDefinitionRequest($request))->getParameters())
        );

        $responseBuilder
            ->setJSON([
                'envelope_mark_definition' => $this->formatter->formatOne($envelopeMarkDefinition),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}