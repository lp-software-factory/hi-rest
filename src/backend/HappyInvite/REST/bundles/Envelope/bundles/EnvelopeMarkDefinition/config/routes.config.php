<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Middleware\EnvelopeMarkDefinitionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/marks/definition/{command:create}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:edit}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:delete}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:move-up}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:activate}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:deactivate}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:move-down}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/marks/definition/{envelopeMarkDefinitionId}/{command:get}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/marks/definition/{command:get-all}[/]',
                'middleware' => EnvelopeMarkDefinitionMiddleware::class,
            ],
        ]
    ]
];