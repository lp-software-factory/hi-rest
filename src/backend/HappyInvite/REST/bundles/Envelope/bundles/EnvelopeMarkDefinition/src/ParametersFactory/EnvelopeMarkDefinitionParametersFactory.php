<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition\CreateEnvelopeMarkDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition\EditEnvelopeMarkDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarksService;

final class EnvelopeMarkDefinitionParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeMarksService */
    private $envelopeMarksService;

    public function __construct(AttachmentService $attachmentService, EnvelopeMarksService $envelopeMarksService)
    {
        $this->attachmentService = $attachmentService;
        $this->envelopeMarksService = $envelopeMarksService;
    }

    public function factoryCreateParameters(array $json): CreateEnvelopeMarkDefinitionParameters
    {
        return new CreateEnvelopeMarkDefinitionParameters(
            $this->envelopeMarksService->getEnvelopeMarksById($json['owner_envelope_marks_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->attachmentService->getById($json['attachment_id'])
        );
    }

    public function factoryEditParameters(array $json): EditEnvelopeMarkDefinitionParameters
    {
        return new EditEnvelopeMarkDefinitionParameters(
            $this->envelopeMarksService->getEnvelopeMarksById($json['owner_envelope_marks_id']),
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->attachmentService->getById($json['attachment_id'])
        );
    }
}