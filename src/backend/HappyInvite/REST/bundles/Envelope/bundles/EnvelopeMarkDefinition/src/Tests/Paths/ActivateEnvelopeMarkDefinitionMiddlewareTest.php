<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\EnvelopeMarkDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;

final class ActivateEnvelopeMarkDefinitionMiddlewareTest extends EnvelopeMarkDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $envelopeFixture = EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarkDefinitionActivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_mark_definition' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopeMarkDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_mark_definition' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopeMarkDefinitionDeactivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_mark_definition' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);

        $this->requestEnvelopeMarkDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_mark_definition' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $envelopeFixture = EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarkDefinitionActivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarkDefinitionActivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarkDefinitionDeactivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarkDefinitionDeactivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeMarkDefinitionFixture());

        $this->requestEnvelopeMarkDefinitionActivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEnvelopeMarkDefinitionDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}