<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\ActivateEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\CreateEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\DeactivateEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\DeleteEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\EditEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\GetAllEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\GetEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\MoveDownEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command\MoveUpEnvelopeTextureDefinitionCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeTextureDefinitionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('edit', EditEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('delete', DeleteEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('activate', ActivateEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('get', GetEnvelopeTextureDefinitionCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeTextureDefinitionCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}