<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeTextureDefinitionCommand extends AbstractEnvelopeTextureDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeTextureDefinitions = $this->envelopeTextureDefinitionService->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_texture_definitions' => $this->envelopeTextureDefinitionFormatter->formatMany($envelopeTextureDefinitions),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}