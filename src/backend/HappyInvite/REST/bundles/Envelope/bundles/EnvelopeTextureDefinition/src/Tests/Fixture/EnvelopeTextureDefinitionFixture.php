<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinitionResource;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition\CreateEnvelopeTextureDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureDefinitionService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeTextureDefinitionFixture implements Fixture
{
    /** @var EnvelopeTextureDefinition */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeTextureDefinition */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeTextureDefinition */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeTextureDefinitionService::class); /** @var EnvelopeTextureDefinitionService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeTextureDefinition(new CreateEnvelopeTextureDefinitionParameters(
            EnvelopeTextureFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture Definition 1"]]),
            AttachmentFixture::$attachmentImage,
            [
                new EnvelopeTextureDefinitionResource(500, 400, ['front' => AttachmentFixture::$attachmentImage->getId()], ['x' => 100, 'y' => 200]),
                new EnvelopeTextureDefinitionResource(900, 500, ['front' => AttachmentFixture::$attachmentImage->getId()], ['x' => 100, 'y' => 200]),
            ]
        ));

        self::$envelopeFixtureEF2 =$service->createEnvelopeTextureDefinition(new CreateEnvelopeTextureDefinitionParameters(
            EnvelopeTextureFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture Definition 2"]]),
            AttachmentFixture::$attachmentImage,
            [
                new EnvelopeTextureDefinitionResource(500, 400, ['front' => AttachmentFixture::$attachmentImage->getId()], ['x' => 100, 'y' => 200]),
                new EnvelopeTextureDefinitionResource(900, 500, ['front' => AttachmentFixture::$attachmentImage->getId()], ['x' => 100, 'y' => 200]),
            ]
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeTextureDefinition(new CreateEnvelopeTextureDefinitionParameters(
            EnvelopeTextureFixture::$envelopeFixtureEF1,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Texture Definition 3"]]),
            AttachmentFixture::$attachmentImage,
            [
                new EnvelopeTextureDefinitionResource(500, 400, ['front' => AttachmentFixture::$attachmentImage->getId()], ['x' => 100, 'y' => 200]),
                new EnvelopeTextureDefinitionResource(900, 500, ['front' => AttachmentFixture::$attachmentImage->getId()], ['x' => 100, 'y' => 200]),
            ]
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}