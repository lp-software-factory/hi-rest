<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Exceptions\EnvelopeTextureDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEnvelopeTextureDefinitionCommand extends AbstractEnvelopeTextureDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeTextureDefinitionId = $request->getAttribute('envelopeTextureDefinitionId');

            $this->envelopeTextureDefinitionService->deleteEnvelopeTextureDefinition($envelopeTextureDefinitionId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(EnvelopeTextureDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}