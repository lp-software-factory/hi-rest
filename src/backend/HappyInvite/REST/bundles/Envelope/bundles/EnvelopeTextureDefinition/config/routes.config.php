<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\EnvelopeTextureDefinitionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/texture/definition/{command:create}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:edit}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:delete}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:activate}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:deactivate}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:move-up}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:move-down}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/texture/definition/{envelopeTextureDefinitionId}/{command:get}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/texture/definition/{command:get-all}[/]',
                'middleware' => EnvelopeTextureDefinitionMiddleware::class,
            ],
        ]
    ]
];