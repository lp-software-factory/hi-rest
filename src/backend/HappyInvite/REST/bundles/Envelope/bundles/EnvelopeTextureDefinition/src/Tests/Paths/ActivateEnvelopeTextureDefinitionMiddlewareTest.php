<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\EnvelopeTextureDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;

final class ActivateEnvelopeTextureDefinitionMiddlewareTest extends EnvelopeTextureDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $envelopeFixture = EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureDefinitionActivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_texture_definition' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopeTextureDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_texture_definition' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopeTextureDefinitionDeactivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_texture_definition' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);

        $this->requestEnvelopeTextureDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_texture_definition' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $envelopeFixture = EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureDefinitionActivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureDefinitionActivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureDefinitionDeactivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureDefinitionDeactivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $this->requestEnvelopeTextureDefinitionActivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEnvelopeTextureDefinitionDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}