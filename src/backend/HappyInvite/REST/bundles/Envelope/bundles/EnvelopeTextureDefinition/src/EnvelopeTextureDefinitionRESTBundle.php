<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeTextureDefinitionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}