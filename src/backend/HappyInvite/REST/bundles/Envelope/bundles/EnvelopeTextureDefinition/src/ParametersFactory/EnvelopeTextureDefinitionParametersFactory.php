<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinitionResource;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition\CreateEnvelopeTextureDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition\EditEnvelopeTextureDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Request\CreateEnvelopeTextureDefinitionRequest;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Request\EditEnvelopeTextureDefinitionRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EnvelopeTextureDefinitionParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeTextureService */
    private $envelopeTextureService;

    public function __construct(
        AttachmentService $attachmentService,
        EnvelopeTextureService $envelopeTextureService
    ) {
        $this->attachmentService = $attachmentService;
        $this->envelopeTextureService = $envelopeTextureService;
    }

    public function factoryCreateEnvelopeTextureDefinitionParameters(ServerRequestInterface $request): CreateEnvelopeTextureDefinitionParameters
    {
        $json = (new CreateEnvelopeTextureDefinitionRequest($request))->getParameters();

        return new CreateEnvelopeTextureDefinitionParameters(
            $this->envelopeTextureService->getEnvelopeTextureById($json['owner_envelope_texture_id']),
            new ImmutableLocalizedString($json['title']),
            $this->attachmentService->getById($json['preview_attachment_id']),
            array_map(function(array $input) {
                return EnvelopeTextureDefinitionResource::fromJSON($input);
            }, $json['resources'])
        );
    }

    public function factoryEditEnvelopeTextureDefinitionParameters(ServerRequestInterface $request): EditEnvelopeTextureDefinitionParameters
    {
        $json = (new EditEnvelopeTextureDefinitionRequest($request))->getParameters();

        return new EditEnvelopeTextureDefinitionParameters(
            $this->envelopeTextureService->getEnvelopeTextureById($json['owner_envelope_texture_id']),
            new ImmutableLocalizedString($json['title']),
            $this->attachmentService->getById($json['preview_attachment_id']),
            array_map(function(array $input) {
                return EnvelopeTextureDefinitionResource::fromJSON($input);
            }, $json['resources'])
        );
    }
}