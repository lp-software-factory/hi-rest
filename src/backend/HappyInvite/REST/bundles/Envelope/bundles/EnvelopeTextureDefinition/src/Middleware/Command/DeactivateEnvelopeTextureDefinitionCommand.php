<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Exceptions\EnvelopeTextureDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateEnvelopeTextureDefinitionCommand extends AbstractEnvelopeTextureDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeTextureDefinitionId = $request->getAttribute('envelopeTextureDefinitionId');

            $envelopeTextureDefinition = $this->envelopeTextureDefinitionService->deactivateEnvelopeTextureDefinition($envelopeTextureDefinitionId);

            $responseBuilder
                ->setJSON([
                    'envelope_texture_definition' => $this->envelopeTextureDefinitionFormatter->formatOne($envelopeTextureDefinition),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeTextureDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}