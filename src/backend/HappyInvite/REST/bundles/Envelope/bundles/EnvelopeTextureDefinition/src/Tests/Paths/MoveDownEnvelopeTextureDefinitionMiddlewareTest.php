<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\EnvelopeTextureDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;

final class MoveDownEnvelopeTextureDefinitionMiddlewareTest extends EnvelopeTextureDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $this->assertEnvelopeTextureDefinitionPosition(1, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTextureDefinitionPosition(2, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeTextureDefinitionPosition(3, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeTextureDefinitionMoveDown(EnvelopeTextureDefinitionFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopeTextureDefinitionPosition(1, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTextureDefinitionPosition(2, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeTextureDefinitionPosition(3, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeTextureDefinitionMoveDown(EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopeTextureDefinitionPosition(1, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTextureDefinitionPosition(2, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeTextureDefinitionPosition(3, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2->getId());

        $this->requestEnvelopeTextureDefinitionMoveDown(EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeTextureDefinitionPosition(1, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeTextureDefinitionPosition(2, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeTextureDefinitionPosition(3, EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $envelopeFixture = EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureDefinitionMoveDown($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureDefinitionMoveDown($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $this->requestEnvelopeTextureDefinitionMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}