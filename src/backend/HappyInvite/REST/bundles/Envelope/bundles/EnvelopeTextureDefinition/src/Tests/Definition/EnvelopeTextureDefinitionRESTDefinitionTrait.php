<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeTextureDefinitionRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeTextureDefinitionCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/texture/definition/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeTextureDefinitionEdit(int $envelopeTextureDefinitionId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/definition/%d/edit', $envelopeTextureDefinitionId))
            ->setParameters($json);
    }

    protected function requestEnvelopeTextureDefinitionDelete(int $envelopeTextureDefinitionId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/texture/definition/%d/delete', $envelopeTextureDefinitionId));
    }

    protected function requestEnvelopeTextureDefinitionMoveUp(int $envelopeTextureDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/definition/%d/move-up', $envelopeTextureDefinitionId));
    }

    protected function requestEnvelopeTextureDefinitionMoveDown(int $envelopeTextureDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/definition/%d/move-down', $envelopeTextureDefinitionId));
    }

    protected function requestEnvelopeTextureDefinitionActivate(int $envelopeTextureDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/definition/%d/activate', $envelopeTextureDefinitionId));
    }

    protected function requestEnvelopeTextureDefinitionDeactivate(int $envelopeTextureDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/texture/definition/%d/deactivate', $envelopeTextureDefinitionId));
    }

    protected function requestEnvelopeTextureDefinitionGetById(int $envelopeTextureDefinitionId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/texture/definition/%d/get', $envelopeTextureDefinitionId));
    }

    protected function requestEnvelopeTextureDefinitionGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/texture/definition/get-all');
    }
}