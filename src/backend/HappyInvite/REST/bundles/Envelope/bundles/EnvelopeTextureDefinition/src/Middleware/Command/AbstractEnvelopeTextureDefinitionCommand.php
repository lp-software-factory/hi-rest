<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Formatter\EnvelopeTextureDefinitionFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureDefinitionService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\ParametersFactory\EnvelopeTextureDefinitionParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeTextureDefinitionCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeTextureDefinitionService */
    protected $envelopeTextureDefinitionService;

    /** @var EnvelopeTextureDefinitionFormatter */
    protected $envelopeTextureDefinitionFormatter;

    /** @var EnvelopeTextureDefinitionParametersFactory */
    protected $parametersFactory;

    public function __construct(
        AccessService $accessService,
        EnvelopeTextureDefinitionService $envelopeTextureDefinitionService,
        EnvelopeTextureDefinitionFormatter $envelopeTextureDefinitionFormatter,
        EnvelopeTextureDefinitionParametersFactory $parametersFactory
    ) {
        $this->accessService = $accessService;
        $this->envelopeTextureDefinitionService = $envelopeTextureDefinitionService;
        $this->envelopeTextureDefinitionFormatter = $envelopeTextureDefinitionFormatter;
        $this->parametersFactory = $parametersFactory;
    }
}