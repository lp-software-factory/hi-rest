<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Request\CreateEnvelopeTextureDefinitionRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeTextureDefinitionCommand extends AbstractEnvelopeTextureDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeTextureDefinition = $this->envelopeTextureDefinitionService->createEnvelopeTextureDefinition(
            $this->parametersFactory->factoryCreateEnvelopeTextureDefinitionParameters($request)
        );

        $responseBuilder
            ->setJSON([
                'envelope_texture_definition' => $this->envelopeTextureDefinitionFormatter->formatOne($envelopeTextureDefinition),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}