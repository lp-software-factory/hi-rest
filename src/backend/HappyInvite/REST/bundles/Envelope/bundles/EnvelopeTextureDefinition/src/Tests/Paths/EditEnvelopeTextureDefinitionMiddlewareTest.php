<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\EnvelopeTextureDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;

final class EditEnvelopeTextureDefinitionMiddlewareTest extends EnvelopeTextureDefinitionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "owner_envelope_texture_id" => EnvelopeTextureFixture::$envelopeFixtureEF1->getId(),
            "preview_attachment_id" => AttachmentFixture::$attachmentImage2->getId(),
            "title" => [["region" => "en_GB", "value" => "* My Envelope Texture"]],
            'resources' => [
                [
                    'width' => 250,
                    'height' => 400,
                    'attachment_ids' => [AttachmentFixture::$attachmentImage->getId()],
                    'stamp' => [
                        'x' => 150,
                        'y' => 800,
                    ]
                ]
            ],
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureDefinitionEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_texture_definition' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'position' => 1,
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ],
                        'owner_envelope_texture_id' => $json['owner_envelope_texture_id'],
                        'resources' => $json['resources'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureDefinitionEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureDefinitionEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopeTextureDefinitionEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}