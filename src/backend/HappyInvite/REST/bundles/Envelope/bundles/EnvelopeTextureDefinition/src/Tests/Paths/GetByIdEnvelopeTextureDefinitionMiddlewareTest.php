<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\EnvelopeTextureDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;

final class GetByIdEnvelopeTextureDefinitionMiddlewareTest extends EnvelopeTextureDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $envelopeFixture = EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeTextureDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_texture_definition' => [
                    'entity' => [
                        'id' => $this->expectId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTextureDefinitionFixture());

        $this->requestEnvelopeTextureDefinitionGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}