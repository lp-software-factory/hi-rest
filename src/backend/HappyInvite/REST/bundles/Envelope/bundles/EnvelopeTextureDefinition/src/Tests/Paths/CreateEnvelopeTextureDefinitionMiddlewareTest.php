<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\Tests\Fixture\EnvelopeTextureFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\EnvelopeTextureDefinitionMiddlewareTestCase;

final class CreateEnvelopeTextureDefinitionMiddlewareTest extends EnvelopeTextureDefinitionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "owner_envelope_texture_id" => EnvelopeTextureFixture::$envelopeFixtureEF1->getId(),
            "preview_attachment_id" => AttachmentFixture::$attachmentImage->getId(),
            "title" => [["region" => "en_GB", "value" => "My Envelope Texture"]],
            'resources' => [
                [
                    'width' => 100,
                    'height' => 250,
                    'attachment_ids' => [AttachmentFixture::$attachmentImage->getId()],
                    'stamp' => [
                        'x' => 100,
                        'y' => 200,
                    ]
                ]
            ],
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeTextureDefinitionCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_texture_definition' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'position' => 1,
                        'preview' => [
                            'id' => $json['preview_attachment_id']
                        ],
                        'owner_envelope_texture_id' => $json['owner_envelope_texture_id'],
                        'resources' => $json['resources'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopeTextureDefinitionCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTextureDefinitionCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}