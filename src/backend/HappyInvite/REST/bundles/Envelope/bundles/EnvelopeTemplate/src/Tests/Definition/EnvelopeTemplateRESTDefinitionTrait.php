<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeTemplateRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeTemplateCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/template/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeTemplateEdit(int $envelopeTemplateId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/template/%d/edit', $envelopeTemplateId))
            ->setParameters($json);
    }

    protected function requestEnvelopeTemplateDelete(int $envelopeTemplateId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/template/%d/delete', $envelopeTemplateId));
    }

    protected function requestEnvelopeTemplateGetById(int $envelopeTemplateId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/template/%d/get', $envelopeTemplateId));
    }
}