<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\EnvelopeTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;

final class EditEnvelopeTemplateMiddlewareTest extends EnvelopeTemplateMiddlewareTestCase
{
    private function getCreateTestJSON(): array
    {
        return [
            'title' => [['region' => 'en_US', 'value' => 'Envelope Template 1']],
            'description' => [['region' => 'en_US', 'value' => 'Envelope Template 1 Description']],
            'envelope_backdrop_definition_id' => EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId(),
            'envelope_pattern_definition_id' => EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId(),
            'envelope_texture_definition_id' => EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId(),
            'envelope_mark_definition_id' => EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId(),
            'config' => ['bar' => 'foo']
        ];
    }

    private function getTestJSON(): array
    {
        return [
            'title' => [['region' => 'en_US', 'value' => 'Envelope Template 1 *']],
            'description' => [['region' => 'en_US', 'value' => 'Envelope Template 1 * Description']],
            'envelope_backdrop_definition_id' => EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2->getId(),
            'envelope_pattern_definition_id' => EnvelopePatternDefinitionFixture::$envelopeFixtureEF2->getId(),
            'envelope_texture_definition_id' => EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2->getId(),
            'envelope_mark_definition_id' => EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2->getId(),
            'config' => ['bar' => 'foo']
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $id = $this->requestEnvelopeTemplateCreate($json = $this->getCreateTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->fetch(function(array $json) {
                return $json['envelope_template']['entity']['id'];
            });

        $this->requestEnvelopeTemplateEdit($id, $json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_template' => [
                    'entity' => [
                        'id' => $id,
                        'envelope_backdrop_definition' => [
                            'entity' => [
                                'id' => $json['envelope_backdrop_definition_id']
                            ]
                        ],
                        'envelope_texture_definition' => [
                            'entity' => [
                                'id' => $json['envelope_texture_definition_id']
                            ]
                        ],
                        'envelope_pattern_definition' => [
                            'entity' => [
                                'id' => $json['envelope_pattern_definition_id']
                            ]
                        ],
                        'envelope_mark_definition' => [
                            'entity' => [
                                'id' => $json['envelope_mark_definition_id']
                            ]
                        ],
                        'config' => $json['config'],
                        'solution' => [
                            'is_user_specified' => false,
                            'owner' => [
                                'has' => false,
                            ]
                        ]
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $fixture = EnvelopeTemplateFixture::$envelopeTemplateEF1;

        $this->requestEnvelopeTemplateEdit($fixture->getId(), $this->getTestJSON())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTemplateEdit($fixture->getId(), $this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $this->requestEnvelopeTemplateEdit(self::NOT_FOUND_ID, $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}