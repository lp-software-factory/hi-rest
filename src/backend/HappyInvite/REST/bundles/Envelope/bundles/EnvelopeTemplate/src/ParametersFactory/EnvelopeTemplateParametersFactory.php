<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\ParametersFactory;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarkDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters\CreateEnvelopeTemplateParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters\EditEnvelopeTemplateParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureDefinitionService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Request\CreateEnvelopeTemplateRequest;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Request\EditEnvelopeTemplateRequest;
use Psr\Http\Message\ServerRequestInterface;

final class EnvelopeTemplateParametersFactory
{
    /** @var EnvelopeBackdropDefinitionService */
    private $envelopeBackdropDefinitionService;

    /** @var EnvelopeMarkDefinitionService */
    private $envelopeMarkDefinitionService;

    /** @var EnvelopePatternDefinitionService */
    private $envelopePatternDefinitionService;

    /** @var EnvelopeTextureDefinitionService */
    private $envelopeTextureDefinitionService;

    public function __construct(
        EnvelopeBackdropDefinitionService $envelopeBackdropDefinitionService,
        EnvelopeMarkDefinitionService $envelopeMarkDefinitionService,
        EnvelopePatternDefinitionService $envelopePatternDefinitionService,
        EnvelopeTextureDefinitionService $envelopeTextureDefinitionService
    ) {
        $this->envelopeBackdropDefinitionService = $envelopeBackdropDefinitionService;
        $this->envelopeMarkDefinitionService = $envelopeMarkDefinitionService;
        $this->envelopePatternDefinitionService = $envelopePatternDefinitionService;
        $this->envelopeTextureDefinitionService = $envelopeTextureDefinitionService;
    }


    public function factoryCreateParameters(ServerRequestInterface $request): CreateEnvelopeTemplateParameters
    {
        $json = (new CreateEnvelopeTemplateRequest($request))->getParameters();

        return new CreateEnvelopeTemplateParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->envelopeBackdropDefinitionService->getEnvelopeBackdropDefinitionById($json['envelope_backdrop_definition_id']),
            $this->envelopeTextureDefinitionService->getEnvelopeTextureDefinitionById($json['envelope_texture_definition_id']),
            $this->envelopePatternDefinitionService->getEnvelopePatternDefinitionById($json['envelope_pattern_definition_id']),
            $this->envelopeMarkDefinitionService->getEnvelopeMarkDefinitionById($json['envelope_mark_definition_id']),
            $json['config']
        );
    }

    public function factoryEditParameters(int $envelopeTemplateId, ServerRequestInterface $request): EditEnvelopeTemplateParameters
    {
        $json = (new EditEnvelopeTemplateRequest($request))->getParameters();

        return new EditEnvelopeTemplateParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->envelopeBackdropDefinitionService->getEnvelopeBackdropDefinitionById($json['envelope_backdrop_definition_id']),
            $this->envelopeTextureDefinitionService->getEnvelopeTextureDefinitionById($json['envelope_texture_definition_id']),
            $this->envelopePatternDefinitionService->getEnvelopePatternDefinitionById($json['envelope_pattern_definition_id']),
            $this->envelopeMarkDefinitionService->getEnvelopeMarkDefinitionById($json['envelope_mark_definition_id']),
            $json['config']
        );
    }
}