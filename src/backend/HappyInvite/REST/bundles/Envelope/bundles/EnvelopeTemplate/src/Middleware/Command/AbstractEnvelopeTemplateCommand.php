<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Formatter\EnvelopeTemplateFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service\EnvelopeTemplateService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\ParametersFactory\EnvelopeTemplateParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeTemplateCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeTemplateParametersFactory */
    protected $parametersFactory;

    /** @var EnvelopeTemplateFormatter */
    protected $formatter;

    /** @var EnvelopeTemplateService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        EnvelopeTemplateParametersFactory $parametersFactory,
        EnvelopeTemplateFormatter $formatter,
        EnvelopeTemplateService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}