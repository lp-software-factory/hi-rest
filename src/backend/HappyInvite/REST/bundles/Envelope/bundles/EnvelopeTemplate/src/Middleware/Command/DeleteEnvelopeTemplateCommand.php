<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Exceptions\EnvelopeTemplateNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEnvelopeTemplateCommand extends AbstractEnvelopeTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $envelopeTemplateId = $request->getAttribute('envelopeTemplateId');

        try {
            $template = $this->service->getById($envelopeTemplateId);

            if(! $this->accessService->isAdmin()) {
                if($template->isSolutionUserSpecified() /* Don't! It's pretty strange behaviour if we hot this case: && $template->hasSolutionComponentOwner() */) {
                    $this->accessService->requireOwner($template->getSolutionComponentOwner()->getId());
                }else{
                    throw new AccessDeniedException(sprintf("You're not allowed to edit envelope template `%s`", $envelopeTemplateId));
                }
            }

            $this->service->deleteEnvelopeTemplate($envelopeTemplateId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(EnvelopeTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}