<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Exceptions\EnvelopeTemplateNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEnvelopeTemplateCommand extends AbstractEnvelopeTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeTemplateId = $request->getAttribute('envelopeTemplateId');

        try {
            $entity = $this->service->getById($envelopeTemplateId);

            $responseBuilder
                ->setJSON([
                    'envelope_template' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(EnvelopeTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}