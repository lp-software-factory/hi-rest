<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\EnvelopeTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;

final class GetEnvelopeTemplateMiddlewareTest extends EnvelopeTemplateMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $fixture = EnvelopeTemplateFixture::$envelopeTemplateEF1;

        $this->requestEnvelopeTemplateGetById($fixture->getId())
            ->__invoke()
            ->expectJSONContentType()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'envelope_template' => [
                    'entity' => [
                        'id' => $fixture->getId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $this->requestEnvelopeTemplateGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}