<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters\CreateEnvelopeTemplateParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service\EnvelopeTemplateService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeTemplateFixture implements Fixture
{
    /** @var EnvelopeTemplate */
    public static $envelopeTemplateEF1;

    /** @var EnvelopeTemplate */
    public static $envelopeTemplateEF2;

    /** @var EnvelopeTemplate */
    public static $envelopeTemplateEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeTemplateService::class); /** @var EnvelopeTemplateService $service */

        self::$envelopeTemplateEF1 = $service->createEnvelopeTemplate(new CreateEnvelopeTemplateParameters(
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Envelope Template 1']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Envelope Template 1 Description']]),
            EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1,
            EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1,
            EnvelopePatternDefinitionFixture::$envelopeFixtureEF1,
            EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1,
            []
        ));

        self::$envelopeTemplateEF2 = $service->createEnvelopeTemplate(new CreateEnvelopeTemplateParameters(
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Envelope Template 2']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Envelope Template 2 Description']]),
            EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF2,
            EnvelopeTextureDefinitionFixture::$envelopeFixtureEF2,
            EnvelopePatternDefinitionFixture::$envelopeFixtureEF2,
            EnvelopeMarkDefinitionFixture::$envelopeFixtureEF2,
            []
        ));

        self::$envelopeTemplateEF3 = $service->createEnvelopeTemplate(new CreateEnvelopeTemplateParameters(
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Envelope Template 3']]),
            new ImmutableLocalizedString([['region' => 'en_US', 'value' => 'Envelope Template 3 Description']]),
            EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF3,
            EnvelopeTextureDefinitionFixture::$envelopeFixtureEF3,
            EnvelopePatternDefinitionFixture::$envelopeFixtureEF3,
            EnvelopeMarkDefinitionFixture::$envelopeFixtureEF3,
            []
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeTemplateEF1,
            self::$envelopeTemplateEF2,
            self::$envelopeTemplateEF3,
        ];
    }
}