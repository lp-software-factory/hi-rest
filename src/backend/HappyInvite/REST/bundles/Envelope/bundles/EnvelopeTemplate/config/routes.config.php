<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\EnvelopeTemplateMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/template/{command:create}[/]',
                'middleware' => EnvelopeTemplateMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/template/{envelopeTemplateId}/{command:edit}[/]',
                'middleware' => EnvelopeTemplateMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/template/{envelopeTemplateId}/{command:delete}[/]',
                'middleware' => EnvelopeTemplateMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/template/{envelopeTemplateId}/{command:get}[/]',
                'middleware' => EnvelopeTemplateMiddleware::class,
            ],
        ]
    ]
];