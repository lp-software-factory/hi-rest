<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeTemplateRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}