<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeTemplateCommand extends AbstractEnvelopeTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        $parameters = $this->parametersFactory->factoryCreateParameters($request);

        $entity = $this->service->createEnvelopeTemplate($parameters);

        if(! $this->accessService->isAdmin()) {
            $this->service->markAsUserSpecified($entity->getId(), $this->authToken->getProfile());
        }

        $responseBuilder
            ->setJSON([
                'envelope_template' => $this->formatter->formatOne($entity)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}