<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\EnvelopeTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Fixture\EnvelopeTemplateFixture;

final class DeleteEnvelopeTemplateMiddlewareTest extends EnvelopeTemplateMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $id = EnvelopeTemplateFixture::$envelopeTemplateEF1->getId();

        $this->requestEnvelopeTemplateGetById($id)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopeTemplateDelete($id)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopeTemplateGetById($id)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $id = EnvelopeTemplateFixture::$envelopeTemplateEF1->getId();

        $this->requestEnvelopeTemplateDelete($id)
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeTemplateDelete($id)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeTemplateFixture());

        $this->requestEnvelopeTemplateDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}