<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEnvelopeTemplateRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return self::getSchemaService()->getDefinition('HI_EnvelopeTemplateBundle_CreateEnvelopeTemplateRequest');
    }
}