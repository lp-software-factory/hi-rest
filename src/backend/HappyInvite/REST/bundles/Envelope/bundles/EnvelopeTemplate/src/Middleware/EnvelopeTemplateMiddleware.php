<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command\CreateEnvelopeTemplateCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command\DeleteEnvelopeTemplateCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command\EditEnvelopeTemplateCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Middleware\Command\GetEnvelopeTemplateCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeTemplateMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeTemplateCommand::class)
            ->attachDirect('edit', EditEnvelopeTemplateCommand::class)
            ->attachDirect('delete', DeleteEnvelopeTemplateCommand::class)
            ->attachDirect('get', GetEnvelopeTemplateCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}