<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\Tests\Fixture\EnvelopeBackdropDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\Tests\Fixture\EnvelopeMarkDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\Tests\EnvelopeTemplateMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\Tests\Fixture\EnvelopeTextureDefinitionFixture;

final class CreateEnvelopeTemplateMiddlewareTest extends EnvelopeTemplateMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            'title' => [['region' => 'en_US', 'value' => 'Envelope Template 1']],
            'description' => [['region' => 'en_US', 'value' => 'Envelope Template 1 Description']],
            'envelope_backdrop_definition_id' => EnvelopeBackdropDefinitionFixture::$envelopeFixtureEF1->getId(),
            'envelope_pattern_definition_id' => EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId(),
            'envelope_texture_definition_id' => EnvelopeTextureDefinitionFixture::$envelopeFixtureEF1->getId(),
            'envelope_mark_definition_id' => EnvelopeMarkDefinitionFixture::$envelopeFixtureEF1->getId(),
            'config' => ['foo' => 'bar'],
        ];
    }

    public function test200()
    {
        $id = $this->requestEnvelopeTemplateCreate($json = $this->getTestJSON())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'envelope_backdrop_definition' => [
                            'entity' => [
                                'id' => $json['envelope_backdrop_definition_id']
                            ]
                        ],
                        'envelope_texture_definition' => [
                            'entity' => [
                                'id' => $json['envelope_texture_definition_id']
                            ]
                        ],
                        'envelope_pattern_definition' => [
                            'entity' => [
                                'id' => $json['envelope_pattern_definition_id']
                            ]
                        ],
                        'envelope_mark_definition' => [
                            'entity' => [
                                'id' => $json['envelope_mark_definition_id']
                            ]
                        ],
                        'config' => $json['config'],
                        'solution' => [
                            'is_user_specified' => false,
                            'owner' => [
                                'has' => false,
                            ]
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['envelope_template']['entity']['id'];
            });

        $this->requestEnvelopeTemplateGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test200UserSpecified()
    {
        $id = $this->requestEnvelopeTemplateCreate($this->getTestJSON())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'solution' => [
                            'is_user_specified' => true,
                            'owner' => [
                                'has' => true,
                                'profile' => [
                                    'id' => UserAccountFixture::getProfile()->getId(),
                                ]
                            ]
                        ]
                    ]
                ]
            ])
            ->fetch(function(array $json) {
                return $json['envelope_template']['entity']['id'];
            });

        $this->requestEnvelopeTemplateGetById($id)
            ->__invoke()
            ->expectStatusCode(200);
    }

    public function test403()
    {
        $this->requestEnvelopeTemplateCreate($this->getTestJSON())
            ->__invoke()
            ->expectAuthError();
    }
}