<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions\EnvelopeMarksNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ActivateEnvelopeMarksCommand extends AbstractEnvelopeMarksCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeMarksId = $request->getAttribute('envelopeMarksId');

            $envelopeMarks = $this->envelopeMarksService->activateEnvelopeMarks($envelopeMarksId);

            $responseBuilder
                ->setJSON([
                    'envelope_marks' => $this->envelopeMarksFormatter->formatOne($envelopeMarks),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeMarksNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}