<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\CreateEnvelopeMarksParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarksService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopeMarksFixture implements Fixture
{
    /** @var EnvelopeMarks */
    public static $envelopeFixtureEF1;

    /** @var EnvelopeMarks */
    public static $envelopeFixtureEF2;

    /** @var EnvelopeMarks */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopeMarksService::class); /** @var EnvelopeMarksService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopeMarks(new CreateEnvelopeMarksParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 1 Description"]])
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopeMarks(new CreateEnvelopeMarksParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 2 Description"]])
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopeMarks(new CreateEnvelopeMarksParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Marks 3 Description"]])
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}