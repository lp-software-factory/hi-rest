<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopeMarksCommand extends AbstractEnvelopeMarksCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopeMarkss = $this->envelopeMarksService->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_marks' => $this->envelopeMarksFormatter->formatMany($envelopeMarkss),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}