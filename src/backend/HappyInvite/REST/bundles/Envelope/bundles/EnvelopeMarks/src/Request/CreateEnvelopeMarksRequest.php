<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\CreateEnvelopeMarksParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEnvelopeMarksRequest extends SchemaRequest
{
    public function getParameters(): CreateEnvelopeMarksParameters
    {
        $data = $this->getData();

        return new CreateEnvelopeMarksParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EnvelopeMarks_EnvelopeMarksRequest');
    }
}