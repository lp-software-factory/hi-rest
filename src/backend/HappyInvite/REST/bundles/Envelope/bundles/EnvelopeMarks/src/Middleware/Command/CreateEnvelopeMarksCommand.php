<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Request\CreateEnvelopeMarksRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopeMarksCommand extends AbstractEnvelopeMarksCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopeMarks = $this->envelopeMarksService->createEnvelopeMarks(
            (new CreateEnvelopeMarksRequest($request))->getParameters()
        );

        $responseBuilder
            ->setJSON([
                'envelope_marks' => $this->envelopeMarksFormatter->formatOne($envelopeMarks),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}