<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\EnvelopeMarksMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/marks/{command:create}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:edit}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:delete}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:move-up}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:activate}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:deactivate}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:move-down}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/marks/{envelopeMarksId}/{command:get}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/marks/{command:get-all}[/]',
                'middleware' => EnvelopeMarksMiddleware::class,
            ],
        ]
    ]
];