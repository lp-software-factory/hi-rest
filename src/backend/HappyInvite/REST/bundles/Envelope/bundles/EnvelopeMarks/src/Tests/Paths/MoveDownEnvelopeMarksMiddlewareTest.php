<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\EnvelopeMarksMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture\EnvelopeMarksFixture;

final class MoveDownEnvelopeMarksMiddlewareTest extends EnvelopeMarksMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $this->assertEnvelopeMarksPosition(1, EnvelopeMarksFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarksPosition(2, EnvelopeMarksFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeMarksPosition(3, EnvelopeMarksFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeMarksMoveDown(EnvelopeMarksFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopeMarksPosition(1, EnvelopeMarksFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarksPosition(2, EnvelopeMarksFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopeMarksPosition(3, EnvelopeMarksFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopeMarksMoveDown(EnvelopeMarksFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopeMarksPosition(1, EnvelopeMarksFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarksPosition(2, EnvelopeMarksFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeMarksPosition(3, EnvelopeMarksFixture::$envelopeFixtureEF2->getId());

        $this->requestEnvelopeMarksMoveDown(EnvelopeMarksFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopeMarksPosition(1, EnvelopeMarksFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopeMarksPosition(2, EnvelopeMarksFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopeMarksPosition(3, EnvelopeMarksFixture::$envelopeFixtureEF2->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $envelopeFixture = EnvelopeMarksFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarksMoveDown($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarksMoveDown($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $this->requestEnvelopeMarksMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}