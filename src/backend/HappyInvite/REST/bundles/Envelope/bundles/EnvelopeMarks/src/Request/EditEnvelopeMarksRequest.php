<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\EditEnvelopeMarksParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditEnvelopeMarksRequest extends SchemaRequest
{
    public function getParameters(): EditEnvelopeMarksParameters
    {
        $data = $this->getData();

        return new EditEnvelopeMarksParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EnvelopeMarks_EnvelopeMarksRequest');
    }
}