<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\ActivateEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\CreateEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\DeactivateEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\DeleteEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\EditEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\GetAllEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\GetEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\MoveDownEnvelopeMarksCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command\MoveUpEnvelopeMarksCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopeMarksMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopeMarksCommand::class)
            ->attachDirect('edit', EditEnvelopeMarksCommand::class)
            ->attachDirect('delete', DeleteEnvelopeMarksCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopeMarksCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopeMarksCommand::class)
            ->attachDirect('activate', ActivateEnvelopeMarksCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopeMarksCommand::class)
            ->attachDirect('get', GetEnvelopeMarksCommand::class)
            ->attachDirect('get-all', GetAllEnvelopeMarksCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}