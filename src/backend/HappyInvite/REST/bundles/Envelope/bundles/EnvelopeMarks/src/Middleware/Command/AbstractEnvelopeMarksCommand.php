<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Formatter\EnvelopeMarksFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarksService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopeMarksCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopeMarksService */
    protected $envelopeMarksService;

    /** @var EnvelopeMarksFormatter */
    protected $envelopeMarksFormatter;

    public function __construct(
        AccessService $accessService,
        EnvelopeMarksService $envelopeMarksService,
        EnvelopeMarksFormatter $envelopeMarksFormatter
    ) {
        $this->accessService = $accessService;
        $this->envelopeMarksService = $envelopeMarksService;
        $this->envelopeMarksFormatter = $envelopeMarksFormatter;
    }
}