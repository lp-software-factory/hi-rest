<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopeMarksRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}