<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class EnvelopeMarksMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new AttachmentFixture(),
        ];
    }

    protected function assertEnvelopeMarksPosition(int $position, int $envelopeMarksId)
    {
        $this->requestEnvelopeMarksGetById($envelopeMarksId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_marks' => [
                    'entity' => [
                        'id' => $envelopeMarksId,
                        'position' => $position,
                    ]
                ]
            ]);
    }
}