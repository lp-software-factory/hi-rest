<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions\EnvelopeMarksNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Request\EditEnvelopeMarksRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditEnvelopeMarksCommand extends AbstractEnvelopeMarksCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopeMarksId = $request->getAttribute('envelopeMarksId');

            $envelopeMarks = $this->envelopeMarksService->editEnvelopeMarks(
                $envelopeMarksId, (new EditEnvelopeMarksRequest($request))->getParameters()
            );

            $responseBuilder
                ->setJSON([
                    'envelope_marks' => $this->envelopeMarksFormatter->formatOne($envelopeMarks),
                ])
                ->setStatusSuccess();
        }catch(EnvelopeMarksNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}