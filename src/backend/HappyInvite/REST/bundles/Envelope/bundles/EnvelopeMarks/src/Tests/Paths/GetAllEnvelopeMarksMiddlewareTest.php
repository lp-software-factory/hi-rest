<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\EnvelopeMarksMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture\EnvelopeMarksFixture;

final class GetAllEnvelopeMarksMiddlewareTest extends EnvelopeMarksMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $ids = $this->requestEnvelopeMarksGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_marks' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_marks']);
            });

        $this->assertEquals(array_map(function(EnvelopeMarks $envelopeMarks) {
            return $envelopeMarks->getId();
        }, EnvelopeMarksFixture::getOrderedList()), $ids);
    }
}