<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopeMarksRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopeMarksCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/marks/create')
            ->setParameters($json);
    }

    protected function requestEnvelopeMarksEdit(int $envelopeMarksId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/%d/edit', $envelopeMarksId))
            ->setParameters($json);
    }

    protected function requestEnvelopeMarksDelete(int $envelopeMarksId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/marks/%d/delete', $envelopeMarksId));
    }

    protected function requestEnvelopeMarksMoveUp(int $envelopeMarksId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/%d/move-up', $envelopeMarksId));
    }

    protected function requestEnvelopeMarksMoveDown(int $envelopeMarksId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/%d/move-down', $envelopeMarksId));
    }

    protected function requestEnvelopeMarksActivate(int $envelopeMarksId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/%d/activate', $envelopeMarksId));
    }

    protected function requestEnvelopeMarksDeactivate(int $envelopeMarksId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/marks/%d/deactivate', $envelopeMarksId));
    }

    protected function requestEnvelopeMarksGetById(int $envelopeMarksId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/marks/%d/get', $envelopeMarksId));
    }

    protected function requestEnvelopeMarksGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/marks/get-all');
    }
}