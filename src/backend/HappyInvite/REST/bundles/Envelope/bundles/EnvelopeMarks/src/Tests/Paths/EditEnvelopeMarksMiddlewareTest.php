<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\EnvelopeMarksMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\Tests\Fixture\EnvelopeMarksFixture;

final class EditEnvelopeMarksMiddlewareTest extends EnvelopeMarksMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "* My Envelope Marks"]],
            "description" => [["region" => "en_GB", "value" => "* My Envelope Marks Description"]],
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeMarksFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarksEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_marks' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopeMarksFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopeMarksEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopeMarksEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopeMarksFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopeMarksEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}