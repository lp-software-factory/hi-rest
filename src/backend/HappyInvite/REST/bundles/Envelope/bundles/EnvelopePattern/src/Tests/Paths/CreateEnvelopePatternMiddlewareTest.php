<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\EnvelopePatternMiddlewareTestCase;

final class CreateEnvelopePatternMiddlewareTest extends EnvelopePatternMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "My Envelope Pattern"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Pattern Description"]],
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopePatternCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_pattern' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopePatternCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}