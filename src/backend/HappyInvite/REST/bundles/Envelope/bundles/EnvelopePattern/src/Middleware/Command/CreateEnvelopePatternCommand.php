<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Request\CreateEnvelopePatternRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopePatternCommand extends AbstractEnvelopePatternCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $envelopePattern = $this->envelopePatternService->createEnvelopePattern(
            (new CreateEnvelopePatternRequest($request))->getParameters()
        );

        $responseBuilder
            ->setJSON([
                'envelope_pattern' => $this->envelopePatternFormatter->formatOne($envelopePattern),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}