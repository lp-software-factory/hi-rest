<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\EnvelopePatternMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;

final class GetAllEnvelopePatternMiddlewareTest extends EnvelopePatternMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $ids = $this->requestEnvelopePatternGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_patterns' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_patterns']);
            });

        $this->assertEquals(array_map(function(EnvelopePattern $envelopePattern) {
            return $envelopePattern->getId();
        }, EnvelopePatternFixture::getOrderedList()), $ids);
    }
}