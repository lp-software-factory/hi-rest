<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\ActivateEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\CreateEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\DeactivateEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\DeleteEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\EditEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\GetAllEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\GetEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\MoveDownEnvelopePatternCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command\MoveUpEnvelopePatternCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopePatternMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopePatternCommand::class)
            ->attachDirect('edit', EditEnvelopePatternCommand::class)
            ->attachDirect('delete', DeleteEnvelopePatternCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopePatternCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopePatternCommand::class)
            ->attachDirect('activate', ActivateEnvelopePatternCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopePatternCommand::class)
            ->attachDirect('get', GetEnvelopePatternCommand::class)
            ->attachDirect('get-all', GetAllEnvelopePatternCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}