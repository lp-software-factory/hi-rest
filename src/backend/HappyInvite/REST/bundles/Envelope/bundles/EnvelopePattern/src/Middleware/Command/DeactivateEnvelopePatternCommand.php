<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions\EnvelopePatternNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateEnvelopePatternCommand extends AbstractEnvelopePatternCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopePatternId = $request->getAttribute('envelopePatternId');

            $envelopePattern = $this->envelopePatternService->deactivateEnvelopePattern($envelopePatternId);

            $responseBuilder
                ->setJSON([
                    'envelope_pattern' => $this->envelopePatternFormatter->formatOne($envelopePattern),
                ])
                ->setStatusSuccess();
        }catch(EnvelopePatternNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}