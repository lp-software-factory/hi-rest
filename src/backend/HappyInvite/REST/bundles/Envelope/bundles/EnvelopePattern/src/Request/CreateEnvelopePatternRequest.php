<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Request;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\CreateEnvelopePatternParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateEnvelopePatternRequest extends SchemaRequest
{
    public function getParameters(): CreateEnvelopePatternParameters
    {
        $data = $this->getData();

        return new CreateEnvelopePatternParameters(
            new ImmutableLocalizedString($data['title']),
            new ImmutableLocalizedString($data['description'])
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_EnvelopePattern_EnvelopePatternRequest');
    }
}