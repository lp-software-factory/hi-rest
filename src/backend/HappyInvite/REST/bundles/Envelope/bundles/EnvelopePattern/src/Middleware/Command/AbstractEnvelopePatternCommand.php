<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Formatter\EnvelopePatternFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopePatternCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopePatternService */
    protected $envelopePatternService;

    /** @var EnvelopePatternFormatter */
    protected $envelopePatternFormatter;

    public function __construct(
        AccessService $accessService,
        EnvelopePatternService $envelopePatternService,
        EnvelopePatternFormatter $envelopePatternFormatter
    ) {
        $this->accessService = $accessService;
        $this->envelopePatternService = $envelopePatternService;
        $this->envelopePatternFormatter = $envelopePatternFormatter;
    }
}