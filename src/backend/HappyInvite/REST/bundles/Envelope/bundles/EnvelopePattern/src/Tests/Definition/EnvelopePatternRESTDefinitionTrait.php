<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopePatternRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopePatternCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/pattern/create')
            ->setParameters($json);
    }

    protected function requestEnvelopePatternEdit(int $envelopePatternId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/%d/edit', $envelopePatternId))
            ->setParameters($json);
    }

    protected function requestEnvelopePatternDelete(int $envelopePatternId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/pattern/%d/delete', $envelopePatternId));
    }

    protected function requestEnvelopePatternMoveUp(int $envelopePatternId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/%d/move-up', $envelopePatternId));
    }

    protected function requestEnvelopePatternMoveDown(int $envelopePatternId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/%d/move-down', $envelopePatternId));
    }

    protected function requestEnvelopePatternActivate(int $envelopePatternId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/%d/activate', $envelopePatternId));
    }

    protected function requestEnvelopePatternDeactivate(int $envelopePatternId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/%d/deactivate', $envelopePatternId));
    }

    protected function requestEnvelopePatternGetById(int $envelopePatternId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/pattern/%d/get', $envelopePatternId));
    }

    protected function requestEnvelopePatternGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/pattern/get-all');
    }
}