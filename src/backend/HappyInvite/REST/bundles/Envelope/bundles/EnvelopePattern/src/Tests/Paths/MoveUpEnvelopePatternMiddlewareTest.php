<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\EnvelopePatternMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;

final class MoveUpEnvelopePatternMiddlewareTest extends EnvelopePatternMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $this->assertEnvelopePatternPosition(1, EnvelopePatternFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternPosition(2, EnvelopePatternFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopePatternPosition(3, EnvelopePatternFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopePatternMoveUp(EnvelopePatternFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopePatternPosition(1, EnvelopePatternFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternPosition(2, EnvelopePatternFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopePatternPosition(3, EnvelopePatternFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopePatternMoveUp(EnvelopePatternFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertEnvelopePatternPosition(1, EnvelopePatternFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopePatternPosition(2, EnvelopePatternFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternPosition(3, EnvelopePatternFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopePatternMoveUp(EnvelopePatternFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopePatternPosition(1, EnvelopePatternFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopePatternPosition(2, EnvelopePatternFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopePatternPosition(3, EnvelopePatternFixture::$envelopeFixtureEF1->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $envelopeFixture = EnvelopePatternFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternMoveUp($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternMoveUp($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $this->requestEnvelopePatternMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}