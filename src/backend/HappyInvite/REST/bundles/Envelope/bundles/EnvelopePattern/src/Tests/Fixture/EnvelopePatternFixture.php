<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\CreateEnvelopePatternParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopePatternFixture implements Fixture
{
    /** @var EnvelopePattern */
    public static $envelopeFixtureEF1;

    /** @var EnvelopePattern */
    public static $envelopeFixtureEF2;

    /** @var EnvelopePattern */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopePatternService::class); /** @var EnvelopePatternService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopePattern(new CreateEnvelopePatternParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 1 Description"]])
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopePattern(new CreateEnvelopePatternParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 2 Description"]])
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopePattern(new CreateEnvelopePatternParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 3 Description"]])
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}