<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopePatternRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}