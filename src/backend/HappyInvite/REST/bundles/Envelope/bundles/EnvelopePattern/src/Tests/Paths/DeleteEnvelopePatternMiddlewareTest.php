<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\EnvelopePatternMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;

final class DeleteEnvelopePatternMiddlewareTest extends EnvelopePatternMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $envelopeFixture = EnvelopePatternFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopePatternDelete($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopePatternGetById($envelopeFixtureId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternFixture());
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternFixture());
    }
}