<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\EnvelopePatternMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;

final class ActivateEnvelopePatternMiddlewareTest extends EnvelopePatternMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $envelopeFixture = EnvelopePatternFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternActivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopePatternGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopePatternDeactivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);

        $this->requestEnvelopePatternGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $envelopeFixture = EnvelopePatternFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternActivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternActivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDeactivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDeactivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $this->requestEnvelopePatternActivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEnvelopePatternDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}