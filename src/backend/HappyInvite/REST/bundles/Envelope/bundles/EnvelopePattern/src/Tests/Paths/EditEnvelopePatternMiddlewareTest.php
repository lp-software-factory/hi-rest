<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\EnvelopePatternMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;

final class EditEnvelopePatternMiddlewareTest extends EnvelopePatternMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "* My Envelope Pattern"]],
            "description" => [["region" => "en_GB", "value" => "* My Envelope Pattern Description"]],
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopePatternFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_pattern' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopePatternFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopePatternEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}