<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\EnvelopePatternMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/pattern/{command:create}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:edit}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:delete}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:activate}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:deactivate}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:move-up}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:move-down}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/pattern/{envelopePatternId}/{command:get}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/pattern/{command:get-all}[/]',
                'middleware' => EnvelopePatternMiddleware::class,
            ],
        ]
    ]
];