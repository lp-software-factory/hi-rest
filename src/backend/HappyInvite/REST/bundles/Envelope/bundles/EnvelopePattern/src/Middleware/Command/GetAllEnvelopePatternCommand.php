<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopePatternCommand extends AbstractEnvelopePatternCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopePatterns = $this->envelopePatternService->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_patterns' => $this->envelopePatternFormatter->formatMany($envelopePatterns),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}