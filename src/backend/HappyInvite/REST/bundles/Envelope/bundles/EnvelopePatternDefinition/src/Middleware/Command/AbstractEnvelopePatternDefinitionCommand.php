<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Formatter\EnvelopePatternDefinitionFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternDefinitionService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\ParametersFactory\EnvelopePatternDefinitionParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractEnvelopePatternDefinitionCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var EnvelopePatternDefinitionService */
    protected $envelopePatternDefinitionService;

    /** @var EnvelopePatternDefinitionFormatter */
    protected $envelopePatternDefinitionFormatter;

    /** @var EnvelopePatternDefinitionParametersFactory */
    protected $envelopePatternDefinitionParametersFactory;

    public function __construct(
        AccessService $accessService,
        EnvelopePatternDefinitionService $envelopePatternDefinitionService,
        EnvelopePatternDefinitionFormatter $envelopePatternDefinitionFormatter,
        EnvelopePatternDefinitionParametersFactory $envelopePatternDefinitionParametersFactory
    ) {
        $this->accessService = $accessService;
        $this->envelopePatternDefinitionService = $envelopePatternDefinitionService;
        $this->envelopePatternDefinitionFormatter = $envelopePatternDefinitionFormatter;
        $this->envelopePatternDefinitionParametersFactory = $envelopePatternDefinitionParametersFactory;
    }
}