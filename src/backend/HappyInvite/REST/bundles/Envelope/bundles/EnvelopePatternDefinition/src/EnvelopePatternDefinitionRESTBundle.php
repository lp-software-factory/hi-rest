<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition;

use HappyInvite\REST\Bundle\RESTBundle;

final class EnvelopePatternDefinitionRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}