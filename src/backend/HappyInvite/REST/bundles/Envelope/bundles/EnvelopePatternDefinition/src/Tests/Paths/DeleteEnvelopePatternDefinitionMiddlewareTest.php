<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;

final class DeleteEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestEnvelopePatternDefinitionDelete($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ]);

        $this->requestEnvelopePatternDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());
    }
}