<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions\EnvelopePatternDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Request\EditEnvelopePatternDefinitionRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditEnvelopePatternDefinitionCommand extends AbstractEnvelopePatternDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopePatternDefinitionId = $request->getAttribute('envelopePatternDefinitionId');

            $parameters =  $this->envelopePatternDefinitionParametersFactory->factoryEditParameters(
                $envelopePatternDefinitionId,
                (new EditEnvelopePatternDefinitionRequest($request))->getParameters()
            );

            $envelopePatternDefinition = $this->envelopePatternDefinitionService->editEnvelopePatternDefinition(
                $envelopePatternDefinitionId, $parameters
            );

            $responseBuilder
                ->setJSON([
                    'envelope_pattern' => $this->envelopePatternDefinitionFormatter->formatOne($envelopePatternDefinition),
                ])
                ->setStatusSuccess();
        }catch(EnvelopePatternDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}