<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class EnvelopePatternDefinitionMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new AttachmentFixture(),
            new EnvelopeColorFixture(),
            new EnvelopePatternFixture(),
        ];
    }

    protected function assertEnvelopePatternDefinitionPosition(int $position, int $envelopePatternDefinitionId)
    {
        $this->requestEnvelopePatternDefinitionGetById($envelopePatternDefinitionId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'id' => $envelopePatternDefinitionId,
                        'position' => $position,
                    ]
                ]
            ]);
    }
}