<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Request\CreateEnvelopePatternDefinitionRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateEnvelopePatternDefinitionCommand extends AbstractEnvelopePatternDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->envelopePatternDefinitionParametersFactory->factoryCreateParameters(
            (new CreateEnvelopePatternDefinitionRequest($request))->getParameters()
        );

        $envelopePatternDefinition = $this->envelopePatternDefinitionService->createEnvelopePatternDefinition($parameters);

        $responseBuilder
            ->setJSON([
                'envelope_pattern' => $this->envelopePatternDefinitionFormatter->formatOne($envelopePatternDefinition),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}