<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;

final class MoveDownEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $this->assertEnvelopePatternDefinitionPosition(1, EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternDefinitionPosition(2, EnvelopePatternDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopePatternDefinitionPosition(3, EnvelopePatternDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopePatternDefinitionMoveDown(EnvelopePatternDefinitionFixture::$envelopeFixtureEF3->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopePatternDefinitionPosition(1, EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternDefinitionPosition(2, EnvelopePatternDefinitionFixture::$envelopeFixtureEF2->getId());
        $this->assertEnvelopePatternDefinitionPosition(3, EnvelopePatternDefinitionFixture::$envelopeFixtureEF3->getId());

        $this->requestEnvelopePatternDefinitionMoveDown(EnvelopePatternDefinitionFixture::$envelopeFixtureEF2->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertEnvelopePatternDefinitionPosition(1, EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternDefinitionPosition(2, EnvelopePatternDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopePatternDefinitionPosition(3, EnvelopePatternDefinitionFixture::$envelopeFixtureEF2->getId());

        $this->requestEnvelopePatternDefinitionMoveDown(EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertEnvelopePatternDefinitionPosition(1, EnvelopePatternDefinitionFixture::$envelopeFixtureEF3->getId());
        $this->assertEnvelopePatternDefinitionPosition(2, EnvelopePatternDefinitionFixture::$envelopeFixtureEF1->getId());
        $this->assertEnvelopePatternDefinitionPosition(3, EnvelopePatternDefinitionFixture::$envelopeFixtureEF2->getId());
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionMoveDown($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDefinitionMoveDown($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $this->requestEnvelopePatternDefinitionMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}