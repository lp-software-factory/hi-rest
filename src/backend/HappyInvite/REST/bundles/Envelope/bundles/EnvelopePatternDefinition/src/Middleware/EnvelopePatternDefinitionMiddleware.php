<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\ActivateEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\CreateEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\DeactivateEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\DeleteEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\EditEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\GetAllEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\GetEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\MoveDownEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command\MoveUpEnvelopePatternDefinitionCommand;
use HappyInvite\REST\Command\Service\CommandService;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EnvelopePatternDefinitionMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateEnvelopePatternDefinitionCommand::class)
            ->attachDirect('edit', EditEnvelopePatternDefinitionCommand::class)
            ->attachDirect('delete', DeleteEnvelopePatternDefinitionCommand::class)
            ->attachDirect('move-up', MoveUpEnvelopePatternDefinitionCommand::class)
            ->attachDirect('move-down', MoveDownEnvelopePatternDefinitionCommand::class)
            ->attachDirect('activate', ActivateEnvelopePatternDefinitionCommand::class)
            ->attachDirect('deactivate', DeactivateEnvelopePatternDefinitionCommand::class)
            ->attachDirect('get', GetEnvelopePatternDefinitionCommand::class)
            ->attachDirect('get-all', GetAllEnvelopePatternDefinitionCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}