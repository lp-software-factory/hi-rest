<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;

final class GetAllEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $ids = $this->requestEnvelopePatternDefinitionGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_patterns' => $this->expectArray(),
            ])
            ->fetch(function(array $json) {
                return array_map(function(array $entity) {
                    return $entity['entity']['id'];
                }, $json['envelope_patterns']);
            });

        $this->assertEquals(array_map(function(EnvelopePatternDefinition $envelopePatternDefinition) {
            return $envelopePatternDefinition->getId();
        }, EnvelopePatternDefinitionFixture::getOrderedList()), $ids);
    }
}