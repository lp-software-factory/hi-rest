<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions\EnvelopePatternDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEnvelopePatternDefinitionCommand extends AbstractEnvelopePatternDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopePatternDefinitionId = $request->getAttribute('envelopePatternDefinitionId');

            $this->envelopePatternDefinitionService->deleteEnvelopePatternDefinition($envelopePatternDefinitionId);

            $responseBuilder
                ->setStatusSuccess();
        }catch(EnvelopePatternDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}