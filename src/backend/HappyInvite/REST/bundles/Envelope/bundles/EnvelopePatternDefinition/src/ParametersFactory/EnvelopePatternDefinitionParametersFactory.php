<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service\EnvelopeColorService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition\CreateEnvelopePatternDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition\EditEnvelopePatternDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternService;

final class EnvelopePatternDefinitionParametersFactory
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeColorService */
    private $envelopeColorService;

    /** @var EnvelopePatternService */
    private $envelopePatternService;

    public function __construct(
        AttachmentService $attachmentService,
        EnvelopeColorService $envelopeColorService,
        EnvelopePatternService $envelopePatternService
    ) {
        $this->attachmentService = $attachmentService;
        $this->envelopeColorService = $envelopeColorService;
        $this->envelopePatternService = $envelopePatternService;
    }

    public function factoryCreateParameters(array $input): CreateEnvelopePatternDefinitionParameters
    {
        return new CreateEnvelopePatternDefinitionParameters(
            $this->envelopePatternService->getEnvelopePatternById($input['envelope_pattern_id']),
            $this->attachmentService->getById($input['texture_attachment_id']),
            $this->attachmentService->getById($input['preview_attachment_id']),
            new ImmutableLocalizedString($input['title']),
            new ImmutableLocalizedString($input['description']),
            $this->envelopeColorService->getEnvelopeColorById($input['envelope_color_id']),
            $input['width'],
            $input['height']
        );
    }

    public function factoryEditParameters(int $envelopePatternDefinitionId, array $input): EditEnvelopePatternDefinitionParameters
    {
        return new EditEnvelopePatternDefinitionParameters(
            $this->envelopePatternService->getEnvelopePatternById($input['envelope_pattern_id']),
            $this->attachmentService->getById($input['texture_attachment_id']),
            $this->attachmentService->getById($input['preview_attachment_id']),
            new ImmutableLocalizedString($input['title']),
            new ImmutableLocalizedString($input['description']),
            $this->envelopeColorService->getEnvelopeColorById($input['envelope_color_id']),
            $input['width'],
            $input['height']
        );
    }
}