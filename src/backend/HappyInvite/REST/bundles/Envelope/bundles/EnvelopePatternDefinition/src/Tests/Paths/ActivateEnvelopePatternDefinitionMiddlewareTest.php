<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;

final class ActivateEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionActivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopePatternDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestEnvelopePatternDefinitionDeactivate($envelopeFixtureId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);

        $this->requestEnvelopePatternDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'envelope_pattern' => [
                    'entity' => [
                        'is_activated' => false,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionActivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDefinitionActivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDefinitionDeactivate($envelopeFixtureId)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDefinitionDeactivate($envelopeFixtureId)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $this->requestEnvelopePatternDefinitionActivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $this->requestEnvelopePatternDefinitionDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}