<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;

final class GetByIdEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionGetById($envelopeFixtureId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_pattern' => [
                    'entity' => [
                        'id' => $this->expectId(),
                    ]
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $this->requestEnvelopePatternDefinitionGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}