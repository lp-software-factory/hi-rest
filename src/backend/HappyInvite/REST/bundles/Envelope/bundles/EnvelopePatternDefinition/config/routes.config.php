<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition;

use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\EnvelopePatternDefinitionMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/envelope/pattern/definition/{command:create}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:edit}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:delete}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:activate}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:deactivate}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:move-up}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:move-down}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/pattern/definition/{envelopePatternDefinitionId}/{command:get}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/envelope/pattern/definition/{command:get-all}[/]',
                'middleware' => EnvelopePatternDefinitionMiddleware::class,
            ],
        ]
    ]
];