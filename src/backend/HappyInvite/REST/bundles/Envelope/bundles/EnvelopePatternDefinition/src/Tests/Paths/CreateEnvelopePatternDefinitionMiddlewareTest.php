<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;

final class CreateEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "My Envelope Pattern"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Pattern Description"]],
            'texture_attachment_id' => AttachmentFixture::$attachmentImage->getId(),
            'preview_attachment_id' => AttachmentFixture::$attachmentImage2->getId(),
            'envelope_pattern_id' => EnvelopePatternFixture::$envelopeFixtureEF1->getId(),
            'envelope_color_id' => EnvelopeColorFixture::$envelopeFixtureEF1->getId(),
            'width' => 570,
            'height' => 810
        ];
    }

    public function test200()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopePatternDefinitionCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_pattern' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'attachment' => [
                            'id' => $json['texture_attachment_id']
                        ],
                        'preview' => [
                            'id' => $json['preview_attachment_id'],
                        ],
                        'width' => $json['width'],
                        'height' => $json['height'],
                        'envelope_pattern_id' => $json['envelope_pattern_id'],
                        'envelope_color_id' => $json['envelope_color_id'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $json = $this->getTestJSON();

        $this->requestEnvelopePatternDefinitionCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDefinitionCreate($json)
            ->__invoke()
            ->expectAuthError();
    }
}