<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition\CreateEnvelopePatternDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternDefinitionService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class EnvelopePatternDefinitionFixture implements Fixture
{
    /** @var EnvelopePatternDefinition */
    public static $envelopeFixtureEF1;

    /** @var EnvelopePatternDefinition */
    public static $envelopeFixtureEF2;

    /** @var EnvelopePatternDefinition */
    public static $envelopeFixtureEF3;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(EnvelopePatternDefinitionService::class); /** @var EnvelopePatternDefinitionService $service */

        self::$envelopeFixtureEF1 = $service->createEnvelopePatternDefinition(new CreateEnvelopePatternDefinitionParameters(
            EnvelopePatternFixture::$envelopeFixtureEF1,
            AttachmentFixture::$attachmentImage,
            AttachmentFixture::$attachmentImage2,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 1"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 1 Description"]]),
            EnvelopeColorFixture::$envelopeFixtureEF1,
            570, 810
        ));

        self::$envelopeFixtureEF2 = $service->createEnvelopePatternDefinition(new CreateEnvelopePatternDefinitionParameters(
            EnvelopePatternFixture::$envelopeFixtureEF1,
            AttachmentFixture::$attachmentImage2,
            AttachmentFixture::$attachmentImage3,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 2"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 2 Description"]]),
            EnvelopeColorFixture::$envelopeFixtureEF1,
            570, 750
        ));

        self::$envelopeFixtureEF3 = $service->createEnvelopePatternDefinition(new CreateEnvelopePatternDefinitionParameters(
            EnvelopePatternFixture::$envelopeFixtureEF1,
            AttachmentFixture::$attachmentImage3,
            AttachmentFixture::$attachmentImage,
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 3"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Envelope Pattern 3 Description"]]),
            EnvelopeColorFixture::$envelopeFixtureEF3,
            810, 570
        ));
    }

    public static function getOrderedList(): array
    {
        return [
            self::$envelopeFixtureEF1,
            self::$envelopeFixtureEF2,
            self::$envelopeFixtureEF3,
        ];
    }
}