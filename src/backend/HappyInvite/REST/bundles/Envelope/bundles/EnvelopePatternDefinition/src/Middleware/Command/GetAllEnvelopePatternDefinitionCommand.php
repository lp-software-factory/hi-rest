<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllEnvelopePatternDefinitionCommand extends AbstractEnvelopePatternDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $envelopePatternDefinitions = $this->envelopePatternDefinitionService->getAll();

        $responseBuilder
            ->setJSON([
                'envelope_patterns' => $this->envelopePatternDefinitionFormatter->formatMany($envelopePatternDefinitions),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}