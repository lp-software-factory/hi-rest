<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Middleware\Command;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions\EnvelopePatternDefinitionNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ActivateEnvelopePatternDefinitionCommand extends AbstractEnvelopePatternDefinitionCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $envelopePatternDefinitionId = $request->getAttribute('envelopePatternDefinitionId');

            $envelopePatternDefinition = $this->envelopePatternDefinitionService->activateEnvelopePatternDefinition($envelopePatternDefinitionId);

            $responseBuilder
                ->setJSON([
                    'envelope_pattern' => $this->envelopePatternDefinitionFormatter->formatOne($envelopePatternDefinition),
                ])
                ->setStatusSuccess();
        }catch(EnvelopePatternDefinitionNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}