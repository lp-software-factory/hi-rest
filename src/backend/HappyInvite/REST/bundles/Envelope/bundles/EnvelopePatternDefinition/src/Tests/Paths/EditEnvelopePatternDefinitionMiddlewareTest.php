<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\Tests\Fixture\EnvelopeColorFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\Tests\Fixture\EnvelopePatternFixture;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\EnvelopePatternDefinitionMiddlewareTestCase;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Fixture\EnvelopePatternDefinitionFixture;

final class EditEnvelopePatternDefinitionMiddlewareTest extends EnvelopePatternDefinitionMiddlewareTestCase
{
    private function getTestJSON(): array
    {
        return [
            "title" => [["region" => "en_GB", "value" => "My Envelope Pattern *"]],
            "description" => [["region" => "en_GB", "value" => "My Envelope Pattern Description *"]],
            'texture_attachment_id' => AttachmentFixture::$attachmentImage3->getId(),
            'preview_attachment_id' => AttachmentFixture::$attachmentImage2->getId(),
            'envelope_pattern_id' => EnvelopePatternFixture::$envelopeFixtureEF2->getId(),
            'envelope_color_id' => EnvelopeColorFixture::$envelopeFixtureEF2->getId(),
            'width' => 1024,
            'height' => 768
        ];
    }

    public function test200()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionEdit($envelopeFixtureId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'envelope_pattern' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'position' => 1,
                        'attachment' => [
                            'id' => $json['texture_attachment_id']
                        ],
                        'preview' => [
                            'id' => $json['preview_attachment_id'],
                        ],
                        'width' => $json['width'],
                        'height' => $json['height'],
                        'envelope_pattern_id' => $json['envelope_pattern_id'],
                        'envelope_color_id' => $json['envelope_color_id'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $json = $this->getTestJSON();
        $envelopeFixture = EnvelopePatternDefinitionFixture::$envelopeFixtureEF1;
        $envelopeFixtureId = $envelopeFixture->getId();

        $this->requestEnvelopePatternDefinitionEdit($envelopeFixtureId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();

        $this->requestEnvelopePatternDefinitionEdit($envelopeFixtureId, $json)
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new EnvelopePatternDefinitionFixture());

        $json = $this->getTestJSON();

        $this->requestEnvelopePatternDefinitionEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}