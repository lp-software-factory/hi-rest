<?php
namespace HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait EnvelopePatternDefinitionRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestEnvelopePatternDefinitionCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/envelope/pattern/definition/create')
            ->setParameters($json);
    }

    protected function requestEnvelopePatternDefinitionEdit(int $envelopePatternDefinitionId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/definition/%d/edit', $envelopePatternDefinitionId))
            ->setParameters($json);
    }

    protected function requestEnvelopePatternDefinitionDelete(int $envelopePatternDefinitionId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/envelope/pattern/definition/%d/delete', $envelopePatternDefinitionId));
    }

    protected function requestEnvelopePatternDefinitionMoveUp(int $envelopePatternDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/definition/%d/move-up', $envelopePatternDefinitionId));
    }

    protected function requestEnvelopePatternDefinitionMoveDown(int $envelopePatternDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/definition/%d/move-down', $envelopePatternDefinitionId));
    }

    protected function requestEnvelopePatternDefinitionActivate(int $envelopePatternDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/definition/%d/activate', $envelopePatternDefinitionId));
    }

    protected function requestEnvelopePatternDefinitionDeactivate(int $envelopePatternDefinitionId): RESTRequest
    {
        return $this->request('POST', sprintf('/envelope/pattern/definition/%d/deactivate', $envelopePatternDefinitionId));
    }

    protected function requestEnvelopePatternDefinitionGetById(int $envelopePatternDefinitionId): RESTRequest
    {
        return $this->request('GET', sprintf('/envelope/pattern/definition/%d/get', $envelopePatternDefinitionId));
    }

    protected function requestEnvelopePatternDefinitionGetAll(): RESTRequest
    {
        return $this->request('GET', '/envelope/pattern/definition/get-all');
    }
}