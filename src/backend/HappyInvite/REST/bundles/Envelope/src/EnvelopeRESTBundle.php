<?php
namespace HappyInvite\REST\Bundles\Envelope;

use HappyInvite\REST\Bundle\RESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdropDefinition\EnvelopeBackdropDefinitionRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarkDefinition\EnvelopeMarkDefinitionRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePatternDefinition\EnvelopePatternDefinitionRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTemplate\EnvelopeTemplateRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeBackdrop\EnvelopeBackdropRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeColor\EnvelopeColorRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeMarks\EnvelopeMarksRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopePattern\EnvelopePatternRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTexture\EnvelopeTextureRESTBundle;
use HappyInvite\REST\Bundles\Envelope\Bundles\EnvelopeTextureDefinition\EnvelopeTextureDefinitionRESTBundle;

final class EnvelopeRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new EnvelopeTextureRESTBundle(),
            new EnvelopeMarksRESTBundle(),
            new EnvelopeColorRESTBundle(),
            new EnvelopeBackdropRESTBundle(),
            new EnvelopePatternRESTBundle(),
            new EnvelopeTemplateRESTBundle(),
            new EnvelopePatternDefinitionRESTBundle(),
            new EnvelopeMarkDefinitionRESTBundle(),
            new EnvelopeTextureDefinitionRESTBundle(),
            new EnvelopeBackdropDefinitionRESTBundle(),
        ];
    }
}