<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers;

use HappyInvite\REST\Bundle\RESTBundle;

final class OAuth2ProvidersRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}