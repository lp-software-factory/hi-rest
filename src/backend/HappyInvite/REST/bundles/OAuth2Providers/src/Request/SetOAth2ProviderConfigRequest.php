<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Request;

use HappyInvite\Domain\Bundles\OAuth2\Parameters\SetOAuth2ConfigParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class SetOAth2ProviderConfigRequest extends SchemaRequest
{
    public function getParameters(): SetOAuth2ConfigParameters
    {
        return new SetOAuth2ConfigParameters($this->getData()['config']);
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_OAuth2ProvidersBundle_SetConfigRequest');
    }
}