<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Request;

use HappyInvite\Domain\Bundles\OAuth2\Parameters\CreateOAuth2ProviderParameters;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateOAuth2ProviderRequest extends SchemaRequest
{
    public function getParameters(): CreateOAuth2ProviderParameters
    {
        $data = $this->getData();

        return new CreateOAuth2ProviderParameters(
            $data['code'],
            $data['handler'],
            $data['config']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_OAuth2ProvidersBundle_CreateOAuth2ProviderRequest');
    }
}