<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Middleware\Command;

use HappyInvite\Domain\Bundles\OAuth2\Exceptions\OAuth2ProviderNotFoundException;
use HappyInvite\Domain\Bundles\OAuth2\Exceptions\UnknownLeagueProviderException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetProviderCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $provider = $this->oauth2ProviderService->getProviderByCode($request->getAttribute('code'));

            $responseBuilder
                ->setJSON([
                    'provider' => $provider->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(OAuth2ProviderNotFoundException | UnknownLeagueProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}