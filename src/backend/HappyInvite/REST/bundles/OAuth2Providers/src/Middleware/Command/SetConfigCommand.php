<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Middleware\Command;

use HappyInvite\Domain\Bundles\OAuth2\Exceptions\OAuth2ProviderNotFoundException;
use HappyInvite\Domain\Bundles\OAuth2\Exceptions\UnknownLeagueProviderException;
use HappyInvite\REST\Bundles\OAuth2Providers\Request\SetOAth2ProviderConfigRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetConfigCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $code = $request->getAttribute('code');
            $config = (new SetOAth2ProviderConfigRequest($request))->getParameters();

            $provider =  $this->oauth2ProviderService->setConfigFor($code, $config);

            $responseBuilder
                ->setJSON([
                    'provider' => $provider->toJSON()]
                )
                ->setStatusSuccess();
        }catch(OAuth2ProviderNotFoundException | UnknownLeagueProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}