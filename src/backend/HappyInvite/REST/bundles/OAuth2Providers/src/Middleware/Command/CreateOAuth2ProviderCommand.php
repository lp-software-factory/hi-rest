<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Middleware\Command;

use HappyInvite\Domain\Bundles\OAuth2\Exceptions\DuplicateOAuth2ProviderException;
use HappyInvite\REST\Bundles\OAuth2Providers\Request\CreateOAuth2ProviderRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateOAuth2ProviderCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $parameters = (new CreateOAuth2ProviderRequest($request))->getParameters();

            $oauth2Provider = $this->oauth2ProviderService->createOAuth2Provider($parameters);

            $responseBuilder
                ->setJSON([
                    'provider' => $oauth2Provider->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(DuplicateOAuth2ProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}