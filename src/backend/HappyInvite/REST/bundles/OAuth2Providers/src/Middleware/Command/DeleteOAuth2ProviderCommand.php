<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Middleware\Command;

use HappyInvite\Domain\Bundles\OAuth2\Exceptions\OAuth2ProviderNotFoundException;
use HappyInvite\Domain\Bundles\OAuth2\Exceptions\UnknownLeagueProviderException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteOAuth2ProviderCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $this->oauth2ProviderService->deleteOAuth2Provider($request->getAttribute('code'));

            $responseBuilder
                ->setStatusSuccess();
        }catch(OAuth2ProviderNotFoundException | UnknownLeagueProviderException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}