<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Middleware\Command;

use HappyInvite\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllProvidersCommand extends AbstractOAuth2ProvidersCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $responseBuilder
            ->setJSON([
                'providers' => array_map(function(OAuth2Provider $provider) {
                    return $provider->toJSON();
                }, $this->oauth2ProviderService->getAllProviders()),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}