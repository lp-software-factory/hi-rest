<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Middleware\Command;

use HappyInvite\Domain\Bundles\OAuth2\Config\ListAvailableLeagueProviders;
use HappyInvite\Domain\Bundles\OAuth2\Service\OAuth2ProviderService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractOAuth2ProvidersCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var ListAvailableLeagueProviders */
    protected $listAvailableAdapter;

    /** @var OAuth2ProviderService */
    protected $oauth2ProviderService;

    public function __construct(
        AccessService $accessService,
        ListAvailableLeagueProviders $listAvailableAdapter,
        OAuth2ProviderService $oauth2ProviderService
    ) {
        $this->accessService = $accessService;
        $this->listAvailableAdapter = $listAvailableAdapter;
        $this->oauth2ProviderService = $oauth2ProviderService;
    }
}