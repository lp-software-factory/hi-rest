<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Tests\Paths;

use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\Fixture\OAuth2ProviderFixture;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\OAuth2ProvidersMiddlewareTest;

final class GetOAuth2ProviderMiddlewareTest extends OAuth2ProvidersMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $this->requestOAuth2ProvidersGetConfig(OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode())
            ->__invoke()
            ->expectAuthError();

        $this->requestOAuth2ProvidersGetConfig(OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestOAuth2ProvidersDelete(GenerateRandomString::generate(8))
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $this->requestOAuth2ProvidersGetConfig(OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'provider' => OAuth2ProviderFixture::$PROVIDER_GOOGLE->toJSON(),
            ])
        ;
    }
}