<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Tests\Paths;

use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\Fixture\OAuth2ProviderFixture;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\OAuth2ProvidersMiddlewareTest;

final class EditOAuth2ProviderMiddlewareTest extends OAuth2ProvidersMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $json = [
            'config' => [
                'clientId' => '* CLIENT_ID',
                'clientSecret' => '* CLIENT_SECRET',
                'redirectUri' => 'http://localhost/redirect/modified'
            ]
        ];

        $this->requestOAuth2ProvidersSetConfig(OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestOAuth2ProvidersSetConfig(OAuth2ProviderFixture::$PROVIDER_GOOGLE->getCode(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $json = [
            'config' => [
                'clientId' => '* CLIENT_ID',
                'clientSecret' => '* CLIENT_SECRET',
                'redirectUri' => 'http://localhost/redirect/modified'
            ]
        ];

        $this->requestOAuth2ProvidersSetConfig(GenerateRandomString::generate(8), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $provider = OAuth2ProviderFixture::$PROVIDER_GOOGLE;
        $json = [
            'config' => [
                'clientId' => '* CLIENT_ID',
                'clientSecret' => '* CLIENT_SECRET',
                'redirectUri' => 'http://localhost/redirect/modified'
            ]
        ];

        $this->requestOAuth2ProvidersSetConfig($provider->getCode(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'provider' => [
                    'config' => $json['config'],
                ]
            ]);
    }
}