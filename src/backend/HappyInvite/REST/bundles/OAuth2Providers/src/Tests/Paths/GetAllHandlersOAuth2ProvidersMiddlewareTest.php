<?php
namespace HappyInvite\REST\Bundles\OAuth2Providers\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\Fixture\OAuth2ProviderFixture;
use HappyInvite\REST\Bundles\OAuth2Providers\Tests\OAuth2ProvidersMiddlewareTest;
use League\OAuth2\Client\Provider\AbstractProvider;

final class GetAllHandlersOAuth2ProvidersMiddlewareTest extends OAuth2ProvidersMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $this->requestOAuth2ProvidersAllHandlers()
            ->__invoke()
            ->expectAuthError();

        $this->requestOAuth2ProvidersAllHandlers()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new OAuth2ProviderFixture());

        $handlers = $this->requestOAuth2ProvidersAllHandlers()
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
            ->fetch(function(array $json) {
                return $json['handlers'];
            });

        $this->assertTrue(count($handlers) > 0);

        foreach($handlers as $strHandler) {
            $this->assertTrue(is_string($strHandler) && strlen($strHandler) > 0);
            $this->assertTrue(is_subclass_of($strHandler, AbstractProvider::class));
        }
    }
}