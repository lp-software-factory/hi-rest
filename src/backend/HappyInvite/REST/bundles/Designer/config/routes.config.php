<?php
namespace HappyInvite\REST\Bundles\Designer;

use HappyInvite\REST\Bundles\Designer\Middleware\DesignerMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/designer/{command:all}[/]',
                'middleware' => DesignerMiddleware::class,
            ],
        ],
    ],
];