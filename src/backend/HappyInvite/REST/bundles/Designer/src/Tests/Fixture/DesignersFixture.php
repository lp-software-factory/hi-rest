<?php
namespace HappyInvite\REST\Bundles\Designer\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture\DesignerRequestsFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class DesignersFixture implements Fixture
{
    /** @var Designer */
    public static $designerFirst;

    /** @var Designer */
    public static $designerSecond;

    /** @var Designer */
    public static $designerThird;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(DesignerFormService::class); /** @var DesignerFormService $service */
        $designerService = $app->getContainer()->get(DesignerService::class); /** @var DesignerService $designerService */

        $drFirst = $service->acceptDesignerRequest(DesignerRequestsFixture::getRequest(1)->getId());
        $drSecond = $service->acceptDesignerRequest(DesignerRequestsFixture::getRequest(2)->getId());
        $drThird = $service->acceptDesignerRequest(DesignerRequestsFixture::getRequest(3)->getId());

        self::$designerFirst = $designerService->getDesignerByRequest($drFirst);
        self::$designerSecond = $designerService->getDesignerByRequest($drSecond);
        self::$designerThird = $designerService->getDesignerByRequest($drThird);
    }

    public static function getOrderedDesigners(): array
    {
        return [
            self::$designerFirst,
            self::$designerSecond,
            self::$designerThird,
        ];
    }
}