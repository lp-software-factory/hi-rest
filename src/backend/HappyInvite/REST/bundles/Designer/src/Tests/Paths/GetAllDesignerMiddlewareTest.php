<?php
namespace HappyInvite\REST\Bundles\Designer\Tests\Paths;

use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Designer\Tests\DesignerMiddlewareTestCase;
use HappyInvite\REST\Bundles\Designer\Tests\Fixture\DesignersFixture;

final class GetAllDesignerMiddlewareTest extends DesignerMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture($fixture = new DesignersFixture());

        $this->requestDesignerAll()
            ->__invoke()
            ->expectAuthError();

        $this->requestDesignerAll()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture($fixture = new DesignersFixture());

        $ids = $this->requestDesignerAll()
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ])
            ->fetch(function(array $json) {
                $this->assertTrue(isset($json['designers']));
                $this->assertTrue(is_array($json['designers']));
                $this->assertEquals(count(DesignersFixture::getOrderedDesigners()), count($json['designers']));

                return array_map(function(array $input) {
                    return $input['id'];
                }, $json['designers']);
            });
        ;

        $this->assertEquals($ids, array_map(function(Designer $designer) {
            return $designer->getId();
        }, $fixture::getOrderedDesigners()));
    }
}