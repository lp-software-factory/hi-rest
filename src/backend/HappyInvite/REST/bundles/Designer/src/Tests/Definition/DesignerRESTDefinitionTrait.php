<?php
namespace HappyInvite\REST\Bundles\Designer\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait DesignerRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestDesignerAll(): RESTRequest
    {
        return $this->request('GET', '/designer/all');
    }
}