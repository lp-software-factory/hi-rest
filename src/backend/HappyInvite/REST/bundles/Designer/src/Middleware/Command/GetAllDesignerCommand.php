<?php
namespace HappyInvite\REST\Bundles\Designer\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllDesignerCommand extends AbstractDesignerCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $designers = $this->designerService->getAllDesigners();

        $responseBuilder
            ->setJSON([
                'designers' => $this->designerFormatter->formatMany($designers),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}