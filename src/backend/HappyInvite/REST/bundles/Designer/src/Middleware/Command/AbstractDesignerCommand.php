<?php
namespace HappyInvite\REST\Bundles\Designer\Middleware\Command;

use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\Domain\Bundles\Designer\Formatter\DesignerFormatter;
use HappyInvite\REST\Command\Command;

abstract class AbstractDesignerCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var DesignerService */
    protected $designerService;

    /** @var DesignerFormatter */
    protected $designerFormatter;

    public function __construct(
        AccessService $accessService,
        DesignerService $designerService,
        DesignerFormatter $designerFormatter
    ) {
        $this->accessService = $accessService;
        $this->designerService = $designerService;
        $this->designerFormatter = $designerFormatter;
    }
}