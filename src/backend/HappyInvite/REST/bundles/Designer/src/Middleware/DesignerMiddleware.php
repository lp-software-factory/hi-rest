<?php
namespace HappyInvite\REST\Bundles\Designer\Middleware;

use HappyInvite\REST\Bundles\Designer\Middleware\Command\GetAllDesignerCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class DesignerMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('all', GetAllDesignerCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}