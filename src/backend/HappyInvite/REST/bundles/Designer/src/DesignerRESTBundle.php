<?php
namespace HappyInvite\REST\Bundles\Designer;

use HappyInvite\REST\Bundle\RESTBundle;

final class DesignerRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}