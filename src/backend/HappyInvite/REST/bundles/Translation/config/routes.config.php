<?php
namespace HappyInvite\REST\Bundles\Translation;

use HappyInvite\REST\Bundles\Translation\Middleware\TranslationMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'delete',
                'path' => '/translation/{projectId}/{command:clear}[/]',
                'middleware' => TranslationMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/translation/{projectId}/{command:get}[/]',
                'middleware' => TranslationMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/translation/{projectId}/{command:list}[/]',
                'middleware' => TranslationMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/translation/{projectId}/{command:list-region}/{region}[/]',
                'middleware' => TranslationMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/translation/{projectId}/{command:set}[/]',
                'middleware' => TranslationMiddleware::class,
            ],
        ]
    ]
];