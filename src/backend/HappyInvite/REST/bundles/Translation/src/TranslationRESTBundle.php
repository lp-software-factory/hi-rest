<?php
namespace HappyInvite\REST\Bundles\Translation;

use HappyInvite\REST\Bundle\RESTBundle;

final class TranslationRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
       return __DIR__;
    }
}