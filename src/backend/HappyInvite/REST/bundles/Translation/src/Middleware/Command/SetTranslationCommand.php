<?php
namespace HappyInvite\REST\Bundles\Translation\Middleware\Command;

use HappyInvite\Domain\Bundles\Translation\Exceptions\TranslationNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetTranslationCommand extends AbstractTranslationCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {

        $this->accessService->requireAdminAccess();

        try {
            $entity = $this->service->upsert($this->parametersFactory->factorySetTranslationParameters($request));

            $responseBuilder
                ->setJSON([
                    'translation' => $this->formatter->formatOne($entity)
                ])
                ->setStatusSuccess();
        }catch(TranslationNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}