<?php
namespace HappyInvite\REST\Bundles\Translation\Middleware\Command;

use HappyInvite\Domain\Bundles\Translation\Exceptions\TranslationNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListTranslationCommand extends AbstractTranslationCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $projectId = $request->getAttribute('projectId');

        $this->accessService->requireAdminAccess();

        try {
            $responseBuilder
                ->setJSON([
                    'translations' => $this->formatter->formatMany(
                        $this->service->listAllTranslations($projectId)
                    )
                ])
                ->setStatusSuccess();
        }catch(TranslationNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}