<?php
namespace HappyInvite\REST\Bundles\Translation\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Translation\Formatter\TranslationFormatter;
use HappyInvite\Domain\Bundles\Translation\Service\TranslationService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\Translation\ParametersFactory\TranslationParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractTranslationCommand implements Command
{
    /** @var AuthToken */
    protected $authToken;

    /** @var AccessService */
    protected $accessService;

    /** @var TranslationParametersFactory */
    protected $parametersFactory;

    /** @var TranslationFormatter */
    protected $formatter;

    /** @var TranslationService */
    protected $service;

    public function __construct(
        AuthToken $authToken,
        AccessService $accessService,
        TranslationParametersFactory $parametersFactory,
        TranslationFormatter $formatter,
        TranslationService $service
    ) {
        $this->authToken = $authToken;
        $this->accessService = $accessService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
        $this->service = $service;
    }
}