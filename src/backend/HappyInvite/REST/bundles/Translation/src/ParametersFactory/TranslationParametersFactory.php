<?php
namespace HappyInvite\REST\Bundles\Translation\ParametersFactory;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Translation\Parameters\SetTranslationParameters;
use HappyInvite\REST\Bundles\Translation\Request\SetTranslationRequest;
use Psr\Http\Message\ServerRequestInterface;

final class TranslationParametersFactory
{
    public function factorySetTranslationParameters(ServerRequestInterface $request): SetTranslationParameters
    {
        $key = $request->getQueryParams()['key'];
        $projectId = $request->getAttribute('projectId');

        $json = (new SetTranslationRequest($request))->getParameters();

        return new SetTranslationParameters($projectId, $key, new ImmutableLocalizedString($json['value']));
    }
}