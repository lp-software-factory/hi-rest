<?php
namespace HappyInvite\REST\Bundles\Translation\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait TranslationRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestTranslationList(int $projectId): RESTRequest
    {
        return $this->request('GET', sprintf('/translation/%d/list', $projectId));
    }

    protected function requestTranslationListRegion(int $projectId, string $region): RESTRequest
    {
        return $this->request('GET', sprintf('/translation/%d/list-region/%s', $projectId, $region));
    }

    protected function requestTranslationGet(int $projectId, string $key): RESTRequest
    {
        return $this->request('GET', sprintf('/translation/%d/get', $projectId), ['key' => $key]);
    }

    protected function requestTranslationSet(int $projectId, string $key, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/translation/%d/set', $projectId), ['key' => $key])
            ->setParameters($json);
    }
}