<?php
namespace HappyInvite\REST\Bundles\Translation\Tests;

return [
    'hi' => [
        'main' => [
            'foo' => [
                'x' => 'x-foo-gr',
            ]
        ]
    ]
];