<?php
namespace HappyInvite\REST\Bundles\Translation\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Translation\Tests\Fixture\TranslationFixture;
use HappyInvite\REST\Bundles\Translation\Tests\TranslationMiddlewareTestCase;

final class ListTranslationMiddlewareTest extends TranslationMiddlewareTestCase
{
    public function test200()
    {
        $this->requestTranslationList(TranslationFixture::PROJECT_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType();
    }

    public function test200HasBootstrapData()
    {
        $entities = $this->requestTranslationList(TranslationFixture::PROJECT_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_values(array_filter($json['translations'], function(array $input) {
                    return $input['key'] === 'hi.test-bootstrap.foo';
                }));
            })
        ;

        $this->assertEquals(1, count($entities));
        $this->assertEquals(null, $entities[0]['id']);
        $this->assertEquals([
            ['region' => 'en_GB', 'value' => 'GB'],
            ['region' => 'en_US', 'value' => 'US'],
        ], $entities[0]['value']);
    }

    public function test200MergeSavesId()
    {
        $entities = $this->requestTranslationList(TranslationFixture::PROJECT_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_values(array_filter($json['translations'], function(array $input) {
                    return $input['key'] === 'hi.main.foo.y';
                }));
            })
        ;

        $this->assertEquals(1, count($entities));
        $this->assertTrue(is_int($entities[0]['id']));
        $this->assertEquals(2, count($entities[0]['value']));
        $this->assertEquals([
            ['region' => 'en_US', 'value' => 'y-foo'],
            ['region' => 'ru_RU', 'value' => 'this-entity-is-attached'],
        ], $entities[0]['value']);
    }

    public function test200PHPReader()
    {
        $entities = $this->requestTranslationList(TranslationFixture::PROJECT_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_values(array_filter($json['translations'], function(array $input) {
                    return $input['key'] === 'hi.main.foo.x';
                }));
            })
        ;

        $this->assertEquals(1, count($entities));
        $this->assertTrue(is_int($entities[0]['id']));
        $this->assertEquals(3, count($entities[0]['value']));
        $this->assertEquals([
            ['region' => 'en_US', 'value' => 'x-foo'],
            ['region' => 'ru_RU', 'value' => 'x-foo-ru'],
            ['region' => 'gr_GR', 'value' => 'x-foo-gr'],
        ], $entities[0]['value']);
    }
}