<?php
namespace HappyInvite\REST\Bundles\Translation\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Translation\Tests\Fixture\TranslationFixture;
use HappyInvite\REST\Bundles\Translation\Tests\TranslationMiddlewareTestCase;

final class GetTranslationMiddlewareTest extends TranslationMiddlewareTestCase
{
    public function test200()
    {
        $this->requestTranslationGet(TranslationFixture::PROJECT_ID, $key = 'hi.main.foo.x')
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'translation' => [
                    'id' => $this->expectId(),
                    'key' => $key,
                    'value' => [
                        ['region' => 'en_US', 'value' => 'x-foo'],
                    ]
                ]
            ]);
    }
}