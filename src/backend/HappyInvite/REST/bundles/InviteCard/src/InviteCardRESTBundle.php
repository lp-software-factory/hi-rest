<?php
namespace HappyInvite\REST\Bundles\InviteCard;

use HappyInvite\REST\Bundle\RESTBundle;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\CardSizeRESTBundle;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\ColorRESTBundle;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\FamilyRESTBundle;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\InviteCardGammaRESTBundle;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\StyleRESTBundle;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\InviteCardTemplateRESTBundle;

final class InviteCardRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new ColorRESTBundle(),
            new StyleRESTBundle(),
            new CardSizeRESTBundle(),
            new InviteCardGammaRESTBundle(),
            new FamilyRESTBundle(),
            new InviteCardTemplateRESTBundle(),
        ];
    }
}