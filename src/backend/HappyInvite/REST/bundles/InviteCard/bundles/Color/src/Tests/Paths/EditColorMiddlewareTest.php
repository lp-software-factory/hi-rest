<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class EditColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new InviteCardColorFixture());

        $color = InviteCardColorFixture::getBlue();
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue Color"]],
            "hex_code" => "#1122ff"
        ];

        $this->requestInviteCardColorEdit($color->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardColorEdit($color->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue Color"]],
            "hex_code" => "#1122ff"
        ];

        $this->requestInviteCardColorEdit(self::NOT_FOUND_ID, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardColorFixture());

        $color = InviteCardColorFixture::getBlue();
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue Color"]],
            "hex_code" => "#1122ff"
        ];

        $this->requestInviteCardColorEdit($color->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'color' => [
                    'id' => $color->getId(),
                    'hex_code' => $json['hex_code'],
                    'title' => $json['title'],
                ]
            ]);

        $this->requestInviteCardColorGet($color->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'color' => [
                    'id' => $color->getId(),
                    'hex_code' => $json['hex_code'],
                    'title' => $json['title'],
                ]
            ]);
    }
}