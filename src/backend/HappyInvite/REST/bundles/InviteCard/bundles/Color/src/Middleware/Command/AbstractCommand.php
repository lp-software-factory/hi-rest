<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Service\InviteCardColorService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Formatter\InviteCardColorFormatter;
use HappyInvite\REST\Command\Command;

abstract class AbstractCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var InviteCardColorService */
    protected $colorService;

    /** @var InviteCardColorFormatter */
    protected $formatter;

    public function __construct(
        AccessService $accessService,
        InviteCardColorService $colorService,
        InviteCardColorFormatter $formatter
    ) {
        $this->accessService = $accessService;
        $this->colorService = $colorService;
        $this->formatter = $formatter;
    }
}