<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class DeleteColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new InviteCardColorFixture());

        $color = InviteCardColorFixture::getBlue();

        $this->requestInviteCardColorDelete($color->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardColorDelete($color->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardColorDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardColorFixture());

        $colorId = InviteCardColorFixture::getBlue()->getId();

        $this->requestInviteCardColorGet($colorId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestInviteCardColorDelete($colorId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);


        $this->requestInviteCardColorGet($colorId)
            ->__invoke()
            ->expectNotFoundError();
    }
}