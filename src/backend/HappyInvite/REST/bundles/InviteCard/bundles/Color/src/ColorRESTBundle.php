<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color;

use HappyInvite\REST\Bundle\RESTBundle;

final class ColorRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}