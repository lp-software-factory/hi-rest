<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Request;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\EditInviteCardColorParameters;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\ColorRESTBundle;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditColorRequest extends SchemaRequest
{
    public function getParameters(): EditInviteCardColorParameters
    {
        $data = $this->getData();

        return new EditInviteCardColorParameters(
            new ImmutableLocalizedString($data['title']),
            $data['hex_code']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_InviteCardColorBundle_Request_EditColorRequest');
    }
}