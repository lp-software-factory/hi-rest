<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\ColorMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/invite-card/color/{command:create}[/]',
                'middleware' => ColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/color/{colorId}/{command:edit}[/]',
                'middleware' => ColorMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/color/{colorId}/{command:delete}[/]',
                'middleware' => ColorMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/color/{colorId}/{command:get}[/]',
                'middleware' => ColorMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/color/{command:all}[/]',
                'middleware' => ColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/color/{colorId}/move/{command:up}[/]',
                'middleware' => ColorMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/color/{colorId}/move/{command:down}[/]',
                'middleware' => ColorMiddleware::class,
            ],
        ],
    ],
];