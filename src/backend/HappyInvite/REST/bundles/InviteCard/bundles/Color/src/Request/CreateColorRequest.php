<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Request;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\CreateInviteCardColorParameters;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\ColorRESTBundle;
use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class CreateColorRequest extends SchemaRequest
{
    public function getParameters(): CreateInviteCardColorParameters
    {
        $data = $this->getData();

        return new CreateInviteCardColorParameters(
            new ImmutableLocalizedString($data['title']),
            $data['hex_code']
        );
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_InviteCardColorBundle_Request_CreateColorRequest');
    }
}