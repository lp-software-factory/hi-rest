<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Formatter;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;

final class InviteCardColorFormatter
{
    public function formatOne(InviteCardColor $cardColor): array
    {
        return $cardColor->toJSON();
    }

    public function formatMany(array $colors): array
    {
        return array_map(function(InviteCardColor $cardColor) {
            return $this->formatOne($cardColor);
        }, $colors);
    }
}