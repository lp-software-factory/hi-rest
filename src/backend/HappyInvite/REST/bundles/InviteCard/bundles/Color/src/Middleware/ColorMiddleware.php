<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\CreateColorCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\DeleteColorCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\EditColorCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\GetAllColorCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\GetByIdColorCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\MoveDownColorCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command\MoveUpColorCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class ColorMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateColorCommand::class)
            ->attachDirect('edit', EditColorCommand::class)
            ->attachDirect('delete', DeleteColorCommand::class)
            ->attachDirect('get', GetByIdColorCommand::class)
            ->attachDirect('all', GetAllColorCommand::class)
            ->attachDirect('up', MoveUpColorCommand::class)
            ->attachDirect('down', MoveDownColorCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}