<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class CreateColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test403()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Pink color"]],
            "hex_code" => "#ff00ff"
        ];

        $this->requestInviteCardColorCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardColorCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Pink color"]],
            "hex_code" => "#ff00ff"
        ];

        $this->requestInviteCardColorCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'color' => [
                    'id' => $this->expectId(),
                    'sid' => $this->expectString(),
                    'metadata' => [
                        'version' => $this->expectString(),
                    ],
                    'title' => $json['title'],
                    'hex_code' => $json['hex_code']
                ],
            ]);
    }
}