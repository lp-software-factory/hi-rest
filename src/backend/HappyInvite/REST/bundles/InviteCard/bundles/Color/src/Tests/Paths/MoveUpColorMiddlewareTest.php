<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class MoveUpColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new InviteCardColorFixture());

        $color = InviteCardColorFixture::getBlue();

        $this->requestInviteCardColorMoveUp($color->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardColorMoveUp($color->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardColorMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardColorFixture());

        $red = InviteCardColorFixture::getRed();
        $green = InviteCardColorFixture::getGreen();
        $blue = InviteCardColorFixture::getBlue();

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardColorMoveUp($red->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardColorMoveUp($green->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $red->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardColorMoveUp($blue->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $blue->getId());
        $this->assertPosition(3, $red->getId());
    }
}