<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait InviteCardColorRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestInviteCardColorCreate(array $json): RESTRequest
    {
        return $this->request('put', '/invite-card/color/create')
            ->setParameters($json);
    }

    protected function requestInviteCardColorEdit(int $id, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/color/%d/edit', $id))
            ->setParameters($json);
    }

    protected function requestInviteCardColorGet(int $id): RESTRequest
    {
        return $this->request('get', sprintf('/invite-card/color/%d/get', $id));
    }

    protected function requestInviteCardColorGetAll(): RESTRequest
    {
        return $this->request('get', sprintf('/invite-card/color/all'));
    }

    protected function requestInviteCardColorDelete(int $id): RESTRequest
    {
        return $this->request('delete', sprintf('/invite-card/color/%d/delete', $id));
    }

    protected function requestInviteCardColorMoveUp(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/color/%d/move/up', $id));
    }

    protected function requestInviteCardColorMoveDown(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/color/%d/move/down', $id));
    }
}