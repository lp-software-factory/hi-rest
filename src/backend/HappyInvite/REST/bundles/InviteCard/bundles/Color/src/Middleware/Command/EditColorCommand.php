<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions\InviteCardColorNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Request\EditColorRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditColorCommand extends AbstractCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $colorId = $request->getAttribute('colorId');
            $parameters = (new EditColorRequest($request))->getParameters();
            $color = $this->colorService->editColor($colorId, $parameters);

            $responseBuilder
                ->setJSON([
                    'color' => $this->formatter->formatOne($color),
                ])
                ->setStatusSuccess();
        }catch(InviteCardColorNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}