<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllColorCommand extends AbstractCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $colors = $this->colorService->getAll();

        $responseBuilder
            ->setJSON([
                'colors' => $this->formatter->formatMany($colors),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}