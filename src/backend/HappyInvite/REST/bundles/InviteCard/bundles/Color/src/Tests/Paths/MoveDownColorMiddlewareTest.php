<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class MoveDownColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture(new InviteCardColorFixture());

        $color = InviteCardColorFixture::getBlue();

        $this->requestInviteCardColorMoveDown($color->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardColorMoveDown($color->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardColorMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardColorFixture());

        $red = InviteCardColorFixture::getRed();
        $green = InviteCardColorFixture::getGreen();
        $blue = InviteCardColorFixture::getBlue();

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardColorMoveDown($blue->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardColorMoveDown($red->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $red->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardColorMoveDown($red->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $blue->getId());
        $this->assertPosition(3, $red->getId());
    }
}