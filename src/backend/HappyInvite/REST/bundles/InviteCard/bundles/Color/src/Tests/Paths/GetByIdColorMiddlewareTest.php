<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class GetByIdColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test404()
    {
        $this->requestInviteCardColorGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardColorFixture());

        $color = InviteCardColorFixture::getBlue();

        $this->requestInviteCardColorGet($color->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'color' => $color->toJSON(),
            ]);
    }
}