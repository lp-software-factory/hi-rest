<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Paths;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\ColorMiddlewareTestCase;

final class GetAllColorMiddlewareTestCase extends ColorMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new InviteCardColorFixture());

        $this->requestInviteCardColorGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'colors' => function(array $input) {
                    $this->assertEquals(count(InviteCardColorFixture::getAll()), count($input));
                    $this->assertEquals(
                        Chain::create(InviteCardColorFixture::getAll())
                            ->map(function(InviteCardColor $color) {
                                return $color->getId();
                            })
                            ->sort()
                            ->array,
                        Chain::create($input)
                            ->map(function(array $color) {
                                $this->assertTrue(isset($color['id']) && is_int($color['id']));

                                return $color['id'];
                            })
                            ->sort()
                            ->array
                    );
                }
            ]);
    }
}