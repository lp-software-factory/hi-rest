<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\CreateInviteCardColorParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Service\InviteCardColorService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class InviteCardColorFixture implements Fixture
{
    private static $colorRed;
    private static $colorBlue;
    private static $colorGreen;
    private static $colorWhite;

    private $upWhite = false;

    public function __construct(bool $upWhite = false)
    {
        $this->upWhite = $upWhite;
    }

    public function up(Application $app, EntityManager $em)
    {
        /** @var InviteCardColorService $colorService */
        $colorService = $app->getContainer()->get(InviteCardColorService::class);

        self::$colorRed = $colorService->createColor(new CreateInviteCardColorParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Red']]),
            '#ff0000'
        ));

        self::$colorGreen = $colorService->createColor(new CreateInviteCardColorParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Green']]),
            '#00ff00'
        ));

        self::$colorBlue = $colorService->createColor(new CreateInviteCardColorParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Blue']]),
            '#0000ff'
        ));

        if($this->upWhite) {
            self::$colorWhite = $colorService->createColor(new CreateInviteCardColorParameters(
                new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Blue']]),
                '#ffffff'
            ));
        }else{
            self::$colorWhite = null;
        }
    }

    public static function getAll(): array
    {
        if(self::$colorWhite) {
            return [
                self::$colorRed,
                self::$colorGreen,
                self::$colorBlue,
                self::$colorWhite,
            ];
        }else{
            return [
                self::$colorRed,
                self::$colorGreen,
                self::$colorBlue,
            ];
        }
    }

    public static function getRed(): InviteCardColor
    {
        return self::$colorRed;
    }

    public static function getBlue(): InviteCardColor
    {
        return self::$colorBlue;
    }

    public static function getGreen(): InviteCardColor
    {
        return self::$colorGreen;
    }

    public static function getWhite(): InviteCardColor
    {
        return self::$colorWhite;
    }
}