<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Middleware\Command;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Request\CreateColorRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateColorCommand extends AbstractCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = (new CreateColorRequest($request))->getParameters();
        $color = $this->colorService->createColor($parameters);

        $responseBuilder
            ->setJSON([
                'color' => $this->formatter->formatOne($color),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}