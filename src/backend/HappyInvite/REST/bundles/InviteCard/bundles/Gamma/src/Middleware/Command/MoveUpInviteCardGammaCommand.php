<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Exceptions\InviteCardGammaNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class MoveUpInviteCardGammaCommand extends AbstractInviteCardGammaCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardGammaId = $request->getAttribute('inviteCardGammaId');

            $newPosition = $this->gammaService->moveUpGamma($inviteCardGammaId);

            $responseBuilder
                ->setJSON([
                    'position' => $newPosition
                ])
                ->setStatusSuccess();
        }catch(InviteCardGammaNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}