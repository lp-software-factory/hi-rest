<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions\InviteCardColorNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Request\CreateInviteCardGammaRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateInviteCardGammaCommand extends AbstractInviteCardGammaCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $gamma = $this->gammaService->createGamma(
                $this->parametersFactory->factoryCreateInviteCardParameters((new CreateInviteCardGammaRequest($request))->getParameters())
            );

            $responseBuilder
                ->setJSON([
                    'gamma' => $this->formatter->formatOne($gamma)
                ])
                ->setStatusSuccess();
        }catch(InviteCardColorNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}