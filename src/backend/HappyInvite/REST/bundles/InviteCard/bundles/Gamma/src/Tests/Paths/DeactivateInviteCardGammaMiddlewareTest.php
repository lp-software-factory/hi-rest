<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class DeactivateInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;
        $gammaId = $gamma->getId();

        $this->requestInviteCardGammaActivate($gamma->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardGammaIsActivated($gammaId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => true,
            ])
        ;

        $this->requestInviteCardGammaDeactivate($gamma->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardGammaIsActivated($gammaId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;

        $this->requestInviteCardGammaDeactivate($gamma->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardGammaDeactivate($gamma->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}