<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaActivationService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Formatter\InviteCardGammaFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\ParametersFactory\InviteCardGammaParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractInviteCardGammaCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var InviteCardGammaService */
    protected $gammaService;

    /** @var InviteCardGammaImageService */
    protected $imageService;

    /** @var InviteCardGammaActivationService */
    protected $activationService;

    /** @var InviteCardGammaParametersFactory */
    protected $parametersFactory;

    /** @var InviteCardGammaFormatter */
    protected $formatter;

    public function __construct(
        AccessService $accessService,
        InviteCardGammaService $gammaService,
        InviteCardGammaImageService $imageService,
        InviteCardGammaActivationService $activationService,
        InviteCardGammaParametersFactory $parametersFactory,
        InviteCardGammaFormatter $formatter
    ) {
        $this->accessService = $accessService;
        $this->gammaService = $gammaService;
        $this->activationService = $activationService;
        $this->imageService = $imageService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
    }
}