<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class AbstractInviteCardGammaMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
        ];
    }

    protected function assertPosition(int $position, int $gammaId)
    {
        $this->requestInviteCardGammaGetById($gammaId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'gamma' => [
                    'entity' => [
                        'id' => $gammaId,
                        'position' => $position,
                    ]
                ]
            ]);
    }
}