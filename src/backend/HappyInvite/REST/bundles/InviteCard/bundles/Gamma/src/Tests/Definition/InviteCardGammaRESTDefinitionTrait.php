<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Definition;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use Zend\Diactoros\UploadedFile;

trait InviteCardGammaRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestInviteCardGammaCreate(array $json): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/gamma/create'))
            ->setParameters($json);
    }

    protected function requestInviteCardGammaEdit(int $gammaId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/gamma/%d/edit', $gammaId))
            ->setParameters($json);
    }

    protected function requestInviteCardGammaDelete(int $gammaId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/gamma/%d/delete', $gammaId));
    }

    protected function requestInviteCardGammaMoveUp(int $gammaId): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/gamma/%d/move-up', $gammaId));
    }

    protected function requestInviteCardGammaMoveDown(int $gammaId): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/gamma/%d/move-down', $gammaId));
    }

    protected function requestInviteCardGammaGetById(int $gammaId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/gamma/%d/get', $gammaId));
    }

    protected function requestInviteCardGammaList(): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/gamma/list'));
    }

    protected function requestInviteCardGammaImageUpload(int $gammaId, UploadedFile $localFile, Point $start, Point $end): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/gamma/%d/image-upload/start/%d/%d/end/%d/%d', $gammaId, $start->getX(), $start->getY(), $end->getX(), $end->getY()))
            ->setUploadedFiles([
                'file' => $localFile,
            ]);
    }

    protected function requestInviteCardGammaImageDelete(int $gammaId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/gamma/%d/image-delete', $gammaId));
    }

    protected function requestInviteCardGammaActivate(int $gammaId): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/gamma/%d/activate', $gammaId));
    }

    protected function requestInviteCardGammaDeactivate(int $gammaId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/gamma/%d/deactivate', $gammaId));
    }

    protected function requestInviteCardGammaIsActivated(int $gammaId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/gamma/%d/is-activated', $gammaId));
    }
}