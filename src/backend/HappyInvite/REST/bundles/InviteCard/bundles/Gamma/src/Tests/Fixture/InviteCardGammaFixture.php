<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Parameters\CreateInviteCardGammaParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class InviteCardGammaFixture implements Fixture
{
    /** @var InviteCardGamma */
    public static $gammaRed;

    /** @var InviteCardGamma */
    public static $gammaBlue;

    /** @var InviteCardGamma */
    public static $gammaGreen;

    public function up(Application $app, EntityManager $em)
    {
        /** @var InviteCardGammaService $service */
        $service = $app->getContainer()->get(InviteCardGammaService::class);

        self::$gammaRed = $service->createGamma(new CreateInviteCardGammaParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Red  Gamma"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Red Gamma Description"]])
        ));

        self::$gammaBlue = $service->createGamma(new CreateInviteCardGammaParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Blue Gamma"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Blue Gamma Description"]])
        ));

        self::$gammaGreen = $service->createGamma(new CreateInviteCardGammaParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Green Gamma"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Green Gamma Description"]])
        ));
    }

    public static function getOrderedGammas(): array
    {
        return [
            self::$gammaRed,
            self::$gammaBlue,
            self::$gammaGreen,
        ];
    }
}