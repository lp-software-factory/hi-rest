<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Exceptions\InviteCardGammaNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class IsActivatedInviteCardGammaCommand extends AbstractInviteCardGammaCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $inviteCardGammaId = $request->getAttribute('inviteCardGammaId');

            $result = $this->activationService->isGammaActivated($inviteCardGammaId);

            $responseBuilder
                ->setJSON([
                    'is_activated' => $result,
                ])
                ->setStatusSuccess()
            ;
        }catch(InviteCardGammaNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}