<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class IsActivatedInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;
        $gammaId = $gamma->getId();

        $this->requestInviteCardGammaIsActivated($gammaId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ]);
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;

        $this->requestInviteCardGammaIsActivated(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}