<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\InviteCardGammaMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/invite-card/gamma/{command:create}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:edit}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:delete}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:get}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/gamma/{command:list}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:image-upload}/start/{x1}/{y1}/end/{x2}/{y2}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:image-delete}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:move-up}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:move-down}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'put',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:activate}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:deactivate}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/gamma/{inviteCardGammaId}/{command:is-activated}[/]',
                'middleware' => InviteCardGammaMiddleware::class,
            ],
        ],
    ],
];