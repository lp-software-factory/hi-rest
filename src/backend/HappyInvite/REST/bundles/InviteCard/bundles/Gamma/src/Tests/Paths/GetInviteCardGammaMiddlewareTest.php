<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class GetInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;

        $this->requestInviteCardGammaGetById($gamma->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'gamma' => [
                    'entity' => [
                        'id' => $gamma->getId(),
                    ],
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaGetById(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}