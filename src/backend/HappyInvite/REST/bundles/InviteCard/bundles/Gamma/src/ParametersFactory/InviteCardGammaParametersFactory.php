<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\ParametersFactory;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Parameters\CreateInviteCardGammaParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Parameters\EditInviteCardGammaParameters;

final class InviteCardGammaParametersFactory
{
    public function factoryCreateInviteCardParameters(array $json): CreateInviteCardGammaParameters
    {
        return new CreateInviteCardGammaParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }

    public function factoryEditInviteCardParameters( array $json): EditInviteCardGammaParameters
    {
        return new EditInviteCardGammaParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }
}