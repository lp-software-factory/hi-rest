<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;

final class CreateInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $json = [
            'gamma' => [
                "title" => [["region" => "en_GB", "value" => "My Gamma"]],
                "description" => [["region" => "en_GB", "value" => "My Gamma Description"]],
            ],
        ];

        $this->requestInviteCardGammaCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'gamma' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'date_created_at' => $this->expectDate(),
                        'last_updated_on' => $this->expectDate(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['gamma']['title'],
                        'description' => $json['gamma']['description'],
                    ],
                ],
            ]);
    }

    public function test403()
    {
        $json = [
            'gamma' => [
                "title" => [["region" => "en_GB", "value" => "My Gamma"]],
                "description" => [["region" => "en_GB", "value" => "My Gamma Description"]],
            ],
        ];

        $this->requestInviteCardGammaCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardGammaCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}