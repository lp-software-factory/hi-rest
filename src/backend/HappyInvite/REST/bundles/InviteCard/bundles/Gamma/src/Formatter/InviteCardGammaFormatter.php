<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Formatter;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;

final class InviteCardGammaFormatter
{
    public function formatOne(InviteCardGamma $inviteCardGamma): array
    {
        return [
            'entity' => $inviteCardGamma->toJSON(),
        ];
    }

    public function formatMany(array $inviteCardGammas): array
    {
        return array_map(function (InviteCardGamma $gamma) {
            return $this->formatOne($gamma);
        }, $inviteCardGammas);
    }
}