<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class ImageDeleteInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaImageDelete(InviteCardGammaFixture::$gammaRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'icon' => [
                    'is_auto_generated' => true,
                ],
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaImageDelete(InviteCardGammaFixture::$gammaRed->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardGammaImageDelete(InviteCardGammaFixture::$gammaRed->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaImageDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}