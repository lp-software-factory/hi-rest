<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class ActivateInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;
        $gammaId = $gamma->getId();

        $this->requestInviteCardGammaIsActivated($gammaId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ])
        ;

        $this->requestInviteCardGammaActivate($gamma->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardGammaIsActivated($gammaId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => true,
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;

        $this->requestInviteCardGammaActivate($gamma->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardGammaActivate($gamma->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaActivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}