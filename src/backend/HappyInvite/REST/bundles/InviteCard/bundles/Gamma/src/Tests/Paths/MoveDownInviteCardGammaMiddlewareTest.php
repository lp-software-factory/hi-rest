<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class MoveDownInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->assertPosition(1, InviteCardGammaFixture::$gammaRed->getId());
        $this->assertPosition(2, InviteCardGammaFixture::$gammaBlue->getId());
        $this->assertPosition(3, InviteCardGammaFixture::$gammaGreen->getId());

        $this->requestInviteCardGammaMoveDown(InviteCardGammaFixture::$gammaGreen->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3,
            ]);

        $this->assertPosition(1, InviteCardGammaFixture::$gammaRed->getId());
        $this->assertPosition(2, InviteCardGammaFixture::$gammaBlue->getId());
        $this->assertPosition(3, InviteCardGammaFixture::$gammaGreen->getId());

        $this->requestInviteCardGammaMoveDown(InviteCardGammaFixture::$gammaRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2,
            ]);

        $this->assertPosition(1, InviteCardGammaFixture::$gammaBlue->getId());
        $this->assertPosition(2, InviteCardGammaFixture::$gammaRed->getId());
        $this->assertPosition(3, InviteCardGammaFixture::$gammaGreen->getId());

        $this->requestInviteCardGammaMoveDown(InviteCardGammaFixture::$gammaRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3,
            ]);

        $this->assertPosition(1, InviteCardGammaFixture::$gammaBlue->getId());
        $this->assertPosition(2, InviteCardGammaFixture::$gammaGreen->getId());
        $this->assertPosition(3, InviteCardGammaFixture::$gammaRed->getId());
    }

    public function test403()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $this->requestInviteCardGammaMoveDown(InviteCardGammaFixture::$gammaGreen->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}