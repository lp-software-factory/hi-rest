<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\ActivateInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\CreateInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\DeactivateInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\DeleteInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\EditInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\GetInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\ImageDeleteInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\ImageUploadInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\IsActivatedInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\ListInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\MoveDownInviteCardGammaCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command\MoveUpInviteCardGammaCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class InviteCardGammaMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateInviteCardGammaCommand::class)
            ->attachDirect('delete', DeleteInviteCardGammaCommand::class)
            ->attachDirect('edit', EditInviteCardGammaCommand::class)
            ->attachDirect('get', GetInviteCardGammaCommand::class)
            ->attachDirect('image-delete', ImageDeleteInviteCardGammaCommand::class)
            ->attachDirect('image-upload', ImageUploadInviteCardGammaCommand::class)
            ->attachDirect('list', ListInviteCardGammaCommand::class)
            ->attachDirect('move-up', MoveUpInviteCardGammaCommand::class)
            ->attachDirect('move-down', MoveDownInviteCardGammaCommand::class)
            ->attachDirect('activate', ActivateInviteCardGammaCommand::class)
            ->attachDirect('deactivate', DeactivateInviteCardGammaCommand::class)
            ->attachDirect('is-activated', IsActivatedInviteCardGammaCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}