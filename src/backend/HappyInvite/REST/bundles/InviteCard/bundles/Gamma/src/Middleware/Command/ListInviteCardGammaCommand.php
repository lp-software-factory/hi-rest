<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListInviteCardGammaCommand extends AbstractInviteCardGammaCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $gammas = $this->gammaService->getAll();

        $responseBuilder
            ->setJSON([
                'gammas' => $this->formatter->formatMany($gammas)
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}