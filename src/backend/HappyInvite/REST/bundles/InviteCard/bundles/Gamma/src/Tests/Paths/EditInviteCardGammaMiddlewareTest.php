<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\AbstractInviteCardGammaMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;

final class EditInviteCardGammaMiddlewareTest extends AbstractInviteCardGammaMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;

        $json = [
            'gamma' => [
                "title" => [["region" => "en_GB", "value" => "* My Gamma"]],
                "description" => [["region" => "en_GB", "value" => "* My Gamma Description"]],
            ]
        ];

        $this->requestInviteCardGammaEdit($gamma->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'gamma' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'date_created_at' => $this->expectDate(),
                        'last_updated_on' => $this->expectDate(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['gamma']['title'],
                        'description' => $json['gamma']['description'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $gamma = InviteCardGammaFixture::$gammaRed;

        $json = [
            'gamma' => [
                "title" => [["region" => "en_GB", "value" => "* My Gamma"]],
                "description" => [["region" => "en_GB", "value" => "* My Gamma Description"]],
            ]
        ];

        $this->requestInviteCardGammaEdit($gamma->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardGammaEdit($gamma->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardGammaFixture());

        $json = [
            'gamma' => [
                "title" => [["region" => "en_GB", "value" => "* My Gamma"]],
                "description" => [["region" => "en_GB", "value" => "* My Gamma Description"]],
            ]
        ];

        $this->requestInviteCardGammaEdit(self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}