<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Exceptions\InviteCardGammaNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Request\EditInviteCardGammaRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditInviteCardGammaCommand extends AbstractInviteCardGammaCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardGammaId = $request->getAttribute('inviteCardGammaId');

            $gamma = $this->gammaService->editGamma($inviteCardGammaId,
                $this->parametersFactory->factoryEditInviteCardParameters((new EditInviteCardGammaRequest($request))->getParameters())
            );

            $responseBuilder
                ->setJSON([
                    'gamma' => $this->formatter->formatOne($gamma)
                ])
                ->setStatusSuccess();
        }catch(InviteCardGammaNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}