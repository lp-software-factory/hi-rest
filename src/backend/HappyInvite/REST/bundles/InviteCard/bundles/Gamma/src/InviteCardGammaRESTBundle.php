<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma;

use HappyInvite\REST\Bundle\RESTBundle;

final class InviteCardGammaRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}