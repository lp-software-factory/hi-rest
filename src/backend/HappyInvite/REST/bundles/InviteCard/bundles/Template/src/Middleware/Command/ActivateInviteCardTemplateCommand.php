<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\InviteCardTemplateNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ActivateInviteCardTemplateCommand extends AbstractInviteCardTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardTemplateId = $request->getAttribute('inviteCardTemplateId');

            $this->activationService->activateTemplate($inviteCardTemplateId);

            $responseBuilder->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException | InviteCardTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}