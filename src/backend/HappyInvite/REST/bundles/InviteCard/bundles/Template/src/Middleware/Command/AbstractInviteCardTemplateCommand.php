<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateActivationService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Formatter\InviteCardTemplateFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\ParametersFactory\InviteCardTemplateParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractInviteCardTemplateCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var InviteCardFamilyService */
    protected $familyService;

    /** @var InviteCardTemplateService */
    protected $templateService;

    /** @var InviteCardTemplateImageService */
    protected $imageService;

    /** @var InviteCardTemplateActivationService */
    protected $activationService;

    /** @var InviteCardTemplateParametersFactory */
    protected $parametersFactory;

    /** @var InviteCardTemplateFormatter */
    protected $formatter;

    public function __construct(
        AccessService $accessService,
        InviteCardFamilyService $familyService,
        InviteCardTemplateService $templateService,
        InviteCardTemplateImageService $imageService,
        InviteCardTemplateActivationService $activationService,
        InviteCardTemplateParametersFactory $parametersFactory,
        InviteCardTemplateFormatter $formatter
    ) {
        $this->accessService = $accessService;
        $this->familyService = $familyService;
        $this->templateService = $templateService;
        $this->activationService = $activationService;
        $this->imageService = $imageService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
    }
}