<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template;

use HappyInvite\REST\Bundle\RESTBundle;

final class InviteCardTemplateRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}