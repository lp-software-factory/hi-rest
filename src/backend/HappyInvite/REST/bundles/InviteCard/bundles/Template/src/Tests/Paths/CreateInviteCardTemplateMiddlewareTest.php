<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;

final class CreateInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $color = InviteCardColorFixture::getBlue();
        $familyId = InviteCardFamilyFixture::$familyBusinessCorporate->getId();

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "My Template"]],
                "description" => [["region" => "en_GB", "value" => "My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentImage->getId(),
                "config" => ['foo' => 'bar'],
                "attachment_ids" => [],
            ]
        ];

        $templateId = $this->requestInviteCardTemplateCreate($familyId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'date_created_at' => $this->expectDate(),
                        'last_updated_on' => $this->expectDate(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['template']['title'],
                        'description' => $json['template']['description'],
                        'family_id' => $familyId,
                        'color' => [
                            'title' => $color->getTitle()->toJSON(),
                            'hex_code' => $color->getHexCode(),
                        ],
                        'backdrop' => [
                            'id' => $json['template']['backdrop']
                        ],
                        'config' => $json['template']['config'],
                    ],
                ]
            ])
            ->fetch(function(array $json) {
                return $json['template']['entity']['id'];
            })
        ;

        $this->assertTrue(in_array($templateId, $this->getTemplateIdsOfFamily($familyId), true));
    }

    public function test200WithPhoto()
    {
        $color = InviteCardColorFixture::getBlue();
        $familyId = InviteCardFamilyFixture::$familyBusinessCorporate->getId();

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "My Template"]],
                "description" => [["region" => "en_GB", "value" => "My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentImage->getId(),
                "photo" => [
                    'enabled' => true,
                    'placeholder' => AttachmentFixture::$attachmentLink->getId(),
                    'container' => [
                        'start' => ['x' => 10, 'y' => 15],
                        'end' => ['x' => 25, 'y' => 50],
                    ]
                ],
                "config" => ['foo' => 'bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateCreate($familyId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'date_created_at' => $this->expectDate(),
                        'last_updated_on' => $this->expectDate(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['template']['title'],
                        'description' => $json['template']['description'],
                        'family_id' => $familyId,
                        'color' => [
                            'title' => $color->getTitle()->toJSON(),
                            'hex_code' => $color->getHexCode(),
                        ],
                        'backdrop' => [
                            'id' => $json['template']['backdrop']
                        ],
                        'config' => $json['template']['config'],
                        'photo' => [
                            'placeholder' => [
                                'id' => $json['template']['photo']['placeholder']
                            ],
                            'container' => $json['template']['photo']['container']
                        ]
                    ],
                ]
            ]);
    }

    public function test403()
    {
        $color = InviteCardColorFixture::getBlue();
        $familyId = InviteCardFamilyFixture::$familyBusinessCorporate->getId();

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "My Template"]],
                "description" => [["region" => "en_GB", "value" => "My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentImage->getId(),
                "config" => ['foo' => 'bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateCreate($familyId, $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardTemplateCreate($familyId, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $color = InviteCardColorFixture::getBlue();
        $familyId = self::NOT_FOUND_ID;

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "My Template"]],
                "description" => [["region" => "en_GB", "value" => "My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentImage->getId(),
                "config" => ['foo' => 'bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateCreate($familyId, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}