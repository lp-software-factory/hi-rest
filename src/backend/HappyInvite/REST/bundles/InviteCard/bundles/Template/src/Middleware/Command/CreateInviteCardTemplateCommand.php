<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions\InviteCardColorNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\DuplicateColorInFamilyException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Request\CreateInviteCardTemplateRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateInviteCardTemplateCommand extends AbstractInviteCardTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardFamilyId = $request->getAttribute('inviteCardFamilyId');

            $parameters = $this->parametersFactory->factoryCreateInviteCardParameters($inviteCardFamilyId, (new CreateInviteCardTemplateRequest($request))->getParameters());
            $parameters->bindToFamily();

            $template = $this->templateService->createTemplate($parameters);

            $responseBuilder
                ->setJSON([
                    'template' => $this->formatter->formatOne($template)
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException | InviteCardColorNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(DuplicateColorInFamilyException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}