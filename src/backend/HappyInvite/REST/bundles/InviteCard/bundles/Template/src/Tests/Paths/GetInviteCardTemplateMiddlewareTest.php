<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class GetInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateGetById($template->getFamily()->getId(), $template->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'template' => [
                    'entity' => [
                        'id' => $template->getId(),
                    ],
                ]
            ]);
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateGetById($template->getFamily()->getId(), self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}