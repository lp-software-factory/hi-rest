<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class ImageDeleteInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateImageDelete($familyId, InviteCardTemplateFixture::$templateRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'image' => [
                    'is_auto_generated' => true,
                ],
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateImageDelete($familyId, InviteCardTemplateFixture::$templateRed->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardTemplateImageDelete($familyId, InviteCardTemplateFixture::$templateRed->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateImageDelete($familyId, self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}