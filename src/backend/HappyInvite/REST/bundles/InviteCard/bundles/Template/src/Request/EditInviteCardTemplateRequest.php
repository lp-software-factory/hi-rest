<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditInviteCardTemplateRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData()['template'];
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_InviteCardTemplateBundle_EditInviteCardTemplateRequest');
    }
}