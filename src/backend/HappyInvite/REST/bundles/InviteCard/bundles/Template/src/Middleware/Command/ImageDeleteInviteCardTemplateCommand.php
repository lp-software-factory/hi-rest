<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\InviteCardTemplateNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ImageDeleteInviteCardTemplateCommand extends AbstractInviteCardTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardTemplateId = $request->getAttribute('inviteCardTemplateId');

            $image = $this->imageService->generateImage($inviteCardTemplateId);

            $responseBuilder
                ->setJSON([
                    'image' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(InviteCardTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}