<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\CreateInviteCardTemplateParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class InviteCardTemplateFixture implements Fixture
{
    /** @var InviteCardTemplate */
    public static $templateRed;

    /** @var InviteCardTemplate */
    public static $templateBlue;

    /** @var InviteCardTemplate */
    public static $templateGreen;

    public function up(Application $app, EntityManager $em)
    {
        /** @var InviteCardTemplateService $service */
        $service = $app->getContainer()->get(InviteCardTemplateService::class);

        $redParameters = new CreateInviteCardTemplateParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Red  Template"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Red Template Description"]]),
            InviteCardFamilyFixture::$familyBirthdayDefault,
            InviteCardColorFixture::getRed(),
            AttachmentFixture::$attachmentImage,
            [],
            ['foo' => 'bar']
        );

        $blueParameters = new CreateInviteCardTemplateParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Blue Template"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Blue Template Description"]]),
            InviteCardFamilyFixture::$familyBirthdayDefault,
            InviteCardColorFixture::getBlue(),
            AttachmentFixture::$attachmentImage,
            [],
            ['foo' => 'bar']
        );

        $greenParameters = new CreateInviteCardTemplateParameters(
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Green Template"]]),
            new ImmutableLocalizedString([["region" => "en_GB", "value" => "My Green Template Description"]]),
            InviteCardFamilyFixture::$familyBirthdayDefault,
            InviteCardColorFixture::getGreen(),
            AttachmentFixture::$attachmentImage,
            [],
            ['foo' => 'bar']
        );

        $redParameters->bindToFamily();
        $blueParameters->bindToFamily();
        $greenParameters->bindToFamily();

        self::$templateRed = $service->createTemplate($redParameters);
        self::$templateBlue = $service->createTemplate($blueParameters);
        self::$templateGreen = $service->createTemplate($greenParameters);
    }

    public static function getOrderedTemplates(): array
    {
        return [
            self::$templateRed,
            self::$templateBlue,
            self::$templateGreen,
        ];
    }
}