<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\InviteCardTemplateNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Request\InviteCardImageUploadRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ImageUploadInviteCardTemplateCommand extends AbstractInviteCardTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $inviteCardTemplateId = $request->getAttribute('inviteCardTemplateId');

            $template = $this->templateService->getById($inviteCardTemplateId);

            if($template->hasSolutionComponentOwner()) {
                $this->accessService->requireOwner($template->getSolutionComponentOwner()->getId());
            }else{
                $this->accessService->requireAdminAccess();
            }

            $image = $this->imageService->uploadImage($inviteCardTemplateId, (new InviteCardImageUploadRequest($request))->getParameters());

            $responseBuilder
                ->setJSON([
                    'image' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(InviteCardTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}