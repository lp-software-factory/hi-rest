<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class DeactivateInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $templateId = $template->getId();

        $this->requestInviteCardTemplateActivate($template->getFamily()->getId(), $template->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardTemplateIsActivated($template->getFamily()->getId(), $templateId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => true,
            ])
        ;

        $this->requestInviteCardTemplateDeactivate($template->getFamily()->getId(), $template->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardTemplateIsActivated($template->getFamily()->getId(), $templateId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateDeactivate($template->getFamily()->getId(), $template->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardTemplateDeactivate($template->getFamily()->getId(), $template->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateDeactivate($template->getFamily()->getId(), self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}