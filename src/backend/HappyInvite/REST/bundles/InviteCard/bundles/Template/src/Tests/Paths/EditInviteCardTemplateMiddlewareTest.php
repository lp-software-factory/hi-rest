<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class EditInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $color = InviteCardColorFixture::getWhite();
        $familyId = $template->getFamily()->getId();

        $this->assertTrue(in_array($template->getId(), $this->getTemplateIdsOfFamily($familyId), true));

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "* My Template"]],
                "description" => [["region" => "en_GB", "value" => "* My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentVideo->getId(),
                "config" => ['foo' => '* bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateEdit($familyId, $template->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'date_created_at' => $this->expectDate(),
                        'last_updated_on' => $this->expectDate(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['template']['title'],
                        'description' => $json['template']['description'],
                        'family_id' => $familyId,
                        'color' => [
                            'title' => $color->getTitle()->toJSON(),
                            'hex_code' => $color->getHexCode(),
                        ],
                        'backdrop' => [
                            'id' => $json['template']['backdrop']
                        ],
                        'config' => $json['template']['config'],
                    ]
                ]
            ]);

        $this->assertTrue(in_array($template->getId(), $this->getTemplateIdsOfFamily($familyId), true));
    }

    public function test200NoConflictUpdate()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $color = InviteCardColorFixture::getRed();
        $familyId = $template->getFamily()->getId();

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "* My Template"]],
                "description" => [["region" => "en_GB", "value" => "* My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentVideo->getId(),
                "config" => ['foo' => 'bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateEdit($familyId, $template->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'template' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'date_created_at' => $this->expectDate(),
                        'last_updated_on' => $this->expectDate(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['template']['title'],
                        'description' => $json['template']['description'],
                        'family_id' => $familyId,
                        'color' => [
                            'title' => $color->getTitle()->toJSON(),
                            'hex_code' => $color->getHexCode(),
                        ],
                        'backdrop' => [
                            'id' => $json['template']['backdrop']
                        ],
                        'config' => $json['template']['config'],
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $color = InviteCardColorFixture::getWhite();
        $familyId = $template->getFamily()->getId();

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "* My Template"]],
                "description" => [["region" => "en_GB", "value" => "* My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentVideo->getId(),
                "config" => ['foo' => '* bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateEdit($familyId, $template->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardTemplateEdit($familyId, $template->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $color = InviteCardColorFixture::getWhite();
        $familyId = $template->getFamily()->getId();

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "* My Template"]],
                "description" => [["region" => "en_GB", "value" => "* My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentVideo->getId(),
                "config" => ['foo' => '* bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateEdit($familyId, self::NOT_FOUND_ID, $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();

        $template = InviteCardTemplateFixture::$templateRed;
        $color = InviteCardColorFixture::getWhite();
        $familyId = self::NOT_FOUND_ID;

        $json = [
            'template' => [
                'color' => [
                    'title' => $color->getTitle()->toJSON(),
                    'hex_code' => $color->getHexCode(),
                ],
                "title" => [["region" => "en_GB", "value" => "* My Template"]],
                "description" => [["region" => "en_GB", "value" => "* My Template Description"]],
                "backdrop" => AttachmentFixture::$attachmentVideo->getId(),
                "config" => ['foo' => '* bar'],
                "attachment_ids" => [],
            ]
        ];

        $this->requestInviteCardTemplateEdit($familyId, $template->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}