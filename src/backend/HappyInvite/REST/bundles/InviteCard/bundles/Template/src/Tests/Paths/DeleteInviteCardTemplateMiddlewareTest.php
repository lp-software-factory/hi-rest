<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class DeleteInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $templateId = $template->getId();
        $familyId = $template->getFamily()->getId();

        $this->assertTrue(in_array($templateId, $this->getTemplateIdsOfFamily($familyId), true));

        $this->requestInviteCardTemplateGetById($template->getFamily()->getId(), $templateId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestInviteCardTemplateDelete($template->getFamily()->getId(), $template->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardTemplateGetById($template->getFamily()->getId(), $templateId)
            ->__invoke()
            ->expectNotFoundError();

        $templateIds = $this->requestInviteCardFamilyGet($familyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_map(function(array $template) {
                    return $template['entity']['id'];
                }, $json['family']['entity']['templates']);
            })
        ;

        $this->assertFalse(in_array($templateId, $this->getTemplateIdsOfFamily($familyId), true));
    }

    public function test403()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateDelete($template->getFamily()->getId(), $template->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardTemplateDelete($template->getFamily()->getId(), $template->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateDelete($template->getFamily()->getId(), self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}