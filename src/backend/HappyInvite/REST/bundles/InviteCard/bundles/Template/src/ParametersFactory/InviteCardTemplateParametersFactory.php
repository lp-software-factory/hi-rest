<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\ParametersFactory;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\DoctrineORM\Doctrine\Type\Point\Point;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\CreateInviteCardColorParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Service\InviteCardColorService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\CreateInviteCardTemplateParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\EditInviteCardTemplateParameters;

final class InviteCardTemplateParametersFactory
{
    /** @var InviteCardFamilyService */
    private $familyService;

    /** @var InviteCardColorService */
    private $colorService;

    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(
        InviteCardFamilyService $familyService,
        InviteCardColorService $colorService,
        AttachmentService $attachmentService
    ) {
        $this->familyService = $familyService;
        $this->colorService = $colorService;
        $this->attachmentService = $attachmentService;
    }

    public function factoryCreateInviteCardParameters(int $inviteCardFamilyId, array $json): CreateInviteCardTemplateParameters
    {
        $newColor = $this->colorService->createColor(new CreateInviteCardColorParameters(
            new ImmutableLocalizedString($json['color']['title']),
            $json['color']['hex_code']
        ));

        $parameters = new CreateInviteCardTemplateParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->familyService->getById($inviteCardFamilyId),
            $newColor,
            $this->attachmentService->getById($json['backdrop']),
            $json['attachment_ids'],
            $json['config']
        );

        if(isset($json['photo']) && isset($json['photo']['enabled']) && ($json['photo']['enabled'])) {
            $parameters->withPhoto(
                $this->attachmentService->getById($json['photo']['placeholder']),
                new Point($json['photo']['container']['start']['x'], $json['photo']['container']['start']['y']),
                new Point($json['photo']['container']['end']['x'], $json['photo']['container']['end']['y'])
            );
        }

        return $parameters;
    }

    public function factoryEditInviteCardParameters(int $inviteCardFamilyId, array $json): EditInviteCardTemplateParameters
    {
        $newColor = $this->colorService->createColor(new CreateInviteCardColorParameters(
            new ImmutableLocalizedString($json['color']['title']),
            $json['color']['hex_code']
        ));

        $parameters = new EditInviteCardTemplateParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $this->familyService->getById($inviteCardFamilyId),
            $newColor,
            $this->attachmentService->getById($json['backdrop']),
            $json['attachment_ids'],
            $json['config']
        );

        if(isset($json['photo']) && isset($json['photo']['enabled']) && ($json['photo']['enabled'])) {
            $parameters->withPhoto(
                $this->attachmentService->getById($json['photo']['placeholder']),
                new Point($json['photo']['container']['start']['x'], $json['photo']['container']['start']['y']),
                new Point($json['photo']['container']['end']['x'], $json['photo']['container']['end']['y'])
            );
        }

        return $parameters;
    }
}