<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\ActivateInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\CreateInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\DeactivateInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\DeleteInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\EditInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\GetInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\ImageDeleteInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\ImageUploadInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\IsActivatedInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\ListInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\MoveDownInviteCardTemplateCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command\MoveUpInviteCardTemplateCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class InviteCardTemplateMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateInviteCardTemplateCommand::class)
            ->attachDirect('delete', DeleteInviteCardTemplateCommand::class)
            ->attachDirect('edit', EditInviteCardTemplateCommand::class)
            ->attachDirect('get', GetInviteCardTemplateCommand::class)
            ->attachDirect('image-delete', ImageDeleteInviteCardTemplateCommand::class)
            ->attachDirect('image-upload', ImageUploadInviteCardTemplateCommand::class)
            ->attachDirect('list', ListInviteCardTemplateCommand::class)
            ->attachDirect('move-up', MoveUpInviteCardTemplateCommand::class)
            ->attachDirect('move-down', MoveDownInviteCardTemplateCommand::class)
            ->attachDirect('activate', ActivateInviteCardTemplateCommand::class)
            ->attachDirect('deactivate', DeactivateInviteCardTemplateCommand::class)
            ->attachDirect('is-activated', IsActivatedInviteCardTemplateCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}