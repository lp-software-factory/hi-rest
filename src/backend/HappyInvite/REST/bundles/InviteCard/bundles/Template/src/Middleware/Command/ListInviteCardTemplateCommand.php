<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ListInviteCardTemplateCommand extends AbstractInviteCardTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $familyId = $request->getAttribute('inviteCardFamilyId');

            $templates = $this->templateService->getAllTemplatesOfFamily(
                $this->familyService->getById($familyId)
            );

            $responseBuilder
                ->setJSON([
                    'templates' => $this->formatter->formatMany($templates)
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}