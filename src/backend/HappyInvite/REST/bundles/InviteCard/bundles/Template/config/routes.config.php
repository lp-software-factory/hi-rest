<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\InviteCardTemplateMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{command:create}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:edit}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:delete}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:get}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{command:list}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:image-upload}/start/{x1}/{y1}/end/{x2}/{y2}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:image-delete}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:move-up}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:move-down}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'put',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:activate}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:deactivate}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/family/{inviteCardFamilyId}/template/{inviteCardTemplateId}/{command:is-activated}[/]',
                'middleware' => InviteCardTemplateMiddleware::class,
            ],
        ],
    ],
];