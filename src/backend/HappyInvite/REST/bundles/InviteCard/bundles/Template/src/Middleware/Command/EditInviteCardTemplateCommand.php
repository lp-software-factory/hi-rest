<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions\InviteCardColorNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\DuplicateColorInFamilyException;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\InviteCardTemplateNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Request\EditInviteCardTemplateRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditInviteCardTemplateCommand extends AbstractInviteCardTemplateCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $inviteCardFamilyId = $request->getAttribute('inviteCardFamilyId');
            $inviteCardTemplateId = $request->getAttribute('inviteCardTemplateId');

            $template = $this->templateService->getById($inviteCardTemplateId);

            if($template->hasSolutionComponentOwner()) {
                $this->accessService->requireOwner($template->getSolutionComponentOwner()->getId());
            }else{
                $this->accessService->requireAdminAccess();
            }

            $this->templateService->editTemplate($inviteCardTemplateId,
                $this->parametersFactory->factoryEditInviteCardParameters($inviteCardFamilyId, (new EditInviteCardTemplateRequest($request))->getParameters())
            );

            $responseBuilder
                ->setJSON([
                    'template' => $this->formatter->formatOne($template)
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException | InviteCardColorNotFoundException | InviteCardTemplateNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }catch(DuplicateColorInFamilyException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusConflict();
        }

        return $responseBuilder->build();
    }
}