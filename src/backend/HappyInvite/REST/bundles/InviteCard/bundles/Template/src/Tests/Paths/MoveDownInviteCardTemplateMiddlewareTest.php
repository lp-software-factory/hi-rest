<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class MoveDownInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->assertPosition(1, $familyId, InviteCardTemplateFixture::$templateRed->getId());
        $this->assertPosition(2, $familyId, InviteCardTemplateFixture::$templateBlue->getId());
        $this->assertPosition(3, $familyId, InviteCardTemplateFixture::$templateGreen->getId());

        $this->requestInviteCardTemplateMoveDown($familyId, InviteCardTemplateFixture::$templateGreen->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);


        $this->assertPosition(1, $familyId, InviteCardTemplateFixture::$templateRed->getId());
        $this->assertPosition(2, $familyId, InviteCardTemplateFixture::$templateBlue->getId());
        $this->assertPosition(3, $familyId, InviteCardTemplateFixture::$templateGreen->getId());

        $this->requestInviteCardTemplateMoveDown($familyId, InviteCardTemplateFixture::$templateRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertPosition(1, $familyId, InviteCardTemplateFixture::$templateBlue->getId());
        $this->assertPosition(2, $familyId, InviteCardTemplateFixture::$templateRed->getId());
        $this->assertPosition(3, $familyId, InviteCardTemplateFixture::$templateGreen->getId());

        $this->requestInviteCardTemplateMoveDown($familyId, InviteCardTemplateFixture::$templateRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, $familyId, InviteCardTemplateFixture::$templateBlue->getId());
        $this->assertPosition(2, $familyId, InviteCardTemplateFixture::$templateGreen->getId());
        $this->assertPosition(3, $familyId, InviteCardTemplateFixture::$templateRed->getId());
    }

    public function test403()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateMoveDown($familyId, self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateMoveDown($familyId, InviteCardTemplateFixture::$templateGreen->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }
}