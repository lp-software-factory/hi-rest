<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;
use Zend\Diactoros\UploadedFile;

final class ImageUploadInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateImageUpload($familyId, InviteCardTemplateFixture::$templateRed->getId(), $this->getUploadedFile(), new Point(0, 0), new Point(100, 100))
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'image' => [
                    'is_auto_generated' => false,
                ],
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateImageUpload($familyId, InviteCardTemplateFixture::$templateRed->getId(), $this->getUploadedFile(), new Point(0, 0), new Point(100, 100))
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardTemplateImageDelete($familyId, InviteCardTemplateFixture::$templateRed->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $familyId = InviteCardTemplateFixture::$templateRed->getFamily()->getId();

        $this->requestInviteCardTemplateImageUpload($familyId, self::NOT_FOUND_ID, $this->getUploadedFile(), new Point(0, 0), new Point(100, 100))
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    private function getUploadedFile(): UploadedFile
    {
        $localFileName = __DIR__ . '/Resources/grid-example.png';
        $uploadFile = new UploadedFile($localFileName, filesize($localFileName), 0);

        return $uploadFile;
    }
}