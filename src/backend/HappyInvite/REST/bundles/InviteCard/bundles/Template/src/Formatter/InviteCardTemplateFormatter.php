<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Formatter;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;

final class InviteCardTemplateFormatter
{
    public const OPTION_WITH_IDS = 'with_ids';

    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function formatOne(InviteCardTemplate $inviteCardTemplate, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_WITH_IDS => false,
        ], $options);

        $json = $inviteCardTemplate->toJSON();

        if($options[self::OPTION_WITH_IDS]) {
            unset($json['card_size']);

            $json['card_size_id'] = $inviteCardTemplate->getCardSize()->getId();
        }else{

            unset($json['attachment_ids']);

            $json['attachments'] = array_map(function(int $id): array {
                return $this->attachmentService->getById($id)->toJSON();
            }, $inviteCardTemplate->getAttachmentIds());
        }

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $inviteCardTemplates, array $options = []): array
    {
        return array_map(function (InviteCardTemplate $template) use ($options) {
            return $this->formatOne($template, $options);
        }, $inviteCardTemplates);
    }
}