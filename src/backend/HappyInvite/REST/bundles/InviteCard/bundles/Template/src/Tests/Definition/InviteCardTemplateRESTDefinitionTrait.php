<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Definition;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use Zend\Diactoros\UploadedFile;

trait InviteCardTemplateRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestInviteCardTemplateCreate(int $familyId, array $json): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/family/%d/template/create', $familyId))
            ->setParameters($json);
    }

    protected function requestInviteCardTemplateEdit(int $familyId, int $templateId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/template/%d/edit', $familyId, $templateId))
            ->setParameters($json);
    }

    protected function requestInviteCardTemplateDelete(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/family/%d/template/%d/delete', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateMoveUp(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/template/%d/move-up', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateMoveDown(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/template/%d/move-down', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateGetById(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/family/%d/template/%d/get', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateList(int $familyId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/family/%d/template/list', $familyId));
    }

    protected function requestInviteCardTemplateImageUpload(int $familyId, int $templateId, UploadedFile $localFile, Point $start, Point $end): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/template/%d/image-upload/start/%d/%d/end/%d/%d', $familyId, $templateId, $start->getX(), $start->getY(), $end->getX(), $end->getY()))
            ->setUploadedFiles([
                'file' => $localFile,
            ]);
    }

    protected function requestInviteCardTemplateImageDelete(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/family/%d/template/%d/image-delete', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateActivate(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/family/%d/template/%d/activate', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateDeactivate(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/family/%d/template/%d/deactivate', $familyId, $templateId));
    }

    protected function requestInviteCardTemplateIsActivated(int $familyId, int $templateId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/family/%d/template/%d/is-activated', $familyId, $templateId));
    }
}