<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Attachment\Tests\Fixture\AttachmentFixture;
use HappyInvite\REST\Bundles\Designer\Tests\Fixture\DesignersFixture;
use HappyInvite\REST\Bundles\DesignerRequests\Tests\Fixture\DesignerRequestsFixture;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\EventTypeGroup\Tests\Fixture\EventTypeGroupsFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Tests\Fixture\InviteCardColorFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;
use Zend\Diactoros\UploadedFile;

abstract class AbstractInviteCardTemplateMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
            new AttachmentFixture(),
            new EventTypeGroupsFixture(),
            new EventTypesFixture(),
            new DesignerRequestsFixture(),
            new DesignersFixture(),
            new InviteCardColorFixture(true),
            new InviteCardSizeFixture(),
            new InviteCardStyleFixture(),
            new InviteCardGammaFixture(),
            new InviteCardFamilyFixture(),
        ];
    }

    protected function assertPosition(int $position, int $familyId, int $templateId)
    {
        $this->requestInviteCardTemplateGetById($familyId, $templateId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'template' => [
                    'entity' => [
                        'id' => $templateId,
                        'position' => $position,
                    ]
                ]
            ]);
    }

    protected function getTemplateIdsOfFamily(int $familyId): array
    {
        return $this->requestInviteCardFamilyGet($familyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->fetch(function(array $json) {
                return array_map(function(array $template) {
                    return $template['entity']['id'];
                }, $json['family']['entity']['templates']);
            })
        ;
    }
}