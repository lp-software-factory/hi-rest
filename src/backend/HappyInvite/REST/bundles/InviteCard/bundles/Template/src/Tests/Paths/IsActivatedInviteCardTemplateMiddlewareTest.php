<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\AbstractInviteCardTemplateMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Tests\Fixture\InviteCardTemplateFixture;

final class IsActivatedInviteCardTemplateMiddlewareTest extends AbstractInviteCardTemplateMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;
        $templateId = $template->getId();

        $this->requestInviteCardTemplateIsActivated($template->getFamily()->getId(), $templateId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ])
        ;
    }

    public function test404()
    {
        $this->upFixture(new InviteCardTemplateFixture());

        $template = InviteCardTemplateFixture::$templateRed;

        $this->requestInviteCardTemplateIsActivated($template->getFamily()->getId(), self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}