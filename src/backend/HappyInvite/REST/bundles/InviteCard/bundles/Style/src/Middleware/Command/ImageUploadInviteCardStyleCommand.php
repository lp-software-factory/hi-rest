<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions\InviteCardStyleNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Request\InviteCardImageUploadRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ImageUploadInviteCardStyleCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardStyleId = $request->getAttribute('inviteCardStyleId');

            $image = $this->imageService->uploadImage($inviteCardStyleId, (new InviteCardImageUploadRequest($request))->getParameters());

            $responseBuilder
                ->setJSON([
                    'icon' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(InviteCardStyleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}