<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class ActivateInviteCardStyleMiddlewareTest extends AbstractInviteCardStyleMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::$styleRed;
        $styleId = $style->getId();

        $this->requestInviteCardStyleIsActivated($styleId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ])
        ;

        $this->requestInviteCardStyleActivate($style->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardStyleIsActivated($styleId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => true,
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::$styleRed;

        $this->requestInviteCardStyleActivate($style->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardStyleActivate($style->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $this->requestInviteCardStyleActivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}