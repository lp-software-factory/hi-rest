<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Formatter;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;

final class InviteCardStyleFormatter
{
    public function formatOne(InviteCardStyle $inviteCardStyle): array
    {
        return [
            'entity' => $inviteCardStyle->toJSON(),
        ];
    }

    public function formatMany(array $inviteCardStyles): array
    {
        return array_map(function (InviteCardStyle $style) {
            return $this->formatOne($style);
        }, $inviteCardStyles);
    }
}