<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class ImageDeleteInviteCardStyleMiddlewareTest extends AbstractInviteCardStyleMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $this->requestInviteCardStyleImageDelete(InviteCardStyleFixture::$styleRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'icon' => [
                    'is_auto_generated' => true,
                ],
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $this->requestInviteCardStyleImageDelete(InviteCardStyleFixture::$styleRed->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardStyleImageDelete(InviteCardStyleFixture::$styleRed->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $this->requestInviteCardStyleImageDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}