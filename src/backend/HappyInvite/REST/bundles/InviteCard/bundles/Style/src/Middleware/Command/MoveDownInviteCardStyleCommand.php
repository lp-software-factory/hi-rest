<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions\InviteCardStyleNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class MoveDownInviteCardStyleCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $styleId = $request->getAttribute('styleId');
            $newPosition = $this->styleService->moveDownStyle($styleId);

            $responseBuilder
                ->setJSON([
                    'position' => $newPosition
                ])
                ->setStatusSuccess();
        }catch(InviteCardStyleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}