<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Definition;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use Zend\Diactoros\UploadedFile;

trait InviteCardStyleRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestInviteCardStyleCreate(array $json): RESTRequest
    {
        return $this->request('put', '/invite-card/style/create')
            ->setParameters($json);
    }

    protected function requestInviteCardStyleEdit(int $id, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/style/%d/edit', $id))
            ->setParameters($json);
    }

    protected function requestInviteCardStyleGet(int $id): RESTRequest
    {
        return $this->request('get', sprintf('/invite-card/style/%d/get', $id));
    }

    protected function requestInviteCardStyleGetAll(): RESTRequest
    {
        return $this->request('get', sprintf('/invite-card/style/all'));
    }

    protected function requestInviteCardStyleDelete(int $id): RESTRequest
    {
        return $this->request('delete', sprintf('/invite-card/style/%d/delete', $id));
    }

    protected function requestInviteCardStyleMoveUp(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/style/%d/move/up', $id));
    }

    protected function requestInviteCardStyleMoveDown(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/style/%d/move/down', $id));
    }

    protected function requestInviteCardStyleImageUpload(int $styleId, UploadedFile $localFile, Point $start, Point $end): RESTRequest
    {
        return $this->request('POST',
            sprintf('/invite-card/style/%d/image-upload/start/%d/%d/end/%d/%d', $styleId, $start->getX(),
                $start->getY(), $end->getX(), $end->getY()))
            ->setUploadedFiles([
                'file' => $localFile,
            ]);
    }

    protected function requestInviteCardStyleImageDelete(int $styleId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/style/%d/image-delete', $styleId));
    }

    protected function requestInviteCardStyleActivate(int $styleId): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/style/%d/activate', $styleId));
    }

    protected function requestInviteCardStyleDeactivate(int $styleId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/style/%d/deactivate', $styleId));
    }

    protected function requestInviteCardStyleIsActivated(int $styleId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/style/%d/is-activated', $styleId));
    }
}