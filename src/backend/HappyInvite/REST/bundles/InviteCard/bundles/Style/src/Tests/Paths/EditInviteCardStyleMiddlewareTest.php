<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class EditInviteCardStyleMiddlewareTestCase extends AbstractInviteCardStyleMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::getBlue();
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue Style"]],
            "description" => [["region" => "en_GB", "value" => "* Blue Style"]],
        ];

        $this->requestInviteCardStyleEdit($style->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardStyleEdit($style->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue Style"]],
            "description" => [["region" => "en_GB", "value" => "* Blue Style"]],
        ];

        $this->requestInviteCardStyleEdit(self::NOT_FOUND_ID, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::getBlue();
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue Style"]],
            "description" => [["region" => "en_GB", "value" => "* Blue Style"]],
        ];

        $this->requestInviteCardStyleEdit($style->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'style' => [
                    'entity' => [
                        'id' => $style->getId(),
                        'description' => $json['description'],
                        'title' => $json['title'],
                    ]
                ]
            ]);

        $this->requestInviteCardStyleGet($style->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'style' => [
                    'entity' => [
                        'id' => $style->getId(),
                        'description' => $json['description'],
                        'title' => $json['title'],
                    ]
                ]
            ]);
    }
}