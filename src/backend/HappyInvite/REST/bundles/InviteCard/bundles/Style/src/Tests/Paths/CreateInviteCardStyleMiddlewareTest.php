<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;

final class CreateInviteCardStyleMiddlewareTestCase extends AbstractInviteCardStyleMiddlewareTest
{
    public function test403()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Pink style"]],
            "description" => [["region" => "en_GB", "value" => "Pink style description"]],
        ];

        $this->requestInviteCardStyleCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardStyleCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Pink style"]],
            "description" => [["region" => "en_GB", "value" => "Pink style description"]],
        ];

        $this->requestInviteCardStyleCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'style' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['title'],
                        'description' => $json['description']
                    ]
                ],
            ]);
    }
}