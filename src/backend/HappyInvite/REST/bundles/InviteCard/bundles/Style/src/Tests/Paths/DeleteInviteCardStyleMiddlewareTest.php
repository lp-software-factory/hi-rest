<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class DeleteInviteCardStyleMiddlewareTestCase extends AbstractInviteCardStyleMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::getBlue();

        $this->requestInviteCardStyleDelete($style->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardStyleDelete($style->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardStyleDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $styleId = InviteCardStyleFixture::getBlue()->getId();

        $this->requestInviteCardStyleGet($styleId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestInviteCardStyleDelete($styleId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);


        $this->requestInviteCardStyleGet($styleId)
            ->__invoke()
            ->expectNotFoundError();
    }
}