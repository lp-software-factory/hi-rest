<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions\InviteCardStyleNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class IsActivatedInviteCardStyleCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $inviteCardStyleId = $request->getAttribute('inviteCardStyleId');

            $result = $this->activationService->isStyleActivated($inviteCardStyleId);

            $responseBuilder
                ->setJSON([
                    'is_activated' => $result,
                ])
                ->setStatusSuccess()
            ;
        }catch(InviteCardStyleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}