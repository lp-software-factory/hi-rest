<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Parameters\CreateInviteCardStyleParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class InviteCardStyleFixture implements Fixture
{
    /** @var InviteCardStyle */
    public static $styleRed;

    /** @var InviteCardStyle */
    public static $styleBlue;

    /** @var InviteCardStyle */
    public static $styleGreen;

    public function up(Application $app, EntityManager $em)
    {
        /** @var InviteCardStyleService $styleService */
        $styleService = $app->getContainer()->get(InviteCardStyleService::class);

        self::$styleRed = $styleService->createStyle(new CreateInviteCardStyleParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Style 1']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Style 1 Description']])
        ));

        self::$styleGreen = $styleService->createStyle(new CreateInviteCardStyleParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Style 2']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Style 2 Description']])
        ));

        self::$styleBlue = $styleService->createStyle(new CreateInviteCardStyleParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Style 3']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Style 3 Description']])
        ));
    }

    public static function getAll(): array
    {
        return [
            self::$styleRed,
            self::$styleGreen,
            self::$styleBlue,
        ];
    }

    public static function getRed(): InviteCardStyle
    {
        return self::$styleRed;
    }

    public static function getBlue(): InviteCardStyle
    {
        return self::$styleBlue;
    }

    public static function getGreen(): InviteCardStyle
    {
        return self::$styleGreen;
    }
}