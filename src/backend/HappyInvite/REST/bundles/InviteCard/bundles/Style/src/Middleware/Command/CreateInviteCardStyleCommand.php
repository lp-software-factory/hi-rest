<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Request\CreateStyleRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateInviteCardStyleCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->parametersFactory->factoryCreateInviteCardParameters(
            (new CreateStyleRequest($request))->getParameters()
        );

        $style = $this->styleService->createStyle($parameters);

        $responseBuilder
            ->setJSON([
                'style' => $this->formatter->formatOne($style),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}