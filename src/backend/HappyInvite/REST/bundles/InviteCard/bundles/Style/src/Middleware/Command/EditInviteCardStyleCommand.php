<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions\InviteCardStyleNotFoundException;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Request\EditStyleRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditInviteCardStyleCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $styleId = $request->getAttribute('styleId');
            $parameters = $this->parametersFactory->factoryEditInviteCardParameters(
                (new EditStyleRequest($request))->getParameters()
            );

            $style = $this->styleService->editStyle($styleId, $parameters);

            $responseBuilder
                ->setJSON([
                    'style' => $this->formatter->formatOne($style),
                ])
                ->setStatusSuccess();
        }catch(InviteCardStyleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}