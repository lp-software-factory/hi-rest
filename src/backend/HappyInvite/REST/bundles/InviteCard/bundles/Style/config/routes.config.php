<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style;

use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\InviteCardStyleMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/invite-card/style/{command:create}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/style/{styleId}/{command:edit}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/style/{styleId}/{command:delete}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/style/{styleId}/{command:get}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/style/{command:all}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/style/{styleId}/move/{command:up}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/style/{styleId}/move/{command:down}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'put',
                'path' => '/invite-card/style/{inviteCardStyleId}/{command:activate}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/style/{inviteCardStyleId}/{command:deactivate}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/style/{inviteCardStyleId}/{command:is-activated}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/style/{inviteCardStyleId}/{command:image-upload}/start/{x1}/{y1}/end/{x2}/{y2}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/style/{inviteCardStyleId}/{command:image-delete}[/]',
                'middleware' => InviteCardStyleMiddleware::class,
            ],
        ],
    ],
];