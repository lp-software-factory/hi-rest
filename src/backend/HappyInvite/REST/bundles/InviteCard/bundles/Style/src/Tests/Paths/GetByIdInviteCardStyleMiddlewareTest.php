<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;

final class GetByIdInviteCardStyleMiddlewareTestCase extends AbstractInviteCardStyleMiddlewareTest
{
    public function test404()
    {
        $this->requestInviteCardStyleGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::getBlue();

        $this->requestInviteCardStyleGet($style->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'style' => [
                    'entity' => $style->toJSON(),
                ]
            ]);
    }
}