<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style;

use HappyInvite\REST\Bundle\RESTBundle;

final class StyleRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}