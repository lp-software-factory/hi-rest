<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class MoveUpInviteCardStyleMiddlewareTestCase extends AbstractInviteCardStyleMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::getBlue();

        $this->requestInviteCardStyleMoveUp($style->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardStyleMoveUp($style->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardStyleMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $red = InviteCardStyleFixture::getRed();
        $green = InviteCardStyleFixture::getGreen();
        $blue = InviteCardStyleFixture::getBlue();

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardStyleMoveUp($red->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardStyleMoveUp($green->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $red->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardStyleMoveUp($blue->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $blue->getId());
        $this->assertPosition(3, $red->getId());
    }
}