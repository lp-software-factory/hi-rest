<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class IsActivatedInviteCardStyleMiddlewareTest extends AbstractInviteCardStyleMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::$styleRed;
        $styleId = $style->getId();

        $this->requestInviteCardStyleIsActivated($styleId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ]);
    }

    public function test404()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $style = InviteCardStyleFixture::$styleRed;

        $this->requestInviteCardStyleIsActivated(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}