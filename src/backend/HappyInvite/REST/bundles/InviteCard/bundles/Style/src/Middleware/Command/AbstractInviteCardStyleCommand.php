<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleActivationService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Formatter\InviteCardStyleFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\ParametersFactory\InviteCardStyleParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractInviteCardStyleCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var InviteCardStyleService */
    protected $styleService;

    /** @var InviteCardStyleImageService */
    protected $imageService;

    /** @var InviteCardStyleActivationService */
    protected $activationService;

    /** @var InviteCardStyleParametersFactory */
    protected $parametersFactory;

    /** @var InviteCardStyleFormatter */
    protected $formatter;

    public function __construct(
        AccessService $accessService,
        InviteCardStyleService $styleService,
        InviteCardStyleImageService $imageService,
        InviteCardStyleActivationService $activationService,
        InviteCardStyleParametersFactory $parametersFactory,
        InviteCardStyleFormatter $formatter
    ) {
        $this->accessService = $accessService;
        $this->styleService = $styleService;
        $this->activationService = $activationService;
        $this->imageService = $imageService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
    }
}