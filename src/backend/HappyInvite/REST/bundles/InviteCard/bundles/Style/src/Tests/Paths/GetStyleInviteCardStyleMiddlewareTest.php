<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Paths;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\AbstractInviteCardStyleMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;

final class GetAllInviteCardStyleMiddlewareTestCase extends AbstractInviteCardStyleMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardStyleFixture());

        $this->requestInviteCardStyleGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'styles' => function(array $input) {
                    $this->assertEquals(count(InviteCardStyleFixture::getAll()), count($input));
                    $this->assertEquals(
                        Chain::create(InviteCardStyleFixture::getAll())
                            ->map(function(InviteCardStyle $style) {
                                return $style->getId();
                            })
                            ->sort()
                            ->array,
                        Chain::create($input)
                            ->map(function(array $style) {
                                $this->assertTrue(isset($style['entity']['id']) && is_int($style['entity']['id']));

                                return $style['entity']['id'];
                            })
                            ->sort()
                            ->array
                    );
                },
            ]);
    }
}