<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\ParametersFactory;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Parameters\CreateInviteCardStyleParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Parameters\EditInviteCardStyleParameters;

final class InviteCardStyleParametersFactory
{
    public function factoryCreateInviteCardParameters(array $json): CreateInviteCardStyleParameters
    {
        return new CreateInviteCardStyleParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }

    public function factoryEditInviteCardParameters( array $json): EditInviteCardStyleParameters
    {
        return new EditInviteCardStyleParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description'])
        );
    }
}