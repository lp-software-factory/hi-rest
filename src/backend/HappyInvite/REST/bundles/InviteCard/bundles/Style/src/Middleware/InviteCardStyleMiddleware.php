<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\ActivateInviteCardStyleCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\CreateInviteCardStyleCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\DeactivateInviteCardStyleCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\DeleteInviteCardStyleCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\EditInviteCardStyleCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\GetAllInviteCardStylesCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\GetByIdInviteCardStyleCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\ImageDeleteInviteCardStyleCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\ImageUploadInviteCardStyleCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\IsActivatedInviteCardStyleCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\MoveDownInviteCardStyleCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command\MoveUpInviteCardStyleCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class InviteCardStyleMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateInviteCardStyleCommand::class)
            ->attachDirect('edit', EditInviteCardStyleCommand::class)
            ->attachDirect('delete', DeleteInviteCardStyleCommand::class)
            ->attachDirect('get', GetByIdInviteCardStyleCommand::class)
            ->attachDirect('all', GetAllInviteCardStylesCommand::class)
            ->attachDirect('up', MoveUpInviteCardStyleCommand::class)
            ->attachDirect('down', MoveDownInviteCardStyleCommand::class)
            ->attachDirect('activate', ActivateInviteCardStyleCommand::class)
            ->attachDirect('deactivate', DeactivateInviteCardStyleCommand::class)
            ->attachDirect('is-activated', IsActivatedInviteCardStyleCommand::class)
            ->attachDirect('image-delete', ImageDeleteInviteCardStyleCommand::class)
            ->attachDirect('image-upload', ImageUploadInviteCardStyleCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}