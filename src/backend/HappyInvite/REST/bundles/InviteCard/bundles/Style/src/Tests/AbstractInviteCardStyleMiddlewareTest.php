<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class AbstractInviteCardStyleMiddlewareTest extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new UserAccountFixture(),
            new AdminAccountFixture(),
        ];
    }

    protected function assertPosition(int $position, int $id)
    {
        $this->requestInviteCardStyleGet($id)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'style' => [
                    'entity' => [
                        'id' => $id,
                        'position' => $position,
                    ],
                ],
            ]);
    }
}