<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllInviteCardStylesCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $styles = $this->styleService->getAll();

        $responseBuilder
            ->setJSON([
                'styles' => $this->formatter->formatMany($styles),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}