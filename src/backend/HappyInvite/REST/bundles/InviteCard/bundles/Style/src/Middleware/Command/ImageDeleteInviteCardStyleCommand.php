<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions\InviteCardStyleNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ImageDeleteInviteCardStyleCommand extends AbstractInviteCardStyleCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardStyleId = $request->getAttribute('inviteCardStyleId');

            $image = $this->imageService->generateImage($inviteCardStyleId);

            $responseBuilder
                ->setJSON([
                    'icon' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(InviteCardStyleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}