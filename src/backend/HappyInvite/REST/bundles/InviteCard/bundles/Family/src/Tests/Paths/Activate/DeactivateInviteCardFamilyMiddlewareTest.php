<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\Activate;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;

final class DeactivateInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new InviteCardFamilyFixture());

        $family = InviteCardFamilyFixture::$familyBirthdayDefault;

        $this->requestInviteCardFamilyGet($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'family' => [
                    'entity' => [
                        'id' => $family->getId(),
                        'is_activated' => false,
                    ]
                ]
            ]);

        $this->requestInviteCardFamilyActivate($family->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'is_changed' => true,
            ]);

        $this->requestInviteCardFamilyGet($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'family' => [
                    'entity' => [
                        'id' => $family->getId(),
                        'is_activated' => true,
                    ]
                ]
            ]);

        $this->requestInviteCardFamilyDeactivate($family->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'is_changed' => true,
            ]);

        $this->requestInviteCardFamilyGet($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'family' => [
                    'entity' => [
                        'id' => $family->getId(),
                        'is_activated' => false,
                    ]
                ]
            ]);

        $this->requestInviteCardFamilyDeactivate($family->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'is_changed' => false,
            ]);

        $this->requestInviteCardFamilyGet($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'family' => [
                    'entity' => [
                        'id' => $family->getId(),
                        'is_activated' => false,
                    ]
                ]
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardFamilyFixture());

        $family = InviteCardFamilyFixture::$familyBirthdayDefault;

        $this->requestInviteCardFamilyDeactivate($family->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardFamilyDeactivate($family->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardFamilyFixture());

        $this->requestInviteCardFamilyDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}