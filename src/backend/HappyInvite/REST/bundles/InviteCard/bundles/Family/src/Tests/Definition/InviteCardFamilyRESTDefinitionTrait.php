<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait InviteCardFamilyRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestInviteCardFamilyCreate(array $json): RESTRequest
    {
        return $this->request('PUT', '/invite-card/family/create')
            ->setParameters($json);
    }

    protected function requestInviteCardFamilyEdit($familyId, array $json): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/edit', $familyId))
            ->setParameters($json);
    }

    protected function requestInviteCardFamilyDelete(int $familyId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/family/%d/delete', $familyId));
    }

    protected function requestInviteCardFamilyGet(int $familyId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/family/%d/get', $familyId));
    }

    protected function requestInviteCardFamilyAll(): RESTRequest
    {
        return $this->request('GET', '/invite-card/family/all');
    }

    protected function requestInviteCardFamilyMoveUp(int $familyId): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/move-up', $familyId));
    }

    protected function requestInviteCardFamilyMoveDown(int $familyId): RESTRequest
    {
        return $this->request('POST', sprintf('/invite-card/family/%d/move-down', $familyId));
    }

    protected function requestInviteCardFamilyActivate(int $familyId): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/family/%d/activate', $familyId));
    }

    protected function requestInviteCardFamilyDeactivate(int $familyId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/family/%d/deactivate', $familyId));
    }

    protected function requestInviteCardFamilyIsActivated(int $familyId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/family/%d/is-activated', $familyId));
    }
}