<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\CRUD;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;

final class CreateInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test403()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "My Family"]],
            "description" => [["region" => "en_GB", "value" => "My Family"]],
            'locale_id' => LocaleFixture::getLocaleEN()->getId(),
            'author_id' => UserAccountFixture::getAccount()->getCurrentProfile()->getId(),
            'event_type_id' => EventTypesFixture::getETBirthday1()->getId(),
            'style_id' => InviteCardStyleFixture::getBlue()->getId(),
            'card_size_id' => InviteCardSizeFixture::getB5()->getId(),
            'gamma_id' => InviteCardGammaFixture::$gammaGreen->getId(),
            'has_photo' => true,
        ];

        $this->requestInviteCardFamilyCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardFamilyCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "My Family"]],
            "description" => [["region" => "en_GB", "value" => "My Family"]],
            'locale_id' => LocaleFixture::getLocaleEN()->getId(),
            'author_id' => UserAccountFixture::getAccount()->getCurrentProfile()->getId(),
            'event_type_id' => EventTypesFixture::getETBirthday1()->getId(),
            'style_id' => InviteCardStyleFixture::getBlue()->getId(),
            'card_size_id' => InviteCardSizeFixture::getB5()->getId(),
            'gamma_id' => InviteCardGammaFixture::$gammaGreen->getId(),
            'has_photo' => true,
        ];

        $this->requestInviteCardFamilyCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'family' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'title' => $json['title'],
                        'description' => $json['description'],
                        'locale' => [
                            'id' => $json['locale_id'],
                        ],
                        'author' => [
                            'entity' => [
                                'id' => $json['author_id'],
                            ],
                        ],
                        'event_type' => [
                            'id' => $json['event_type_id'],
                        ],
                        'style' => [
                            'entity' => [
                                'id' => $json['style_id'],
                            ]
                        ],
                        'card_size' => [
                            'entity' => [
                                'id' => $json['card_size_id'],
                            ]
                        ],
                        'gamma' => [
                            'entity' => [
                                'id' => $json['gamma_id'],
                            ]
                        ],
                        'has_photo' => $json['has_photo'],
                    ],
                ],
            ]);
    }
}