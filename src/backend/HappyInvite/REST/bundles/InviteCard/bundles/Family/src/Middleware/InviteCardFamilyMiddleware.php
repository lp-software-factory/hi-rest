<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Activate\ActivateInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Activate\DeactivateInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Activate\IsActivatedInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD\CreateInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD\DeleteInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD\EditInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD\GetAllInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD\GetInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Move\MoveDownCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Move\MoveUpCardFamilyCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Zend\Stratigility\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class InviteCardFamilyMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateInviteCardFamilyCommand::class)
            ->attachDirect('delete', DeleteInviteCardFamilyCommand::class)
            ->attachDirect('edit', EditInviteCardFamilyCommand::class)
            ->attachDirect('all', GetAllInviteCardFamilyCommand::class)
            ->attachDirect('get', GetInviteCardFamilyCommand::class)
            ->attachDirect('move-up', MoveUpCardFamilyCommand::class)
            ->attachDirect('move-down', MoveDownCardFamilyCommand::class)
            ->attachDirect('is-activated', IsActivatedInviteCardFamilyCommand::class)
            ->attachDirect('activate', ActivateInviteCardFamilyCommand::class)
            ->attachDirect('deactivate', DeactivateInviteCardFamilyCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}