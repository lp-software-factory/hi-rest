<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\InviteCardFamilyMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/invite-card/family/{command:create}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:edit}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:delete}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:move-up}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:move-down}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:get}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/family/{command:all}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'put',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:activate}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:deactivate}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/family/{inviteCardFamilyId}/{command:is-activated}[/]',
                'middleware' => InviteCardFamilyMiddleware::class,
            ],
        ],
    ],
];