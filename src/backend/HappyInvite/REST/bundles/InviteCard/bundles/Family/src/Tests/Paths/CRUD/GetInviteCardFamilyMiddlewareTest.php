<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\CRUD;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;

final class GetInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test404()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());
        
        $this->requestInviteCardFamilyGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());

        $family = $fixture::$familyBusinessTraining;

        $this->requestInviteCardFamilyGet($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'family' => [
                    'entity' => [
                        'id' => $family->getId(),
                    ]
                ],
            ])
        ;
    }
}