<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\CRUD;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;

final class DeleteInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());

        $family = $fixture::$familyBusinessTraining;
        $this->requestInviteCardFamilyDelete($family->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardFamilyDelete($family->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());
        
        $this->requestInviteCardFamilyDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());

        $familyId = $fixture::$familyBusinessTraining->getId();

        $this->requestInviteCardFamilyGet($familyId)
            ->__invoke()
            ->expectStatusCode(200);
        
        $this->requestInviteCardFamilyDelete($familyId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardFamilyGet($familyId)
            ->__invoke()
            ->expectStatusCode(404);
    }
}