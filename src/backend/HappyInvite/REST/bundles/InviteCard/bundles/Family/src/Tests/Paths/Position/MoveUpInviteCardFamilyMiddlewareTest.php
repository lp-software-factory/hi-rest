<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\Position;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;

final class MoveUpInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test403()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());

        $family = $fixture::$familyBusinessTraining;
        $this->requestInviteCardFamilyMoveUp($family->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardFamilyMoveUp($family->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());
        
        $this->requestInviteCardFamilyMoveUp(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());

        $this->assertPosition(1, $fixture::$familyBusinessCorporate->getId());
        $this->assertPosition(2, $fixture::$familyBusinessTraining->getId());
        $this->assertPosition(3, $fixture::$familyBusinessWalkOut->getId());

        $this->requestInviteCardFamilyMoveUp($fixture::$familyBusinessCorporate->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1,
            ])
        ;

        $this->assertPosition(1, $fixture::$familyBusinessCorporate->getId());
        $this->assertPosition(2, $fixture::$familyBusinessTraining->getId());
        $this->assertPosition(3, $fixture::$familyBusinessWalkOut->getId());

        $this->requestInviteCardFamilyMoveUp($fixture::$familyBusinessTraining->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 1,
            ])
        ;

        $this->assertPosition(1, $fixture::$familyBusinessTraining->getId());
        $this->assertPosition(2, $fixture::$familyBusinessCorporate->getId());
        $this->assertPosition(3, $fixture::$familyBusinessWalkOut->getId());

        $this->requestInviteCardFamilyMoveUp($fixture::$familyBusinessWalkOut->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2,
            ])
        ;

        $this->assertPosition(1, $fixture::$familyBusinessTraining->getId());
        $this->assertPosition(2, $fixture::$familyBusinessWalkOut->getId());
        $this->assertPosition(3, $fixture::$familyBusinessCorporate->getId());
    }
}