<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD;

use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\AbstractInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Request\CreateInviteCardFamilyRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateInviteCardFamilyCommand extends AbstractInviteCardFamilyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->parametersFactory->createInviteCardFamilyParameters(
            (new CreateInviteCardFamilyRequest($request))->getParameters()
        );

        $family = $this->familyService->createFamily($parameters);

        $responseBuilder
            ->setJSON([
                'family' => $this->formatter->formatOne($family),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}