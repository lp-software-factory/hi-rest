<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\Activate;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;

final class IsActivatedInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture(new InviteCardFamilyFixture());

        $family = InviteCardFamilyFixture::$familyBirthdayDefault;

        $this->requestInviteCardFamilyActivate($family->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestInviteCardFamilyIsActivated($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'is_activated' => true,
            ]);

        $this->requestInviteCardFamilyDeactivate($family->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestInviteCardFamilyIsActivated($family->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ]);
    }

    public function test404()
    {
        $this->upFixture(new InviteCardFamilyFixture());

        $this->requestInviteCardFamilyIsActivated(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}