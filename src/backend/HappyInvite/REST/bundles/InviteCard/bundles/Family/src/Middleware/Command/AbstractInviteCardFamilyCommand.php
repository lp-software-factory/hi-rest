<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Factory\InviteCardFamilyParametersFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Formatter\InviteCardFamilyFormatter;
use HappyInvite\REST\Command\Command;

abstract class AbstractInviteCardFamilyCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var InviteCardFamilyService */
    protected $familyService;

    /** @var InviteCardFamilyParametersFactory */
    protected $parametersFactory;

    /** @var InviteCardFamilyFormatter */
    protected $formatter;

    public function __construct(
        AccessService $accessService,
        InviteCardFamilyService $inviteCardFamilyService,
        InviteCardFamilyParametersFactory $inviteCardFamilyParametersFactory,
        InviteCardFamilyFormatter $inviteCardFamilyFormatter
    ) {
        $this->accessService = $accessService;
        $this->familyService = $inviteCardFamilyService;
        $this->parametersFactory = $inviteCardFamilyParametersFactory;
        $this->formatter = $inviteCardFamilyFormatter;
    }
}