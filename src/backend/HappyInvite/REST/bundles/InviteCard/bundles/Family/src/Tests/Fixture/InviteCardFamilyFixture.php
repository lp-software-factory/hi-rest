<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Factory\InviteCardFamilyParametersFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Parameters\InviteCardFamilyParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Designer\Tests\Fixture\DesignersFixture;
use HappyInvite\REST\Bundles\EventType\Tests\Fixture\EventTypesFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Tests\Fixture\InviteCardGammaFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Tests\Fixture\InviteCardStyleFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class InviteCardFamilyFixture implements Fixture
{
    /** @var InviteCardFamilyParametersFactory */
    private $factory;

    /** @var InviteCardFamily */
    public static $familyBusinessCorporate;

    /** @var InviteCardFamily */
    public static $familyBusinessTraining;

    /** @var InviteCardFamily */
    public static $familyBusinessWalkOut;

    /** @var InviteCardFamily */
    public static $familyBirthdayDefault;

    /** @var InviteCardFamily */
    public static $familyWeddingDefault;

    public function up(Application $app, EntityManager $em)
    {
        $service = $app->getContainer()->get(InviteCardFamilyService::class); /** @var InviteCardFamilyService $service */

        $this->factory = $app->getContainer()->get(InviteCardFamilyParametersFactory::class);

        self::$familyBusinessCorporate = $service->createFamily($this->getParameters('Business Corporate', 'My Business Corporate'));
        self::$familyBusinessTraining = $service->createFamily($this->getParameters('Business Training', 'My Business Training'));
        self::$familyBusinessWalkOut = $service->createFamily($this->getParameters('Business Walk Out', 'My Business Walk Out'));

        self::$familyBirthdayDefault = $service->createFamily($this->getParameters('Birthday', 'My Birthday'));

        self::$familyWeddingDefault = $service->createFamily($this->getParameters('Wedding', 'My Wedding'));
    }

    private function getParameters(string $title, string $description): InviteCardFamilyParameters
    {
        return $this->factory->createInviteCardFamilyParameters([
            'title' => [["region" => "en_GB", "value" => $title]],
            'description' => [["region" => "en_GB", "value" => $description]],
            'locale_id' => LocaleFixture::getLocaleEN()->getId(),
            'author_id' => DesignersFixture::$designerFirst->getProfile()->getId(),
            'event_type_id' => EventTypesFixture::getETBirthday1()->getId(),
            'style_id' => InviteCardStyleFixture::getBlue()->getId(),
            'card_size_id' => InviteCardSizeFixture::getB5()->getId(),
            'gamma_id' => InviteCardGammaFixture::$gammaGreen->getId(),
            'has_photo' => true,
        ]);
    }

    public static function getOrderedBusinessFamilies(): array
    {
        return [
            self::$familyBusinessCorporate,
            self::$familyBusinessTraining,
            self::$familyBusinessWalkOut,
        ];
    }

    public static function getOrderedAll(): array
    {
        return [
            self::$familyBusinessCorporate,
            self::$familyBusinessTraining,
            self::$familyBusinessWalkOut,
            self::$familyBirthdayDefault,
            self::$familyWeddingDefault,
        ];
    }
}