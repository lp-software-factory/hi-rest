<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Activate;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\AbstractInviteCardFamilyCommand;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeactivateInviteCardFamilyCommand extends AbstractInviteCardFamilyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardFamilyId = $request->getAttribute('inviteCardFamilyId');

            $isChanged = $this->familyService->deactivateFamily($inviteCardFamilyId);

            $responseBuilder
                ->setJSON([
                    'is_changed' => $isChanged,
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}