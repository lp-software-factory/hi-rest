<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Formatter\EventTypeFormatter;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\Profile\Formatter\ProfileFormatter;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Formatter\InviteCardSizeFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Formatter\InviteCardGammaFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Formatter\InviteCardStyleFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Formatter\InviteCardTemplateFormatter;

final class InviteCardFamilyFormatter
{
    public const OPTION_USE_IDS = 'use_ids';

    /** @var EventTypeFormatter */
    private $eventTypeFormatter;

    /** @var InviteCardGammaFormatter */
    private $gammaFormatter;

    /** @var InviteCardStyleFormatter */
    private $styleFormatter;

    /** @var InviteCardSizeFormatter */
    private $cardSizeFormatter;

    /** @var ProfileFormatter */
    private $profileFormatter;

    /** @var InviteCardTemplateFormatter */
    private $templatesFormatter;

    /** @var InviteCardTemplateService */
    private $templatesService;

    public function __construct(
        EventTypeFormatter $eventTypeFormatter,
        InviteCardGammaFormatter $gammaFormatter,
        InviteCardStyleFormatter $styleFormatter,
        InviteCardSizeFormatter $cardSizeFormatter,
        ProfileFormatter $profileFormatter,
        InviteCardTemplateFormatter $templatesFormatter,
        InviteCardTemplateService $templatesService
    ) {
        $this->eventTypeFormatter = $eventTypeFormatter;
        $this->gammaFormatter = $gammaFormatter;
        $this->styleFormatter = $styleFormatter;
        $this->cardSizeFormatter = $cardSizeFormatter;
        $this->profileFormatter = $profileFormatter;
        $this->templatesFormatter = $templatesFormatter;
        $this->templatesService = $templatesService;
    }

    public function formatOne(InviteCardFamily $family, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_USE_IDS => false,
        ], $options);

        $json = [
            'entity' => $family->toJSON(),
        ];

        unset($json['entity']['locale']);
        unset($json['entity']['author']);
        unset($json['entity']['event_type']);
        unset($json['entity']['gamma']);
        unset($json['entity']['style']);
        unset($json['entity']['templates']);
        unset($json['entity']['card_size']);

        if($options[self::OPTION_USE_IDS]) {
            $json['entity']['locale_id'] = $family->getLocale()->getId();
            $json['entity']['author_profile_id'] = $family->getAuthor()->getId();
            $json['entity']['event_type_id'] = $family->getEventType()->getId();
            $json['entity']['gamma_id'] = $family->getGamma()->getId();
            $json['entity']['style_id'] = $family->getStyle()->getId();
            $json['entity']['card_size_id'] = $family->getCardSize()->getId();
        }else{
            $json['entity']['locale'] = $family->getLocale()->toJSON();
            $json['entity']['author'] = $this->profileFormatter->formatOne($family->getAuthor());
            $json['entity']['event_type'] = $this->eventTypeFormatter->formatOne($family->getEventType(), [
                EventTypeFormatter::OPTION_USE_IDS => true,
            ]);
            $json['entity']['style'] = $this->styleFormatter->formatOne($family->getStyle());
            $json['entity']['gamma'] = $this->gammaFormatter->formatOne($family->getGamma());
            $json['entity']['card_size'] = $this->cardSizeFormatter->formatOne($family->getCardSize());
            $json['entity']['templates'] = $this->templatesFormatter->formatMany(
                $this->templatesService->getByIds(new IdsCriteria($json['entity']['template_ids']))
            );
        }

        return $json;
    }

    public function formatMany(array $families, array $options = []): array
    {
        return array_map(function(InviteCardFamily $family) use ($options) {
            return $this->formatOne($family, $options);
        }, $families);
    }
}