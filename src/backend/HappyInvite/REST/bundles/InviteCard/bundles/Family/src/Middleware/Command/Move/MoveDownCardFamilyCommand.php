<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Move;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\AbstractInviteCardFamilyCommand;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class MoveDownCardFamilyCommand extends AbstractInviteCardFamilyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->accessService->requireAdminAccess();

            $newPosition = $this->familyService->moveDownFamily($request->getAttribute('inviteCardFamilyId'));

            $responseBuilder
                ->setJSON([
                    'position' => $newPosition,
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}