<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\AbstractInviteCardFamilyCommand;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllInviteCardFamilyCommand extends AbstractInviteCardFamilyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $families = $this->familyService->getAll();

            $responseBuilder
                ->setJSON([
                    'families' => $this->formatter->formatMany($families),
                    'total' => count($families),
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();

    }
}