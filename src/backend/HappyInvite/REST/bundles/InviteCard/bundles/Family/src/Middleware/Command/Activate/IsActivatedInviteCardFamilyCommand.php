<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\Activate;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\AbstractInviteCardFamilyCommand;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class IsActivatedInviteCardFamilyCommand extends AbstractInviteCardFamilyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $inviteCardFamilyId = $request->getAttribute('inviteCardFamilyId');

            $isActivated = $this->familyService->isActivated($inviteCardFamilyId);

            $responseBuilder
                ->setJSON([
                    'is_activated' => $isActivated,
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}