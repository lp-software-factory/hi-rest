<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Paths\CRUD;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\Fixture\InviteCardFamilyFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Tests\InviteCardFamilyMiddlewareTestCase;

final class GetAllInviteCardFamilyMiddlewareTest extends InviteCardFamilyMiddlewareTestCase
{
    public function test200()
    {
        $this->upFixture($fixture = new InviteCardFamilyFixture());

        $ids = $this->requestInviteCardFamilyAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
            ->fetch(function(array $json) use ($fixture) {
                $this->assertTrue(isset($json['families']));
                $this->assertTrue(count($json['families']) === count($fixture::getOrderedAll()));

                return array_map(function(array $input) {
                    return (int) $input['entity']['id'];
                }, $json['families']);
            })
        ;

        $this->assertEquals($ids, array_map(function(InviteCardFamily $family) {
            return (int) $family->getId();
        }, $fixture::getOrderedAll()));
    }
}