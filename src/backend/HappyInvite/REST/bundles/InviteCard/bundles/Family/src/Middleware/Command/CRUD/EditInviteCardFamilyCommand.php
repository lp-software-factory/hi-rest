<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\CRUD;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Middleware\Command\AbstractInviteCardFamilyCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Request\EditInviteCardFamilyRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditInviteCardFamilyCommand extends AbstractInviteCardFamilyCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->accessService->requireAdminAccess();

            $parameters = $this->parametersFactory->createInviteCardFamilyParameters(
                (new EditInviteCardFamilyRequest($request))->getParameters()
            );

            $family = $this->familyService->editFamily($request->getAttribute('inviteCardFamilyId'), $parameters);

            $responseBuilder
                ->setJSON([
                    'family' => $this->formatter->formatOne($family),
                ])
                ->setStatusSuccess();
        }catch(InviteCardFamilyNotFoundException $e){
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}