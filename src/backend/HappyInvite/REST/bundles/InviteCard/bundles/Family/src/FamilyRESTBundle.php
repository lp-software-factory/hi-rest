<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family;

use HappyInvite\REST\Bundle\RESTBundle;

final class FamilyRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}