<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Request;

use HappyInvite\Platform\Bundles\APIDocs\Schema\JSONSchema;
use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;

final class EditInviteCardFamilyRequest extends SchemaRequest
{
    public function getParameters(): array
    {
        return $this->getData();
    }

    protected function getSchema(): JSONSchema
    {
        return SchemaRequest::getSchemaService()->getDefinition('HI_InviteCardFamilyBundle_InviteCardParametersRequest');
    }
}