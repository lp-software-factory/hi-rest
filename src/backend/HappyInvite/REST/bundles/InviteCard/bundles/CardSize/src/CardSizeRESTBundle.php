<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize;

use HappyInvite\REST\Bundle\RESTBundle;

final class CardSizeRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}