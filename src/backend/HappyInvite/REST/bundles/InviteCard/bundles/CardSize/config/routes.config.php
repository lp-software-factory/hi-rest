<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize;

use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\InviteCardSizeMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'put',
                'path' => '/invite-card/card-size/{command:create}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/card-size/{cardSizeId}/{command:edit}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/card-size/{cardSizeId}/{command:delete}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/card-size/{cardSizeId}/{command:get}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/card-size/{command:all}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/card-size/{cardSizeId}/move/{command:up}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/card-size/{cardSizeId}/move/{command:down}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'put',
                'path' => '/invite-card/card-size/{inviteCardSizeId}/{command:activate}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/card-size/{inviteCardSizeId}/{command:deactivate}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/invite-card/card-size/{inviteCardSizeId}/{command:is-activated}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'post',
                'path' => '/invite-card/card-size/{inviteCardSizeId}/{command:image-upload}/start/{x1}/{y1}/end/{x2}/{y2}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
            [
                'method' => 'delete',
                'path' => '/invite-card/card-size/{inviteCardSizeId}/{command:image-delete}[/]',
                'middleware' => InviteCardSizeMiddleware::class,
            ],
        ],
    ],
];