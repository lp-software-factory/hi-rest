<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Request\CreateCardSizeRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        $parameters = $this->parametersFactory->factoryCreateInviteCardParameters(
            (new CreateCardSizeRequest($request))->getParameters()
        );
        $cardSize = $this->cardSizeService->createCardSize($parameters);

        $responseBuilder
            ->setJSON([
                'card_size' => $this->formatter->formatOne($cardSize),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}