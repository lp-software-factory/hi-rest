<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class DeleteInviteCardSizeMiddlewareTestCase extends AbstractInviteCardSizeMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $cardSize = InviteCardSizeFixture::getB5();

        $this->requestInviteCardSizeDelete($cardSize->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeDelete($cardSize->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardSizeDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $cardSizeId = InviteCardSizeFixture::getB5()->getId();

        $this->requestInviteCardSizeGet($cardSizeId)
            ->__invoke()
            ->expectStatusCode(200);

        $this->requestInviteCardSizeDelete($cardSizeId)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);


        $this->requestInviteCardSizeGet($cardSizeId)
            ->__invoke()
            ->expectNotFoundError();
    }
}