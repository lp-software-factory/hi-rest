<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InviteCardSizeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class IsActivatedInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $inviteCardSizeId = $request->getAttribute('inviteCardSizeId');

            $result = $this->activationService->isCardSizeActivated($inviteCardSizeId);

            $responseBuilder
                ->setJSON([
                    'is_activated' => $result,
                ])
                ->setStatusSuccess()
            ;
        }catch(InviteCardSizeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}