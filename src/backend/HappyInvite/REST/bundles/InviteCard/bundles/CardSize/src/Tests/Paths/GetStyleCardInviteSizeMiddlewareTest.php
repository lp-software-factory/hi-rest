<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class GetAllCardInviteSizeMiddlewareTestCase extends AbstractInviteCardSizeMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeGetAll()
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'card_sizes' => function(array $input) {
                    $this->assertEquals(count(InviteCardSizeFixture::getAll()), count($input));
                    $this->assertEquals(
                        Chain::create(InviteCardSizeFixture::getAll())
                            ->map(function(InviteCardSize $cardSize) {
                                return $cardSize->getId();
                            })
                            ->sort()
                            ->array,
                        Chain::create($input)
                            ->map(function(array $cardSize) {
                                $this->assertTrue(isset($cardSize['entity']['id']) && is_int($cardSize['entity']['id']));

                                return $cardSize['entity']['id'];
                            })
                            ->sort()
                            ->array
                    );
                }
            ]);
    }
}