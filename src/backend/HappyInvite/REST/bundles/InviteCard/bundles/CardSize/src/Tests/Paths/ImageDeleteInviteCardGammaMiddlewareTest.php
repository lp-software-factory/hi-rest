<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class ImageDeleteInviteCardSizeMiddlewareTest extends AbstractInviteCardSizeMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeImageDelete(InviteCardSizeFixture::$cardSizeRed->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'icon' => [
                    'is_auto_generated' => true,
                ],
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeImageDelete(InviteCardSizeFixture::$cardSizeRed->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeImageDelete(InviteCardSizeFixture::$cardSizeRed->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeImageDelete(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}