<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeActivationService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Formatter\InviteCardSizeFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\ParametersFactory\InviteCardSizeParametersFactory;
use HappyInvite\REST\Command\Command;

abstract class AbstractInviteCardSizeCommand implements Command
{
    /** @var AccessService */
    protected $accessService;

    /** @var InviteCardSizeService */
    protected $cardSizeService;

    /** @var InviteCardSizeImageService */
    protected $imageService;

    /** @var InviteCardSizeActivationService */
    protected $activationService;

    /** @var InviteCardSizeParametersFactory */
    protected $parametersFactory;

    /** @var InviteCardSizeFormatter */
    protected $formatter;

    public function __construct(
        AccessService $accessService,
        InviteCardSizeService $sizeService,
        InviteCardSizeImageService $imageService,
        InviteCardSizeActivationService $activationService,
        InviteCardSizeParametersFactory $parametersFactory,
        InviteCardSizeFormatter $formatter
    ) {
        $this->accessService = $accessService;
        $this->cardSizeService = $sizeService;
        $this->activationService = $activationService;
        $this->imageService = $imageService;
        $this->parametersFactory = $parametersFactory;
        $this->formatter = $formatter;
    }
}