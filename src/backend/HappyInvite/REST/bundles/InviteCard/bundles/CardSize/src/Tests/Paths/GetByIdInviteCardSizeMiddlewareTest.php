<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;

final class GetByIdInviteCardSizeMiddlewareTestCase extends AbstractInviteCardSizeMiddlewareTest
{
    public function test404()
    {
        $this->requestInviteCardSizeGet(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $cardSize = InviteCardSizeFixture::getB5();

        $this->requestInviteCardSizeGet($cardSize->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'card_size' => [
                    'entity' => [
                        'id' => $cardSize->getId(),
                    ]
                ]
            ]);
    }
}