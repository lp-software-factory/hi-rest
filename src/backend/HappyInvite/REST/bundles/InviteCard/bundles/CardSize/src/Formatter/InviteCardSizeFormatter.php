<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Formatter;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;

final class InviteCardSizeFormatter
{
    public function formatOne(InviteCardSize $inviteCardSize): array
    {
        return [
            'entity' => $inviteCardSize->toJSON(),
        ];
    }

    public function formatMany(array $inviteCardSizes): array
    {
        return array_map(function (InviteCardSize $size) {
            return $this->formatOne($size);
        }, $inviteCardSizes);
    }
}