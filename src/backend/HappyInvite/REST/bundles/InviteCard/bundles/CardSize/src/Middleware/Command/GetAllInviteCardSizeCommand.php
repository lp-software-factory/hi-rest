<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetAllInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $entities = $this->cardSizeService->getAll();

        $responseBuilder
            ->setJSON([
                'card_sizes' => $this->formatter->formatMany($entities),
            ])
            ->setStatusSuccess();

        return $responseBuilder->build();
    }
}