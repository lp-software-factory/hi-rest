<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InviteCardSizeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $cardSizeId = $request->getAttribute('cardSizeId');

            $this->cardSizeService->deleteCardSize($cardSizeId);

            $responseBuilder->setStatusSuccess();
        }catch(InviteCardSizeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}