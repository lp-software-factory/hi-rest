<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class MoveDownInviteCardSizeMiddlewareTestCase extends AbstractInviteCardSizeMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $cardSize = InviteCardSizeFixture::getB5();

        $this->requestInviteCardSizeMoveDown($cardSize->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeMoveDown($cardSize->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->requestInviteCardSizeMoveDown(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $red = InviteCardSizeFixture::getA4();
        $green = InviteCardSizeFixture::getA0();
        $blue = InviteCardSizeFixture::getB5();

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardSizeMoveDown($blue->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, $red->getId());
        $this->assertPosition(2, $green->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardSizeMoveDown($red->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 2
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $red->getId());
        $this->assertPosition(3, $blue->getId());

        $this->requestInviteCardSizeMoveDown($red->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'position' => 3
            ]);

        $this->assertPosition(1, $green->getId());
        $this->assertPosition(2, $blue->getId());
        $this->assertPosition(3, $red->getId());
    }
}