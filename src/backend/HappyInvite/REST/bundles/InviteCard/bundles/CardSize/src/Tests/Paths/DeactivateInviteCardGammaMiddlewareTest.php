<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class DeactivateInviteCardSizeMiddlewareTest extends AbstractInviteCardSizeMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $size = InviteCardSizeFixture::$cardSizeRed;
        $sizeId = $size->getId();

        $this->requestInviteCardSizeActivate($size->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardSizeIsActivated($sizeId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => true,
            ])
        ;

        $this->requestInviteCardSizeDeactivate($size->getId())
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
            ])
        ;

        $this->requestInviteCardSizeIsActivated($sizeId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ])
        ;
    }

    public function test403()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $size = InviteCardSizeFixture::$cardSizeRed;

        $this->requestInviteCardSizeDeactivate($size->getId())
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeDeactivate($size->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeDeactivate(self::NOT_FOUND_ID)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }
}