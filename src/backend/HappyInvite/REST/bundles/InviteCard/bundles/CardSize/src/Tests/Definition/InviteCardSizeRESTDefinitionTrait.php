<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Definition;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;
use Zend\Diactoros\UploadedFile;

trait InviteCardSizeRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestInviteCardSizeCreate(array $json): RESTRequest
    {
        return $this->request('put', '/invite-card/card-size/create')
            ->setParameters($json);
    }

    protected function requestInviteCardSizeEdit(int $id, array $json): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/card-size/%d/edit', $id))
            ->setParameters($json);
    }

    protected function requestInviteCardSizeGet(int $id): RESTRequest
    {
        return $this->request('get', sprintf('/invite-card/card-size/%d/get', $id));
    }

    protected function requestInviteCardSizeGetAll(): RESTRequest
    {
        return $this->request('get', sprintf('/invite-card/card-size/all'));
    }

    protected function requestInviteCardSizeDelete(int $id): RESTRequest
    {
        return $this->request('delete', sprintf('/invite-card/card-size/%d/delete', $id));
    }

    protected function requestInviteCardSizeMoveUp(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/card-size/%d/move/up', $id));
    }

    protected function requestInviteCardSizeMoveDown(int $id): RESTRequest
    {
        return $this->request('post', sprintf('/invite-card/card-size/%d/move/down', $id));
    }

    protected function requestInviteCardSizeImageUpload(int $cardSizeId, UploadedFile $localFile, Point $start, Point $end): RESTRequest
    {
        return $this->request('POST',
            sprintf('/invite-card/card-size/%d/image-upload/start/%d/%d/end/%d/%d', $cardSizeId, $start->getX(),
                $start->getY(), $end->getX(), $end->getY()))
            ->setUploadedFiles([
                'file' => $localFile,
            ]);
    }

    protected function requestInviteCardSizeImageDelete(int $cardSizeId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/card-size/%d/image-delete', $cardSizeId));
    }

    protected function requestInviteCardSizeActivate(int $cardSizeId): RESTRequest
    {
        return $this->request('PUT', sprintf('/invite-card/card-size/%d/activate', $cardSizeId));
    }

    protected function requestInviteCardSizeDeactivate(int $cardSizeId): RESTRequest
    {
        return $this->request('DELETE', sprintf('/invite-card/card-size/%d/deactivate', $cardSizeId));
    }

    protected function requestInviteCardSizeIsActivated(int $cardSizeId): RESTRequest
    {
        return $this->request('GET', sprintf('/invite-card/card-size/%d/is-activated', $cardSizeId));
    }
}