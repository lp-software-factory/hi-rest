<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;
use Zend\Diactoros\UploadedFile;

final class ImageUploadInviteCardSizeMiddlewareTest extends AbstractInviteCardSizeMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeImageUpload(InviteCardSizeFixture::$cardSizeRed->getId(), $this->getUploadedFile(), new Point(0, 0),
            new Point(100, 100))
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'icon' => [
                    'is_auto_generated' => false,
                ],
            ]);
    }

    public function test403()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeImageUpload(InviteCardSizeFixture::$cardSizeRed->getId(), $this->getUploadedFile(), new Point(0, 0),
            new Point(100, 100))
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeImageDelete(InviteCardSizeFixture::$cardSizeRed->getId())
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $this->requestInviteCardSizeImageUpload(self::NOT_FOUND_ID, $this->getUploadedFile(), new Point(0, 0), new Point(100, 100))
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectNotFoundError();
    }

    private function getUploadedFile(): UploadedFile
    {
        $localFileName = __DIR__ . '/Resources/grid-example.png';
        $uploadFile = new UploadedFile($localFileName, filesize($localFileName), 0);

        return $uploadFile;
    }
}