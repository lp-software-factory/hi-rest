<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InviteCardSizeNotFoundException;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Request\InviteCardImageUploadRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ImageUploadInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $inviteCardSizeId = $request->getAttribute('inviteCardSizeId');

            $image = $this->imageService->uploadImage($inviteCardSizeId, (new InviteCardImageUploadRequest($request))->getParameters());

            $responseBuilder
                ->setJSON([
                    'icon' => $image->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(InviteCardSizeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}