<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\ParametersFactory;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters\CreateInviteCardSizeParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters\EditInviteCardSizeParameters;

final class InviteCardSizeParametersFactory
{
    public function factoryCreateInviteCardParameters(array $json): CreateInviteCardSizeParameters
    {
        return new CreateInviteCardSizeParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $json['width'],
            $json['height'],
            $json['browser_preview']['width'],
            $json['browser_preview']['height']
        );
    }

    public function factoryEditInviteCardParameters(array $json): EditInviteCardSizeParameters
    {
        return new EditInviteCardSizeParameters(
            new ImmutableLocalizedString($json['title']),
            new ImmutableLocalizedString($json['description']),
            $json['width'],
            $json['height'],
            $json['browser_preview']['width'],
            $json['browser_preview']['height']
        );
    }
}