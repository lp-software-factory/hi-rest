<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InviteCardSizeNotFoundException;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Request\EditCardSizeRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class EditInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireAdminAccess();

        try {
            $cardSizeId = $request->getAttribute('cardSizeId');
            $parameters = $this->parametersFactory->factoryEditInviteCardParameters(
                (new EditCardSizeRequest($request))->getParameters()
            );

            $cardSize = $this->cardSizeService->editCardSize($cardSizeId, $parameters);

            $responseBuilder
                ->setJSON([
                    'card_size' => $this->formatter->formatOne($cardSize),
                ])
                ->setStatusSuccess();
        }catch(InviteCardSizeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}