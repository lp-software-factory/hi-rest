<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware;

use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\ActivateInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\CreateInviteCardSizeCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\DeactivateInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\DeleteInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\EditInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\GetAllInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\GetByIdInviteCardSizeCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\ImageDeleteInviteCardSizeCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\ImageUploadInviteCardSizeCommand;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\IsActivatedInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\MoveDownInviteCardSizeCommand;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command\MoveUpInviteCardSizeCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class InviteCardSizeMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('create', CreateInviteCardSizeCommand::class)
            ->attachDirect('edit', EditInviteCardSizeCommand::class)
            ->attachDirect('delete', DeleteInviteCardSizeCommand::class)
            ->attachDirect('get', GetByIdInviteCardSizeCommand::class)
            ->attachDirect('all', GetAllInviteCardSizeCommand::class)
            ->attachDirect('up', MoveUpInviteCardSizeCommand::class)
            ->attachDirect('down', MoveDownInviteCardSizeCommand::class)
            ->attachDirect('activate', ActivateInviteCardSizeCommand::class)
            ->attachDirect('deactivate', DeactivateInviteCardSizeCommand::class)
            ->attachDirect('is-activated', IsActivatedInviteCardSizeCommand::class)
            ->attachDirect('image-delete', ImageDeleteInviteCardSizeCommand::class)
            ->attachDirect('image-upload', ImageUploadInviteCardSizeCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}