<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class EditInviteCardSizeMiddlewareTestCase extends AbstractInviteCardSizeMiddlewareTest
{
    public function test403()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $cardSize = InviteCardSizeFixture::getB5();
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue CardSize"]],
            "description" => [["region" => "en_GB", "value" => "* Blue CardSize"]],
            "width" => 500,
            "height" => 400,
            "browser_preview" => [
                "width" => 300,
                "height" => 250,
            ],
        ];

        $this->requestInviteCardSizeEdit($cardSize->getId(), $json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeEdit($cardSize->getId(), $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue CardSize"]],
            "description" => [["region" => "en_GB", "value" => "* Blue CardSize"]],
            "width" => 500,
            "height" => 400,
            "browser_preview" => [
                "width" => 300,
                "height" => 250,
            ],
        ];

        $this->requestInviteCardSizeEdit(self::NOT_FOUND_ID, $json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $cardSize = InviteCardSizeFixture::getB5();
        $json = [
            "title" => [["region" => "en_GB", "value" => "* Blue CardSize"]],
            "description" => [["region" => "en_GB", "value" => "* Blue CardSize"]],
            "width" => 500,
            "height" => 400,
            "browser_preview" => [
                "width" => 300,
                "height" => 250,
            ],
        ];

        $this->requestInviteCardSizeEdit($cardSize->getId(), $json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'card_size' => [
                    'entity' => [
                        'id' => $cardSize->getId(),
                        'description' => $json['description'],
                        'title' => $json['title'],
                        "width" => $json['width'],
                        "height" => $json['height'],
                        "browser_preview" => [
                            "width" => $json["browser_preview"]['width'],
                            "height" => $json["browser_preview"]['height'],
                        ]
                    ]
                ]
            ]);

        $this->requestInviteCardSizeGet($cardSize->getId())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'card_size' => [
                    'entity' => [
                        'id' => $cardSize->getId(),
                        'description' => $json['description'],
                        'title' => $json['title'],
                        "width" => $json['width'],
                        "height" => $json['height'],
                        "browser_preview" => [
                            "width" => $json["browser_preview"]['width'],
                            "height" => $json["browser_preview"]['height'],
                        ]
                    ]
                ]
            ]);
    }
}