<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters\CreateInviteCardSizeParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use Zend\Expressive\Application;

final class InviteCardSizeFixture implements Fixture
{
    /** @var InviteCardSize */
    public static $cardSizeRed;

    /** @var InviteCardSize */
    public static $cardSizeBlue;

    /** @var InviteCardSize */
    public static $cardSizeGreen;

    public function up(Application $app, EntityManager $em)
    {
        /** @var InviteCardSizeService $cardSizeService */
        $cardSizeService = $app->getContainer()->get(InviteCardSizeService::class);

        self::$cardSizeRed = $cardSizeService->createCardSize(new CreateInviteCardSizeParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Form 1']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Form 1 Description']]),
            500,
            400,
            250,
            200
        ));

        self::$cardSizeGreen = $cardSizeService->createCardSize(new CreateInviteCardSizeParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Form 2']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Form 2 Description']]),
            900,
            120,
            300,
            60
        ));

        self::$cardSizeBlue = $cardSizeService->createCardSize(new CreateInviteCardSizeParameters(
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Form 3']]),
            new ImmutableLocalizedString([['region' => 'en_GB', 'value' => 'Form 3 Description']]),
            1920,
            1080,
            300,
            169
        ));
    }

    public static function getAll(): array
    {
        return [
            self::$cardSizeRed,
            self::$cardSizeGreen,
            self::$cardSizeBlue,
        ];
    }

    public static function getA4(): InviteCardSize
    {
        return self::$cardSizeRed;
    }

    public static function getB5(): InviteCardSize
    {
        return self::$cardSizeBlue;
    }

    public static function getA0(): InviteCardSize
    {
        return self::$cardSizeGreen;
    }
}