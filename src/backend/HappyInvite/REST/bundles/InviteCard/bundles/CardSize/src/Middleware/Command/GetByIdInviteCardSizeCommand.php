<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Middleware\Command;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InviteCardSizeNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetByIdInviteCardSizeCommand extends AbstractInviteCardSizeCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $cardSizeId = $request->getAttribute('cardSizeId');
            $cardSize = $this->cardSizeService->getById($cardSizeId);

            $responseBuilder
                ->setJSON([
                    'card_size' => $this->formatter->formatOne($cardSize),
                ])
                ->setStatusSuccess();
        }catch(InviteCardSizeNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}