<?php
namespace  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use  HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;

final class CreateInviteCardSizeMiddlewareTestCase extends AbstractInviteCardSizeMiddlewareTest
{
    public function test403()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Pink cardSize"]],
            "description" => [["region" => "en_GB", "value" => "Pink cardSize description"]],
            "width" => 500,
            "height" => 400,
            "browser_preview" => [
                "width" => 300,
                "height" => 250,
            ],
        ];

        $this->requestInviteCardSizeCreate($json)
            ->__invoke()
            ->expectAuthError();

        $this->requestInviteCardSizeCreate($json)
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectAuthError();
    }

    public function test200()
    {
        $json = [
            "title" => [["region" => "en_GB", "value" => "Pink cardSize"]],
            "description" => [["region" => "en_GB", "value" => "Pink cardSize description"]],
            "width" => 500,
            "height" => 400,
            "browser_preview" => [
                "width" => 300,
                "height" => 250,
            ],
        ];

        $this->requestInviteCardSizeCreate($json)
            ->auth(AdminAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'card_size' => [
                    'entity' => [
                        'id' => $this->expectId(),
                        'sid' => $this->expectString(),
                        'metadata' => [
                            'version' => $this->expectString(),
                        ],
                        'title' => $json['title'],
                        'description' => $json['description'],
                        "width" => $json['width'],
                        "height" => $json['height'],
                        "browser_preview" => [
                            "width" => $json["browser_preview"]['width'],
                            "height" => $json["browser_preview"]['height'],
                        ]
                    ]
                ],
            ]);
    }
}