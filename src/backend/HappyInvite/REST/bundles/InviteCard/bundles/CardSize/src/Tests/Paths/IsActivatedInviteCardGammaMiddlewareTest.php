<?php
namespace HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Paths;

use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\AbstractInviteCardSizeMiddlewareTest;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Tests\Fixture\InviteCardSizeFixture;

final class IsActivatedInviteCardSizeMiddlewareTest extends AbstractInviteCardSizeMiddlewareTest
{
    public function test200()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $size = InviteCardSizeFixture::$cardSizeRed;
        $sizeId = $size->getId();

        $this->requestInviteCardSizeIsActivated($sizeId)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONBody([
                'success' => true,
                'is_activated' => false,
            ]);
    }

    public function test404()
    {
        $this->upFixture(new InviteCardSizeFixture());

        $size = InviteCardSizeFixture::$cardSizeRed;

        $this->requestInviteCardSizeIsActivated(self::NOT_FOUND_ID)
            ->__invoke()
            ->expectNotFoundError();
    }
}