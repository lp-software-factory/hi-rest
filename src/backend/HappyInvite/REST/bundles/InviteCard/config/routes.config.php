<?php
namespace HappyInvite\REST\Bundles\InviteCard;

use HappyInvite\REST\Bundles\InviteCard\Middleware\InviteCardMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/invite-card/{command:repository}/locale/{region}[/]',
                'middleware' => InviteCardMiddleware::class,
            ],
        ],
    ],
];