<?php
namespace HappyInvite\REST\Bundles\Account;

use HappyInvite\REST\Bundle\RESTBundle;

final class AccountRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}