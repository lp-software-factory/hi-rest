<?php
namespace HappyInvite\REST\Bundles\Account\Middleware\Command;

use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Account\Service\ResetPasswordService;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Auth\Service\AuthService;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use HappyInvite\REST\Command\Command;

abstract class AbstractAccountCommand implements Command
{
    /** @var AuthService */
    protected $authService;

    /** @var AccessService */
    protected $accessService;

    /** @var AccountService */
    protected $accountService;

    /** @var AuthToken */
    protected $authToken;

    /** @var ResetPasswordService */
    protected $resetPasswordService;

    public final function __construct(
        AuthService $authService,
        AccessService $accessService,
        AccountService $accountService,
        AuthToken $authToken,
        ResetPasswordService $resetPasswordService
    ) {
        $this->authService = $authService;
        $this->accessService = $accessService;
        $this->accountService = $accountService;
        $this->authToken = $authToken;
        $this->resetPasswordService = $resetPasswordService;
    }

}