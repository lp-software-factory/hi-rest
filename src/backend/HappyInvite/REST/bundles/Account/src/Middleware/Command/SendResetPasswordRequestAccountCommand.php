<?php
namespace HappyInvite\REST\Bundles\Account\Middleware\Command;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SendResetPasswordRequestAccountCommand extends AbstractAccountCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $this->resetPasswordService->sendResetPasswordRequest($request->getQueryParams()['email']);

            $responseBuilder
                ->setStatusSuccess();
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setStatusNotFound()
                ->setError($e);
        }

        return $responseBuilder->build();
    }
}