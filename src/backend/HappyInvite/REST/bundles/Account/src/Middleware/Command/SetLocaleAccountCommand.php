<?php
namespace HappyInvite\REST\Bundles\Account\Middleware\Command;

use HappyInvite\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class SetLocaleAccountCommand extends AbstractAccountCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        $this->accessService->requireProtectedAccess();

        try {
            $region = $request->getAttribute('region');
            $locale = $this->accountService->setLocale($this->authToken->getAccount(), $region);

            $responseBuilder
                ->setJSON([
                    'locale' => $locale->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(LocaleNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}