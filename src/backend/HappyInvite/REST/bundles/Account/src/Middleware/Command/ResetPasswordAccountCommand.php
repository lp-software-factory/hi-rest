<?php
namespace HappyInvite\REST\Bundles\Account\Middleware\Command;

use HappyInvite\Domain\Bundles\Auth\Exceptions\ResetPasswordRequestNotFoundException;
use HappyInvite\REST\Bundles\Account\Request\ResetPasswordMiddlewareRequest;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ResetPasswordAccountCommand extends AbstractAccountCommand
{
    public function __invoke(ServerRequestInterface $request, ResponseBuilder $responseBuilder): ResponseInterface
    {
        try {
            $parameters = (new ResetPasswordMiddlewareRequest($request))->getParameters();

            $account = $this->resetPasswordService->resetPassword($parameters['accept_token'], $parameters['new_password']);
            $authToken = $this->authService->auth($account);

            $responseBuilder
                ->setJSON([
                    'token' => $authToken->toJSON(),
                ])
                ->setStatusSuccess();
        }catch(ResetPasswordRequestNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}