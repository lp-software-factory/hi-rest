<?php
namespace HappyInvite\REST\Bundles\Account\Middleware;

use HappyInvite\REST\Bundles\Account\Middleware\Command\ResetPasswordAccountCommand;
use HappyInvite\REST\Bundles\Account\Middleware\Command\SendResetPasswordRequestAccountCommand;
use HappyInvite\REST\Bundles\Account\Middleware\Command\SetLocaleAccountCommand;
use HappyInvite\REST\Command\Service\CommandService;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class AccountMiddleware implements MiddlewareInterface
{
    /** @var CommandService */
    private $commandService;

    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $resolver = $this->commandService->createResolverBuilder()
            ->attachDirect('reset-password', ResetPasswordAccountCommand::class)
            ->attachDirect('request-reset-password', SendResetPasswordRequestAccountCommand::class)
            ->attachDirect('set-locale', SetLocaleAccountCommand::class)
            ->resolve($request);

        return $resolver->__invoke($request, $responseBuilder);
    }
}