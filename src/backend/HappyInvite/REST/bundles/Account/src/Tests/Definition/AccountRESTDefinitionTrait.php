<?php
namespace HappyInvite\REST\Bundles\Account\Tests\Definition;

use HappyInvite\REST\Bundles\PHPUnit\RESTRequest\RESTRequest;

trait AccountRESTDefinitionTrait
{
    abstract function request(string $method, string $uri, array $queryParams = null): RESTRequest;

    protected function requestAccountSendResetPasswordRequest(string $email): RESTRequest
    {
        return $this->request('put', '/account/request-reset-password', ['email' => $email]);
    }

    protected function requestAccountResetPassword(array $json): RESTRequest
    {
        return $this->request('post', '/account/reset-password')
            ->setParameters($json);
    }

    protected function requestAccountSetLocale(string $region): RESTRequest
    {
        return $this->request('post', sprintf('/account/set-locale/%s/', $region));
    }

    protected function requestAccountSignIn(string $email, string $password): RESTRequest
    {
        return $this->request('post', '/auth/sign-in')
            ->setParameters([
                'email' => $email,
                'password' => $password,
            ]);
    }
}