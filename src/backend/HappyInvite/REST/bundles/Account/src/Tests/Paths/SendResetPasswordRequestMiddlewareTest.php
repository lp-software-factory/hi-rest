<?php
namespace HappyInvite\REST\Bundles\Account\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\AccountMiddlewareTestCase;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class SendResetPasswordRequestMiddlewareTest extends AccountMiddlewareTestCase
{
    public function test404()
    {
        $this->requestAccountSendResetPasswordRequest('not-exists@example.com')
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $account = UserAccountFixture::getAccount();

        $this->requestAccountSendResetPasswordRequest($account->getEmail())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);
    }
}