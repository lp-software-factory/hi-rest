<?php
namespace HappyInvite\REST\Bundles\Account\Tests\Paths;

use HappyInvite\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\REST\Bundles\Account\Tests\AccountMiddlewareTestCase;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class ResetPasswordMiddlewareTest extends AccountMiddlewareTestCase
{
    public function test404()
    {
        $json = [
            'accept_token' => GenerateRandomString::generate(ResetPasswordRequest::RESET_PASSWORD_TOKEN_LENGTH),
            'new_password' => '1234'
        ];

        $this->requestAccountResetPassword($json)
            ->__invoke()
            ->expectNotFoundError();
    }

    public function test200()
    {
        $account = UserAccountFixture::getAccount();

        $oldPassword = UserAccountFixture::$PASS;
        $newPassword = GenerateRandomString::generate(12);

        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $this->assertEquals(0, $spool->count());


        $this->requestAccountSendResetPasswordRequest($account->getEmail())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true
            ]);

        /** @var PHPUnitMemorySpool $spool */
        $spool = self::$app->getContainer()->get(PHPUnitMemorySpool::class);
        $this->assertEquals(1, $spool->count());

        preg_match('/token=([a-zA-Z0-90]+)/', $spool->getLastMessage()->getBody(), $matches);

        $this->assertEquals(2, count($matches));
        $this->assertTrue(is_string($token = $matches[1]));
        $this->assertTrue(strlen($token) === ResetPasswordRequest::RESET_PASSWORD_TOKEN_LENGTH);

        $token = $this->requestAccountResetPassword(['accept_token' => $token, 'new_password' => $newPassword])
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => [
                        'id' => $this->expectId(),
                        'email' => $account->getEmail(),
                    ],
                    'profile' => [
                        'id' => $this->expectId(),
                    ]
                ],
            ])
            ->fetch(function(array $json) {
                return $json['token'];
            });

        $this->requestAccountSignIn($account->getEmail(), $newPassword)
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'token' => [
                    'jwt' => $this->expectString(),
                    'account' => $token['account'],
                    'profile' => $token['profile'],
                ]
            ]);

        $this->requestAccountSignIn($account->getEmail(), $oldPassword)
            ->__invoke()
            ->expectAuthError();
    }
}