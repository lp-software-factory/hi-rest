<?php
namespace HappyInvite\REST\Bundles\Account\Tests\Fixtures;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use HappyInvite\Domain\Bundles\Account\Service\AccountRegistrationService;
use HappyInvite\Domain\Bundles\Auth\Service\JWTTokenService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\REST\Bundles\PHPUnit\Fixture;
use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use Zend\Expressive\Application;

final class UserAccountFixture implements Fixture
{
    /** @var Account */
    private static $account;

    /** @var Profile */
    private static $profile;

    /** @var string */
    private static $jwt;

    public static $EMAIL = 'user@example.com';
    public static $PASS = '1234';

    public function up(Application $app, EntityManager $em)
    {
        $jwtTokenService = $app->getContainer()->get(JWTTokenService::class); /** @var JWTTokenService $jwtTokenService */
        $accountRegistrationService = $app->getContainer()->get(AccountRegistrationService::class); /** @var AccountRegistrationService $accountRegistrationService */

        self::$account = $accountRegistrationService->registerUserAccount(new AccountRegisterParameters(
            self::$EMAIL, self::$PASS, new CreateProfileParameters('PHPUnit', 'User')
        ));
        self::$profile = self::$account->getCurrentProfile();
        self::$jwt = $jwtTokenService->generateJWTFor(self::$account, self::$profile);
    }

    public static function getAccount(): Account
    {
        return self::$account;
    }

    public static function getProfile(): Profile
    {
        return self::$profile;
    }

    public static function getJWT(): string
    {
        return self::$jwt;
    }
}