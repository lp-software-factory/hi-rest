<?php
namespace HappyInvite\REST\Bundles\Account\Tests\Paths;

use HappyInvite\REST\Bundles\Account\Tests\AccountMiddlewareTestCase;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;

final class SetLocaleMiddlewareTest extends AccountMiddlewareTestCase
{
    public function test200()
    {
        $this->requestAccountSetLocale('ru_RU')
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => [
                    'region' => 'ru_RU'
                ]
            ]);

        $this->requestFrontend()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'locale' => [
                    'current' => [
                        'region' => 'ru_RU'
                    ]
                ]
            ])
        ;

        $this->requestAccountSetLocale('en_GB')
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'success' => true,
                'locale' => [
                    'region' => 'en_GB'
                ]
            ]);

        $this->requestFrontend()
            ->auth(UserAccountFixture::getJWT())
            ->__invoke()
            ->expectStatusCode(200)
            ->expectJSONContentType()
            ->expectJSONBody([
                'locale' => [
                    'current' => [
                        'region' => 'en_GB'
                    ]
                ]
            ])
        ;
    }

    public function test403()
    {
        $this->requestAccountSetLocale('ru_RU')
            ->__invoke()
            ->expectAuthError();
    }

    public function test404()
    {

    }
}