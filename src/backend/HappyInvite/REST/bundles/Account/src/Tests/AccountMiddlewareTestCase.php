<?php
namespace HappyInvite\REST\Bundles\Account\Tests;

use HappyInvite\REST\Bundles\Account\Tests\Fixtures\AdminAccountFixture;
use HappyInvite\REST\Bundles\Account\Tests\Fixtures\UserAccountFixture;
use HappyInvite\REST\Bundles\Locale\Tests\Fixture\LocaleFixture;
use HappyInvite\REST\Bundles\PHPUnit\TestCase\HappyInviteMiddlewareTestCase;

abstract class AccountMiddlewareTestCase extends HappyInviteMiddlewareTestCase
{
    protected function getFixtures(): array
    {
        return [
            new LocaleFixture(),
            new AdminAccountFixture(),
            new UserAccountFixture(),
        ];
    }
}