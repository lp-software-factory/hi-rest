<?php
namespace HappyInvite\REST\Bundles\Mail;

use HappyInvite\REST\Bundle\RESTBundle;

final class MailRESTBundle extends RESTBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}