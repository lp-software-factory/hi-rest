<?php
namespace HappyInvite\REST;

use HappyInvite\Domain\HIDomainBundle;
use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\HIPlatformBundle;

$appBuilder = require __DIR__.'/../../../Platform/resources/bootstrap/bootstrap.php';

return $appBuilder([
    'environment' => Environment::DEVELOPMENT,
    'cache' => [
        'type' => 'filesystem',
        'enabled' => true,
        'dir' => __DIR__.'/../../../Domain/resources/cache',
    ],
    'bundles' => [
        new HIPlatformBundle(),
        new HIDomainBundle(),
        new HIRESTBundle(),
    ]
]);