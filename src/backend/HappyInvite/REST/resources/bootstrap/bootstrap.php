<?php
namespace HappyInvite\REST;

use HappyInvite\Domain\HIDomainBundle;
use HappyInvite\Platform\HIPlatformBundle;
use HappyInvite\Stage\HIStageBundle;

$appBuilder = require __DIR__.'/../../../Platform/resources/bootstrap/bootstrap.php';

return $appBuilder([
    'bundles' => [
        new HIPlatformBundle(),
        new HIDomainBundle(),
        new HIRESTBundle(),
        new HIStageBundle(),
    ]
]);