<?php
namespace HappyInvite\Domain;

use Symfony\Component\Console\Application;

$application = require_once __DIR__.'./../../REST/resources/bootstrap/bootstrap.php'; /** @var \Zend\Expressive\Application $application */

$consoleApp = $application->getContainer()->get(Application::class);
$consoleApp->run();