<?php
namespace HappyInvite\Domain;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Factory\ComposerJSONFactory;

return [
    'config.hi.rest.composer.json' => factory(ComposerJSONFactory::class),
];