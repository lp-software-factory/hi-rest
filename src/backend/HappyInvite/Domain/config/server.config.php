<?php
namespace HappyInvite\Domain;

return [
    'config.server' => [
        'protocol' => 'http',
        'host' => '127.0.0.1:8080',
        'ip' => '127.0.0.1',
        'port' => 8080
    ],
];