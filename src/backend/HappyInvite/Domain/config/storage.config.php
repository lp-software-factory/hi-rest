<?php
namespace HappyInvite\Domain;

return [
    'config.storage' => [
        'dir' => '/var/hi-storage/storage',
        'www' => '/storage',
    ],
];