<?php
namespace HappyInvite\Domain;

return [
    'config.hi.domain.locale.session' => [
        'key' => 'config.hi.domain.locale.session.key',
        'default' => 'ru_RU',
    ],
];