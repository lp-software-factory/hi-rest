<?php
namespace HappyInvite\Domain;

return [
    'config.hi.domain.attachment' => [
        'dir' => '/var/hi-storage/storage/attachment',
        'www' => '/storage/attachment',
    ],
];