<?php
namespace HappyInvite\Domain;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Service\ServerConfig;
use HappyInvite\Domain\Service\StorageConfig;

return [
    'config.hi.domain.mail.dir' => __DIR__.'/../../../../frontend/src/mail',
    ServerConfig::class => object()->constructorParameter('config', get('config.server')),
    StorageConfig::class => object()->constructorParameter('config', get('config.storage')),
];