<?php
namespace HappyInvite\Domain\Criteria;

final class IdsCriteria
{
    public const UNLIMITED = -1;

    /** @var int[] */
    private $ids;

    public function __construct(array $ids, int $maxIds = self::UNLIMITED)
    {
        $ids = array_filter($ids, function($input) {
            return is_int($input);
        });

        if($maxIds !== self::UNLIMITED) {
            if(count($ids) > $maxIds) {
                throw new \Exception(sprintf('Too much entities requested', $maxIds));
            }
        }

        $this->ids = $ids;
    }

    public function getIds(): array
    {
        return $this->ids;
    }
}