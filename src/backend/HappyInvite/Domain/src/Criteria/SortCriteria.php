<?php
namespace HappyInvite\Domain\Criteria;

use HappyInvite\Domain\Markers\JSONSerializable;

final class SortCriteria implements JSONSerializable
{
    /** @var string */
    private $field;

    /** @var string */
    private $direction;

    public function __construct(string $field, string $direction)
    {
        $direction = strtolower($direction);

        if(! in_array($direction, ['desc', 'asc'])) {
            throw new \Exception(sprintf('Invalid order direction `%s`', $direction));
        }

        $this->field = $field;
        $this->direction = $direction;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'field' => $this->getField(),
            'direction' => $this->getDirection(),
        ];
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }
}