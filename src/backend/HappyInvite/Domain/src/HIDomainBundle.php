<?php
namespace HappyInvite\Domain;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\ContactDomainBundle;
use HappyInvite\Domain\Bundles\Attachment\AttachmentBundle;
use HappyInvite\Domain\Bundles\Avatar\AvatarBundle;
use HappyInvite\Domain\Bundles\CompanyProfile\CompanyProfileBundle;
use HappyInvite\Domain\Bundles\Designer\DesignerDomainBundle;
use HappyInvite\Domain\Bundles\DressCode\DressCodeDomainBundle;
use HappyInvite\Domain\Bundles\Envelope\EnvelopeDomainBundle;
use HappyInvite\Domain\Bundles\Event\EventDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\InviteCardFamilyDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\InviteCardTemplateDomainBundle;
use HappyInvite\Domain\Bundles\Landing\LandingDomainBundle;
use HappyInvite\Domain\Bundles\Mandrill\MandrillDomainBundle;
use HappyInvite\Domain\Bundles\Money\MoneyDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\InviteCardDomainBundle;
use HappyInvite\Domain\Bundles\Locale\LocaleBundle;
use HappyInvite\Domain\Bundles\Mail\MailBundle;
use HappyInvite\Domain\Bundles\MongoDB\MongoDBDomainBundle;
use HappyInvite\Domain\Bundles\Palette\PaletteDomainBundle;
use HappyInvite\Domain\Bundles\Product\ProductDomainBundle;
use HappyInvite\Domain\Bundles\OAuth2\OAuth2DomainBundle;
use HappyInvite\Domain\Bundles\Fonts\FontsDomainBundle;
use HappyInvite\Domain\Bundles\Solution\SolutionDomainBundle;
use HappyInvite\Domain\Bundles\SolutionIndex\SolutionIndexDomainBundle;
use HappyInvite\Domain\Bundles\Translation\TranslationDomainBundle;
use HappyInvite\Platform\Bundles\HIBundle;
use HappyInvite\Domain\Bundles\Company\CompanyBundle;
use HappyInvite\Domain\Bundles\Console\ConsoleBundle;
use HappyInvite\Domain\Bundles\DoctrineORM\DoctrineORMBundle;
use HappyInvite\Domain\Bundles\Frontend\FrontendBundle;
use HappyInvite\Domain\Bundles\Version\VersionBundle;
use HappyInvite\Domain\Bundles\Account\AccountBundle;
use HappyInvite\Domain\Bundles\Profile\ProfileBundle;
use HappyInvite\Domain\Bundles\Auth\AuthBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScriptInjectableBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\EventsInitScript;

final class HIDomainBundle extends HIBundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new MongoDBDomainBundle(),
            new DoctrineORMBundle(),
            new ConsoleBundle(),
            new VersionBundle(),
            new FrontendBundle(),
            new AccountBundle(),
            new ProfileBundle(),
            new AuthBundle(),
            new CompanyBundle(),
            new MailBundle(),
            new LocaleBundle(),
            new AttachmentBundle(),
            new AvatarBundle(),
            new InviteCardDomainBundle(),
            new PaletteDomainBundle(),
            new ProductDomainBundle(),
            new MoneyDomainBundle(),
            new OAuth2DomainBundle(),
            new InviteCardFamilyDomainBundle(),
            new DesignerDomainBundle(),
            new InviteCardTemplateDomainBundle(),
            new CompanyProfileBundle(),
            new FontsDomainBundle(),
            new EnvelopeDomainBundle(),
            new EventDomainBundle(),
            new SolutionDomainBundle(),
            new SolutionIndexDomainBundle(),
            new DressCodeDomainBundle(),
            new LandingDomainBundle(),
            new TranslationDomainBundle(),
            new ContactDomainBundle(),
            new MandrillDomainBundle(),
        ];
    }

    public function getInitScripts(): array
    {
        return [
            new EventsInitScript(),
        ];
    }
}