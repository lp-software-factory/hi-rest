<?php
namespace HappyInvite\Domain\Exceptions;

final class DoNotImplementItException extends \Exception {}