<?php
namespace HappyInvite\Domain\Markers\ActivatedEntity;

interface ActivatedEntity
{
    public function activate();
    public function deactivate();
    public function isActivated(): bool;
}