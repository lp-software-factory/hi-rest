<?php
namespace HappyInvite\Domain\Markers;

interface JSONSerializable
{
    public function toJSON(array $options = []): array;
}