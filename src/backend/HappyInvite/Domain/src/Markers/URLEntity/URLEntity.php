<?php
namespace HappyInvite\Domain\Markers\URLEntity;

interface URLEntity
{
    public function setUrl(string $url);
    public function getUrl(): string;
}