<?php
namespace HappyInvite\Domain\Markers\SIDEntity;

use HappyInvite\Domain\Util\GenerateRandomString;

trait SIDEntityTrait
{
    /**
     * @Column(name="sid", type="string")
     * @var string
     */
    private $sid;

    public function getSID(): string
    {
        return $this->sid;
    }

    public function regenerateSID(): string
    {
        $this->sid = GenerateRandomString::generate(SIDEntity::SID_LENGTH);

        return $this->sid;
    }
}