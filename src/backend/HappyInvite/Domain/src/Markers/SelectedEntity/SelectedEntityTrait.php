<?php
namespace HappyInvite\Domain\Markers\SelectedEntity;

use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;

trait SelectedEntityTrait
{
    /**
     * @Column(name="is_selected", type="boolean")
     * @var bool
     */
    private $isSelected = false;

    public function select()
    {
        if(! $this->isSelected()) {
            $this->isSelected = true;

            if($this instanceof ModificationEntity) {
                /** @noinspection PhpUndefinedMethodInspection */
                $this->markAsUpdated();
            }
        }
    }

    public function blur() {
        if($this->isSelected()) {
            $this->isSelected = false;

            if($this instanceof ModificationEntity) {
                /** @noinspection PhpUndefinedMethodInspection */
                $this->markAsUpdated();
            }
        }

    }

    public function isSelected(): bool
    {
        return $this->isSelected;
    }
}