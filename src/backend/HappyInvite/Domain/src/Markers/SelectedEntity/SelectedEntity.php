<?php
namespace HappyInvite\Domain\Markers\SelectedEntity;

interface SelectedEntity
{
    public function select();
    public function blur();
    public function isSelected(): bool;
}