<?php
namespace HappyInvite\Domain\Markers\ModificationEntity;

interface ModificationEntity
{
    public function getDateCreatedAt(): \DateTime;
    public function getLastUpdatedOn(): \DateTime;
}