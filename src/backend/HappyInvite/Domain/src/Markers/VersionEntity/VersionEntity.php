<?php
namespace HappyInvite\Domain\Markers\VersionEntity;

interface VersionEntity
{
    public function getVersion(): string;
}