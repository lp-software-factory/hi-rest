<?php
namespace HappyInvite\Domain\Events;

interface SubscriptionScript
{
    public function __invoke(DomainEventEmitter $eventEmitter);
}