<?php
namespace HappyInvite\Domain\Events;

use Evenement\EventEmitterInterface;
use Evenement\EventEmitterTrait;

final class DomainEventEmitter implements EventEmitterInterface
{
    use EventEmitterTrait;
}