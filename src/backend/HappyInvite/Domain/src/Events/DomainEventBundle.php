<?php
namespace HappyInvite\Domain\Events;

interface DomainEventBundle
{
    public function getEventScripts(): array;
}