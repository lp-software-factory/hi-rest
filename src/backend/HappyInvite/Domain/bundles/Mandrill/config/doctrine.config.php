<?php
namespace HappyInvite\Domain\Bundles\Mandrill;

use HappyInvite\Domain\Bundles\Mandrill\Doctrine\Type\MandrillSendResults\MandrillSendResultsType;

return [
    'hi.config.domain.doctrine.types' => [
        MandrillSendResultsType::TYPE_NAME => MandrillSendResultsType::class,
    ],
];