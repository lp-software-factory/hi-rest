<?php
namespace HappyInvite\Domain\Bundles\Mandrill;

return [
    'config.hi.domain.mandrill.api_key' => 'Zu7n147DpEjqpIqKO5xqsg',
    'config.hi.domain.mandrill' => [
        'api_key' => \DI\get('config.hi.domain.mandrill.api_key'),
        'from' => [
            'email' => 'no-reply@happy-invite.com',
            'name' => 'HappyInvite.com'
        ]
    ],
];