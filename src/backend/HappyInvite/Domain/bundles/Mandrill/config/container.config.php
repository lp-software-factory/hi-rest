<?php
namespace HappyInvite\Domain\Bundles\Mandrill;

use function DI\get;
use function DI\factory;
use function DI\object;

use DI\Container;
use HappyInvite\Domain\Bundles\Mandrill\Config\MandrillConfig;

return [
    MandrillConfig::class => object()
        ->constructorParameter('config', get('config.hi.domain.mandrill')),
    \Mandrill::class => object()
        ->constructorParameter('apikey', get('config.hi.domain.mandrill.api_key')),
];