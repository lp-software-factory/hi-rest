<?php
namespace HappyInvite\Domain\Bundles\Mandrill\Service;

use Cocur\Chain\Chain;
use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Mandrill\Config\MandrillConfig;
use HappyInvite\Domain\Bundles\Mandrill\Mandrill\SendResult;
use HappyInvite\Domain\Exceptions\NotImplementedException;

final class MandrillService
{
    /** @var string */
    private $currentMode = self::MODE_SYNC;

    public const MODE_SYNC = 'sync';
    public const MODE_ASYNC = 'async';

    public const EVENT_STATUS_PERFORM = 'hi.domain.mandrill.status.perform';
    public const EVENT_STATUS_UPDATE = 'hi.domain.mandrill.status.update';

    /** @var MandrillConfig */
    private $config;

    /** @var \Mandrill */
    private $mandrill;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(MandrillConfig $config, \Mandrill $mandrill)
    {
        $this->config = $config;
        $this->mandrill = $mandrill;
        $this->eventEmitter = new EventEmitter();
    }

    public function enableSyncMode(): void
    {
        $this->currentMode = self::MODE_SYNC;
    }

    public function enableAsyncMode(): void
    {
        $this->currentMode = self::MODE_ASYNC;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function perform(string $mailingId, array $options): void
    {
        $options['mailing']['metadata']['mailing_id'] = $mailingId;

        if($this->currentMode === self::MODE_SYNC) {
            $this->performSync($mailingId, $options);
        }else if($this->currentMode === self::MODE_ASYNC) {
            throw new NotImplementedException();
        }else{
            throw new \Exception(sprintf('Unknown MandrillService mode `%s`', $this->currentMode));
        }
    }

    private function performSync(string $mailingId, array $options): void
    {
        $result = $this->mandrill->messages->send($options['mailing']);

        $sendResults = Chain::create($result)
            ->map(function(array $input) {
                return new SendResult($input);
            })
            ->array;

        $this->eventEmitter->emit(self::EVENT_STATUS_UPDATE, [$sendResults, $mailingId, $options['mailing']['metadata']]);
    }
}