<?php
namespace HappyInvite\Domain\Bundles\Mandrill\Mandrill\Exceptions;

final class MailingSendResultNotFoundException extends \Exception
{}