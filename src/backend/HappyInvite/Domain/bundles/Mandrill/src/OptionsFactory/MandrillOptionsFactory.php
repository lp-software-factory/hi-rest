<?php
namespace HappyInvite\Domain\Bundles\Mandrill\OptionsFactory;

use HappyInvite\Domain\Bundles\Mandrill\Config\MandrillConfig;

final class MandrillOptionsFactory
{
    /** @var MandrillConfig */
    private $config;

    public function __construct(MandrillConfig $config)
    {
        $this->config = $config;
    }

    public function createOptions(string $subject, string $html, string $text, array $recipients, array $metadata): array
    {
        $config = [
            'mailing' => [
                'html' => $html,
                'text' => $text,
                'subject' => $subject,
                'from_email' => $this->config->getFromEmail(),
                'from_name' => $this->config->getFromName(),
                'to' => $recipients,
                'metadata' => $metadata,
            ],
            'options' => [
                'ip_pool' => 'Main Pool'
            ]
        ];

        return $config;
    }
}