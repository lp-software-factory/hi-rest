<?php
namespace HappyInvite\Domain\Bundles\Mandrill\Config;

final class MandrillConfig
{
    /** @var string */
    private $apiKey;

    /** @var string */
    private $fromEmail;

    /** @var string */
    private $fromName;

    public function __construct(array $config)
    {
        $this->apiKey = $config['api_key'];
        $this->fromName = $config['from']['name'];
        $this->fromEmail = $config['from']['email'];
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }
}