<?php
namespace HappyInvite\Domain\Bundles\Mandrill\Mandrill;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Markers\JSONSerializable;

final class SendResult implements JSONSerializable
{
    public const EVENT_STATUS_UPDATE = 'hi.domain.mandrill.status.update';

    public const STATUS_SENT = 'sent';
    public const STATUS_QUEUED = 'queued';
    public const STATUS_SCHEDULED = 'scheduled';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_INVALID = 'invalid';

    public const REJECT_REASON_HARD_BOUNCE = 'hard-bounce';
    public const REJECT_REASON_SOFT_BOUNCE = 'soft-bounce';
    public const REJECT_REASON_SPAM = 'spam';
    public const REJECT_REASON_UNSUB = 'unsub';
    public const REJECT_REASON_CUSTOM = 'custom';
    public const REJECT_REASON_INVALID_SENDER = 'invalid-sender';
    public const REJECT_REASON_INVALID = 'invalid';
    public const REJECT_REASON_TEST_MODE_LIMIT = 'test-mode-limit';
    public const REJECT_REASON_UNSIGNED = 'unsigned';
    public const REJECT_REASON_RULE = 'rule';

    public const ALL_STATUSES = [
        self::STATUS_SENT,
        self::STATUS_QUEUED,
        self::STATUS_SCHEDULED,
        self::STATUS_REJECTED,
        self::STATUS_INVALID,
    ];

    public const ALL_REJECT_REASONS = [
        self::REJECT_REASON_HARD_BOUNCE,
        self::REJECT_REASON_SOFT_BOUNCE,
        self::REJECT_REASON_SPAM,
        self::REJECT_REASON_UNSUB,
        self::REJECT_REASON_CUSTOM,
        self::REJECT_REASON_INVALID_SENDER,
        self::REJECT_REASON_INVALID,
        self::REJECT_REASON_TEST_MODE_LIMIT,
        self::REJECT_REASON_UNSIGNED,
        self::REJECT_REASON_RULE,
    ];

    /** @var string */
    private $_id;

    /** @var string */
    private $email;

    /** @var string */
    private $status;

    /** @var string */
    private $rejectReason;

    /** @var \DateTime */
    private $statusUpdate;

    /** @var EventEmitterInterface */
    private $subject;

    public function __construct(array $input)
    {
        $this->_id = $input['_id'];
        $this->email = $input['email'];
        $this->status = $input['status'];
        $this->rejectReason = $input['reject_reason'] ?? null;
        $this->subject = new EventEmitter();

        if(isset($input['status_update'])) {
            $this->statusUpdate = \DateTime::createFromFormat(\DateTime::RFC2822, $input['status_update']);
        }else{
            $this->statusUpdate = new \DateTime();
        }
    }

    public function getSubject(): EventEmitterInterface
    {
        return $this->subject;
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            '_id' => $this->getId(),
            'email' => $this->getEmail(),
            'status' => $this->getStatus(),
            'status_update' => $this->getStatusUpdate()->format(\DateTime::RFC2822),
            'is_rejected' => $this->isRejected(),
        ];

        if(is_string($this->rejectReason)) {
            $json['reject_reason'] = $this->getRejectReason();
        }

        return $json;
    }

    public static function fromJSON(array $input): self
    {
        return new self($input);
    }

    public function getId(): string
    {
        return $this->_id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getStatusUpdate(): \DateTime
    {
        return $this->statusUpdate;
    }

    public function updateStatus(string $status): void
    {
        $this->status = $status;
        $this->statusUpdate = new \DateTime();

        $this->subject->emit(self::EVENT_STATUS_UPDATE, [$this]);
    }

    public function isSent(): bool
    {
        return in_array($this->getStatus(), [
            self::STATUS_SENT,
        ], true);
    }

    public function isWaiting(): bool
    {
        return in_array($this->getStatus(), [
            self::STATUS_QUEUED,
            self::STATUS_SCHEDULED,
        ], true);
    }

    public function isRejected(): bool
    {
        return in_array($this->getStatus(), [
            self::STATUS_REJECTED,
            self::STATUS_INVALID,
        ], true);
    }

    public function getRejectReason(): string
    {
        return $this->rejectReason;
    }
}