<?php
namespace HappyInvite\Domain\Bundles\Mandrill\Doctrine\Type\MandrillSendResults;

use HappyInvite\Domain\Bundles\Mandrill\Mandrill\SendResult;

trait MandrillSendResultsEntityTrait
{
    /**
     * @Column(name="mandrill_results", type="mandrill_send_results")
     * @var MandrillSendResultsManager
     */
    private $mandrillResults;

    public function importMandrillResults(array $sendResults): void
    {
        array_map(function(SendResult $sendResult) {
            $this->mandrillResults->upsertSendResult($sendResult);
        }, $sendResults);

        $this->mandrillResults = clone $this->mandrillResults;
    }

    public function getMandrillResults(): array
    {
        return $this->mandrillResults->all();
    }
}