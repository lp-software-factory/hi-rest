<?php
namespace HappyInvite\Domain\Bundles\Mandrill\Doctrine\Type\MandrillSendResults;

use HappyInvite\Domain\Bundles\Mandrill\Mandrill\Exceptions\MailingSendResultNotFoundException;
use HappyInvite\Domain\Bundles\Mandrill\Mandrill\SendResult;

final class MandrillSendResultsManager implements \Countable
{
    /** @var array */
    private $sendResults = [];

    public function importFromJSON(array $input): self
    {
        array_map(function(array $json) {
            $this->sendResults[] = new SendResult($json);
        }, $input);

        return $this;
    }

    public function importFromPHPObjects(array $input): self
    {
        array_map(function(SendResult $sendResult) {
            $this->sendResults[] = $sendResult;
        }, $input);

        return $this;
    }

    public function exportsAsJSON(): array
    {
        return array_map(function(SendResult $sendResult) {
            return $sendResult->toJSON();
        }, $this->sendResults);
    }

    public function count(): int
    {
        return count($this->sendResults);
    }

    public function clear(): self
    {
        $this->sendResults = [];

        return $this;
    }

    public function all(): array
    {
        return $this->sendResults;
    }

    public function upsertSendResult(SendResult $sendResult): void
    {
        if($this->hasSendResultWithEmail($sendResult->getEmail())) {
            $record = $this->getSendResultByEmail($sendResult->getEmail());
            $record->updateStatus($sendResult->getStatus());
        }else{
            $this->sendResults[] = $sendResult;
        }
    }

    public function hasSendResultWithEmail(string $email): bool
    {
        return count(array_filter($this->sendResults, function(SendResult $input) use ($email) {
                return $input->getEmail() === $email;
            })) === 1;
    }

    public function getSendResultByEmail(string $email): SendResult
    {
        $result = array_filter($this->sendResults, function(SendResult $input) use ($email) {
            return $input->getEmail() === $email;
        });

        if(count($result) === 0) {
            throw new MailingSendResultNotFoundException(sprintf('MailingSendResult(Email: %s) not found', $email));
        }

        if(count($result) > 1) {
            throw new MailingSendResultNotFoundException(sprintf('MailingSendResult(Email: %s) found but there are some duplicates here!', $email));
        }

        return $result[0];
    }

    public function hasSendResultWithId(string $id): bool
    {
        return count(array_filter($this->sendResults, function(SendResult $input) use ($id) {
                return $input->getId() === $id;
            })) === 1;
    }

    public function getSendResultById(string $id): SendResult
    {
        $result = array_filter($this->sendResults, function(SendResult $input) use ($id) {
            return $input->getId() === $id;
        });

        if(count($result) === 0) {
            throw new MailingSendResultNotFoundException(sprintf('MailingSendResult(Id: %s) not found', $id));
        }

        if(count($result) > 1) {
            throw new MailingSendResultNotFoundException(sprintf('MailingSendResult(Id: %s) found but there are some duplicates here!', $id));
        }

        return $result[0];
    }
}