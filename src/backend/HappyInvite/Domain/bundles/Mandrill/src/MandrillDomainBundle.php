<?php
namespace HappyInvite\Domain\Bundles\Mandrill;

use HappyInvite\Platform\Bundles\HIBundle;

final class MandrillDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}