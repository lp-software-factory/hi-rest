<?php
namespace HappyInvite\Domain\Bundles\Profile;

return [
    'config.hi.domain.profile.image.www' => 'profile/image',
    'config.hi.domain.profile.image.dir' => 'profile/image',
];