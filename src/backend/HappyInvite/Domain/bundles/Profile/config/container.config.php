<?php
namespace HappyInvite\Domain\Bundles\Profile;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Profile\Config\ProfileImageConfig;
use HappyInvite\Domain\Bundles\Profile\Factory\Doctrine\ProfileCompaniesRepositoryFactory;
use HappyInvite\Domain\Bundles\Profile\Factory\Doctrine\ProfileRepositoryFactory;
use HappyInvite\Domain\Bundles\Profile\Factory\ProfileImageConfigFactory;
use HappyInvite\Domain\Bundles\Profile\Repository\ProfileCompaniesRepository;
use HappyInvite\Domain\Bundles\Profile\Repository\ProfileRepository;

return [
    ProfileRepository::class => factory(ProfileRepositoryFactory::class),
    ProfileCompaniesRepository::class => factory(ProfileCompaniesRepositoryFactory::class),
    ProfileImageConfig::class => factory(ProfileImageConfigFactory::class),
];