<?php
namespace HappyInvite\Domain\Bundles\Profile;

use HappyInvite\Domain\Bundles\Profile\Console\Command\RegenerateProfileImageCommand;

return [
    'config.console' => [
        'commands' => [
            'Profile' => [
                RegenerateProfileImageCommand::class,
            ],
        ],
    ],
];