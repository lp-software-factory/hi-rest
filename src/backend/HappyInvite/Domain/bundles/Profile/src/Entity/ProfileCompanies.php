<?php
namespace HappyInvite\Domain\Bundles\Profile\Entity;

use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Profile\Repository\ProfileCompaniesRepository")
 * @Table(name="profile_companies")
 */
class ProfileCompanies implements JSONSerializable, IdEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="profile_id", referencedColumnName="id")
     * @var Profile
     */
    private $profile;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Company\Entity\Company")
     * @JoinColumn(name="company_id", referencedColumnName="id")
     * @var Company
     */
    private $company;

    public function __construct(Profile $profile, Company $company)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->profile = $profile;
        $this->company = $company;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'profile_id' => $this->getProfile()->getIdNoFall(),
            'company_id' => $this->getCompany()->getIdNoFall(),
        ];
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }
}