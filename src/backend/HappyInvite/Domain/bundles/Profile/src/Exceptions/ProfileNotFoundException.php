<?php
namespace HappyInvite\Domain\Bundles\Profile\Exceptions;

final class ProfileNotFoundException extends \Exception {}