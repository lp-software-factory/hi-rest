<?php
namespace HappyInvite\Domain\Bundles\Profile\Image;

use HappyInvite\Domain\Bundles\Profile\Config\ProfileImageConfig;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use League\Flysystem\FilesystemInterface;

final class ProfileImageStrategyFactory
{
    /** @var FilesystemInterface */
    private $fileSystem;

    /** @var string */
    private $wwwPath;

    public function __construct(ProfileImageConfig $config)
    {
        $this->fileSystem = $config->getFlySystem();
        $this->wwwPath = $config->getWww();
    }

    public function forProfile(Profile $profile): ProfileImageStrategy
    {
        return new ProfileImageStrategy($profile, $this->fileSystem, $this->wwwPath);
    }
}