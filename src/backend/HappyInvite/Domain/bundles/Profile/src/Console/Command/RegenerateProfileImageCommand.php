<?php
namespace HappyInvite\Domain\Bundles\Profile\Console\Command;

use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class RegenerateProfileImageCommand extends Command
{
    /** @var ProfileService */
    private $profileService;

    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('profile:image:regenerate')
            ->setDescription('Regenerate image')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('id', InputArgument::REQUIRED, 'Profile Id'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = (int) $input->getArgument('id');

        $this->profileService->regenerateProfileImage($id);

        $output->writeln('Profile image regenerated');
    }
}