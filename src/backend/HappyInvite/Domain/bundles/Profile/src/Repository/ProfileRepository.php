<?php
namespace HappyInvite\Domain\Bundles\Profile\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Exceptions\ProfileNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class ProfileRepository extends EntityRepository
{
    public function createProfile(Profile $profile): int
    {
        while($this->hasWithSID($profile->getSID())) {
            $profile->regenerateSID();
        }

        $this->getEntityManager()->persist($profile);
        $this->getEntityManager()->flush($profile);

        return $profile->getId();
    }

    public function saveProfile(Profile $profile)
    {
        $this->getEntityManager()->flush($profile);
    }

    public function deleteProfile(Profile $profile)
    {
        $this->getEntityManager()->remove($profile);
        $this->getEntityManager()->flush($profile);
    }

    public function listProfiles(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('p');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());

        return $query->getQuery()->execute();
    }

    public function getProfileById(int $profileId): Profile
    {
        $result = $this->find($profileId);

        if($result instanceof Profile) {
            return $result;
        }else{
            throw new ProfileNotFoundException(sprintf('Profile `ID(%d)` not found', $id));
        }
    }

    public function getProfilesByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();

        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Profiles with IDs (%s) not found', array_diff($ids, array_map(function(Profile $profile) {
                return $profile->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Profile
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof Profile) {
            return $result;
        }else{
            throw new ProfileNotFoundException(sprintf('Profile `SID(%s)` not found', $sid));
        }
    }
}