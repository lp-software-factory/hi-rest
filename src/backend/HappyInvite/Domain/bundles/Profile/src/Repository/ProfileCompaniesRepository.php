<?php
namespace HappyInvite\Domain\Bundles\Profile\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Entity\ProfileCompanies;

final class ProfileCompaniesRepository extends EntityRepository
{
    public function setCompaniesOfProfile(Profile $profile, array $companies)
    {
        $this->clearCompaniesOfProfile($profile);

        $entities = array_map(function(Company $company) use ($profile) {
            return new ProfileCompanies($profile, $company);
        }, $companies);

        foreach($entities as $entity) {
            $this->getEntityManager()->persist($entity);
        }

        $this->getEntityManager()->flush($entities);
    }

    public function getCompaniesOfProfile(Profile $profile): array
    {
        return $this->findBy(['profile' => $profile]);
    }

    public function clearCompaniesOfProfile(Profile $profile)
    {
        $entities = array_map(function(ProfileCompanies $pc) {
            $this->getEntityManager()->remove($pc);

            return $pc;
        }, $this->getCompaniesOfProfile($profile));

        $this->getEntityManager()->flush($entities);
    }

    public function findCompaniesOfProfiles(array $profileIds): array
    {
        return $this->findBy(['profile' => $profileIds]);
    }
}