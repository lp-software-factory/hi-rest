<?php
namespace HappyInvite\Domain\Bundles\Profile;

use HappyInvite\Platform\Bundles\HIBundle;

final class ProfileBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}