<?php
namespace HappyInvite\Domain\Bundles\Profile\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class ProfileRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Profile::class;
    }
}