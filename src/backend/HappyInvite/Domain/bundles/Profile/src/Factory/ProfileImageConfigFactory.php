<?php
namespace HappyInvite\Domain\Bundles\Profile\Factory;

use HappyInvite\Domain\Bundles\Attachment\Config\AttachmentConfig;
use HappyInvite\Domain\Bundles\Profile\Config\ProfileImageConfig;
use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\Service\EnvironmentService;
use Interop\Container\ContainerInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;

final class ProfileImageConfigFactory
{
    public function __invoke(ContainerInterface $container, AttachmentConfig $config, EnvironmentService $environmentService)
    {
        $www = sprintf('%s/%s', $config->getWww(), $container->get('config.hi.domain.profile.image.www'));
        $dir = $dir = sprintf('%s/%s', $config->getDir(), $container->get('config.hi.domain.profile.image.dir'));

        if($environmentService->getCurrent() === Environment::TESTING) {
            $adapter = new MemoryAdapter();
        }else{
            $adapter = new Local($dir);
        }

        return new ProfileImageConfig($www, $dir, new Filesystem($adapter));
    }
}