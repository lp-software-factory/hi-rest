<?php
namespace HappyInvite\Domain\Bundles\Profile\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Profile\Entity\ProfileCompanies;

final class ProfileCompaniesRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return ProfileCompanies::class;
    }
}