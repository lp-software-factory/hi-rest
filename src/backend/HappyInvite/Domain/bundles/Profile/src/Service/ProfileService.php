<?php
namespace HappyInvite\Domain\Bundles\Profile\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Avatar\Image\ImageCollection;
use HappyInvite\Domain\Bundles\Avatar\Parameters\UploadImageParameters;
use HappyInvite\Domain\Bundles\Avatar\Service\AvatarService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Image\ProfileImageStrategyFactory;
use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use HappyInvite\Domain\Bundles\Profile\Repository\ProfileCompaniesRepository;
use HappyInvite\Domain\Bundles\Profile\Repository\ProfileRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class ProfileService
{
    public const EVENT_PROFILE_CREATED = 'hi.domain.profile.created';
    public const EVENT_PROFILE_UPDATED = 'hi.domain.profile.updated';
    public const EVENT_PROFILE_EDITED = 'hi.domain.profile.edited';
    public const EVENT_PROFILE_DELETE = 'hi.domain.profile.delete';
    public const EVENT_PROFILE_DELETED = 'hi.domain.profile.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var ProfileRepository */
    private $profileRepository;

    /** @var ProfileCompaniesRepository */
    private $profileCompaniesRepository;

    /** @var AvatarService */
    private $avatarService;

    /** @var ProfileImageStrategyFactory */
    private $profileImageStrategyFactory;

    public function __construct(
        ProfileRepository $profileRepository,
        ProfileCompaniesRepository $profileCompaniesRepository,
        AvatarService $avatarService,
        ProfileImageStrategyFactory $profileImageStrategyFactory
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->profileRepository = $profileRepository;
        $this->profileCompaniesRepository = $profileCompaniesRepository;
        $this->avatarService = $avatarService;
        $this->profileImageStrategyFactory = $profileImageStrategyFactory;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createProfileForAccount(Account $account, CreateProfileParameters $parameters): Profile
    {
        $profile = new Profile(
            $account,
            $parameters->getFirstName(),
            $parameters->getLastName(),
            $parameters->getMiddleName()
        );

        $this->profileRepository->createProfile($profile);

        $this->getEventEmitter()->emit(self::EVENT_PROFILE_CREATED, [$profile]);

        $this->avatarService->generateImage($this->profileImageStrategyFactory->forProfile($profile));
        $this->profileRepository->saveProfile($profile);

        $account->attachProfile($profile);

        return $profile;
    }

    public function regenerateProfileImage(int $profileId): ImageCollection
    {
        $profile = $this->profileRepository->getProfileById($profileId);
        $image = $this->avatarService->generateImage($this->profileImageStrategyFactory->forProfile($profile));

        $this->profileRepository->saveProfile($profile);

        $this->getEventEmitter()->emit(self::EVENT_PROFILE_UPDATED, [$profile]);

        return $image;
    }

    public function uploadProfileImage(int $profileId, UploadImageParameters $parameters): ImageCollection
    {
        $profile = $this->profileRepository->getProfileById($profileId);
        $image = $this->avatarService->uploadImage($this->profileImageStrategyFactory->forProfile($profile), $parameters);

        $this->profileRepository->saveProfile($profile);

        $this->getEventEmitter()->emit(self::EVENT_PROFILE_UPDATED, [$profile]);

        return $image;
    }

    public function setProfileCompanies(Profile $profile, array $companies)
    {
        $this->profileCompaniesRepository->setCompaniesOfProfile($profile, $companies);
    }

    public function getProfileById(int $profileId): Profile
    {
        return $this->profileRepository->getProfileById($profileId);
    }

    public function getProfilesByIds(IdsCriteria $idsCriteria): array
    {
        return $this->profileRepository->getProfilesByIds($idsCriteria);
    }
}