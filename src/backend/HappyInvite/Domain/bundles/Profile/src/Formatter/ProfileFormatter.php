<?php
namespace HappyInvite\Domain\Bundles\Profile\Formatter;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class ProfileFormatter
{
    public function formatMany(array $profiles)
    {
        return array_map(function(Profile $profile) {
            return $this->formatOne($profile);
        }, $profiles);
    }

    public function formatOne(Profile $profile)
    {
        return [
            'entity' => $profile->toJSON(),
        ];
    }
}