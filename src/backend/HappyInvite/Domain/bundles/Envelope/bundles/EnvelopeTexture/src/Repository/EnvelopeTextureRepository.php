<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Exceptions\EnvelopeTextureNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;

final class EnvelopeTextureRepository extends EntityRepository 
{
    public function createEnvelopeTexture(EnvelopeTexture $envelopeTexture): int
    {
        while($this->hasWithSID($envelopeTexture->getSID())) {
            $envelopeTexture->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeTexture);
        $this->getEntityManager()->flush($envelopeTexture);

        return $envelopeTexture->getId();
    }

    public function saveEnvelopeTexture(EnvelopeTexture $envelopeTexture)
    {
        $this->getEntityManager()->flush($envelopeTexture);
    }

    public function saveEnvelopeTextureEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeTexture(EnvelopeTexture $envelopeTexture)
    {
        $this->getEntityManager()->remove($envelopeTexture);
        $this->getEntityManager()->flush($envelopeTexture);
    }

    public function getById(int $id): EnvelopeTexture
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeTexture) {
            return $result;
        }else{
            throw new EnvelopeTextureNotFoundException(sprintf('EnvelopeTexture `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeTextureNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeTexture $envelopeTexture) {
                    return $envelopeTexture->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllPublic(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeTextureWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeTexture
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeTexture) {
            return $result;
        }else{
            throw new EnvelopeTextureNotFoundException(sprintf('EnvelopeTexture `SID(%s)` not found', $sid));
        }
    }
}