<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;

final class EnvelopeTextureFormatter
{
    /** @var EnvelopeTextureDefinitionFormatter */
    private $envelopeTextureDefinitionFormatter;

    public function __construct(EnvelopeTextureDefinitionFormatter $envelopeTextureDefinitionFormatter)
    {
        $this->envelopeTextureDefinitionFormatter = $envelopeTextureDefinitionFormatter;
    }

    public function formatOne(EnvelopeTexture $entity): array
    {
        $json = $entity->toJSON();
        $json['definitions'] = $this->envelopeTextureDefinitionFormatter->formatMany($entity->getDefinitions()->toArray());

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopeTexture $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}