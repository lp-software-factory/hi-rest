<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository\EnvelopeTextureDefinitionRepository")
 * @Table(name="envelope_texture_definition")
 */
class EnvelopeTextureDefinition implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, SerialEntity, JSONMetadataEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, SerialEntityTrait, ModificationEntityTrait, ActivatedEntityTrait, VersionEntityTrait, JSONMetadataEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture")
     * @JoinColumn(name="owner_envelope_texture_id", referencedColumnName="id")
     * @var EnvelopeTexture
     */
    private $owner;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="preview_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $preview;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="resources", type="json_array")
     * @var array
     */
    private $resources = [];

    public function __construct(
        EnvelopeTexture $owner,
        ImmutableLocalizedString $title,
        Attachment $preview
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->owner = $owner;
        $this->preview = $preview;
        $this->title = $title;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'owner_envelope_texture_id' => $this->getOwner()->getId(),
            'preview' => $this->getPreview()->toJSON($options),
            'title' => $this->getTitle()->toJSON($options),
            'resources' => $this->getResources(),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getOwner(): EnvelopeTexture
    {
        return $this->owner;
    }

    public function setOwner(EnvelopeTexture $owner): self
    {
        $this->owner = $owner;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function setPreview(Attachment $preview): self
    {
        $this->preview = $preview;
        $this->markAsUpdated();

        return $this;
    }

    public function getResources(): array
    {
        return $this->resources;
    }

    public function setResources(array $resources): self
    {
        $this->resources = array_map(function($input) {
            if(is_array($input)) {
                return $input;
            }else if($input instanceof EnvelopeTextureDefinitionResource) {
                return $input->toJSON();
            }else{
                throw new \Exception(sprintf('Invalid resources, expected array or %s, got %s', EnvelopeTextureDefinitionResource::class, gettype($input)));
            }
        }, $resources);
        $this->markAsUpdated();

        return $this;
    }
}