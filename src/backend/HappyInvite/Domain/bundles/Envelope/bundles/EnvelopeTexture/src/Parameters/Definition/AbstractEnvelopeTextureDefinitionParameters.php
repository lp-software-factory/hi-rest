<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;

abstract class AbstractEnvelopeTextureDefinitionParameters
{
    /** @var EnvelopeTexture */
    private $owner;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var Attachment */
    private $preview;

    /** @var array */
    private $resources;

    public function __construct(
        EnvelopeTexture $owner,
        ImmutableLocalizedString $title,
        Attachment $preview,
        array $resources
    ) {
        $this->owner = $owner;
        $this->title = $title;
        $this->preview = $preview;
        $this->resources = $resources;
    }

    public function getOwner(): EnvelopeTexture
    {
        return $this->owner;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getResources(): array
    {
        return $this->resources;
    }
}