<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Exceptions;

final class EnvelopeTextureNotFoundException extends \Exception {}