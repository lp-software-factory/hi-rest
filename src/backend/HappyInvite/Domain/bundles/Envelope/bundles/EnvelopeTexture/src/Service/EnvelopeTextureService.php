<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\CreateEnvelopeTextureParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\EditEnvelopeTextureParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository\EnvelopeTextureRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeTextureService
{
    public const EVENT_CREATE = 'hi.domain.envelope.texture.create';
    public const EVENT_EDIT = 'hi.domain.envelope.texture.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.texture.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.texture.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.texture.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.texture.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.texture.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.texture.deactivate';

    /** @var EnvelopeTextureRepository */
    private $envelopeTextureRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        EnvelopeTextureRepository $envelopeTextureRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->envelopeTextureRepository = $envelopeTextureRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeTexture(CreateEnvelopeTextureParameters $parameters): EnvelopeTexture
    {
        $envelopeTexture = new EnvelopeTexture(
            $parameters->getPreview(),
            $parameters->getTitle(),
            $parameters->getDescription()
        );

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($envelopeTexture);
        $serial->normalize();

        $this->envelopeTextureRepository->createEnvelopeTexture($envelopeTexture);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeTexture]);

        return $envelopeTexture;
    }

    public function editEnvelopeTexture(int $envelopeTextureId, EditEnvelopeTextureParameters $parameters): EnvelopeTexture
    {
        $envelopeTexture = $this->envelopeTextureRepository->getById($envelopeTextureId);
        $envelopeTexture
            ->setPreview($parameters->getPreview())
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription());

        $this->envelopeTextureRepository->saveEnvelopeTexture($envelopeTexture);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeTexture]);

        return $envelopeTexture;
    }

    public function moveUpEnvelopeTexture(int $envelopeTextureId): int
    {
        $envelopeTexture = $this->getEnvelopeTextureById($envelopeTextureId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($envelopeTexture);
        $serial->normalize();
        $this->envelopeTextureRepository->saveEnvelopeTextureEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeTexture, $envelopeTexture->getPosition()]);

        return $envelopeTexture->getPosition();
    }

    public function moveDownEnvelopeTexture(int $envelopeTextureId): int
    {
        $envelopeTexture = $this->getEnvelopeTextureById($envelopeTextureId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($envelopeTexture);
        $serial->normalize();
        $this->envelopeTextureRepository->saveEnvelopeTextureEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeTexture, $envelopeTexture->getPosition()]);

        return $envelopeTexture->getPosition();
    }

    public function activateEnvelopeTexture(int $envelopeTextureId): EnvelopeTexture
    {
        $envelopeTexture = $this->getEnvelopeTextureById($envelopeTextureId);

        if(! $envelopeTexture->isActivated()) {
            $envelopeTexture->activate();

            $this->envelopeTextureRepository->saveEnvelopeTexture($envelopeTexture);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeTexture]);
        }

        return $envelopeTexture;
    }

    public function deactivateEnvelopeTexture(int $envelopeTextureId): EnvelopeTexture
    {
        $envelopeTexture = $this->getEnvelopeTextureById($envelopeTextureId);

        if($envelopeTexture->isActivated()) {
            $envelopeTexture->deactivate();

            $this->envelopeTextureRepository->saveEnvelopeTexture($envelopeTexture);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeTexture]);
        }

        return $envelopeTexture;
    }

    public function deleteEnvelopeTexture(int $envelopeTextureId)
    {
        $envelopeTexture = $this->getEnvelopeTextureById($envelopeTextureId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeTextureId, $envelopeTexture]);

        $this->envelopeTextureRepository->deleteEnvelopeTexture(
            $this->envelopeTextureRepository->getById($envelopeTextureId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeTextureId, $envelopeTexture]);
    }

    public function getEnvelopeTextureById(int $id): EnvelopeTexture
    {
        return $this->envelopeTextureRepository->getById($id);
    }

    public function hasEnvelopeTextureWithId(int $envelopeTextureId): bool
    {
        return $this->envelopeTextureRepository->hasEnvelopeTextureWithId($envelopeTextureId);
    }

    public function getAll(): array
    {
        return $this->envelopeTextureRepository->getAll();
    }

    public function getAllPublic(): array
    {
        return $this->envelopeTextureRepository->getAllPublic();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopeTextureRepository->getByIds($idsCriteria);
    }
}