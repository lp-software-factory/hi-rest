<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

abstract class AbstractEnvelopeTextureParameters
{
    /** @var Attachment */
    private $preview;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    public function __construct(
        Attachment $preview,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description
    ) {
        $this->title = $title;
        $this->preview = $preview;
        $this->description = $description;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }
}