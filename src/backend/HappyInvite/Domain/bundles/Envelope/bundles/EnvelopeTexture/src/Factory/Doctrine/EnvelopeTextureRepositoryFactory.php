<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;

final class EnvelopeTextureRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EnvelopeTexture::class;
    }
}