<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;

final class EnvelopeTextureDefinitionRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EnvelopeTextureDefinition::class;
    }
}