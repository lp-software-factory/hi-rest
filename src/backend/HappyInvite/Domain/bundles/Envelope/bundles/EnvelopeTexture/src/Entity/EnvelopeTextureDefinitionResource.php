<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EnvelopeTextureDefinitionResource implements JSONSerializable
{
    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var int[] */
    private $attachmentIds;

    /** @var array */
    private $stamp;

    public function __construct(int $width, int $height, array $attachmentIds, array $stamp)
    {
        $this->width = $width;
        $this->height = $height;
        $this->attachmentIds = $attachmentIds;
        $this->stamp = $stamp;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'attachment_ids' => $this->getAttachmentIds(),
            'stamp' => $this->getStamp(),
        ];
    }

    public static function fromJSON(array $input): self
    {
        return new self(
            $input['width'],
            $input['height'],
            $input['attachment_ids'],
            $input['stamp']
        );
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getAttachmentIds(): array
    {
        return $this->attachmentIds;
    }

    public function getStamp(): array
    {
        return $this->stamp;
    }
}