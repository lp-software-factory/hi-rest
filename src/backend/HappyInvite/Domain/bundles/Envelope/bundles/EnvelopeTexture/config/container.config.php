<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository\EnvelopeTextureDefinitionRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Factory\Doctrine\EnvelopeTextureDefinitionRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Factory\Doctrine\EnvelopeTextureRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository\EnvelopeTextureRepository;

return [
    EnvelopeTextureRepository::class => factory(EnvelopeTextureRepositoryFactory::class),
    EnvelopeTextureDefinitionRepository::class => factory(EnvelopeTextureDefinitionRepositoryFactory::class),
];