<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Formatter;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;

final class EnvelopeTextureDefinitionFormatter
{
    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function formatOne(EnvelopeTextureDefinition $definition)
    {
        $json = $definition->toJSON();

        foreach($json['resources'] as &$resource) {
            $resource['attachments'] = [];

            foreach($resource['attachment_ids'] as $resourceId => $attachmentId) {
                $resource['attachments'][$resourceId] = $this->attachmentService->getById($attachmentId)->toJSON();
            }
        }

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $definitions)
    {
        return array_map(function (EnvelopeTextureDefinition $envelopeTextureDefinition) {
            return $this->formatOne($envelopeTextureDefinition);
        }, $definitions);
    }
}