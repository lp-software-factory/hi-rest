<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository\EnvelopeTextureRepository")
 * @Table(name="envelope_texture")
 */
class EnvelopeTexture implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, SerialEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, ActivatedEntityTrait, SerialEntityTrait, VersionEntityTrait;

    public const PREVIEW_WIDTH = 64;
    public const PREVIEW_HEIGHT = 64;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="preview_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $preview;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition", mappedBy="owner", cascade={"all"})
     * @var Collection
     */
    private $definitions;

    public function __construct(
        Attachment $preview,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->preview = $preview;
        $this->title = $title;
        $this->description = $description;
        $this->definitions = new ArrayCollection();

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'preview' => $this->getPreview()->toJSON($options),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'definitions' => $this->definitions->map(function(EnvelopeTextureDefinition $definition) {
                return $definition->toJSON();
            }),
        ];
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function setPreview(Attachment $preview): self
    {
        $this->preview = $preview;
        $this->markAsUpdated();

        return $this;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getDefinitions(): Collection
    {
        return $this->definitions;
    }
}