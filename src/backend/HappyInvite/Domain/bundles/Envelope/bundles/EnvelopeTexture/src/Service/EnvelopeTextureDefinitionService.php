<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository\EnvelopeTextureDefinitionRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition\CreateEnvelopeTextureDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition\EditEnvelopeTextureDefinitionParameters;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeTextureDefinitionService
{
    public const EVENT_CREATE = 'hi.domain.envelope.texture.definition.create';
    public const EVENT_EDIT = 'hi.domain.envelope.texture.definition.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.texture.definition.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.texture.definition.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.texture.definition.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.texture.definition.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.texture.definition.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.texture.definition.deactivate';

    /** @var EnvelopeTextureDefinitionRepository */
    private $envelopeTextureDefinitionRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        EnvelopeTextureDefinitionRepository $envelopeTextureDefinitionRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->envelopeTextureDefinitionRepository = $envelopeTextureDefinitionRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeTextureDefinition(CreateEnvelopeTextureDefinitionParameters $parameters): EnvelopeTextureDefinition
    {
        $envelopeTextureDefinition = new EnvelopeTextureDefinition(
            $parameters->getOwner(),
            $parameters->getTitle(),
            $parameters->getPreview()
        );

        $envelopeTextureDefinition->setResources($parameters->getResources());

        $serial = new SerialManager($this->getAllByEnvelopeTexture($envelopeTextureDefinition->getOwner()));
        $serial->insertLast($envelopeTextureDefinition);
        $serial->normalize();

        $this->envelopeTextureDefinitionRepository->createEnvelopeTextureDefinition($envelopeTextureDefinition);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeTextureDefinition]);

        return $envelopeTextureDefinition;
    }

    public function editEnvelopeTextureDefinition(int $envelopeTextureDefinitionId, EditEnvelopeTextureDefinitionParameters $parameters): EnvelopeTextureDefinition
    {
        $envelopeTextureDefinition = $this->envelopeTextureDefinitionRepository->getById($envelopeTextureDefinitionId);
        $envelopeTextureDefinition
            ->setTitle($parameters->getTitle())
            ->setOwner($parameters->getOwner())
            ->setResources($parameters->getResources())
            ->setPreview($parameters->getPreview());

        $this->envelopeTextureDefinitionRepository->saveEnvelopeTextureDefinition($envelopeTextureDefinition);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeTextureDefinition]);

        return $envelopeTextureDefinition;
    }

    public function moveUpEnvelopeTextureDefinition(int $envelopeTextureDefinitionId): int
    {
        $envelopeTextureDefinition = $this->getEnvelopeTextureDefinitionById($envelopeTextureDefinitionId);
        $siblings = $this->getAllByEnvelopeTexture($envelopeTextureDefinition->getOwner());

        $serial = new SerialManager($siblings);
        $serial->up($envelopeTextureDefinition);
        $serial->normalize();
        $this->envelopeTextureDefinitionRepository->saveEnvelopeTextureDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeTextureDefinition, $envelopeTextureDefinition->getPosition()]);

        return $envelopeTextureDefinition->getPosition();
    }

    public function moveDownEnvelopeTextureDefinition(int $envelopeTextureDefinitionId): int
    {
        $envelopeTextureDefinition = $this->getEnvelopeTextureDefinitionById($envelopeTextureDefinitionId);
        $siblings = $this->getAllByEnvelopeTexture($envelopeTextureDefinition->getOwner());

        $serial = new SerialManager($siblings);
        $serial->down($envelopeTextureDefinition);
        $serial->normalize();
        $this->envelopeTextureDefinitionRepository->saveEnvelopeTextureDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeTextureDefinition, $envelopeTextureDefinition->getPosition()]);

        return $envelopeTextureDefinition->getPosition();
    }

    public function activateEnvelopeTextureDefinition(int $envelopeTextureDefinitionId): EnvelopeTextureDefinition
    {
        $envelopeTextureDefinition = $this->getEnvelopeTextureDefinitionById($envelopeTextureDefinitionId);

        if(! $envelopeTextureDefinition->isActivated()) {
            $envelopeTextureDefinition->activate();

            $this->envelopeTextureDefinitionRepository->saveEnvelopeTextureDefinition($envelopeTextureDefinition);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeTextureDefinition]);
        }

        return $envelopeTextureDefinition;
    }

    public function deactivateEnvelopeTextureDefinition(int $envelopeTextureDefinitionId): EnvelopeTextureDefinition
    {
        $envelopeTextureDefinition = $this->getEnvelopeTextureDefinitionById($envelopeTextureDefinitionId);

        if($envelopeTextureDefinition->isActivated()) {
            $envelopeTextureDefinition->deactivate();

            $this->envelopeTextureDefinitionRepository->saveEnvelopeTextureDefinition($envelopeTextureDefinition);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeTextureDefinition]);
        }

        return $envelopeTextureDefinition;
    }

    public function deleteEnvelopeTextureDefinition(int $envelopeTextureDefinitionId)
    {
        $envelopeTextureDefinition = $this->getEnvelopeTextureDefinitionById($envelopeTextureDefinitionId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeTextureDefinitionId, $envelopeTextureDefinition]);

        $this->envelopeTextureDefinitionRepository->deleteEnvelopeTextureDefinition(
            $this->envelopeTextureDefinitionRepository->getById($envelopeTextureDefinitionId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeTextureDefinitionId, $envelopeTextureDefinition]);
    }

    public function getEnvelopeTextureDefinitionById(int $id): EnvelopeTextureDefinition
    {
        return $this->envelopeTextureDefinitionRepository->getById($id);
    }

    public function hasEnvelopeTextureDefinitionWithId(int $envelopeTextureDefinitionId): bool
    {
        return $this->envelopeTextureDefinitionRepository->hasEnvelopeTextureDefinitionWithId($envelopeTextureDefinitionId);
    }

    public function getAll(): array
    {
        return $this->envelopeTextureDefinitionRepository->getAll();
    }

    public function getAllByEnvelopeTexture(EnvelopeTexture $envelopeTexture): array
    {
        return $this->envelopeTextureDefinitionRepository->getAllOfEnvelopeTexture($envelopeTexture);
    }


    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopeTextureDefinitionRepository->getByIds($idsCriteria);
    }
}