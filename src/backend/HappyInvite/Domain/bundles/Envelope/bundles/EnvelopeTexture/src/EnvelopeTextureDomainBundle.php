<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture;

use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopeTextureDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}