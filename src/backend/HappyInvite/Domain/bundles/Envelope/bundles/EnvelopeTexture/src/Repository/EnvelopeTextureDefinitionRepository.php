<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Exceptions\EnvelopeTextureDefinitionNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EnvelopeTextureDefinitionRepository extends EntityRepository 
{
    public function createEnvelopeTextureDefinition(EnvelopeTextureDefinition $envelopeTextureDefinition): int
    {
        while($this->hasWithSID($envelopeTextureDefinition->getSID())) {
            $envelopeTextureDefinition->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeTextureDefinition);
        $this->getEntityManager()->flush($envelopeTextureDefinition);

        return $envelopeTextureDefinition->getId();
    }

    public function saveEnvelopeTextureDefinition(EnvelopeTextureDefinition $envelopeTextureDefinition)
    {
        $this->getEntityManager()->flush($envelopeTextureDefinition);
    }

    public function saveEnvelopeTextureDefinitionEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeTextureDefinition(EnvelopeTextureDefinition $envelopeTextureDefinition)
    {
        $this->getEntityManager()->remove($envelopeTextureDefinition);
        $this->getEntityManager()->flush($envelopeTextureDefinition);
    }

    public function getById(int $id): EnvelopeTextureDefinition
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeTextureDefinition) {
            return $result;
        }else{
            throw new EnvelopeTextureDefinitionNotFoundException(sprintf('EnvelopeTextureDefinition `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeTextureDefinitionNotFoundException(sprintf(
                'Envelope texture definitions with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeTextureDefinition $envelopeTextureDefinition) {
                    return $envelopeTextureDefinition->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllOfEnvelopeTexture(EnvelopeTexture $envelopeTexture): array
    {
        return $this->findBy([
            'owner' => $envelopeTexture,
        ], ['position' => 'asc']);
    }

    public function getAllPublic(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeTextureDefinitionWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeTextureDefinition
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeTextureDefinition) {
            return $result;
        }else{
            throw new EnvelopeTextureDefinitionNotFoundException(sprintf('EnvelopeTextureDefinition `SID(%s)` not found', $sid));
        }
    }
}