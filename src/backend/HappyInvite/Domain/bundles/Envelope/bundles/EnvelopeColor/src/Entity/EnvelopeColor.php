<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Repository\EnvelopeColorRepository")
 * @Table(name="envelope_color")
 */
class EnvelopeColor implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, SerialEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, ActivatedEntityTrait, SerialEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @Column(name="hex_code", type="string")
     * @var string
     */
    private $hexCode = [];

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        string $hexCode
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->hexCode = $hexCode;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'hex_code' => $this->getHexCode(),
        ];
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getHexCode(): string
    {
        return $this->hexCode;
    }

    public function setHexCode(string $hexCode)
    {
        $this->hexCode = $hexCode;
    }
}