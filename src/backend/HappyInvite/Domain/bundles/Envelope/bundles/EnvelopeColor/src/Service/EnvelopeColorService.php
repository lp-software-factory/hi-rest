<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Parameters\CreateEnvelopeColorParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Parameters\EditEnvelopeColorParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Repository\EnvelopeColorRepository;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeColorService
{
    public const EVENT_CREATE = 'hi.domain.envelope.color.create';
    public const EVENT_EDIT = 'hi.domain.envelope.color.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.color.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.color.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.color.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.color.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.color.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.color.deactivate';

    /** @var EnvelopeColorRepository */
    private $envelopeColorRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        EnvelopeColorRepository $envelopeColorRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->envelopeColorRepository = $envelopeColorRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeColor(CreateEnvelopeColorParameters $parameters): EnvelopeColor
    {
        $envelopeColor = new EnvelopeColor(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getHexCode()
        );

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($envelopeColor);
        $serial->normalize();

        $this->envelopeColorRepository->createEnvelopeColor($envelopeColor);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeColor]);

        return $envelopeColor;
    }

    public function editEnvelopeColor(int $envelopeColorId, EditEnvelopeColorParameters $parameters): EnvelopeColor
    {
        $envelopeColor = $this->envelopeColorRepository->getById($envelopeColorId);
        $envelopeColor
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setHexCode($parameters->getHexCode());

        $this->envelopeColorRepository->saveEnvelopeColor($envelopeColor);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeColor]);

        return $envelopeColor;
    }

    public function moveUpEnvelopeColor(int $envelopeColorId): int
    {
        $envelopeColor = $this->getEnvelopeColorById($envelopeColorId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($envelopeColor);
        $serial->normalize();
        $this->envelopeColorRepository->saveEnvelopeColorEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeColor, $envelopeColor->getPosition()]);

        return $envelopeColor->getPosition();
    }

    public function moveDownEnvelopeColor(int $envelopeColorId): int
    {
        $envelopeColor = $this->getEnvelopeColorById($envelopeColorId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($envelopeColor);
        $serial->normalize();
        $this->envelopeColorRepository->saveEnvelopeColorEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeColor, $envelopeColor->getPosition()]);

        return $envelopeColor->getPosition();
    }

    public function activateEnvelopeColor(int $envelopeColorId): EnvelopeColor
    {
        $envelopeColor = $this->getEnvelopeColorById($envelopeColorId);

        if(! $envelopeColor->isActivated()) {
            $envelopeColor->activate();

            $this->envelopeColorRepository->saveEnvelopeColor($envelopeColor);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeColor]);
        }

        return $envelopeColor;
    }

    public function deactivateEnvelopeColor(int $envelopeColorId): EnvelopeColor
    {
        $envelopeColor = $this->getEnvelopeColorById($envelopeColorId);

        if($envelopeColor->isActivated()) {
            $envelopeColor->deactivate();

            $this->envelopeColorRepository->saveEnvelopeColor($envelopeColor);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeColor]);
        }

        return $envelopeColor;
    }

    public function deleteEnvelopeColor(int $envelopeColorId)
    {
        $envelopeColor = $this->getEnvelopeColorById($envelopeColorId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeColorId, $envelopeColor]);

        $this->envelopeColorRepository->deleteEnvelopeColor(
            $this->envelopeColorRepository->getById($envelopeColorId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeColorId, $envelopeColor]);
    }

    public function getEnvelopeColorById(int $id): EnvelopeColor
    {
        return $this->envelopeColorRepository->getById($id);
    }

    public function hasEnvelopeColorWithId(int $envelopeColorId): bool
    {
        return $this->envelopeColorRepository->hasEnvelopeColorWithId($envelopeColorId);
    }

    public function getAll(): array
    {
        return $this->envelopeColorRepository->getAll();
    }

    public function getAllPublic(): array
    {
        return $this->envelopeColorRepository->getAllPublic();
    }
}