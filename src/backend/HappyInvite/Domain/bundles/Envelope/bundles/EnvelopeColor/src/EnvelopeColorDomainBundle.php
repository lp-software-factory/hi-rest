<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor;

use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopeColorDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}