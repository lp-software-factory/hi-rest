<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;

final class EnvelopeColorFormatter
{
    public function formatOne(EnvelopeColor $entity): array
    {
        $json = $entity->toJSON();

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopeColor $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}