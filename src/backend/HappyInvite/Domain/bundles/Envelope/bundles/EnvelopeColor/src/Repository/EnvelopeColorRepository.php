<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Exceptions\EnvelopeColorNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;

final class EnvelopeColorRepository extends EntityRepository
{
    public function createEnvelopeColor(EnvelopeColor $envelopeColor): int
    {
        while($this->hasWithSID($envelopeColor->getSID())) {
            $envelopeColor->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeColor);
        $this->getEntityManager()->flush($envelopeColor);

        return $envelopeColor->getId();
    }

    public function saveEnvelopeColor(EnvelopeColor $envelopeColor)
    {
        $this->getEntityManager()->flush($envelopeColor);
    }

    public function saveEnvelopeColorEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeColor(EnvelopeColor $envelopeColor)
    {
        $this->getEntityManager()->remove($envelopeColor);
        $this->getEntityManager()->flush($envelopeColor);
    }

    public function getById(int $id): EnvelopeColor
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeColor) {
            return $result;
        }else{
            throw new EnvelopeColorNotFoundException(sprintf('EnvelopeColor `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeColorNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeColor $envelopeColor) {
                    return $envelopeColor->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllPublic(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeColorWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeColor
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeColor) {
            return $result;
        }else{
            throw new EnvelopeColorNotFoundException(sprintf('EnvelopeColor `SID(%s)` not found', $sid));
        }
    }
}