<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Factory\Doctrine\EnvelopeColorRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Repository\EnvelopeColorRepository;

return [
    EnvelopeColorRepository::class => factory(EnvelopeColorRepositoryFactory::class),
];