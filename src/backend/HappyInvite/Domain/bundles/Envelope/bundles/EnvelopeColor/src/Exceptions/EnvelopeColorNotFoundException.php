<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Exceptions;

final class EnvelopeColorNotFoundException extends \Exception {}