<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;

final class EnvelopeColorRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EnvelopeColor::class;
    }
}