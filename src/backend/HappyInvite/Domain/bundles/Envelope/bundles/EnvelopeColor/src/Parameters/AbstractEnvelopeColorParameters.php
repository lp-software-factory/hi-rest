<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

abstract class AbstractEnvelopeColorParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var string */
    private $hexCode;

    public function __construct(ImmutableLocalizedString $title, ImmutableLocalizedString $description, string $hexCode)
    {
        $this->title = $title;
        $this->description = $description;
        $this->hexCode = $hexCode;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getHexCode(): string
    {
        return $this->hexCode;
    }
}