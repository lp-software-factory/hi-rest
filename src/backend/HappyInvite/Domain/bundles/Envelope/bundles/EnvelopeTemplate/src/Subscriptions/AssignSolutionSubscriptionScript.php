<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Subscriptions;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service\EnvelopeTemplateService;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AssignSolutionSubscriptionScript implements SubscriptionScript
{
    /** @var SolutionService */
    private $solutionService;

    /** @var EnvelopeTemplateService */
    private $envelopeTemplateService;

    public function __construct(
        SolutionService $solutionService,
        EnvelopeTemplateService $envelopeTemplateService
    ) {
        $this->solutionService = $solutionService;
        $this->envelopeTemplateService = $envelopeTemplateService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_ASSIGNED_TO, function(Solution $solution) {
            /** @var EnvelopeTemplate $envelopeTemplate */
            foreach($solution->getEnvelopeTemplates() as $envelopeTemplate) {
                $this->envelopeTemplateService->setOwner($envelopeTemplate->getId(), $solution->getTarget());
            }
        });

        $this->envelopeTemplateService->getEventEmitter()->on(EnvelopeTemplateService::EVENT_EDITED, function(EnvelopeTemplate $envelopeTemplate) {
            if($envelopeTemplate->hasSolutionComponentOwner()) {
                $this->envelopeTemplateService->markAsUserSpecified($envelopeTemplate->getId(), $envelopeTemplate->getSolutionComponentOwner());
            }
        });
    }
}