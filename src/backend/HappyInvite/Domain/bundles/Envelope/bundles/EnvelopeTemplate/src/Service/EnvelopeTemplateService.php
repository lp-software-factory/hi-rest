<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters\CreateEnvelopeTemplateParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters\EditEnvelopeTemplateParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Repository\EnvelopeTemplateRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EnvelopeTemplateService
{
    public const EVENT_CREATED = 'hi.domain.envelope-template.template.created';
    public const EVENT_EDITED = 'hi.domain.envelope-template.template.edited';
    public const EVENT_USER_SPECIFIED = 'hi.domain.envelope-template.template.solution.user-specified';
    public const EVENT_OWNER_SPECIFIED = 'hi.domain.envelope-template.template.solution.owner-specified';
    public const EVENT_UPDATED = 'hi.domain.envelope-template.template.updated';
    public const EVENT_DELETE = 'hi.domain.envelope-template.template.delete.before';
    public const EVENT_DELETED = 'hi.domain.envelope-template.template.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EnvelopeTemplateRepository */
    private $templateRepository;

    public function __construct(EnvelopeTemplateRepository $envelopeTemplateRepository)
    {
        $this->templateRepository = $envelopeTemplateRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeTemplate(CreateEnvelopeTemplateParameters $parameters): EnvelopeTemplate
    {
        $template = new EnvelopeTemplate(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getEnvelopeBackdropDefinition(),
            $parameters->getEnvelopeTextureDefinition(),
            $parameters->getEnvelopePatternDefinition(),
            $parameters->getEnvelopeMarkDefinition(),

            $parameters->getConfig()
        );

        $this->templateRepository->createTemplate($template);

        $this->eventEmitter->emit(self::EVENT_CREATED, [$template]);

        return $template;
    }

    public function duplicateEnvelopeTemplate(EnvelopeTemplate $input): EnvelopeTemplate
    {
        return $this->createEnvelopeTemplate(new CreateEnvelopeTemplateParameters(
            $input->getTitle(),
            $input->getDescription(),
            $input->getEnvelopeBackdropDefinition(),
            $input->getEnvelopeTextureDefinition(),
            $input->getEnvelopePatternDefinition(),
            $input->getEnvelopeMarkDefinition(),
            $input->getConfig()
        ));
    }

    public function editEnvelopeTemplate(int $id, EditEnvelopeTemplateParameters $parameters): EnvelopeTemplate
    {
        $template = $this->templateRepository->getById($id);

        $template
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setEnvelopeBackdropDefinition($parameters->getEnvelopeBackdropDefinition())
            ->setEnvelopeTextureDefinition($parameters->getEnvelopeTextureDefinition())
            ->setEnvelopePatternDefinition($parameters->getEnvelopePatternDefinition())
            ->setEnvelopeMarkDefinition($parameters->getEnvelopeMarkDefinition())
            ->setConfig($parameters->getConfig());

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        return $template;
    }

    public function setOwner(int $id, Profile $newOwner): void
    {
        $template = $this->templateRepository->getById($id);
        $template->setSolutionComponentOwner($newOwner);

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_OWNER_SPECIFIED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        $this->templateRepository->saveTemplate($template);
    }
    
    public function markAsUserSpecified(int $id, Profile $newOwner)
    {
        $template = $this->templateRepository->getById($id);
        $template->setSolutionComponentOwner($newOwner);
        $template->markSolutionAsUserSpecified();

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_USER_SPECIFIED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        $this->templateRepository->saveTemplate($template);
    }

    public function deleteEnvelopeTemplate(int $id)
    {
        $template = $this->getById($id);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$template]);
        $this->templateRepository->deleteTemplate($template);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$id, $template]);
    }

    public function getById(int $id): EnvelopeTemplate
    {
        return $this->templateRepository->getById($id);
    }

    public function getByIds(IdsCriteria $ids): array
    {
        return $this->templateRepository->getByIds($ids);
    }

    public function getAll(): array
    {
        return $this->templateRepository->getAll();
    }
}