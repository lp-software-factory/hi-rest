<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarkDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters\CreateEnvelopeTemplateParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureDefinitionService;

final class EnvelopeTemplatePresetService
{
    /** @var EnvelopeTemplateService */
    private $envelopeTemplateService;

    /** @var EnvelopeBackdropDefinitionService */
    private $envelopeBackdropDefinitionService;

    /** @var EnvelopeTextureDefinitionService */
    private $envelopeTextureDefinitionService;

    /** @var EnvelopePatternDefinitionService */
    private $envelopePatternDefinitionService;

    /** @var EnvelopeMarkDefinitionService */
    private $envelopeMarkDefinitionService;

    public function __construct(
        EnvelopeTemplateService $envelopeTemplateService,
        EnvelopeBackdropDefinitionService $envelopeBackdropDefinitionService,
        EnvelopeTextureDefinitionService $envelopeTextureDefinitionService,
        EnvelopePatternDefinitionService $envelopePatternDefinitionService,
        EnvelopeMarkDefinitionService $envelopeMarkDefinitionService
    ) {
        $this->envelopeTemplateService = $envelopeTemplateService;
        $this->envelopeBackdropDefinitionService = $envelopeBackdropDefinitionService;
        $this->envelopeTextureDefinitionService = $envelopeTextureDefinitionService;
        $this->envelopePatternDefinitionService = $envelopePatternDefinitionService;
        $this->envelopeMarkDefinitionService = $envelopeMarkDefinitionService;
    }

    public function createRandomTemplate(): EnvelopeTemplate
    {
        $allBackdropDefinitions = $this->envelopeBackdropDefinitionService->getAll();
        $allTextureDefinitions = $this->envelopeTextureDefinitionService->getAll();
        $allPatternDefinitions = $this->envelopePatternDefinitionService->getAll();
        $allMarkDefinitions = $this->envelopeMarkDefinitionService->getAll();

        return $this->envelopeTemplateService->createEnvelopeTemplate(new CreateEnvelopeTemplateParameters(
            new ImmutableLocalizedString([]),
            new ImmutableLocalizedString([]),
            $allBackdropDefinitions[array_rand($allBackdropDefinitions)],
            $allTextureDefinitions[array_rand($allTextureDefinitions)],
            $allPatternDefinitions[array_rand($allPatternDefinitions)],
            $allMarkDefinitions[array_rand($allMarkDefinitions)],
            []
        ));
    }
}