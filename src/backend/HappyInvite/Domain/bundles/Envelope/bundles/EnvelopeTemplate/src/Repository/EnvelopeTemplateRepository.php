<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Exceptions\EnvelopeTemplateNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EnvelopeTemplateRepository extends EntityRepository
{
    public function createTemplate(EnvelopeTemplate $template): int
    {
        while ($this->hasWithSID($template->getSID())) {
            $template->regenerateSID();
        }

        $this->getEntityManager()->persist($template);
        $this->getEntityManager()->flush($template);

        return $template->getId();
    }

    public function saveTemplate(EnvelopeTemplate $template)
    {
        $this->getEntityManager()->flush($template);
    }

    public function saveTemplateEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveTemplate($entity);
        }
    }

    public function deleteTemplate(EnvelopeTemplate $template)
    {
        $this->getEntityManager()->remove($template);
        $this->getEntityManager()->flush($template);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EnvelopeTemplate
    {
        $result = $this->find($id);

        if ($result instanceof EnvelopeTemplate) {
            return $result;
        } else {
            throw new EnvelopeTemplateNotFoundException(sprintf('Envelope template `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Envelope templates with IDs (%s) not found', array_diff($ids, array_map(function (EnvelopeTemplate $template) {
                return $template->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EnvelopeTemplate
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EnvelopeTemplate) {
            return $result;
        } else {
            throw new EnvelopeTemplateNotFoundException(sprintf('Envelope template `SID(%s)` not found', $sid));
        }
    }
}