<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Subscriptions;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service\EnvelopeTemplateService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class SolutionDuplicateSubscription implements SubscriptionScript
{
    /** @var EnvelopeTemplateService */
    private $envelopeTemplateService;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(EnvelopeTemplateService $envelopeTemplateService, SolutionService $solutionService)
    {
        $this->envelopeTemplateService = $envelopeTemplateService;
        $this->solutionService = $solutionService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_BEFORE_DUPLICATE, function(Solution $origSolution, Solution $copySolution, Profile $owner = null) {
            $copySolution->setEnvelopeTemplates(array_map(function(EnvelopeTemplate $input) use ($owner) {
                $result = $this->envelopeTemplateService->duplicateEnvelopeTemplate($input);

                if($owner) {
                    $this->envelopeTemplateService->setOwner($result->getId(), $owner);
                }

                return $result;
            }, $origSolution->getEnvelopeTemplates()));
        });
    }
}