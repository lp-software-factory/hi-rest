<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntity;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Repository\EnvelopeTemplateRepository")
 * @Table(name="envelope_template")
 */
class EnvelopeTemplate implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity, SolutionComponentEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait, SolutionComponentEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition")
     * @JoinColumn(name="envelope_backdrop_definition_id", referencedColumnName="id")
     * @var EnvelopeBackdropDefinition
     */
    private $envelopeBackdropDefinition;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition")
         * @JoinColumn(name="envelope_mark_definition_id", referencedColumnName="id")
     * @var EnvelopeMarkDefinition
     */
    private $envelopeMarkDefinition;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition")
     * @JoinColumn(name="envelope_pattern_definition_id", referencedColumnName="id")
     * @var EnvelopePatternDefinition
     */
    private $envelopePatternDefinition;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition")
     * @JoinColumn(name="envelope_texture_definition_id", referencedColumnName="id")
     * @var EnvelopeTextureDefinition
     */
    private $envelopeTextureDefinition;

    /**
     * @Column(name="config", type="json_array")
     * @var array
     */
    private $config = [];

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        EnvelopeBackdropDefinition $envelopeBackdropDefinition,
        EnvelopeTextureDefinition $envelopeTextureDefinition,
        EnvelopePatternDefinition $envelopePatternDefinition,
        EnvelopeMarkDefinition $envelopeMarkDefinition,
        array $config = []
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->envelopeBackdropDefinition = $envelopeBackdropDefinition;
        $this->envelopeMarkDefinition = $envelopeMarkDefinition;
        $this->envelopePatternDefinition = $envelopePatternDefinition;
        $this->envelopeTextureDefinition = $envelopeTextureDefinition;
        $this->config = $config;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'solution' => [
                'is_user_specified' => $this->isSolutionUserSpecified(),
                'owner' => [
                    'has' => $this->hasSolutionComponentOwner(),
                ]
            ],
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'envelope_backdrop_definition' => $this->getEnvelopeBackdropDefinition()->toJSON($options),
            'envelope_marks_definition' => $this->getEnvelopeMarkDefinition()->toJSON($options),
            'envelope_pattern_definition' => $this->getEnvelopePatternDefinition()->toJSON($options),
            'envelope_texture_definition' => $this->getEnvelopeTextureDefinition()->toJSON($options),
            'config' => $this->getConfig(),
        ];

        if($this->hasSolutionComponentOwner()) {
            $json['solution']['owner']['profile'] = $this->getSolutionComponentOwner()->toJSON($options);
        }

        return $json;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getEnvelopeBackdropDefinition(): EnvelopeBackdropDefinition
    {
        return $this->envelopeBackdropDefinition;
    }

    public function setEnvelopeBackdropDefinition(EnvelopeBackdropDefinition $envelopeBackdropDefinition): self
    {
        $this->envelopeBackdropDefinition = $envelopeBackdropDefinition;
        $this->markAsUpdated();

        return $this;
    }

    public function getEnvelopeMarkDefinition(): EnvelopeMarkDefinition
    {
        return $this->envelopeMarkDefinition;
    }

    public function setEnvelopeMarkDefinition(EnvelopeMarkDefinition $envelopeMarkDefinition): self
    {
        $this->envelopeMarkDefinition = $envelopeMarkDefinition;
        $this->markAsUpdated();

        return $this;
    }

    public function getEnvelopePatternDefinition(): EnvelopePatternDefinition
    {
        return $this->envelopePatternDefinition;
    }

    public function setEnvelopePatternDefinition(EnvelopePatternDefinition $envelopePatternDefinition): self
    {
        $this->envelopePatternDefinition = $envelopePatternDefinition;
        $this->markAsUpdated();

        return $this;
    }

    public function getEnvelopeTextureDefinition(): EnvelopeTextureDefinition
    {
        return $this->envelopeTextureDefinition;
    }

    public function setEnvelopeTextureDefinition(EnvelopeTextureDefinition $envelopeTextureDefinition): self
    {
        $this->envelopeTextureDefinition = $envelopeTextureDefinition;
        $this->markAsUpdated();

        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): self
    {
        $this->config = $config;
        $this->markAsUpdated();

        return $this;
    }
}