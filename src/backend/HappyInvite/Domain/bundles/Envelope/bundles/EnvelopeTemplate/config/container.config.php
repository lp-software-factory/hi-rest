<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Factory\Doctrine\EnvelopeTemplateDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Repository\EnvelopeTemplateRepository;

return [
    EnvelopeTemplateRepository::class => factory(EnvelopeTemplateDoctrineRepositoryFactory::class),
];