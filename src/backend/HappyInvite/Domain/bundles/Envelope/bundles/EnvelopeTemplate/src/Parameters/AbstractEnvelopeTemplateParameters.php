<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTextureDefinition;

abstract class AbstractEnvelopeTemplateParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var EnvelopeBackdropDefinition */
    private $envelopeBackdropDefinition;

    /** @var EnvelopeTextureDefinition */
    private $envelopeTextureDefinition;

    /** @var EnvelopePatternDefinition */
    private $envelopePatternDefinition;

    /** @var EnvelopeMarkDefinition */
    private $envelopeMarkDefinition;

    /** @var array */
    private $config;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        EnvelopeBackdropDefinition $envelopeBackdropDefinition,
        EnvelopeTextureDefinition $envelopeTextureDefinition,
        EnvelopePatternDefinition $envelopePatternDefinition,
        EnvelopeMarkDefinition $envelopeMarkDefinition,
        array $config
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->envelopeBackdropDefinition = $envelopeBackdropDefinition;
        $this->envelopeTextureDefinition = $envelopeTextureDefinition;
        $this->envelopePatternDefinition = $envelopePatternDefinition;
        $this->envelopeMarkDefinition = $envelopeMarkDefinition;
        $this->config = $config;
    }


    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getEnvelopeBackdropDefinition(): EnvelopeBackdropDefinition
    {
        return $this->envelopeBackdropDefinition;
    }

    public function getEnvelopeTextureDefinition(): EnvelopeTextureDefinition
    {
        return $this->envelopeTextureDefinition;
    }

    public function getEnvelopePatternDefinition(): EnvelopePatternDefinition
    {
        return $this->envelopePatternDefinition;
    }

    public function getEnvelopeMarkDefinition(): EnvelopeMarkDefinition
    {
        return $this->envelopeMarkDefinition;
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}