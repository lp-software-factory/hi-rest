<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Exceptions;

final class EnvelopeTemplateNotFoundException extends \Exception {}