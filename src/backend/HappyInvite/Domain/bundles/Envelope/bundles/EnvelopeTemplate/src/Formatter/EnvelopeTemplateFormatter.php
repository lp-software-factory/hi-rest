<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Formatter\EnvelopeBackdropDefinitionFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Formatter\EnvelopeMarkDefinitionFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Formatter\EnvelopePatternDefinitionFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Formatter\EnvelopeTextureDefinitionFormatter;

final class EnvelopeTemplateFormatter
{
    public const OPTIONS_USE_IDS = 'options_use_ids';

    /** @var EnvelopeBackdropDefinitionFormatter */
    private $envelopeBackdropDefinitionFormatter;

    /** @var EnvelopeMarkDefinitionFormatter */
    private $envelopeMarkDefinitionFormatter;

    /** @var EnvelopePatternDefinitionFormatter */
    private $envelopePatternDefinitionFormatter;

    /** @var EnvelopeTextureDefinitionFormatter */
    private $envelopeTextureDefinitionFormatter;

    public function __construct(
        EnvelopeBackdropDefinitionFormatter $envelopeBackdropDefinitionFormatter,
        EnvelopeMarkDefinitionFormatter $envelopeMarkDefinitionFormatter,
        EnvelopePatternDefinitionFormatter $envelopePatternDefinitionFormatter,
        EnvelopeTextureDefinitionFormatter $envelopeTextureDefinitionFormatter
    ) {
        $this->envelopeBackdropDefinitionFormatter = $envelopeBackdropDefinitionFormatter;
        $this->envelopeMarkDefinitionFormatter = $envelopeMarkDefinitionFormatter;
        $this->envelopePatternDefinitionFormatter = $envelopePatternDefinitionFormatter;
        $this->envelopeTextureDefinitionFormatter = $envelopeTextureDefinitionFormatter;
    }

    public function formatOne(EnvelopeTemplate $entity, array $options = []): array
    {
        $options = array_merge([
            self::OPTIONS_USE_IDS => false,
        ], $options);

        $json = $entity->toJSON();

        unset($json['envelope_pattern']);
        unset($json['envelope_texture']);
        unset($json['envelope_backdrop']);
        unset($json['envelope_marks']);
        unset($json['envelope_color']);

        if($options[self::OPTIONS_USE_IDS]) {
            $json['envelope_pattern_definition_id'] = $entity->getEnvelopePatternDefinition()->getId();
            $json['envelope_texture_definition_id'] = $entity->getEnvelopeTextureDefinition()->getId();
            $json['envelope_backdrop_definition_id'] = $entity->getEnvelopeBackdropDefinition()->getId();
            $json['envelope_mark_definition_id'] = $entity->getEnvelopeMarkDefinition()->getId();
        }else{
            $json['envelope_pattern_definition'] = $this->envelopePatternDefinitionFormatter->formatOne($entity->getEnvelopePatternDefinition());
            $json['envelope_texture_definition'] = $this->envelopeTextureDefinitionFormatter->formatOne($entity->getEnvelopeTextureDefinition());
            $json['envelope_backdrop_definition'] = $this->envelopeBackdropDefinitionFormatter->formatOne($entity->getEnvelopeBackdropDefinition());
            $json['envelope_mark_definition'] = $this->envelopeMarkDefinitionFormatter->formatOne($entity->getEnvelopeMarkDefinition());
        }

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities, array $options = []): array
    {
        return array_map(function(EnvelopeTemplate $entity) use ($options) {
            return $this->formatOne($entity, $options);
        }, $entities);
    }
}