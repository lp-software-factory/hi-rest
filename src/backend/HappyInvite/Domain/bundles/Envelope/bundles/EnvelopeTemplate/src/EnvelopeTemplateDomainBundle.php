<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Subscriptions\AssignSolutionSubscriptionScript;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Subscriptions\SolutionDuplicateSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopeTemplateDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AssignSolutionSubscriptionScript::class,
            SolutionDuplicateSubscription::class,
        ];
    }
}