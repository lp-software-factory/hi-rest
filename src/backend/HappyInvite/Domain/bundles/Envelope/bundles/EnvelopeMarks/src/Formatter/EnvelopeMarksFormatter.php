<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;

final class EnvelopeMarksFormatter
{
    /** @var EnvelopeMarkDefinitionFormatter */
    private $envelopeMarkDefinitionFormatter;

    public function __construct(EnvelopeMarkDefinitionFormatter $envelopeMarkDefinitionFormatter)
    {
        $this->envelopeMarkDefinitionFormatter = $envelopeMarkDefinitionFormatter;
    }

    public function formatOne(EnvelopeMarks $entity): array
    {
        $json = $entity->toJSON();
        $json['definitions'] = $this->envelopeMarkDefinitionFormatter->formatMany($entity->getDefinitions()->toArray());

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopeMarks $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}