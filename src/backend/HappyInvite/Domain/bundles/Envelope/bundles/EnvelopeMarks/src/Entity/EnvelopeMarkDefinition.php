<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository\EnvelopeMarkDefinitionRepository")
 * @Table(name="envelope_mark_definition")
 */
class EnvelopeMarkDefinition implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, SerialEntity, VersionEntity, ActivatedEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait, VersionEntityTrait, ModificationEntityTrait, ActivatedEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks")
     * @JoinColumn(name="owner_envelope_marks_id", referencedColumnName="id")
     * @var EnvelopeMarks
     */
    private $owner;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $attachment;

    public function __construct(
        EnvelopeMarks $owner,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Attachment $attachment
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->owner = $owner;
        $this->title = $title;
        $this->description = $description;
        $this->attachment = $attachment;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'owner_envelope_marks_id' => $this->getOwner()->getId(),
            'attachment' => $this->getAttachment()->toJSON($options),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }
    public function getOwner(): EnvelopeMarks
    {
        return $this->owner;
    }

    public function setOwner(EnvelopeMarks $owner): self
    {
        $this->owner = $owner;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getAttachment(): Attachment
    {
        return $this->attachment;
    }

    public function setAttachment(Attachment $attachment): self
    {
        $this->attachment = $attachment;
        $this->markAsUpdated();

        return $this;
    }
}