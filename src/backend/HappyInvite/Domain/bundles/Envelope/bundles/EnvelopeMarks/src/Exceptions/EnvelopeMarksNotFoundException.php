<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions;

final class EnvelopeMarksNotFoundException extends \Exception {}