<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;

abstract class AbstractEnvelopeMarkDefinitionParameters
{
    /** @var EnvelopeMarks */
    private $owner;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var Attachment */
    private $attachment;

    public function __construct(
        EnvelopeMarks $owner,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Attachment $attachment
    ) {
        $this->owner = $owner;
        $this->title = $title;
        $this->description = $description;
        $this->attachment = $attachment;
    }

    public function getOwner(): EnvelopeMarks
    {
        return $this->owner;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getAttachment(): Attachment
    {
        return $this->attachment;
    }
}