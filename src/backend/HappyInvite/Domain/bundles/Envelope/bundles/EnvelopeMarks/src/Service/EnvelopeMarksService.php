<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\CreateEnvelopeMarksParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\EditEnvelopeMarksParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository\EnvelopeMarksRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeMarksService
{
    public const EVENT_CREATE = 'hi.domain.envelope.marks.create';
    public const EVENT_EDIT = 'hi.domain.envelope.marks.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.marks.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.marks.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.marks.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.marks.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.marks.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.marks.deactivate';

    /** @var EnvelopeMarksRepository */
    private $envelopeMarksRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        EnvelopeMarksRepository $envelopeMarksRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->envelopeMarksRepository = $envelopeMarksRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeMarks(CreateEnvelopeMarksParameters $parameters): EnvelopeMarks
    {
        $envelopeMarks = new EnvelopeMarks(
            $parameters->getTitle(),
            $parameters->getDescription()
        );

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($envelopeMarks);
        $serial->normalize();

        $this->envelopeMarksRepository->createEnvelopeMarks($envelopeMarks);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeMarks]);

        return $envelopeMarks;
    }

    public function editEnvelopeMarks(int $envelopeMarksId, EditEnvelopeMarksParameters $parameters): EnvelopeMarks
    {
        $envelopeMarks = $this->envelopeMarksRepository->getById($envelopeMarksId);
        $envelopeMarks
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription());

        $this->envelopeMarksRepository->saveEnvelopeMarks($envelopeMarks);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeMarks]);

        return $envelopeMarks;
    }

    public function moveUpEnvelopeMarks(int $envelopeMarksId): int
    {
        $envelopeMarks = $this->getEnvelopeMarksById($envelopeMarksId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($envelopeMarks);
        $serial->normalize();
        $this->envelopeMarksRepository->saveEnvelopeMarksEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeMarks, $envelopeMarks->getPosition()]);

        return $envelopeMarks->getPosition();
    }

    public function moveDownEnvelopeMarks(int $envelopeMarksId): int
    {
        $envelopeMarks = $this->getEnvelopeMarksById($envelopeMarksId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($envelopeMarks);
        $serial->normalize();
        $this->envelopeMarksRepository->saveEnvelopeMarksEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeMarks, $envelopeMarks->getPosition()]);

        return $envelopeMarks->getPosition();
    }

    public function activateEnvelopeMarks(int $envelopeMarksId): EnvelopeMarks
    {
        $envelopeMarks = $this->getEnvelopeMarksById($envelopeMarksId);

        if(! $envelopeMarks->isActivated()) {
            $envelopeMarks->activate();

            $this->envelopeMarksRepository->saveEnvelopeMarks($envelopeMarks);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeMarks]);
        }

        return $envelopeMarks;
    }

    public function deactivateEnvelopeMarks(int $envelopeMarksId): EnvelopeMarks
    {
        $envelopeMarks = $this->getEnvelopeMarksById($envelopeMarksId);

        if($envelopeMarks->isActivated()) {
            $envelopeMarks->deactivate();

            $this->envelopeMarksRepository->saveEnvelopeMarks($envelopeMarks);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeMarks]);
        }

        return $envelopeMarks;
    }

    public function deleteEnvelopeMarks(int $envelopeMarksId)
    {
        $envelopeMarks = $this->getEnvelopeMarksById($envelopeMarksId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeMarksId, $envelopeMarks]);

        $this->envelopeMarksRepository->deleteEnvelopeMarks(
            $this->envelopeMarksRepository->getById($envelopeMarksId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeMarksId, $envelopeMarks]);
    }

    public function getEnvelopeMarksById(int $id): EnvelopeMarks
    {
        return $this->envelopeMarksRepository->getById($id);
    }

    public function hasEnvelopeMarksWithId(int $envelopeMarksId): bool
    {
        return $this->envelopeMarksRepository->hasEnvelopeMarksWithId($envelopeMarksId);
    }

    public function getAll(): array
    {
        return $this->envelopeMarksRepository->getAll();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopeMarksRepository->getByIds($idsCriteria);
    }
}