<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository\EnvelopeMarkDefinitionRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition\CreateEnvelopeMarkDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition\EditEnvelopeMarkDefinitionParameters;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeMarkDefinitionService
{
    public const EVENT_CREATE = 'hi.domain.envelope.mark.definition.create';
    public const EVENT_EDIT = 'hi.domain.envelope.mark.definition.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.mark.definition.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.mark.definition.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.mark.definition.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.mark.definition.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.mark.definition.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.mark.definition.deactivate';

    /** @var EnvelopeMarkDefinitionRepository */
    private $envelopeMarkDefinitionRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        EnvelopeMarkDefinitionRepository $envelopeMarkDefinitionRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->envelopeMarkDefinitionRepository = $envelopeMarkDefinitionRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeMarkDefinition(CreateEnvelopeMarkDefinitionParameters $parameters): EnvelopeMarkDefinition
    {
        $envelopeMarkDefinition = new EnvelopeMarkDefinition(
            $parameters->getOwner(),
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getAttachment()
        );

        $serial = new SerialManager($this->getAllOfCollection($envelopeMarkDefinition->getOwner()));
        $serial->insertLast($envelopeMarkDefinition);
        $serial->normalize();

        $this->envelopeMarkDefinitionRepository->createEnvelopeMarkDefinition($envelopeMarkDefinition);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeMarkDefinition]);

        return $envelopeMarkDefinition;
    }

    public function editEnvelopeMarkDefinition(int $envelopeMarkDefinitionId, EditEnvelopeMarkDefinitionParameters $parameters): EnvelopeMarkDefinition
    {
        $envelopeMarkDefinition = $this->envelopeMarkDefinitionRepository->getById($envelopeMarkDefinitionId);
        $envelopeMarkDefinition
            ->setOwner($parameters->getOwner())
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setAttachment($parameters->getAttachment());

        $this->envelopeMarkDefinitionRepository->saveEnvelopeMarkDefinition($envelopeMarkDefinition);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeMarkDefinition]);

        return $envelopeMarkDefinition;
    }

    public function moveUpEnvelopeMarkDefinition(int $envelopeMarkDefinitionId): int
    {
        $envelopeMarkDefinition = $this->getEnvelopeMarkDefinitionById($envelopeMarkDefinitionId);
        $siblings = $this->getAllOfCollection($envelopeMarkDefinition->getOwner());

        $serial = new SerialManager($siblings);
        $serial->up($envelopeMarkDefinition);
        $serial->normalize();
        $this->envelopeMarkDefinitionRepository->saveEnvelopeMarkDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeMarkDefinition, $envelopeMarkDefinition->getPosition()]);

        return $envelopeMarkDefinition->getPosition();
    }

    public function moveDownEnvelopeMarkDefinition(int $envelopeMarkDefinitionId): int
    {
        $envelopeMarkDefinition = $this->getEnvelopeMarkDefinitionById($envelopeMarkDefinitionId);
        $siblings = $this->getAllOfCollection($envelopeMarkDefinition->getOwner());

        $serial = new SerialManager($siblings);
        $serial->down($envelopeMarkDefinition);
        $serial->normalize();
        $this->envelopeMarkDefinitionRepository->saveEnvelopeMarkDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeMarkDefinition, $envelopeMarkDefinition->getPosition()]);

        return $envelopeMarkDefinition->getPosition();
    }

    public function activateEnvelopeMarkDefinition(int $envelopeMarkDefinitionId): EnvelopeMarkDefinition
    {
        $envelopeMarkDefinition = $this->getEnvelopeMarkDefinitionById($envelopeMarkDefinitionId);

        if(! $envelopeMarkDefinition->isActivated()) {
            $envelopeMarkDefinition->activate();

            $this->envelopeMarkDefinitionRepository->saveEnvelopeMarkDefinition($envelopeMarkDefinition);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeMarkDefinition]);
        }

        return $envelopeMarkDefinition;
    }

    public function deactivateEnvelopeMarkDefinition(int $envelopeMarkDefinitionId): EnvelopeMarkDefinition
    {
        $envelopeMarkDefinition = $this->getEnvelopeMarkDefinitionById($envelopeMarkDefinitionId);

        if($envelopeMarkDefinition->isActivated()) {
            $envelopeMarkDefinition->deactivate();

            $this->envelopeMarkDefinitionRepository->saveEnvelopeMarkDefinition($envelopeMarkDefinition);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeMarkDefinition]);
        }

        return $envelopeMarkDefinition;
    }

    public function deleteEnvelopeMarkDefinition(int $envelopeMarkDefinitionId)
    {
        $envelopeMarkDefinition = $this->getEnvelopeMarkDefinitionById($envelopeMarkDefinitionId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeMarkDefinitionId, $envelopeMarkDefinition]);

        $this->envelopeMarkDefinitionRepository->deleteEnvelopeMarkDefinition(
            $this->envelopeMarkDefinitionRepository->getById($envelopeMarkDefinitionId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeMarkDefinitionId, $envelopeMarkDefinition]);
    }

    public function getEnvelopeMarkDefinitionById(int $id): EnvelopeMarkDefinition
    {
        return $this->envelopeMarkDefinitionRepository->getById($id);
    }

    public function hasEnvelopeMarkDefinitionWithId(int $envelopeMarkDefinitionId): bool
    {
        return $this->envelopeMarkDefinitionRepository->hasEnvelopeMarkDefinitionWithId($envelopeMarkDefinitionId);
    }

    public function getAll(): array
    {
        return $this->envelopeMarkDefinitionRepository->getAll();
    }

    public function getAllOfCollection(EnvelopeMarks $collection): array
    {
        return $this->envelopeMarkDefinitionRepository->getAllOfCollection($collection);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopeMarkDefinitionRepository->getByIds($idsCriteria);
    }
}