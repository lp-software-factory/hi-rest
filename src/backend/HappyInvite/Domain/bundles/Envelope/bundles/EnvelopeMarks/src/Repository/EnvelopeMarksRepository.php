<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions\EnvelopeMarksNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;

final class EnvelopeMarksRepository extends EntityRepository
{
    public function createEnvelopeMarks(EnvelopeMarks $envelopeMarks): int
    {
        while($this->hasWithSID($envelopeMarks->getSID())) {
            $envelopeMarks->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeMarks);
        $this->getEntityManager()->flush($envelopeMarks);

        return $envelopeMarks->getId();
    }

    public function saveEnvelopeMarks(EnvelopeMarks $envelopeMarks)
    {
        $this->getEntityManager()->flush($envelopeMarks);
    }

    public function saveEnvelopeMarksEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeMarks(EnvelopeMarks $envelopeMarks)
    {
        $this->getEntityManager()->remove($envelopeMarks);
        $this->getEntityManager()->flush($envelopeMarks);
    }

    public function getById(int $id): EnvelopeMarks
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeMarks) {
            return $result;
        }else{
            throw new EnvelopeMarksNotFoundException(sprintf('EnvelopeMarks `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeMarksNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeMarks $envelopeMarks) {
                    return $envelopeMarks->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeMarksWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeMarks
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeMarks) {
            return $result;
        }else{
            throw new EnvelopeMarksNotFoundException(sprintf('EnvelopeMarks `SID(%s)` not found', $sid));
        }
    }
}