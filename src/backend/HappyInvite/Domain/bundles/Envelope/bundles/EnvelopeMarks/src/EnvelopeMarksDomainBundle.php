<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks;

use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopeMarksDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}