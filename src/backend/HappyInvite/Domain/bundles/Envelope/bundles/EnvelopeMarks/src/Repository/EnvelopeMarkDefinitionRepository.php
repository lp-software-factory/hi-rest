<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions\EnvelopeMarkDefinitionNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EnvelopeMarkDefinitionRepository extends EntityRepository 
{
    public function createEnvelopeMarkDefinition(EnvelopeMarkDefinition $envelopeMarkDefinition): int
    {
        while($this->hasWithSID($envelopeMarkDefinition->getSID())) {
            $envelopeMarkDefinition->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeMarkDefinition);
        $this->getEntityManager()->flush($envelopeMarkDefinition);

        return $envelopeMarkDefinition->getId();
    }

    public function saveEnvelopeMarkDefinition(EnvelopeMarkDefinition $envelopeMarkDefinition)
    {
        $this->getEntityManager()->flush($envelopeMarkDefinition);
    }

    public function saveEnvelopeMarkDefinitionEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeMarkDefinition(EnvelopeMarkDefinition $envelopeMarkDefinition)
    {
        $this->getEntityManager()->remove($envelopeMarkDefinition);
        $this->getEntityManager()->flush($envelopeMarkDefinition);
    }

    public function getById(int $id): EnvelopeMarkDefinition
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeMarkDefinition) {
            return $result;
        }else{
            throw new EnvelopeMarkDefinitionNotFoundException(sprintf('EnvelopeMarkDefinition `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeMarkDefinitionNotFoundException(sprintf(
                'EnvelopeMarkDefinitions with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeMarkDefinition $envelopeMarkDefinition) {
                    return $envelopeMarkDefinition->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllOfCollection(EnvelopeMarks $collection): array
    {
        return $this->findBy([
            'owner' => $collection
        ], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeMarkDefinitionWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeMarkDefinition
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeMarkDefinition) {
            return $result;
        }else{
            throw new EnvelopeMarkDefinitionNotFoundException(sprintf('EnvelopeMarkDefinition `SID(%s)` not found', $sid));
        }
    }
}