<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarkDefinition;

final class EnvelopeMarkDefinitionFormatter
{
    public function formatOne(EnvelopeMarkDefinition $entity): array
    {
        $json = $entity->toJSON();

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopeMarkDefinition $definition) {
            return $this->formatOne($definition);
        }, $entities);
    }
}