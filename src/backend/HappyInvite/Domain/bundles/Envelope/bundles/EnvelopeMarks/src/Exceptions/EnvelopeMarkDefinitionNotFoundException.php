<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Exceptions;

final class EnvelopeMarkDefinitionNotFoundException extends \Exception {}