<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository\EnvelopeMarkDefinitionRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Factory\Doctrine\EnvelopeMarkDefinitionRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Factory\Doctrine\EnvelopeMarksRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Repository\EnvelopeMarksRepository;

return [
    EnvelopeMarksRepository::class => factory(EnvelopeMarksRepositoryFactory::class),
    EnvelopeMarkDefinitionRepository::class => factory(EnvelopeMarkDefinitionRepositoryFactory::class),
];