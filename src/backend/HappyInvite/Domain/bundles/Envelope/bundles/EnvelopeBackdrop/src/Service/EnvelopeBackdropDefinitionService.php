<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition\CreateEnvelopeBackdropDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition\EditEnvelopeBackdropDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository\EnvelopeBackdropDefinitionRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeBackdropDefinitionService
{
    public const EVENT_CREATE = 'hi.domain.envelope.backdrop.definition.create';
    public const EVENT_EDIT = 'hi.domain.envelope.backdrop.definition.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.backdrop.definition.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.backdrop.definition.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.backdrop.definition.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.backdrop.definition.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.backdrop.definition.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.backdrop.definition.deactivate';

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeBackdropDefinitionRepository */
    private $envelopeBackdropDefinitionRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        AttachmentService $attachmentService,
        EnvelopeBackdropDefinitionRepository $envelopeBackdropDefinitionRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->attachmentService = $attachmentService;
        $this->envelopeBackdropDefinitionRepository = $envelopeBackdropDefinitionRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeBackdropDefinition(CreateEnvelopeBackdropDefinitionParameters $parameters): EnvelopeBackdropDefinition
    {
        $envelopeBackdropDefinition = new EnvelopeBackdropDefinition(
            $parameters->getOwner(),
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getPreview(),
            $parameters->getResource()
        );

        $serial = new SerialManager($this->getAllOfEnvelopeBackdrop($envelopeBackdropDefinition->getOwner()));
        $serial->insertLast($envelopeBackdropDefinition);
        $serial->normalize();

        $this->envelopeBackdropDefinitionRepository->createEnvelopeBackdropDefinition($envelopeBackdropDefinition);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeBackdropDefinition]);

        return $envelopeBackdropDefinition;
    }

    public function editEnvelopeBackdropDefinition(int $envelopeBackdropDefinitionId, EditEnvelopeBackdropDefinitionParameters $parameters): EnvelopeBackdropDefinition
    {
        $envelopeBackdropDefinition = $this->envelopeBackdropDefinitionRepository->getById($envelopeBackdropDefinitionId);
        $envelopeBackdropDefinition
            ->setOwner($parameters->getOwner())
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setPreview($parameters->getPreview())
            ->setResource($parameters->getResource());

        $this->envelopeBackdropDefinitionRepository->saveEnvelopeBackdropDefinition($envelopeBackdropDefinition);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeBackdropDefinition]);

        return $envelopeBackdropDefinition;
    }

    public function moveUpEnvelopeBackdropDefinition(int $envelopeBackdropDefinitionId): int
    {
        $envelopeBackdropDefinition = $this->getEnvelopeBackdropDefinitionById($envelopeBackdropDefinitionId);
        $siblings = $this->getAllOfEnvelopeBackdrop($envelopeBackdropDefinition->getOwner());

        $serial = new SerialManager($siblings);
        $serial->up($envelopeBackdropDefinition);
        $serial->normalize();
        $this->envelopeBackdropDefinitionRepository->saveEnvelopeBackdropDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeBackdropDefinition, $envelopeBackdropDefinition->getPosition()]);

        return $envelopeBackdropDefinition->getPosition();
    }

    public function moveDownEnvelopeBackdropDefinition(int $envelopeBackdropDefinitionId): int
    {
        $envelopeBackdropDefinition = $this->getEnvelopeBackdropDefinitionById($envelopeBackdropDefinitionId);
        $siblings = $this->getAllOfEnvelopeBackdrop($envelopeBackdropDefinition->getOwner());

        $serial = new SerialManager($siblings);
        $serial->down($envelopeBackdropDefinition);
        $serial->normalize();
        $this->envelopeBackdropDefinitionRepository->saveEnvelopeBackdropDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeBackdropDefinition, $envelopeBackdropDefinition->getPosition()]);

        return $envelopeBackdropDefinition->getPosition();
    }

    public function activateEnvelopeBackdropDefinition(int $envelopeBackdropDefinitionId): EnvelopeBackdropDefinition
    {
        $envelopeBackdropDefinition = $this->getEnvelopeBackdropDefinitionById($envelopeBackdropDefinitionId);

        if(! $envelopeBackdropDefinition->isActivated()) {
            $envelopeBackdropDefinition->activate();

            $this->envelopeBackdropDefinitionRepository->saveEnvelopeBackdropDefinition($envelopeBackdropDefinition);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeBackdropDefinition]);
        }

        return $envelopeBackdropDefinition;
    }

    public function deactivateEnvelopeBackdropDefinition(int $envelopeBackdropDefinitionId): EnvelopeBackdropDefinition
    {
        $envelopeBackdropDefinition = $this->getEnvelopeBackdropDefinitionById($envelopeBackdropDefinitionId);

        if($envelopeBackdropDefinition->isActivated()) {
            $envelopeBackdropDefinition->deactivate();

            $this->envelopeBackdropDefinitionRepository->saveEnvelopeBackdropDefinition($envelopeBackdropDefinition);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeBackdropDefinition]);
        }

        return $envelopeBackdropDefinition;
    }

    public function deleteEnvelopeBackdropDefinition(int $envelopeBackdropDefinitionId)
    {
        $envelopeBackdropDefinition = $this->getEnvelopeBackdropDefinitionById($envelopeBackdropDefinitionId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeBackdropDefinitionId, $envelopeBackdropDefinition]);

        $this->envelopeBackdropDefinitionRepository->deleteEnvelopeBackdropDefinition(
            $this->envelopeBackdropDefinitionRepository->getById($envelopeBackdropDefinitionId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeBackdropDefinitionId, $envelopeBackdropDefinition]);
    }

    public function getEnvelopeBackdropDefinitionById(int $id): EnvelopeBackdropDefinition
    {
        return $this->envelopeBackdropDefinitionRepository->getById($id);
    }

    public function hasEnvelopeBackdropDefinitionWithId(int $envelopeBackdropDefinitionId): bool
    {
        return $this->envelopeBackdropDefinitionRepository->hasEnvelopeBackdropDefinitionWithId($envelopeBackdropDefinitionId);
    }

    public function getAll(): array
    {
        return $this->envelopeBackdropDefinitionRepository->getAll();
    }

    public function getAllOfEnvelopeBackdrop(EnvelopeBackdrop $owner): array
    {
        return $this->envelopeBackdropDefinitionRepository->getAllOfEnvelopeBackdrop($owner);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopeBackdropDefinitionRepository->getByIds($idsCriteria);
    }
}