<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

abstract class AbstractEnvelopeBackdropParameters
{
    /** @var Attachment */
    private $preview;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    public function __construct(
        Attachment $preview,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description
    ) {
        $this->preview = $preview;
        $this->title = $title;
        $this->description = $description;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }
}