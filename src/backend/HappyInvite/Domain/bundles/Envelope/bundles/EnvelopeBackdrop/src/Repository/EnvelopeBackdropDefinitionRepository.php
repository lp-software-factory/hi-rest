<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions\EnvelopeBackdropDefinitionNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EnvelopeBackdropDefinitionRepository extends EntityRepository
{
    public function createEnvelopeBackdropDefinition(EnvelopeBackdropDefinition $envelopeBackdropDefinition): int
    {
        while($this->hasWithSID($envelopeBackdropDefinition->getSID())) {
            $envelopeBackdropDefinition->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeBackdropDefinition);
        $this->getEntityManager()->flush($envelopeBackdropDefinition);

        return $envelopeBackdropDefinition->getId();
    }

    public function saveEnvelopeBackdropDefinition(EnvelopeBackdropDefinition $envelopeBackdropDefinition)
    {
        $this->getEntityManager()->flush($envelopeBackdropDefinition);
    }

    public function saveEnvelopeBackdropDefinitionEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeBackdropDefinition(EnvelopeBackdropDefinition $envelopeBackdropDefinition)
    {
        $this->getEntityManager()->remove($envelopeBackdropDefinition);
        $this->getEntityManager()->flush($envelopeBackdropDefinition);
    }

    public function getById(int $id): EnvelopeBackdropDefinition
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeBackdropDefinition) {
            return $result;
        }else{
            throw new EnvelopeBackdropDefinitionNotFoundException(sprintf('EnvelopeBackdropDefinition `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeBackdropDefinitionNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeBackdropDefinition $envelopeBackdropDefinition) {
                    return $envelopeBackdropDefinition->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllOfEnvelopeBackdrop(EnvelopeBackdrop $backdrop): array
    {
        return $this->findBy([
            'owner' => $backdrop,
        ], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeBackdropDefinitionWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeBackdropDefinition
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeBackdropDefinition) {
            return $result;
        }else{
            throw new EnvelopeBackdropDefinitionNotFoundException(sprintf('EnvelopeBackdropDefinition `SID(%s)` not found', $sid));
        }
    }
}