<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop;

use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopeBackdropDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}