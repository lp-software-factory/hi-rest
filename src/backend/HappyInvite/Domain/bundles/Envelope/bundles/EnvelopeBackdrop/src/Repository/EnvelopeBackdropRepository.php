<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions\EnvelopeBackdropNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;

final class EnvelopeBackdropRepository extends EntityRepository
{
    public function createEnvelopeBackdrop(EnvelopeBackdrop $envelopeBackdrop): int
    {
        while($this->hasWithSID($envelopeBackdrop->getSID())) {
            $envelopeBackdrop->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopeBackdrop);
        $this->getEntityManager()->flush($envelopeBackdrop);

        return $envelopeBackdrop->getId();
    }

    public function saveEnvelopeBackdrop(EnvelopeBackdrop $envelopeBackdrop)
    {
        $this->getEntityManager()->flush($envelopeBackdrop);
    }

    public function saveEnvelopeBackdropEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopeBackdrop(EnvelopeBackdrop $envelopeBackdrop)
    {
        $this->getEntityManager()->remove($envelopeBackdrop);
        $this->getEntityManager()->flush($envelopeBackdrop);
    }

    public function getById(int $id): EnvelopeBackdrop
    {
        $result = $this->find($id);

        if($result instanceof EnvelopeBackdrop) {
            return $result;
        }else{
            throw new EnvelopeBackdropNotFoundException(sprintf('EnvelopeBackdrop `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopeBackdropNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopeBackdrop $envelopeBackdrop) {
                    return $envelopeBackdrop->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllPublic(): array
    {
        return $this->findBy([], [
            'position' => 'asc'
        ]);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopeBackdropWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopeBackdrop
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopeBackdrop) {
            return $result;
        }else{
            throw new EnvelopeBackdropNotFoundException(sprintf('EnvelopeBackdrop `SID(%s)` not found', $sid));
        }
    }
}