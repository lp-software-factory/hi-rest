<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\CreateEnvelopeBackdropParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\EditEnvelopeBackdropParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository\EnvelopeBackdropRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopeBackdropService
{
    public const EVENT_CREATE = 'hi.domain.envelope.backdrop.create';
    public const EVENT_EDIT = 'hi.domain.envelope.backdrop.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.backdrop.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.backdrop.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.backdrop.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.backdrop.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.backdrop.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.backdrop.deactivate';

    /** @var EnvelopeBackdropRepository */
    private $envelopeBackdropRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        EnvelopeBackdropRepository $envelopeBackdropRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->envelopeBackdropRepository = $envelopeBackdropRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopeBackdrop(CreateEnvelopeBackdropParameters $parameters): EnvelopeBackdrop
    {
        $envelopeBackdrop = new EnvelopeBackdrop(
            $parameters->getPreview(),
            $parameters->getTitle(),
            $parameters->getDescription()
        );

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($envelopeBackdrop);
        $serial->normalize();

        $this->envelopeBackdropRepository->createEnvelopeBackdrop($envelopeBackdrop);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopeBackdrop]);

        return $envelopeBackdrop;
    }

    public function editEnvelopeBackdrop(int $envelopeBackdropId, EditEnvelopeBackdropParameters $parameters): EnvelopeBackdrop
    {
        $envelopeBackdrop = $this->envelopeBackdropRepository->getById($envelopeBackdropId);
        $envelopeBackdrop
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setPreview($parameters->getPreview());

        $this->envelopeBackdropRepository->saveEnvelopeBackdrop($envelopeBackdrop);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopeBackdrop]);

        return $envelopeBackdrop;
    }

    public function moveUpEnvelopeBackdrop(int $envelopeBackdropId): int
    {
        $envelopeBackdrop = $this->getEnvelopeBackdropById($envelopeBackdropId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($envelopeBackdrop);
        $serial->normalize();
        $this->envelopeBackdropRepository->saveEnvelopeBackdropEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeBackdrop, $envelopeBackdrop->getPosition()]);

        return $envelopeBackdrop->getPosition();
    }

    public function moveDownEnvelopeBackdrop(int $envelopeBackdropId): int
    {
        $envelopeBackdrop = $this->getEnvelopeBackdropById($envelopeBackdropId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($envelopeBackdrop);
        $serial->normalize();
        $this->envelopeBackdropRepository->saveEnvelopeBackdropEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopeBackdrop, $envelopeBackdrop->getPosition()]);

        return $envelopeBackdrop->getPosition();
    }

    public function activateEnvelopeBackdrop(int $envelopeBackdropId): EnvelopeBackdrop
    {
        $envelopeBackdrop = $this->getEnvelopeBackdropById($envelopeBackdropId);

        if(! $envelopeBackdrop->isActivated()) {
            $envelopeBackdrop->activate();

            $this->envelopeBackdropRepository->saveEnvelopeBackdrop($envelopeBackdrop);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopeBackdrop]);
        }

        return $envelopeBackdrop;
    }

    public function deactivateEnvelopeBackdrop(int $envelopeBackdropId): EnvelopeBackdrop
    {
        $envelopeBackdrop = $this->getEnvelopeBackdropById($envelopeBackdropId);

        if($envelopeBackdrop->isActivated()) {
            $envelopeBackdrop->deactivate();

            $this->envelopeBackdropRepository->saveEnvelopeBackdrop($envelopeBackdrop);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopeBackdrop]);
        }

        return $envelopeBackdrop;
    }

    public function deleteEnvelopeBackdrop(int $envelopeBackdropId)
    {
        $envelopeBackdrop = $this->getEnvelopeBackdropById($envelopeBackdropId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopeBackdropId, $envelopeBackdrop]);

        $this->envelopeBackdropRepository->deleteEnvelopeBackdrop(
            $this->envelopeBackdropRepository->getById($envelopeBackdropId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopeBackdropId, $envelopeBackdrop]);
    }

    public function getEnvelopeBackdropById(int $id): EnvelopeBackdrop
    {
        return $this->envelopeBackdropRepository->getById($id);
    }

    public function hasEnvelopeBackdropWithId(int $envelopeBackdropId): bool
    {
        return $this->envelopeBackdropRepository->hasEnvelopeBackdropWithId($envelopeBackdropId);
    }

    public function getAll(): array
    {
        return $this->envelopeBackdropRepository->getAll();
    }

    public function getAllPublic(): array
    {
        return $this->envelopeBackdropRepository->getAllPublic();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopeBackdropRepository->getByIds($idsCriteria);
    }
}