<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository\EnvelopeBackdropDefinitionRepository")
 * @Table(name="envelope_backdrop_definition")
 */
class EnvelopeBackdropDefinition implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, SerialEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, ActivatedEntityTrait, SerialEntityTrait, VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop")
     * @JoinColumn(name="owner_envelope_backdrop_id", referencedColumnName="id")
     * @var EnvelopeBackdrop
     */
    private $owner;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="preview_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $preview;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="resource_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $resource;

    public function __construct(
        EnvelopeBackdrop $owner,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Attachment $preview,
        Attachment $resource
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->owner = $owner;
        $this->title = $title;
        $this->description = $description;
        $this->preview = $preview;
        $this->resource = $resource;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'owner_envelope_backdrop_id' => $this->getOwner()->getId(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'resource' => $this->getResource()->toJSON($options),
            'preview' => $this->getPreview()->toJSON($options),
        ];
    }

    public function getMetadataVersion(): string
    {
        return '1.0.0';
    }

    public function getOwner(): EnvelopeBackdrop
    {
        return $this->owner;
    }

    public function setOwner(EnvelopeBackdrop $owner): self
    {
        $this->owner = $owner;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function setPreview(Attachment $preview): self
    {
        $this->preview = $preview;
        $this->markAsUpdated();

        return $this;
    }

    public function getResource(): Attachment
    {
        return $this->resource;
    }

    public function setResource(Attachment $resource): self
    {
        $this->resource = $resource;
        $this->markAsUpdated();

        return $this;
    }
}