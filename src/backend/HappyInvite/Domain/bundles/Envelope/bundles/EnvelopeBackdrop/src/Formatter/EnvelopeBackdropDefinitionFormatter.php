<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;

final class EnvelopeBackdropDefinitionFormatter
{
    public function formatOne(EnvelopeBackdropDefinition $entity): array
    {
        $json = $entity->toJSON();

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopeBackdropDefinition $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}