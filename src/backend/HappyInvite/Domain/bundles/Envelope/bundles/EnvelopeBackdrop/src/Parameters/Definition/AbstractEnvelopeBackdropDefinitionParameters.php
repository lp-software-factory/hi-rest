<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;

abstract class AbstractEnvelopeBackdropDefinitionParameters
{
    /** @var EnvelopeBackdrop */
    private $owner;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var Attachment */
    private $preview;

    /** @var Attachment */
    private $resource;

    public function __construct(EnvelopeBackdrop $owner, ImmutableLocalizedString $title, ImmutableLocalizedString $description, Attachment $preview, Attachment $resource)
    {
        $this->owner = $owner;
        $this->title = $title;
        $this->description = $description;
        $this->preview = $preview;
        $this->resource = $resource;
    }

    public function getOwner(): EnvelopeBackdrop
    {
        return $this->owner;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getResource(): Attachment
    {
        return $this->resource;
    }
}