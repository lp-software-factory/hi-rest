<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions;

final class EnvelopeBackdropDefinitionNotFoundException extends \Exception {}