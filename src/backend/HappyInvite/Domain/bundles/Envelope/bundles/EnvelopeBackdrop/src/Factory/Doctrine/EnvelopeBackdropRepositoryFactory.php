<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;

final class EnvelopeBackdropRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EnvelopeBackdrop::class;
    }
}