<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Exceptions;

final class EnvelopeBackdropNotFoundException extends \Exception {}