<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Factory\Doctrine\EnvelopeBackdropDefinitionRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Factory\Doctrine\EnvelopeBackdropRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository\EnvelopeBackdropDefinitionRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Repository\EnvelopeBackdropRepository;

return [
    EnvelopeBackdropRepository::class => factory(EnvelopeBackdropRepositoryFactory::class),
    EnvelopeBackdropDefinitionRepository::class => factory(EnvelopeBackdropDefinitionRepositoryFactory::class),
];