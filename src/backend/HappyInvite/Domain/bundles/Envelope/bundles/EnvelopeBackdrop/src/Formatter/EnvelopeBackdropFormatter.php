<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;

final class EnvelopeBackdropFormatter
{
    /** @var EnvelopeBackdropDefinitionFormatter */
    private $envelopeBackdropDefinitionFormatter;

    public function __construct(EnvelopeBackdropDefinitionFormatter $envelopeBackdropDefinitionFormatter)
    {
        $this->envelopeBackdropDefinitionFormatter = $envelopeBackdropDefinitionFormatter;
    }

    public function formatOne(EnvelopeBackdrop $entity): array
    {
        $json = $entity->toJSON();
        $json['definitions'] = $this->envelopeBackdropDefinitionFormatter->formatMany($entity->getDefinitions()->toArray());

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopeBackdrop $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}