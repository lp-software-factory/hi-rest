<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions;

final class EnvelopePatternNotFoundException extends \Exception {}