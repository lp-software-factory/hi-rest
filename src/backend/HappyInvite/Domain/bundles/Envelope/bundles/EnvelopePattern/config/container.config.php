<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Factory\Doctrine\EnvelopePatternDefinitionRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Factory\Doctrine\EnvelopePatternRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository\EnvelopePatternDefinitionRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository\EnvelopePatternRepository;

return [
    EnvelopePatternRepository::class => factory(EnvelopePatternRepositoryFactory::class),
    EnvelopePatternDefinitionRepository::class => factory(EnvelopePatternDefinitionRepositoryFactory::class),
];