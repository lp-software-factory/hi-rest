<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository\EnvelopePatternDefinitionRepository")
 * @Table(name="envelope_pattern_definition")
 */
class EnvelopePatternDefinition implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, SerialEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, ActivatedEntityTrait, SerialEntityTrait, VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern")
     * @JoinColumn(name="envelope_pattern_id", referencedColumnName="id")
     * @var EnvelopePattern
     */
    private $envelopePattern;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="preview_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $preview;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="texture_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $attachment;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor")
     * @JoinColumn(name="envelope_color_id", referencedColumnName="id")
     * @var EnvelopeColor
     */
    private $envelopeColor;

    /**
     * @Column(name="width", type="integer")
     * @var int
     */
    private $width;

    /**
     * @Column(name="height", type="integer")
     * @var int
     */
    private $height;

    public function __construct(
        EnvelopePattern $envelopePattern,
        Attachment $preview,
        Attachment $attachment,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        EnvelopeColor $envelopeColor,
        int $width,
        int $height
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->envelopePattern = $envelopePattern;
        $this->preview = $preview;
        $this->attachment = $attachment;
        $this->title = $title;
        $this->description = $description;
        $this->envelopeColor = $envelopeColor;
        $this->width = $width;
        $this->height = $height;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'envelope_color_id' => $this->envelopeColor->getId(),
            'envelope_pattern_id' => $this->envelopePattern->getId(),
            'attachment' => $this->getAttachment()->toJSON($options),
            'preview' => $this->getPreview()->toJSON($options),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getEnvelopePattern(): EnvelopePattern
    {
        return $this->envelopePattern;
    }

    public function setEnvelopePattern(EnvelopePattern $envelopePattern): self
    {
        $this->envelopePattern = $envelopePattern;
        $this->markAsUpdated();

        return $this;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function setPreview(Attachment $preview): self
    {
        $this->preview = $preview;
        $this->markAsUpdated();

        return $this;
    }

    public function getAttachment(): Attachment
    {
        return $this->attachment;
    }

    public function setAttachment(Attachment $attachment): self
    {
        $this->attachment = $attachment;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getEnvelopeColor(): EnvelopeColor
    {
        return $this->envelopeColor;
    }

    public function setEnvelopeColor(EnvelopeColor $envelopeColor): self
    {
        $this->envelopeColor = $envelopeColor;
        $this->markAsUpdated();

        return $this;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;
        $this->markAsUpdated();

        return $this;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;
        $this->markAsUpdated();

        return $this;
    }
}