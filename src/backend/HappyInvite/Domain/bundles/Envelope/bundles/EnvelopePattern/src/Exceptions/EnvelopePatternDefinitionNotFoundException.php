<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions;

final class EnvelopePatternDefinitionNotFoundException extends \Exception {}