<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;

final class EnvelopePatternDefinitionRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EnvelopePatternDefinition::class;
    }
}