<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;

abstract class AbstractEnvelopePatternParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description
    ) {
        $this->title = $title;
        $this->description = $description;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }
}