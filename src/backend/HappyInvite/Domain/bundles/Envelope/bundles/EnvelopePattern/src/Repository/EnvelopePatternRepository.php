<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions\EnvelopePatternNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;

final class EnvelopePatternRepository extends EntityRepository
{
    public function createEnvelopePattern(EnvelopePattern $envelopePattern): int
    {
        while($this->hasWithSID($envelopePattern->getSID())) {
            $envelopePattern->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopePattern);
        $this->getEntityManager()->flush($envelopePattern);

        return $envelopePattern->getId();
    }

    public function saveEnvelopePattern(EnvelopePattern $envelopePattern)
    {
        $this->getEntityManager()->flush($envelopePattern);
    }

    public function saveEnvelopePatternEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopePattern(EnvelopePattern $envelopePattern)
    {
        $this->getEntityManager()->remove($envelopePattern);
        $this->getEntityManager()->flush($envelopePattern);
    }

    public function getById(int $id): EnvelopePattern
    {
        $result = $this->find($id);

        if($result instanceof EnvelopePattern) {
            return $result;
        }else{
            throw new EnvelopePatternNotFoundException(sprintf('EnvelopePattern `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopePatternNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopePattern $envelopePattern) {
                    return $envelopePattern->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllPublic(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopePatternWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopePattern
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopePattern) {
            return $result;
        }else{
            throw new EnvelopePatternNotFoundException(sprintf('EnvelopePattern `SID(%s)` not found', $sid));
        }
    }
}