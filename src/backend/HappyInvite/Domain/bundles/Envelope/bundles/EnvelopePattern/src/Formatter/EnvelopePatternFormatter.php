<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;

final class EnvelopePatternFormatter
{
    /** @var EnvelopePatternDefinitionFormatter */
    private $envelopePatternDefinitionFormatter;


    public function __construct(EnvelopePatternDefinitionFormatter $envelopePatternDefinitionFormatter)
    {
        $this->envelopePatternDefinitionFormatter = $envelopePatternDefinitionFormatter;
    }

    public function formatOne(EnvelopePattern $entity): array
    {
        $json = $entity->toJSON();

        $json['definitions'] = $this->envelopePatternDefinitionFormatter->formatMany(
            $entity->getEnvelopePatternDefinitions()
        );

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopePattern $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}