<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition\CreateEnvelopePatternDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition\EditEnvelopePatternDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository\EnvelopePatternDefinitionRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopePatternDefinitionService
{
    public const EVENT_CREATE = 'hi.domain.envelope.pattern.definition.create';
    public const EVENT_EDIT = 'hi.domain.envelope.pattern.definition.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.pattern.definition.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.pattern.definition.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.pattern.definition.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.pattern.definition.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.pattern.definition.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.pattern.definition.deactivate';

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopePatternDefinitionRepository */
    private $envelopePatternDefinitionRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        AttachmentService $attachmentService,
        EnvelopePatternDefinitionRepository $envelopePatternDefinitionRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->attachmentService = $attachmentService;
        $this->envelopePatternDefinitionRepository = $envelopePatternDefinitionRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopePatternDefinition(CreateEnvelopePatternDefinitionParameters $parameters): EnvelopePatternDefinition
    {
        $envelopePatternDefinition = new EnvelopePatternDefinition(
            $parameters->getEnvelopePattern(),
            $parameters->getPreview(),
            $parameters->getAttachment(),
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getEnvelopeColor(),
            $parameters->getWidth(),
            $parameters->getHeight()
        );

        $serial = new SerialManager($this->getAllOfEnvelopePattern($envelopePatternDefinition->getEnvelopePattern()));
        $serial->insertLast($envelopePatternDefinition);
        $serial->normalize();

        $this->envelopePatternDefinitionRepository->createEnvelopePatternDefinition($envelopePatternDefinition);
        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopePatternDefinition]);

        return $envelopePatternDefinition;
    }

    public function editEnvelopePatternDefinition(int $envelopePatternDefinitionId, EditEnvelopePatternDefinitionParameters $parameters): EnvelopePatternDefinition
    {
        $envelopePatternDefinition = $this->envelopePatternDefinitionRepository->getById($envelopePatternDefinitionId);
        $envelopePatternDefinition
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setAttachment($parameters->getAttachment())
            ->setPreview($parameters->getPreview())
            ->setEnvelopePattern($parameters->getEnvelopePattern())
            ->setEnvelopeColor($parameters->getEnvelopeColor())
            ->setWidth($parameters->getWidth())
            ->setHeight($parameters->getHeight())
        ;

        $this->envelopePatternDefinitionRepository->saveEnvelopePatternDefinition($envelopePatternDefinition);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopePatternDefinition]);

        return $envelopePatternDefinition;
    }

    public function moveUpEnvelopePatternDefinition(int $envelopePatternDefinitionId): int
    {
        $envelopePatternDefinition = $this->getEnvelopePatternDefinitionById($envelopePatternDefinitionId);
        $siblings = $this->getAllOfEnvelopePattern($envelopePatternDefinition->getEnvelopePattern());

        $serial = new SerialManager($siblings);
        $serial->up($envelopePatternDefinition);
        $serial->normalize();
        $this->envelopePatternDefinitionRepository->saveEnvelopePatternDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopePatternDefinition, $envelopePatternDefinition->getPosition()]);

        return $envelopePatternDefinition->getPosition();
    }

    public function moveDownEnvelopePatternDefinition(int $envelopePatternDefinitionId): int
    {
        $envelopePatternDefinition = $this->getEnvelopePatternDefinitionById($envelopePatternDefinitionId);
        $siblings = $this->getAllOfEnvelopePattern($envelopePatternDefinition->getEnvelopePattern());

        $serial = new SerialManager($siblings);
        $serial->down($envelopePatternDefinition);
        $serial->normalize();
        $this->envelopePatternDefinitionRepository->saveEnvelopePatternDefinitionEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopePatternDefinition, $envelopePatternDefinition->getPosition()]);

        return $envelopePatternDefinition->getPosition();
    }

    public function activateEnvelopePatternDefinition(int $envelopePatternDefinitionId): EnvelopePatternDefinition
    {
        $envelopePatternDefinition = $this->getEnvelopePatternDefinitionById($envelopePatternDefinitionId);

        if(! $envelopePatternDefinition->isActivated()) {
            $envelopePatternDefinition->activate();

            $this->envelopePatternDefinitionRepository->saveEnvelopePatternDefinition($envelopePatternDefinition);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopePatternDefinition]);
        }

        return $envelopePatternDefinition;
    }

    public function deactivateEnvelopePatternDefinition(int $envelopePatternDefinitionId): EnvelopePatternDefinition
    {
        $envelopePatternDefinition = $this->getEnvelopePatternDefinitionById($envelopePatternDefinitionId);

        if($envelopePatternDefinition->isActivated()) {
            $envelopePatternDefinition->deactivate();

            $this->envelopePatternDefinitionRepository->saveEnvelopePatternDefinition($envelopePatternDefinition);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopePatternDefinition]);
        }

        return $envelopePatternDefinition;
    }

    public function deleteEnvelopePatternDefinition(int $envelopePatternDefinitionId)
    {
        $envelopePatternDefinition = $this->getEnvelopePatternDefinitionById($envelopePatternDefinitionId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopePatternDefinitionId, $envelopePatternDefinition]);

        $this->envelopePatternDefinitionRepository->deleteEnvelopePatternDefinition(
            $this->envelopePatternDefinitionRepository->getById($envelopePatternDefinitionId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopePatternDefinitionId, $envelopePatternDefinition]);
    }

    public function getEnvelopePatternDefinitionById(int $id): EnvelopePatternDefinition
    {
        return $this->envelopePatternDefinitionRepository->getById($id);
    }

    public function hasEnvelopePatternDefinitionWithId(int $envelopePatternDefinitionId): bool
    {
        return $this->envelopePatternDefinitionRepository->hasEnvelopePatternDefinitionWithId($envelopePatternDefinitionId);
    }

    public function getAll(): array
    {
        return $this->envelopePatternDefinitionRepository->getAll();
    }

    public function getAllOfEnvelopePattern(EnvelopePattern $envelopePattern): array
    {
        return $this->envelopePatternDefinitionRepository->getAllOfEnvelopePattern($envelopePattern);
    }

    public function getAllPublic(): array
    {
        return $this->envelopePatternDefinitionRepository->getAllPublic();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopePatternDefinitionRepository->getByIds($idsCriteria);
    }
}