<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Exceptions\EnvelopePatternDefinitionNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EnvelopePatternDefinitionRepository extends EntityRepository
{
    public function createEnvelopePatternDefinition(EnvelopePatternDefinition $envelopePatternDefinition): int
    {
        while($this->hasWithSID($envelopePatternDefinition->getSID())) {
            $envelopePatternDefinition->regenerateSID();
        }

        $this->getEntityManager()->persist($envelopePatternDefinition);
        $this->getEntityManager()->flush($envelopePatternDefinition);

        return $envelopePatternDefinition->getId();
    }

    public function saveEnvelopePatternDefinition(EnvelopePatternDefinition $envelopePatternDefinition)
    {
        $this->getEntityManager()->flush($envelopePatternDefinition);
    }

    public function saveEnvelopePatternDefinitionEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEnvelopePatternDefinition(EnvelopePatternDefinition $envelopePatternDefinition)
    {
        $this->getEntityManager()->remove($envelopePatternDefinition);
        $this->getEntityManager()->flush($envelopePatternDefinition);
    }

    public function getById(int $id): EnvelopePatternDefinition
    {
        $result = $this->find($id);

        if($result instanceof EnvelopePatternDefinition) {
            return $result;
        }else{
            throw new EnvelopePatternDefinitionNotFoundException(sprintf('EnvelopePatternDefinition `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new EnvelopePatternDefinitionNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(EnvelopePatternDefinition $envelopePatternDefinition) {
                    return $envelopePatternDefinition->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function getAll(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function getAllOfEnvelopePattern(EnvelopePattern $envelopePattern): array
    {
        return $this->findBy([
            'envelopePattern' => $envelopePattern,
        ]);
    }

    public function getAllPublic(): array
    {
        return $this->findBy([], ['position' => 'asc']);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasEnvelopePatternDefinitionWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): EnvelopePatternDefinition
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof EnvelopePatternDefinition) {
            return $result;
        }else{
            throw new EnvelopePatternDefinitionNotFoundException(sprintf('EnvelopePatternDefinition `SID(%s)` not found', $sid));
        }
    }
}