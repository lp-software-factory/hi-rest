<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePatternDefinition;

final class EnvelopePatternDefinitionFormatter
{
    public function formatOne(EnvelopePatternDefinition $entity): array
    {
        $json = $entity->toJSON();

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EnvelopePatternDefinition $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}