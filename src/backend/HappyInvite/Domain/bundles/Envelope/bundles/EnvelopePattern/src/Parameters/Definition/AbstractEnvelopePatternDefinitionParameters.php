<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;

abstract class AbstractEnvelopePatternDefinitionParameters
{
    /** @var EnvelopePattern */
    private $envelopePattern;

    /** @var Attachment */
    private $attachment;

    /** @var Attachment */
    private $preview;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var EnvelopeColor */
    private $envelopeColor;

    /** @var int */
    private $width;

    /** @var int */
    private $height;

    public function __construct(
        EnvelopePattern $envelopePattern,
        Attachment $attachment,
        Attachment $preview,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        EnvelopeColor $envelopeColor,
        int $width,
        int $height
    ) {
        $this->envelopePattern = $envelopePattern;
        $this->attachment = $attachment;
        $this->preview = $preview;
        $this->title = $title;
        $this->description = $description;
        $this->envelopeColor = $envelopeColor;
        $this->width = $width;
        $this->height = $height;
    }

    public function getEnvelopePattern(): EnvelopePattern
    {
        return $this->envelopePattern;
    }

    public function getAttachment(): Attachment
    {
        return $this->attachment;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getEnvelopeColor(): EnvelopeColor
    {
        return $this->envelopeColor;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }
}