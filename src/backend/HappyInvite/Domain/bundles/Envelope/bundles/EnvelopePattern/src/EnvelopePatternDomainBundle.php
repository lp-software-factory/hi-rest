<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern;

use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopePatternDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}