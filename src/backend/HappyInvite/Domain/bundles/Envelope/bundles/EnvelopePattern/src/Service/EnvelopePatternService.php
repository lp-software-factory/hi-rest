<?php
namespace HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\CreateEnvelopePatternParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\EditEnvelopePatternParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Repository\EnvelopePatternRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EnvelopePatternService
{
    public const EVENT_CREATE = 'hi.domain.envelope.pattern.create';
    public const EVENT_EDIT = 'hi.domain.envelope.pattern.edit';
    public const EVENT_DELETE = 'hi.domain.envelope.pattern.delete';
    public const EVENT_DELETED = 'hi.domain.envelope.pattern.deleted';
    public const EVENT_MOVE_UP = 'hi.domain.envelope.pattern.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.envelope.pattern.move.down';
    public const EVENT_ACTIVATE = 'hi.domain.envelope.pattern.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.envelope.pattern.deactivate';

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopePatternRepository */
    private $envelopePatternRepository;

    /** @var EventEmitterInterface */
    private $eventEmitter;

    public function __construct(
        AttachmentService $attachmentService,
        EnvelopePatternRepository $envelopePatternRepository
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->attachmentService = $attachmentService;
        $this->envelopePatternRepository = $envelopePatternRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEnvelopePattern(CreateEnvelopePatternParameters $parameters): EnvelopePattern
    {
        $envelopePattern = new EnvelopePattern(
            $parameters->getTitle(),
            $parameters->getDescription()
        );

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($envelopePattern);
        $serial->normalize();

        $this->envelopePatternRepository->createEnvelopePattern($envelopePattern);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$envelopePattern]);

        return $envelopePattern;
    }

    public function editEnvelopePattern(int $envelopePatternId, EditEnvelopePatternParameters $parameters): EnvelopePattern
    {
        $envelopePattern = $this->envelopePatternRepository->getById($envelopePatternId);
        $envelopePattern
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
        ;

        $this->envelopePatternRepository->saveEnvelopePattern($envelopePattern);

        $this->eventEmitter->emit(self::EVENT_EDIT, [$envelopePattern]);

        return $envelopePattern;
    }

    public function moveUpEnvelopePattern(int $envelopePatternId): int
    {
        $envelopePattern = $this->getEnvelopePatternById($envelopePatternId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($envelopePattern);
        $serial->normalize();
        $this->envelopePatternRepository->saveEnvelopePatternEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopePattern, $envelopePattern->getPosition()]);

        return $envelopePattern->getPosition();
    }

    public function moveDownEnvelopePattern(int $envelopePatternId): int
    {
        $envelopePattern = $this->getEnvelopePatternById($envelopePatternId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($envelopePattern);
        $serial->normalize();
        $this->envelopePatternRepository->saveEnvelopePatternEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_MOVE_UP, [$envelopePattern, $envelopePattern->getPosition()]);

        return $envelopePattern->getPosition();
    }

    public function activateEnvelopePattern(int $envelopePatternId): EnvelopePattern
    {
        $envelopePattern = $this->getEnvelopePatternById($envelopePatternId);

        if(! $envelopePattern->isActivated()) {
            $envelopePattern->activate();

            $this->envelopePatternRepository->saveEnvelopePattern($envelopePattern);

            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$envelopePattern]);
        }

        return $envelopePattern;
    }

    public function deactivateEnvelopePattern(int $envelopePatternId): EnvelopePattern
    {
        $envelopePattern = $this->getEnvelopePatternById($envelopePatternId);

        if($envelopePattern->isActivated()) {
            $envelopePattern->deactivate();

            $this->envelopePatternRepository->saveEnvelopePattern($envelopePattern);

            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$envelopePattern]);
        }

        return $envelopePattern;
    }

    public function deleteEnvelopePattern(int $envelopePatternId)
    {
        $envelopePattern = $this->getEnvelopePatternById($envelopePatternId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$envelopePatternId, $envelopePattern]);

        $this->envelopePatternRepository->deleteEnvelopePattern(
            $this->envelopePatternRepository->getById($envelopePatternId)
        );

        $this->eventEmitter->emit(self::EVENT_DELETED, [$envelopePatternId, $envelopePattern]);
    }

    public function getEnvelopePatternById(int $id): EnvelopePattern
    {
        return $this->envelopePatternRepository->getById($id);
    }

    public function hasEnvelopePatternWithId(int $envelopePatternId): bool
    {
        return $this->envelopePatternRepository->hasEnvelopePatternWithId($envelopePatternId);
    }

    public function getAll(): array
    {
        return $this->envelopePatternRepository->getAll();
    }

    public function getAllPublic(): array
    {
        return $this->envelopePatternRepository->getAllPublic();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->envelopePatternRepository->getByIds($idsCriteria);
    }
}