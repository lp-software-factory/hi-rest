<?php
namespace HappyInvite\Domain\Bundles\Envelope;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\EnvelopeBackdropDomainBundle;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\EnvelopeColorDomainBundle;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\EnvelopeMarksDomainBundle;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\EnvelopePatternDomainBundle;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\EnvelopeTemplateDomainBundle;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\EnvelopeTextureDomainBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EnvelopeDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new EnvelopeTextureDomainBundle(),
            new EnvelopeMarksDomainBundle(),
            new EnvelopeColorDomainBundle(),
            new EnvelopeBackdropDomainBundle(),
            new EnvelopePatternDomainBundle(),
            new EnvelopeTemplateDomainBundle(),
        ];
    }
}