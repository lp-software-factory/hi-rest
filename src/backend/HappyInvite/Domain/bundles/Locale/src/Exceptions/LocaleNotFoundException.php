<?php
namespace HappyInvite\Domain\Bundles\Locale\Exceptions;

final class LocaleNotFoundException extends \Exception {}