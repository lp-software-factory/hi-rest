<?php
namespace HappyInvite\Domain\Bundles\Locale\Exceptions;

final class DuplicateLocaleException extends \Exception {}