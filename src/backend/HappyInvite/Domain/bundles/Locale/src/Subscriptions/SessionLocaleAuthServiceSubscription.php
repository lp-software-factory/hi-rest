<?php
namespace HappyInvite\Domain\Bundles\Locale\Subscriptions;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Auth\Service\AuthService;
use HappyInvite\Domain\Bundles\Locale\Service\SessionLocaleService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class SessionLocaleAuthServiceSubscription implements SubscriptionScript
{
    /** @var AuthService */
    private $authService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(AuthService $authService, SessionLocaleService $sessionLocaleService)
    {
        $this->authService = $authService;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->authService->getEventEmitter()->on(AuthService::EVENT_SIGN_IN, function(Account $account) {
            $this->sessionLocaleService->setCurrentLocale($account->getLocale());
        });
    }
}