<?php
namespace HappyInvite\Domain\Bundles\Locale\Subscriptions;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Locale\Service\SessionLocaleService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class SessionLocaleAccountServiceSubscription implements SubscriptionScript
{
    /** @var AccountService */
    private $accountService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(AccountService $accountService, SessionLocaleService $sessionLocaleService)
    {
        $this->accountService = $accountService;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->accountService->getEventEmitter()->on(AccountService::EVENT_SET_LOCALE, function(Account $account) {
            $this->sessionLocaleService->setCurrentLocale($account->getLocale());
        });
    }
}