<?php
namespace HappyInvite\Domain\Bundles\Locale\Config;

final class SessionLocaleConfig
{
    /** @var string */
    private $sessionKey;

    /** @var string */
    private $defaultRegion;

    /** @var array */
    private $forced;

    public function __construct(string $sessionKey, string $defaultRegion, array $forced)
    {
        $this->sessionKey = $sessionKey;
        $this->defaultRegion = $defaultRegion;
        $this->forced = $forced;
    }

    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    public function getDefaultRegion(): string
    {
        return $this->defaultRegion;
    }

    public function getForced(): array
    {
        return $this->forced;
    }
}