<?php
namespace HappyInvite\Domain\Bundles\Locale;

use HappyInvite\Domain\Bundles\Locale\Subscriptions\SessionLocaleAccountServiceSubscription;
use HappyInvite\Domain\Bundles\Locale\Subscriptions\SessionLocaleAuthServiceSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class LocaleBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            SessionLocaleAccountServiceSubscription::class,
            SessionLocaleAuthServiceSubscription::class,
        ];
    }
}