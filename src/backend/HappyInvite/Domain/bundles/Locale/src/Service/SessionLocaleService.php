<?php
namespace HappyInvite\Domain\Bundles\Locale\Service;

use HappyInvite\Domain\Bundles\Locale\Config\SessionLocaleConfig;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Service\ServerConfig;

final class SessionLocaleService
{
    /** @var ServerConfig */
    private $server;

    /** @var SessionLocaleConfig */
    private $config;

    /** @var LocaleService */
    private $service;

    public function __construct(
        ServerConfig $serverConfig,
        SessionLocaleConfig $config,
        LocaleService $service
    ) {
        $this->server = $serverConfig;
        $this->config = $config;
        $this->service = $service;
    }

    public function setCurrentLocale(Locale $locale)
    {
        $_SESSION[$this->config->getSessionKey()] = $locale->getRegion();
    }

    public function getCurrentLocale(): Locale
    {
        $forced = $this->config->getForced();
        $host = $this->server->getHost();

        if(isset($forced[$host])) {
            return $this->service->getLocaleByRegion($forced[$host]);
        }else{
            return $this->service->getLocaleByRegion($this->getCurrentRegion());
        }
    }

    public function getCurrentRegion(): string
    {
        if(! isset($_SESSION[$this->config->getSessionKey()])) {
            return $this->config->getDefaultRegion();
        }else{
            return $_SESSION[$this->config->getSessionKey()];
        }
    }
}