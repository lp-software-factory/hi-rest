<?php
namespace HappyInvite\Domain\Bundles\Locale\Parameters;

final class CreateLocaleParameters
{
    /** @var string */
    private $language;

    /** @var string */
    private $region;

    public function __construct(string $language, string $region)
    {
        $this->language = $language;
        $this->region = $region;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getRegion(): string
    {
        return $this->region;
    }
}