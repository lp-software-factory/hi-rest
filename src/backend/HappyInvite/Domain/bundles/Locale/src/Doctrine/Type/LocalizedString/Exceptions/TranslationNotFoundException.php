<?php
namespace HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Exceptions;

final class TranslationNotFoundException extends \Exception {}