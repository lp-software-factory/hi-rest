<?php
namespace HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString;

interface LocalizedString
{
    public function toJSON() : array;
    public function getDefinition() : array;
    public function hasTranslation(string $region) : bool;
    public function getTranslation(string $region) : string;
    public function countTranslation(): int;
}