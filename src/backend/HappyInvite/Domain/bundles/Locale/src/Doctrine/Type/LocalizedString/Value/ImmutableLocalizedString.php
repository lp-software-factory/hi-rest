<?php
namespace HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Exceptions\InvalidRegionException;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Exceptions\TranslationNotFoundException;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\LocalizedString;
use HappyInvite\Domain\Markers\JSONSerializable;

final class ImmutableLocalizedString implements JSONSerializable, LocalizedString
{
    /** @var string[] */
    private $translations = [];

    public function __construct(array $translations = [])
    {
        foreach($translations as $entity) {
            if(! (is_array($entity) && isset($entity['region']) && isset($entity['value']))) {
                throw new \Exception('Invalid LocaleInput definition');
            }

            $this->setTranslation($entity['region'], $entity['value']);
        }
    }

    public function toJSON(array $options = []): array
    {
        return $this->getDefinition();
    }

    public function toMutable(): MutableLocalizedString
    {
        return new MutableLocalizedString($this->getDefinition());
    }

    public function getDefinition(): array
    {
        $result = [];

        foreach($this->translations as $region => $value) {
            $result[] = [
                'region' => $region,
                'value' => $value
            ];
        }

        return $result;
    }

    private function setTranslation(string $region, string $value): self
    {
        $this->validateRegion($region);
        $this->translations[$region] = $value;

        return $this;
    }

    public function hasTranslation(string $region): bool
    {
        $this->validateRegion($region);

        return isset($this->translations[$region]) && is_string($this->translations[$region]);
    }

    public function getTranslation(string $region): string
    {
        $this->validateRegion($region);

        if($this->hasTranslation($region)) {
            return $this->translations[$region];
        }else{
            throw new TranslationNotFoundException(sprintf('Translation for region `%s` not found'));
        }
    }

    public function countTranslation(): int
    {
        return count($this->translations);
    }

    private function validateRegion(string $region)
    {
        $regex = '/^[a-z]{2}_[A-Z]{2}$/';

        if(! preg_match($regex, $region)) {
            throw new InvalidRegionException(sprintf('Invalid region identifier `%s`', $region));
        }
    }
}