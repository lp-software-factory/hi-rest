<?php
namespace HappyInvite\Domain\Bundles\Locale\Frontend;

use HappyInvite\Domain\Bundles\Frontend\FrontendScript;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\Domain\Bundles\Locale\Service\SessionLocaleService;

final class LocaleFrontendScript implements FrontendScript
{
    /** @var LocaleService */
    private $localeService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(LocaleService $localeService, SessionLocaleService $sessionLocaleService)
    {
        $this->localeService = $localeService;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function tags(): array
    {
        return [
            FrontendScript::TAG_GLOBAL,
        ];
    }

    public function __invoke(): array
    {
        return [
            'locale' => [
                'current' => $this->sessionLocaleService->getCurrentLocale()->toJSON(),
                'available' => array_map(function(Locale $locale) {
                    return $locale->toJSON();
                }, $this->localeService->getAllLocales()),
            ],
        ];
    }
}