<?php
namespace HappyInvite\Domain\Bundles\Locale\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Locale\Exceptions\LocaleNotFoundException;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;

final class LocaleRepository extends EntityRepository
{
    public function createLocale(Locale $locale): int
    {
        $this->getEntityManager()->persist($locale);
        $this->getEntityManager()->flush($locale);

        return $locale->getId();
    }

    public function deleteLocale(Locale $locale)
    {
        $this->getEntityManager()->remove($locale);
        $this->getEntityManager()->flush($locale);
    }

    public function hasLocale(string $language, string $region): bool
    {
        return $this->findOneBy([
            'language' => $language,
            'region' => $region,
        ]) !== null;
    }

    public function getAllLocales(): array
    {
        return $this->findAll();
    }

    public function getLocale(string $region): Locale
    {
        $result = $this->findOneBy([
            'region' => $region,
        ]);

        if($result instanceof Locale) {
            return $result;
        }else{
            throw new LocaleNotFoundException(sprintf('Locale "%s" not found', $region));
        }
    }

    public function getById(int $id): Locale
    {
        $result = $this->find($id);

        if($result instanceof Locale) {
            return $result;
        }else{
            throw new LocaleNotFoundException(sprintf('Locale `ID(%d)` not found', $id));
        }
    }

    public function loadLocales()
    {
        $this->findAll();
    }
}