<?php
namespace HappyInvite\Domain\Bundles\Locale\Console\Command;

use HappyInvite\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateLocaleCommand extends Command
{
    /** @var LocaleService */
    private $localeService;

    public function __construct(LocaleService $localeService)
    {
        $this->localeService = $localeService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('locale:create')
            ->setDescription('Create new locale')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('language', InputArgument::REQUIRED, 'Language (en)'),
                    new InputArgument('region', InputArgument::REQUIRED, 'Region (en_GB)'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $language = $input->getArgument('language');
        $region = $input->getArgument('region');

        $locale = $this->localeService->createLocale(new CreateLocaleParameters($language, $region));

        $output->writeln(sprintf(
            'Locale %s(%s) created',
            $locale->getLanguage(),
            $locale->getRegion()
        ));

        $output->writeln('');
    }
}