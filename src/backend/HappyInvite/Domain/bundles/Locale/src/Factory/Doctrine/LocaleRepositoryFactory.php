<?php
namespace HappyInvite\Domain\Bundles\Locale\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;

final class LocaleRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Locale::class;
    }
}