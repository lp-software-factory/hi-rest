<?php
namespace HappyInvite\Domain\Bundles\Locale\Factory;

use HappyInvite\Domain\Bundles\Locale\Config\LocaleConfig;
use Interop\Container\ContainerInterface;

final class LocaleConfigFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config.hi.domain.locale');

        return new LocaleConfig($config['default'], $config['fallback']);
    }
}