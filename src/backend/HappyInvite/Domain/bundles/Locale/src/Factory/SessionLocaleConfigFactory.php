<?php
namespace HappyInvite\Domain\Bundles\Locale\Factory;

use HappyInvite\Domain\Bundles\Locale\Config\SessionLocaleConfig;
use Interop\Container\ContainerInterface;

final class SessionLocaleConfigFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config.hi.domain.locale');

        return new SessionLocaleConfig(
            $config['session']['key'],
            $config['session']['default'],
            $config['session']['forced']
        );
    }
}