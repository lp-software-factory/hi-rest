<?php
namespace HappyInvite\Domain\Bundles\Locale\Functions;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\LocalizedString;

function localizedString(string $region, array $input): string
{
    return Chain::create($input)
        ->reduce(function(string $carry, array $next) use ($region) {
            return ($next['region'] === $region)
                ? $next['value']
                : $carry;
        }, '');
}