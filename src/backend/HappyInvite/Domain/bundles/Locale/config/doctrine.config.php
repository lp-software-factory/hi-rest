<?php
namespace HappyInvite\Domain\Bundles\Locale;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\LocalizedStringType;

return [
    'hi.config.domain.doctrine.types' => [
        'i18n' => LocalizedStringType::class,
    ]
];