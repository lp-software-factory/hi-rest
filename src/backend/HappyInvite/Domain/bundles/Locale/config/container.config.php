<?php
namespace HappyInvite\Domain\Bundles\Frontend;

use DI\Container;
use function DI\object;
use function DI\factory;
use function DI\get;

use HappyInvite\Domain\Bundles\Locale\Config\LocaleConfig;
use HappyInvite\Domain\Bundles\Locale\Config\SessionLocaleConfig;
use HappyInvite\Domain\Bundles\Locale\Factory\Doctrine\LocaleRepositoryFactory;
use HappyInvite\Domain\Bundles\Locale\Factory\LocaleConfigFactory;
use HappyInvite\Domain\Bundles\Locale\Factory\SessionLocaleConfigFactory;
use HappyInvite\Domain\Bundles\Locale\Repository\LocaleRepository;

return [
    SessionLocaleConfig::class => object()->constructorParameter('config', get('config.hi.domain.locale.session')),
    LocaleRepository::class => factory(LocaleRepositoryFactory::class),
    LocaleConfig::class => factory(LocaleConfigFactory::class),
    SessionLocaleConfig::class => factory(SessionLocaleConfigFactory::class),
];