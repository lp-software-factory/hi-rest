<?php
namespace HappyInvite\Domain\Bundles\Locale;

use HappyInvite\Domain\Bundles\Locale\Console\Command\CreateLocaleCommand;

return [
    'config.console' => [
        'commands' => [
            'Locale' => [
                CreateLocaleCommand::class,
            ],
        ],
    ],
];