<?php
namespace HappyInvite\Domain\Bundles\Locale;

return [
    'config.hi.domain.locale' => [
        'fallback' => [
            ['language' => 'ru', 'region' => 'ru_RU'],
            ['language' => 'en', 'region' => 'en_US'],
            ['language' => 'en', 'region' => 'en_GB'],
        ],
        'default' => ['language' => 'ru', 'region' => 'ru_RU'],
        'session' => [
            'key' => 'config.hi.domain.locale.session.key',
            'default' => 'ru_RU',
            'forced' => [
                'happy-invite.ru' => 'ru_RU',
                'happy-invite.com' => 'en_US',
            ]
        ]
    ],
];