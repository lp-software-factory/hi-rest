<?php
namespace HappyInvite\Domain\Bundles\Designer;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Designer\Factory\Doctrine\DesignerRepositoryFactory;
use HappyInvite\Domain\Bundles\Designer\Factory\Doctrine\DesignerRequestRepositoryFactory;
use HappyInvite\Domain\Bundles\Designer\Repository\DesignerRepository;
use HappyInvite\Domain\Bundles\Designer\Repository\DesignerRequestRepository;

return [
    DesignerRepository::class => factory(DesignerRepositoryFactory::class),
    DesignerRequestRepository::class => factory(DesignerRequestRepositoryFactory::class),
];