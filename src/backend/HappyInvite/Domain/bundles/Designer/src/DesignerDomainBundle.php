<?php
namespace HappyInvite\Domain\Bundles\Designer;

use HappyInvite\Domain\Bundles\Designer\Events\AppAccessSubscription;
use HappyInvite\Domain\Bundles\Designer\Events\DesignerMailSubscription;
use HappyInvite\Domain\Bundles\Designer\Events\DesignerServiceSubscription;
use HappyInvite\Domain\Bundles\Designer\Events\RegisterAccountSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class DesignerDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            DesignerMailSubscription::class,
            RegisterAccountSubscription::class,
            AppAccessSubscription::class,
            DesignerServiceSubscription::class,
        ];
    }
}