<?php
namespace HappyInvite\Domain\Bundles\Designer\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Designer\Criteria\ListDesignersCriteria;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerRequestNotFoundException;
use Doctrine\ORM\Query\Expr\OrderBy;

final class DesignerRequestRepository extends EntityRepository
{
    public function save(DesignerRequest $request): int
    {
        while($this->hasWithSID($request->getSID())) {
            $request->regenerateSID();
        }

        if(! $request->isPersisted()) {
            $this->getEntityManager()->persist($request);
        }

        $this->getEntityManager()->flush($request);

        return $request->getId();
    }

    public function delete(DesignerRequest $form)
    {
        $this->getEntityManager()->remove($form);
        $this->getEntityManager()->flush($form);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $designerRequestId): DesignerRequest
    {
        $result = $this->find($designerRequestId);

        if($result instanceof DesignerRequest) {
            return $result;
        }else{
            throw new DesignerRequestNotFoundException(sprintf('DesignerRequest `ID(%d)` not found', $designerRequestId));
        }
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): DesignerRequest
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof DesignerRequest) {
            return $result;
        }else{
            throw new DesignerRequestNotFoundException(sprintf('DesignerRequest `SID(%s)` not found', $sid));
        }
    }

    public function listDesignerRequests(ListDesignersCriteria $criteria): array
    {
        $query = $this->createQueryBuilder('c');

        $query->setFirstResult($criteria->getSeek()->getOffset());
        $query->setMaxResults($criteria->getSeek()->getLimit());
        $query->orderBy(new OrderBy('c.'.$criteria->getSort()->getField(), $criteria->getSort()->getDirection()));

        return $query->getQuery()->execute();
    }

    public function countDesignerRequests(ListDesignersCriteria $criteria): int
    {
        $query = $this->createQueryBuilder('c');
        $query->select('count(c)');

        return (int) $query->getQuery()->getSingleScalarResult();
    }
}