<?php
namespace HappyInvite\Domain\Bundles\Designer\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerNotFoundException;

final class DesignerRepository extends EntityRepository
{
    public function save(Designer $designer): int
    {
        while($this->hasWithSID($designer->getSID())) {
            $designer->regenerateSID();
        }

        if(! $designer->isPersisted()) {
            $this->getEntityManager()->persist($designer);
        }

        $this->getEntityManager()->flush($designer);

        return $designer->getId();
    }

    public function delete(Designer $form)
    {
        $this->getEntityManager()->remove($form);
        $this->getEntityManager()->flush($form);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getDesignerByRequest(DesignerRequest $request): Designer
    {
        $result = $this->findOneBy(['request' => $request]);

        if($result instanceof Designer) {
            return $result;
        }else{
            throw new DesignerNotFoundException(sprintf('Designer with `DesignerRequest(%d)` not found', $request->getId()));
        }
    }

    public function getById(int $designerId): Designer
    {
        $result = $this->find($designerId);

        if($result instanceof Designer) {
            return $result;
        }else{
            throw new DesignerNotFoundException(sprintf('Designer `ID(%d)` not found',
                $designerId));
        }
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Designer
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof Designer) {
            return $result;
        }else{
            throw new DesignerNotFoundException(sprintf('Designer `SID(%s)` not found', $sid));
        }
    }
}