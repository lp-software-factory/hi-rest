<?php
namespace HappyInvite\Domain\Bundles\Designer\Entity;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Designer\Repository\DesignerRepository")
 * @Table(name="designer")
 */
class Designer implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity,VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait,VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest")
     * @JoinColumn(name="designer_request_id", referencedColumnName="id")
     * @var DesignerRequest
     */
    private $request;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="profile_id", referencedColumnName="id")
     * @var Profile
     */
    private $profile;

    public function __construct(DesignerRequest $request, Profile $profile)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->profile = $profile;
        $this->request = $request;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function   getVersion(): string {
        return "1.0.0";
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'profile' => $this->getProfile()->toJSON($options),
            'request' => $this->getRequest()->toJSON($options),
        ];
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getRequest(): DesignerRequest
    {
        return $this->request;
    }
}