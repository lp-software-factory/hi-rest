<?php
namespace HappyInvite\Domain\Bundles\Designer\Entity;

use HappyInvite\Domain\Bundles\Designer\Form\DesignerRequestForm;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Designer\Repository\DesignerRequestRepository")
 * @Table(name="designer_request")
 */
class DesignerRequest implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    public const STATUS_NONE = null;
    public const STATUS_DECLINED = 0;
    public const STATUS_ACCEPTED = 1;

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="form", type="json_array")
     * @var array
     */
    private $form;

    /** @var DesignerRequestForm */
    private $cache;

    /**
     * @Column(name="is_reviewed", type="boolean")
     * @var bool
     */
    private $isReviewed = false;

    /**
     * @Column(name="date_reviewed", type="datetime")
     * @var \DateTime
     */
    private $dateReviewed;

    /**
     * @Column(name="status", type="integer")
     * @var int
     */
    private $status = self::STATUS_NONE;

    public function __construct(DesignerRequestForm $form)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->form = $form->toJSON();

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function   getVersion(): string {
        return "1.0.0";
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        $result = [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'is_reviewed' => $this->isReviewed(),
            'form' => $this->getFilledForm()->toJSON($options),
            'is_accepted' => false,
            'is_declined' => false,
        ];

        if($this->isReviewed()) {
            $result = array_merge($result, [
                'date_reviewed' => $this->getDateReviewed()->format(\DateTime::RFC2822),
                'is_accepted' => $this->isAccepted(),
                'is_declined' => $this->isDeclined(),
            ]);
        }

        return $result;
    }

    public function getFilledForm(): DesignerRequestForm
    {
        if(! $this->cache) {
            $this->cache = DesignerRequestForm::createFromJSON($this->form);
        }

        return $this->cache;
    }

    public function accept(): \DateTime
    {
        if($this->isReviewed()) {
            throw new \Exception('Designer request was reviewed earlier');
        }

        $this->isReviewed = true;
        $this->dateReviewed = new \DateTime();
        $this->status = self::STATUS_ACCEPTED;

        return $this->dateReviewed;
    }

    public function decline(): \DateTime
    {
        $this->isReviewed = true;
        $this->dateReviewed = new \DateTime();
        $this->status = self::STATUS_DECLINED;

        return $this->dateReviewed;
    }

    public function isReviewed(): bool
    {
        return $this->isReviewed;
    }

    public function getDateReviewed(): \DateTime
    {
        return $this->dateReviewed;
    }

    public function isAccepted(): bool
    {
        return $this->isReviewed() && $this->status === self::STATUS_ACCEPTED;
    }

    public function isDeclined(): bool
    {
        return $this->isReviewed() && $this->status === self::STATUS_DECLINED;
    }
}