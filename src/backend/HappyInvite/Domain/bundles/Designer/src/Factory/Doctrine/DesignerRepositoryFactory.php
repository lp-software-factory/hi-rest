<?php
namespace HappyInvite\Domain\Bundles\Designer\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class DesignerRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Designer::class;
    }
}