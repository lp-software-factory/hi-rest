<?php
namespace HappyInvite\Domain\Bundles\Designer\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class DesignerRequestRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return DesignerRequest::class;
    }
}