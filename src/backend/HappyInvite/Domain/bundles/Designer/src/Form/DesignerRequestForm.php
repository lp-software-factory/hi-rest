<?php
namespace HappyInvite\Domain\Bundles\Designer\Form;

use HappyInvite\Domain\Markers\JSONSerializable;

final class DesignerRequestForm implements JSONSerializable
{
    public const VERSION = "1.0.0";

    /**
     * Имя дизайнера
     * @var string
     */
    private $firstName;

    /**
     * Фамилия дизайнера
     * @var string
     */
    private $lastName;

    /**
     * E-mail, на которой будет зарегистрирован аккаунт
     * @var string
     */
    private $email;

    /**
     * Телефон (опционально)
     * @var string
     */
    private $phone;

    /**
     * Ссылка на портфолик
     * @var string
     */
    private $linkPortfolio;

    /**
     * Дополнительная информация
     * @var string
     */
    private $description;

    /**
     * Дизайнер согласился на условия оферты?
     * Мы храним данное значение исключительно в юридических целях.
     * @var bool
     */
    private $areTermsAccepted;

    public function __construct(
        string $firstName,
        string $lastName,
        string $email,
        string $phone,
        string $linkPortfolio,
        string $description,
        bool $areTermsAccepted
    )
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phone = $phone;
        $this->linkPortfolio = $linkPortfolio;
        $this->description = $description;
        $this->areTermsAccepted = $areTermsAccepted;
    }

    public static function createFromJSON(array $json): self
    {
        return new self(
            $json['first_name'],
            $json['last_name'],
            $json['email'],
            $json['phone'],
            $json['link_portfolio'],
            $json['description'],
            $json['are_terms_accepted']
        );
    }

    public function toJSON(array $options = []): array
    {
        return [
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'link_portfolio' => $this->getLinkPortfolio(),
            'description' => $this->getDescription(),
            'are_terms_accepted' => $this->areTermsAccepted,
        ];
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getLinkPortfolio(): string
    {
        return $this->linkPortfolio;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function areTermsAccepted(): bool
    {
        return $this->areTermsAccepted;
    }
}