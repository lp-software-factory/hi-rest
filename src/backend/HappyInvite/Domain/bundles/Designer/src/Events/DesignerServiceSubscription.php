<?php
namespace HappyInvite\Domain\Bundles\Designer\Events;

use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class DesignerServiceSubscription implements SubscriptionScript
{
    /** @var DesignerService */
    private $designerService;

    /** @var DesignerFormService */
    private $designerFormService;

    /** @var AccountService */
    private $accountService;

    public function __construct(
        DesignerService $designerService,
        DesignerFormService $designerFormService,
        AccountService $accountService
    ) {
        $this->designerService = $designerService;
        $this->designerFormService = $designerFormService;
        $this->accountService = $accountService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->designerFormService->getEventEmitter()->on(DesignerFormService::EVENT_ACCEPTED, function(DesignerRequest $request) {
            $this->designerService->registerDesigner($request, $this->accountService->getAccountWithEmail($request->getFilledForm()->getEmail())->getCurrentProfile());
        });
    }
}