<?php
namespace HappyInvite\Domain\Bundles\Designer\Events;

use HappyInvite\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use HappyInvite\Domain\Bundles\Account\Service\AccountRegistrationService;
use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Account\Service\ResetPasswordService;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;
use HappyInvite\Domain\Util\GenerateRandomString;

final class RegisterAccountSubscription implements SubscriptionScript
{
    /** @var AccountService */
    private $accountService;

    /** @var AccountRegistrationService */
    private $accountRegistrationService;

    /** @var ProfileService */
    private $profileService;

    /** @var ResetPasswordService */
    private $resetPasswordService;

    /** @var DesignerFormService */
    private $designerFormService;

    public function __construct(
        AccountService $accountService,
        AccountRegistrationService $accountRegistrationService,
        ProfileService $profileService,
        ResetPasswordService $resetPasswordService,
        DesignerFormService $designerFormService
    ) {
        $this->accountService = $accountService;
        $this->accountRegistrationService = $accountRegistrationService;
        $this->profileService = $profileService;
        $this->resetPasswordService = $resetPasswordService;
        $this->designerFormService = $designerFormService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $em = $this->designerFormService->getEventEmitter();

        $em->on(DesignerFormService::EVENT_ACCEPTED, function(DesignerRequest $request) {
            $form = $request->getFilledForm();
            $email = $form->getEmail();

            if(! $this->accountService->hasAccountWithEmail($email)) {
                $this->accountRegistrationService->registerDesignerAccount(new AccountRegisterParameters(
                    $email, GenerateRandomString::generate(32), new CreateProfileParameters(
                        $form->getFirstName(),
                        $form->getLastName()
                    )
                ));

                $this->resetPasswordService->sendResetPasswordRequest($email);
            }
        });
    }
}