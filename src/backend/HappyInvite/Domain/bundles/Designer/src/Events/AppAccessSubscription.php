<?php
namespace HappyInvite\Domain\Bundles\Designer\Events;

use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AppAccessSubscription implements SubscriptionScript
{
    /** @var AccountService */
    private $accountService;

    /** @var DesignerFormService */
    private $designerFormService;

    public function __construct(
        AccountService $accountService,
        DesignerFormService $designerFormService
    ) {
        $this->accountService = $accountService;
        $this->designerFormService = $designerFormService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $em = $this->designerFormService->getEventEmitter();

        $em->on(DesignerFormService::EVENT_ACCEPTED, function(DesignerRequest $request) {
            $account = $this->accountService->getAccountWithEmail($request->getFilledForm()->getEmail());
            $account->grantAccess('designer');

            $this->accountService->saveAccess($account);
        });
    }
}