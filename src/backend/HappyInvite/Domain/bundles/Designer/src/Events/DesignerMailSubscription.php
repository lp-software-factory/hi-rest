<?php
namespace HappyInvite\Domain\Bundles\Designer\Events;

use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerMailService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class DesignerMailSubscription implements SubscriptionScript
{
    /** @var DesignerFormService */
    private $designerFormService;

    /** @var DesignerMailService */
    private $designerMailService;

    public function __construct(DesignerFormService $designerFormService, DesignerMailService $designerMailService)
    {
        $this->designerFormService = $designerFormService;
        $this->designerMailService = $designerMailService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $ms = $this->designerMailService;
        $em = $this->designerFormService->getEventEmitter();

        $em->on(DesignerFormService::EVENT_SENT, function(DesignerRequest $request) use ($ms) {
            $ms->sendRequestSentEmail($request);
        });

        $em->on(DesignerFormService::EVENT_ACCEPTED, function(DesignerRequest $request) use ($ms) {
            $ms->sendRequestAcceptedEmail($request);
        });

        $em->on(DesignerFormService::EVENT_DECLINED, function(DesignerRequest $request) use ($ms) {
            $ms->sendRequestDeclinedEmail($request);
        });
    }
}