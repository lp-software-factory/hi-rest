<?php
namespace HappyInvite\Domain\Bundles\Designer\Criteria;

use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Domain\Markers\JSONSerializable;

final class ListDesignersCriteria implements JSONSerializable
{
    public const MAX_LIMIT = 1000;
    public const ALLOWED_SORT_FIELDS = [
        'id', 'date_created_at', 'last_updated_on', 'date_reviewed', 'is_reviewed', 'status'
    ];

    /** @var SeekCriteria */
    private $seek;

    /** @var SortCriteria */
    private $sort;

    public function __construct(SeekCriteria $seek, SortCriteria $sort)
    {
        if(! in_array($sort->getField(), self::ALLOWED_SORT_FIELDS)) {
            throw new \Exception(sprintf('Invalid sort field `%s`, allowed are `%s`', $sort->getField(), json_encode(self::ALLOWED_SORT_FIELDS)));
        }

        $this->seek = $seek;
        $this->sort = $sort;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'seek' => $this->getSeek()->toJSON($options),
            'sort' => $this->getSort()->toJSON($options),
        ];
    }

    public function getSeek(): SeekCriteria
    {
        return $this->seek;
    }

    public function getSort(): SortCriteria
    {
        return $this->sort;
    }
}