<?php
namespace HappyInvite\Domain\Bundles\Designer\Formatter;

use HappyInvite\Domain\Bundles\Designer\Entity\Designer;

final class DesignerFormatter
{
    const OPTION_USE_IDS = 'use_ids';

    public function formatOne(Designer $designer, array $options = [])
    {
        $options = array_merge([
            self::OPTION_USE_IDS => false,
        ], $options);

        $json = $designer->toJSON();

        if($options[self::OPTION_USE_IDS]) {
            $json['profile'] = null;
            $json['profile_id'] = $designer->getProfile()->getId();
            $json['request'] = null;
            $json['request_id'] = $designer->getRequest()->getId();
        }

        return $json;
    }

    public function formatMany(array $designers, array $options = [])
    {
        return array_map(function(Designer $designer) use ($options) {
            return $this->formatOne($designer, $options);
        }, $designers);
    }
}