<?php
namespace HappyInvite\Domain\Bundles\Designer\Service;

use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Repository\DesignerRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class DesignerService
{
    /** @var DesignerRepository */
    private $repository;

    public function __construct(DesignerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function registerDesigner(DesignerRequest $request, Profile $profile): Designer
    {
        $entity = new Designer($request, $profile);

        $this->repository->save($entity);

        return $entity;
    }

    public function unregister(int $designerId)
    {
        $this->repository->delete(
            $this->repository->getById($designerId)
        );
    }

    public function isHeADesigner(Profile $profile): bool
    {
        return $profile->getAccount()->hasAccess('designer');
    }

    public function hasDesignerWithProfileId(int $profileId): bool
    {
        /** @var Designer $designer */
        foreach($this->getAllDesigners() as $designer) {
            if($designer->getProfile()->getId() === $profileId) {
                return true;
            }
        }

        return false;
    }

    public function getAllDesigners(): array
    {
        /** @var Designer[] $designers */
        $designers = $this->repository->getAll();

        return $designers;
    }

    public function getDesignerByRequest(DesignerRequest $request)
    {
        return $this->repository->getDesignerByRequest($request);
    }
}