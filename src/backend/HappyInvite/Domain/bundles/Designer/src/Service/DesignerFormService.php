<?php
namespace HappyInvite\Domain\Bundles\Designer\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Designer\Criteria\ListDesignersCriteria;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerDuplicateEmailException;
use HappyInvite\Domain\Bundles\Designer\Exceptions\DesignerTermsAreNotAcceptedException;
use HappyInvite\Domain\Bundles\Designer\Form\DesignerRequestForm;
use HappyInvite\Domain\Bundles\Designer\Repository\DesignerRequestRepository;

final class DesignerFormService
{
    public const EVENT_SENT = 'hi.domain.designer.sent';
    public const EVENT_ACCEPTED = 'hi.domain.designer.accepted';
    public const EVENT_DECLINED = 'hi.domain.designer.declined';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var DesignerRequestRepository */
    private $formRepository;

    /** @var AccountService */
    private $accountService;

    public function __construct(DesignerRequestRepository $formRepository, AccountService $accountService)
    {
        $this->formRepository = $formRepository;
        $this->accountService = $accountService;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function sendDesignerRequestForm(DesignerRequestForm $filledForm): DesignerRequest
    {
        $email = $filledForm->getEmail();

        if(! $filledForm->areTermsAccepted()) {
            throw new DesignerTermsAreNotAcceptedException("You're not accepted terms.");
        }

        if($this->accountService->hasAccountWithEmail($email)) {
            $account = $this->accountService->getAccountWithEmail($email);

            if($account->hasAccess('designer')) {
                throw new DesignerDuplicateEmailException(sprintf('Designer with email `%s` already exists', $email));
            }
        }

        $this->formRepository->save(
            $request = new DesignerRequest($filledForm)
        );

        $this->eventEmitter->emit(self::EVENT_SENT, [$request]);

        return $request;
    }

    public function acceptDesignerRequest(int $requestId): DesignerRequest
    {
        $request = $this->formRepository->getById($requestId);
        $request->accept();

        $this->formRepository->save($request);

        $this->eventEmitter->emit(self::EVENT_ACCEPTED, [$request]);

        return $request;
    }

    public function declineDesignerRequest(int $requestId): DesignerRequest
    {
        $request = $this->formRepository->getById($requestId);
        $request->decline();

        $this->formRepository->save($request);

        $this->eventEmitter->emit(self::EVENT_DECLINED, [$request]);

        return $request;
    }

    public function listDesignerRequests(ListDesignersCriteria $criteria): array
    {
        return $this->formRepository->listDesignerRequests($criteria);
    }

    public function countDesignerRequests(ListDesignersCriteria $criteria): int
    {
        return $this->formRepository->countDesignerRequests($criteria);
    }
}