<?php
namespace HappyInvite\Domain\Bundles\Designer\Service;

use HappyInvite\Domain\Bundles\Designer\DesignerDomainBundle;
use HappyInvite\Domain\Bundles\Designer\Entity\DesignerRequest;
use HappyInvite\Domain\Bundles\Mail\Service\MailService;

final class DesignerMailService
{
    public const MAIL_TYPE_DESIGNER_REQUEST_SENT = 'hi.domain.designer.request-sent';
    public const MAIL_TYPE_DESIGNER_REQUEST_ACCEPTED = 'hi.domain.designer.request-accepted';
    public const MAIL_TYPE_DESIGNER_REQUEST_DECLINED = 'hi.domain.designer.request-declined';

    /** @var MailService */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendRequestSentEmail(DesignerRequest $request): \Swift_Message
    {
        $form = $request->getFilledForm();

        $replaces = $this->getReplacesFromDesignerRequest($request);

        $message = \Swift_Message::newInstance()
            ->setTo($form->getEmail())
            ->setContentType('text/html')
            ->setBody($this->mailService->getTemplate('designer', 'request-sent', $replaces));

        $this->mailService->sendNoReply($message, self::MAIL_TYPE_DESIGNER_REQUEST_SENT);

        return $message;
    }

    public function sendRequestAcceptedEmail(DesignerRequest $request)
    {
        $form = $request->getFilledForm();

        $replaces = $this->getReplacesFromDesignerRequest($request);

        $message = \Swift_Message::newInstance()
            ->setTo($form->getEmail())
            ->setContentType('text/html')
            ->setBody($this->mailService->getTemplate('designer', 'request-accepted', $replaces));

        $this->mailService->sendNoReply($message, self::MAIL_TYPE_DESIGNER_REQUEST_SENT);

        return $message;
    }

    public function sendRequestDeclinedEmail(DesignerRequest $request)
    {
        $form = $request->getFilledForm();

        $replaces = $this->getReplacesFromDesignerRequest($request);

        $message = \Swift_Message::newInstance()
            ->setTo($form->getEmail())
            ->setContentType('text/html')
            ->setBody($this->mailService->getTemplate('designer', 'request-declined', $replaces));

        $this->mailService->sendNoReply($message, self::MAIL_TYPE_DESIGNER_REQUEST_SENT);

        return $message;
    }

    public function getReplacesFromDesignerRequest(DesignerRequest $request):array
    {
        $form = $request->getFilledForm();

        return [
            '{{ DESIGNER_REQUEST_FIRST_NAME }}' => $form->getFirstName(),
            '{{ DESIGNER_REQUEST_LAST_NAME }}' => $form->getLastName(),
            '{{ DESIGNER_REQUEST_DESCRIPTION }}' => $form->getDescription(),
            '{{ DESIGNER_REQUEST_LINK_PORTFOLIO }}' => $form->getLinkPortfolio(),
            '{{ DESIGNER_REQUEST_PHONE }}' => $form->getPhone(),
            '{{ DESIGNER_REQUEST_EMAIL }}' => $form->getEmail(),
        ];
    }
}