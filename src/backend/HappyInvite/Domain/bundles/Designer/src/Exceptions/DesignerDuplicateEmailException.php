<?php
namespace HappyInvite\Domain\Bundles\Designer\Exceptions;

final class DesignerDuplicateEmailException extends \Exception {}