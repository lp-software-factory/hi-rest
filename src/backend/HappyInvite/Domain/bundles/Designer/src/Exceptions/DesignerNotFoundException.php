<?php
namespace HappyInvite\Domain\Bundles\Designer\Exceptions;

final class DesignerNotFoundException extends \Exception {}