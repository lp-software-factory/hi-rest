<?php
namespace HappyInvite\Domain\Bundles\Designer\Exceptions;

final class DesignerTermsAreNotAcceptedException extends \Exception {}