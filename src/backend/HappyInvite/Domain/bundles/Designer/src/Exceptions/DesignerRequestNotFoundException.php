<?php
namespace HappyInvite\Domain\Bundles\Designer\Exceptions;

final class DesignerRequestNotFoundException extends \Exception {}