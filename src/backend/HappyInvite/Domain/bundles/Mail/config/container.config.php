<?php
namespace HappyInvite\Domain\Bundles\Mail;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Mail\Factory\Doctrine\MailRepositoryFactory;
use HappyInvite\Domain\Bundles\Mail\Factory\SwiftTransportFactory;
use HappyInvite\Domain\Bundles\Mail\Repository\MailRepository;
use HappyInvite\Domain\Bundles\Mail\Spool\PHPUnitMemorySpool;

return [
    MailRepository::class => factory(MailRepositoryFactory::class),
    \Swift_MemorySpool::class => get(PHPUnitMemorySpool::class),
    \Swift_Events_EventDispatcher::class => get(\Swift_Events_SimpleEventDispatcher::class),
    \Swift_Transport::class => factory(SwiftTransportFactory::class),
];