<?php
namespace HappyInvite\Domain\Bundles\Mail\Spool;

final class PHPUnitMemorySpool extends \Swift_MemorySpool implements \Countable
{
    public function clear()
    {
        $this->messages = [];
    }

    public function count()
    {
        return count($this->messages);
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getLastMessage(): \Swift_Message
    {
        return $this->messages[count($this->messages) - 1];
    }
}