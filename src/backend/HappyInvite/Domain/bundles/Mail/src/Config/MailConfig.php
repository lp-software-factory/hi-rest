<?php
namespace HappyInvite\Domain\Bundles\Mail\Config;

final class MailConfig
{
    /** @var string */
    private $dir;

    /** @var string */
    private $host;

    /** @var string */
    private $from;

    /** @var string */
    private $fromName;

    public function __construct(array $config)
    {
        $this->dir = $config['dir'];
        $this->host = $config['host'];
        $this->from = $config['no-reply']['from'];
        $this->fromName = $config['no-reply']['name'];

    }

    public function getTemplateBaseDir(): string
    {
        return $this->dir;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }
}