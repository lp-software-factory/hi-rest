<?php
namespace HappyInvite\Domain\Bundles\Mail\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Mail\Entity\Mail;
use HappyInvite\Domain\Bundles\Mail\Exceptions\MailNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class MailRepository extends EntityRepository
{
    public function createMail(Mail $mail): int
    {
        while($this->hasWithSID($mail->getSID())) {
            $mail->regenerateSID();
        }

        $this->getEntityManager()->persist($mail);
        $this->getEntityManager()->flush($mail);

        return $mail->getId();
    }

    public function saveMail(Mail $mail)
    {
        $this->getEntityManager()->flush($mail);
    }

    public function deleteMail(Mail $mail)
    {
        $this->getEntityManager()->remove($mail);
        $this->getEntityManager()->flush($mail);
    }

    public function listMails(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());

        return $query->getQuery()->execute();
    }

    public function getById(int $id): Mail
    {
        $result = $this->find($id);

        if($result instanceof Mail) {
            return $result;
        }else{
            throw new MailNotFoundException(sprintf('Mail `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Mails with IDs (%s) not found', array_diff($ids, array_map(function(Mail $mail) {
                return $mail->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Mail
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof Mail) {
            return $result;
        }else{
            throw new MailNotFoundException(sprintf('Mail `SID(%s)` not found', $sid));
        }
    }
}