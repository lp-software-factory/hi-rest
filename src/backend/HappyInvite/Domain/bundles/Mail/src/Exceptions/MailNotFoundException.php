<?php
namespace HappyInvite\Domain\Bundles\Mail\Exceptions;

final class MailNotFoundException extends \Exception {}