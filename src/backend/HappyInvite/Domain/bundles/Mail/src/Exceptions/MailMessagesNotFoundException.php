<?php
namespace HappyInvite\Domain\Bundles\Mail\Exceptions;

final class MailMessagesNotFoundException extends \Exception {}