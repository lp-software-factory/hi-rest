<?php
namespace HappyInvite\Domain\Bundles\Mail\Exceptions;

final class MailTemplateNotFoundException extends \Exception {}