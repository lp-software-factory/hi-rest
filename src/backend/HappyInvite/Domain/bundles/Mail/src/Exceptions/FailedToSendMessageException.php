<?php
namespace HappyInvite\Domain\Bundles\Mail\Exceptions;

final class FailedToSendMessageException extends \Exception {}