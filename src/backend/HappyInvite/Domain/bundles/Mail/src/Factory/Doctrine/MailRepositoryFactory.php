<?php
namespace HappyInvite\Domain\Bundles\Mail\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Mail\Entity\Mail;

final class MailRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Mail::class;
    }
}