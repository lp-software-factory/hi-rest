<?php
namespace HappyInvite\Domain\Bundles\Mail\Factory;

use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\Service\EnvironmentService;
use Interop\Container\ContainerInterface;

final class SwiftTransportFactory
{
    public function __invoke(ContainerInterface $container, EnvironmentService $environmentService)
    {
        if($environmentService->getCurrent() === Environment::TESTING) {
            return new \Swift_Transport_SpoolTransport(
                new \Swift_Events_SimpleEventDispatcher(),
                $container->get(\Swift_MemorySpool::class)
            );
        }else{
            return new \Swift_Transport_MailTransport(
                new \Swift_Transport_SimpleMailInvoker(),
                new \Swift_Events_SimpleEventDispatcher()
            );
        }
    }
}