<?php
namespace HappyInvite\Domain\Bundles\Mail;

use HappyInvite\Platform\Bundles\HIBundle;

final class MailBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}