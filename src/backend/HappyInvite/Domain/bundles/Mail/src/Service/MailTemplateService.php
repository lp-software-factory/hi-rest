<?php
namespace HappyInvite\Domain\Bundles\Mail\Service;

use DI\Container;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\Domain\Bundles\Mail\Config\MailConfig;
use HappyInvite\Domain\Bundles\Mail\Exceptions\MailTemplateNotFoundException;
use HappyInvite\Domain\Service\ServerConfig;

final class MailTemplateService
{
    /** @var Container*/
    private $container;

    /** @var MailConfig */
    private $config;

    /** @var LocaleService */
    private $localeService;

    /** @var ServerConfig */
    private $serverConfig;

    public function __construct(
        Container $container,
        LocaleService $localeService,
        MailConfig $mailConfig,
        ServerConfig $serverConfig
    ) {
        $this->container = $container;
        $this->config = $mailConfig;
        $this->localeService = $localeService;
        $this->serverConfig = $serverConfig;
    }

    public function getTemplate(string $dir, string $file, array $replaces): string
    {
        $locales = [
            $this->localeService->getCurrentLocale(),
            $this->localeService->getFallbackLocale(),
        ];

        $replaces = array_merge($replaces, [
            '{{ PROTOCOL }}' => $this->serverConfig->getProtocol(),
            '{{ PORT }}' => $this->serverConfig->getPort(),
            '{{ HOST }}' => $this->serverConfig->getHost(),
        ]);

        /** @var Locale $locale */
        foreach($locales as $locale) {
            $fileName = sprintf('%s/%s/%s/%s.%s/index.html', $this->config->getTemplateBaseDir() , $dir, $file, $locale->getLanguage(), $locale->getRegion());

            if(file_exists($fileName)) {
                return str_replace(array_keys($replaces), array_values($replaces), file_get_contents($fileName));
            }
        }

        throw new MailTemplateNotFoundException(sprintf('No mail template found for `%s::%s`', $dir, $file));
    }
}