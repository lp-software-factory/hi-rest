<?php
namespace HappyInvite\Domain\Bundles\Mail\Service;

use HappyInvite\Domain\Bundles\Mail\Config\MailConfig;
use HappyInvite\Domain\Bundles\Mail\Entity\Mail;
use HappyInvite\Domain\Bundles\Mail\Exceptions\FailedToSendMessageException;
use HappyInvite\Domain\Bundles\Mail\Repository\MailRepository;

final class MailService
{
    /** @var bool */
    private $enabled = true;

    /** @var \Swift_Mailer */
    private $mailer;

    /** @var MailRepository */
    private $mailRepository;

    /** @var MailConfig */
    private $mailConfig;

    /** @var MailTemplateService */
    private $mailTemplateService;

    public function __construct(
        \Swift_Mailer $mailer,
        MailRepository $mailRepository,
        MailTemplateService $mailTemplateService,
        MailConfig $noReply
    ) {
        $this->mailer = $mailer;
        $this->mailRepository = $mailRepository;
        $this->mailTemplateService = $mailTemplateService;
        $this->mailConfig = $noReply;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }

    public function getTemplate(string $dir, string $file, array $replaces): string
    {
        return $this->mailTemplateService->getTemplate($dir, $file, $replaces);
    }

    public function sendNoReply(\Swift_Message $message, string $mailType): Mail
    {
        $message->setFrom([$this->mailConfig->getFrom() => $this->mailConfig->getFromName()]);

        return $this->send($message, $mailType);
    }

    public function send(\Swift_Message $message, string $mailType): Mail
    {
        if(! strlen($message->getSubject())) {
            preg_match('/\<title\>(.*)\<\/title\>/', $message->getBody(), $matches);

            if(isset($matches[1])) {
                $message->setSubject($matches[1]);
            }else{
                $message->setSubject('Happy Invite');
            }
        }

        if(defined('HI_DISABLE_MAIL_SERVICE')) {
            $messageCopy = $this->createMessageCopy($message, $mailType);

            return $messageCopy;
        }

        if($this->isEnabled()) {
            if($this->mailer->send($message) === 0) {
                throw new FailedToSendMessageException(sprintf('Failed to send message `%s`',$message->getSubject()));
            }
        }

        $messageCopy = $this->createMessageCopy($message, $mailType);

        $this->mailRepository->createMail($messageCopy);

        return $messageCopy;
    }

    private function createMessageCopy(\Swift_Message $message, string $mailType):Mail
    {
        $messageCopy = new Mail(
            $mailType,
            $message->getSubject(),
            array_keys($message->getFrom())[0],
            array_keys($message->getTo())[0],
            $message->getContentType(),
            $message->getBody()
        );

        return $messageCopy;
    }
}