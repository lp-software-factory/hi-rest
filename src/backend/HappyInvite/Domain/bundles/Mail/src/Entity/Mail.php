<?php
namespace HappyInvite\Domain\Bundles\Mail\Entity;

use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Mail\Repository\MailRepository")
 * @Table(name="mail")
 */
class Mail implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="date_created_at", type="datetime")
     * @var \DateTime
     */
    private $dateCreatedAt;

    /**
     * @Column(name="mail_type", type="string")
     * @var string
     */
    private $mailType;

    /**
     * @Column(name="subject", type="string")
     * @var string
     */
    private $subject;

    /**
     * @Column(name="from_address", type="string")
     * @var string
     */
    private $from;

    /**
     * @Column(name="to_address", type="string")
     * @var string
     */
    private $to;

    /**
     * @Column(name="content_type", type="string")
     * @var string
     */
    private $contentType;

    /**
     * @Column(name="body", type="string")
     * @var string
     */
    private $body;

    public function __construct(
        string $mailType,
        string $subject,
        string $from,
        string $to,
        string $contentType,
        string $body
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->mailType = $mailType;
        $this->subject = $subject;
        $this->from = $from;
        $this->contentType = $contentType;
        $this->to = $to;
        $this->body = $body;

        $this->dateCreatedAt = new \DateTime();
        $this->initModificationEntity();
        $this->regenerateSID();
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'mail_type' => $this->getMailType(),
            'subject' => $this->getSubject(),
            'from' => $this->getFrom(),
            'to' => $this->getTo(),
            'content_type' => $this->getContentType(),
            'body' => $this->getBody(),
        ];
    }

    public function getMetadataVersion(): string
    {
        switch($this->getMailType()) {
            default:
                return "1.0.0";
        }
    }

    public function getDateCreatedAt(): \DateTime
    {
        return $this->dateCreatedAt;
    }

    public function getMailType(): string
    {
        return $this->mailType;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function getBody(): string
    {
        return $this->body;
    }
}