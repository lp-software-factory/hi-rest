<?php
namespace HappyInvite\Domain\Bundles\Event;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\EventAnalyticsDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\EventMailingDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\EventRecipientsGroupDomainBundle;
use HappyInvite\Platform\Bundles\HIBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\EventTypeDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\EventTypeGroupDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\EventDomainBundle as EventEntityDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\EventLandingTemplateDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\EventMailingOptionsDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\EventRecipientsDomainBundle;

final class EventDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new EventTypeDomainBundle(),
            new EventTypeGroupDomainBundle(),
            new EventEntityDomainBundle(),
            new EventLandingTemplateDomainBundle(),
            new EventMailingOptionsDomainBundle(),
            new EventRecipientsDomainBundle(),
            new EventRecipientsGroupDomainBundle(),
            new EventMailingDomainBundle(),
            new EventAnalyticsDomainBundle(),
        ];
    }
}