<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

final class EditEventTypeGroupParameters
{
    /** @var string */
    private $url;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    public function __construct(
        string $url,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description
    ) {
        $this->url = $url;
        $this->title = $title;
        $this->description = $description;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }
}