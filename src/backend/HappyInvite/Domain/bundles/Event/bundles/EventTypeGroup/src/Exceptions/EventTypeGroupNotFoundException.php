<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Exceptions;

final class EventTypeGroupNotFoundException extends \Exception {}