<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Exceptions\EventTypeGroupNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventTypeGroupRepository extends EntityRepository
{
    public function createEventTypeGroup(EventTypeGroup $eventTypeGroup): int
    {
        while($this->hasWithSID($eventTypeGroup->getSID())) {
            $eventTypeGroup->regenerateSID();
        }

        $this->getEntityManager()->persist($eventTypeGroup);
        $this->getEntityManager()->flush($eventTypeGroup);

        return $eventTypeGroup->getId();
    }

    public function saveEventTypeGroup(EventTypeGroup $eventTypeGroup)
    {
        $this->getEntityManager()->flush($eventTypeGroup);
    }

    public function saveEventTypeGroupEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEventTypeGroup(EventTypeGroup $eventTypeGroup)
    {
        $this->getEntityManager()->remove($eventTypeGroup);
        $this->getEntityManager()->flush($eventTypeGroup);
    }

    public function listEventTypeGroups(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventTypeGroup
    {
        $result = $this->find($id);

        if($result instanceof EventTypeGroup) {
            return $result;
        }else{
            throw new EventTypeGroupNotFoundException(sprintf('EventTypeGroup `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('EventTypeGroups with IDs (%s) not found', array_diff($ids, array_map(function(EventTypeGroup $eventTypeGroup) {
                return $eventTypeGroup->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventTypeGroup
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof EventTypeGroup) {
            return $result;
        }else{
            throw new EventTypeGroupNotFoundException(sprintf('EventTypeGroup `SID(%s)` not found', $sid));
        }
    }
}