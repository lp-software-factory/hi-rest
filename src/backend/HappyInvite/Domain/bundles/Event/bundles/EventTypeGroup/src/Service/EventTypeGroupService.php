<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service;

use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters\CreateEventTypeGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters\EditEventTypeGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Repository\EventTypeGroupRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EventTypeGroupService
{
    /** @var EventTypeGroupRepository */
    private $eventTypeGroupRepository;

    public function __construct(EventTypeGroupRepository $eventTypeGroupRepository)
    {
        $this->eventTypeGroupRepository = $eventTypeGroupRepository;
    }

    public function createEventTypeGroup(CreateEventTypeGroupParameters $parameters): EventTypeGroup
    {
        $siblings = $this->getAllEventTypeGroups();

        $eventTypeGroup = new EventTypeGroup(
            $parameters->getUrl(),
            $parameters->getTitle(),
            $parameters->getDescription()
        );

        $serial = new SerialManager($siblings);
        $serial->insertLast($eventTypeGroup);
        $serial->normalize();

        $this->eventTypeGroupRepository->createEventTypeGroup($eventTypeGroup);
        $this->eventTypeGroupRepository->saveEventTypeGroupEntities($siblings);

        return $eventTypeGroup;
    }

    public function editEventTypeGroup(int $eventTypeGroupId, EditEventTypeGroupParameters $parameters): EventTypeGroup
    {
        $eventTypeGroup = $this->getEventTypeGroupById($eventTypeGroupId);
        $eventTypeGroup->setUrl($parameters->getUrl());
        $eventTypeGroup->setTitle($parameters->getTitle());
        $eventTypeGroup->setDescription($parameters->getDescription());

        $this->eventTypeGroupRepository->saveEventTypeGroup($eventTypeGroup);

        return $eventTypeGroup;
    }

    public function moveUpEventTypeGroup(int $eventTypeGroupId): int
    {
        $eventTypeGroup = $this->getEventTypeGroupById($eventTypeGroupId);
        $siblings = $this->getAllEventTypeGroups();

        $serial = new SerialManager($siblings);
        $serial->up($eventTypeGroup);
        $serial->normalize();
        $this->eventTypeGroupRepository->saveEventTypeGroupEntities($siblings);

        return $eventTypeGroup->getPosition();
    }

    public function moveDownEventTypeGroup(int $eventTypeGroupId): int
    {
        $eventTypeGroup = $this->getEventTypeGroupById($eventTypeGroupId);
        $siblings = $this->getAllEventTypeGroups();

        $serial = new SerialManager($siblings);
        $serial->down($eventTypeGroup);
        $serial->normalize();
        $this->eventTypeGroupRepository->saveEventTypeGroupEntities($siblings);

        return $eventTypeGroup->getPosition();
    }

    public function deleteEventTypeGroup(int $eventTypeGroupId)
    {
        $this->eventTypeGroupRepository->deleteEventTypeGroup(
            $this->getEventTypeGroupById($eventTypeGroupId)
        );
    }

    public function getEventTypeGroupById(int $eventTypeGroupId): EventTypeGroup
    {
        return $this->eventTypeGroupRepository->getById($eventTypeGroupId);
    }

    public function getEventTypeGroupByIds(IdsCriteria $criteria): array
    {
        return $this->eventTypeGroupRepository->getByIds($criteria);
    }

    public function getAllEventTypeGroups(): array
    {
        return $this->eventTypeGroupRepository->getAll();
    }
}