<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup;

final class EventTypeGroupRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventTypeGroup::class;
    }
}