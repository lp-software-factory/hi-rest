<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup;

use HappyInvite\Platform\Bundles\HIBundle;

final class EventTypeGroupDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}