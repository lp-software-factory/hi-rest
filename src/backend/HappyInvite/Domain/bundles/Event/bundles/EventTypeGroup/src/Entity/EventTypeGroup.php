<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\URLEntity\URLEntity;
use HappyInvite\Domain\Markers\URLEntity\URLEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Repository\EventTypeGroupRepository")
 * @Table(name="event_type_group")
 */
class EventTypeGroup implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, SerialEntity, VersionEntity, URLEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait, VersionEntityTrait, URLEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType", mappedBy="group", cascade={"all"})
     * @var Collection
     */
    private $eventTypes;

    public function __construct(string $url, ImmutableLocalizedString $title, ImmutableLocalizedString $description)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->eventTypes = new ArrayCollection();

        $this->setUrl($url);
        $this->regenerateSID();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            "position" => $this->getPosition(),
            "url" => $this->getUrl(),
            "title" => $this->getTitle()->toJSON($options),
            "description" => $this->getDescription()->toJSON($options),
            "event_types" => array_map(function(EventType $eventType) {
                return $eventType->toJSON();
            }, $this->eventTypes->toArray()),
        ];
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title)
    {
        $this->title = $title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description)
    {
        return $this->description = $description;
    }

    /** @return EventType[] */
    public function getEventTypes(): array
    {
        return $this->eventTypes->toArray();
    }
}