<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup;

final class EventTypeGroupFormatter
{
    public const OPTION_WITH_EVENT_TYPES_JSON = 'with_event_types_json';
    public const OPTION_WITH_EVENT_TYPES_IDS = 'with_event_types_ids';

    public function formatOne(EventTypeGroup $eventTypeGroup, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_WITH_EVENT_TYPES_JSON => true,
            self::OPTION_WITH_EVENT_TYPES_IDS => false,
        ], $options);

        $json = $eventTypeGroup->toJSON();

        if(! $options[self::OPTION_WITH_EVENT_TYPES_JSON]) {
            $json['event_types'] = null;
        }

        if($options[self::OPTION_WITH_EVENT_TYPES_IDS]) {
            $json['event_type_ids'] = array_map(function(EventType $eventType) {
                return $eventType->getId();
            }, $eventTypeGroup->getEventTypes());
        }

        return $json;
    }

    public function formatMany(array $eventTypeGroups, array $options = []): array
    {
        return array_map(function(EventTypeGroup $group) use ($options) {
            return $this->formatOne($group, $options);
        }, $eventTypeGroups);
    }
}