<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Factory\Doctrine\EventTypeGroupRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Repository\EventTypeGroupRepository;

return [
    EventTypeGroupRepository::class => factory(EventTypeGroupRepositoryFactory::class),
];