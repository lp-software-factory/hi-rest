<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Entity\EventRecipients;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Exceptions\EventRecipientsNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventRecipientsRepository extends EntityRepository
{
    public function createRecipients(EventRecipients $recipients): int
    {
        while ($this->hasWithSID($recipients->getSID())) {
            $recipients->regenerateSID();
        }

        $this->getEntityManager()->persist($recipients);
        $this->getEntityManager()->flush($recipients);

        return $recipients->getId();
    }

    public function saveRecipients(EventRecipients $recipients)
    {
        $this->getEntityManager()->flush($recipients);
    }

    public function saveRecipientsEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveRecipients($entity);
        }
    }

    public function deleteRecipients(EventRecipients $recipients)
    {
        $this->getEntityManager()->remove($recipients);
        $this->getEntityManager()->flush($recipients);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventRecipients
    {
        $result = $this->find($id);

        if ($result instanceof EventRecipients) {
            return $result;
        } else {
            throw new EventRecipientsNotFoundException(sprintf('Event recipients recipients `ID(%d)` not found', $id));
        }
    }

    public function getByEventId(int $eventId): EventRecipients
    {
        $result = $this->findOneBy([
            'event' => $eventId,
        ]);

        if ($result instanceof EventRecipients) {
            return $result;
        } else {
            throw new EventRecipientsNotFoundException(sprintf('Event recipients recipients `EventId(%d)` not found', $eventId));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Event recipients recipientss with IDs (%s) not found', array_diff($ids, array_map(function (EventRecipients $recipients) {
                return $recipients->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventRecipients
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EventRecipients) {
            return $result;
        } else {
            throw new EventRecipientsNotFoundException(sprintf('Event recipients recipients `SID(%s)` not found', $sid));
        }
    }
}