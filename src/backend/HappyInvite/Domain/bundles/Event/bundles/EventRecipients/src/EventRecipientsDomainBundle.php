<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Subscriptions\AutoCreateEventRecipientsSubscriptionScript;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Subscriptions\AutoDestroyEventRecipientsSubscriptionScript;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EventRecipientsDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoCreateEventRecipientsSubscriptionScript::class,
            AutoDestroyEventRecipientsSubscriptionScript::class,
        ];
    }
}