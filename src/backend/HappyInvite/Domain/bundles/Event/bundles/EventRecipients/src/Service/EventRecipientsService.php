<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Entity\EventRecipients;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Parameters\EditEventRecipientsParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Repository\EventRecipientsRepository;

final class EventRecipientsService
{
    public const EVENT_CREATED = 'hi.domain.event-recipients.recipients.created';
    public const EVENT_EDITED = 'hi.domain.event-recipients.recipients.edited';
    public const EVENT_DELETE = 'hi.domain.event-recipients.recipients.delete';
    public const EVENT_DELETED = 'hi.domain.event-recipients.recipients.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EventRecipientsRepository */
    private $recipientsRepository;

    /** @var AuthToken */
    private $authToken;

    public function __construct(EventRecipientsRepository $eventRecipientsRepository, AuthToken $authToken)
    {
        $this->eventEmitter = new EventEmitter();
        $this->recipientsRepository = $eventRecipientsRepository;
        $this->authToken = $authToken;
    }

    private function validateOwner(Event $event): void {
        if(! $this->authToken->isSignedIn()) {
            throw new EventIsNotYoursException("You're not signed in.");
        }

        if(! $this->authToken->getAccount()->hasAccess('admin') && ! ($event->getOwner()->getId() === $this->authToken->getProfile()->getId())) {
            throw new EventIsNotYoursException(sprintf('Event with ID %s is not yours', $event->getId()));
        }
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEventRecipientsForEvent(Event $event): EventRecipients
    {
        $this->recipientsRepository->createRecipients($entity = new EventRecipients($event, []));
        $this->eventEmitter->emit(self::EVENT_CREATED, [$entity, $event]);

        return $entity;
    }

    public function deleteEventRecipients(int $id): void
    {
        $recipients = $this->recipientsRepository->getById($id);
        $recipientsId = $recipients->getId();

        $this->validateOwner($recipients->getEvent());

        $this->eventEmitter->emit(self::EVENT_DELETE, [$recipients, $recipientsId]);
        $this->recipientsRepository->deleteRecipients($recipients);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$recipients, $recipientsId]);
    }

    public function deleteEventRecipientsOfEvent(int $eventId): void
    {
        $recipients = $this->recipientsRepository->getByEventId($eventId);
        $recipientsId = $recipients->getId();

        $this->validateOwner($recipients->getEvent());

        $this->eventEmitter->emit(self::EVENT_DELETE, [$recipients, $recipientsId]);
        $this->recipientsRepository->deleteRecipients($recipients);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$recipients, $recipientsId]);
    }

    public function editEventRecipients(int $id, EditEventRecipientsParameters $parameters): EventRecipients
    {
        $recipients = $this->recipientsRepository->getById($id);

        $this->validateOwner($recipients->getEvent());

        $recipients->setContactsFromJSON($parameters->getContacts());
        $this->recipientsRepository->saveRecipients($recipients);
        $this->eventEmitter->emit(self::EVENT_EDITED, [$recipients]);

        return $recipients;
    }

    public function saveMailingChanges(EventRecipients $eventRecipients): void
    {
        $this->recipientsRepository->saveRecipients($eventRecipients);
    }

    public function getById(int $id): EventRecipients
    {
        return $this->recipientsRepository->getById($id);
    }

    public function getByEventId(int $eventId): EventRecipients
    {
        return $this->recipientsRepository->getByEventId($eventId);
    }
}