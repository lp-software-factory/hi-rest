<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoDestroyEventRecipientsSubscriptionScript implements SubscriptionScript
{
    /** @var EventService */
    private $eventService;

    /** @var EventRecipientsService */
    private $eventRecipientsService;

    public function __construct(
        EventService $eventService,
        EventRecipientsService $eventRecipientsService
    ) {
        $this->eventService = $eventService;
        $this->eventRecipientsService = $eventRecipientsService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->eventService->getEventEmitter()->on(EventService::EVENT_DELETE, function(Event $event) {
            $this->eventRecipientsService->deleteEventRecipientsOfEvent($event->getId());
        });
    }
}