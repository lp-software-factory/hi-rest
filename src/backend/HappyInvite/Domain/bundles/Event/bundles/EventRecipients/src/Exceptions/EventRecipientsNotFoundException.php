<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Exceptions;

final class EventRecipientsNotFoundException extends \Exception {}