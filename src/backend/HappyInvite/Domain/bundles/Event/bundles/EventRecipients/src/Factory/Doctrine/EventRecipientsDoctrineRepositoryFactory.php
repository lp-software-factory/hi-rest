<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Entity\EventRecipients;

final class EventRecipientsDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventRecipients::class;
    }
}