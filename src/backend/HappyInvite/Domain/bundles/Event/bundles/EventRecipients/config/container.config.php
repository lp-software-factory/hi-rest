<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Factory\Doctrine\EventRecipientsDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Repository\EventRecipientsRepository;

return [
    EventRecipientsRepository::class => factory(EventRecipientsDoctrineRepositoryFactory::class),
];