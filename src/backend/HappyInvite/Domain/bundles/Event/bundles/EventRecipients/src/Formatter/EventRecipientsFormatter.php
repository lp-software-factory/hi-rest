<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Entity\EventRecipients;

final class EventRecipientsFormatter
{
    public function formatOne(EventRecipients $entity): array
    {
        return [
            'entity' => $entity->toJSON(),
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EventRecipients $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}