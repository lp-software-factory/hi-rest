<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Exceptions\EventRecipientContactNotFoundException;

final class EventRecipientContactsManager implements \Countable
{
    /** @var array */
    private $contacts = [];

    public function normalize(): self
    {
        $usedSIDs = [];

        /** @var EventRecipientContact $contact */
        foreach($this->contacts as $contact) {
            if(in_array($contact->getSID(), $usedSIDs, true)) {
                $contact->regenerateSID();

                return $this->normalize();
            }

            $usedSIDs[] = $contact->getSID();
        }

        return $this;
    }

    public function importFromJSON(array $input): self
    {
        array_map(function(array $json) {
            $this->contacts[] = new EventRecipientContact($json);
        }, $input);

        $this->normalize();

        return $this;
    }

    public function importFromPHPObjects(array $input): self
    {
        array_map(function(EventRecipientContact $contact) {
            $this->contacts[] = $contact;
        }, $input);

        $this->normalize();

        return $this;
    }

    public function exportsAsJSON(): array
    {
        return array_map(function(EventRecipientContact $contact) {
            return $contact->toJSON();
        }, $this->contacts);
    }

    public function count(): int
    {
        return count($this->contacts);
    }

    public function clear(): self
    {
        $this->contacts = [];

        return $this;
    }

    public function all(): array
    {
        return $this->contacts;
    }

    public function hasContactWithSID(string $sid): bool
    {
        return count(array_filter($this->contacts, function(EventRecipientContact $input) use ($sid) {
                return $input->getSID() === $sid;
            })) === 1;
    }

    public function getContactBySID(string $sid): EventRecipientContact
    {
        $result = array_filter($this->contacts, function(EventRecipientContact $input) use ($sid) {
            return $input->getSID() === $sid;
        });

        if(count($result) === 0) {
            throw new EventRecipientContactNotFoundException(sprintf('EventRecipientContact(SID: %s) not found', $sid));
        }

        if(count($result) > 1) {
            throw new EventRecipientContactNotFoundException(sprintf('EventRecipientContact(SID: %s) found but there are some duplicates here!', $sid));
        }

        return $result[0];
    }
}