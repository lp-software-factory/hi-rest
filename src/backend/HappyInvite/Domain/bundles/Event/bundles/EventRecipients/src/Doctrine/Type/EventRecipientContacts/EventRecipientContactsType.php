<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class EventRecipientContactsType extends Type
{
    public const TYPE_NAME = 'event_recipient_contacts';

    public function getName()
    {
        return self::TYPE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if($value instanceof EventRecipientContactsManager) {
            return json_encode($value->exportsAsJSON());
        }else{
            throw new \Exception(sprintf('Value is not an instance of %s', EventRecipientContactsManager::class));
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $manager = new EventRecipientContactsManager();

        if (! ($value === null || $value === '')) {
            $value = (is_resource($value)) ? stream_get_contents($value) : $value;
            $json = json_decode($value, true);

            $manager->importFromJSON($json);
        }

        return $manager;
    }
}