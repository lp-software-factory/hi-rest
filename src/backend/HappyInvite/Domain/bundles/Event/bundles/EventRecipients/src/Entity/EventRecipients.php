<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Entity;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts\EventRecipientContactsManager;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Repository\EventRecipientsRepository")
 * @Table(name="event_recipients")
 */
class EventRecipients implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event")
     * @JoinColumn(name="event_id", referencedColumnName="id")
     * @var Event
     */
    private $event;

    /**
     * @Column(name="contacts", type="event_recipient_contacts")
     * @var EventRecipientContactsManager
     */
    private $contacts;

    public function __construct(Event $event, array $contacts)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->event = $event;
        $this->contacts = new EventRecipientContactsManager();

        $this->contacts->importFromJSON($contacts);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'event_id' => $this->getEvent()->getId(),
            'contacts' => $this->getContacts()->exportsAsJSON(),
        ];

        return $json;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getEvent(): Event
    {
        return $this->event;
    }

    public function getContacts(): EventRecipientContactsManager
    {
        return $this->contacts;
    }

    public function setContactsFromJSON(array $contacts): self
    {
        $this->contacts = new EventRecipientContactsManager();
        $this->contacts->importFromJSON($contacts);

        $this->markAsUpdated();

        return $this;
    }
}