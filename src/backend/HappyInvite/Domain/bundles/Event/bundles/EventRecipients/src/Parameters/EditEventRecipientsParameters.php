<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Parameters;

final class EditEventRecipientsParameters
{
    /** @var array */
    private $contacts;

    public function __construct(array $contacts)
    {
        $this->contacts = $contacts;
    }

    public function getContacts(): array
    {
        return $this->contacts;
    }
}