<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts\EventRecipientContactsType;

return [
    'hi.config.domain.doctrine.types' => [
        EventRecipientContactsType::TYPE_NAME => EventRecipientContactsType::class,
    ],
];