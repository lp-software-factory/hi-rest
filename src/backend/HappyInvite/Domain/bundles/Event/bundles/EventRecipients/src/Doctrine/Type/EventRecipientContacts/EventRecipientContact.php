<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts;

use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Util\GenerateRandomString;

final class EventRecipientContact implements JSONSerializable, SIDEntity
{
    /** @var string */
    private $sid;

    /** @var string */
    private $email;

    /** @var string|null */
    private $firstName;

    /** @var string|null */
    private $lastName;

    /** @var string|null */
    private $middleName;

    /** @var string|null */
    private $phone;

    /** @var bool */
    private $referral = false;

    /** @var string|null */
    private $with;

    /** @var int|null */
    private $groupId;

    public function __construct(array $input)
    {
        if(! self::isValidInput($input)) {
            throw new \Exception('Unable to create EventRecipientRecord with given input');
        }

        $this->sid = $input['sid'];
        $this->email = $input['email'];
        $this->firstName = $input['first_name'] ?? null;
        $this->lastName = $input['last_name'] ?? null;
        $this->middleName = $input['middle_name'] ?? null;
        $this->phone = $input['phone'] ?? null;
        $this->referral = $input['referral'] ?? false;
        $this->with = $input['width'] ?? null;
        $this->groupId =  $input['group_id'] ?? null;
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            'sid' => $this->getSID(),
            'email' => $this->getEmail(),
            'referral' => $this->isReferral(),
        ];

        if($this->hasFirstName()) {
            $json['first_name'] = $this->getFirstName();
        }

        if($this->hasLastName()) {
            $json['last_name'] = $this->getLastName();
        }

        if($this->hasMiddleName()) {
            $json['middle_name'] = $this->getMiddleName();
        }

        if($this->hasPhone()) {
            $json['phone'] = $this->getPhone();
        }

        if($this->hasGroupId()) {
            $json['group_id'] = $this->getGroupId();
        }

        if($this->hasWith()) {
            $json['with'] = $this->getWith();
        }

        return $json;
    }

    public function regenerateSID(): string
    {
        $this->sid = GenerateRandomString::generate(self::SID_LENGTH);

        return $this->sid;
    }

    public static function isValidInput(array $input): bool
    {
        foreach(['sid', 'email'] as $requiredField) {
            if(! (isset($input[$requiredField]) && is_string($input[$requiredField]))) {
                return false;
            }
        }

        foreach(['sid', 'email'] as $requiredField) {
            if(! (isset($input[$requiredField]) && is_string($input[$requiredField]) && strlen($input[$requiredField]) > 0)) {
                return false;
            }
        }

        return true;
    }

    public function getSID(): string
    {
        return $this->sid;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function hasFirstName(): bool
    {
        return $this->firstName !== null;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function hasLastName(): bool
    {
        return $this->lastName !== null;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function hasMiddleName(): bool
    {
        return $this->middleName !== null;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function hasPhone(): bool
    {
        return $this->phone !== null;
    }

    public function isReferral(): bool
    {
        return $this->referral;
    }

    public function getWith(): string
    {
        return $this->with;
    }

    public function hasWith(): bool
    {
        return $this->with !== null;
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function hasGroupId(): bool
    {
        return $this->groupId !== null;
    }
}