<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions\AutoCreateEventMailingSubscriptionScript;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions\AutoDestroyEventMailingSubscriptionScript;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions\AutoMailingPerformSubscription;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions\UpdateMandrillResultsSubscriptionScript;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EventMailingDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoCreateEventMailingSubscriptionScript::class,
            AutoDestroyEventMailingSubscriptionScript::class,
            AutoMailingPerformSubscription::class,
            UpdateMandrillResultsSubscriptionScript::class,
        ];
    }
}