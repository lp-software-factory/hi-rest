<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Options\EventMailingOptions;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;

final class EventMailingOptionsService
{
    public const MAILING_ID_MATCH_REGEX = '/^event\:mailing\:(\d+)$/';

    /** @var EventRecipientsService */
    private $eventRecipientsService;

    public function __construct(EventRecipientsService $eventRecipientsService)
    {
        $this->eventRecipientsService = $eventRecipientsService;
    }


    public function getEventMailingIdFor(EventMailing $mailing): string
    {
        return sprintf('event:mailing:%d', $mailing->getId());
    }

    public function createEventMailingOptionsFor(EventMailing $mailing): EventMailingOptions
    {
        $eventRecipients = $this->eventRecipientsService->getByEventId($mailing->getEvent()->getId());

        return new EventMailingOptions(
            'Demo Subject',
            'Demo Html',
            'Demo text',
            $eventRecipients->getContacts()->all(),
            [
                'event_id' => $mailing->getEvent()->getId(),
                'event_mailing_id' => $mailing->getId(),
            ]
        );
    }
}