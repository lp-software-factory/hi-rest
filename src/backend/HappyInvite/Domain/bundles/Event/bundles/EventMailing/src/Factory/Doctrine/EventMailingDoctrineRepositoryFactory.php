<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;

final class EventMailingDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventMailing::class;
    }
}