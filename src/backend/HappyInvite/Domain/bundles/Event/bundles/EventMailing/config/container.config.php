<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing;

use function DI\get;
use function DI\factory;
use function DI\object;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Factory\Doctrine\EventMailingDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Repository\EventMailingRepository;

return [
    EventMailingRepository::class => factory(EventMailingDoctrineRepositoryFactory::class),
];