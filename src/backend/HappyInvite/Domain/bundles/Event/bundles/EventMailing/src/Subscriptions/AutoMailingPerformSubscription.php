<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingOptionsService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts\EventRecipientContact;
use HappyInvite\Domain\Bundles\Mandrill\Service\MandrillService;
use HappyInvite\Domain\Bundles\Mandrill\OptionsFactory\MandrillOptionsFactory;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoMailingPerformSubscription implements SubscriptionScript
{
    /** @var EventMailingService */
    private $mailingService;

    /** @var \HappyInvite\Domain\Bundles\Mandrill\Service\MandrillService */
    private $mandrillService;

    /** @var MandrillOptionsFactory */
    private $mandrillOptionsFactory;

    /** @var EventMailingOptionsService */
    private $mailingOptionsService;

    public function __construct(
        EventMailingService $mailingService,
        MandrillService $mandrillService,
        MandrillOptionsFactory $mailingOptionsFactory,
        EventMailingOptionsService $mailingOptionsService
    ) {
        $this->mailingService = $mailingService;
        $this->mandrillService = $mandrillService;
        $this->mandrillOptionsFactory = $mailingOptionsFactory;
        $this->mailingOptionsService = $mailingOptionsService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->mailingService->getEventEmitter()->on(EventMailingService::EVENT_REQUEST_PERFORM, function(EventMailing $mailing) {
            $mailingOptions = $this->mailingOptionsService->createEventMailingOptionsFor($mailing);
            $mandrillOptions = $this->mandrillOptionsFactory->createOptions(
                $mailingOptions->getSubject(),
                $mailingOptions->getHtml(),
                $mailingOptions->getText(),
                array_map(function(EventRecipientContact $contact) {
                    $name = null;

                    if($contact->hasFirstName() && $contact->hasLastName()) {
                        $name = sprintf('%s %s', $contact->getFirstName(), $contact->getLastName());
                    }else if($contact->hasFirstName()) {
                        $name = $contact->getFirstName();
                    }else if($contact->hasLastName()) {
                        $name = $contact->getLastName();
                    }

                    return [
                        'email' => $contact->getEmail(),
                        'type' => 'to',
                        'name' => $name,
                    ];
                }, $mailingOptions->getRecipients()),
                $mailingOptions->getMetadata()
            );

            $this->mandrillService->perform($this->mailingOptionsService->getEventMailingIdFor($mailing), $mandrillOptions);
        });
    }
}