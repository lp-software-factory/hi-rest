<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Exceptions;

final class EventMailingNotFoundException extends \Exception {}