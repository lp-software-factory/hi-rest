<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingOptionsService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService;
use HappyInvite\Domain\Bundles\Mandrill\Service\MandrillService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class UpdateMandrillResultsSubscriptionScript implements SubscriptionScript
{
    /** @var EventMailingService */
    private $eventMailingService;

    /** @var MandrillService */
    private $mandrillService;

    public function __construct(
        EventMailingService $eventMailingService,
        MandrillService $mandrillService
    ) {
        $this->eventMailingService = $eventMailingService;
        $this->mandrillService = $mandrillService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->mandrillService->getEventEmitter()->on(MandrillService::EVENT_STATUS_UPDATE, function(array $sendResults, string $mailingId, array $metadata) {
            if(preg_match_all(EventMailingOptionsService::MAILING_ID_MATCH_REGEX, $mailingId, $matches)) {
                $this->eventMailingService->updateMandrillResults($metadata['event_mailing_id'], $sendResults);
            }
        });
    }
}