<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Repository\EventMailingRepository;

final class EventMailingService
{
    public const EVENT_CREATED = 'hi.domain.event.mailing.created';
    public const EVENT_DELETE = 'hi.domain.event.mailing.delete';
    public const EVENT_DELETED = 'hi.domain.event.mailing.deleted';
    public const EVENT_REQUEST_PERFORM = 'hi.domain.event.mailing.request-perform';
    public const EVENT_PERFORMED = 'hi.domain.event.mailing.performed';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EventMailingRepository */
    private $mailingRepository;

    public function __construct(EventMailingRepository $mailingRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->mailingRepository = $mailingRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createForEvent(Event $event): EventMailing
    {
        $this->mailingRepository->create(
            $mailing = new EventMailing($event)
        );

        $this->eventEmitter->emit(self::EVENT_CREATED, [$mailing]);

        return $mailing;
    }

    public function deleteEventMailing(int $eventMailingId): void
    {
        $eventMailing = $this->getMailingByEventId($eventMailingId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$eventMailing, $eventMailingId]);
        $this->mailingRepository->delete($eventMailing);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$eventMailing, $eventMailingId]);
    }

    public function requestPerform(int $mailingId): void
    {
        $mailing = $this->getMailingById($mailingId);

        $this->mailingRepository->save(
            $mailing->markAsPerformRequested()
        );

        $this->eventEmitter->emit(self::EVENT_REQUEST_PERFORM, [$mailing]);
    }

    public function updateMandrillResults(int $mailingId, array $sendResults): EventMailing
    {
        $mailing = $this->getMailingById($mailingId);
        $mailing->importMandrillResults($sendResults);

        $this->mailingRepository->save($mailing);

        return $mailing;
    }

    public function markAsPerformed(int $mailingId): void
    {
        $mailing = $this->getMailingById($mailingId);

        $this->mailingRepository->save(
            $mailing->markAsPerformed()
        );

        $this->eventEmitter->emit(self::EVENT_PERFORMED, [$mailing]);
    }

    public function getMailingById(int $mailingId): EventMailing
    {
        return $this->mailingRepository->getById($mailingId);
    }

    public function getMailingByEventId(int $eventId): EventMailing
    {
        return $this->mailingRepository->getByEventId($eventId);
    }
}