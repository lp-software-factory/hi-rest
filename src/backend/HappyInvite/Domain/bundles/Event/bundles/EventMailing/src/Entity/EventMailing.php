<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Mandrill\Doctrine\Type\MandrillSendResults\MandrillSendResultsEntityTrait;
use HappyInvite\Domain\Bundles\Mandrill\Doctrine\Type\MandrillSendResults\MandrillSendResultsManager;
use HappyInvite\Domain\Bundles\Mandrill\Mandrill\SendResult;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Repository\EventMailingRepository")
 * @Table(name="event_mailing")
 */
class EventMailing implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, VersionEntityTrait, MandrillSendResultsEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event")
     * @JoinColumn(name="event_id", referencedColumnName="id")
     * @var Event
     */
    private $event;

    /**
     * @Column(name="is_perform_requested", type="boolean")
     * @var bool
     */
    private $isPerformRequested = false;

    /**
     * @Column(name="is_performed", type="boolean")
     * @var bool
     */
    private $isPerformed = false;

    /**
     * @Column(name="date_perform_requested", type="datetime")
     * @var \DateTime
     */
    private $datePerformRequested;

    /**
     * @Column(name="date_performed", type="datetime")
     * @var \DateTime
     */
    private $datePerformed;

    public function __construct(Event $event)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->event = $event;
        $this->mandrillResults = new MandrillSendResultsManager();

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return '1.0.0';
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'is_perform_requested' => $this->isPerformRequested(),
            'is_performed' => $this->isPerformed(),
            'event_id' => $this->getEvent()->getId(),
            'mandrill_results' => array_map(function(SendResult $result) {
                return $result->toJSON();
            }, $this->getMandrillResults()),
        ];

        if($this->isPerformRequested()) {
            $json['date_perform_requested'] = $this->getDatePerformRequested()->format(\DateTime::RFC2822);
        }

        if($this->isPerformed()) {
            $json['date_performed'] = $this->getDatePerformed()->format(\DateTime::RFC2822);
        }

        return $json;
    }

    public function markAsPerformRequested(): self
    {
        $this->isPerformRequested = true;
        $this->datePerformRequested = new \DateTime();

        $this->markAsUpdated();

        return $this;
    }

    public function markAsPerformed(): self
    {
        $this->isPerformed = true;
        $this->datePerformed = new \DateTime();

        $this->markAsUpdated();

        return $this;
    }

    public function getEvent(): Event
    {
        return $this->event;
    }

    public function isPerformRequested(): bool
    {
        return $this->isPerformRequested;
    }

    public function getDatePerformRequested(): \DateTime
    {
        return $this->datePerformRequested;
    }

    public function isPerformed(): bool
    {
        return $this->isPerformed;
    }

    public function getDatePerformed(): \DateTime
    {
        return $this->datePerformed;
    }
}