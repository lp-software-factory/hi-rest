<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoDestroyEventMailingSubscriptionScript implements SubscriptionScript
{
    /** @var EventService */
    private $eventService;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService */
    private $mailingService;

    public function __construct(EventService $eventService, EventMailingService $mailingService)
    {
        $this->eventService = $eventService;
        $this->mailingService = $mailingService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->eventService->getEventEmitter()->on(EventService::EVENT_DELETE, function(Event $event) {
            $eventMailing = $this->mailingService->getMailingByEventId($event->getId());

            $this->mailingService->deleteEventMailing($event->getId());
        });
    }
}