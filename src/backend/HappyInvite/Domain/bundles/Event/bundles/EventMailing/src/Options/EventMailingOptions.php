<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Options;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Doctrine\Type\EventRecipientContacts\EventRecipientContact;

final class EventMailingOptions
{
    /** @var string */
    private $subject;

    /** @var string */
    private $html;

    /** @var string */
    private $text;

    /** @var EventRecipientContact[] */
    private $recipients;

    /** @var array */
    private $metadata;

    public function __construct($subject, $html, $text, array $recipients, array $metadata)
    {
        $this->subject = $subject;
        $this->html = $html;
        $this->text = $text;
        $this->recipients = $recipients;
        $this->metadata = $metadata;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getHtml(): string
    {
        return $this->html;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }
}