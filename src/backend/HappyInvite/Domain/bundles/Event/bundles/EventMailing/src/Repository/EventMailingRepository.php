<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Exceptions\EventMailingNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EventMailingRepository extends EntityRepository
{
    public function create(EventMailing $mailing): int
    {
        while ($this->hasWithSID($mailing->getSID())) {
            $mailing->regenerateSID();
        }

        $this->getEntityManager()->persist($mailing);
        $this->getEntityManager()->flush($mailing);

        return $mailing->getId();
    }

    public function save(EventMailing $mailing)
    {
        $this->getEntityManager()->flush($mailing);
    }

    public function saveEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->save($entity);
        }
    }

    public function delete(EventMailing $mailing)
    {
        $this->getEntityManager()->remove($mailing);
        $this->getEntityManager()->flush($mailing);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventMailing
    {
        $result = $this->find($id);

        if ($result instanceof EventMailing) {
            return $result;
        } else {
            throw new EventMailingNotFoundException(sprintf('Mailing   `ID(%d)` not found', $id));
        }
    }

    public function getByEventId(int $eventId): EventMailing
    {
        $result = $this->findOneBy([
            'event' => $eventId,
        ]);

        if($result instanceof EventMailing) {
            return $result;
        }else{
            throw new EventMailingNotFoundException(sprintf('Mailing with Event(ID: %d) not found', $eventId));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Mailing  s with IDs (%s) not found', array_diff($ids, array_map(function (EventMailing $mailing) {
                return $mailing->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventMailing
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EventMailing) {
            return $result;
        } else {
            throw new EventMailingNotFoundException(sprintf('Mailing   `SID(%s)` not found', $sid));
        }
    }
}