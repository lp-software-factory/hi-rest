<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoCreateEventMailingSubscriptionScript implements SubscriptionScript
{
    /** @var EventService */
    private $eventService;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService */
    private $mailingService;

    public function __construct(EventService $eventService, EventMailingService $mailingService)
    {
        $this->eventService = $eventService;
        $this->mailingService = $mailingService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->eventService->getEventEmitter()->on(EventService::EVENT_CREATED, function(Event $event) {
            $this->mailingService->createForEvent($event);
        });

        /**
         * TODO: это первая версия рассылки. Подразумевается, что будут введены очереди и
         * рассылка будет происходить в режиме очереди, а не в синхронном при непосредтсвенн
         * submit'е события (Event)
         */
        $this->eventService->getEventEmitter()->on(EventService::EVENT_SUBMIT, function(Event $event) {
            $this->mailingService->requestPerform(
                $this->mailingService->getMailingByEventId($event->getId())->getId()
            );
        });
    }
}