<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;

final class QuizAnswerPushEvent extends PushEvent
{
    public const LATEST_VERSION = '1.0.0';

    public const PUSH_EVENT_TYPE = 'quiz.answer';

    /** @var string */
    protected $quizSID;

    /** @var string */
    protected $quizAnswerSID;

    public function __construct(string $quizSID, string $quizAnswerSID)
    {
        $this->version = self::LATEST_VERSION;
        $this->quizSID = $quizSID;
        $this->quizAnswerSID = $quizAnswerSID;
        $this->dateCreatedAt = new \DateTime();
        $this->lastUpdatedOn = clone $this->dateCreatedAt;
    }

    public static function fromJSON(array $input): self
    {
        $result = new self($input['quiz_sid'], $input['quiz_answer_sid']);
        $result->dateCreatedAt = \DateTime::createFromFormat(\DateTime::RFC2822, $input['date_created_at']);
        $result->lastUpdatedOn = \DateTime::createFromFormat(\DateTime::RFC2822, $input['last_updated_on']);
        $result->version = $input['version'] ?? self::LATEST_VERSION;

        return $result;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'type' => self::PUSH_EVENT_TYPE,
            'version' => $this->getVersion(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'quiz_sid' => $this->getQuizSID(),
            'quiz_answer_sid' => $this->getQuizAnswerSID(),
        ];
    }

    public function getType(): string
    {
        return self::PUSH_EVENT_TYPE;
    }

    public function getQuizSID(): string
    {
        return $this->quizSID;
    }

    public function getQuizAnswerSID(): string
    {
        return $this->quizAnswerSID;
    }
}