<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Doctrine\Factory\EventAnalyticsRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Repository\EventAnalyticsRepository;

return [
    EventAnalyticsRepository::class => factory(EventAnalyticsRepositoryFactory::class),
];