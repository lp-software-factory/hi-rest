<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Factory\EventAnalyticsRecordEventFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Service\EventAnalyticsService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoCreateEventAnalyticsSubscriptionScript implements SubscriptionScript
{
    /** @var EventMailingService */
    private $eventMailingService;

    /** @var EventAnalyticsService */
    private $eventAnalyticsService;

    /** @var EventAnalyticsRecordEventFactory */
    private $eventAnalyticsRecordEventFactory;

    /** @var EventRecipientsService */
    private $eventRecipientsService;

    public function __construct(
        EventMailingService $eventMailingService,
        EventAnalyticsService $eventAnalyticsService,
        EventAnalyticsRecordEventFactory $eventAnalyticsRecordEventFactory,
        EventRecipientsService $eventRecipientsService
    ) {
        $this->eventMailingService = $eventMailingService;
        $this->eventAnalyticsService = $eventAnalyticsService;
        $this->eventAnalyticsRecordEventFactory = $eventAnalyticsRecordEventFactory;
        $this->eventRecipientsService = $eventRecipientsService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->eventMailingService->getEventEmitter()->on(eventMailingService::EVENT_CREATED, function(EventMailing $eventMailing) {
            $recipients = $this->eventRecipientsService->getByEventId($eventMailing->getEvent()->getId());
            $mailing = $this->eventAnalyticsService->createAnalyticsForEventMailing($eventMailing);

            $this->eventAnalyticsService->pushAnalytics($mailing->getId(), '*', $this->eventAnalyticsRecordEventFactory->createEventMailInit(
                $recipients->getContacts()->all()
            ));
        });
    }
}