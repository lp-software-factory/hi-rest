<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Doctrine\Factory;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;

final class EventAnalyticsRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventAnalytics::class;
    }
}