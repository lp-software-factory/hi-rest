<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\EventAnalyticsStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\MailDeliveredPushEvent;

final class MailDeliveredStats implements EventAnalyticsStats
{
    public function getKey(): string
    {
        return 'delivered';
    }

    public function getStats(Event $event, array $recipients, EventAnalytics $eventAnalytics, array $pushEvents): array
    {
        $results = [
            'total' => count($recipients),
            'delivered' => Chain::create($pushEvents)
                ->filter(function (PushEvent $pushEvent){
                    return $pushEvent instanceof MailDeliveredPushEvent;
                })
                ->count(),
        ];

        if($results['total'] < $results['delivered']) {
            $results['delivered'] = $results['total'];
        }

        return $results;
    }
}