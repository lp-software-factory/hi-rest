<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;

final class MailDeliveredPushEvent extends PushEvent
{
    public const LATEST_VERSION = '1.0.0';

    public const PUSH_EVENT_TYPE = 'mail.delivered';

    public function __construct()
    {
        $this->version = self::LATEST_VERSION;
        $this->dateCreatedAt = new \DateTime();
        $this->lastUpdatedOn = clone $this->dateCreatedAt;
    }

    public static function fromJSON(array $input): self
    {
        $result = new self();
        $result->dateCreatedAt = \DateTime::createFromFormat(\DateTime::RFC2822, $input['date_created_at']);
        $result->lastUpdatedOn = \DateTime::createFromFormat(\DateTime::RFC2822, $input['last_updated_on']);
        $result->version = $input['version'] ?? self::LATEST_VERSION;

        return $result;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'type' => self::PUSH_EVENT_TYPE,
            'version' => $this->getVersion(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
        ];
    }

    public function getType(): string
    {
        return self::PUSH_EVENT_TYPE;
    }
}