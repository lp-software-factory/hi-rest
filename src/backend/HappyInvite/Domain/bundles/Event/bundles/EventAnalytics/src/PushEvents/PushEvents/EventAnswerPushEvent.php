<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;

final class EventAnswerPushEvent extends PushEvent
{
    public const DECISION_YES = 'yes';
    public const DECISION_NO = 'no';
    public const DECISION_NONE = 'none';

    public const LATEST_VERSION = '1.0.0';

    public const PUSH_EVENT_TYPE = 'event.answer';

    /** @var string */
    protected $decision;

    public function __construct(string $decision)
    {
        $this->version = self::LATEST_VERSION;
        $this->decision = $decision;
        $this->dateCreatedAt = new \DateTime();
        $this->lastUpdatedOn = clone $this->dateCreatedAt;
    }

    public static function fromJSON(array $input): self
    {
        $result = new self($input['decision']);
        $result->dateCreatedAt = \DateTime::createFromFormat(\DateTime::RFC2822, $input['date_created_at']);
        $result->lastUpdatedOn = \DateTime::createFromFormat(\DateTime::RFC2822, $input['last_updated_on']);
        $result->version = $input['version'] ?? self::LATEST_VERSION;

        return $result;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'type' => self::PUSH_EVENT_TYPE,
            'version' => $this->getVersion(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'decision' => $this->decision,
        ];
    }

    public function getType(): string
    {
        return self::PUSH_EVENT_TYPE;
    }

    public function getRawDecision(): string
    {
        return $this->decision;
    }

    public function noAnswerYet(): bool
    {
        return $this->decision === self::DECISION_NONE;
    }

    public function isAnsweredYes(): bool
    {
        return $this->decision === self::DECISION_YES;
    }

    public function isAnsweredNo(): bool
    {
        return ! $this->decision === self::DECISION_NO;
    }
}