<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Subscriptions\AutoCreateEventAnalyticsSubscriptionScript;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Subscriptions\AutoDestroyEventAnalyticsSubscriptionScript;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EventAnalyticsDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoCreateEventAnalyticsSubscriptionScript::class,
            AutoDestroyEventAnalyticsSubscriptionScript::class,
        ];
    }
}