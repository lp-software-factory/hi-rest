<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Repository\EventAnalyticsRepository")
 * @Table(name="event_analytics")
 */
class EventAnalytics implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, VersionEntity, JSONMetadataEntity
{
    public const LATEST_VERSION = '1.0.0';

    public const TO_JSON_OPTION_SKIP_RESULTS = 'skip-results';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing")
     * @JoinColumn(name="event_mailing_id", referencedColumnName="id")
     * @var EventMailing
     */
    private $eventMailing;

    /**
     * @Column(name="records", type="json_array")
     * @var array
     */
    private $records = [];

    public function __construct(EventMailing $eventMailing)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->eventMailing = $eventMailing;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'event_mailing_id' => $this->getEventMailing()->getId(),
            'event_id' => $this->getEventMailing()->getEvent()->getId(),
        ];

        if(! (isset($options[self::TO_JSON_OPTION_SKIP_RESULTS]) && $options[self::TO_JSON_OPTION_SKIP_RESULTS])) {
            $json['records'] = array_map(function(EventAnalyticsRecord $record) {
                return $record->toJSON();
            }, $this->allRecords());
        }

        return $json;
    }

    public function getEventMailing(): EventMailing
    {
        return $this->eventMailing;
    }

    public function addRecord(EventAnalyticsRecord $record): self
    {
        $this->records[] = $record->toJSON();
        $this->markAsUpdated();

        return $this;
    }

    public function allRecords(): array
    {
        return array_map(function(array $input) {
            return EventAnalyticsRecord::fromJSON($input);
        }, $this->records);
    }
}