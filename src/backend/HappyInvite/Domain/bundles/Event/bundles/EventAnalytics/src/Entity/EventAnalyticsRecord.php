<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Util\GenerateRandomString;

final class EventAnalyticsRecord implements JSONSerializable, SIDEntity
{
    public const EVENT_MAIL_DELIVERED = 'mail.delivered';
    public const EVENT_MAIL_OPENED = 'mail.opened';
    public const EVENT_EVENT_ANSWER= 'event.answer';
    public const EVENT_QUIZ_ANSWER = 'event.quiz.answer';

    /** @var string */
    private $sid;

    /** @var string */
    private $recipient;

    /** @var array */
    private $event;

    public function __construct(string $recipient, array $event, string $sid = null)
    {
        $this->recipient = $recipient;
        $this->event = $event;

        if($sid) {
            $this->sid = $sid;
        }else{
            $this->sid = $this->regenerateSID();
        }
    }

    public function getSID(): string
    {
        return $this->sid;
    }

    public function regenerateSID(): string
    {
        $this->sid = GenerateRandomString::generate(SIDEntity::SID_LENGTH);

        return $this->sid;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'sid' => $this->getSID(),
            'recipient' => $this->getRecipient(),
            'event' => $this->getEvent(),
        ];
    }

    public static function fromJSON(array $input): self
    {
        return new self(
            $input['recipient'],
            $input['event'],
            $input['sid'] ?? null
        );
    }

    public function getRecipient(): string
    {
        return $this->recipient;
    }

    public function getEvent(): array
    {
        return $this->event;
    }
}