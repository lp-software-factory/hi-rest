<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Exceptions\EventAnalyticsNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EventAnalyticsRepository extends EntityRepository
{
    public function create(EventAnalytics $analytics): int
    {
        while ($this->hasWithSID($analytics->getSID())) {
            $analytics->regenerateSID();
        }

        $this->getEntityManager()->persist($analytics);
        $this->getEntityManager()->flush($analytics);

        return $analytics->getId();
    }

    public function save(EventAnalytics $analytics)
    {
        $this->getEntityManager()->flush($analytics);
    }

    public function saveEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->save($entity);
        }
    }

    public function delete(EventAnalytics $analytics)
    {
        $this->getEntityManager()->remove($analytics);
        $this->getEntityManager()->flush($analytics);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventAnalytics
    {
        $result = $this->find($id);

        if ($result instanceof EventAnalytics) {
            return $result;
        } else {
            throw new EventAnalyticsNotFoundException(sprintf('EventAnalytics `ID(%d)` not found', $id));
        }
    }

    public function getByEventMailingId(int $eventMailingId): EventAnalytics
    {
        $result = $this->findOneBy([
            'eventMailing' => $eventMailingId,
        ]);

        if($result instanceof EventAnalytics) {
            return $result;
        }else{
            throw new EventAnalyticsNotFoundException(sprintf('EventAnalytics with Event(EventMailingID: %d) not found', $eventMailingId));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('EventAnalytics with IDs (%s) not found', array_diff($ids, array_map(function (EventAnalytics $analytics) {
                return $analytics->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventAnalytics
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EventAnalytics) {
            return $result;
        } else {
            throw new EventAnalyticsNotFoundException(sprintf('EventAnalytics `SID(%s)` not found', $sid));
        }
    }
}