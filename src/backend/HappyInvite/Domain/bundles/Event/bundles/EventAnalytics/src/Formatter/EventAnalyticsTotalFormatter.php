<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Factory\EventAnalyticsRecordEventFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\EventAnalyticsStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats\AnsweredStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats\MailDeliveredStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats\MailOpenedStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats\QuizStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\MailInitPushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use Interop\Container\ContainerInterface;

final class EventAnalyticsTotalFormatter
{
    /** @var ContainerInterface */
    private $container;

    /** @var EventRecipientsService */
    private $eventRecipientsService;

    /** @var EventAnalyticsRecordEventFactory */
    private $pushEventsFactory;

    public function __construct(ContainerInterface $container, EventRecipientsService $eventRecipientsService, EventAnalyticsRecordEventFactory $pushEventsFactory)
    {
        $this->container = $container;
        $this->eventRecipientsService = $eventRecipientsService;
        $this->pushEventsFactory = $pushEventsFactory;
    }

    public function formatOne(EventAnalytics $eventAnalytics)
    {
        $json = $eventAnalytics->toJSON([
            EventAnalytics::TO_JSON_OPTION_SKIP_RESULTS => true,
        ]);

        $json['results'] = $this->formatRecords($eventAnalytics);

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $eventAnalytics): array
    {
        return array_map(function(EventAnalytics $input) {
            return $this->formatOne($input);
        }, $eventAnalytics);
    }

    private function formatRecords(EventAnalytics $eventAnalytics): array
    {
        if($eventAnalytics->getEventMailing()->getEvent()->isSubmit()) {
            $result = [];

            $pushEvents = $this->pushEventsFactory->arrayToPushEvents($eventAnalytics->allRecords());
            $recipients = $this->getRecipients($pushEvents, $eventAnalytics);

            return Chain::create([
                AnsweredStats::class,
                MailDeliveredStats::class,
                MailOpenedStats::class,
                QuizStats::class,
            ])
                ->map(function(string $className) {
                    return $this->container->get($className);
                })
                ->map(function(EventAnalyticsStats $stats) use (&$result, $eventAnalytics, $recipients, $pushEvents) {
                    $result[$stats->getKey()] = $stats->getStats($eventAnalytics->getEventMailing()->getEvent(), $recipients, $eventAnalytics, $pushEvents);
                })
                ->array;
        }else{
            return [];
        }
    }

    private function getRecipients(array $pushEvents, EventAnalytics $eventAnalytics): array
    {
        foreach($pushEvents as $pushEvent) {
            if($pushEvent instanceof MailInitPushEvent) {
                return $pushEvent->getContacts();
            }
        }

        throw new \Exception('No initial push event found');
    }
}