<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalyticsRecord;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Repository\EventAnalyticsRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;

final class EventAnalyticsService
{
    public const EVENT_CREATED = 'hi.domain.event.analytics.created';
    public const EVENT_UPDATED = 'hi.domain.event.analytics.updated';
    public const EVENT_DELETE = 'hi.domain.event.analytics.delete';
    public const EVENT_DELETED = 'hi.domain.event.analytics.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EventAnalyticsRepository */
    private $repository;

    public function __construct(EventAnalyticsRepository $repository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->repository = $repository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createAnalyticsForEventMailing(EventMailing $eventMailing): EventAnalytics
    {
        $eventAnalytics = new EventAnalytics($eventMailing);

        $this->repository->create($eventAnalytics);
        $this->eventEmitter->emit(self::EVENT_CREATED, [$eventAnalytics]);

        return $eventAnalytics;
    }

    public function destroyAnalytics(int $eventAnalyticsId): void
    {
        $eventAnalytics = $this->getAnalyticsById($eventAnalyticsId);
        $eventAnalyticsId = $eventAnalytics->getId();

        $this->eventEmitter->emit(self::EVENT_DELETE, [$eventAnalytics, $eventAnalyticsId]);
        $this->repository->delete($eventAnalytics);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$eventAnalytics, $eventAnalyticsId]);
    }

    public function getAnalyticsById(int $id): EventAnalytics
    {
        return $this->repository->getById($id);
    }

    public function getAnalyticsForEventMailing(int $eventId): EventAnalytics
    {
        return $this->repository->getByEventMailingId($eventId);
    }

    public function pushAnalytics(int $eventMailingId, string $recipient, PushEvent $event): EventAnalytics
    {
        $eventAnalytics = $this->getAnalyticsForEventMailing($eventMailingId);
        $eventAnalytics->addRecord(new EventAnalyticsRecord($recipient, $event->toJSON()));

        $this->repository->save($eventAnalytics);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$eventAnalytics]);

        return $eventAnalytics;
    }

    public function pushManyAnalytics(int $eventMailingId, array $map): EventAnalytics
    {
        $eventAnalytics = $this->getAnalyticsForEventMailing($eventMailingId);

        /** @var PushEvent $event */
        foreach($map as $recipient => $event) {
            $eventAnalytics->addRecord(new EventAnalyticsRecord($recipient, $event->toJSON()));
        }

        $this->repository->save($eventAnalytics);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$eventAnalytics]);

        return $eventAnalytics;
    }
}