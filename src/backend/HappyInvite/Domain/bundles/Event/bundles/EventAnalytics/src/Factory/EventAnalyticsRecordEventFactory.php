<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Factory;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Exceptions\UnknownPushEventTypeException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\EventAnswerPushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\MailDeliveredPushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\MailInitPushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\MailOpenedPushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\QuizAnswerPushEvent;

final class EventAnalyticsRecordEventFactory
{
    public function arrayToPushEvents(array $input): array
    {
        return array_map(function(array $arrPushEvent): PushEvent {
            switch($arrPushEvent['type']) {
                default:
                    throw new UnknownPushEventTypeException(sprintf('PushEvent with type `%s` is unknown', $arrPushEvent));

                case MailInitPushEvent::PUSH_EVENT_TYPE:
                    return MailInitPushEvent::fromJSON($arrPushEvent);

                case MailDeliveredPushEvent::PUSH_EVENT_TYPE:
                    return MailDeliveredPushEvent::fromJSON($arrPushEvent);

                case MailOpenedPushEvent::PUSH_EVENT_TYPE:
                    return MailOpenedPushEvent::fromJSON($arrPushEvent);

                case EventAnswerPushEvent::PUSH_EVENT_TYPE:
                    return EventAnswerPushEvent::fromJSON($arrPushEvent);

                case QuizAnswerPushEvent::PUSH_EVENT_TYPE:
                    return QuizAnswerPushEvent::fromJSON($arrPushEvent);
            }
        }, $input);
    }

    public function createEventMailInit(array $contacts): MailInitPushEvent
    {
        return new MailInitPushEvent($contacts);
    }

    public function createEventMailDelivered(): MailDeliveredPushEvent
    {
        return new MailDeliveredPushEvent();
    }

    public function createEventMailOpened(): MailOpenedPushEvent
    {
        return new MailOpenedPushEvent();
    }

    public function createEventAnswerNone(): EventAnswerPushEvent
    {
        return new EventAnswerPushEvent(EventAnswerPushEvent::DECISION_NONE);
    }

    public function createEventAnswerYes(): EventAnswerPushEvent
    {
        return new EventAnswerPushEvent(EventAnswerPushEvent::DECISION_YES);
    }

    public function createEventAnswerNo(): EventAnswerPushEvent
    {
        return new EventAnswerPushEvent(EventAnswerPushEvent::DECISION_NO);
    }

    public function createEventQuizAnswer(string $quizSID, string $quizAnswerSID): QuizAnswerPushEvent
    {
        return new QuizAnswerPushEvent($quizSID, $quizAnswerSID);
    }
}