<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\EventAnalyticsStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\EventAnswerPushEvent;

final class AnsweredStats implements EventAnalyticsStats
{
    public function getKey(): string
    {
        return 'answered';
    }

    public function getStats(Event $event, array $recipients, EventAnalytics $eventAnalytics, array $pushEvents): array
    {
        $results = [
            EventAnswerPushEvent::DECISION_NO => 0,
            EventAnswerPushEvent::DECISION_YES => 0,
            EventAnswerPushEvent::DECISION_NONE => 0,
        ];

        Chain::create($pushEvents)
            ->filter(function (PushEvent $pushEvent){
                return $pushEvent instanceof EventAnswerPushEvent;
            })
            ->map(function(EventAnswerPushEvent $event) use (&$results)  {
                $results[$event->getRawDecision()]++;

                return $event;
            })
        ;

        return $results;
    }
}