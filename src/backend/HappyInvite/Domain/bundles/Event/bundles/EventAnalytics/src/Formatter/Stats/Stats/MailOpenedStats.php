<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\EventAnalyticsStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\MailOpenedPushEvent;

final class MailOpenedStats implements EventAnalyticsStats
{
    public function getKey(): string
    {
        return 'opened';
    }

    public function getStats(Event $event, array $recipients, EventAnalytics $eventAnalytics, array $pushEvents): array
    {
        $results = [
            'total' => count($recipients),
            'opened' => Chain::create($pushEvents)
                ->filter(function (PushEvent $pushEvent){
                    return $pushEvent instanceof MailOpenedPushEvent;
                })
                ->count(),
        ];

        if($results['total'] < $results['opened']) {
            $results['opened'] = $results['total'];
        }

        return $results;
    }
}