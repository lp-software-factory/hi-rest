<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Exceptions;

final class EventAnalyticsNotFoundException extends \Exception {}