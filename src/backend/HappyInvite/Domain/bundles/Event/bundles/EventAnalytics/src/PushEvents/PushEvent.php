<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents;

use HappyInvite\Domain\Markers\JSONSerializable;

abstract class PushEvent implements JSONSerializable
{
    /** @var string */
    protected $sid;

    /** @var string */
    protected $version;

    /** @var \DateTime */
    protected $dateCreatedAt;

    /** @var \DateTime */
    protected $lastUpdatedOn;

    abstract public function getType(): string;

    public function getSID(): string
    {
        return $this->sid;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getDateCreatedAt(): \DateTime
    {
        return $this->dateCreatedAt;
    }

    public function getLastUpdatedOn(): \DateTime
    {
        return $this->lastUpdatedOn;
    }
}