<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;

final class EventAnalyticsFormatter
{
    public function formatOne(EventAnalytics $eventAnalytics)
    {
        $json = $eventAnalytics->toJSON();

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $eventAnalytics): array
    {
        return array_map(function(EventAnalytics $input) {
            return $this->formatOne($input);
        }, $eventAnalytics);
    }
}