<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;


interface EventAnalyticsStats
{
    public function getKey(): string;
    public function getStats(Event $event, array $recipients, EventAnalytics $eventAnalytics, array $pushEvents): array;
}