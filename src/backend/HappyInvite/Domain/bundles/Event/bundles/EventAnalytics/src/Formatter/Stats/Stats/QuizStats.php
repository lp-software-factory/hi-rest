<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\Stats;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Entity\EventAnalytics;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Formatter\Stats\EventAnalyticsStats;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\PushEvents\PushEvents\QuizAnswerPushEvent;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigQuiz;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigQuizAnswer;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\EventMailingOptionsConfigDataMapper;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service\EventMailingOptionsService;

final class QuizStats implements EventAnalyticsStats
{
    /** @var EventMailingOptionsService */
    private $eventMailingOptionsService;

    /** @var EventMailingOptionsConfigDataMapper */
    private $eventMailingOptionsDataMapper;

    public function __construct(
        EventMailingOptionsService $eventMailingOptionsService,
        EventMailingOptionsConfigDataMapper $eventMailingOptionsConfigDataMapper
    ) {
        $this->eventMailingOptionsService = $eventMailingOptionsService;
        $this->eventMailingOptionsDataMapper = $eventMailingOptionsConfigDataMapper;
    }

    public function getKey(): string
    {
        return 'quiz';
    }

    public function getStats(Event $event, array $recipients, EventAnalytics $eventAnalytics, array $pushEvents): array
    {
        $eventMailingOptionsConfig = $this->eventMailingOptionsDataMapper->createConfigFromArray(
            $this->eventMailingOptionsService->getByEventId($event->getId())->getConfig()
        );

        $map = [];
        $quizzes = array_map(function(EventMailingOptionsConfigQuiz $quiz) use ($recipients) {
            $result = [
                'quiz_sid' => $quiz->getSID(),
                'answers' => array_map(function(EventMailingOptionsConfigQuizAnswer $quizAnswer) {
                    return [
                        'quiz_answer_sid' => $quizAnswer->getSID(),
                        'count' => 0,
                    ];
                }, $quiz->getAnswers())
            ];

            return $result;
        }, $eventMailingOptionsConfig->getQuizzes());

        foreach($quizzes as $quiz) {
            foreach($quiz['answers'] as $quizAnswer) {
                $map[$quizAnswer['quiz_answer_sid']] = &$quizAnswer;
            }
        }

        Chain::create($pushEvents)
            ->filter(function(PushEvent $pushEvent) {
                return $pushEvent instanceof QuizAnswerPushEvent;
            })
            ->map(function(QuizAnswerPushEvent $pushEvent) use ($map) {
                if(isset($map[$pushEvent->getQuizAnswerSID()])) {
                    $map[$pushEvent->getQuizAnswerSID()]['count']++;
                }
            });

        return $quizzes;
    }
}