<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventAnalytics\Service\EventAnalyticsService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Entity\EventMailing;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailing\Service\EventMailingService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoDestroyEventAnalyticsSubscriptionScript implements SubscriptionScript
{
    /** @var EventMailingService */
    private $eventMailingService;

    /** @var EventAnalyticsService */
    private $eventAnalyticsService;

    public function __construct(
        EventMailingService $eventMailingService,
        EventAnalyticsService $eventAnalyticsService
    ) {
        $this->eventMailingService = $eventMailingService;
        $this->eventAnalyticsService = $eventAnalyticsService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->eventMailingService->getEventEmitter()->on(eventMailingService::EVENT_DELETE, function(EventMailing $eventMailing) {
            $eventAnalytics = $this->eventAnalyticsService->getAnalyticsForEventMailing($eventMailing->getId());

            $this->eventAnalyticsService->destroyAnalytics($eventAnalytics->getId());
        });
    }
}