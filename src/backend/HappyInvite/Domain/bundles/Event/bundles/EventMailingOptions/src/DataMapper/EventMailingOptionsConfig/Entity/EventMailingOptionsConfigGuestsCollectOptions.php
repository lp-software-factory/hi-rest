<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfigGuestsCollectOptions implements JSONSerializable
{
    /** @var bool */
    private $firstName;

    /** @var bool */
    private $lastName;

    /** @var bool */
    private $city;

    /** @var bool */
    private $email;

    /** @var bool */
    private $firstNameRequired;

    /** @var bool */
    private $lastNameRequired;

    /** @var bool */
    private $cityRequired;

    /** @var bool */
    private $emailRequired;

    public function __construct(
        bool $isFirstNameCollected,
        bool $isLastNameCollected,
        bool $isCityCollected,
        bool $isEmailCollected,
        bool $isFirstNameRequired,
        bool $isLastNameRequired,
        bool $isCityRequired,
        bool $isEmailRequired
    ) {
        $this->firstName = $isFirstNameCollected;
        $this->lastName = $isLastNameCollected;
        $this->city = $isCityCollected;
        $this->email = $isEmailCollected;
        $this->firstNameRequired = $isFirstNameRequired;
        $this->lastNameRequired = $isLastNameRequired;
        $this->cityRequired = $isCityRequired;
        $this->emailRequired = $isEmailRequired;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'firstName' => $this->isFirstNameCollected(),
            'lastName' => $this->isLastNameCollected(),
            'city' => $this->isCityCollected(),
            'email' => $this->isEmailCollected(),
            'firstNameRequired' => $this->isFirstNameRequired(),
            'lastNameRequired' => $this->isLastNameRequired(),
            'cityRequired' => $this->isCityRequired(),
            'emailRequired' => $this->isEmailRequired(),
        ];
    }

    public function isFirstNameCollected(): bool
    {
        return $this->firstName;
    }

    public function isLastNameCollected(): bool
    {
        return $this->lastName;
    }

    public function isCityCollected(): bool
    {
        return $this->city;
    }

    public function isEmailCollected(): bool
    {
        return $this->email;
    }

    public function isFirstNameRequired(): bool
    {
        return $this->firstNameRequired;
    }

    public function isLastNameRequired(): bool
    {
        return $this->lastNameRequired;
    }

    public function isCityRequired(): bool
    {
        return $this->cityRequired;
    }

    public function isEmailRequired(): bool
    {
        return $this->emailRequired;
    }
}