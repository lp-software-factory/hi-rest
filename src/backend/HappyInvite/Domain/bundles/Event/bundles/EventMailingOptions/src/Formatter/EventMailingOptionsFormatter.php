<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Entity\EventMailingOptions;

final class EventMailingOptionsFormatter
{
    public function formatOne(EventMailingOptions $entity): array
    {
        return [
            'entity' => $entity->toJSON(),
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EventMailingOptions $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}