<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service\EventMailingOptionsService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoCreateEventMailingOptionsSubscriptionScript implements SubscriptionScript
{
    /** @var EventService */
    private $eventService;

    /** @var EventMailingOptionsService */
    private $eventMailingOptionsService;

    public function __construct(EventService $eventService, EventMailingOptionsService $eventMailingOptionsService)
    {
        $this->eventService = $eventService;
        $this->eventMailingOptionsService = $eventMailingOptionsService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->eventService->getEventEmitter()->on(EventService::EVENT_CREATED, function(Event $event) {
            $this->eventMailingOptionsService->createEventMailingOptionsForEvent($event);
        });
    }
}