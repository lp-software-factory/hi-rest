<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfigQuiz implements JSONSerializable
{
    public const TARGET_NO = 'no';
    public const TARGET_YES = 'yes';
    public const TARGET_ALL = ' all';

    /** @var string */
    private $sid;

    /** @var string */
    private $target;

    /** @var string */
    private $question;

    /** @var EventMailingOptionsConfigQuizAnswer[] */
    private $answers;

    public function __construct(string $sid, string $target, string $question, array $answers)
    {
        $this->sid = $sid;
        $this->target = $target;
        $this->question = $question;
        $this->answers = $answers;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'sid' => $this->getSID(),
            'target' => $this->getRawTarget(),
            'question' => $this->getQuestion(),
            'answers' => array_map(function(EventMailingOptionsConfigQuizAnswer $answer) {
                return $answer->toJSON();
            }, $this->getAnswers())
        ];
    }

    public function getSID(): string
    {
        return $this->sid;
    }

    public function isTargetedToYes(): bool
    {
        return $this->target === self::TARGET_YES;
    }

    public function isTargetedToNo(): bool
    {
        return $this->target === self::TARGET_NO;
    }

    public function isTargetedToAll(): bool
    {
        return $this->target === self::TARGET_ALL;
    }

    public function getRawTarget(): string
    {
        return $this->target;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function getAnswers(): array
    {
        return $this->answers;
    }
}