<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Parameters;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfig;

final class EditEventMailingOptionsParameters
{
    /** @var EventMailingOptionsConfig */
    private $config;

    public function __construct(EventMailingOptionsConfig $config)
    {
        $this->config = $config;
    }

    public function getConfig(): EventMailingOptionsConfig
    {
        return $this->config;
    }
}