<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Exceptions;

final class EventMailingOptionsNotFoundException extends \Exception {}