<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfigQuizAnswer implements JSONSerializable
{
    /** @var string */
    private $sid;

    /** @var string */
    private $title;

    public function __construct(string $sid, string $title)
    {
        $this->sid = $sid;
        $this->title = $title;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'sid' => $this->getSID(),
            'title' => $this->getTitle(),
        ];
    }

    public function getSID(): string
    {
        return $this->sid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}