<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfigBasic implements JSONSerializable
{
    /** @var \DateTime */
    private $date;

    /** @var bool */
    private $answerCollect;

    /** @var bool */
    private $answerAfterDateOver;

    /** @var int */
    private $guestsMax;

    /** @var bool */
    private $showEmptySpaces;

    /** @var string */
    private $description;

    public function __construct(?\DateTime $date, bool $answerCollect, bool $answerAfterDateOver, int $guestsMax, bool $showEmptySpaces, string $description)
    {
        $this->date = $date;
        $this->answerCollect = $answerCollect;
        $this->answerAfterDateOver = $answerAfterDateOver;
        $this->guestsMax = $guestsMax;
        $this->showEmptySpaces = $showEmptySpaces;
        $this->description = $description;
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            'answerCollect' => $this->isAnswerCollect(),
            'answerAfterDateOver' => $this->isAnswerAfterDateOver(),
            'guestsMax' => $this->getGuestsMax(),
            'showEmptySpaces' => $this->isShowEmptySpaces(),
            'description' => $this->getDescription(),
        ];

        if($this->hasDate()) {
            $json['date'] = $this->getDate()->format(\DateTime::RFC2822);
        }else{
            $json['date'] = null;
        }

        return $json;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function hasDate(): bool
    {
        return $this->date !== null;
    }

    public function isAnswerCollect(): bool
    {
        return $this->answerCollect;
    }

    public function isAnswerAfterDateOver(): bool
    {
        return $this->answerAfterDateOver;
    }

    public function getGuestsMax(): int
    {
        return $this->guestsMax;
    }

    public function isShowEmptySpaces(): bool
    {
        return $this->showEmptySpaces;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}