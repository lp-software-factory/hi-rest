<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Entity;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfig;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Repository\EventMailingOptionsRepository")
 * @Table(name="event_mailing_options")
 */
class EventMailingOptions implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event")
     * @JoinColumn(name="event_id", referencedColumnName="id")
     * @var Event
     */
    private $event;

    /**
     * @Column(name="config", type="json_array")
     * @var array
     */
    private $config;

    public function __construct(Event $event, EventMailingOptionsConfig $config)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->event = $event;
        $this->config = $config->toJSON();

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'event_id' => $this->getEvent()->getId(),
            'config' => $this->getConfig(),
        ];

        return $json;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getEvent(): Event
    {
        return $this->event;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(EventMailingOptionsConfig $config): self
    {
        $this->config = $config->toJSON();
        $this->markAsUpdated();

        return $this;
    }
}