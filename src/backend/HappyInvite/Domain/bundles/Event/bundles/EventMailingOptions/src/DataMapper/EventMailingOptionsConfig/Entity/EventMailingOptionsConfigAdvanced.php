<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfigAdvanced implements JSONSerializable
{
    /** @var bool */
    private $allowedEditOnAnswerPage;

    /** @var bool */
    private $allowedEditAfterSend;

    /** @var bool */
    private $allowedSendMessagesWithAnswer;

    /** @var bool */
    private $canSendMailAfterAnswer;

    /** @var string */
    private $yesDescription;

    /** @var string */
    private $noDescription;

    public function __construct(
        bool $isAllowedEditOnAnswerPage,
        bool $isAllowedEditAfterSend,
        bool $isAllowedSendMessagesWithAnswer,
        bool $canSendMailAfterAnswer,
        string $yesDescription,
        string $noDescription
    ) {
        $this->allowedEditOnAnswerPage = $isAllowedEditOnAnswerPage;
        $this->allowedEditAfterSend = $isAllowedEditAfterSend;
        $this->allowedSendMessagesWithAnswer = $isAllowedSendMessagesWithAnswer;
        $this->canSendMailAfterAnswer = $canSendMailAfterAnswer;
        $this->yesDescription = $yesDescription;
        $this->noDescription = $noDescription;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'allowEditOnAnswerPage' => $this->isAllowedEditOnAnswerPage(),
            'allowEditAfterSend' => $this->isAllowedEditAfterSend(),
            'allowSendMessagesWithAnswer' => $this->isAllowedSendMessagesWithAnswer(),
            'sendMailAfterAnswer' => $this->canSendMailAfterAnswer(),
            'yesDescription' => $this->getYesDescription(),
            'noDescription' => $this->getNoDescription(),
        ];
    }

    public function isAllowedEditOnAnswerPage(): bool
    {
        return $this->allowedEditOnAnswerPage;
    }

    public function isAllowedEditAfterSend(): bool
    {
        return $this->allowedEditAfterSend;
    }

    public function isAllowedSendMessagesWithAnswer(): bool
    {
        return $this->allowedSendMessagesWithAnswer;
    }

    public function canSendMailAfterAnswer(): bool
    {
        return $this->canSendMailAfterAnswer;
    }

    public function getYesDescription(): string
    {
        return $this->yesDescription;
    }

    public function getNoDescription(): string
    {
        return $this->noDescription;
    }
}