<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfigGuests implements JSONSerializable
{
    /** @var bool */
    private $canBringAdditionalGuests;

    /** @var int */
    private $maxAdditionalGuests;

    /** @var EventMailingOptionsConfigGuestsCollectOptions */
    private $collectOptions;

    public function __construct(
        bool $canBringAdditionalGuests,
        int $maxAdditionalGuests,
        EventMailingOptionsConfigGuestsCollectOptions $collectOptions
    ) {
        $this->canBringAdditionalGuests = $canBringAdditionalGuests;
        $this->maxAdditionalGuests = $maxAdditionalGuests;
        $this->collectOptions = $collectOptions;

        if(! $this->canBringAdditionalGuests) {
            $this->maxAdditionalGuests = 0;
        }
    }

    public function toJSON(array $options = []): array
    {
        return [
            'additionalGuest' => $this->canBringAdditionalGuests(),
            'additionalGuestMax' => $this->getMaxAdditionalGuests(),
            'collectInfo' => $this->getCollectOptions()->toJSON(),
        ];
    }

    public function canBringAdditionalGuests(): bool
    {
        return $this->canBringAdditionalGuests;
    }

    public function getMaxAdditionalGuests(): int
    {
        return $this->maxAdditionalGuests;
    }

    public function getCollectOptions(): EventMailingOptionsConfigGuestsCollectOptions
    {
        return $this->collectOptions;
    }
}