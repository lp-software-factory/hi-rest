<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfig;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\EventMailingOptionsConfigDataMapper;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\EventMailingOptionsDomainBundle;

final class DefaultEventMailingOptionsConfigService
{
    /** @var EventMailingOptionsDomainBundle */
    private $bundle;

    /** @var EventMailingOptionsConfigDataMapper */
    private $dataMapper;

    public function __construct(EventMailingOptionsDomainBundle $bundle, EventMailingOptionsConfigDataMapper $dataMapper)
    {
        $this->bundle = $bundle;
        $this->dataMapper = $dataMapper;
    }

    public function createDefaultOptions(): EventMailingOptionsConfig
    {
        $source = file_get_contents(sprintf('%s/defaults.json', $this->bundle->getResourcesDir()));

        return $this->dataMapper->createConfigFromArray(json_decode($source, true));
    }
}