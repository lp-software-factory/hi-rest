<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Entity\EventMailingOptions;

final class EventMailingOptionsDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventMailingOptions::class;
    }
}