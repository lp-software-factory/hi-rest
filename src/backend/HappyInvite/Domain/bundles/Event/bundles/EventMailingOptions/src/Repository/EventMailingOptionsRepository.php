<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Entity\EventMailingOptions;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Exceptions\EventMailingOptionsNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventMailingOptionsRepository extends EntityRepository
{
    public function createOptions(EventMailingOptions $options): int
    {
        while ($this->hasWithSID($options->getSID())) {
            $options->regenerateSID();
        }

        $this->getEntityManager()->persist($options);
        $this->getEntityManager()->flush($options);

        return $options->getId();
    }

    public function saveOptions(EventMailingOptions $options)
    {
        $this->getEntityManager()->flush($options);
    }

    public function saveOptionsEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveOptions($entity);
        }
    }

    public function deleteOptions(EventMailingOptions $options)
    {
        $this->getEntityManager()->remove($options);
        $this->getEntityManager()->flush($options);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventMailingOptions
    {
        $result = $this->find($id);

        if ($result instanceof EventMailingOptions) {
            return $result;
        } else {
            throw new EventMailingOptionsNotFoundException(sprintf('Event mailing options `ID(%d)` not found', $id));
        }
    }

    public function getByEventId(int $eventId): EventMailingOptions
    {
        $result = $this->findOneBy([
            'event' => $eventId,
        ]);

        if ($result instanceof EventMailingOptions) {
            return $result;
        } else {
            throw new EventMailingOptionsNotFoundException(sprintf('Event mailing options `EventID(%d)` not found', $eventId));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Event mailing optionss with IDs (%s) not found', array_diff($ids, array_map(function (EventMailingOptions $options) {
                return $options->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventMailingOptions
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EventMailingOptions) {
            return $result;
        } else {
            throw new EventMailingOptionsNotFoundException(sprintf('Event mailing options `SID(%s)` not found', $sid));
        }
    }
}