<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfig;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigAdvanced;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigBasic;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigGuests;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigGuestsCollectOptions;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigQuiz;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity\EventMailingOptionsConfigQuizAnswer;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Exceptions\EventMailingOptionsDataMapperException;

final class EventMailingOptionsConfigDataMapper
{
    public function createConfigFromArray(array $input): EventMailingOptionsConfig
    {
        try {
            $basic = $this->createConfigBasicFromArray($input['blocks']['baseInfo']);
            $advanced = $this->createConfigAdvancedFromArray($input['blocks']['advancedSettings']);
            $guests = $this->createConfigGuestsFromArray($input['blocks']['guests']);
            $quizzes = array_map(function(array $quizInput) {
                return $this->createConfigQuiz($quizInput);
            }, $input['blocks']['quizzes']);

            return new EventMailingOptionsConfig(
                $basic,
                $advanced,
                $guests,
                $quizzes
            );
        }catch(\Exception|\Error $e) {
            throw new EventMailingOptionsDataMapperException($e->getMessage());
        }
    }

    private function createConfigBasicFromArray(array $input): EventMailingOptionsConfigBasic
    {
        return new EventMailingOptionsConfigBasic(
            $input['date'] === null ? null : \DateTime::createFromFormat(\DateTime::RFC2822, $input['date']),
            $input['answerCollect'],
            $input['answersAfterDateOver'],
            $input['guestsMax'],
            $input['showEmptySpaces'],
            $input['description']
        );
    }

    private function createConfigAdvancedFromArray(array $input): EventMailingOptionsConfigAdvanced
    {
        return new EventMailingOptionsConfigAdvanced(
            $input['allowEditOnAnswerPage'],
            $input['allowEditAfterSend'],
            $input['allowSendMessagesWithAnswer'],
            $input['sendMailAfterAnswer'],
            $input['yesDescription'],
            $input['noDescription']
        );
    }

    private function createConfigGuestsFromArray(array $input): EventMailingOptionsConfigGuests
    {
        return new EventMailingOptionsConfigGuests(
            $input['additionalGuest'],
            $input['additionalGuestMax'],
            $this->createConfigGuestsOptionsFromArray($input['collectInfo'])
        );
    }

    private function createConfigGuestsOptionsFromArray(array $input): EventMailingOptionsConfigGuestsCollectOptions
    {
        return new EventMailingOptionsConfigGuestsCollectOptions(
            $input['firstName'],
            $input['lastName'],
            $input['city'],
            $input['email'],
            $input['firstNameRequired'],
            $input['lastNameRequired'],
            $input['cityRequired'],
            $input['emailRequired']
        );
    }

    private function createConfigQuiz(array $input): EventMailingOptionsConfigQuiz
    {
        return new EventMailingOptionsConfigQuiz(
            $input['sid'],
            $input['target'],
            $input['question'],
            array_map(function(array $inputAnswer) {
                return $this->createConfigQuizAnswer($inputAnswer);
            }, $input['answers'])
        );
    }

    private function createConfigQuizAnswer(array $input): EventMailingOptionsConfigQuizAnswer
    {
        return new EventMailingOptionsConfigQuizAnswer(
            $input['sid'],
            $input['title']
        );
    }
}