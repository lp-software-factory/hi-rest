<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\DataMapper\EventMailingOptionsConfig\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;

final class EventMailingOptionsConfig implements JSONSerializable
{
    /** @var EventMailingOptionsConfigBasic */
    private $basic;

    /** @var EventMailingOptionsConfigAdvanced */
    private $advanced;

    /** @var EventMailingOptionsConfigGuests */
    private $guests;

    /** @var EventMailingOptionsConfigQuiz[] */
    private $quizzes;

    public function __construct(
        EventMailingOptionsConfigBasic $basic,
        EventMailingOptionsConfigAdvanced $advanced,
        EventMailingOptionsConfigGuests $guests,
        array $quizzes
    ) {
        foreach($quizzes as $quiz) {
            assert('$quiz instanceof EventMailingOptionsConfigQuiz');
        }

        $this->basic = $basic;
        $this->advanced = $advanced;
        $this->guests = $guests;
        $this->quizzes = $quizzes;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'baseInfo' => $this->getBasic()->toJSON(),
            'advancedSettings' => $this->getAdvanced()->toJSON(),
            'guests' => $this->getGuests()->toJSON(),
            'quizzes' => array_map(function(EventMailingOptionsConfigQuiz $quiz) {
                return $quiz->toJSON();
            }, $this->getQuizzes())
        ];
    }

    public function getBasic(): EventMailingOptionsConfigBasic
    {
        return $this->basic;
    }

    public function getAdvanced(): EventMailingOptionsConfigAdvanced
    {
        return $this->advanced;
    }

    public function getGuests(): EventMailingOptionsConfigGuests
    {
        return $this->guests;
    }

    public function getQuizzes(): array
    {
        return $this->quizzes;
    }
}