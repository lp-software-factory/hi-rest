<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Exceptions;

final class EventMailingOptionsDataMapperException extends \Exception {}