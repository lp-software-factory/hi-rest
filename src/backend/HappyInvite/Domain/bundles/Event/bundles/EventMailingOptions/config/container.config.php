<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Factory\Doctrine\EventMailingOptionsDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Repository\EventMailingOptionsRepository;

return [
    EventMailingOptionsRepository::class => factory(EventMailingOptionsDoctrineRepositoryFactory::class),
];