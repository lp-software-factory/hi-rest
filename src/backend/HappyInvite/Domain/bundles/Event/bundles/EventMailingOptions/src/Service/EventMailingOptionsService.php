<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Entity\EventMailingOptions;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Parameters\EditEventMailingOptionsParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Repository\EventMailingOptionsRepository;

final class EventMailingOptionsService
{
    public const EVENT_CREATED = 'hi.domain.event-mailing-options.options.created';
    public const EVENT_UPDATED = 'hi.domain.event-mailing-options.options.updated';
    public const EVENT_DELETE = 'hi.domain.event-mailing-options.options.delete';
    public const EVENT_DELETED = 'hi.domain.event-mailing-options.options.deleted';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EventMailingOptionsRepository */
    private $optionsRepository;

    /** @var AuthToken */
    private $authToken;

    /** @var DefaultEventMailingOptionsConfigService */
    private $defaults;

    public function __construct(
        EventMailingOptionsRepository $eventMailingOptionsRepository,
        AuthToken $authToken,
        DefaultEventMailingOptionsConfigService $defaultEventMailingOptionsConfigService
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->optionsRepository = $eventMailingOptionsRepository;
        $this->authToken = $authToken;
        $this->defaults = $defaultEventMailingOptionsConfigService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    private function validateOwner(Event $event): void {
        if(! $this->authToken->isSignedIn()) {
            throw new EventIsNotYoursException("You're not signed in.");
        }

        if(! $this->authToken->getAccount()->hasAccess('admin') && ! ($event->getOwner()->getId() === $this->authToken->getProfile()->getId())) {
            throw new EventIsNotYoursException(sprintf('Event with ID %s is not yours', $event->getId()));
        }
    }

    public function createEventMailingOptionsForEvent(Event $event): EventMailingOptions
    {
        $eventMailingOptions = new EventMailingOptions($event, $this->defaults->createDefaultOptions());

        $this->optionsRepository->createOptions($eventMailingOptions);
        $this->eventEmitter->emit(self::EVENT_CREATED, [$eventMailingOptions, $event]);

        return $eventMailingOptions;
    }

    public function editEventMailingOptions(int $id, EditEventMailingOptionsParameters $parameters): EventMailingOptions
    {
        $options = $this->optionsRepository->getById($id);

        $this->validateOwner($options->getEvent());

        $options->setConfig($parameters->getConfig());
        $this->optionsRepository->saveOptions($options);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$options]);

        return $options;
    }

    public function deleteEventMailingOptionsOfEvent(int $eventId): void
    {
        $options = $this->getByEventId($eventId);
        $optionsId = $options->getId();

        $this->eventEmitter->emit(self::EVENT_DELETE, [$options, $optionsId, $eventId]);
        $this->optionsRepository->deleteOptions($options);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$options, $optionsId, $eventId]);
    }

    public function getById(int $id): EventMailingOptions
    {
        return $this->optionsRepository->getById($id);
    }

    public function getByEventId(int $eventId): EventMailingOptions
    {
        return $this->optionsRepository->getByEventId($eventId);
    }
}