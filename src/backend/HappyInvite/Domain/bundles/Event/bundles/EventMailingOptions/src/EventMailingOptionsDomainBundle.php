<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Subscriptions\AutoCreateEventMailingOptionsSubscriptionScript;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Subscriptions\AutoDestroyEventMailingOptionsSubscriptionScript;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EventMailingOptionsDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoCreateEventMailingOptionsSubscriptionScript::class,
            AutoDestroyEventMailingOptionsSubscriptionScript::class,
        ];
    }
}