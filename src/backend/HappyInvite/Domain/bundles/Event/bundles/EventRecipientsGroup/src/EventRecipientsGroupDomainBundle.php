<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Subscriptions\DefaultEventRecipientsEventScript;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EventRecipientsGroupDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            DefaultEventRecipientsEventScript::class,
        ];
    }
}