<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;

final class EventRecipientsGroupDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventRecipientsGroup::class;
    }
}