<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupIsNotYoursException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters\CreateEventRecipientsGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters\SetTitleEventRecipientsGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Repository\EventRecipientsGroupRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class EventRecipientsGroupService
{
    public const EVENT_CREATED = 'hi.domain.event-recipients-group.entity.created';
    public const EVENT_EDITED = 'hi.domain.event-recipients-group.entity.edited';
    public const EVENT_UPDATED = 'hi.domain.event-recipients-group.entity.updated';
    public const EVENT_DELETE = 'hi.domain.event-recipients-group.entity.delete.before';
    public const EVENT_DELETED = 'hi.domain.event-recipients-group.entity.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var AuthToken */
    private $authToken;

    /** @var EventRecipientsGroupRepository */
    private $entityRepository;

    public function __construct(AuthToken $authToken, EventRecipientsGroupRepository $eventRecipientsGroupRepository)
    {
        $this->authToken = $authToken;
        $this->entityRepository = $eventRecipientsGroupRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    private function validateOwner(Profile $owner): void
    {
        if(! $this->authToken->isSignedIn()) {
            throw new EventRecipientsGroupIsNotYoursException("You're not signed in.");
        }


        if(! ($this->authToken->getAccount()->hasAccess('admin'))  && ! ($this->authToken->getProfile()->getId() === $owner->getId())) {
            throw new EventRecipientsGroupIsNotYoursException("You're not an owner of EventRecipientsGroup");
        }
    }

    public function create(CreateEventRecipientsGroupParameters $parameters): EventRecipientsGroup
    {
        $entity = new EventRecipientsGroup(
            $parameters->getOwner(),
            $parameters->getTitle()
        );

        $this->entityRepository->createEntity($entity);

        $this->eventEmitter->emit(self::EVENT_CREATED, [$entity]);

        return $entity;
    }

    public function setTitle(int $entityId, SetTitleEventRecipientsGroupParameters $parameters): EventRecipientsGroup
    {
        $entity = $this->entityRepository->getById($entityId);

        $this->validateOwner($entity->getOwner());
        
        $entity->setTitle($parameters->getTitle());

        $this->entityRepository->saveEntity($entity);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$entity]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$entity]);

        return $entity;
    }

    public function delete(int $entityId)
    {
        $entity = $this->getById($entityId);

        $this->validateOwner($entity->getOwner());

        $this->eventEmitter->emit(self::EVENT_DELETE, [$entity]);
        $this->entityRepository->deleteEntity($entity);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$entityId, $entity]);
    }

    public function getById(int $entityId): EventRecipientsGroup
    {
        return $this->entityRepository->getById($entityId);
    }

    public function getByIdAdnOwner(int $entityId): EventRecipientsGroup
    {
        $entity = $this->entityRepository->getById($entityId);

        $this->validateOwner($entity->getOwner());

        return $entity;
    }

    public function getAllByOwner(Profile $owner): array
    {
        return $this->entityRepository->getAllByOwner($owner);
    }
}