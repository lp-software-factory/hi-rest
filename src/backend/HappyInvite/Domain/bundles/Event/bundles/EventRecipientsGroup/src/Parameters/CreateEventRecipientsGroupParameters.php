<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class CreateEventRecipientsGroupParameters
{
    /** @var Profile */
    private $owner;

    /** @var string */
    private $title;

    public function __construct(Profile $owner, $title)
    {
        $this->owner = $owner;
        $this->title = $title;
    }

    public function getOwner(): Profile
    {
        return $this->owner;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}