<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Repository\EventRecipientsGroupRepository")
 * @Table(name="event_recipients_group")
 */
class EventRecipientsGroup implements JSONSerializable, IdEntity, SIDEntity, SerialEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait, SerialEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="owner_profile_id", referencedColumnName="id")
     * @var Profile
     */
    private $owner;

    /**
     * @Column(name="title", type="string")
     * @var string
     */
    private $title;

    public function __construct(Profile $owner, string $title)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->owner = $owner;
        $this->title = $title;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'position' => $this->getPosition(),
            'version' => $this->getVersion(),
            'owner_profile_id' => $this->getOwner()->getId(),
            'title' => $this->getTitle(),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getOwner(): Profile
    {
        return $this->owner;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }
}