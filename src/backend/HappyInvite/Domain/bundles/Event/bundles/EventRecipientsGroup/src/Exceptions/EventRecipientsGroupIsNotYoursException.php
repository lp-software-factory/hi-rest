<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions;

final class EventRecipientsGroupIsNotYoursException extends \Exception {}