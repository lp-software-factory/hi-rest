<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Factory\Doctrine\EventRecipientsGroupDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Repository\EventRecipientsGroupRepository;

return [
    EventRecipientsGroupRepository::class => factory(EventRecipientsGroupDoctrineRepositoryFactory::class),
];