<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Exceptions\EventRecipientsGroupNotFoundException;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventRecipientsGroupRepository extends EntityRepository
{
    public function createEntity(EventRecipientsGroup $entity): int
    {
        while ($this->hasWithSID($entity->getSID())) {
            $entity->regenerateSID();
        }

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        return $entity->getId();
    }

    public function saveEntity(EventRecipientsGroup $entity)
    {
        $this->getEntityManager()->flush($entity);
    }

    public function saveEntityEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveEntity($entity);
        }
    }

    public function deleteEntity(EventRecipientsGroup $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function listEntities(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAllByOwner(Profile $owner): array
    {
        return $this->findBy([
            'owner' => $owner,
        ], ['position' => 'asc']);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventRecipientsGroup
    {
        $result = $this->find($id);

        if ($result instanceof EventRecipientsGroup) {
            return $result;
        } else {
            throw new EventRecipientsGroupNotFoundException(sprintf('Event recipients group `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Event recipients groups with IDs (%s) not found', array_diff($ids, array_map(function (EventRecipientsGroup $entity) {
                return $entity->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventRecipientsGroup
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EventRecipientsGroup) {
            return $result;
        } else {
            throw new EventRecipientsGroupNotFoundException(sprintf('Event recipients group `SID(%s)` not found', $sid));
        }
    }
}