<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters\CreateEventRecipientsGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Service\EventRecipientsGroupService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class DefaultEventRecipientsEventScript implements SubscriptionScript
{
    public const PREDEFINED_GROUPS = [
        '$predefined:family',
        '$predefined:friends',
        '$predefined:colleagues',
    ];

    /** @var ProfileService */
    private $profileService;

    /** @var EventRecipientsGroupService */
    private $eventRecipientsGroupService;

    public function __construct(
        ProfileService $profileService,
        EventRecipientsGroupService $eventRecipientsGroupService
    ) {
        $this->profileService = $profileService;
        $this->eventRecipientsGroupService = $eventRecipientsGroupService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->profileService->getEventEmitter()->on(ProfileService::EVENT_PROFILE_CREATED, function(Profile $profile) {
            foreach(self::PREDEFINED_GROUPS as $title) {
                $this->eventRecipientsGroupService->create(new CreateEventRecipientsGroupParameters($profile, $title));
            }
        });
    }
}