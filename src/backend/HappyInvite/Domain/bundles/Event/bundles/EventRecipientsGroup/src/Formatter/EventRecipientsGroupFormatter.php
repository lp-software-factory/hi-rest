<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;

final class EventRecipientsGroupFormatter
{
    public function formatOne(EventRecipientsGroup $entity): array
    {
        return [
            'entity' => $entity->toJSON(),
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(EventRecipientsGroup $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}