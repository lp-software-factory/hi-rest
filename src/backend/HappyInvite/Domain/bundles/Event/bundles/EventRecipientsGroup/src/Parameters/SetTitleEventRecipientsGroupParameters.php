<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Parameters;

final class SetTitleEventRecipientsGroupParameters
{
    /** @var string */
    private $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}