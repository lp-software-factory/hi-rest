<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters;

abstract class AbstractEventLandingTemplateParameters
{
    /** @var array */
    private $json;

    public function __construct(array $json)
    {
        $this->json = $json;
    }

    public function getJson(): array
    {
        return $this->json;
    }
}