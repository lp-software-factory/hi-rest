<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters\CreateEventLandingTemplateParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters\EditEventLandingTemplateParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Repository\EventLandingTemplateRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class EventLandingTemplateService
{
    public const EVENT_CREATED = 'hi.domain.event-landing-template.template.created';
    public const EVENT_EDITED = 'hi.domain.event-landing-template.template.edited';
    public const EVENT_USER_SPECIFIED = 'hi.domain.event-landing-template.template.solution.user-specified';
    public const EVENT_OWNER_SPECIFIED = 'hi.domain.event-landing-template.template.solution.owner-specified';
    public const EVENT_UPDATED = 'hi.domain.event-landing-template.template.updated';
    public const EVENT_DELETE = 'hi.domain.event-landing-template.template.delete.before';
    public const EVENT_DELETED = 'hi.domain.event-landing-template.template.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EventLandingTemplateRepository */
    private $templateRepository;

    public function __construct(EventLandingTemplateRepository $eventLandingTemplateRepository)
    {
        $this->templateRepository = $eventLandingTemplateRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEventLandingTemplate(CreateEventLandingTemplateParameters $parameters): EventLandingTemplate
    {
        $template = new EventLandingTemplate(
            $parameters->getJson()
        );

        $this->templateRepository->createTemplate($template);

        $this->eventEmitter->emit(self::EVENT_CREATED, [$template]);

        return $template;
    }

    public function duplicateEventLandingTemplate(EventLandingTemplate $input): EventLandingTemplate
    {
        return $this->createEventLandingTemplate(new CreateEventLandingTemplateParameters(
            $input->getJson()
        ));
    }

    public function editEventLandingTemplate(int $id, EditEventLandingTemplateParameters $parameters): EventLandingTemplate
    {
        $template = $this->templateRepository->getById($id);

        $template->setJson($parameters->getJson());

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        return $template;
    }

    public function setOwner(int $id, Profile $newOwner): void
    {
        $template = $this->templateRepository->getById($id);
        $template->setSolutionComponentOwner($newOwner);

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_OWNER_SPECIFIED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        $this->templateRepository->saveTemplate($template);
    }

    public function markAsUserSpecified(int $id, Profile $newOwner)
    {
        $template = $this->templateRepository->getById($id);
        $template->setSolutionComponentOwner($newOwner);
        $template->markSolutionAsUserSpecified();

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_USER_SPECIFIED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        $this->templateRepository->saveTemplate($template);
    }

    public function deleteEventLandingTemplate(int $id)
    {
        $template = $this->getById($id);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$template]);
        $this->templateRepository->deleteTemplate($template);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$id, $template]);
    }

    public function getById(int $id): EventLandingTemplate
    {
        return $this->templateRepository->getById($id);
    }

    public function getByIds(IdsCriteria $ids): array
    {
        return $this->templateRepository->getByIds($ids);
    }

    public function getAll(): array
    {
        return $this->templateRepository->getAll();
    }
}