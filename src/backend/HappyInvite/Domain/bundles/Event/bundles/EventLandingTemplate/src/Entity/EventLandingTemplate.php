<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity;

use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntity;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Repository\EventLandingTemplateRepository")
 * @Table(name="event_landing_template")
 */
class EventLandingTemplate implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity, SolutionComponentEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait, SolutionComponentEntityTrait;
    /**
     * @Column(name="json", type="json_array")
     * @var array
     */
    private $json;

    public function __construct(array $json)
    {
        $this->json = $json;

        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'solution' => [
                'is_user_specified' => $this->isSolutionUserSpecified(),
                'owner' => [
                    'has' => $this->hasSolutionComponentOwner(),
                ]
            ],
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'json' => $this->getJson(),
        ];

        if($this->hasSolutionComponentOwner()) {
            $json['solution']['owner']['profile'] = $this->getSolutionComponentOwner()->toJSON($options);
        }

        return $json;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getJson(): array
    {
        return $this->json;
    }

    public function setJson(array $json): self
    {
        $this->json = $json;
        $this->markAsUpdated();

        return $this;
    }
}