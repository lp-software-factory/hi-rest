<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Exceptions;

final class EventLandingTemplateNotFoundException extends \Exception {}