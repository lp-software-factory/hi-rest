<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service\EventLandingTemplateService;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AssignSolutionSubscriptionScript implements SubscriptionScript
{
    /** @var SolutionService */
    private $solutionService;

    /** @var EventLandingTemplateService */
    private $eventLandingTemplateService;

    public function __construct(
        SolutionService $solutionService,
        EventLandingTemplateService $eventLandingTemplateService
    ) {
        $this->solutionService = $solutionService;
        $this->eventLandingTemplateService = $eventLandingTemplateService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_ASSIGNED_TO, function(Solution $solution) {
            /** @var EventLandingTemplate $eventLandingTemplate */
            foreach($solution->getEventLandingTemplates() as $eventLandingTemplate) {
                $this->eventLandingTemplateService->setOwner($eventLandingTemplate->getId(), $solution->getTarget());
            }
        });

        $this->eventLandingTemplateService->getEventEmitter()->on(EventLandingTemplateService::EVENT_EDITED, function(EventLandingTemplate $eventLandingTemplate) {
            if($eventLandingTemplate->hasSolutionComponentOwner()) {
                $this->eventLandingTemplateService->markAsUserSpecified($eventLandingTemplate->getId(), $eventLandingTemplate->getSolutionComponentOwner());
            }
        });
    }
}