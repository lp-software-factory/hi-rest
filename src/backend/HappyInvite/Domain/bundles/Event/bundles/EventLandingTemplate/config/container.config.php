<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Factory\Doctrine\EventLandingTemplateDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Repository\EventLandingTemplateRepository;

return [
    EventLandingTemplateRepository::class => factory(EventLandingTemplateDoctrineRepositoryFactory::class),
];