<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;

final class EventLandingTemplateDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventLandingTemplate::class;
    }
}