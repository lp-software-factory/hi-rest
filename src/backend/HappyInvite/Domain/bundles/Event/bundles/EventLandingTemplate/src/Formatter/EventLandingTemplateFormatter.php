<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Formatter;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;

final class EventLandingTemplateFormatter
{
    public const OPTION_USE_IDS = 'use_ids';

    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function formatOne(EventLandingTemplate $entity, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_USE_IDS => false,
        ], $options);

        $json = $entity->toJSON();

        return [
            'entity' => $json,
            'attachments' => array_map(function(int $attachmentId) {
                return $this->attachmentService->getById($attachmentId)->toJSON();
            }, $this->getAttachmentIds($entity))
        ];
    }

    public function formatMany(array $entities, array $options = []): array
    {
        return array_map(function(EventLandingTemplate $entity) use ($options) {
            return $this->formatOne($entity, $options);
        }, $entities);
    }

    private function getAttachmentIds(EventLandingTemplate $entity): array
    {
        if(isset($entity->getJson()['import_attachment_ids'])) {
            return $entity->getJson()['import_attachment_ids'];
        }else{
            return [];
        }
    }
}