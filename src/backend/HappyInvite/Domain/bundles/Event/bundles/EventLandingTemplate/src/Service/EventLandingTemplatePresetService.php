<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\EventLandingTemplateDomainBundle;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Parameters\CreateEventLandingTemplateParameters;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Util\GenerateRandomString;

final class EventLandingTemplatePresetService
{
    /** @var EventLandingTemplateDomainBundle */
    private $eventLandingTemplateDomainBundle;

    /** @var EventLandingTemplateService */
    private $eventLandingTemplateService;

    public function __construct(
        EventLandingTemplateDomainBundle $eventLandingTemplateDomainBundle,
        EventLandingTemplateService $eventLandingTemplateService
    ) {
        $this->eventLandingTemplateDomainBundle = $eventLandingTemplateDomainBundle;
        $this->eventLandingTemplateService = $eventLandingTemplateService;
    }

    public function createBlackEventLandingTemplate(): EventLandingTemplate
    {
        $source = file_get_contents(sprintf('%s/defaults.json', $this->eventLandingTemplateDomainBundle->getResourcesDir()));
        $config = json_decode($source, true);

        $this->regenerateSIDs($config);

        return $this->eventLandingTemplateService->createEventLandingTemplate(new CreateEventLandingTemplateParameters($config));
    }

    private function regenerateSIDs(array &$input)
    {
        foreach($input as $key => &$value) {
            if(is_array($value)) {
                $this->regenerateSIDs($value);
            }else if($key === 'sid') {
                $value = GenerateRandomString::generate(SIDEntity::SID_LENGTH);
            }
        }
    }
}