<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Subscriptions;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service\EventLandingTemplateService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class SolutionDuplicateSubscription implements SubscriptionScript
{
    /** @var EventLandingTemplateService */
    private $eventLandingTemplateService;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(EventLandingTemplateService $eventLandingTemplateService, SolutionService $solutionService)
    {
        $this->eventLandingTemplateService = $eventLandingTemplateService;
        $this->solutionService = $solutionService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_BEFORE_DUPLICATE, function(Solution $origSolution, Solution $copySolution, Profile $owner = null) {
            $copySolution->setLandingTemplates(array_map(function(EventLandingTemplate $input) use ($owner) {
                $result = $this->eventLandingTemplateService->duplicateEventLandingTemplate($input);

                if($owner) {
                    $this->eventLandingTemplateService->setOwner($result->getId(), $owner);
                }

                return $result;
            }, $origSolution->getEventLandingTemplates()));
        });
    }
}