<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Exceptions\EventLandingTemplateNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventLandingTemplateRepository extends EntityRepository
{
    public function createTemplate(EventLandingTemplate $template): int
    {
        while ($this->hasWithSID($template->getSID())) {
            $template->regenerateSID();
        }

        $this->getEntityManager()->persist($template);
        $this->getEntityManager()->flush($template);

        return $template->getId();
    }

    public function saveTemplate(EventLandingTemplate $template)
    {
        $this->getEntityManager()->flush($template);
    }

    public function saveTemplateEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveTemplate($entity);
        }
    }

    public function deleteTemplate(EventLandingTemplate $template)
    {
        $this->getEntityManager()->remove($template);
        $this->getEntityManager()->flush($template);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): EventLandingTemplate
    {
        $result = $this->find($id);

        if ($result instanceof EventLandingTemplate) {
            return $result;
        } else {
            throw new EventLandingTemplateNotFoundException(sprintf('Event landing template `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Event landing templates with IDs (%s) not found', array_diff($ids, array_map(function (EventLandingTemplate $template) {
                return $template->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventLandingTemplate
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof EventLandingTemplate) {
            return $result;
        } else {
            throw new EventLandingTemplateNotFoundException(sprintf('Event landing template `SID(%s)` not found', $sid));
        }
    }
}