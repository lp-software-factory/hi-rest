<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Subscriptions\AssignSolutionSubscriptionScript;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Subscriptions\SolutionDuplicateSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class EventLandingTemplateDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AssignSolutionSubscriptionScript::class,
            SolutionDuplicateSubscription::class,
        ];
    }
}