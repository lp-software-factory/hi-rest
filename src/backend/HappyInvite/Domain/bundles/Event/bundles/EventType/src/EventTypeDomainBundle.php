<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType;

use HappyInvite\Platform\Bundles\HIBundle;

final class EventTypeDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}