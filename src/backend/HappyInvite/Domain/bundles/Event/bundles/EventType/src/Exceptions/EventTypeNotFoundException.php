<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Exceptions;

final class EventTypeNotFoundException extends \Exception {}