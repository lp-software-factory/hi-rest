<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Parameters\CreateEventTypeParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Parameters\EditEventTypeParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Repository\EventTypeRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class EventTypeService
{
    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventType\Repository\EventTypeRepository */
    private $eventTypeRepository;

    /** @var EventTypeGroupService */
    private $eventTypeGroupService;

    public function __construct(
        EventTypeRepository $eventTypeRepository,
        EventTypeGroupService $eventTypeGroupService
    ) {
        $this->eventTypeRepository = $eventTypeRepository;
        $this->eventTypeGroupService = $eventTypeGroupService;
    }

    public function createEventType(CreateEventTypeParameters $parameters): EventType
    {
        $siblings = $this->getEventTypesByGroup($parameters->getGroupId());

        $eventType = new EventType(
            $parameters->getUrl(),
            $parameters->getTitle(),
            $parameters->getDescription(),
            $this->eventTypeGroupService->getEventTypeGroupById($parameters->getGroupId())
        );

        $serial = new SerialManager($siblings);
        $serial->insertLast($eventType);
        $serial->normalize();

        $this->eventTypeRepository->createEventType($eventType);
        $this->eventTypeRepository->saveEventTypeEntities($siblings);

        return $eventType;
    }

    public function editEventType(int $eventTypeId, EditEventTypeParameters $parameters): EventType
    {
        $eventType = $this->getEventTypeById($eventTypeId);

        $eventType->setUrl($parameters->getUrl());
        $eventType->setTitle($parameters->getTitle());
        $eventType->setDescription($parameters->getDescription());
        $eventType->setGroup($this->eventTypeGroupService->getEventTypeGroupById($parameters->getGroupId()));

        $this->eventTypeRepository->saveEventType($eventType);

        return $eventType;
    }

    public function moveUpEventType(int $eventTypeId): int
    {
        $eventType = $this->getEventTypeById($eventTypeId);
        $siblings = $this->getEventTypesByGroup($eventType->getGroup()->getId());

        $serial = new SerialManager($siblings);
        $serial->up($eventType);
        $serial->normalize();
        $this->eventTypeRepository->saveEventTypeEntities($siblings);

        return $eventType->getPosition();
    }

    public function moveDownEventType(int $eventTypeId): int
    {
        $eventType = $this->getEventTypeById($eventTypeId);
        $siblings = $this->getEventTypesByGroup($eventType->getGroup()->getId());

        $serial = new SerialManager($siblings);
        $serial->down($eventType);
        $serial->normalize();
        $this->eventTypeRepository->saveEventTypeEntities($siblings);

        return $eventType->getPosition();
    }

    public function deleteEventType(int $eventTypeId)
    {
        $this->eventTypeRepository->deleteEventType(
            $this->getEventTypeById($eventTypeId)
        );
    }

    public function getEventTypeById(int $eventTypeId): EventType
    {
        return $this->eventTypeRepository->getById($eventTypeId);
    }

    public function getEventTypeByIds(IdsCriteria $criteria): array
    {
        return $this->eventTypeRepository->getByIds($criteria);
    }

    public function getAll(): array
    {
        return $this->eventTypeRepository->getAll();
    }

    public function getEventTypesByGroup(int $eventGroupTypeId): array
    {
        return $this->eventTypeRepository->getByEventGroupType($eventGroupTypeId);
    }
}