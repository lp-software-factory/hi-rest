<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;

final class EventTypeRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return EventType::class;
    }
}