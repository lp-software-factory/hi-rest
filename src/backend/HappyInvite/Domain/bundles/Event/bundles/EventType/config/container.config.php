<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Factory\Doctrine\EventTypeRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Repository\EventTypeRepository;

return [
    EventTypeRepository::class => factory(EventTypeRepositoryFactory::class),
];