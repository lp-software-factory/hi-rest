<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\MutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\URLEntity\URLEntity;
use HappyInvite\Domain\Markers\URLEntity\URLEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\EventType\Repository\EventTypeRepository")
 * @Table(name="event_type")
 */
class EventType implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, SerialEntity,VersionEntity, URLEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait,VersionEntityTrait, URLEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup")
     * @JoinColumn(name="event_type_group_id", referencedColumnName="id")
     * @var \HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup
     */
    private $group;


    public function __construct(string $url, ImmutableLocalizedString $title, ImmutableLocalizedString $description, EventTypeGroup $group)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->group = $group;

        $this->setUrl($url);
        $this->regenerateSID();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            "position" => $this->getPosition(),
            "url" => $this->getUrl(),
            "title" => $this->getTitle()->toJSON($options),
            "description" => $this->getDescription()->toJSON($options),
            "event_type_group_id" => $this->getGroup()->getId(),
            "group" => [
                "id" => $this->getGroup()->getIdNoFall(),
                "title" => $this->getGroup()->getTitle()->toJSON($options),
                "description" => $this->getGroup()->getDescription()->toJSON($options),
            ],
        ];
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title)
    {
        $this->title = $title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description)
    {
        return $this->description = $description;
    }

    public function getGroup(): \HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Entity\EventTypeGroup
    {
        return $this->group;
    }

    public function setGroup(EventTypeGroup $group)
    {
        $this->group = $group;
    }
}