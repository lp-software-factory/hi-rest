<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

final class EditEventTypeParameters
{
    /** @var string */
    private $url;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var int */
    private $groupId;

    public function __construct(
        string $url,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        int $groupId
    ) {
        $this->url = $url;
        $this->title = $title;
        $this->description = $description;
        $this->groupId = $groupId;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }
}