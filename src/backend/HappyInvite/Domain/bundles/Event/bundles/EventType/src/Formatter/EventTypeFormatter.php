<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;

final class EventTypeFormatter
{
    public const OPTION_USE_IDS = 'use_ids';

    public function formatMany(array $eventTypes, array $options = []): array
    {
        return array_map(function(EventType $eventType) use ($options) {
            return $this->formatOne($eventType, $options);
        }, $eventTypes);
    }

    public function formatOne(EventType $eventType, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_USE_IDS => false,
        ], $options);

        $json = $eventType->toJSON();

        if($options[self::OPTION_USE_IDS]) {
            $json['group'] = null;
            $json['event_type_group_id'] = $eventType->getGroup()->getId();
        }

        return $json;
    }
}