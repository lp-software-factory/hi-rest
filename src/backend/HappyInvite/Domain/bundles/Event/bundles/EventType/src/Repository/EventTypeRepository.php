<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\EventType\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Exceptions\EventTypeNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventTypeRepository extends EntityRepository 
{
    public function createEventType(EventType $eventType): int
    {
        while($this->hasWithSID($eventType->getSID())) {
            $eventType->regenerateSID();
        }

        $this->getEntityManager()->persist($eventType);
        $this->getEntityManager()->flush($eventType);

        return $eventType->getId();
    }

    public function saveEventType(EventType $eventType)
    {
        $this->getEntityManager()->flush($eventType);
    }

    public function saveEventTypeEntities(array $entities)
    {
        $this->getEntityManager()->flush($entities);
    }

    public function deleteEventType(EventType $eventType)
    {
        $this->getEntityManager()->remove($eventType);
        $this->getEntityManager()->flush($eventType);
    }

    public function listEventTypes(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getById(int $id): EventType
    {
        $result = $this->find($id);

        if($result instanceof EventType) {
            return $result;
        }else{
            throw new EventTypeNotFoundException(sprintf('EventType `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('EventTypes with IDs (%s) not found', array_diff($ids, array_map(function(EventType $eventType) {
                return $eventType->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): EventType
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof EventType) {
            return $result;
        }else{
            throw new EventTypeNotFoundException(sprintf('EventType `SID(%s)` not found', $sid));
        }
    }

    public function getByEventGroupType(int $eventGroupTypeId)
    {
        return $this->findBy([
            'group' => $eventGroupTypeId
        ]);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }
}