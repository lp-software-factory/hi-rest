<?php
namespace HappyInvite\Domain\Bundles\Event;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Type\EventFeaturesType;

return [
    'hi.config.domain.doctrine.types' => [
        'event_features' => EventFeaturesType::class,
    ]
];