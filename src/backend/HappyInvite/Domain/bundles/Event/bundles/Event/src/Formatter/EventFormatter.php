<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Formatter;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Formatter\EventMailingOptionsFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventMailingOptions\Service\EventMailingOptionsService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Formatter\EventRecipientsFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionFormatter;

final class EventFormatter
{
    /** @var SolutionFormatter */
    private $solutionFormatter;

    /** @var EventRecipientsService */
    private $eventRecipientsService;

    /** @var EventRecipientsFormatter */
    private $eventRecipientsFormatter;

    /** @var EventMailingOptionsService */
    private $eventMailingOptionsService;

    /** @var EventMailingOptionsFormatter */
    private $eventMailingOptionsFormatter;

    public function __construct(
        SolutionFormatter $solutionFormatter,
        EventRecipientsService $eventRecipientsService,
        EventRecipientsFormatter $eventRecipientsFormatter,
        EventMailingOptionsService $eventMailingOptionsService,
        EventMailingOptionsFormatter $eventMailingOptionsFormatter
    ) {
        $this->solutionFormatter = $solutionFormatter;
        $this->eventRecipientsService = $eventRecipientsService;
        $this->eventRecipientsFormatter = $eventRecipientsFormatter;
        $this->eventMailingOptionsService = $eventMailingOptionsService;
        $this->eventMailingOptionsFormatter = $eventMailingOptionsFormatter;
    }

    public function formatOne(Event $entity): array
    {
        $json = $entity->toJSON();

        $eventRecipients = $this->eventRecipientsService->getByEventId($entity->getId());
        $eventMailingOptions = $this->eventMailingOptionsService->getByEventId($entity->getId());

        $json['event_recipients'] = $this->eventRecipientsFormatter->formatOne($eventRecipients);
        $json['event_mailing_options'] = $this->eventMailingOptionsFormatter->formatOne($eventMailingOptions);
        $json['solution']['original'] = $this->solutionFormatter->formatOne($entity->getOriginalSolution());
        $json['solution']['current'] = $this->solutionFormatter->formatOne($entity->getCurrentSolution());

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(Event $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }

    public function formatList(array $entities): array
    {
        return array_map(function(Event $event) {
            $eventRecipients = $this->eventRecipientsService->getByEventId($event->getId());
            $eventMailingOptions = $this->eventMailingOptionsService->getByEventId($event->getId());

            return [
                'event_id' => $event->getId(),
                'event_title' => $event->getTitle(),
                'event_recipients_id' => $eventRecipients->getId(),
                'event_mailing_options_id' => $eventMailingOptions->getId(),
                'original_solution_id' => $event->getOriginalSolution()->getId(),
                'current_solution_id' => $event->getCurrentSolution()->getId(),
                'event_has_contacts' => count($eventRecipients->getContacts()) > 0,
            ];
        }, $entities);
    }
}