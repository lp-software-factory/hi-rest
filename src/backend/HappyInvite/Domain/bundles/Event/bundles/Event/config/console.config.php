<?php
namespace HappyInvite\Domain\Bundles\Event;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Console\Command\EventSubmitCommand;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Console\Command\EventTestCommand;

return [
    'config.console' => [
        'commands' => [
            'Event' => [
                EventSubmitCommand::class,
                EventTestCommand::class,
            ]
        ]
    ]
];