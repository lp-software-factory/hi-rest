<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Factory;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;

final class EventDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Event::class;
    }
}