<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Console\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EventTestCommand extends Command
{
    /** @var EventService */
    private $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('event:test')
            ->setDescription('Test event with given SID')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('sid', InputArgument::REQUIRED, 'Event SID'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $event = $this->eventService->getBySID($input->getArgument('sid'));

        $output->writeln(sprintf('Test event: %s', $event->getSID()));
    }
}