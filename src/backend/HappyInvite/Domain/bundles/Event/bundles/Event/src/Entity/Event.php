<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Type\EventFeatures;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Event\Bundles\Event\Repository\EventRepository")
 * @Table(name="event")
 */
class Event implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait;

    /**
     * @Column(name="title", type="string")
     * @var string
     */
    private $title = '';

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="owner_id", referencedColumnName="id")
     * @var Profile
     */
    private $owner;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\Solution", cascade={"all"})
     * @JoinColumn(name="original_solution_id", referencedColumnName="id")
     * @var Solution
     */
    private $originalSolution;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\Solution", cascade={"all"})
     * @JoinColumn(name="current_solution_id", referencedColumnName="id")
     * @var Solution
     */
    private $currentSolution;

    /**
     * @Column(name="event_features", type="event_features")
     * @var EventFeatures
     */
    private $eventFeatures;

    /**
     * @Column(name="is_submit", type="boolean")
     * @var bool
     */
    private $isSubmit = false;

    /**
     * @Column(name="date_submit", type="datetime")
     * @var \DateTime
     */
    private $dateSubmit;

    public function __construct(
        Profile $owner,
        Solution $origSolution,
        Solution $currentSolution
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->owner = $owner;
        $this->originalSolution = $origSolution;
        $this->currentSolution = $currentSolution;
        $this->eventFeatures = new EventFeatures();

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'title' => $this->getTitle(),
            'owner' => $this->getOwner()->toJSON($options),
            'event_features' => $this->getEventFeatures()->toJSON($options),
            'solution' => [
                'original' => $this->getOriginalSolution()->toJSON($options),
                'current' => $this->getCurrentSolution()->toJSON($options),
            ],
            'is_submit' => $this->isSubmit(),
        ];

        if($this->isSubmit()) {
            $json['date_submit'] = $this->getDateSubmit()->format(\DateTime::RFC2822);
        }

        return $json;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getOwner(): Profile
    {
        return $this->owner;
    }

    public function getEventFeatures(): EventFeatures
    {
        return $this->eventFeatures;
    }

    public function setEventFeatures(EventFeatures $eventFeatures)
    {
        $this->eventFeatures = $eventFeatures;
    }

    public function getOriginalSolution(): Solution
    {
        return $this->originalSolution;
    }

    public function getCurrentSolution(): Solution
    {
        return $this->currentSolution;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function markAsSubmit(): self
    {
        $this->isSubmit = true;
        $this->dateSubmit = new \DateTime();

        $this->markAsUpdated();

        return $this;
    }

    public function isSubmit(): bool
    {
        return $this->isSubmit;
    }

    public function getDateSubmit(): \DateTime
    {
        return $this->dateSubmit;
    }
}