<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Type;

use HappyInvite\Domain\Exceptions\DoNotImplementItException;
use HappyInvite\Domain\Markers\JSONSerializable;

final class EventFeatures implements JSONSerializable
{
    /** @var bool */
    private $envelope = true;
    
    /** @var bool */
    private $landing = true;
    
    public function __construct(array $json = null)
    {
        $this->envelope = $json['envelope'] ?? true;
        $this->landing = $json['landing'] ?? true;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'envelope' => $this->isEnvelopeEnabled(),
            'landing' => $this->isLandingEnabled(),
        ];
    }

    public function isEnvelopeEnabled(): bool
    {
        return $this->envelope;
    }

    public function isLandingEnabled(): bool
    {
        return $this->landing;
    }

    /** @deprecated */
    public function setEnvelope(bool $envelope)
    {
        throw new DoNotImplementItException('It will not work. You should replace EventFeatures object instead of trying to modify one.');
    }

    /** @deprecated */
    public function setLanding(bool $landing)
    {
        throw new DoNotImplementItException('It will not work. You should replace EventFeatures object instead of trying to modify one.');
    }
}