<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Factory\EventDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Repository\EventRepository;

return [
    EventRepository::class => factory(EventDoctrineRepositoryFactory::class),
];