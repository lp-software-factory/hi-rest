<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;

final class CreateEventParameters extends AbstractEventParameters
{
    /** @var Profile */
    private $owner;

    /** @var Solution */
    private $originalSolution;

    public function __construct(Profile $owner, Solution $originalSolution)
    {
        $this->owner = $owner;
        $this->originalSolution = $originalSolution;
    }

    public function getOwner(): Profile
    {
        return $this->owner;
    }

    public function getOriginalSolution(): Solution
    {
        return $this->originalSolution;
    }
}