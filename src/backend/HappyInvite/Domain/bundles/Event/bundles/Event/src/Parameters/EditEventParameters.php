<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters;

final class EditEventParameters extends AbstractEventParameters
{
    /** @var string */
    private $title;

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}