<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions;

final class EventIsNotYoursException extends \Exception {}