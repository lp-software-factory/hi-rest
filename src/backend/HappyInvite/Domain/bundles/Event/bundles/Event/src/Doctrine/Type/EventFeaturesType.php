<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class EventFeaturesType extends Type
{
    public const TYPE_NAME = 'event_features';

    public function getName()
    {
        return self::TYPE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if($value instanceof EventFeatures) {
            return json_encode($value->toJSON());
        }else{
            return '[]';
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return new EventFeatures();
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;

        return new EventFeatures(json_decode($value, true));
    }
}