<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventNotFoundException;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class EventRepository extends EntityRepository
{
    public function create(Event $event): int
    {
        while ($this->hasWithSID($event->getSID())) {
            $event->regenerateSID();
        }

        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush($event);

        return $event->getId();
    }

    public function save(Event $event)
    {
        $this->getEntityManager()->flush($event);
    }

    public function saveEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->save($entity);
        }
    }

    public function delete(Event $event)
    {
        $this->getEntityManager()->remove($event);
        $this->getEntityManager()->flush($event);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): Event
    {
        $result = $this->find($id);

        if ($result instanceof Event) {
            return $result;
        } else {
            throw new EventNotFoundException(sprintf('Event   `ID(%d)` not found', $id));
        }
    }

    public function getByOwner(Profile $owner): array
    {
        return $this->findBy([
            'owner' => $owner,
        ], ['id' => 'desc']);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Event  s with IDs (%s) not found', array_diff($ids, array_map(function (Event $event) {
                return $event->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Event
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof Event) {
            return $result;
        } else {
            throw new EventNotFoundException(sprintf('Event   `SID(%s)` not found', $sid));
        }
    }
}