<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Console\Command;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\SubmitEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EventSubmitCommand extends Command
{
    /** @var EventService */
    private $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('event:submit')
            ->setDescription('Submit event with given SID')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('sid', InputArgument::REQUIRED, 'Event SID'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        EventService::$ALLOW_DUPLICATE_SUBMIT = true;

        $event = $this->eventService->getBySID($input->getArgument('sid'));

        $output->writeln(sprintf('Submit event: %s', $event->getSID()));
        $this->eventService->submitEvent($event->getId(), new SubmitEventParameters());
        $output->writeln('Done.');
    }
}