<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event;

use HappyInvite\Platform\Bundles\HIBundle;

final class EventDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}