<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions;

final class EventIsSubmittedException extends \Exception {}