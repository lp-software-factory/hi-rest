<?php
namespace HappyInvite\Domain\Bundles\Event\Bundles\Event\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventIsSubmittedException;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\CreateEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\EditEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\SubmitEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Parameters\TestEventParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Repository\EventRepository;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Doctrine\Type\EventFeatures;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;

final class EventService
{
    public static $ALLOW_DUPLICATE_SUBMIT = false;

    public const EVENT_CREATED = 'hi.domain.event.created';
    public const EVENT_EDITED = 'hi.domain.event.edited';
    public const EVENT_SUBMIT = 'hi.domain.event.submit';
    public const EVENT_SET_EVENT_FEATURES = 'hi.domain.event.set-event-features';
    public const EVENT_TEST = 'hi.domain.event.test';
    public const EVENT_USER_SPECIFIED = 'hi.domain.event.solution.user-specified';
    public const EVENT_UPDATED = 'hi.domain.event.updated';
    public const EVENT_DELETE = 'hi.domain.event.delete.before';
    public const EVENT_DELETED = 'hi.domain.event.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var EventRepository */
    private $repository;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(EventRepository $eventRepository, SolutionService $solutionService)
    {
        $this->repository = $eventRepository;
        $this->eventEmitter = new EventEmitter();
        $this->solutionService = $solutionService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createEvent(CreateEventParameters $parameters): Event
    {
        $duplicate = $this->solutionService->duplicateSolution($parameters->getOriginalSolution(), $parameters->getOwner());

        $event = new Event(
            $parameters->getOwner(),
            $parameters->getOriginalSolution(),
            $duplicate
        );

        $this->solutionService->assignTo($duplicate->getId(), $parameters->getOwner());
        $this->solutionService->markAsUserSpecified($duplicate->getId(), $parameters->getOwner());

        $this->repository->create($event);
        $this->eventEmitter->emit(self::EVENT_CREATED, [$event]);

        return $event;
    }

    public function editEvent(int $id, EditEventParameters $parameters): Event
    {
        $event = $this->repository->getById($id);
        $event->setTitle($parameters->getTitle());

        $this->repository->save($event);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$event]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$event]);

        return $event;
    }

    public function setEventFeatures(int $id, EventFeatures $eventFeatures): Event
    {
        $event = $this->repository->getById($id);
        $event->setEventFeatures($eventFeatures);

        $this->repository->save($event);

        $this->eventEmitter->emit(self::EVENT_SET_EVENT_FEATURES, [$event]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$event]);

        return $event;
    }

    public function submitEvent(int $id, SubmitEventParameters $parameters): Event
    {
        $event = $this->repository->getById($id);

        if($event->isSubmit() && !self::$ALLOW_DUPLICATE_SUBMIT) {
            throw new EventIsSubmittedException(sprintf('Event(ID: %d) is already submitted', $id));
        }

        $event->markAsSubmit();

        $this->repository->save($event);

        $this->eventEmitter->emit(self::EVENT_SUBMIT, [$event]);

        return $event;
    }

    public function testEvent(int $id, TestEventParameters $parameters): Event
    {
        $event = $this->repository->getById($id);

        $this->eventEmitter->emit(self::EVENT_TEST, [$event]);

        return $event;
    }

    public function setEventTitle(int $id, string $title): Event
    {
        $event = $this->repository->getById($id);
        $event->setTitle($title);

        $this->repository->save($event);

        $this->eventEmitter->emit(self::EVENT_UPDATED, [$event]);

        return $event;
    }
    
    public function deleteEvent(int $id)
    {
        $event = $this->getById($id);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$event, $id]);
        $this->repository->delete($event);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$event, $id]);
    }

    public function getById(int $id): Event
    {
        return $this->repository->getById($id);
    }

    public function getByOwner(Profile $owner): array
    {
        return $this->repository->getByOwner($owner);
    }

    public function getBySID(string $sid): Event
    {
        return $this->repository->getBySID($sid);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }
}