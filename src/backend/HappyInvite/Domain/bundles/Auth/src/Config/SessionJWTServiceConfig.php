<?php
namespace HappyInvite\Domain\Bundles\Auth\Config;

final class SessionJWTServiceConfig
{
    private $sessionKey;

    public function __construct(array $config)
    {
        $this->sessionKey = $config['key'];
    }

    public function getSessionKey()
    {
        return $this->sessionKey;
    }
}