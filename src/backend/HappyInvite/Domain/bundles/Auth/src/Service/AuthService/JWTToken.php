<?php
namespace HappyInvite\Domain\Bundles\Auth\Service\AuthService;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class JWTToken
{
    /** @var string */
    private $jwt;

    /** @var Account */
    private $account;

    /** @var Profile */
    private $profile;

    public function __construct(string $jwt, Account $account, Profile $profile)
    {
        $this->jwt = $jwt;
        $this->account = $account;
        $this->profile = $profile;
    }

    public function getAPIKey(): string
    {
        return $this->jwt;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }
}