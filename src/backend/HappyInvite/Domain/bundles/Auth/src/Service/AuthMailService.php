<?php
namespace HappyInvite\Domain\Bundles\Auth\Service;

use HappyInvite\Domain\Bundles\Auth\Entity\SignUpRequest;
use HappyInvite\Domain\Bundles\Mail\Service\MailService;

final class AuthMailService
{
    public const MAIL_TYPE_SIGN_UP = 'hi.domain.auth.sign-up';

    /** @var MailService */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendTokenVerificationEmail(SignUpRequest $request): \Swift_Message
    {
        $replaces = [
            '{{ FIRST_NAME }}' => $request->getFirstName(),
            '{{ TOKEN }}' => $request->getAcceptToken(),
            '{{ EMAIL }}' => $request->getEmail(),
        ];

        $message = \Swift_Message::newInstance()
            ->setTo($request->getEmail())
            ->setContentType('text/html')
            ->setBody($this->mailService->getTemplate('auth', 'sign-up', $replaces));


        $this->mailService->sendNoReply($message, self::MAIL_TYPE_SIGN_UP);

        return $message;
    }
}