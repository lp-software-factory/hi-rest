<?php
namespace HappyInvite\Domain\Bundles\Auth\Inject;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Auth\Exceptions\AuthTokenIsNotAvailableException;
use HappyInvite\Domain\Bundles\Auth\Service\JWTTokenService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Markers\JSONSerializable;

final class AuthToken implements JSONSerializable
{
    public const SESSION_KEY_JWT = 'hi.auth.jwt';

    public const EVENT_AUTH = 'hi.domain.auth.sign-in';
    public const EVENT_SIGN_OUT = 'hi.domain.auth.sign-out';

    /** @var EventEmitterInterface */
    private $evenEmitter;

    /** @var string */
    private $jwt;

    /** @var Account */
    private $account;

    /** @var Profile */
    private $profile;

    /** @var AccountService */
    private $accountService;

    /** @var JWTTokenService */
    private $jwtTokenService;

    public function __construct(AccountService $accountService, JWTTokenService $jwtTokenService)
    {
        $this->evenEmitter = new EventEmitter();
        $this->accountService = $accountService;
        $this->jwtTokenService = $jwtTokenService;
    }

    public function getEvenEmitter(): EventEmitterInterface
    {
        return $this->evenEmitter;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'jwt' => $this->getJWT(),
            'account' => $this->getAccount()->toJSON($options),
            'profile' => $this->getProfile()->toJSON($options),
        ];
    }

    public function auth(string $jwt)
    {
        $token = $this->jwtTokenService->decodeJWT($jwt);

        $accountId = $token['data']['accountId'];
        $profileId = $token['data']['profileId'];

        $this->jwt = $jwt;
        $this->account = $this->accountService->getAccountWithId($accountId);
        $this->profile = $this->account->getProfileWithId($profileId);

        $this->jwt = $jwt;

        $this->evenEmitter->emit(self::EVENT_AUTH, [$jwt, $this]);

        $_SESSION[self::SESSION_KEY_JWT] = $jwt;
    }

    public function signOut()
    {
        $this->jwt = null;
        $this->account = null;
        $this->profile = null;

        $this->evenEmitter->emit(self::EVENT_SIGN_OUT, [$this]);

        $_SESSION[self::SESSION_KEY_JWT] = null;
    }

    public function isSignedIn(): bool
    {
        return $this->jwt !== null;
    }

    public function getJWT(): string
    {
        if(! $this->isSignedIn()) {
            throw new AuthTokenIsNotAvailableException("You're not signed in");
        }

        return $this->jwt;
    }

    public function getAccount(): Account
    {
        if(! $this->isSignedIn()) {
            throw new AuthTokenIsNotAvailableException("You're not signed in");
        }

        return $this->account;
    }

    public function getProfile(): Profile
    {
        if(! $this->isSignedIn()) {
            throw new AuthTokenIsNotAvailableException("You're not signed in");
        }

        return $this->profile;
    }
}