<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class InvalidAcceptTokenException extends \Exception {}