<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class JWTTokenIsNotAvailableException extends \Exception {}