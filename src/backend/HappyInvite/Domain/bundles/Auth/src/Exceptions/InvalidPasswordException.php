<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class InvalidPasswordException extends \Exception {}