<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class AuthTokenIsNotAvailableException extends \Exception {}