<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class JWTTokenFailException extends \Exception {}