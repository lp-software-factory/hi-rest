<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class ResetPasswordRequestNotFoundException extends \Exception {}