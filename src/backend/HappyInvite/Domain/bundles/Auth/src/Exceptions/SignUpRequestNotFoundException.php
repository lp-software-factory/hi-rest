<?php
namespace HappyInvite\Domain\Bundles\Auth\Exceptions;

final class SignUpRequestNotFoundException extends \Exception {}