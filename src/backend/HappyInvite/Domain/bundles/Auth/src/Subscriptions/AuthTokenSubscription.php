<?php
namespace HappyInvite\Domain\Bundles\Auth\Subscriptions;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;
use HappyInvite\Domain\Bundles\Auth\Service\SessionJWTService;

final class AuthTokenSubscription implements SubscriptionScript
{
    /** @var AuthToken */
    private $authToken;

    /** @var SessionJWTService */
    private $sessionJWTService;

    public function __construct(AuthToken $authToken, SessionJWTService $sessionJWTService)
    {
        $this->authToken = $authToken;
        $this->sessionJWTService = $sessionJWTService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->authToken->getEvenEmitter()->on(AuthToken::EVENT_AUTH, function(string $jwt, AuthToken $authToken) {
            $this->sessionJWTService->saveJWT($jwt);
        });
    }
}