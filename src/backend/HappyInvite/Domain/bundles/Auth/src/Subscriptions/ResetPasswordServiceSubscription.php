<?php
namespace HappyInvite\Domain\Bundles\Auth\Subscriptions;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Account\Service\ResetPasswordService;
use HappyInvite\Domain\Bundles\Auth\Service\AuthService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class ResetPasswordServiceSubscription implements SubscriptionScript
{
    /** @var AuthService */
    private $authService;

    /** @var ResetPasswordService */
    private $resetPasswordService;

    public function __construct(AuthService $authService, ResetPasswordService $resetPasswordService)
    {
        $this->authService = $authService;
        $this->resetPasswordService = $resetPasswordService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->resetPasswordService->getEventEmitter()->on(ResetPasswordService::EVENT_RESET_PASSWORD, function(Account $account) {
            $this->authService->auth($account);
        });
    }
}