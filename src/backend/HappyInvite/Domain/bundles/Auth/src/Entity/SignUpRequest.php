<?php
namespace HappyInvite\Domain\Bundles\Auth\Entity;

use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;
use HappyInvite\Domain\Util\GenerateRandomString;
use Zend\Validator\ValidatorChain;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Auth\Repository\SignUpRequestRepository")
 * @Table(name="sign_up_request")
 */
class SignUpRequest implements IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, VersionEntity
{
    public const SIGN_UP_TOKEN_LENGTH = 32;
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @Column(type="string", name="accept_token")
     * @var string
     */
    private $acceptToken;

    /**
     * @Column(type="string", name="email")
     * @var string
     */
    private $email;

    /**
     * @Column(type="string", name="password")
     * @var string
     */
    private $password;

    /**
     * @Column(type="string", name="first_name")
     * @var string
     */
    private $firstName;

    /**
     * @Column(type="string", name="last_name")
     * @var string
     */
    private $lastName;

    /**
     * @Column(type="boolean", name="is_company_profile")
     * @var string
     */
    private $isCompanyProfile;

    /**
     * @Column(type="string", name="company_name")
     * @var string
     */
    private $companyName;

    /**
     * @Column(type="string", name="phone")
     * @var string
     */
    private $phone;

    public function __construct(
        string $email,
        string $password,
        string $firstName,
        string $lastName,
        ?string $phone,
        bool $isCompanyProfile = false,
        ?string $companyName = null
    )
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $emailValidator = (new ValidatorChain())
            ->attach(new \Zend\Validator\NotEmpty())
            ->attach(new \Zend\Validator\StringLength(['min' => 3, 'max' => 127]))
            ->attach(new \Zend\Validator\EmailAddress());

        if(! $emailValidator->isValid($email)) {
            throw new \Exception(sprintf('Invalid email `%s`', $email));
        }

        $passwordValidator = (new ValidatorChain())
            ->attach(new \Zend\Validator\NotEmpty())
            ->attach(new \Zend\Validator\StringLength(['min' => 3, 'max' => 127]));

        if(! $passwordValidator->isValid($password)) {
            throw new \Exception('Invalid password');
        }

        $this->email = $email;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->isCompanyProfile = $isCompanyProfile;
        $this->companyName = $companyName;
        $this->phone = $phone;

        $this->acceptToken = GenerateRandomString::generate(self::SIGN_UP_TOKEN_LENGTH);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getVersion(): string
    {
        return "1.0.0";
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getAcceptToken(): string
    {
        return $this->acceptToken;
    }

    public function isAcceptTokenValid(string $token): bool
    {
        return $this->acceptToken === $token;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function isCompanyProfile(): bool
    {
        return $this->isCompanyProfile;
    }

    public function getCompanyName(): string
    {
        if(! $this->isCompanyProfile()) {
            throw new \Exception(sprintf('Not a company'));
        }

        return $this->companyName;
    }

    public function hasPhone(): bool
    {
        return $this->phone !== null;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}