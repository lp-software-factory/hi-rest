<?php
namespace HappyInvite\Domain\Bundles\Auth\Parameters;

final class SignUpParameters
{
    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var bool */
    private $isCompanyProfile;

    /** @var string|null */
    private $companyName;

    /** @var string */
    private $phone;

    public function __construct(
        string $email,
        string $password,
        string $firstName,
        string $lastName,
        ?string $phone,
        bool $isCompanyProfile = false,
        ?string $companyName = null
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->isCompanyProfile = $isCompanyProfile;
        $this->companyName = $companyName;
        $this->phone = $phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function isIsCompanyProfile(): bool
    {
        return $this->isCompanyProfile;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function hasPhone(): bool
    {
        return $this->phone !== null;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}