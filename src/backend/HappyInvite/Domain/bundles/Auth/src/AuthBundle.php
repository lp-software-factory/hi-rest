<?php
namespace HappyInvite\Domain\Bundles\Auth;

use HappyInvite\Domain\Bundles\Auth\Frontend\AuthTokenFrontendScript;
use HappyInvite\Domain\Bundles\Auth\Subscriptions\AuthTokenSubscription;
use HappyInvite\Domain\Bundles\Auth\Subscriptions\ResetPasswordServiceSubscription;
use HappyInvite\Domain\Bundles\Frontend\FrontendBundleInjectable;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class AuthBundle extends HIBundle implements FrontendBundleInjectable, DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            AuthTokenFrontendScript::class,
        ];
    }

    public function getEventScripts(): array
    {
        return [
            AuthTokenSubscription::class,
            ResetPasswordServiceSubscription::class,
        ];
    }
}