<?php
namespace HappyInvite\Domain\Bundles\Auth\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Auth\Entity\SignUpRequest;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class SignUpRequestRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return SignUpRequest::class;
    }
}