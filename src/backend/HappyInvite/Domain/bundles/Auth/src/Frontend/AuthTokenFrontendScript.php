<?php
namespace HappyInvite\Domain\Bundles\Auth\Frontend;

use HappyInvite\Domain\Bundles\Auth\Formatter\AuthTokenFormatter;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Frontend\FrontendScript;

final class AuthTokenFrontendScript implements FrontendScript
{
    /** @var AuthToken */
    private $authToken;

    /** @var AuthTokenFormatter */
    private $authTokenFormatter;
    
    public function __construct(AuthToken $authToken, AuthTokenFormatter $authTokenFormatter)
    {
        $this->authToken = $authToken;
        $this->authTokenFormatter = $authTokenFormatter;
    }

    public function tags(): array
    {
        return [
            FrontendScript::TAG_GLOBAL,
            FrontendScript::TAG_IS_AUTHENTICATED,
        ];
    }

    public function __invoke(): array
    {
        if($this->authToken->isSignedIn()) {
            return [
                'auth' => $this->authTokenFormatter->format($this->authToken)
            ];
        }else{
            return [];
        }
    }
}