<?php
namespace HappyInvite\Domain\Bundles\Auth\Formatter;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Company\Service\CompanyService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Company\Formatter\Traits\ProfileCompaniesFormatterTrait;

final class AuthTokenFormatter
{
    use ProfileCompaniesFormatterTrait;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    public function format(AuthToken $authToken): array
    {
        return array_merge($authToken->toJSON(), [
            'companies' => $this->getProfileCompanies(array_map(function(Profile $profile) {
                return $profile->getId();
            }, $authToken->getAccount()->getProfiles())),
        ]);
    }
}