<?php
namespace HappyInvite\Domain\Bundles\Auth;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Auth\Config\SessionJWTServiceConfig;
use HappyInvite\Domain\Bundles\Auth\Factory\Doctrine\SignUpRequestRepositoryFactory;
use HappyInvite\Domain\Bundles\Auth\Repository\SignUpRequestRepository;
use HappyInvite\Domain\Bundles\Auth\Service\JWTTokenService;

return [
    SessionJWTServiceConfig::class => object()->constructorParameter('config', get('config.hi.main-site.account.session')),
    SignUpRequestRepository::class => factory(SignUpRequestRepositoryFactory::class),
    JWTTokenService::class => object()
        ->constructorParameter('env', get('config.environment'))
        ->constructorParameter('jwtSecret', get('config.auth.jwt.secret')),
];