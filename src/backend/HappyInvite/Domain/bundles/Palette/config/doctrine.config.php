<?php
namespace HappyInvite\Domain\Bundles\Palette;

use HappyInvite\Domain\Bundles\Palette\Doctrine\Type\Palette\PaletteType;

return [
    'hi.config.domain.doctrine.types' => [
        'palette' => PaletteType::class,
    ]
];