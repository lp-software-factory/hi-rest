<?php
namespace HappyInvite\Domain\Bundles\Palette\Doctrine\Type\Palette;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use HappyInvite\Domain\Bundles\Palette\Entity\Color;
use HappyInvite\Domain\Bundles\Palette\Entity\Palette;

final class PaletteType extends Type
{
    public const TYPE_NAME = 'palette';

    public function getName()
    {
        return self::TYPE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if($value instanceof Palette) {
            return json_encode($value->toJSON());
        }else{
            return '[]';
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return new Palette(
                'blue',
                new Color('blue', '500'),
                new Color('blue', '50'),
                new Color('blue', '900')
            );
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;
        $json = json_decode($value, true);

        return new Palette(
            $json['code'],
            new Color($json['background']['code'], $json['background']['hexCode']),
            new Color($json['foreground']['code'], $json['foreground']['hexCode']),
            new Color($json['border']['code'], $json['border']['hexCode'])
        );
    }
}