<?php
namespace HappyInvite\Domain\Bundles\Palette;

use HappyInvite\Domain\Bundles\Palette\Frontend\PalettesFrontendScript;
use HappyInvite\Domain\Bundles\Frontend\FrontendBundleInjectable;
use HappyInvite\Platform\Bundles\HIBundle;

final class PaletteDomainBundle extends HIBundle implements FrontendBundleInjectable
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            PalettesFrontendScript::class,
        ];
    }
}