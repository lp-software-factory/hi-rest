<?php
namespace HappyInvite\Domain\Bundles\Palette\Frontend;

use HappyInvite\Domain\Bundles\Frontend\FrontendScript;
use HappyInvite\Domain\Bundles\Palette\Entity\Palette;
use HappyInvite\Domain\Bundles\Palette\Service\PaletteService;

final class PalettesFrontendScript implements FrontendScript
{
    /** @var PaletteService */
    private $paletteService;

    public function __construct(PaletteService $paletteService)
    {
        $this->paletteService = $paletteService;
    }

    public function tags(): array
    {
        return [
            self::TAG_GLOBAL,
        ];
    }

    public function __invoke(): array
    {
        return [
            'palettes' => array_map(function(Palette $palette) {
                return $palette->toJSON();
            }, $this->paletteService->getPalettes())
        ];
    }
}