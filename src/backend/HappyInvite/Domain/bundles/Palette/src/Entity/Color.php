<?php
namespace HappyInvite\Domain\Bundles\Palette\Entity;

use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

class Color implements JSONSerializable, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use VersionEntityTrait;

    private const REGEX_COLOR = '/\#[0-9abcdefABCDEF]{6}/';

    /** @var string */
    private $code;

    /** @var string */
    private $hexCode;

    public function __construct(string $code, string $hexCode)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        if (!preg_match(self::REGEX_COLOR, $hexCode)) {
            throw new \InvalidArgumentException('Invalid hexCode');
        }

        if (strlen($code) < 3) {
            throw new \Exception('Invalid code');
        }

        $this->code = $code;
        $this->hexCode = $hexCode;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'code' => $this->code,
            'hexCode' => $this->hexCode
        ];
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getHexCode(): string
    {
        return $this->hexCode;
    }

    public function getName():string
    {
        preg_match('#([a-z-]*)\.#i', $this->code, $m);
        return $m[1];
    }
}