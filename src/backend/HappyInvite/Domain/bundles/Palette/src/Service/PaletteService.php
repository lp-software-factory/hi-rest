<?php
namespace HappyInvite\Domain\Bundles\Palette\Service;

use HappyInvite\Domain\Bundles\Palette\Entity\Palette;
use HappyInvite\Domain\Bundles\Palette\Repository\ColorsRepository;
use HappyInvite\Domain\Bundles\Palette\Repository\PaletteRepository;

class PaletteService
{
    /** @var \HappyInvite\Domain\Bundles\Palette\Repository\ColorsRepository */
    private $colorRepository;

    /** @var \HappyInvite\Domain\Bundles\Palette\Repository\PaletteRepository */
    private $paletteRepository;

    public function __construct(ColorsRepository $colorRepository, PaletteRepository $paletteRepository)
    {
        $this->colorRepository = $colorRepository;
        $this->paletteRepository = $paletteRepository;
    }

    public function getColors(): array
    {
        return $this->colorRepository->getColors();
    }

    public function getPalettes(): array
    {
        return $this->paletteRepository->getPalettes();
    }

    public function getRandomPalette(): Palette
    {
        $palettes = $this->paletteRepository->getPalettes();

        return $palettes[array_rand($palettes)];
    }
}