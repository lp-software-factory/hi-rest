<?php
namespace HappyInvite\Domain\Bundles\Translation;

use HappyInvite\Platform\Bundles\HIBundle;

final class TranslationDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}