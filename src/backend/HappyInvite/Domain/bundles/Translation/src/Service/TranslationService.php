<?php
namespace HappyInvite\Domain\Bundles\Translation\Service;

use Cocur\Chain\Chain;
use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Translation\Entity\Translation;
use HappyInvite\Domain\Bundles\Translation\Entity\TranslationByRegion;
use HappyInvite\Domain\Bundles\Translation\Parameters\ClearTranslationParameters;
use HappyInvite\Domain\Bundles\Translation\Parameters\SetTranslationParameters;
use HappyInvite\Domain\Bundles\Translation\Reader\TranslationSourceReaderFactory;
use HappyInvite\Domain\Bundles\Translation\Repository\TranslationProjectRepository;
use HappyInvite\Domain\Bundles\Translation\Repository\TranslationRepository;

final class TranslationService
{
    public const EVENT_CREATED = 'hi.domain.translation.entity.created';
    public const EVENT_UPSERTED = 'hi.domain.translation.entity.upserted';
    public const EVENT_DELETE = 'hi.domain.translation.entity.delete.before';
    public const EVENT_DELETED = 'hi.domain.translation.entity.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var AuthToken */
    private $authToken;

    /** @var TranslationRepository */
    private $entityRepository;

    /** @var TranslationProjectRepository */
    private $translationProjectRepository;

    /** @var TranslationSourceReaderFactory */
    private $translationSourceReaderFactory;

    public function __construct(
        AuthToken $authToken,
        TranslationRepository $translationRepository,
        TranslationProjectRepository $translationProjectRepository,
        TranslationSourceReaderFactory $translationSourceReaderFactory
    ) {
        $this->authToken = $authToken;
        $this->entityRepository = $translationRepository;
        $this->translationProjectRepository = $translationProjectRepository;
        $this->translationSourceReaderFactory = $translationSourceReaderFactory;
        $this->eventEmitter = new EventEmitter();
    }

    public function upsert(SetTranslationParameters $parameters): Translation
    {
        if ($this->hasTranslation($parameters->getProjectId(), $parameters->getKey())) {
            $translation = $this->getTranslationByKey($parameters->getProjectId(), $parameters->getKey());
            $translation->setValue($parameters->getValue());

            $this->entityRepository->saveEntity($translation);

            $this->getEventEmitter()->emit(self::EVENT_UPSERTED, [$translation]);

            return $translation;
        } else {
            $translation = new Translation(
                $parameters->getProjectId(),
                $parameters->getKey(),
                $parameters->getValue()
            );

            $this->entityRepository->createEntity($translation);

            $this->getEventEmitter()->emit(self::EVENT_CREATED, [$translation]);
            $this->getEventEmitter()->emit(self::EVENT_UPSERTED, [$translation]);

            return $translation;
        }
    }

    public function clear(ClearTranslationParameters $parameters): void
    {
        if($this->hasTranslation($parameters->getProjectId(), $parameters->getKey())) {
            $translation = $this->getTranslationByKey($parameters->getProjectId(), $parameters->getKey());
            $translationId = $translation->getId();

            $this->getEventEmitter()->emit(self::EVENT_DELETE, [$translation, $translationId]);
            $this->entityRepository->deleteEntity($translation);
            $this->getEventEmitter()->emit(self::EVENT_DELETED, [$translation, $translationId]);
        }
    }

    public function listTranslationsByRegion(int $projectId, string $region): array
    {
        return array_values(Chain::create($this->listAllTranslations($projectId))
            ->filter(function(Translation $translation) use ($region) {
                return $translation->getValue()->hasTranslation($region);
            })
            ->map(function(Translation $translation) use ($region) {
                return new TranslationByRegion(
                    $translation->getKey(),
                    $translation->getValue()->getTranslation($region)
                );
            })
            ->array);
    }

    public function listAllTranslations(int $projectId): array
    {
        $local = $this->readLocalTranslations($projectId);
        $db = $this->readDBTranslations($projectId);

        $map = [];
        $pull = [];

        foreach($db as $entity) {
            $pull[] = $entity;
        }

        /** @var Translation $entity */
        foreach($local as $entity) {
            $pull[] = $entity;
        }

        /** @var Translation $entity */
        foreach($pull as $entity) {
            if(isset($map[$entity->getKey()])) {
                /** @var Translation $mergeEntity */
                $mergeEntity = $map[$entity->getKey()];
                $translations = [];

                foreach($mergeEntity->getValue()->getDefinition() as $definition) {
                    $translations[$definition['region']] = $definition;
                }

                foreach($entity->getValue()->getDefinition() as $definition) {
                    $translations[$definition['region']] = $definition;
                }

                $mergeEntity->setValue(new ImmutableLocalizedString($translations));
            }else{
                $map[$entity->getKey()] = $entity;
            }
        }

        return array_values($map);
    }

    public function readDBTranslations(int $projectId): array
    {
        return $this->entityRepository->getAllByProject($projectId);
    }

    public function readLocalTranslations(int $projectId): array
    {
        $project = $this->translationProjectRepository->getProjectById($projectId);
        $results = [];

        foreach($project->getSources() as $source) {
            $reader = $this->translationSourceReaderFactory->createTranslationSourceReader($source->getType());

            foreach($source->getDirs() as $dir) {
                $results = array_merge($results, $reader->read($dir, $project));
            }
        }

        return $results;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function getById(int $entityId): Translation
    {
        return $this->entityRepository->getById($entityId);
    }

    public function hasTranslation(int $projectId, string $key): bool
    {
        return $this->entityRepository->hasEntity($projectId, $key);
    }

    public function getTranslationByKey(int $projectId, string $key): Translation
    {
        return $this->entityRepository->getEntityByKey($projectId, $key);
    }
}