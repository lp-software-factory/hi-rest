<?php
namespace HappyInvite\Domain\Bundles\Translation\Repository;

use HappyInvite\Domain\Bundles\Translation\Entity\TranslationProject;
use HappyInvite\Domain\Bundles\Translation\Entity\TranslationProjectSource;

final class TranslationProjectRepository
{
    public const PROJECT_ID_TEST = -1;

    public const PROJECT_ID_HAPPY_INVITE_REST_API = 1;
    public const PROJECT_ID_HAPPY_INVITE_MAIN_FRONTEND = 2;
    public const PROJECT_ID_HAPPY_INVITE_ADMIN_FRONTEND = 3;

    /** @var TranslationProject[] */
    private $projects = [];

    public function __construct(array $projects)
    {
        $this->projects = array_map(function(array $input) {
            return new TranslationProject(
                $input['id'],
                $input['title'],
                $input['position'],
                array_map(function(array $source) {
                    return new TranslationProjectSource($source['type'], $source['dirs']);
                }, $input['sources'])
            );
        }, $projects);
    }

    public function listProjects(): array
    {
        return $this->projects;
    }

    public function getProjectById(int $projectId): TranslationProject
    {
        $result = array_values(array_filter($this->listProjects(), function(TranslationProject $input) use ($projectId) {
            return $input->getId() === $projectId;
        }));

        if(count($result) !== 1) {
            throw new \Exception(sprintf('Translation project with ID `%d` does not exists or has any duplicates', $projectId));
        }

        return $result[0];
    }
}