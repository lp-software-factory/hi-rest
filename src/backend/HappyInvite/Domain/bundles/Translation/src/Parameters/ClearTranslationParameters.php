<?php
namespace HappyInvite\Domain\Bundles\Translation\Parameters;

final class ClearTranslationParameters
{
    /** @var int */
    private $projectId;

    /** @var string */
    private $key;

    public function __construct($projectId, $key)
    {
        $this->projectId = $projectId;
        $this->key = $key;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function getKey(): string
    {
        return $this->key;
    }
}