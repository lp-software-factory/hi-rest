<?php
namespace HappyInvite\Domain\Bundles\Translation\Entity;

final class TranslationProjectSource
{
    public const TYPE_JSON = 'json';
    public const TYPE_PHP = 'php';

    /** @var string */
    private $type;

    /** @var string[] */
    private $dirs;

    public function __construct(string $type, array $dirs)
    {
        $this->type = $type;
        $this->dirs = $dirs;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDirs(): array
    {
        return $this->dirs;
    }
}