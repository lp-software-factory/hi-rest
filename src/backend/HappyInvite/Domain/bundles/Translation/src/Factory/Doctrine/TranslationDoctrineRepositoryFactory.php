<?php
namespace HappyInvite\Domain\Bundles\Translation\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Translation\Entity\Translation;

final class TranslationDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Translation::class;
    }
}