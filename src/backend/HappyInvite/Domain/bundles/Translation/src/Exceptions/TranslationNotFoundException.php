<?php
namespace HappyInvite\Domain\Bundles\Translation\Exceptions;

final class TranslationNotFoundException extends \Exception {}