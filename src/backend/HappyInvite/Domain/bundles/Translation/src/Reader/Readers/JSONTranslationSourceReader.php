<?php
namespace HappyInvite\Domain\Bundles\Translation\Reader\Readers;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Translation\Entity\Translation;
use HappyInvite\Domain\Bundles\Translation\Entity\TranslationProject;
use HappyInvite\Domain\Bundles\Translation\Entity\TranslationProjectSource;
use HappyInvite\Domain\Bundles\Translation\Reader\TranslationSourceReader;

final class JSONTranslationSourceReader implements TranslationSourceReader
{
    public function read(string $region, TranslationProject $project): array
    {
        $results = [];
        $translations = [];

        /** @var TranslationProjectSource $source */
        foreach($project->getSources() as $source) {
            foreach($source->getDirs() as $dir) {
                foreach(glob(sprintf('%s/*.json', $dir)) as $current) {
                    $pi = pathinfo($current);

                    $this->readSubTree(json_decode(file_get_contents($current), true), $pi['filename'], $results);
                }
            }
        }

        foreach($results as $key => $value) {
            $translations[] = new Translation(
                $project->getId(),
                $key,
                new ImmutableLocalizedString($value)
            );
        }

        return $translations;
    }

    private function readSubTree(array $input, string $region, &$results, array $carry = []) {
        foreach($input as $key => $value) {
            $next = array_values($carry);
            $next[] = $key;

            if(is_array($value)) {
                $this->readSubTree($value, $region, $results, $next);
            }else if(is_string($value)) {
                $key = implode('.', $next);

                if(! isset($results[$key])) {
                    $results[$key] = [];
                }

                $results[$key][] = [
                    'region' => $region,
                    'value' => $value,
                ];
            }
        }
    }
}