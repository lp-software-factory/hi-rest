<?php
namespace HappyInvite\Domain\Bundles\Translation\Reader;

use HappyInvite\Domain\Bundles\Translation\Entity\TranslationProject;

interface TranslationSourceReader
{
    public function read(string $region, TranslationProject $project): array;
}