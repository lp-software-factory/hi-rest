<?php
namespace HappyInvite\Domain\Bundles\Translation;

use HappyInvite\Domain\Bundles\Translation\Repository\TranslationProjectRepository;

return [
    'hi.config.domain.translation.projects' => [
        [
            'id' => TranslationProjectRepository::PROJECT_ID_TEST,
            'title' => 'HappyInvite Test',
            'position' => -1,
            'sources' => [
                [
                    'type' => 'json',
                    'dirs' => [
                        __DIR__.'/../../../../REST/bundles/Translation/src/Tests/Resources/json',
                    ],
                ],
                [
                    'type' => 'php',
                    'dirs' => [
                        __DIR__.'/../../../../REST/bundles/Translation/src/Tests/Resources/php',
                    ],
                ],
            ],
        ],
        [
            'id' => TranslationProjectRepository::PROJECT_ID_HAPPY_INVITE_MAIN_FRONTEND,
            'title' => 'HappyInvite Main Frontend&Twig',
            'position' => 1,
            'sources' => [
                [
                    'type' => 'json',
                    'dirs' => [
                        __DIR__.'/../../../../../../frontend/src/app/translations/bootstrap'
                    ],
                ],

            ],
        ],
        [
            'id' => TranslationProjectRepository::PROJECT_ID_HAPPY_INVITE_REST_API,
            'title' => 'HappyInvite REST API',
            'position' => 2,
            'sources' => [
                [
                    'type' => 'php',
                    'dirs' => [
                        __DIR__.'/../../../resources/translations'
                    ],
                ],

            ],
        ],
        [
            'id' => TranslationProjectRepository::PROJECT_ID_HAPPY_INVITE_ADMIN_FRONTEND,
            'title' => 'HappyInvite Admin Frontend',
            'position' => 3,
            'sources' => [
                [
                    'type' => 'json',
                    'dirs' => [
                        __DIR__.'/../../../../../../../../hi-admin/src/frontend/src/translations'
                    ],
                ],

            ],
        ],
    ]
];