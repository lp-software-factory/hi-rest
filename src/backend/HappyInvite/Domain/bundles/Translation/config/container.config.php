<?php
namespace HappyInvite\Domain\Bundles\Translation;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Translation\Factory\Doctrine\TranslationDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Translation\Repository\TranslationProjectRepository;
use HappyInvite\Domain\Bundles\Translation\Repository\TranslationRepository;

return [
    TranslationRepository::class => factory(TranslationDoctrineRepositoryFactory::class),
    TranslationProjectRepository::class => object()
        ->constructorParameter('projects', get('hi.config.domain.translation.projects')),
];