<?php
namespace HappyInvite\Domain\Bundles\Frontend;

interface FrontendScript
{
    const TAG_GLOBAL = 'global';
    const TAG_IS_AUTHENTICATED = 'is-authenticated';

    public function tags(): array;
    public function __invoke(): array;
}