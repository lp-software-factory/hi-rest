<?php
namespace HappyInvite\Domain\Bundles\Frontend;

interface FrontendBundleInjectable
{
    public function getFrontendScripts(): array;
}