<?php
namespace HappyInvite\Domain\Bundles\Frontend\Service;

use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\Platform\Service\BundlesService;
use HappyInvite\Domain\Bundles\Frontend\FrontendBundleInjectable;
use HappyInvite\Domain\Bundles\Frontend\FrontendScript;
use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService\Filter;
use Cocur\Chain\Chain;
use DI\Container;

class HasExporterWithSameIdException extends \Exception {}

class FrontendService
{
    /** @var Container */
    private $container;
    
    /** @var BundlesService */
    private $bundlesService;

    /** @var array */
    private $exported = [];

    public function __construct(
        Container $container,
        BundlesService $bundlesService)
    {
        $this->container = $container;
        $this->bundlesService = $bundlesService;
    }

    public function export(string $key, $data): self
    {
        $this->exported[$key] = $data;

        return $this;
    }

    public function fetch(Filter $filter = null): array {
        if(! $filter) {
            $filter = new FrontendService\NoneFilter();
        }

        $result = [];
        $scripts = Chain::create($this->bundlesService->listBundles())
            ->filter(function(Bundle $bundle) {
                return $bundle instanceof FrontendBundleInjectable;
            })
            ->map(function(FrontendBundleInjectable $bundle) {
                return $bundle->getFrontendScripts();
            })
            ->reduce(function(array $carry, array $scripts) {
                return array_merge($carry, $scripts);
            }, [])
        ;

        $scripts = array_map(function(string $script) {
            return $this->container->get($script);
        }, $scripts);

        foreach($filter->filter($scripts) as $script) {
            if($script instanceof FrontendScript) {
                $result = array_merge_recursive($result, $script());
            }else{
                throw new \Exception;
            }
        }

        $result['exports'] = $this->exported;

        return $result;
    }
}