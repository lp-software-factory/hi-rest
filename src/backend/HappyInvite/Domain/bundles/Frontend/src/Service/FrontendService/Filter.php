<?php
namespace HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;

interface Filter
{
    public function filter(array $scripts): array;
}