<?php
namespace HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;

class NoneFilter implements Filter
{
    public function filter(array $scripts): array {
        return $scripts;
    }
}