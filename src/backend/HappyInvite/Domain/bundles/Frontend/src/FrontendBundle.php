<?php
namespace HappyInvite\Domain\Bundles\Frontend;

use HappyInvite\Domain\Bundles\Locale\Frontend\LocaleFrontendScript;
use HappyInvite\Platform\Bundles\HIBundle;

class FrontendBundle extends HIBundle implements FrontendBundleInjectable
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            LocaleFrontendScript::class
        ];
    }
}