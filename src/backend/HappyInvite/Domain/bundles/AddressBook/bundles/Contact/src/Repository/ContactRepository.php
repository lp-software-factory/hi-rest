<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions\ContactNotFoundException;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class ContactRepository extends EntityRepository
{
    public function createEntity(Contact $entity): int
    {
        while ($this->hasWithSID($entity->getSID())) {
            $entity->regenerateSID();
        }

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        return $entity->getId();
    }

    public function saveEntity(Contact $entity)
    {
        $this->getEntityManager()->flush($entity);
    }

    public function saveEntityEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveEntity($entity);
        }
    }

    public function deleteEntity(Contact $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function clearGroups(Contact $entity)
    {
        $entity->clearGroups();

        $this->getEntityManager()->flush($entity);
    }

    public function listEntities(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAllByOwner(Profile $owner): array
    {
        return $this->findBy([
            'owner' => $owner,
        ], ['id' => 'asc']);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): Contact
    {
        $result = $this->find($id);

        if ($result instanceof Contact) {
            return $result;
        } else {
            throw new ContactNotFoundException(sprintf('Contact `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Contacts with IDs (%s) not found', array_diff($ids, array_map(function (Contact $entity) {
                return $entity->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Contact
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof Contact) {
            return $result;
        } else {
            throw new ContactNotFoundException(sprintf('Contact `SID(%s)` not found', $sid));
        }
    }
}