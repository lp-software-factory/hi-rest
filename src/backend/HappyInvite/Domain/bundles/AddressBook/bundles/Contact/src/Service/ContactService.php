<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Parameters\EditContactParameters;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository\ContactGroupEQRepository;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions\ContactIsNotYoursException;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Parameters\CreateContactParameters;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository\ContactRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class ContactService
{
    public const EVENT_CREATED = 'hi.domain.event-recipients-group.entity.created';
    public const EVENT_EDITED = 'hi.domain.event-recipients-group.entity.edited';
    public const EVENT_UPDATED = 'hi.domain.event-recipients-group.entity.updated';
    public const EVENT_DELETE = 'hi.domain.event-recipients-group.entity.delete.before';
    public const EVENT_DELETED = 'hi.domain.event-recipients-group.entity.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var AuthToken */
    private $authToken;

    /** @var ContactRepository */
    private $entityRepository;

    /** @var ContactGroupEQRepository */
    private $groupEQRepository;

    public function __construct(
        AuthToken $authToken,
        ContactRepository $contactRepository,
        ContactGroupEQRepository $groupEQRepository
    ) {
        $this->authToken = $authToken;
        $this->entityRepository = $contactRepository;
        $this->groupEQRepository = $groupEQRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    private function validateOwner(Profile $owner): void
    {
        if(! $this->authToken->isSignedIn()) {
            throw new ContactIsNotYoursException("You're not signed in.");
        }


        if(! ($this->authToken->getAccount()->hasAccess('admin'))  && ! ($this->authToken->getProfile()->getId() === $owner->getId())) {
            throw new ContactIsNotYoursException("You're not an owner of Contact");
        }
    }

    public function create(CreateContactParameters $parameters): Contact
    {
        $entity = new Contact(
            $parameters->getOwner(),
            $parameters->getFirstName(),
            $parameters->getLastName(),
            $parameters->getMiddleName(),
            $parameters->getEmail(),
            $parameters->getPhone()
        );

        $entity->setGroups($parameters->getGroups());

        $this->entityRepository->createEntity($entity);

        $this->eventEmitter->emit(self::EVENT_CREATED, [$entity]);

        return $entity;
    }

    public function edit(int $entityId, EditContactParameters $parameters): Contact
    {
        $entity = $this->getById($entityId);

        $this->validateOwner($entity->getOwner());

        $this->groupEQRepository->clearContactGroups($entity);

        $entity
            ->setName(
                $parameters->getFirstName(),
                $parameters->getLastName(),
                $parameters->getMiddleName()
            )
            ->setEmail($parameters->getEmail())
            ->setPhone($parameters->getPhone())
            ->setGroups($parameters->getGroups());

        $this->entityRepository->saveEntity($entity);
        $this->eventEmitter->emit(self::EVENT_EDITED, [$entity, $entityId]);

        return $entity;
    }

    public function listByOwner(Profile $owner): Contact
    {
        $this->validateOwner($owner);

        return $this->entityRepository->getAllByOwner($owner);
    }

    public function getByOwnerAndId(int $entityId): Contact
    {
        $entity = $this->getById($entityId);

        $this->validateOwner($entity->getOwner());

        return $entity;
    }

    public function delete(int $entityId): void
    {
        $entity = $this->getById($entityId);

        $this->validateOwner($entity->getOwner());

        $this->eventEmitter->emit(self::EVENT_DELETE, [$entity]);
        $this->entityRepository->deleteEntity($entity);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$entityId, $entity]);
    }

    public function getById(int $entityId): Contact
    {
        return $this->entityRepository->getById($entityId);
    }

    public function getAllByOwner(Profile $owner): array
    {
        return $this->entityRepository->getAllByOwner($owner);
    }
}