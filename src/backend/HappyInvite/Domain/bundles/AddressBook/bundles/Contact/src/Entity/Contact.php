<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions\ContactShouldHaveEmailException;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions\ContactShouldHaveNameException;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository\ContactRepository")
 * @Table(name="address_book_contact")
 */
class Contact implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="owner_profile_id", referencedColumnName="id")
     * @var Profile
     */
    private $owner;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\ContactGroupsEQ", mappedBy="contact", cascade={"all"})
     * @var Collection
     */
    private $groups;

    /**
     * @Column(name="first_name", type="string")
     * @var string
     */
    private $firstName;

    /**
     * @Column(name="last_name", type="string")
     * @var string
     */
    private $lastName;

    /**
     * @Column(name="middle_name", type="string")
     * @var string
     */
    private $middleName;

    /**
     * @Column(name="email", type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(name="phone", type="string")
     * @var string
     */
    private $phone;

    public function __construct(
        Profile $owner, 
        string $firstName, 
        string $lastName, 
        string $middleName, 
        string $email, 
        string $phone
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);
        
        $this->owner = $owner;
        $this->phone = $phone;
        $this->groups = new ArrayCollection();

        $this->setName($firstName, $lastName, $middleName);
        $this->setEmail($email);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'owner_profile_id' => $this->getOwner()->getId(),
            'group_ids' => $this->groups->map(function(ContactGroupsEQ $eq) {
                return $eq->getGroup()->getId();
            })->toArray(),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'middle_name' => $this->getMiddleName(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getOwner(): Profile
    {
        return $this->owner;
    }

        public function clearGroups(): void
    {
        $this->groups->clear();
    }

    public function getGroups(): array
    {
        return $this->groups->map(function(ContactGroupsEQ $eq) {
            return $eq->getGroup();
        })->toArray();
    }

    public function getEQGroups(): array
    {
        return $this->groups->toArray();
    }

    public function setGroups(array $groups): self
    {
        if($this->groups->count()) {
            throw new \Exception('Collection is not empty.');
        }

        array_map(function(EventRecipientsGroup $group) {
            $this->groups->add(new ContactGroupsEQ($this, $group));
        }, $groups);

        $this->markAsUpdated();

        return $this;
    }

    public function setName(string $firstName, string $lastName, string $middleName): self
    {
        $firstName = trim($firstName);
        $lastName = trim($lastName);
        $middleName = trim($middleName);

        foreach([$firstName, $lastName, $middleName] as &$input) {
            $input = mb_ereg_replace("\n", null, $input);
            $input = mb_ereg_replace("\r", null, $input);
        }

        $namesLength = mb_strlen($firstName)
            + mb_strlen($lastName)
            + mb_strlen($middleName);

        if($namesLength === 0) {
            throw new ContactShouldHaveNameException;
        }

        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;

        $this->markAsUpdated();

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        if(! strlen($email)) {
            throw new ContactShouldHaveEmailException;
        }

        $this->email = $email;
        $this->markAsUpdated();

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        $this->markAsUpdated();

        return $this;
    }
}