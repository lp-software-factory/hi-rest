<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Factory\Doctrine\ContactDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Factory\Doctrine\ContactGroupEQDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository\ContactGroupEQRepository;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository\ContactRepository;

return [
    ContactRepository::class => factory(ContactDoctrineRepositoryFactory::class),
    ContactGroupEQRepository::class => factory(ContactGroupEQDoctrineRepositoryFactory::class),
];