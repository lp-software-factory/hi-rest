<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Exceptions;

final class ContactShouldHaveEmailException extends \Exception {}