<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact;

final class ContactGroupEQRepository extends EntityRepository
{
    public function clearContactGroups(Contact $contact)
    {
        $groups = $contact->getEQGroups();

        foreach($groups as $group) {
            $this->getEntityManager()->remove($group);
        }

        $this->getEntityManager()->flush($groups);

        $contact->clearGroups();
    }
}