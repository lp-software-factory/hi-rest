<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact;

final class ContactDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Contact::class;
    }
}