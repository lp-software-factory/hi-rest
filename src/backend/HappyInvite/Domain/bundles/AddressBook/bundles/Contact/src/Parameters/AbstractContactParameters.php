<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Parameters;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

abstract class AbstractContactParameters
{
    /** @var Profile */
    private $owner;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $middleName;

    /** @var string */
    private $email;

    /** @var string */
    private $phone;

    /** @var EventRecipientsGroup[] */
    private $groups;

    public function __construct(
        Profile $owner,
        string $firstName,
        string $lastName,
        string $middleName,
        string $email,
        string $phone,
        array $groups
    ) {
        $this->owner = $owner;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
        $this->email = $email;
        $this->phone = $phone;
        $this->groups = $groups;
    }

    public function getOwner(): Profile
    {
        return $this->owner;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }
}