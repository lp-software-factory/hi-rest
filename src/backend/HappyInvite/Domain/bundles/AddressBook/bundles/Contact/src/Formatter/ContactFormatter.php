<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Formatter;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact;

final class ContactFormatter
{
    public function formatOne(Contact $entity): array
    {
        return [
            'entity' => $entity->toJSON(),
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(Contact $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}