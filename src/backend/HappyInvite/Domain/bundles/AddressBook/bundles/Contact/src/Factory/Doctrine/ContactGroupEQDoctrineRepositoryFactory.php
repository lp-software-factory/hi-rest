<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Factory\Doctrine;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\ContactGroupsEQ;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class ContactGroupEQDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return ContactGroupsEQ::class;
    }
}