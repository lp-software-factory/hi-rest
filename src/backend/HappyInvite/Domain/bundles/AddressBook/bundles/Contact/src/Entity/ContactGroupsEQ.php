<?php
namespace HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity;

use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Repository\ContactGroupEQRepository")
 * @Table(name="address_book_contact_group_eq")
 */
class ContactGroupsEQ implements IdEntity
{
    use IdEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\Entity\Contact")
     * @JoinColumn(name="contact_id", referencedColumnName="id")
     * @var Contact
     */
    private $contact;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Entity\EventRecipientsGroup")
     * @JoinColumn(name="contact_group_id", referencedColumnName="id")
     * @var EventRecipientsGroup
     */
    private $group;

    /**
     * ContactGroupsEQ constructor.
     * @param Contact $contact
     * @param EventRecipientsGroup $group
     */
    public function __construct(Contact $contact, EventRecipientsGroup $group)
    {
        $this->contact = $contact;
        $this->group = $group;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function getGroup(): EventRecipientsGroup
    {
        return $this->group;
    }
}