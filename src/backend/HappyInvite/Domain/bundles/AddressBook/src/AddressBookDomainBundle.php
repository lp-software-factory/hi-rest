<?php
namespace HappyInvite\Domain\Bundles\AddressBook;

use HappyInvite\Domain\Bundles\AddressBook\Bundles\Contact\ContactDomainBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class AddressBookDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new ContactDomainBundle(),
        ];
    }
}