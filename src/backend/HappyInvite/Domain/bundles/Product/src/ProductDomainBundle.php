<?php
namespace HappyInvite\Domain\Bundles\Product;

use HappyInvite\Platform\Bundles\HIBundle;

final class ProductDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}