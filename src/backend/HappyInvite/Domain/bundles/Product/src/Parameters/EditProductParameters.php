<?php
namespace HappyInvite\Domain\Bundles\Product\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;

final class EditProductParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var bool */
    private $isActivated;

    /** @var string */
    private $basePriceCurrency;

    /** @var int */
    private $basePriceAmount;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        bool $isActivated,
        string $basePriceCurrency,
        int $basePriceAmount
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->isActivated = $isActivated;
        $this->basePriceCurrency = $basePriceCurrency;
        $this->basePriceAmount = $basePriceAmount;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function isActivated(): bool
    {
        return $this->isActivated;
    }

    public function getBasePriceCurrency(): string
    {
        return $this->basePriceCurrency;
    }

    public function getBasePriceAmount(): int
    {
        return $this->basePriceAmount;
    }
}