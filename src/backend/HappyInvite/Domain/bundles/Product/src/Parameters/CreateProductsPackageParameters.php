<?php
namespace HappyInvite\Domain\Bundles\Product\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Palette\Entity\Palette;

final class CreateProductsPackageParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var Palette */
    private $palette;

    /** @var string */
    private $basePriceCurrency;

    /** @var int */
    private $basePriceAmount;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Palette $palette,
        string $basePriceCurrency,
        int $basePriceAmount
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->palette = $palette;
        $this->basePriceCurrency = $basePriceCurrency;
        $this->basePriceAmount = $basePriceAmount;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getPalette(): Palette
    {
        return $this->palette;
    }

    public function getBasePriceCurrency(): string
    {
        return $this->basePriceCurrency;
    }

    public function getBasePriceAmount(): int
    {
        return $this->basePriceAmount;
    }
}