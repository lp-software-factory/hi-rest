<?php
namespace HappyInvite\Domain\Bundles\Product\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;

final class CreateProductParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var string */
    private $basePriceCurrency;

    /** @var int */
    private $basePriceAmount;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        string $basePriceCurrency,
        int $basePriceAmount
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->basePriceCurrency = $basePriceCurrency;
        $this->basePriceAmount = $basePriceAmount;
    }


    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getBasePriceCurrency(): string
    {
        return $this->basePriceCurrency;
    }

    public function getBasePriceAmount(): int
    {
        return $this->basePriceAmount;
    }
}