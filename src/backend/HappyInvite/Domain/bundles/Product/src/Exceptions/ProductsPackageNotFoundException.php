<?php
namespace HappyInvite\Domain\Bundles\Product\Exceptions;

final class ProductsPackageNotFoundException extends \Exception {}