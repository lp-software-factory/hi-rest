<?php
namespace HappyInvite\Domain\Bundles\Product\Exceptions;

final class ProductNotFoundException extends \Exception {}