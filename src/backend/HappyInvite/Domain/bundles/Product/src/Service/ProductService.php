<?php
namespace HappyInvite\Domain\Bundles\Product\Service;

use HappyInvite\Domain\Bundles\Money\Service\CurrencyService;
use HappyInvite\Domain\Bundles\Money\Service\PriceService;
use HappyInvite\Domain\Bundles\Product\Entity\Product;
use HappyInvite\Domain\Bundles\Product\Parameters\CreateProductParameters;
use HappyInvite\Domain\Bundles\Product\Parameters\EditProductParameters;
use HappyInvite\Domain\Bundles\Product\Repository\ProductRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class ProductService
{
    /** @var ProductRepository */
    private $productRepository;

    /** @var PriceService */
    private $priceService;

    public function __construct(
        ProductRepository $productRepository,
        PriceService $priceService
    ) {
        $this->productRepository = $productRepository;
        $this->priceService = $priceService;
    }

    public function createProduct(CreateProductParameters $parameters): Product
    {
        $product = new Product(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $this->priceService->createPrice(
                $parameters->getBasePriceCurrency(),
                $parameters->getBasePriceAmount()
            )
        );

        $this->productRepository->createProduct($product);

        return $product;
    }

    public function editProduct(int $productId, EditProductParameters $parameters): Product
    {
        $product = $this->productRepository->getById($productId);
        $product->setTitle($parameters->getTitle());
        $product->setDescription($parameters->getDescription());

        if(! $product->getBasePrice()->same($parameters->getBasePriceCurrency(), $parameters->getBasePriceAmount())) {
            $product->setBasePrice(
                $this->priceService->createPrice(
                    $parameters->getBasePriceCurrency(),
                    $parameters->getBasePriceAmount()
                ));
        }

        if($product->isActivated() !== $parameters->isActivated()) {
            $parameters->isActivated()
                ? $product->activate()
                : $product->deactivate();
        }

        $this->productRepository->saveProduct($product);

        return $product;
    }

    public function deleteProduct(int $productId)
    {
        $this->productRepository->deleteProduct(
            $this->getById($productId)
        );
    }
    
    public function getById(int $productId): Product
    {
        return $this->productRepository->getById($productId);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->productRepository->getByIds($idsCriteria);
    }

    public function getAll(): array
    {
        return $this->productRepository->getAll();
    }
}