<?php
namespace HappyInvite\Domain\Bundles\Product\Service;

use HappyInvite\Domain\Bundles\Money\Service\PriceService;
use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage;
use HappyInvite\Domain\Bundles\Product\Parameters\CreateProductsPackageParameters;
use HappyInvite\Domain\Bundles\Product\Parameters\EditProductsPackageParameters;
use HappyInvite\Domain\Bundles\Product\Repository\ProductsPackageRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class ProductsPackageService
{
    /** @var ProductsPackageRepository */
    private $repository;

    /** @var PriceService */
    private $priceService;

    /** @var ProductService */
    private $productService;

    public function __construct(
        ProductsPackageRepository $repository,
        ProductService $productService,
        PriceService $priceService
    ) {
        $this->repository = $repository;
        $this->productService = $productService;
        $this->priceService = $priceService;
    }

    public function createProductsPackage(CreateProductsPackageParameters $parameters): ProductsPackage
    {
        $productsPackage = new ProductsPackage(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getPalette(),
            $this->priceService->createPrice(
                $parameters->getBasePriceCurrency(),
                $parameters->getBasePriceAmount()
            )
        );

        $this->repository->createProductsPackage($productsPackage);

        return $productsPackage;
    }

    public function editProductsPackage(int $productsPackageId, EditProductsPackageParameters $parameters): ProductsPackage
    {
        $productsPackage = $this->repository->getById($productsPackageId);
        $productsPackage
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setPalette($parameters->getPalette())
            ->setBasePrice($this->priceService->createPrice(
                $parameters->getBasePriceCurrency(),
                $parameters->getBasePriceAmount()
            ))
        ;

        if($productsPackage->isActivated() !== $parameters->isActivated()) {
            $parameters->isActivated()
                ? $productsPackage->activate()
                : $productsPackage->deactivate();
        }

        $this->repository->saveProductsPackage($productsPackage);

        return $productsPackage;
    }

    public function deleteProductsPackage(int $productsPackageId)
    {
        $this->repository->deleteProductsPackage(
            $this->getById($productsPackageId)
        );
    }

    public function clearProducts(int $productsPackageId): ProductsPackage
    {
        $package = $this->getById($productsPackageId);
        $package->clearProducts();

        $this->repository->saveProductsPackage($package);

        return $package;
    }

    public function setProducts(int $productsPackageId, array $productIds): ProductsPackage
    {
        $this->clearProducts($productsPackageId);

        $package = $this->getById($productsPackageId);
        $package->setProducts(array_map(function(int $productId) {
            return $this->productService->getById($productId);
        }, $productIds));

        $this->repository->saveProductsPackage($package);

        return $package;
    }

    public function getById(int $productsPackageId): ProductsPackage
    {
        return $this->repository->getById($productsPackageId);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }
}