<?php
namespace HappyInvite\Domain\Bundles\Product\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage;
use HappyInvite\Domain\Bundles\Product\Exceptions\ProductsPackageNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class ProductsPackageRepository extends EntityRepository
{
    public function createProductsPackage(ProductsPackage $productsPackage): int
    {
        while($this->hasWithSID($productsPackage->getSID())) {
            $productsPackage->regenerateSID();
        }

        $this->getEntityManager()->persist($productsPackage);
        $this->getEntityManager()->flush($productsPackage);

        return $productsPackage->getId();
    }

    public function saveProductsPackage(ProductsPackage $productsPackage)
    {
        $this->getEntityManager()->flush($productsPackage);
    }

    public function saveProductsPackageEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveProductsPackage($entity);
        }
    }

    public function deleteProductsPackage(ProductsPackage $productsPackage)
    {
        $this->getEntityManager()->remove($productsPackage);
        $this->getEntityManager()->flush($productsPackage);
    }

    public function listProductsPackages(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): ProductsPackage
    {
        $result = $this->find($id);

        if($result instanceof ProductsPackage) {
            return $result;
        }else{
            throw new ProductsPackageNotFoundException(sprintf('ProductsPackage `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('ProductsPackages with IDs (%s) not found', array_diff($ids, array_map(function(ProductsPackage $productsPackage) {
                return $productsPackage->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): ProductsPackage
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof ProductsPackage) {
            return $result;
        }else{
            throw new ProductsPackageNotFoundException(sprintf('ProductsPackage `SID(%s)` not found', $sid));
        }
    }
}