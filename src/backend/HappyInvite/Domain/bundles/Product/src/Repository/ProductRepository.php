<?php
namespace HappyInvite\Domain\Bundles\Product\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Product\Entity\Product;
use HappyInvite\Domain\Bundles\Product\Exceptions\ProductNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class ProductRepository extends EntityRepository
{
    public function createProduct(Product $product): int
    {
        while($this->hasWithSID($product->getSID())) {
            $product->regenerateSID();
        }

        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush($product);

        return $product->getId();
    }

    public function saveProduct(Product $product)
    {
        $this->getEntityManager()->flush($product);
    }

    public function saveProductEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveProduct($entity);
        }
    }

    public function deleteProduct(Product $product)
    {
        $this->getEntityManager()->remove($product);
        $this->getEntityManager()->flush($product);
    }

    public function listProducts(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): Product
    {
        $result = $this->find($id);

        if($result instanceof Product) {
            return $result;
        }else{
            throw new ProductNotFoundException(sprintf('Product `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Products with IDs (%s) not found', array_diff($ids, array_map(function(Product $product) {
                return $product->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Product
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof Product) {
            return $result;
        }else{
            throw new ProductNotFoundException(sprintf('Product `SID(%s)` not found', $sid));
        }
    }
}