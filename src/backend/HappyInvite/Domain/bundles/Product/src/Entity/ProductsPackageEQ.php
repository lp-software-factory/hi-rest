<?php
namespace HappyInvite\Domain\Bundles\Product\Entity;

use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Product\Repository\ProductsPackageEQRepository")
 * @Table(name="products_package_eq")
 */
final class ProductsPackageEQ implements IdEntity, SerialEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait;
    use SerialEntityTrait;
    use VersionEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage")
     * @JoinColumn(name="products_package_id", referencedColumnName="id")
     * @var ProductsPackage
     */
    private $package;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Product\Entity\Product")
     * @JoinColumn(name="product_id", referencedColumnName="id")
     * @var Product
     */
    private $product;

    public function __construct(ProductsPackage $package, Product $product)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->package = $package;
        $this->product = $product;
    }

    public function getPackage(): ProductsPackage
    {
        return $this->package;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}