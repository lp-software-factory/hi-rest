<?php
namespace HappyInvite\Domain\Bundles\Product\Entity;

use Cocur\Chain\Chain;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Entity\Price;
use HappyInvite\Domain\Bundles\Palette\Entity\Palette;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Product\Repository\ProductsPackageRepository")
 * @Table(name="products_package")
 */
class ProductsPackage implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, ModificationEntityTrait, ActivatedEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @Column(name="palette", type="palette")
     * @var \HappyInvite\Domain\Bundles\Palette\Entity\Palette
     */
    private $palette;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\Product\Entity\ProductsPackageEQ", mappedBy="package", cascade={"all"})
     * @var Collection
     */
    private $products;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Money\Entity\Price")
     * @JoinColumn(name="base_price_id", referencedColumnName="id")
     * @var Price
     */
    private $basePrice;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Palette $palette,
        Price $basePrice
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->palette = $palette;
        $this->basePrice = $basePrice;

        $this->products = new ArrayCollection();
        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'palette' => $this->getPalette()->toJSON($options),
            'base_price' => $this->getBasePrice()->toJSON($options),
            'is_activated' => $this->isActivated(),
            'products' => Chain::create($this->getProducts())
                ->sort(function(ProductsPackageEQ $a, ProductsPackageEQ $b) {
                    return $a->getPosition() > $b->getPosition();
                })
                ->map(function(ProductsPackageEQ $eq) {
                    return $eq->getProduct()->toJSON();
                })
                ->array
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getBasePrice(): Price
    {
        return $this->basePrice;
    }

    public function setBasePrice(Price $basePrice): self
    {
        $this->basePrice = $basePrice;
        $this->markAsUpdated();

        return $this;
    }

    public function getPalette(): Palette
    {
        return $this->palette;
    }

    public function setPalette(Palette $palette): self
    {
        $this->palette = $palette;
        $this->markAsUpdated();

        return $this;
    }

    public function clearProducts(): self
    {
        $this->products->clear();
        $this->markAsUpdated();

        return $this;
    }

    public function getProducts(): array
    {
        return $this->products->toArray();
    }

    public function getProductsEQ(): array
    {
        return $this->products->toArray();
    }

    public function setProducts(array $products): self
    {
        $position = 1;
        $this->products->clear();

        array_map(function(Product $product) use(&$position) {
            $eq = new ProductsPackageEQ($this, $product);
            $eq->setPosition($position++);

            $this->products->add($eq);
        }, $products);

        $this->markAsUpdated();

        return $this;
    }
}