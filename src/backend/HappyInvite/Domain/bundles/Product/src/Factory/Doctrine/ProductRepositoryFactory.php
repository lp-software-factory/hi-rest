<?php
namespace HappyInvite\Domain\Bundles\Product\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Product\Entity\Product;

final class ProductRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Product::class;
    }
}