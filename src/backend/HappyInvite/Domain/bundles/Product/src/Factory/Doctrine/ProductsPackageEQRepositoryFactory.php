<?php
namespace HappyInvite\Domain\Bundles\Product\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackageEQ;

final class ProductsPackageEQRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return ProductsPackageEQ::class;
    }
}