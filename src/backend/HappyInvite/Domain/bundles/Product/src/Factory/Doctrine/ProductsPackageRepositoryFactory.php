<?php
namespace HappyInvite\Domain\Bundles\Product\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Product\Entity\ProductsPackage;

final class ProductsPackageRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return ProductsPackage::class;
    }
}