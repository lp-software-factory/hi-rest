<?php
namespace HappyInvite\Domain\Bundles\Product;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Product\Factory\Doctrine\ProductRepositoryFactory;
use HappyInvite\Domain\Bundles\Product\Factory\Doctrine\ProductsPackageEQRepositoryFactory;
use HappyInvite\Domain\Bundles\Product\Factory\Doctrine\ProductsPackageRepositoryFactory;
use HappyInvite\Domain\Bundles\Product\Repository\ProductRepository;
use HappyInvite\Domain\Bundles\Product\Repository\ProductsPackageEQRepository;
use HappyInvite\Domain\Bundles\Product\Repository\ProductsPackageRepository;

return [
    ProductRepository::class => factory(ProductRepositoryFactory::class),
    ProductsPackageRepository::class => factory(ProductsPackageRepositoryFactory::class),
    ProductsPackageEQRepository::class => factory(ProductsPackageEQRepositoryFactory::class),
];