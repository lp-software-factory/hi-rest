<?php
namespace HappyInvite\Domain\Bundles\InviteCard;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\InviteCardSizeDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\InviteCardColorsDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\InviteCardFamilyDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\InviteCardGammaDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\InviteCardStyleDomainBundle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\InviteCardTemplateDomainBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class InviteCardDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new InviteCardStyleDomainBundle(),
            new InviteCardColorsDomainBundle(),
            new InviteCardSizeDomainBundle(),
            new InviteCardGammaDomainBundle(),
            new InviteCardFamilyDomainBundle(),
            new InviteCardTemplateDomainBundle(),
        ];
    }
}