<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Formatter;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Designer\Formatter\DesignerFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Formatter\EventTypeGroupFormatter;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\InviteCard\Service\InviteCardRepositoryService;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Formatter\InviteCardSizeFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Color\Formatter\InviteCardColorFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Formatter\InviteCardGammaFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Formatter\InviteCardStyleFormatter;

final class InviteCardRepositoryFormatter
{
    /** @var Locale */
    private $current;

    /** @var InviteCardRepositoryService */
    private $repository;

    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var DesignerFormatter */
    private $designerFormatter;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Formatter\EventTypeGroupFormatter */
    private $eventGroupTypeFormatter;

    /** @var InviteCardSizeFormatter */
    private $cardSizeFormatter;

    /** @var InviteCardColorFormatter */
    private $colorFormatter;

    /** @var InviteCardGammaFormatter */
    private $gammaFormatter;

    /** @var InviteCardStyleFormatter */
    private $styleFormatter;

    public function __construct(
        InviteCardTemplateService $inviteCardTemplateService,
        DesignerFormatter $designerFormatter,
        EventTypeGroupFormatter $eventGroupTypeFormatter,
        InviteCardSizeFormatter $cardSizeFormatter,
        InviteCardColorFormatter $colorFormatter,
        InviteCardGammaFormatter $gammaFormatter,
        InviteCardStyleFormatter $styleFormatter
    ) {
        $this->repository = $repository;
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->designerFormatter = $designerFormatter;
        $this->eventGroupTypeFormatter = $eventGroupTypeFormatter;
        $this->cardSizeFormatter = $cardSizeFormatter;
        $this->colorFormatter = $colorFormatter;
        $this->gammaFormatter = $gammaFormatter;
        $this->styleFormatter = $styleFormatter;
    }


    public function format(Locale $locale)
    {
        $this->current = $locale;

        return [
            'designers' => array_values($this->formatDesigners()),
            'event_group_types' => array_values($this->formatEventGroupTypes()),
            'card_sizes' => array_values($this->formatCardSizes()),
            'colors' => array_values($this->formatColors()),
            'gammas' => array_values($this->formatGammas()),
            'styles' => array_values($this->formatStyles()),
            'families' => array_values($this->formatFamilies()),
        ];
    }

    private function formatDesigners(): array
    {
        return $this->designerFormatter->formatMany($this->repository->getDesigners());
    }

    private function formatEventGroupTypes(): array
    {
        return $this->eventGroupTypeFormatter->formatMany($this->repository->getEventGroupTypes());
    }

    private function formatCardSizes(): array
    {
        return $this->cardSizeFormatter->formatMany($this->repository->getCardSizes());
    }

    private function formatColors(): array
    {
        return $this->colorFormatter->formatMany($this->repository->getColors());
    }

    private function formatGammas(): array
    {
        return $this->gammaFormatter->formatMany($this->repository->getGammas());
    }

    private function formatStyles(): array
    {
        return $this->styleFormatter->formatMany($this->repository->getStyles());
    }

    private function formatFamilies(): array
    {
        return Chain::create($this->repository->getFamilies())
            ->filter(function(InviteCardFamily $family) {
                return $family->getLocale()->getRegion() === $this->current->getRegion();
            })
            ->map(function(InviteCardFamily $family) {
                return $this->formatFamily($family);
            })
            ->array;
    }

    private function formatFamily(InviteCardFamily $family): array
    {
        $json = [
            'id' => $family->getIdNoFall(),
            'sid' => $family->getSID(),
            'metadata' => array_merge($family->getMetadata(), [
                'version' => $family->getMetadataVersion(),
            ]),
            'date_created_at' => $family->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $family->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'locale' => $family->getLocale()->toJSON(),
            'title' => $family->getTitle()->toJSON(),
            'description' => $family->getDescription()->toJSON(),
            'author_id' => $family->getAuthor()->getId(),
            'event_group_type_id' => $family->getEventType()->getGroup()->getId(),
            'event_type_id' => $family->getEventType()->getId(),
            'gamma_id' => $family->getGamma()->getId(),
            'style_id' => $family->getStyle()->getId(),
            'card_size_id' => $family->getCardSize()->getId(),
            'has_photo' => $family->hasPhoto(),
            'position' => $family->getPosition(),
            'templates' => array_map(function(InviteCardTemplate $template) {
                return $this->formatTemplate($template);
            }, $this->inviteCardTemplateService->getAllTemplatesOfFamily($family))
        ];

        return $json;
    }

    private function formatTemplate(InviteCardTemplate $template): array
    {
        $json = [
            "id" => $template->getId(),
            "sid" => $template->getSID(),
            "metadata" => array_merge($template->getMetadata(), [
                'version' => $template->getMetadataVersion(),
            ]),
            'date_created_at' => $template->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $template->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $template->getPosition(),
            'title' => $template->getTitle()->toJSON(),
            'description' => $template->getDescription()->toJSON(),
            'family_id' => $template->getFamily()->getId(),
            'color_id' => $template->getColor()->getId(),
            'preview' => $template->getImages()->toJSON(),
            'backdrop' => $template->getBackdrop()->toJSON(),
            'is_activated' => $template->isActivated(),
        ];

        if($template->isPhotoEnabled()) {
            $json['photo'] = array_merge($template->getPhoto()->toJSON(), [
                'enabled' => true,
            ]);
        }else{
            $json['photo'] = array_merge([], [
                'enabled' => false,
            ]);
        }

        return $json;
    }
}