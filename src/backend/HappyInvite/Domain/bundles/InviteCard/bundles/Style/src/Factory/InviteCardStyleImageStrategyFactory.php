<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Factory;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Config\InviteCardStyleImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Image\StylePreviewImageStrategy;

final class InviteCardStyleImageStrategyFactory
{
    /** @var InviteCardStyleImageConfig */
    private $config;

    public function __construct(InviteCardStyleImageConfig $config)
    {
        $this->config = $config;
    }

    public function createStrategyFor(InviteCardStyle $inviteCardStyle): StylePreviewImageStrategy
    {
        return new StylePreviewImageStrategy(
            $inviteCardStyle,
            $this->config->getFileSystem(),
            $this->config->getWwwPath()
        );
    }
}