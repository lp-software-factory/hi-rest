<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Events;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoGenerateImageSubscription implements SubscriptionScript
{
    /** @var InviteCardStyleService */
    private $styleService;

    /** @var InviteCardStyleImageService */
    private $imageService;

    public function __construct(InviteCardStyleService $styleService, InviteCardStyleImageService $imageService)
    {
        $this->styleService = $styleService;
        $this->imageService = $imageService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $ts = $this->styleService;
        $is = $this->imageService;

        $ts->getEventEmitter()->on(InviteCardStyleService::EVENT_CREATED, function(InviteCardStyle  $style) use ($is) {
            $is->generateImage($style->getId());
        });
    }
}