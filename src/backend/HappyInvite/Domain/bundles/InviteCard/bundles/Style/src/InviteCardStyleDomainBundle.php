<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Events\AutoGenerateImageSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class InviteCardStyleDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoGenerateImageSubscription::class,
        ];
    }
}