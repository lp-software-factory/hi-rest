<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;

final class InviteCardStyleActivationService
{
    public const EVENT_ACTIVATED = 'hi.domain.invite-card.style.activated';
    public const EVENT_DEACTIVATED = 'hi.domain.invite-card.style.deactivated';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardStyleService */
    private $styleService;

    public function __construct(InviteCardStyleService $styleService)
    {
        $this->eventEmitter = new EventEmitter();
        $this->styleService = $styleService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function activateStyle(int $styleId): InviteCardStyle
    {
        $style = $this->styleService->getById($styleId);

        if(! $style->isActivated()) {
            $style->activate();

            $this->eventEmitter->emit(self::EVENT_ACTIVATED, [$style]);
            $this->styleService->updateActivationStatusOf($style);
        }

        return $style;
    }

    public function deactivateStyle(int $styleId): InviteCardStyle
    {
        $style = $this->styleService->getById($styleId);

        if($style->isActivated()) {
            $style->deactivate();

            $this->eventEmitter->emit(self::EVENT_DEACTIVATED, [$style]);
            $this->styleService->updateActivationStatusOf($style);
        }

        return $style;
    }

    public function isStyleActivated(int $styleId): bool
    {
        return $this->styleService->getById($styleId)->isActivated();
    }
}