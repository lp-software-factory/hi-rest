<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style;

return [
    'config.hi.domain.invite-card.bundles.style.image.dir' => 'invite-card/style/image',
    'config.hi.domain.invite-card.bundles.style.image.www' => 'invite-card/style/image',
];