<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Avatar\Image\ImageCollection;
use HappyInvite\Domain\Bundles\Avatar\Parameters\UploadImageParameters;
use HappyInvite\Domain\Bundles\Avatar\Service\AvatarService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Factory\InviteCardStyleImageStrategyFactory;

final class InviteCardStyleImageService
{
    public const EVENT_IMAGE_CREATED = 'hi.domain.invite-card.style.image.created';
    public const EVENT_IMAGE_GENERATED = 'hi.domain.invite-card.style.image.generated';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardStyleImageStrategyFactory */
    private $factory;

    /** @var InviteCardStyleService */
    private $styleService;

    /** @var AvatarService */
    private $avatarService;

    public function __construct(
        InviteCardStyleImageStrategyFactory $factory,
        InviteCardStyleService $styleService,
        AvatarService $avatarService
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->factory = $factory;
        $this->avatarService = $avatarService;
        $this->styleService = $styleService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function uploadImage(int $inviteCardStyleId, UploadImageParameters $parameters): ImageCollection
    {
        $style = $this->styleService->getById($inviteCardStyleId);
        $strategy = $this->factory->createStrategyFor($style);

        $image = $this->avatarService->uploadImage($strategy, $parameters);

        $this->getEventEmitter()->emit(self::EVENT_IMAGE_CREATED,  [$image, $inviteCardStyleId]);

        $this->styleService->updateImageOf($style);

        return $image;
    }

    public function generateImage(int $inviteCardStyleId): ImageCollection
    {
        $style = $this->styleService->getById($inviteCardStyleId);
        $strategy = $this->factory->createStrategyFor($style);

        $image = $this->avatarService->generateImage($strategy);

        $this->getEventEmitter()->emit(self::EVENT_IMAGE_GENERATED,  [$image, $inviteCardStyleId]);

        $this->styleService->updateImageOf($style);

        return $image;
    }
}