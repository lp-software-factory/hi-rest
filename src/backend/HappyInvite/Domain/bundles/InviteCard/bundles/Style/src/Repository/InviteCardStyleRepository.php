<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions\InviteCardStyleNotFoundException;

final class InviteCardStyleRepository extends EntityRepository
{
    public function createStyle(InviteCardStyle $style): int
    {
        while($this->hasWithSID($style->getSID())) {
            $style->regenerateSID();
        }

        $this->getEntityManager()->persist($style);
        $this->getEntityManager()->flush($style);

        return $style->getId();
    }

    public function saveStyle(InviteCardStyle $style)
    {
        $this->getEntityManager()->flush($style);
    }

    public function saveStyleEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveStyle($entity);
        }
    }

    public function deleteStyle(InviteCardStyle $style)
    {
        $this->getEntityManager()->remove($style);
        $this->getEntityManager()->flush($style);
    }

    public function listStyles(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): InviteCardStyle
    {
        $result = $this->find($id);

        if($result instanceof InviteCardStyle) {
            return $result;
        }else{
            throw new InviteCardStyleNotFoundException(sprintf('Style `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Styles with IDs (%s) not found', array_diff($ids, array_map(function(InviteCardStyle $style) {
                return $style->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): InviteCardStyle
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof InviteCardStyle) {
            return $result;
        }else{
            throw new InviteCardStyleNotFoundException(sprintf('Style `SID(%s)` not found', $sid));
        }
    }
}