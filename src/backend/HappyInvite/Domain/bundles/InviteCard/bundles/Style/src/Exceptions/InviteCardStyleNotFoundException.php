<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Exceptions;

final class InviteCardStyleNotFoundException extends \Exception
{
}