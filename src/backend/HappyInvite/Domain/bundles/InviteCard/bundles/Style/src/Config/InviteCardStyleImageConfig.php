<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Config;

use League\Flysystem\FilesystemInterface;

final class InviteCardStyleImageConfig
{
    /** @var string */
    private $wwwPath;

    /** @var FilesystemInterface */
    private $fileSystem;

    public function __construct(string $wwwPath, FilesystemInterface $fileSystem)
    {
        $this->wwwPath = $wwwPath;
        $this->fileSystem = $fileSystem;
    }

    public function getWwwPath(): string
    {
        return $this->wwwPath;
    }

    public function getFileSystem(): FilesystemInterface
    {
        return $this->fileSystem;
    }
}