<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Parameters\CreateInviteCardStyleParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Parameters\EditInviteCardStyleParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Repository\InviteCardStyleRepository;

final class InviteCardStyleService
{
    public const EVENT_CREATED = 'hi.domain.invite-card.style.created';
    public const EVENT_EDITED = 'hi.domain.invite-card.style.edited';
    public const EVENT_UPDATED = 'hi.domain.invite-card.style.updated';
    public const EVENT_DELETE = 'hi.domain.invite-card.style.delete.before';
    public const EVENT_DELETED = 'hi.domain.invite-card.style.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardStyleRepository */
    private $styleRepository;

    public function __construct(InviteCardStyleRepository $styleRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->styleRepository = $styleRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createStyle(CreateInviteCardStyleParameters $parameters): InviteCardStyle
    {
        $style = new InviteCardStyle($parameters->getTitle(), $parameters->getDescription());

        $this->styleRepository->createStyle($style);

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($style);
        $serial->normalize();

        $this->eventEmitter->emit(self::EVENT_CREATED, [$style]);

        return $style;
    }

    public function editStyle(int $styleId, EditInviteCardStyleParameters $parameters): InviteCardStyle
    {
        $style = $this->styleRepository->getById($styleId);
        $style->setTitle($parameters->getTitle());
        $style->setDescription($parameters->getDescription());

        $this->styleRepository->saveStyle($style);
        $this->eventEmitter->emit(self::EVENT_EDITED, [$style]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$style]);

        return $style;
    }

    public function deleteStyle(int $styleId)
    {
        $style = $this->getById($styleId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$style]);
        $this->styleRepository->deleteStyle($style);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$styleId, $style]);
    }

    public function moveUpStyle(int $styleId): int
    {
        $style = $this->getById($styleId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($style);
        $serial->normalize();

        $this->styleRepository->saveStyleEntities($siblings);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$style]);

        return $style->getPosition();
    }

    public function moveDownStyle(int $styleId): int
    {
        $style = $this->getById($styleId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($style);
        $serial->normalize();

        $this->styleRepository->saveStyleEntities($siblings);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$style]);

        return $style->getPosition();
    }

    public function updateActivationStatusOf(InviteCardStyle $style): InviteCardStyle
    {
        $this->styleRepository->saveStyle($style);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$style]);

        return $style;
    }

    public function updateImageOf(InviteCardStyle $style): InviteCardStyle
    {
        $this->styleRepository->saveStyle($style);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$style]);

        return $style;
    }

    public function getById(int $styleId): InviteCardStyle
    {
        return $this->styleRepository->getById($styleId);
    }

    public function getAll(): array
    {
        return $this->styleRepository->getAll();
    }
}