<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Config\InviteCardStyleImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Factory\Doctrine\InviteCardStyleRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Factory\InviteCardStyleImageConfigFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Repository\InviteCardStyleRepository;

return [
    InviteCardStyleRepository::class => factory(InviteCardStyleRepositoryFactory::class),
    InviteCardStyleImageConfig::class => factory(InviteCardStyleImageConfigFactory::class),
];