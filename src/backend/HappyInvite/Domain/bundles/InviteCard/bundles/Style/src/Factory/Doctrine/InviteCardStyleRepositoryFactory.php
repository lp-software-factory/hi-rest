<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;

final class InviteCardStyleRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return InviteCardStyle::class;
    }
}