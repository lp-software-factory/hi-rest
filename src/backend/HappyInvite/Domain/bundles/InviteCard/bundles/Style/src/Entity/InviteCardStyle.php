<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity;

use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntity;
use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntityTrait;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Repository\InviteCardStyleRepository")
 * @Table(name="invite_card_style")
 */
class InviteCardStyle implements JSONSerializable, JSONMetadataEntity, IdEntity, SIDEntity, SerialEntity, ActivatedEntity, ImageEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait, ActivatedEntityTrait, ImageEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    public function __construct(ImmutableLocalizedString $title, ImmutableLocalizedString $description)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->regenerateSID();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            "position" => $this->getPosition(),
            "title" => $this->getTitle()->toJSON($options),
            "description" => $this->getDescription()->toJSON($options),
            "icon" => $this->getImages()->toJSON($options),
            "is_activated" => $this->isActivated(),
        ];
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title)
    {
        $this->title = $title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description)
    {
        $this->description = $description;
    }
}