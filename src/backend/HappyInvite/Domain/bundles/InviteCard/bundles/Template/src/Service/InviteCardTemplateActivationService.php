<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;

final class InviteCardTemplateActivationService
{
    public const EVENT_ACTIVATED = 'hi.domain.invite-card.template.activated';
    public const EVENT_DEACTIVATED = 'hi.domain.invite-card.template.deactivated';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardTemplateService */
    private $templateService;

    public function __construct(InviteCardTemplateService $templateService)
    {
        $this->eventEmitter = new EventEmitter();
        $this->templateService = $templateService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function activateTemplate(int $templateId): InviteCardTemplate
    {
        $template = $this->templateService->getById($templateId);

        if(! $template->isActivated()) {
            $template->activate();

            $this->eventEmitter->emit(self::EVENT_ACTIVATED, [$template]);
            $this->templateService->updateActivationStatusOf($template);
        }

        return $template;
    }

    public function deactivateTemplate(int $templateId): InviteCardTemplate
    {
        $template = $this->templateService->getById($templateId);

        if($template->isActivated()) {
            $template->deactivate();

            $this->eventEmitter->emit(self::EVENT_DEACTIVATED, [$template]);
            $this->templateService->updateActivationStatusOf($template);
        }

        return $template;
    }

    public function isTemplateActivated(int $templateId): bool
    {
        return $this->templateService->getById($templateId)->isActivated();
    }
}