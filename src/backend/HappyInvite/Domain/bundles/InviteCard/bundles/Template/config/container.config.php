<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Config\InviteCardTemplateImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Factory\Doctrine\InviteCardTemplateRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Factory\InviteCardTemplateImageConfigFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Repository\InviteCardTemplateRepository;

return [
    InviteCardTemplateRepository::class => factory(InviteCardTemplateRepositoryFactory::class),
    InviteCardTemplateImageConfig::class => factory(InviteCardTemplateImageConfigFactory::class),
];