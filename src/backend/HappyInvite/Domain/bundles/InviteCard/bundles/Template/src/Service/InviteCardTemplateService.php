<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplatePhoto;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\CreateInviteCardTemplateParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\EditInviteCardTemplateParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Repository\InviteCardTemplateRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class InviteCardTemplateService
{
    public const EVENT_CREATED = 'hi.domain.invite-card.template.created';
    public const EVENT_EDITED = 'hi.domain.invite-card.template.edited';
    public const EVENT_UPDATED = 'hi.domain.invite-card.template.updated';
    public const EVENT_USER_SPECIFIED = 'hi.domain.invite-card.template.user-specified';
    public const EVENT_OWNER_SPECIFIED = 'hi.domain.invite-card.template.owner-specified';
    public const EVENT_DELETE = 'hi.domain.invite-card.template.delete.before';
    public const EVENT_DELETED = 'hi.domain.invite-card.template.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardTemplateRepository */
    private $templateRepository;

    public function __construct(InviteCardTemplateRepository $inviteCardTemplateRepository)
    {
        $this->templateRepository = $inviteCardTemplateRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createTemplate(CreateInviteCardTemplateParameters $parameters): InviteCardTemplate
    {
        $template = new InviteCardTemplate(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getFamily(),
            $parameters->getColor(),
            $parameters->getBackdrop(),
            $parameters->getAttachmentIds(),
            $parameters->getConfig()
        );

        if($parameters->hasPhoto()) {
            $template->enablePhoto(new InviteCardTemplatePhoto(
                $parameters->getPhotoPlaceholder(),
                $parameters->getPhotoContainerStart(),
                $parameters->getPhotoContainerEnd()
            ));
        }

        $this->templateRepository->createTemplate($template);

        $serial = new SerialManager($this->getAllTemplatesOfFamily($template->getFamily()));
        $serial->insertLast($template);
        $serial->normalize();

        $this->eventEmitter->emit(self::EVENT_CREATED, [$template, $parameters]);

        return $template;
    }

    public function editTemplate(int $templateId, EditInviteCardTemplateParameters $parameters): InviteCardTemplate
    {
        $template = $this->templateRepository->getById($templateId)
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setColor($parameters->getColor())
            ->setFamily($parameters->getFamily())
            ->setBackdrop($parameters->getBackdrop())
            ->setConfig($parameters->getConfig())
            ->setAttachmentIds($parameters->getAttachmentIds())
        ;

        if($parameters->hasPhoto()) {
            if($template->isPhotoEnabled()) {
                $template->getPhoto()
                    ->setPlaceholder($parameters->getPhotoPlaceholder())
                    ->setContainerStart($parameters->getPhotoContainerStart())
                    ->setContainerEnd($parameters->getPhotoContainerEnd());
            }else{
                $template->enablePhoto(new InviteCardTemplatePhoto(
                    $parameters->getPhotoPlaceholder(),
                    $parameters->getPhotoContainerStart(),
                    $parameters->getPhotoContainerEnd()
                ));
            }
        }else{
            if($template->isPhotoEnabled()) {
                $template->disablePhoto();
            }
        }

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$template, $parameters]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template, $parameters]);

        return $template;
    }

    public function duplicateInviteCardTemplate(InviteCardTemplate $input): InviteCardTemplate
    {
        $parameters = new CreateInviteCardTemplateParameters(
            $input->getTitle(),
            $input->getDescription(),
            $input->getFamily(),
            $input->getColor(),
            $input->getBackdrop(),
            $input->getAttachmentIds(),
            $input->getConfig()
        );

        if($input->isPhotoEnabled()) {
            $parameters->withPhoto(
                $input->getPhoto()->getPlaceholder(),
                $input->getPhoto()->getContainerStart(),
                $input->getPhoto()->getContainerEnd()
            );
        }

        return $this->createTemplate($parameters);
    }

    public function setOwner(int $id, Profile $newOwner): void
    {
        $template = $this->templateRepository->getById($id);
        $template->setSolutionComponentOwner($newOwner);

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_OWNER_SPECIFIED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        $this->templateRepository->saveTemplate($template);
    }

    public function markAsUserSpecified(int $id, Profile $newOwner)
    {
        $template = $this->templateRepository->getById($id);
        $template->setSolutionComponentOwner($newOwner);
        $template->markSolutionAsUserSpecified();

        $this->templateRepository->saveTemplate($template);

        $this->eventEmitter->emit(self::EVENT_USER_SPECIFIED, [$template]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        $this->templateRepository->saveTemplate($template);
    }

    public function updateActivationStatusOf(InviteCardTemplate $template): InviteCardTemplate
    {
        $this->templateRepository->saveTemplate($template);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        return $template;
    }

    public function updateImageOf(InviteCardTemplate $template): InviteCardTemplate
    {
        $this->templateRepository->saveTemplate($template);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        return $template;
    }

    public function deleteTemplate(int $templateId)
    {
        $template = $this->getById($templateId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$template]);
        $this->templateRepository->deleteTemplate($template);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$templateId, $template]);
    }

    public function moveUpTemplate(int $templateId): int
    {
        $template = $this->getById($templateId);
        $siblings = $this->getAllTemplatesOfFamily($template->getFamily());

        $serial = new SerialManager($siblings);
        $serial->up($template);
        $serial->normalize();
        $this->templateRepository->saveTemplateEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        return $template->getPosition();
    }

    public function moveDownTemplate(int $templateId): int
    {
        $template = $this->getById($templateId);
        $siblings = $this->getAllTemplatesOfFamily($template->getFamily());

        $serial = new SerialManager($siblings);
        $serial->down($template);
        $serial->normalize();
        $this->templateRepository->saveTemplateEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_UPDATED, [$template]);

        return $template->getPosition();
    }

    public function getById(int $templateId): InviteCardTemplate
    {
        return $this->templateRepository->getById($templateId);
    }

    public function getAll(): array
    {
        return $this->templateRepository->getAll();
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->templateRepository->getByIds($idsCriteria);
    }

    public function getAllTemplatesOfFamily(InviteCardFamily $family)
    {
        return $this->templateRepository->getAllTemplatesOfFamily($family);
    }
}