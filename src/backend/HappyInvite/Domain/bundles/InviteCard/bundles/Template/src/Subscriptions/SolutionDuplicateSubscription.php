<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Subscriptions;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class SolutionDuplicateSubscription implements SubscriptionScript
{
    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(InviteCardTemplateService $inviteCardTemplateService, SolutionService $solutionService)
    {
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->solutionService = $solutionService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_BEFORE_DUPLICATE, function(Solution $origSolution, Solution $copySolution, Profile $owner = null) {
            $copySolution->setInviteCardTemplates(array_map(function(InviteCardTemplate $input) use ($owner) {
                $result = $this->inviteCardTemplateService->duplicateInviteCardTemplate($input);

                if($owner) {
                    $this->inviteCardTemplateService->setOwner($result->getId(), $owner);
                }

                return $result;
            }, $origSolution->getInviteCardTemplates()));
        });
    }
}