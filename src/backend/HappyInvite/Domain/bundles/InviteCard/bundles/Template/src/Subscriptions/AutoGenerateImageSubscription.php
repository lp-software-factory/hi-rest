<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Subscriptions;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoGenerateImageSubscription implements SubscriptionScript
{
    /** @var InviteCardTemplateService */
    private $templateService;

    /** @var InviteCardTemplateImageService */
    private $imageService;

    public function __construct(InviteCardTemplateService $templateService, InviteCardTemplateImageService $imageService)
    {
        $this->templateService = $templateService;
        $this->imageService = $imageService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $ts = $this->templateService;
        $is = $this->imageService;

        $ts->getEventEmitter()->on(InviteCardTemplateService::EVENT_CREATED, function(InviteCardTemplate  $template) use ($is) {
            $is->generateImage($template->getId());
        });
    }
}