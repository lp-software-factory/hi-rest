<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\DoctrineORM\Doctrine\Type\Point\Point;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;

abstract class AbstractInviteCardTemplateParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var InviteCardFamily */
    private $family;

    /** @var InviteCardColor */
    private $color;

    /** @var Attachment */
    private $backdrop;

    /** @var array */
    private $config;

    /** @var array */
    private $attachmentIds = [];

    /** @var Attachment */
    private $photoPlaceholder;

    /** @var Point */
    private $photoContainerStart;

    /** @var Point */
    private $photoContainerEnd;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        InviteCardFamily $family,
        InviteCardColor $color,
        Attachment $backdrop,
        array $attachmentIds,
        array $config
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->family = $family;
        $this->color = $color;
        $this->backdrop = $backdrop;
        $this->attachmentIds = $attachmentIds;
        $this->config = $config;
    }

    public function withPhoto(Attachment $placeholder, Point $containerStart, Point $containerEnd)
    {
        $this->photoPlaceholder = $placeholder;
        $this->photoContainerStart = $containerStart;
        $this->photoContainerEnd = $containerEnd;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getFamily(): InviteCardFamily
    {
        return $this->family;
    }

    public function getColor(): InviteCardColor
    {
        return $this->color;
    }

    public function getBackdrop(): Attachment
    {
        return $this->backdrop;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function getAttachmentIds(): array
    {
        return $this->attachmentIds;
    }

    public function hasPhoto(): bool
    {
        return $this->photoPlaceholder !== null;
    }

    public function getPhotoPlaceholder(): Attachment
    {
        return $this->photoPlaceholder;
    }

    public function getPhotoContainerStart(): Point
    {
        return $this->photoContainerStart;
    }

    public function getPhotoContainerEnd(): Point
    {
        return $this->photoContainerEnd;
    }
}