<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Image;

use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntity;
use HappyInvite\Domain\Bundles\Avatar\Strategy\ImageStrategy;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use League\Flysystem\FilesystemInterface;

final class TemplatePreviewImageStrategy implements ImageStrategy
{
    /** @var InviteCardTemplate */
    private $inviteCardTemplate;

    /** @var FilesystemInterface */
    private $fileSystem;

    /** @var string */
    private $wwwPath;

    public function __construct(InviteCardTemplate $inviteCardTemplate, FilesystemInterface $fileSystem, string $wwwPath)
    {
        $this->inviteCardTemplate = $inviteCardTemplate;
        $this->fileSystem = $fileSystem;
        $this->wwwPath = $wwwPath;
    }

    public function getEntity(): ImageEntity
    {
        return $this->inviteCardTemplate;
    }

    public function getEntityId(): string
    {
        return $this->inviteCardTemplate->getIdNoFall();
    }

    public function getLetter(): string
    {
        $title = $this->inviteCardTemplate->getTitle();

        if($title->countTranslation()) {
            $definitions = $title->getDefinition();
            $definition = array_pop($definitions);

            return mb_substr($definition['value'], 0, 1);
        }else{
            return 'T';
        }
    }

    public function getFilesystem(): FilesystemInterface
    {
        return $this->fileSystem;
    }

    public function getPublicPath(): string
    {
        return $this->wwwPath;
    }

    public function getDefaultSize(): int
    {
        return $this->inviteCardTemplate->getFamily()->getCardSize()->getWidth();
    }

    public function getSizes(): array
    {
        $card = $this->inviteCardTemplate->getFamily()->getCardSize();

        return [
            "orig" => $card->getWidth(),
            "preview" => $card->getBrowserPreviewWidth()
        ];
    }

    public function getRatio(): float
    {
        $card = $this->inviteCardTemplate->getFamily()->getCardSize();

        return $card->getWidth() / $card->getHeight();
    }

    public function validate(\Intervention\Image\Image $origImage)
    {
        return true;
    }
}