<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template;

return [
    'config.hi.domain.invite-card.bundles.template.image.dir' => 'invite-card/template/image',
    'config.hi.domain.invite-card.bundles.template.image.www' => 'invite-card/template/image',
];