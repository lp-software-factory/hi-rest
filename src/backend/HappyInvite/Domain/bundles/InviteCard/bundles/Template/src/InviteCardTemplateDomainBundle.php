<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Subscriptions\AssignSolutionSubscriptionScript;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Subscriptions\AutoGenerateImageSubscription;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Subscriptions\SolutionDuplicateSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class InviteCardTemplateDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoGenerateImageSubscription::class,
            AssignSolutionSubscriptionScript::class,
            SolutionDuplicateSubscription::class,
        ];
    }
}