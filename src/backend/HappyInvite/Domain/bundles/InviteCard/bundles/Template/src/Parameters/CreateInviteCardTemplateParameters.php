<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters;

final class CreateInviteCardTemplateParameters extends AbstractInviteCardTemplateParameters
{
    /** @var bool */
    private $bindToFamily = false;

    public function bindToFamily(): void
    {
        $this->bindToFamily = true;
    }

    public function shouldBeBindedToFamily(): bool
    {
        return $this->bindToFamily;
    }
}