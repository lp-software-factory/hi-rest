<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions\InviteCardTemplateNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class InviteCardTemplateRepository extends EntityRepository
{
    public function createTemplate(InviteCardTemplate $template): int
    {
        while ($this->hasWithSID($template->getSID())) {
            $template->regenerateSID();
        }

        $this->getEntityManager()->persist($template);
        $this->getEntityManager()->flush($template);

        return $template->getId();
    }

    public function saveTemplate(InviteCardTemplate $template)
    {
        $this->getEntityManager()->flush($template);
    }

    public function saveTemplateEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveTemplate($entity);
        }
    }

    public function deleteTemplate(InviteCardTemplate $template)
    {
        $this->getEntityManager()->remove($template);
        $this->getEntityManager()->flush($template);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getAllTemplatesOfFamily(InviteCardFamily $family): array
    {
        return $this->findBy([
            'family' => $family
        ]);
    }

    public function getById(int $id): InviteCardTemplate
    {
        $result = $this->find($id);

        if ($result instanceof InviteCardTemplate) {
            return $result;
        } else {
            throw new InviteCardTemplateNotFoundException(sprintf('Template `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Templates with IDs (%s) not found', array_diff($ids, array_map(function (InviteCardTemplate $template) {
                return $template->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): InviteCardTemplate
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof InviteCardTemplate) {
            return $result;
        } else {
            throw new InviteCardTemplateNotFoundException(sprintf('Template `SID(%s)` not found', $sid));
        }
    }
}