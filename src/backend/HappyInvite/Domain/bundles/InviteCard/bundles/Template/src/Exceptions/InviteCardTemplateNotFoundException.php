<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Exceptions;

final class InviteCardTemplateNotFoundException extends \Exception {}