<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;

final class InviteCardTemplateRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return InviteCardTemplate::class;
    }
}