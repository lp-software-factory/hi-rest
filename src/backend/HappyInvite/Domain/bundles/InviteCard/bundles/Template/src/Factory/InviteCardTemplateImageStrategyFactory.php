<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Factory;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Config\InviteCardTemplateImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Image\TemplatePreviewImageStrategy;

final class InviteCardTemplateImageStrategyFactory
{
    /** @var InviteCardTemplateImageConfig */
    private $config;

    public function __construct(InviteCardTemplateImageConfig $config)
    {
        $this->config = $config;
    }

    public function createStrategyFor(InviteCardTemplate $inviteCardTemplate): TemplatePreviewImageStrategy
    {
        return new TemplatePreviewImageStrategy(
            $inviteCardTemplate,
            $this->config->getFileSystem(),
            $this->config->getWwwPath()
        );
    }
}