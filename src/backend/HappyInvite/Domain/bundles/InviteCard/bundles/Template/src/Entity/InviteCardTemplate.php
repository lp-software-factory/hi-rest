<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntity;
use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntityTrait;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntity;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntityTrait;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Repository\InviteCardTemplateRepository")
 * @Table(name="invite_card_template")
 */
class InviteCardTemplate implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, SerialEntity, ImageEntity, ActivatedEntity, VersionEntity, SolutionComponentEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait, ImageEntityTrait, ActivatedEntityTrait, VersionEntityTrait, SolutionComponentEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize")
     * @JoinColumn(name="card_size_id", referencedColumnName="id")
     * @var InviteCardSize
     */
    private $cardSize;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily")
     * @JoinColumn(name="invite_card_family_id", referencedColumnName="id")
     * @var InviteCardFamily
     */
    private $family;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor")
     * @JoinColumn(name="invite_card_color_id", referencedColumnName="id")
     * @var InviteCardColor
     */
    private $color;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="backdrop", referencedColumnName="id")
     * @var Attachment
     */
    private $backdrop;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplatePhoto", cascade={"persist"})
     * @JoinColumn(name="invite_card_template_photo_id", referencedColumnName="id")
     * @var InviteCardTemplatePhoto
     */
    private $photo;

    /**
     * @Column(name="config", type="json_array")
     * @var array
     */
    private $config;

    /**
     * @Column(name="attachment_ids", type="json_array")
     * @var array
     */
    private $attachmentIds = [];

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        InviteCardFamily $family,
        InviteCardColor $color,
        Attachment $backdrop,
        array $attachmentIds,
        array $config
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->family = $family;
        $this->color = $color;
        $this->backdrop = $backdrop;
        $this->config = $config;
        $this->cardSize = $family->getCardSize();

        $this->setAttachmentIds($attachmentIds);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'solution' => [
                'is_user_specified' => $this->isSolutionUserSpecified(),
                'owner' => [
                    'has' => $this->hasSolutionComponentOwner(),
                ]
            ],
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            "position" => $this->getPosition(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'family_id' => $this->getFamily()->getId(),
            'color' => $this->getColor()->toJSON($options),
            'preview' => $this->getImages()->toJSON($options),
            'is_activated' => $this->isActivated(),
            'backdrop' => $this->getBackdrop()->toJSON($options),
            'config' => $this->getConfig(),
            'attachment_ids' => $this->getAttachmentIds(),
            'card_size' => $this->getCardSize()->toJSON($options),
        ];

        if($this->hasSolutionComponentOwner()) {
            $json['solution']['owner']['profile'] = $this->getSolutionComponentOwner()->toJSON($options);
        }

        if($this->isPhotoEnabled()) {
            $json['photo'] = array_merge($this->getPhoto()->toJSON($options), [
                'enabled' => true,
            ]);
        }else{
            $json['photo'] = [
                'enabled' => false,
            ];
        }

        return $json;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getFamily(): InviteCardFamily
    {
        return $this->family;
    }

    public function setFamily(InviteCardFamily $family): self
    {
        $this->family = $family;
        $this->cardSize = $family->getCardSize();
        $this->markAsUpdated();

        return $this;
    }

    public function getCardSize(): InviteCardSize
    {
        return $this->cardSize;
    }

    public function getColor(): InviteCardColor
    {
        return $this->color;
    }

    public function setColor(InviteCardColor $color): self
    {
        $this->color = $color;
        $this->markAsUpdated();

        return $this;
    }

    public function getBackdrop(): Attachment
    {
        return $this->backdrop;
    }

    public function setBackdrop(Attachment $backdrop): self
    {
        $this->backdrop = $backdrop;
        $this->markAsUpdated();

        return $this;
    }

    public function isPhotoEnabled(): bool
    {
        return $this->photo !== null;
    }

    public function getPhoto(): InviteCardTemplatePhoto
    {
        return $this->photo;
    }

    public function enablePhoto(InviteCardTemplatePhoto $photo): self
    {
        $this->photo = $photo;
        $this->markAsUpdated();

        return $this;
    }

    public function disablePhoto(): self
    {
        $this->photo = null;
        $this->markAsUpdated();

        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): self
    {
        $this->config = $config;
        $this->markAsUpdated();

        return $this;
    }

    public function getAttachmentIds(): array
    {
        return $this->attachmentIds;
    }

    public function setAttachmentIds(array $attachmentIds): self
    {
        $this->attachmentIds = array_map(function(int $input) {
            return (int) $input;
        }, $attachmentIds);

        $this->markAsUpdated();

        return $this;
    }
}