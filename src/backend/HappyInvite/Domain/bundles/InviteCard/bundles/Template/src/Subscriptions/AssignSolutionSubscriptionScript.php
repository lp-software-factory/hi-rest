<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Subscriptions;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AssignSolutionSubscriptionScript implements SubscriptionScript
{
    /** @var SolutionService */
    private $solutionService;

    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    public function __construct(
        SolutionService $solutionService,
        InviteCardTemplateService $inviteCardTemplateService
    ) {
        $this->solutionService = $solutionService;
        $this->inviteCardTemplateService = $inviteCardTemplateService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_ASSIGNED_TO, function(Solution $solution) {
            /** @var InviteCardTemplate $inviteCardTemplate */
            foreach($solution->getInviteCardTemplates() as $inviteCardTemplate) {
                $this->inviteCardTemplateService->setOwner($inviteCardTemplate->getId(), $solution->getTarget());
            }
        });

        $this->inviteCardTemplateService->getEventEmitter()->on(InviteCardTemplateService::EVENT_EDITED, function(InviteCardTemplate $inviteCardTemplate) {
            if($inviteCardTemplate->hasSolutionComponentOwner()) {
                $this->inviteCardTemplateService->markAsUserSpecified($inviteCardTemplate->getId(), $inviteCardTemplate->getSolutionComponentOwner());
            }
        });
    }
}