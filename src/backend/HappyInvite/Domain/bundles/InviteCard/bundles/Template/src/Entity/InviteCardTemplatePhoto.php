<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\DoctrineORM\Doctrine\Type\Point\Point;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity()
 * @Table(name="invite_card_template_photo")
 */
class InviteCardTemplatePhoto implements JSONSerializable, IdEntity, ModificationEntity, VersionEntity
{
    public const CURRENT_VERSION = '1.0.0';

    use IdEntityTrait, ModificationEntityTrait, VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="placeholder", referencedColumnName="id")
     * @var Attachment
     */
    private $placeholder;

    /**
     * @Column(name="container_start", type="point")
     * @var Point
     */
    private $containerStart;

    /**
     * @Column(name="container_end", type="point")
     * @var Point
     */
    private $containerEnd;

    public function __construct(Attachment $placeholder, Point $containerStart, Point $containerEnd)
    {
        $this->setCurrentVersion(self::CURRENT_VERSION);

        $this->placeholder = $placeholder;
        $this->containerStart = $containerStart;
        $this->containerEnd = $containerEnd;

        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            'placeholder' => $this->getPlaceholder()->toJSON($options),
            'container' => [
                'start' => $this->getContainerStart()->toJSON($options),
                'end' => $this->getContainerEnd()->toJSON($options),
            ],
        ];
    }

    public function getPlaceholder(): Attachment
    {
        return $this->placeholder;
    }

    public function setPlaceholder(Attachment $placeholder): self
    {
        $this->placeholder = $placeholder;
        $this->markAsUpdated();

        return $this;
    }

    public function getContainerStart(): Point
    {
        return $this->containerStart;
    }

    public function setContainerStart(Point $containerStart): self
    {
        $this->containerStart = $containerStart;
        $this->markAsUpdated();

        return $this;
    }

    public function getContainerEnd(): Point
    {
        return $this->containerEnd;
    }

    public function setContainerEnd(Point $containerEnd): self
    {
        $this->containerEnd = $containerEnd;
        $this->markAsUpdated();

        return $this;
    }
}