<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Avatar\Image\ImageCollection;
use HappyInvite\Domain\Bundles\Avatar\Parameters\UploadImageParameters;
use HappyInvite\Domain\Bundles\Avatar\Service\AvatarService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Factory\InviteCardTemplateImageStrategyFactory;

final class InviteCardTemplateImageService
{
    public const EVENT_IMAGE_CREATED = 'hi.domain.invite-card.template.image.created';
    public const EVENT_IMAGE_GENERATED = 'hi.domain.invite-card.template.image.generated';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardTemplateImageStrategyFactory */
    private $factory;

    /** @var InviteCardTemplateService */
    private $templateService;

    /** @var AvatarService */
    private $avatarService;

    public function __construct(
        InviteCardTemplateImageStrategyFactory $factory,
        InviteCardTemplateService $templateService,
        AvatarService $avatarService
    ) {
        $this->eventEmitter = new EventEmitter();
        $this->factory = $factory;
        $this->avatarService = $avatarService;
        $this->templateService = $templateService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function uploadImage(int $inviteCardTemplateId, UploadImageParameters $parameters): ImageCollection
    {
        $template = $this->templateService->getById($inviteCardTemplateId);
        $strategy = $this->factory->createStrategyFor($template);

        $image = $this->avatarService->uploadImage($strategy, $parameters);

        $this->getEventEmitter()->emit(self::EVENT_IMAGE_CREATED,  [$image, $inviteCardTemplateId]);

        $this->templateService->updateImageOf($template);

        return $image;
    }

    public function generateImage(int $inviteCardTemplateId): ImageCollection
    {
        $template = $this->templateService->getById($inviteCardTemplateId);
        $strategy = $this->factory->createStrategyFor($template);

        $image = $this->avatarService->generateImage($strategy);

        $this->getEventEmitter()->emit(self::EVENT_IMAGE_GENERATED,  [$image, $inviteCardTemplateId]);

        $this->templateService->updateImageOf($template);

        return $image;
    }
}