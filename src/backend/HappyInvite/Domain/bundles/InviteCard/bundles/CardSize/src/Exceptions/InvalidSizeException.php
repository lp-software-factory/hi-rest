<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions;

final class InvalidSizeException extends \Exception {}