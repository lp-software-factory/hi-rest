<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;

final class InviteCardSizeActivationService
{
    public const EVENT_ACTIVATED = 'hi.domain.invite-card.cardSize.activated';
    public const EVENT_DEACTIVATED = 'hi.domain.invite-card.cardSize.deactivated';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardSizeService */
    private $cardSizeService;

    public function __construct(InviteCardSizeService $cardSizeService)
    {
        $this->eventEmitter = new EventEmitter();
        $this->cardSizeService = $cardSizeService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function activateCardSize(int $cardSizeId): InviteCardSize
    {
        $cardSize = $this->cardSizeService->getById($cardSizeId);

        if(! $cardSize->isActivated()) {
            $cardSize->activate();

            $this->eventEmitter->emit(self::EVENT_ACTIVATED, [$cardSize]);
            $this->cardSizeService->updateActivationStatusOf($cardSize);
        }

        return $cardSize;
    }

    public function deactivateCardSize(int $cardSizeId): InviteCardSize
    {
        $cardSize = $this->cardSizeService->getById($cardSizeId);

        if($cardSize->isActivated()) {
            $cardSize->deactivate();

            $this->eventEmitter->emit(self::EVENT_DEACTIVATED, [$cardSize]);
            $this->cardSizeService->updateActivationStatusOf($cardSize);
        }

        return $cardSize;
    }

    public function isCardSizeActivated(int $cardSizeId): bool
    {
        return $this->cardSizeService->getById($cardSizeId)->isActivated();
    }
}