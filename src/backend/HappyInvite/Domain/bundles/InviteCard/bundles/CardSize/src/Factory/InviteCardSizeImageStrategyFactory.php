<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Factory;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Config\InviteCardSizeImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Image\CardSizePreviewImageStrategy;

final class InviteCardSizeImageStrategyFactory
{
    /** @var InviteCardSizeImageConfig */
    private $config;

    public function __construct(InviteCardSizeImageConfig $config)
    {
        $this->config = $config;
    }

    public function createStrategyFor(InviteCardSize $inviteCardSize): CardSizePreviewImageStrategy
    {
        return new CardSizePreviewImageStrategy(
            $inviteCardSize,
            $this->config->getFileSystem(),
            $this->config->getWwwPath()
        );
    }
}