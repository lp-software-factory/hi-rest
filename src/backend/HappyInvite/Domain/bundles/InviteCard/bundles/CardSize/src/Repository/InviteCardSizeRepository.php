<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InviteCardSizeNotFoundException;

final class InviteCardSizeRepository extends EntityRepository
{
    public function createCardSize(InviteCardSize $cardSize): int
    {
        while($this->hasWithSID($cardSize->getSID())) {
            $cardSize->regenerateSID();
        }

        $this->getEntityManager()->persist($cardSize);
        $this->getEntityManager()->flush($cardSize);

        return $cardSize->getId();
    }

    public function saveCardSize(InviteCardSize $cardSize)
    {
        $this->getEntityManager()->flush($cardSize);
    }

    public function saveCardSizeEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveCardSize($entity);
        }
    }

    public function deleteCardSize(InviteCardSize $cardSize)
    {
        $this->getEntityManager()->remove($cardSize);
        $this->getEntityManager()->flush($cardSize);
    }

    public function listCardSizes(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): InviteCardSize
    {
        $result = $this->find($id);

        if($result instanceof InviteCardSize) {
            return $result;
        }else{
            throw new InviteCardSizeNotFoundException(sprintf('CardSize `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('CardSizes with IDs (%s) not found', array_diff($ids, array_map(function(InviteCardSize $cardSize) {
                return $cardSize->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): InviteCardSize
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof InviteCardSize) {
            return $result;
        }else{
            throw new InviteCardSizeNotFoundException(sprintf('CardSize `SID(%s)` not found', $sid));
        }
    }
}