<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Events\AutoGenerateImageSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class InviteCardSizeDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoGenerateImageSubscription::class,
        ];
    }
}