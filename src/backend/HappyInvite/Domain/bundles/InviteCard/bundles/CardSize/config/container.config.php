<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Config\InviteCardSizeImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Factory\Doctrine\InviteCardSizeRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Factory\InviteCardSizeImageConfigFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Repository\InviteCardSizeRepository;

return [
    InviteCardSizeRepository::class => factory(InviteCardSizeRepositoryFactory::class),
    InviteCardSizeImageConfig::class => factory(InviteCardSizeImageConfigFactory::class),
];