<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

final class CreateInviteCardSizeParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;
    
    /** @var int */
    private $width;
    
    /** @var int */
    private $height;

    /** @var int */
    private $browserWidth;

    /** @var int */
    private $browserHeight;
    
    public function __construct(
        ImmutableLocalizedString $title, 
        ImmutableLocalizedString $description, 
        int $width, 
        int $height,
        int $browserWidth,
        int $browserHeight
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->width = $width;
        $this->height = $height;
        $this->browserWidth = $browserWidth;
        $this->browserHeight = $browserHeight;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getWidth(): int
    {
        return $this->width;
    }
    
    public function getHeight(): int
    {
        return $this->height;
    }

    public function getBrowserWidth(): int
    {
        return $this->browserWidth;
    }

    public function getBrowserHeight(): int
    {
        return $this->browserHeight;
    }
}