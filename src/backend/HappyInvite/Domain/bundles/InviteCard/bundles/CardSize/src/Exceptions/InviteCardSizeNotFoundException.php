<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions;

final class InviteCardSizeNotFoundException extends \Exception {}