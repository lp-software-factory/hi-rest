<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Factory;

use HappyInvite\Domain\Bundles\Attachment\Config\AttachmentConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Config\InviteCardSizeImageConfig;
use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\Service\EnvironmentService;
use Interop\Container\ContainerInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;

final class InviteCardSizeImageConfigFactory
{
    public function __invoke(ContainerInterface $container, AttachmentConfig $config, EnvironmentService $environmentService)
    {
        $www = sprintf('%s/%s', $config->getWww(), $container->get('config.hi.domain.invite-card.bundles.cardSize.image.www'));
        $dir = $dir = sprintf('%s/%s', $config->getDir(), $container->get('config.hi.domain.invite-card.bundles.cardSize.image.dir'));

        if($environmentService->getCurrent() === Environment::TESTING) {
            $adapter = new MemoryAdapter();
        }else{
            $adapter = new Local($dir);
        }

        return new InviteCardSizeImageConfig($www, new Filesystem($adapter));
    }
}