<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity;

use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntity;
use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntityTrait;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Exceptions\InvalidSizeException;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Repository\InviteCardSizeRepository")
 * @Table(name="invite_card_size")
 */
class InviteCardSize implements JSONSerializable, JSONMetadataEntity, IdEntity, SIDEntity, SerialEntity, ImageEntity, ActivatedEntity,VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait, ImageEntityTrait, ActivatedEntityTrait,VersionEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @Column(name="width", type="integer")
     * @var int
     */
    private $width;

    /**
     * @Column(name="height", type="integer")
     * @var int
     */
    private $height;

    /**
     * @Column(name="browser_preview_width", type="integer")
     * @var int
     */
    private $browserPreviewWidth;

    /**
     * @Column(name="browser_preview_height", type="integer")
     * @var int
     */
    private $browserPreviewHeight;

    public function __construct(
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        int $width,
        int $height
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->setSize($width, $height);
        $this->regenerateSID();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            "position" => $this->getPosition(),
            "title" => $this->getTitle()->toJSON($options),
            "description" => $this->getDescription()->toJSON($options),
            "width" => $this->getWidth(),
            "height" => $this->getHeight(),
            "browser_preview" => [
                "width" => $this->getBrowserPreviewWidth(),
                "height" => $this->getBrowserPreviewHeight(),
            ],
            "icon" => $this->getImages()->toJSON($options),
            "is_activated" => $this->isActivated(),
        ];
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title)
    {
        $this->title = $title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description)
    {
        $this->description = $description;
    }

    public function setSize(int $width, int $height)
    {
        if(! (is_int($width) && is_int($height) && ($width > 0) && ($height > 0))) {
            throw new InvalidSizeException(sprintf('Invalid size `w%sxh%s`', $width, $height));
        }

        $this->width = $width;
        $this->height = $height;

        $this->setBrowserCardSize(ceil($width / 2), ceil($height / 2));
    }

    public function setBrowserCardSize(int $width, int $height)
    {
        if(! (is_int($width) && is_int($height) && ($width > 0) && ($height > 0))) {
            throw new InvalidSizeException(sprintf('Invalid size `w%sxh%s`', $width, $height));
        }

        $this->browserPreviewWidth = $width;
        $this->browserPreviewHeight = $height;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getBrowserPreviewWidth(): int
    {
        return $this->browserPreviewWidth;
    }

    public function getBrowserPreviewHeight(): int
    {
        return $this->browserPreviewHeight;
    }
}