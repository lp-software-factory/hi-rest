<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Events;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoGenerateImageSubscription implements SubscriptionScript
{
    /** @var InviteCardSizeService */
    private $cardSizeService;

    /** @var InviteCardSizeImageService */
    private $imageService;

    public function __construct(InviteCardSizeService $cardSizeService, InviteCardSizeImageService $imageService)
    {
        $this->cardSizeService = $cardSizeService;
        $this->imageService = $imageService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $ts = $this->cardSizeService;
        $is = $this->imageService;

        $ts->getEventEmitter()->on(InviteCardSizeService::EVENT_CREATED, function(InviteCardSize  $cardSize) use ($is) {
            $is->generateImage($cardSize->getId());
        });
    }
}