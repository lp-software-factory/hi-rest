<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;

final class InviteCardSizeRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return InviteCardSize::class;
    }
}