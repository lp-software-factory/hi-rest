<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters\CreateInviteCardSizeParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters\EditInviteCardSizeParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Repository\InviteCardSizeRepository;

final class InviteCardSizeService
{
    public const EVENT_CREATED = 'hi.domain.invite-card.card-size.created';
    public const EVENT_EDITED = 'hi.domain.invite-card.card-size.edited';
    public const EVENT_UPDATED = 'hi.domain.invite-card.card-size.updated';
    public const EVENT_DELETE = 'hi.domain.invite-card.card-size.delete.before';
    public const EVENT_DELETED = 'hi.domain.invite-card.card-size.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;
    
    /** @var InviteCardSizeRepository */
    private $cardSizeRepository;

    public function __construct(InviteCardSizeRepository $cardSizeRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->cardSizeRepository = $cardSizeRepository;
    }
    
    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createCardSize(CreateInviteCardSizeParameters $parameters): InviteCardSize
    {
        $cardSize = new InviteCardSize(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getWidth(),
            $parameters->getHeight()
        );

        $cardSize->setBrowserCardSize(
            $parameters->getBrowserWidth(),
            $parameters->getBrowserHeight()
        );

        $this->cardSizeRepository->createCardSize($cardSize);

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($cardSize);
        $serial->normalize();

        $this->eventEmitter->emit(self::EVENT_CREATED, [$cardSize]);

        return $cardSize;
    }

    public function editCardSize(int $cardSizeId, EditInviteCardSizeParameters $parameters): InviteCardSize
    {
        $cardSize = $this->cardSizeRepository->getById($cardSizeId);
        $cardSize->setTitle($parameters->getTitle());
        $cardSize->setDescription($parameters->getDescription());
        $cardSize->setSize($parameters->getWidth(), $parameters->getHeight());

        $cardSize->setBrowserCardSize(
            $parameters->getBrowserWidth(),
            $parameters->getBrowserHeight()
        );

        $this->cardSizeRepository->saveCardSize($cardSize);
        $this->eventEmitter->emit(self::EVENT_EDITED, [$cardSize]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$cardSize]);

        return $cardSize;
    }

    public function deleteCardSize(int $cardSizeId)
    {
        $cardSize = $this->getById($cardSizeId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$cardSize]);
        $this->cardSizeRepository->deleteCardSize($cardSize);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$cardSizeId,  $cardSize]);
    }

    public function moveUpCardSize(int $cardSizeId): int
    {
        $cardSize = $this->getById($cardSizeId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($cardSize);
        $serial->normalize();

        $this->cardSizeRepository->saveCardSizeEntities($siblings);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$cardSize]);

        return $cardSize->getPosition();
    }

    public function moveDownCardSize(int $cardSizeId): int
    {
        $cardSize = $this->getById($cardSizeId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($cardSize);
        $serial->normalize();

        $this->cardSizeRepository->saveCardSizeEntities($siblings);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$cardSize]);

        return $cardSize->getPosition();
    }

    public function updateActivationStatusOf(InviteCardSize $size): InviteCardSize
    {
        $this->cardSizeRepository->saveCardSize($size);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$size]);

        return $size;
    }

    public function updateImageOf(InviteCardSize $size): InviteCardSize
    {
        $this->cardSizeRepository->saveCardSize($size);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$size]);

        return $size;
    }

    public function getById(int $cardSizeId): InviteCardSize
    {
        return $this->cardSizeRepository->getById($cardSizeId);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->cardSizeRepository->getByIds($idsCriteria);
    }

    public function getAll(): array
    {
        return $this->cardSizeRepository->getAll();
    }
}