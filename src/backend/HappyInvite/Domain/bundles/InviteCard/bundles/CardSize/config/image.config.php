<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize;

return [
    'config.hi.domain.invite-card.bundles.cardSize.image.dir' => 'invite-card/card-size/image',
    'config.hi.domain.invite-card.bundles.cardSize.image.www' => 'invite-card/card-size/image',
];