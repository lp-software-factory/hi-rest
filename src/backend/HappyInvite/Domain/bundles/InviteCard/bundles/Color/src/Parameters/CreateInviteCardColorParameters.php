<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

final class CreateInviteCardColorParameters
{
    /** @var ImmutableLocalizedString */
    private $title;

    /** @var string */
    private $hexCode;

    public function __construct(ImmutableLocalizedString $title, string $hexCode)
    {
        $this->title = $title;
        $this->hexCode = $hexCode;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getHexCode(): string
    {
        return $this->hexCode;
    }
}