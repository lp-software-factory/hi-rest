<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color;

use HappyInvite\Platform\Bundles\HIBundle;

final class InviteCardColorsDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}