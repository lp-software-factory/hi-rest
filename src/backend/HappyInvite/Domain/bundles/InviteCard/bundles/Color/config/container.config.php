<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Factory\Doctrine\InviteCardColorRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Repository\InviteCardColorRepository;

return [
    InviteCardColorRepository::class => factory(InviteCardColorRepositoryFactory::class),
];