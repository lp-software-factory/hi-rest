<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Service;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\CreateInviteCardColorParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\EditInviteCardColorParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Repository\InviteCardColorRepository;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class InviteCardColorService
{
    /** @var InviteCardColorRepository */
    private $colorRepository;

    public function __construct(InviteCardColorRepository $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    public function createColor(CreateInviteCardColorParameters $parameters): InviteCardColor
    {
        $color = new InviteCardColor($parameters->getTitle(), $parameters->getHexCode());

        $this->colorRepository->createColor($color);

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($color);
        $serial->normalize();

        return $color;
    }

    public function editColor(int $colorId, EditInviteCardColorParameters $parameters): InviteCardColor
    {
        $color = $this->colorRepository->getById($colorId);
        $color->setTitle($parameters->getTitle());
        $color->setHexCode($parameters->getHexCode());

        $this->colorRepository->saveColor($color);

        return $color;
    }

    public function deleteColor(int $colorId)
    {
        $this->colorRepository->deleteColor(
            $this->getById($colorId)
        );
    }

    public function moveUpColor(int $colorId): int
    {
        $color = $this->getById($colorId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($color);
        $serial->normalize();
        $this->colorRepository->saveColorEntities($siblings);

        return $color->getPosition();
    }

    public function moveDownColor(int $colorId): int
    {
        $color = $this->getById($colorId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($color);
        $serial->normalize();
        $this->colorRepository->saveColorEntities($siblings);

        return $color->getPosition();
    }

    public function getById(int $colorId): InviteCardColor
    {
        return $this->colorRepository->getById($colorId);
    }

    public function getAll(): array
    {
        return $this->colorRepository->getAll();
    }
}