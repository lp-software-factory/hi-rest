<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions\InvalidHexCodeException;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Repository\InviteCardColorRepository")
 * @Table(name="invite_card_color")
 */
class InviteCardColor implements JSONSerializable, JSONMetadataEntity, IdEntity, SIDEntity, SerialEntity,VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, SerialEntityTrait,VersionEntityTrait;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="hex_code", type="string")
     * @var string
     */
    private $hexCode;

    public function __construct(ImmutableLocalizedString $title, string $hexCode)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->setTitle($title);
        $this->setHexCode($hexCode);
        $this->regenerateSID();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            "position" => $this->getPosition(),
            "title" => $this->getTitle()->toJSON($options),
            "hex_code" => $this->getHexCode()
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title)
    {
        $this->title = $title;
    }

    public function getHexCode(): string
    {
        return $this->hexCode;
    }

    public function setHexCode(string $hexCode)
    {
        if(! preg_match('/^#[abcdef0-9ABCDEF]{6}$/', $hexCode)) {
            throw new InvalidHexCodeException(sprintf('Invalid hex code `%s`, should be in format "#000000"-"#FFFFFF"', $hexCode));
        }

        $this->hexCode = $hexCode;
    }
}