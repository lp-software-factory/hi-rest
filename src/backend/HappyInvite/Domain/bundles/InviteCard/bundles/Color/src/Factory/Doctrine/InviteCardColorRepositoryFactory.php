<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;

final class InviteCardColorRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return InviteCardColor::class;
    }
}