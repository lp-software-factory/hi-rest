<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Entity\InviteCardColor;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions\InviteCardColorNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class InviteCardColorRepository extends EntityRepository
{
    public function createColor(InviteCardColor $color): int
    {
        while($this->hasWithSID($color->getSID())) {
            $color->regenerateSID();
        }

        $this->getEntityManager()->persist($color);
        $this->getEntityManager()->flush($color);

        return $color->getId();
    }

    public function saveColor(InviteCardColor $color)
    {
        $this->getEntityManager()->flush($color);
    }

    public function saveColorEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveColor($entity);
        }
    }

    public function deleteColor(InviteCardColor $color)
    {
        $this->getEntityManager()->remove($color);
        $this->getEntityManager()->flush($color);
    }

    public function listColors(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): InviteCardColor
    {
        $result = $this->find($id);

        if($result instanceof InviteCardColor) {
            return $result;
        }else{
            throw new InviteCardColorNotFoundException(sprintf('Color `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Colors with IDs (%s) not found', array_diff($ids, array_map(function(InviteCardColor $color) {
                return $color->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): InviteCardColor
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof InviteCardColor) {
            return $result;
        }else{
            throw new InviteCardColorNotFoundException(sprintf('Color `SID(%s)` not found', $sid));
        }
    }

    public function hasWithHexCode(string $hexCode, int $excludeId = null): bool
    {
        $criteria = [
            'hexCode' => $hexCode
        ];

        /** @var InviteCardColor $result */
        $result = $this->findOneBy($criteria);

        if($result === null) {
            return false;
        }else{
            if($excludeId !== null) {
                return $result->getId() !== $excludeId;
            }else{
                return true;
            }
        }
    }
}