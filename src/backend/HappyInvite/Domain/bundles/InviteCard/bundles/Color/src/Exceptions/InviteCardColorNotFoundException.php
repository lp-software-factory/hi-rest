<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Exceptions;

final class InviteCardColorNotFoundException extends \Exception {}