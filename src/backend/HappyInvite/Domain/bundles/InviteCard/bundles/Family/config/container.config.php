<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Factory\Doctrine\InviteCardFamilyRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Repository\InviteCardFamilyRepository;

return [
    InviteCardFamilyRepository::class => factory(InviteCardFamilyRepositoryFactory::class),
];