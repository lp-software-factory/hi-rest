<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

final class InviteCardFamilyParameters
{
    /** @var Locale */
    private $locale;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var Profile */
    private $author;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType */
    private $eventType;

    /** @var InviteCardStyle */
    private $style;

    /** @var InviteCardGamma */
    private $gamma;

    /** @var InviteCardSize */
    private $cardSize;

    /** @var bool */
    private $hasPhoto;

    public function __construct(
        Locale $locale,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Profile $author,
        EventType $eventType,
        InviteCardStyle $style,
        InviteCardGamma $gamma,
        InviteCardSize $cardSize,
        bool $hasPhoto
    ) {
        $this->locale = $locale;
        $this->title = $title;
        $this->description = $description;
        $this->author = $author;
        $this->eventType = $eventType;
        $this->style = $style;
        $this->gamma = $gamma;
        $this->cardSize = $cardSize;
        $this->hasPhoto = $hasPhoto;
    }

    public function getLocale(): Locale
    {
        return $this->locale;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getAuthor(): Profile
    {
        return $this->author;
    }

    public function getEventType(): EventType
    {
        return $this->eventType;
    }

    public function getStyle(): InviteCardStyle
    {
        return $this->style;
    }

    public function getGamma(): InviteCardGamma
    {
        return $this->gamma;
    }

    public function getCardSize(): InviteCardSize
    {
        return $this->cardSize;
    }

    public function hasPhoto(): bool
    {
        return $this->hasPhoto;
    }
}