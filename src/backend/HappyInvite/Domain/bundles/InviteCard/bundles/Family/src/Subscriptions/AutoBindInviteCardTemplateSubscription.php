<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Subscriptions;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\CreateInviteCardTemplateParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoBindInviteCardTemplateSubscription implements SubscriptionScript
{
    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var InviteCardFamilyService */
    private $inviteCardFamilyService;

    public function __construct(
        InviteCardTemplateService $inviteCardTemplateService,
        InviteCardFamilyService $inviteCardFamilyService
    ) {
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->inviteCardFamilyService = $inviteCardFamilyService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->inviteCardTemplateService->getEventEmitter()->on(InviteCardTemplateService::EVENT_CREATED, function(InviteCardTemplate $template, CreateInviteCardTemplateParameters $parameters) {
            if($parameters->shouldBeBindedToFamily()) {
                $this->inviteCardFamilyService->bindTemplate($template->getFamily()->getId(), $template);
            }
        });
    }
}