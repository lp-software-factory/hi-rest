<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Factory;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Parameters\InviteCardFamilyParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;

final class InviteCardFamilyParametersFactory
{
    /** @var LocaleService */
    private $localeService;

    /** @var ProfileService */
    private $profileService;

    /** @var \HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService */
    private $eventTypeService;

    /** @var InviteCardStyleService */
    private $styleService;

    /** @var InviteCardGammaService */
    private $gammaService;

    /** @var InviteCardSizeService */
    private $cardSizeService;

    public function __construct(
        LocaleService $localeService,
        ProfileService $profileService,
        EventTypeService $eventTypeService,
        InviteCardStyleService $styleService,
        InviteCardGammaService $gammaService,
        InviteCardSizeService $cardSizeService
    ) {
        $this->localeService = $localeService;
        $this->profileService = $profileService;
        $this->eventTypeService = $eventTypeService;
        $this->styleService = $styleService;
        $this->gammaService = $gammaService;
        $this->cardSizeService = $cardSizeService;
    }

    public function createInviteCardFamilyParameters(array $input): InviteCardFamilyParameters
    {
        return new InviteCardFamilyParameters(
            $this->localeService->getLocaleById($input['locale_id']),
            new ImmutableLocalizedString($input['title']),
            new ImmutableLocalizedString($input['description']),
            $this->profileService->getProfileById($input['author_id']),
            $this->eventTypeService->getEventTypeById($input['event_type_id']),
            $this->styleService->getById($input['style_id']),
            $this->gammaService->getById($input['gamma_id']),
            $this->cardSizeService->getById($input['card_size_id']),
            $input['has_photo']
        );
    }
}