<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;

final class InviteCardFamilyRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return InviteCardFamily::class;
    }
}