<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions;

final class InviteCardFamilyNotFoundException extends \Exception {}