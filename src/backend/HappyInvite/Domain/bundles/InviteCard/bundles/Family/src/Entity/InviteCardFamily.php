<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Repository\InviteCardFamilyRepository")
 * @Table(name="invite_card_family")
 */
class InviteCardFamily implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, SerialEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, SerialEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ActivatedEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Locale\Entity\Locale")
     * @JoinColumn(name="locale_id", referencedColumnName="id")
     * @var Locale
     */
    private $locale;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamilyTemplatesEQ", mappedBy="inviteCardFamily", cascade={"all"})
     * @var Collection
     */
    private $templates;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     * @var Profile
     */
    private $author;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType")
     * @JoinColumn(name="event_type_id", referencedColumnName="id")
     * @var EventType
     */
    private $eventType;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle")
     * @JoinColumn(name="style_id", referencedColumnName="id")
     * @var InviteCardStyle
     */
    private $style;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma")
     * @JoinColumn(name="gamma_id", referencedColumnName="id")
     * @var InviteCardGamma
     */
    private $gamma;

    /**
     * @Column(name="has_photo", type="boolean")
     * @var bool
     */
    private $hasPhoto;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize")
     * @JoinColumn(name="card_size_id", referencedColumnName="id")
     * @var InviteCardSize
     */
    private $cardSize;

    public function __construct(
        Locale $locale,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        Profile $author,
        \HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType $eventType,
        InviteCardStyle $style,
        InviteCardGamma $gamma,
        InviteCardSize $cardSize,
        bool $hasPhoto
    )
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->locale = $locale;
        $this->title = $title;
        $this->description = $description;
        $this->author = $author;
        $this->eventType = $eventType;
        $this->hasPhoto = $hasPhoto;
        $this->gamma = $gamma;
        $this->style = $style;
        $this->cardSize = $cardSize;
        $this->templates = new ArrayCollection();

        $this->initModificationEntity();
        $this->regenerateSID();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'sid' => $this->getSID(),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'locale' => $this->getLocale()->toJSON($options),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'author' => $this->getAuthor()->toJSON($options),
            'event_type' => $this->getEventType()->toJSON($options),
            'gamma' => $this->getGamma()->toJSON($options),
            'style' => $this->getStyle()->toJSON($options),
            'card_size' => $this->getCardSize()->toJSON($options),
            'has_photo' => $this->hasPhoto(),
            'position' => $this->getPosition(),
            'is_activated' => $this->isActivated(),
            'template_ids' => $this->templates
                ->map(function(InviteCardFamilyTemplatesEQ $eq) {
                    return $eq->getInviteCardTemplate()->getId();
                })
                ->toArray(),
        ];
    }

    public function addTemplate(InviteCardTemplate $template): InviteCardFamilyTemplatesEQ
    {
        $this->templates->add(
            $eq = new InviteCardFamilyTemplatesEQ($template, $this)
        );

        return $eq;
    }

    public function removeTemplate(InviteCardTemplate $template): bool
    {
        $eqs = $this->templates->filter(function(InviteCardFamilyTemplatesEQ $eq) use ($template) {
            return $eq->getInviteCardTemplate()->getId() === $template->getId();
        });

        if($eqs->count()) {
            foreach($eqs as $eq) {
                $this->templates->removeElement($eq);
            }

            return true;
        }else{
            return false;
        }
    }

    public function getTemplates(): array
    {
        return $this->templates->toArray();
    }

    public function hasTemplateWithId(int $templateId): bool
    {
        return $this->templates
            ->filter(function(InviteCardFamilyTemplatesEQ $eq) use ($templateId) {
                return $eq->getInviteCardTemplate()->getId() === $templateId;
            })
            ->count() !== 0;
    }

    public function getLocale(): Locale
    {
        return $this->locale;
    }

    public function setLocale(Locale $locale): self
    {
        $this->locale = $locale;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getAuthor(): Profile
    {
        return $this->author;
    }

    public function setAuthor(Profile $author): self
    {
        $this->author = $author;
        $this->markAsUpdated();

        return $this;
    }

    public function getEventType(): \HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType
    {
        return $this->eventType;
    }

    public function setEventType(EventType $eventType): self
    {
        $this->eventType = $eventType;
        $this->markAsUpdated();

        return $this;
    }

    public function hasPhoto(): bool
    {
        return $this->hasPhoto;
    }

    public function setHasPhoto(bool $hasPhoto): self
    {
        $this->hasPhoto = $hasPhoto;
        $this->markAsUpdated();

        return $this;
    }

    public function getStyle(): InviteCardStyle
    {
        return $this->style;
    }

    public function setStyle(InviteCardStyle $style): self
    {
        $this->style = $style;
        $this->markAsUpdated();

        return $this;
    }

    public function getGamma(): InviteCardGamma
    {
        return $this->gamma;
    }

    public function setGamma(InviteCardGamma $gamma): self
    {
        $this->gamma = $gamma;
        $this->markAsUpdated();

        return $this;
    }

    public function getCardSize(): InviteCardSize
    {
        return $this->cardSize;
    }

    public function setCardSize(InviteCardSize $cardSize): self
    {
        $this->cardSize = $cardSize;
        $this->markAsUpdated();

        return $this;
    }
}