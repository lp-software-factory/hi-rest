<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;

/**
 * @Entity
 * @Table(name="invite_card_family_templates_eq")
 */
class InviteCardFamilyTemplatesEQ implements IdEntity
{
    use IdEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate")
     * @JoinColumn(name="invite_card_template_id", referencedColumnName="id")
     * @var InviteCardTemplate
     */
    private $inviteCardTemplate;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily")
     * @JoinColumn(name="invite_card_family_id", referencedColumnName="id")
     * @var InviteCardFamily
     */
    private $inviteCardFamily;

    /**
     * InviteCardFamilyTemplatesEQ constructor.
     * @param InviteCardTemplate $inviteCardTemplate
     * @param InviteCardFamily $inviteCardFamily
     */
    public function __construct(InviteCardTemplate $inviteCardTemplate, InviteCardFamily $inviteCardFamily)
    {
        $this->inviteCardTemplate = $inviteCardTemplate;
        $this->inviteCardFamily = $inviteCardFamily;
    }

    public function getInviteCardTemplate(): InviteCardTemplate
    {
        return $this->inviteCardTemplate;
    }

    public function getInviteCardFamily(): InviteCardFamily
    {
        return $this->inviteCardFamily;
    }
}