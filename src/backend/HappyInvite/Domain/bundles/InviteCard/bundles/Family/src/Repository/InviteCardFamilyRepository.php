<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Exceptions\InviteCardFamilyNotFoundException;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class InviteCardFamilyRepository extends EntityRepository
{
    public function createFamily(InviteCardFamily $family): int
    {
        while($this->hasWithSID($family->getSID())) {
            $family->regenerateSID();
        }

        $this->getEntityManager()->persist($family);
        $this->getEntityManager()->flush($family);

        return $family->getId();
    }

    public function saveFamily(InviteCardFamily $family)
    {
        $this->getEntityManager()->flush($family);
    }

    public function saveFamilyEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveFamily($entity);
        }
    }

    public function deleteFamily(InviteCardFamily $family)
    {
        $this->getEntityManager()->remove($family);
        $this->getEntityManager()->flush($family);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getAllOfLocale(Locale  $locale): array
    {
        return $this->findBy([
            'locale' => $locale
        ]);
    }

    public function getById(int $id): InviteCardFamily
    {
        $result = $this->find($id);

        if($result instanceof InviteCardFamily) {
            return $result;
        }else{
            throw new InviteCardFamilyNotFoundException(sprintf('Family `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new \Exception('Families with IDs (%s) not found', array_diff($ids, array_map(function(InviteCardFamily $family) {
                return $family->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): InviteCardFamily
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if($result instanceof InviteCardFamily) {
            return $result;
        }else{
            throw new InviteCardFamilyNotFoundException(sprintf('Family `SID(%s)` not found', $sid));
        }
    }    
}