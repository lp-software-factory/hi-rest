<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Subscriptions;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoUnBindInviteCardTemplateSubscription implements SubscriptionScript
{
    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var InviteCardFamilyService */
    private $inviteCardFamilyService;

    public function __construct(
        InviteCardTemplateService $inviteCardTemplateService,
        InviteCardFamilyService $inviteCardFamilyService
    ) {
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->inviteCardFamilyService = $inviteCardFamilyService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->inviteCardTemplateService->getEventEmitter()->on(InviteCardTemplateService::EVENT_DELETE, function(InviteCardTemplate $template) {
            $family = $this->inviteCardFamilyService->getById($template->getFamily()->getId());

            if($family->hasTemplateWithId($template->getId())) {
                $this->inviteCardFamilyService->unbindTemplate($template->getFamily()->getId(), $template);
            }
        });
    }
}