<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Parameters\InviteCardFamilyParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Repository\InviteCardFamilyRepository;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class InviteCardFamilyService
{
    public const EVENT_CREATE = 'hi.domain.invite-card.family.create';
    public const EVENT_TEMPLATE_BIND = 'hi.domain.invite-card.family.template.bind';
    public const EVENT_TEMPLATE_UNBIND = 'hi.domain.invite-card.family.template.unbind';
    public const EVENT_EDIT = 'hi.domain.invite-card.family.edit';
    public const EVENT_DELETE = 'hi.domain.invite-card.family.delete';
    public const EVENT_DELETED = 'hi.domain.invite-card.family.deleted';
    public const EVENT_ACTIVATE = 'hi.domain.invite-card.family.activate';
    public const EVENT_DEACTIVATE = 'hi.domain.invite-card.family.deactivate';
    public const EVENT_MOVE_UP = 'hi.domain.invite-card.family.move.up';
    public const EVENT_MOVE_DOWN = 'hi.domain.invite-card.family.move.down';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardFamilyRepository */
    private $familyRepository;

    public function __construct(InviteCardFamilyRepository $familyRepository)
    {
        $this->eventEmitter = new EventEmitter();
        $this->familyRepository = $familyRepository;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function bindTemplate(int $familyId, InviteCardTemplate $template): void
    {
        $family = $this->getById($familyId);
        $family->addTemplate($template);

        $this->familyRepository->saveFamily($family);

        $this->getEventEmitter()->emit(self::EVENT_TEMPLATE_BIND, [$template, $family]);
    }

    public function unbindTemplate(int $familyId, InviteCardTemplate $template): void
    {
        $family = $this->getById($familyId);
        $family->removeTemplate($template);

        $this->familyRepository->saveFamily($family);

        $this->getEventEmitter()->emit(self::EVENT_TEMPLATE_UNBIND, [$template, $family]);
    }

    public function createFamily(InviteCardFamilyParameters $parameters): InviteCardFamily
    {
        $family = new InviteCardFamily(
            $parameters->getLocale(),
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getAuthor(),
            $parameters->getEventType(),
            $parameters->getStyle(),
            $parameters->getGamma(),
            $parameters->getCardSize(),
            $parameters->hasPhoto()
        );

        $this->familyRepository->createFamily($family);

        $serial = new SerialManager($this->getAllOfLocale($family->getLocale()));
        $serial->insertLast($family);
        $serial->normalize();

        $this->getEventEmitter()->emit(self::EVENT_CREATE, [$family]);

        return $family;
    }

    public function editFamily(int $familyId, InviteCardFamilyParameters $parameters): InviteCardFamily
    {
        $family = $this->familyRepository->getById($familyId)
            ->setLocale($parameters->getLocale())
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription())
            ->setAuthor($parameters->getAuthor())
            ->setEventType($parameters->getEventType())
            ->setCardSize($parameters->getCardSize())
            ->setStyle($parameters->getStyle())
            ->setGamma($parameters->getGamma())
            ->setHasPhoto($parameters->hasPhoto());

        $this->familyRepository->saveFamily($family);

        $this->getEventEmitter()->emit(self::EVENT_EDIT, [$family]);

        return $family;
    }

    public function isActivated(int $familyId): bool
    {
        return $this->getById($familyId)->isActivated();
    }

    public function activateFamily(int $familyId): bool
    {
        $family = $this->getById($familyId);

        if(! $this->isActivated($familyId)) {
            $family->activate();

            $this->familyRepository->saveFamily($family);
            $this->getEventEmitter()->emit(self::EVENT_ACTIVATE, [$family]);

            return true;
        }else{
            return false;
        }
    }

    public function deactivateFamily(int $familyId): bool
    {
        $family = $this->getById($familyId);

        if($this->isActivated($familyId)) {
            $family->deactivate();

            $this->familyRepository->saveFamily($family);
            $this->getEventEmitter()->emit(self::EVENT_DEACTIVATE, [$family]);

            return true;
        }else{
            return false;
        }
    }

    public function deleteFamily(int $familyId)
    {
        $family = $this->getById($familyId);

        $this->getEventEmitter()->emit(self::EVENT_DELETE, [$familyId, $family]);

        $this->familyRepository->deleteFamily(
            $this->getById($familyId)
        );

        $this->getEventEmitter()->emit(self::EVENT_DELETED, [$familyId, $family]);
    }

    public function moveUpFamily(int $familyId): int
    {
        $family = $this->getById($familyId);
        $siblings = $this->getAllOfLocale($family->getLocale());

        $serial = new SerialManager($siblings);
        $serial->up($family);
        $serial->normalize();
        $this->familyRepository->saveFamilyEntities($siblings);

        $this->getEventEmitter()->emit(self::EVENT_MOVE_UP, [$family]);

        return $family->getPosition();
    }

    public function moveDownFamily(int $familyId): int
    {
        $family = $this->getById($familyId);
        $siblings = $this->getAllOfLocale($family->getLocale());

        $serial = new SerialManager($siblings);
        $serial->down($family);
        $serial->normalize();
        $this->familyRepository->saveFamilyEntities($siblings);

        $this->getEventEmitter()->emit(self::EVENT_MOVE_DOWN, [$family]);

        return $family->getPosition();
    }

    public function getById(int $familyId): InviteCardFamily
    {
        return $this->familyRepository->getById($familyId);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        return $this->familyRepository->getByIds($idsCriteria);
    }

    public function getAll(): array
    {
        return $this->familyRepository->getAll();
    }

    public function getAllOfLocale(Locale $locale)
    {
        return $this->familyRepository->getAllOfLocale($locale);
    }
}