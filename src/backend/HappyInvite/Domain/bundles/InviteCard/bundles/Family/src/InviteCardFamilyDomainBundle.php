<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Family;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Subscriptions\AutoBindInviteCardTemplateSubscription;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Subscriptions\AutoUnBindInviteCardTemplateSubscription;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class InviteCardFamilyDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
            AutoBindInviteCardTemplateSubscription::class,
            AutoUnBindInviteCardTemplateSubscription::class,
        ];
    }
}