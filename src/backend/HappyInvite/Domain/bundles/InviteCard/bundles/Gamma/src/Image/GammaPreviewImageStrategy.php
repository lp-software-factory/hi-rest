<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Image;

use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntity;
use HappyInvite\Domain\Bundles\Avatar\Strategy\ImageStrategy;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use League\Flysystem\FilesystemInterface;

final class GammaPreviewImageStrategy implements ImageStrategy
{
    /** @var InviteCardGamma */
    private $inviteCardGamma;

    /** @var FilesystemInterface */
    private $fileSystem;

    /** @var string */
    private $wwwPath;

    public function __construct(InviteCardGamma $inviteCardGamma, FilesystemInterface $fileSystem, string $wwwPath)
    {
        $this->inviteCardGamma = $inviteCardGamma;
        $this->fileSystem = $fileSystem;
        $this->wwwPath = $wwwPath;
    }

    public function getEntity(): ImageEntity
    {
        return $this->inviteCardGamma;
    }

    public function getEntityId(): string
    {
        return $this->inviteCardGamma->getIdNoFall();
    }

    public function getLetter(): string
    {
        $title = $this->inviteCardGamma->getTitle();

        if($title->countTranslation()) {
            $definitions = $title->getDefinition();
            $definition = array_pop($definitions);

            return $definition['value'];
        }else{
            return 'T';
        }
    }

    public function getFilesystem(): FilesystemInterface
    {
        return $this->fileSystem;
    }

    public function getPublicPath(): string
    {
        return $this->wwwPath;
    }

    public function getDefaultSize(): int
    {
        return 64;
    }

    public function getSizes(): array
    {
        return [128, 64, 48, 32];
    }

    public function getRatio(): float
    {
        return 1;
    }

    public function validate(\Intervention\Image\Image $origImage)
    {
        return true;
    }
}