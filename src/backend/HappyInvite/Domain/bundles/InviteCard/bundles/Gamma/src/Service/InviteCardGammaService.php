<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Parameters\CreateInviteCardGammaParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Parameters\EditInviteCardGammaParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Repository\InviteCardGammaRepository;
use HappyInvite\Domain\Markers\SerialEntity\SerialManager;

final class InviteCardGammaService
{
    public const EVENT_CREATED = 'hi.domain.invite-card.gamma.created';
    public const EVENT_EDITED = 'hi.domain.invite-card.gamma.edited';
    public const EVENT_UPDATED = 'hi.domain.invite-card.gamma.updated';
    public const EVENT_DELETE = 'hi.domain.invite-card.gamma.delete.before';
    public const EVENT_DELETED = 'hi.domain.invite-card.gamma.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardGammaRepository */
    private $gammaRepository;

    public function __construct(InviteCardGammaRepository $inviteCardGammaRepository)
    {
        $this->gammaRepository = $inviteCardGammaRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createGamma(CreateInviteCardGammaParameters $parameters): InviteCardGamma
    {
        $gamma = new InviteCardGamma(
            $parameters->getTitle(),
            $parameters->getDescription()
        );

        $this->gammaRepository->createGamma($gamma);

        $serial = new SerialManager($this->getAll());
        $serial->insertLast($gamma);
        $serial->normalize();

        $this->eventEmitter->emit(self::EVENT_CREATED, [$gamma]);

        return $gamma;
    }

    public function editGamma(int $gammaId, EditInviteCardGammaParameters $parameters): InviteCardGamma
    {
        $gamma = $this->gammaRepository->getById($gammaId)
            ->setTitle($parameters->getTitle())
            ->setDescription($parameters->getDescription());

        $this->gammaRepository->saveGamma($gamma);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$gamma]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$gamma]);

        return $gamma;
    }

    public function updateActivationStatusOf(InviteCardGamma $gamma): InviteCardGamma
    {
        $this->gammaRepository->saveGamma($gamma);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$gamma]);

        return $gamma;
    }

    public function updateImageOf(InviteCardGamma $gamma): InviteCardGamma
    {
        $this->gammaRepository->saveGamma($gamma);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$gamma]);

        return $gamma;
    }

    public function deleteGamma(int $gammaId)
    {
        $gamma = $this->getById($gammaId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$gamma]);
        $this->gammaRepository->deleteGamma($gamma);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$gammaId, $gamma]);
    }

    public function moveUpGamma(int $gammaId): int
    {
        $gamma = $this->getById($gammaId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->up($gamma);
        $serial->normalize();
        $this->gammaRepository->saveGammaEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_UPDATED, [$gamma]);

        return $gamma->getPosition();
    }

    public function moveDownGamma(int $gammaId): int
    {
        $gamma = $this->getById($gammaId);
        $siblings = $this->getAll();

        $serial = new SerialManager($siblings);
        $serial->down($gamma);
        $serial->normalize();
        $this->gammaRepository->saveGammaEntities($siblings);

        $this->eventEmitter->emit(self::EVENT_UPDATED, [$gamma]);

        return $gamma->getPosition();
    }

    public function getById(int $gammaId): InviteCardGamma
    {
        return $this->gammaRepository->getById($gammaId);
    }

    public function getAll(): array
    {
        return $this->gammaRepository->getAll();
    }
}