<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma;

return [
    'config.hi.domain.invite-card.bundles.gamma.image.dir' => 'invite-card/gamma/image',
    'config.hi.domain.invite-card.bundles.gamma.image.www' => 'invite-card/gamma/image',
];