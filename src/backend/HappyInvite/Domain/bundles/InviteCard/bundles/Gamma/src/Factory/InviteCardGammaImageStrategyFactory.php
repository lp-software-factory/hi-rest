<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Factory;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Config\InviteCardGammaImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Image\GammaPreviewImageStrategy;

final class InviteCardGammaImageStrategyFactory
{
    /** @var InviteCardGammaImageConfig */
    private $config;

    public function __construct(InviteCardGammaImageConfig $config)
    {
        $this->config = $config;
    }

    public function createStrategyFor(InviteCardGamma $inviteCardGamma): GammaPreviewImageStrategy
    {
        return new GammaPreviewImageStrategy(
            $inviteCardGamma,
            $this->config->getFileSystem(),
            $this->config->getWwwPath()
        );
    }
}