<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;

final class InviteCardGammaActivationService
{
    public const EVENT_ACTIVATED = 'hi.domain.invite-card.gamma.activated';
    public const EVENT_DEACTIVATED = 'hi.domain.invite-card.gamma.deactivated';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var InviteCardGammaService */
    private $gammaService;

    public function __construct(InviteCardGammaService $gammaService)
    {
        $this->eventEmitter = new EventEmitter();
        $this->gammaService = $gammaService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function activateGamma(int $gammaId): InviteCardGamma
    {
        $gamma = $this->gammaService->getById($gammaId);

        if(! $gamma->isActivated()) {
            $gamma->activate();

            $this->eventEmitter->emit(self::EVENT_ACTIVATED, [$gamma]);
            $this->gammaService->updateActivationStatusOf($gamma);
        }

        return $gamma;
    }

    public function deactivateGamma(int $gammaId): InviteCardGamma
    {
        $gamma = $this->gammaService->getById($gammaId);

        if($gamma->isActivated()) {
            $gamma->deactivate();

            $this->eventEmitter->emit(self::EVENT_DEACTIVATED, [$gamma]);
            $this->gammaService->updateActivationStatusOf($gamma);
        }

        return $gamma;
    }

    public function isGammaActivated(int $gammaId): bool
    {
        return $this->gammaService->getById($gammaId)->isActivated();
    }
}