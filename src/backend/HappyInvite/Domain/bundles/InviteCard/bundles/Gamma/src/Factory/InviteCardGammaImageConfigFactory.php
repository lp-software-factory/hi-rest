<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Factory;

use HappyInvite\Domain\Bundles\Attachment\Config\AttachmentConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Config\InviteCardGammaImageConfig;
use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\Service\EnvironmentService;
use Interop\Container\ContainerInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;

final class InviteCardGammaImageConfigFactory
{
    public function __invoke(ContainerInterface $container, AttachmentConfig $config, EnvironmentService $environmentService)
    {
        $www = sprintf('%s/%s', $config->getWww(), $container->get('config.hi.domain.invite-card.bundles.gamma.image.www'));
        $dir = $dir = sprintf('%s/%s', $config->getDir(), $container->get('config.hi.domain.invite-card.bundles.gamma.image.dir'));

        if($environmentService->getCurrent() === Environment::TESTING) {
            $adapter = new MemoryAdapter();
        }else{
            $adapter = new Local($dir);
        }

        return new InviteCardGammaImageConfig($www, new Filesystem($adapter));
    }
}