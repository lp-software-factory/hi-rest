<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Exceptions\InviteCardGammaNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class InviteCardGammaRepository extends EntityRepository
{
    public function createGamma(InviteCardGamma $gamma): int
    {
        while ($this->hasWithSID($gamma->getSID())) {
            $gamma->regenerateSID();
        }

        $this->getEntityManager()->persist($gamma);
        $this->getEntityManager()->flush($gamma);

        return $gamma->getId();
    }

    public function saveGamma(InviteCardGamma $gamma)
    {
        $this->getEntityManager()->flush($gamma);
    }

    public function saveGammaEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveGamma($entity);
        }
    }

    public function deleteGamma(InviteCardGamma $gamma)
    {
        $this->getEntityManager()->remove($gamma);
        $this->getEntityManager()->flush($gamma);
    }

    public function listGammas(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): InviteCardGamma
    {
        $result = $this->find($id);

        if ($result instanceof InviteCardGamma) {
            return $result;
        } else {
            throw new InviteCardGammaNotFoundException(sprintf('Gamma `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Gammas with IDs (%s) not found', array_diff($ids, array_map(function (InviteCardGamma $gamma) {
                return $gamma->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): InviteCardGamma
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof InviteCardGamma) {
            return $result;
        } else {
            throw new InviteCardGammaNotFoundException(sprintf('Gamma `SID(%s)` not found', $sid));
        }
    }
}