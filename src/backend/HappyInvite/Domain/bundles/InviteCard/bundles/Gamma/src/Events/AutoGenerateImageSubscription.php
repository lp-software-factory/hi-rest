<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Events;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaImageService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class AutoGenerateImageSubscription implements SubscriptionScript
{
    /** @var InviteCardGammaService */
    private $gammaService;

    /** @var InviteCardGammaImageService */
    private $imageService;

    public function __construct(InviteCardGammaService $gammaService, InviteCardGammaImageService $imageService)
    {
        $this->gammaService = $gammaService;
        $this->imageService = $imageService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $ts = $this->gammaService;
        $is = $this->imageService;

        $ts->getEventEmitter()->on(InviteCardGammaService::EVENT_CREATED, function(InviteCardGamma  $gamma) use ($is) {
            $is->generateImage($gamma->getId());
        });
    }
}