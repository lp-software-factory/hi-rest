<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Config\InviteCardGammaImageConfig;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Factory\Doctrine\InviteCardGammaRepositoryFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Factory\InviteCardGammaImageConfigFactory;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Repository\InviteCardGammaRepository;

return [
    InviteCardGammaRepository::class => factory(InviteCardGammaRepositoryFactory::class),
    InviteCardGammaImageConfig::class => factory(InviteCardGammaImageConfigFactory::class),
];