<?php
namespace HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Exceptions;

final class InviteCardGammaNotFoundException extends \Exception {}