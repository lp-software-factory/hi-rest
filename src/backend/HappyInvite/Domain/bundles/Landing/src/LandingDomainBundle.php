<?php
namespace HappyInvite\Domain\Bundles\Landing;

use HappyInvite\Platform\Bundles\HIBundle;

final class LandingDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}