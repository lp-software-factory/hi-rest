<?php
namespace HappyInvite\Domain\Bundles\Landing\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Landing\Parameters\CreateLandingParameters;
use HappyInvite\Domain\Bundles\Landing\Parameters\EditLandingParameters;
use HappyInvite\Domain\Bundles\Landing\Service\LandingService;
use HappyInvite\REST\Bundles\Landing\Middleware\Command\EditLandingCommand;
use HappyInvite\REST\Bundles\Landing\Request\CreateLandingRequest;
use HappyInvite\REST\Bundles\Landing\Request\EditLandingRequest;
use Psr\Http\Message\ServerRequestInterface;

final class LandingParametersFactory
{
    /** @var LandingService */
    private $landingService;

    public function __construct (
        LandingService $landingService
    )
    {
        $this->landingService = $landingService;
    }

    public function createLandingParameters(ServerRequestInterface $request): CreateLandingParameters
    {
        $json = (new CreateLandingRequest($request))->getParameters();

        return new CreateLandingParameters(
            $json['title'],
            $json['description'],
            $json['definition'],
            $json['type']
        );
    }

    public function editLandingParameters(ServerRequestInterface $request): EditLandingParameters
    {
        $json = (new EditLandingRequest($request))->getParameters();

        return new EditLandingParameters(
            $json['title'],
            $json['description'],
            $json['definition'],
            $json['type']
        );
    }
}