<?php
namespace HappyInvite\Domain\Bundles\Landing\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Landing\Entity\Landing;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class LandingRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Landing::class;
    }
}