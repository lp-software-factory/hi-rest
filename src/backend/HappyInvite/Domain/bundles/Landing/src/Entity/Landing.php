<?php
namespace HappyInvite\Domain\Bundles\Landing\Entity;

use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SelectedEntity\SelectedEntity;
use HappyInvite\Domain\Markers\SelectedEntity\SelectedEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Landing\Repository\LandingRepository")
 * @Table(name="landing")
 */
class Landing implements JSONSerializable, IdEntity , SIDEntity , ModificationEntity, JSONMetadataEntity, VersionEntity, SelectedEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, SelectedEntityTrait;

    /**
     * @Column(type="i18n", name="title")
     * @var string
     */
    private $title;

    /**
     * @Column(type="i18n", name="description")
     * @var string
     */
    private $description;

    /**
     * @Column(type="json_array", name="definition")
     * @var array
     */
    private $definition = [];

    /**
     * @Column(type="string", name="type")
     * @var string
     */
    private $type;

    public function __construct(string $title, string $description, string $definition, string $type)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->description = $description;
        $this->definition = $definition;
        $this->type = $type;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        $result = [
            'id' => $this->getId(),
            'sid' => $this->getSID(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'definition' => $this->getDefinition(),
            'type' => $this->getType()
        ];

        return $result;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle (string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription (string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDefinition(): string
    {
        return $this->definition;
    }

    public function setDefinition (string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType (string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function setSelect(Landing $landing): self
    {
       $landing->select();

        return $landing;
    }

}