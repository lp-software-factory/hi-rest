<?php
namespace HappyInvite\Domain\Bundles\Landing\Exceptions;

final class LandingNotFoundException extends \Exception {}