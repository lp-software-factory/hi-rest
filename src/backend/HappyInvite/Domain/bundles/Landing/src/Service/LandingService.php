<?php
namespace HappyInvite\Domain\Bundles\Landing\Service;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Landing\Criteria\ListLandingsCriteria;
use HappyInvite\Domain\Bundles\Landing\Entity\Landing;
use HappyInvite\Domain\Bundles\Landing\Parameters\CreateLandingParameters;
use HappyInvite\Domain\Bundles\Landing\Parameters\EditLandingParameters;
use HappyInvite\Domain\Bundles\Landing\Repository\LandingRepository;
use HappyInvite\Domain\Markers\SelectedEntity\SelectedEntity;
use HappyInvite\REST\Bundles\Landing\Tests\Fixtures\LandingsFixture;


final class LandingService
{
    /** @var LandingRepository */
    private $landingRepository;

    public function __construct(LandingRepository $landingRepository)
    {
        $this->landingRepository = $landingRepository;
    }

    public function createLanding (CreateLandingParameters $parameters) : Landing
    {
        $landing = new Landing(
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getDefinition(),
            $parameters->getType()
        );

        $this->landingRepository->createLanding($landing);

        return $landing;
    }

    public function editLanding(int $landingId, EditLandingParameters $parameters): Landing
    {
        $landing = $this->landingRepository->getById($landingId);
        $landing->setTitle($parameters->getTitle());
        $landing->setDescription($parameters->getDescription());
        $landing->setDefinition($parameters->getDefinition());
        $landing->setType($parameters->getDescription());

        $this->landingRepository->saveLanding($landing);

        return $landing;
    }

    public function deleteLanding(int $landingId)
    {
        $this->landingRepository->deleteLanding(
            $this->landingRepository->getById($landingId)
        );
    }

    public function listLandings(ListLandingsCriteria $criteria): array
    {
        return $this->landingRepository->listLandings($criteria);
    }

    public function countLandings(ListLandingsCriteria $criteria): int
    {
        return $this->landingRepository->countLandings($criteria);
    }

    public function getLandingById(int $landingId): Landing
    {
        return $this->landingRepository->getById($landingId);
    }

    public function getLandingsByType(array $type): Landing
    {
        return $this->landingRepository->getLandingsByType($type);
    }

    public function selectLanding(int $id): Landing
    {
        $selectLanding = $this->landingRepository->getById($id);
        $allLanding = $this->landingRepository->getAllLandings();

        foreach ($allLanding as $landing)
        {

            if ($landing instanceof Landing)
            {
                $landing->blur();

            }

        }
            $selectLanding->select();

            return $selectLanding;
    }

/*
    public function selectLanding(int $landingId): Landing
    {
        $all = $this->landingRepository->getAllLandings();
        $selected = $this->landingRepository->getById($landingId);

        return Chain::create($all)
            ->filter(function(Landing $landing) use ($selected) {
                return $landing->getType() === $selected->getType();
            })
            ->map(function(Landing $landing) use ($selected) {
                $landing === $selected
                    ? $landing->select()
                    : $landing->blur();
            })
            ->filter(function(SelectedEntity $landing) {
                return $landing->isSelected();
            })
            ->array[0];
    }
*/
}