<?php
namespace HappyInvite\Domain\Bundles\Landing\Parameters;

final class EditLandingParameters
{
    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var string */
    private $definition;

    /** @var string */
    private $type;


    public function __construct(string $title, string $description,string $definition, string $type)
    {
        $this->title = $title;
        $this->description = $description;
        $this->definition = $definition;
        $this->type = $type;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getDefinition(): string
    {
        return $this->definition;
    }

    public function getType(): string
    {
        return $this->type;
    }

}