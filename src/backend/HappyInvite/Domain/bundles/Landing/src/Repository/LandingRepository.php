<?php
namespace HappyInvite\Domain\Bundles\Landing\Repository;


use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Landing\Criteria\ListLandingsCriteria;
use HappyInvite\Domain\Bundles\Landing\Entity\Landing;
use HappyInvite\Domain\Bundles\Landing\Exceptions\LandingNotFoundException;


final class LandingRepository extends EntityRepository
{
    public function createLanding (Landing $landing): int
    {
        while($this->hasWithSID($landing->getSID())) {
            $landing->regenerateSID();
        }

        $this->getEntityManager()->persist($landing);
        $this->getEntityManager()->flush($landing);

        return $landing->getId();
    }

    public function saveLanding (Landing $landing)
    {
        $this->getEntityManager()->flush($landing);
    }

    public function deleteLanding(Landing $landing)
    {
        $this->getEntityManager()->remove($landing);
        $this->getEntityManager()->flush($landing);
    }

    public function listLandings(ListLandingsCriteria $criteria): array
    {
        $seek = $criteria->getSeek();

        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());
        $query->orderBy('c.'.$criteria->getSort()->getField(), $criteria->getSort()->getDirection());

        return $query->getQuery()->execute();
    }

    public function countLandings(ListLandingsCriteria $criteria): int
    {
        $query = $this->createQueryBuilder('c');
        $query->select('count(c)');

        return (int) $query->getQuery()->getSingleScalarResult();
    }

    public function getLandingsByType(string $type): array
    {
        return $this->findBy(['type' => $type]);

        /* $result = $this->findBy((['type' => $type]));

        if($result instanceof Landing){
            return $result;
        }else{
            throw new LandingNotFoundException(sprintf('Landing `Type(%d)` not found', $type));
        }
        */
    }

    public function getById(int $id): Landing
    {
        $result = $this->find($id);

        if($result instanceof Landing) {
            return $result;
        }else{
            throw new LandingNotFoundException(sprintf('Landing `ID(%d)` not found', $id));
        }
    }

    public function getAllLandings():array
    {
       return $this->findAll();
    }

    private function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

}