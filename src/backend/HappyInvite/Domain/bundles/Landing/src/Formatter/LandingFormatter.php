<?php
namespace HappyInvite\Domain\Bundles\Landing\Formatter;

use HappyInvite\Domain\Bundles\Landing\Entity\Landing;

final class LandingFormatter
{
    public function format(Landing $landing)
    {
        return $landing->toJSON();
    }

}
