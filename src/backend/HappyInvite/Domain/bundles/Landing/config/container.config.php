<?php
namespace HappyInvite\Domain\Bundles\Landing;

use function DI\object;
use function DI\factory;
use function DI\get;
use HappyInvite\Domain\Bundles\Landing\Factory\Doctrine\LandingRepositoryFactory;
use HappyInvite\Domain\Bundles\Landing\Repository\LandingRepository;

return [
    LandingRepository::class => factory(LandingRepositoryFactory::class),
];