<?php
namespace HappyInvite\Domain\Bundles\Account\Entity;

use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;
use HappyInvite\Domain\Util\GenerateRandomString;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Account\Repository\ResetPasswordRequestRepository")
 * @Table(name="reset_password_request")
 */
class ResetPasswordRequest implements IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity,VersionEntity
{
    public const RESET_PASSWORD_TOKEN_LENGTH = 32;
    public const LATEST_VERSION = '1.0.0';


    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     *@Column(name="accept_token", type="string")
     * @var string
     */
    private $acceptToken;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Account\Entity\Account")
     * @JoinColumn(name="account_id", referencedColumnName="id")
     * @var Account
     */
    private $account;

    public function __construct(Account $account)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);
        $this->acceptToken = GenerateRandomString::generate(12);
        $this->account = $account;

        $this->regenerateSID();
        $this->initModificationEntity();
        $this->acceptToken = GenerateRandomString::generate(self::RESET_PASSWORD_TOKEN_LENGTH);
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getAcceptToken(): string
    {
        return $this->acceptToken;
    }

    public function isAcceptTokenValid(string $token): bool
    {
        return $this->acceptToken === $token;
    }
}