<?php
namespace HappyInvite\Domain\Bundles\Account;

use HappyInvite\Platform\Bundles\HIBundle;

final class AccountBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}