<?php
namespace HappyInvite\Domain\Bundles\Account\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class ResetPasswordRequestRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return ResetPasswordRequest::class;
    }
}