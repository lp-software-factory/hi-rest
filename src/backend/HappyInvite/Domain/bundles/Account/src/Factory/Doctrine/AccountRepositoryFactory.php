<?php
namespace HappyInvite\Domain\Bundles\Account\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class AccountRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Account::class;
    }
}