<?php
namespace HappyInvite\Domain\Bundles\Account\Service;

use HappyInvite\Domain\Bundles\Account\Entity\ResetPasswordRequest;
use HappyInvite\Domain\Bundles\Mail\Service\MailService;

final class AccountMailService
{
    public const MAIL_TYPE_RESET_PASSWORD = 'hi.domain.account.reset-password';

    /** @var MailService */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendResetPasswordEmail(ResetPasswordRequest $request)
    {
        $replaces = [
            '{{ TOKEN }}' => $request->getAcceptToken(),
            '{{ EMAIL }}' => $request->getAccount()->getEmail(),
            '{{ FIRST_NAME }}' => $request->getAccount()->getCurrentProfile()->getFirstName(),
        ];

        $message = \Swift_Message::newInstance()
            ->setTo($request->getAccount()->getEmail())
            ->setContentType('text/html')
            ->setBody($this->mailService->getTemplate('account', 'reset-password', $replaces));

        $this->mailService->sendNoReply($message, self::MAIL_TYPE_RESET_PASSWORD);

        return $message;
    }
}