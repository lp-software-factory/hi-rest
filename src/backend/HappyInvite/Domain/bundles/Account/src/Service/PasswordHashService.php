<?php
namespace HappyInvite\Domain\Bundles\Account\Service;

final class PasswordHashService
{
    public function hash($input): string
    {
        return password_hash($input, PASSWORD_BCRYPT);
    }
}