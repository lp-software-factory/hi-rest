<?php
namespace HappyInvite\Domain\Bundles\Account\Service;

use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Account\Parameters\AccountRegisterParameters;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;

final class AccountRegistrationService
{
    /** @var AccountService */
    private $accountService;

    /** @var ProfileService */
    private $profileService;

    public function __construct(AccountService $accountService, ProfileService $profileService)
    {
        $this->accountService = $accountService;
        $this->profileService = $profileService;
    }

    public function registerUserAccount(AccountRegisterParameters $parameters, array $options = []): Account
    {
        return $this->registerAccount($parameters, ['user'], $options);
    }

    public function registerAdminAccount(AccountRegisterParameters $parameters, array $options = []): Account
    {
        return $this->registerAccount($parameters, ['user', 'admin'], $options);
    }

    public function registerDesignerAccount(AccountRegisterParameters $parameters, array $options = []): Account
    {
        return $this->registerAccount($parameters, ['user', 'designer'], $options);
    }

    private function registerAccount(AccountRegisterParameters $parameters, array $roles, array $options = []): Account
    {
        $account = $this->accountService->createAccount($parameters->getEmail(), $parameters->getPassword(), $roles, $options);
        $profile = $this->profileService->createProfileForAccount($account, $parameters->getCreateProfileParameters());

        return $account;
    }
}