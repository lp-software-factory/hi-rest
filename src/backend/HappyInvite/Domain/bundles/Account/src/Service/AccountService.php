<?php
namespace HappyInvite\Domain\Bundles\Account\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Account\Entity\Account;
use HappyInvite\Domain\Bundles\Account\Repository\AccountRepository;
use HappyInvite\Domain\Bundles\Account\Exceptions\DuplicateAccountException;
use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;

final class AccountService
{
    public const EVENT_CREATE = 'hi.domain.account.create';
    public const EVENT_EDIT = 'hi.domain.account.edit';
    public const EVENT_SET_PHONE = 'hi.domain.account.set.phone';
    public const EVENT_SET_LOCALE = 'hi.domain.account.set.locale';
    public const EVENT_SET_PASSWORD = 'hi.domain.account.set.password';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var AccountRepository */
    private $accountRepository;

    /** @var AccountMailService */
    private $mailService;

    /** @var LocaleService */
    private $localeService;

    /** @var PasswordHashService */
    private $passwordHashService;

    public function __construct(
        AccountRepository $accountRepository,
        AccountMailService $mailService,
        LocaleService $localeService,
        PasswordHashService $passwordHashService
    )
    {
        $this->eventEmitter = new EventEmitter();
        $this->accountRepository = $accountRepository;
        $this->mailService = $mailService;
        $this->localeService = $localeService;
        $this->passwordHashService = $passwordHashService;
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createAccount(string $email, string $password, array $roles, array $options = []): Account
    {
        $options = array_merge([
            'hash_password' => true,
        ], $options);

        if($this->hasAccountWithEmail($email)) {
            throw new DuplicateAccountException(sprintf('Account with email `%s` already exists', $email));
        }

        $account = new Account($email, $roles, $this->localeService->getCurrentLocale());
        $account->setPassword(
            $options['hash_password']
                ? $this->passwordHashService->hash($password)
                : $password
        );

        $this->accountRepository->createAccount($account);

        $this->eventEmitter->emit(self::EVENT_CREATE, [$account]);

        return $account;
    }

    public function saveAccess(Account $account)
    {
        $this->accountRepository->saveAccount($account);
    }

    public function setPhone(Account $account, string $phone)
    {
        $account->changePhone($phone);
        $this->accountRepository->saveAccount($account);

        $this->eventEmitter->emit(self::EVENT_SET_PHONE, [$account]);
    }

    public function setPassword(Account $account, string $newPassword)
    {
        $account->setPassword($this->passwordHashService->hash($newPassword));
        $this->accountRepository->saveAccount($account);

        $this->eventEmitter->emit(self::EVENT_SET_PASSWORD, [$account]);
    }

    public function setLocale(Account $account, string $region): Locale
    {
        $newLocale = $this->localeService->getLocaleByRegion($region);

        $account->setLocale($newLocale);
        $this->accountRepository->saveAccount($account);

        $this->eventEmitter->emit(self::EVENT_SET_LOCALE, [$account]);

        return $newLocale;
    }

    public function getAccountWithId(int $id): Account
    {
        return $this->accountRepository->getById($id);
    }

    public function hasAccountWithEmail(string $email): bool
    {
        return $this->accountRepository->hasAccountWithEmail($email);
    }

    public function getAccountWithEmail(string $email): Account
    {
        return $this->accountRepository->getAccountWithEmail($email);
    }
}
