<?php
namespace HappyInvite\Domain\Bundles\Account\Exceptions;

final class AccountNotFoundException extends \Exception {}