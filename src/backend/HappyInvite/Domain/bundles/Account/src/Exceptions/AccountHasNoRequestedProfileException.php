<?php
namespace HappyInvite\Domain\Bundles\Account\Exceptions;

final class AccountHasNoRequestedProfileException extends \Exception {}