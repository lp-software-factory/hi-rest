<?php
namespace HappyInvite\Domain\Bundles\Account\Exceptions;

final class DuplicateAccountException extends \Exception {}