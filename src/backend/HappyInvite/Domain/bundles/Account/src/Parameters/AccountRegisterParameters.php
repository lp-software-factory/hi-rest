<?php
namespace HappyInvite\Domain\Bundles\Account\Parameters;

use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;

final class AccountRegisterParameters
{
    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /** @var CreateProfileParameters */
    private $createProfileParameters;

    public function __construct(
        string $email,
        string $password,
        CreateProfileParameters $createProfileParameters
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->createProfileParameters = $createProfileParameters;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getCreateProfileParameters(): CreateProfileParameters
    {
        return $this->createProfileParameters;
    }
}