<?php
namespace HappyInvite\Domain\Bundles\Account;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Account\Factory\Doctrine\AccountRepositoryFactory;
use HappyInvite\Domain\Bundles\Account\Factory\Doctrine\ResetPasswordRequestRepositoryFactory;
use HappyInvite\Domain\Bundles\Account\Repository\AccountRepository;
use HappyInvite\Domain\Bundles\Account\Repository\ResetPasswordRequestRepository;

return [
    AccountRepository::class => factory(AccountRepositoryFactory::class),
    ResetPasswordRequestRepository::class => factory(ResetPasswordRequestRepositoryFactory::class),
];