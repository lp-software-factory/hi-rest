<?php
namespace HappyInvite\Domain\Bundles\Account;

use HappyInvite\Domain\Bundles\Account\Console\Command\CreateAdminAccountCommand;

return [
    'config.console' => [
        'commands' => [
            'Version' => [
                CreateAdminAccountCommand::class,
            ],
        ],
    ],
];