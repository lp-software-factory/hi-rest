<?php
namespace HappyInvite\Domain\Bundles\Money;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Money\Factory\Doctrine\CurrencyRepositoryFactory;
use HappyInvite\Domain\Bundles\Money\Factory\Doctrine\PriceRepositoryFactory;
use HappyInvite\Domain\Bundles\Money\Repository\PriceRepository;
use HappyInvite\Domain\Bundles\Money\Repository\CurrencyRepository;

return [
    PriceRepository::class => factory(PriceRepositoryFactory::class),
    CurrencyRepository::class => factory(CurrencyRepositoryFactory::class),
];