<?php
namespace HappyInvite\Domain\Bundles\Money\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\OrderBy;
use HappyInvite\Domain\Bundles\Money\Criteria\ListPriceCriteria;
use HappyInvite\Domain\Bundles\Money\Entity\Price;
use HappyInvite\Domain\Bundles\Money\Exceptions\PriceNotFoundException;
use HappyInvite\Domain\Exceptions\DoNotImplementItException;

final class PriceRepository extends EntityRepository 
{
    public function createPrice(Price $price): int
    {
        $this->getEntityManager()->persist($price);
        $this->getEntityManager()->flush($price);

        return $price->getId();
    }

    public function savePrice(Price $price)
    {
        throw new DoNotImplementItException("You shouldn't be able to manually save changes in price.  Take a look to README.MD");
    }

    public function deletePrice(Price $price)
    {
        throw new DoNotImplementItException("You shouldn't be able to manually delete price.  Take a look to README.MD");
    }

    public function listPrice(ListPriceCriteria $criteria): array
    {
        $query = $this->createQueryBuilder('c');

        $query->setFirstResult($criteria->getSeek()->getOffset());
        $query->setMaxResults($criteria->getSeek()->getLimit());
        $query->orderBy(new OrderBy('c.'.$criteria->getOrderBy(), $criteria->getOrderByDirection()));

        return $query->getQuery()->execute();
    }

    public function getById(int $id): Price
    {
        $result = $this->find($id);

        if($result instanceof Price) {
            return $result;
        }else{
            throw new PriceNotFoundException(sprintf('Price `ID(%d)` not found', $id));
        }
    }

    public function getByCode(string $code): Price
    {
        $result = $this->findOneBy([
            'code' => $code,
        ]);

        if($result instanceof Price) {
            return $result;
        }else{
            throw new PriceNotFoundException(sprintf('Price `code(%s)` not found', $code));
        }
    }
}