<?php
namespace HappyInvite\Domain\Bundles\Money\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;
use HappyInvite\Domain\Bundles\Money\Exceptions\CurrencyNotFoundException;

final class CurrencyRepository extends EntityRepository 
{
    public function createCurrency(Currency $currency): int
    {
        $this->getEntityManager()->persist($currency);
        $this->getEntityManager()->flush($currency);

        return $currency->getId();
    }

    public function saveCurrency(Currency $currency)
    {
        $this->getEntityManager()->flush($currency);
    }

    public function saveCurrencyEntities(array $entities)
    {
        foreach($entities as $entity) {
            $this->saveCurrency($entity);
        }
    }

    public function deleteCurrency(Currency $currency)
    {
        $this->getEntityManager()->remove($currency);
        $this->getEntityManager()->flush($currency);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): Currency
    {
        $result = $this->find($id);

        if($result instanceof Currency) {
            return $result;
        }else{
            throw new CurrencyNotFoundException(sprintf('Currency `ID(%d)` not found', $id));
        }
    }

    public function hasWithCode(string $code): bool
    {
        return $this->findOneBy([
            'code' => $code,
        ]) !== null;
    }

    public function getByCode(string $code): Currency
    {
        $result = $this->findOneBy([
            'code' => $code,
        ]);

        if($result instanceof Currency) {
            return $result;
        }else{
            throw new CurrencyNotFoundException(sprintf('Currency `code(%s)` not found', $code));
        }
    }
}