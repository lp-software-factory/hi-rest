<?php
namespace HappyInvite\Domain\Bundles\Money;

use HappyInvite\Domain\Bundles\Frontend\FrontendBundleInjectable;
use HappyInvite\Domain\Bundles\Money\Frontend\CurrenciesFrontendScript;
use HappyInvite\Platform\Bundles\HIBundle;

final class MoneyDomainBundle extends HIBundle implements FrontendBundleInjectable
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getFrontendScripts(): array
    {
        return [
            CurrenciesFrontendScript::class
        ];
    }
}