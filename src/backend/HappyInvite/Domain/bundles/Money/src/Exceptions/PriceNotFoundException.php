<?php
namespace HappyInvite\Domain\Bundles\Money\Exceptions;

final class PriceNotFoundException extends \Exception {}