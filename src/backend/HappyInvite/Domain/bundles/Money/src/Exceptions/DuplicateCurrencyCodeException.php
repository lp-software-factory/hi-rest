<?php
namespace HappyInvite\Domain\Bundles\Money\Exceptions;

final class DuplicateCurrencyCodeException extends \Exception {}