<?php
namespace HappyInvite\Domain\Bundles\Money\Exceptions;

final class CurrencyNotFoundException extends \Exception {}