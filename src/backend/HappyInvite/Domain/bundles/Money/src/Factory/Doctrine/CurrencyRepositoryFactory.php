<?php
namespace HappyInvite\Domain\Bundles\Money\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;

final class CurrencyRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Currency::class;
    }
}