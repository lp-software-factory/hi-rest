<?php
namespace HappyInvite\Domain\Bundles\Money\Entity;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Money\Repository\CurrencyRepository")
 * @Table(name="currency")
 */
class Currency implements JSONSerializable, IdEntity, ModificationEntity, JSONMetadataEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="code", type="string")
     * @var string
     */
    private $code;

    /**
     * @Column(name="sign", type="string")
     * @var string
     */
    private $sign;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    public function __construct(string $code, string $sign, ImmutableLocalizedString $title)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->code = $code;
        $this->sign = $sign;
        $this->title = $title;

        $this->initModificationEntity();
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'code' => $this->getCode(),
            'sign' =>  $this->getSign(),
            'title' => $this->getTitle()->toJSON($options),
        ];
    }

    public function equals(Currency $compare): bool
    {
        return $compare->getCode() === $this->getCode();
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;
        $this->markAsUpdated();

        return $this;
    }

    public function getSign(): string
    {
        return $this->sign;
    }

    public function setSign(string $sign): self
    {
        $this->sign = $sign;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }
}