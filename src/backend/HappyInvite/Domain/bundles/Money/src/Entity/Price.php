<?php
namespace HappyInvite\Domain\Bundles\Money\Entity;

use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Money\Repository\PriceRepository")
 * @Table(name="price")
 */
class Price implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity,VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait;

    /**
     * @Column(name="amount", type="integer")
     * @var int
     */
    private $amount;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Money\Entity\Currency")
     * @JoinColumn(name="currency_id", referencedColumnName="id")
     * @var Currency
     */
    private $currency;

    public function __construct(int $amount, Currency $currency)
    {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->amount = $amount;
        $this->currency = $currency;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            'id' => $this->getIdNoFall(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'currency' => $this->getCurrency()->toJSON($options),
            'amount' => $this->getAmount(),
        ];
    }

    public function equals(Price $compare): bool
    {
        return $compare->getCurrency()->equals($this->getCurrency())
            && $compare->getAmount() === $this->getAmount();
    }

    public function same(string $currencyCode, int $amount): bool
    {
        return ($this->getCurrency() === $currencyCode)
            && ($this->getAmount() === $amount);
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function explicitOwner(IdEntity $owner)
    {
        $this->metadata['owner_entity'] = get_class($owner);
        $this->metadata['owner_entity_id'] = $owner->getId();
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }
}