<?php
namespace HappyInvite\Domain\Bundles\Money\Frontend;

use HappyInvite\Domain\Bundles\Frontend\FrontendScript;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;
use HappyInvite\Domain\Bundles\Money\Service\CurrencyService;

final class CurrenciesFrontendScript implements FrontendScript
{
    /** @var CurrencyService */
    private $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    public function tags(): array
    {
        return [
            self::TAG_GLOBAL,
        ];
    }

    public function __invoke(): array
    {
        return [
            'currencies' => array_map(function(Currency $currency) {
                return $currency->toJSON();
            }, $this->currencyService->getAll())
        ];
    }
}