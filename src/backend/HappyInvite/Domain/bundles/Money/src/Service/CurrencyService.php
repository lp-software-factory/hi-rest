<?php
namespace HappyInvite\Domain\Bundles\Money\Service;

use HappyInvite\Domain\Bundles\Money\Exceptions\DuplicateCurrencyCodeException;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;
use HappyInvite\Domain\Bundles\Money\Parameters\CreateCurrencyParameters;
use HappyInvite\Domain\Bundles\Money\Parameters\EditCurrencyParameters;
use HappyInvite\Domain\Bundles\Money\Repository\CurrencyRepository;

final class CurrencyService
{
    /** @var CurrencyRepository */
    private $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    public function createCurrency(CreateCurrencyParameters $parameters): Currency
    {
        if($this->currencyRepository->hasWithCode($parameters->getCode())) {
            throw new DuplicateCurrencyCodeException(sprintf('Currency with code `%s` already exists', $parameters->getCode()));
        }

        $currency = new Currency(
            $parameters->getCode(),
            $parameters->getSign(),
            $parameters->getTitle()
        );

        $this->currencyRepository->createCurrency($currency);

        return $currency;
    }

    public function editCurrency(int $currencyId, EditCurrencyParameters $parameters): Currency
    {
        if($this->currencyRepository->hasWithCode($parameters->getCode())) {
            $test = $this->currencyRepository->getByCode($parameters->getCode());

            if($test->getId() !== $currencyId) {
                throw new DuplicateCurrencyCodeException(sprintf('Currency with code `%s` already exists', $parameters->getCode()));
            }
        }

        $currency = $this->currencyRepository->getById($currencyId);
        $currency->setCode($parameters->getCode());
        $currency->setSign($parameters->getSign());
        $currency->setTitle($parameters->getTitle());

        $this->currencyRepository->saveCurrency($currency);

        return $currency;
    }

    public function deleteCurrency(int $currencyId)
    {
        $this->currencyRepository->deleteCurrency(
            $this->getById($currencyId)
        );
    }
    
    public function getById(int $currencyId): Currency
    {
        return $this->currencyRepository->getById($currencyId);
    }

    public function getByCode(string $code): Currency
    {
        return $this->currencyRepository->getByCode($code);
    }

    public function getAll(): array
    {
        return $this->currencyRepository->getAll();
    }
}