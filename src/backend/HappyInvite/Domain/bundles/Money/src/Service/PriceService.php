<?php
namespace HappyInvite\Domain\Bundles\Money\Service;

use HappyInvite\Domain\Bundles\Money\Entity\Price;
use HappyInvite\Domain\Bundles\Money\Repository\PriceRepository;
use HappyInvite\Domain\Bundles\Money\Criteria\ListPriceCriteria;

final class PriceService
{
    /** @var PriceRepository */
    private $priceRepository;

    /** @var CurrencyService */
    private $currencyService;

    public function __construct(
        PriceRepository $priceRepository,
        CurrencyService $currencyService
    ) {
        $this->priceRepository = $priceRepository;
        $this->currencyService = $currencyService;
    }

    public function createPrice(string $currencyCode, int $amount): Price
    {
        $currency = $this->currencyService->getByCode($currencyCode);
        $price = new Price($amount, $currency);

        $this->priceRepository->createPrice($price);

        return $price;
    }

    public function getPrices(ListPriceCriteria $criteria): array
    {
        return $this->priceRepository->listPrice($criteria);
    }
}