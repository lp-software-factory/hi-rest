<?php
namespace HappyInvite\Domain\Bundles\Money\Criteria;

use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class ListPriceCriteria
{
    public static $MAX_LIMIT = 1000;
    public static $ORDER_BY_FIELDS = ['id', 'date_created_at', 'amount'];

    /** @var SeekCriteria */
    private $seek;

    /** @var SortCriteria */
    private $order;

    public function __construct(SeekCriteria $seek, SortCriteria $orderCriteria)
    {
        $orderBy = $orderCriteria->getField();
        $orderByDirection = $orderCriteria->getDirection();

        if(! in_array($orderBy, self::$ORDER_BY_FIELDS)) {
            throw new \Exception(sprintf('Invalid field `%s` to order by', $orderBy));
        }

        $this->seek = $seek;
        $this->orderBy = $orderBy;
        $this->orderByDirection = $orderByDirection;
    }

    public function getSeek(): SeekCriteria
    {
        return $this->seek;
    }

    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    public function getOrderByDirection(): string
    {
        return $this->orderByDirection;
    }
}