<?php
namespace HappyInvite\Domain\Bundles\Money\Parameters;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

final class EditCurrencyParameters
{
    /** @var string */
    private $code;

    /** @var string */
    private $sign;

    /** @var ImmutableLocalizedString */
    private $title;

    public function __construct(string $code, string $sign, ImmutableLocalizedString $title)
    {
        $this->code = $code;
        $this->sign = $sign;
        $this->title = $title;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getSign(): string
    {
        return $this->sign;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }
}