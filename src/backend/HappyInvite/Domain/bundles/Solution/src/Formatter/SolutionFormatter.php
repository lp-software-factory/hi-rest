<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Formatter\EnvelopeTemplateFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Formatter\EventLandingTemplateFormatter;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Formatter\InviteCardFamilyFormatter;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Formatter\InviteCardTemplateFormatter;

final class SolutionFormatter
{
    public const OPTION_USE_IDS = 'option_use_ids';

    /** @var InviteCardTemplateFormatter */
    private $inviteCardTemplateFormatter;

    /** @var InviteCardFamilyFormatter */
    private $inviteCardFamilyFormatter;

    /** @var EnvelopeTemplateFormatter */
    private $envelopeTemplateFormatter;

    /** @var EventLandingTemplateFormatter */
    private $landingTemplateTemplateFormatter;

    public function __construct(
        InviteCardTemplateFormatter $inviteCardTemplateFormatter,
        InviteCardFamilyFormatter $inviteCardFamilyFormatter,
        EnvelopeTemplateFormatter $envelopeTemplateFormatter,
        EventLandingTemplateFormatter $landingTemplateTemplateFormatter
    ) {
        $this->inviteCardTemplateFormatter = $inviteCardTemplateFormatter;
        $this->inviteCardFamilyFormatter = $inviteCardFamilyFormatter;
        $this->envelopeTemplateFormatter = $envelopeTemplateFormatter;
        $this->landingTemplateTemplateFormatter = $landingTemplateTemplateFormatter;
    }

    public function formatOne(Solution $entity, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_USE_IDS => false
        ], $options);

        $json = $entity->toJSON();

        if($options[self::OPTION_USE_IDS]) {
            unset($json['invite_card_templates']);
            unset($json['envelope_templates']);
            unset($json['landing_templates']);

            if(! $entity->isGlobal()) {
                unset($json['target']['profile']);
                $json['target']['profile_id'] = $entity->getTarget()->getId();
            }

            $json['invite_card_template_ids'] = array_map(function(InviteCardTemplate $inviteCardTemplate) {
                return $inviteCardTemplate->getId();
            }, $entity->getInviteCardTemplates());

            $json['envelope_template_ids'] = array_map(function(EnvelopeTemplate $envelopeTemplate) {
                return $envelopeTemplate->getId();
            }, $entity->getEnvelopeTemplates());

            $json['landing_template_ids'] = array_map(function(EventLandingTemplate $eventLandingTemplate) {
                return $eventLandingTemplate->getId();
            }, $entity->getEventLandingTemplates());
        }else{
            $json['invite_card_templates'] = $this->inviteCardTemplateFormatter->formatMany($entity->getInviteCardTemplates());
            $json['envelope_templates'] = $this->envelopeTemplateFormatter->formatMany($entity->getEnvelopeTemplates());
            $json['landing_templates'] = $this->landingTemplateTemplateFormatter->formatMany($entity->getEventLandingTemplates());
        }

        return [
            'entity' => $json,
            'families' => $this->inviteCardFamilyFormatter->formatMany(array_map(function(InviteCardTemplate $template) {
                return $template->getFamily();
            }, $entity->getInviteCardTemplates()), [
                InviteCardFamilyFormatter::OPTION_USE_IDS => $options[self::OPTION_USE_IDS],
            ])
        ];
    }

    public function formatMany(array $entities, array $options = []): array
    {
        return array_map(function(Solution $entity) use ($options) {
            return $this->formatOne($entity, $options);
        }, $entities);
    }
}