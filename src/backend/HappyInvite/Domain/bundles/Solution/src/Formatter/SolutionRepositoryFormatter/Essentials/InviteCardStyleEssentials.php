<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Style\Formatter\InviteCardStyleFormatter;

final class InviteCardStyleEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var InviteCardStyleService */
    private $inviteCardStyleService;

    /** @var InviteCardStyleFormatter */
    private $inviteCardStyleFormatter;

    public function __construct(InviteCardStyleService $inviteCardStyleService, InviteCardStyleFormatter $inviteCardStyleFormatter)
    {
        $this->inviteCardStyleService = $inviteCardStyleService;
        $this->inviteCardStyleFormatter = $inviteCardStyleFormatter;
    }

    public function toJSON(array $solutions): array
    {
        return [
            'invite_card_styles' => $this->inviteCardStyleFormatter->formatMany(
                $this->inviteCardStyleService->getAll()
            )
        ];
    }
}