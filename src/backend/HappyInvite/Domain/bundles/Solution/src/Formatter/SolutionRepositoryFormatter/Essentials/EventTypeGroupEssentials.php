<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Formatter\EventTypeGroupFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;

final class EventTypeGroupEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var EventTypeGroupFormatter */
    private $eventTypeGroupFormatter;

    /** @var EventTypeGroupService */
    private $eventTypeGroupService;

    public function __construct(
        EventTypeGroupFormatter $eventTypeGroupFormatter,
        EventTypeGroupService $eventTypeGroupService
    ) {
        $this->eventTypeGroupFormatter = $eventTypeGroupFormatter;
        $this->eventTypeGroupService = $eventTypeGroupService;
    }

    public function toJSON(array $solutions): array
    {
        return [
            'event_type_groups' => $this->eventTypeGroupFormatter->formatMany($this->eventTypeGroupService->getAllEventTypeGroups(), [
                EventTypeGroupFormatter::OPTION_WITH_EVENT_TYPES_JSON => false,
                EventTypeGroupFormatter::OPTION_WITH_EVENT_TYPES_IDS => true
            ])
        ];
    }
}