<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter;

interface SolutionRepositoryFormatterEssentials
{
    public function toJSON(array $solutions): array;
}