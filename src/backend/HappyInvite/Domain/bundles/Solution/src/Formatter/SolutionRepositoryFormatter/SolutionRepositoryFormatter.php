<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionFormatter;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\DesignerEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\EventTypeEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\EventTypeGroupEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\InviteCardFamilyEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\InviteCardGammaEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\InviteCardSizeEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\InviteCardStyleEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\InviteCardTemplateEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\LocaleEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials\ProfileEssentials;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Filters\MapFilter;
use Interop\Container\ContainerInterface;

final class SolutionRepositoryFormatter
{
    public const OPTIONS_MAP_FILTER = 'map_filter';

    /** @var ContainerInterface */
    private $container;

    /** @var SolutionFormatter */
    private $solutionFormatter;

    public function __construct(ContainerInterface $container, SolutionFormatter $solutionFormatter)
    {
        $this->container = $container;
        $this->solutionFormatter = $solutionFormatter;
    }

    public function format(array $solutions, array $options = [])
    {
        $options = array_merge([
            self::OPTIONS_MAP_FILTER => false,
        ], $options);

        $json = $this->solutionFormatter->formatMany($solutions, [
            SolutionFormatter::OPTION_USE_IDS => true,
        ]);

        $result = [
            'solutions' => $json,
            'essentials' => $this->getEssentials($solutions),
        ];

        if($options[self::OPTIONS_MAP_FILTER]) {
            $result = (new MapFilter($options[self::OPTIONS_MAP_FILTER]))->filter($result);
        }

        return $result;
    }

    private function getEssentials(array $solutions): array
    {
        return Chain::create([
            LocaleEssentials::class,
            DesignerEssentials::class,
            ProfileEssentials::class,
            EventTypeGroupEssentials::class,
            EventTypeEssentials::class,
            InviteCardFamilyEssentials::class,
            InviteCardGammaEssentials::class,
            InviteCardSizeEssentials::class,
            InviteCardStyleEssentials::class,
            InviteCardTemplateEssentials::class,
        ])
            ->map(function(string $className) {
                return $this->container->get($className);
            })
            ->map(function(SolutionRepositoryFormatterEssentials $essential) use ($solutions) {
                return $essential->toJSON($solutions);
            })
            ->reduce(function(array $carry, array $result) {
                return array_merge($carry, $result);
            }, []);
    }
}