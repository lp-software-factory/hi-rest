<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\Designer\Formatter\DesignerFormatter;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;

final class DesignerEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var DesignerService */
    private $designerService;

    /** @var DesignerFormatter */
    private $designerFormatter;

    public function __construct(DesignerService $designerService, DesignerFormatter $designerFormatter)
    {
        $this->designerService = $designerService;
        $this->designerFormatter = $designerFormatter;
    }

    public function toJSON(array $solutions): array
    {
        $json = $this->designerFormatter->formatMany($this->designerService->getAllDesigners(), [
            DesignerFormatter::OPTION_USE_IDS => true,
        ]);

        return [
            'designers' => $json
        ];
    }
}