<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Formatter\EventTypeFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;

final class EventTypeEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var EventTypeService */
    private $eventTypeService;

    /** @var EventTypeFormatter */
    private $eventTypeFormatter;

    public function __construct(
        EventTypeService $eventTypeService,
        EventTypeFormatter $eventTypeFormatter
    ) {
        $this->eventTypeService = $eventTypeService;
        $this->eventTypeFormatter = $eventTypeFormatter;
    }

    public function toJSON(array $solutions): array
    {
        $json = $this->eventTypeFormatter->formatMany($this->eventTypeService->getAll());

        return [
            'event_types' => $json
        ];
    }
}