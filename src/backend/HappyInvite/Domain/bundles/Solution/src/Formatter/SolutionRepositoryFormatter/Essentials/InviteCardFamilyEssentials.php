<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamily;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Formatter\InviteCardFamilyFormatter;

final class InviteCardFamilyEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var InviteCardFamilyService */
    private $inviteCardFamilyService;

    /** @var InviteCardFamilyFormatter */
    private $inviteCardFamilyFormatter;

    public function __construct(InviteCardFamilyService $inviteCardFamilyService, InviteCardFamilyFormatter $inviteCardFamilyFormatter)
    {
        $this->inviteCardFamilyService = $inviteCardFamilyService;
        $this->inviteCardFamilyFormatter = $inviteCardFamilyFormatter;
    }

    public function toJSON(array $solutions): array
    {
        $inviteCardFamilyIds = [];

        array_map(function(Solution $solution) use (&$inviteCardFamilyIds) {
            /** @var InviteCardTemplate $inviteCardTemplate */
            foreach($solution->getInviteCardTemplates() as $inviteCardTemplate) {
                $inviteCardFamilyIds[$inviteCardTemplate->getFamily()->getId()] = true;
            }
        }, $solutions);

        $json = $this->inviteCardFamilyFormatter->formatMany($this->inviteCardFamilyService->getByIds(new IdsCriteria(array_keys($inviteCardFamilyIds))), [
            InviteCardFamilyFormatter::OPTION_USE_IDS => true,
        ]);

        return [
            'invite_card_families' => $json,
        ];
    }
}