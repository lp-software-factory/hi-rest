<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\Locale\Entity\Locale;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;

final class LocaleEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var LocaleService */
    private $localeService;

    public function __construct(LocaleService $localeService)
    {
        $this->localeService = $localeService;
    }

    public function toJSON(array $solutions): array
    {
        return [
            'locales' => array_map(function(Locale $locale) {
                return $locale->toJSON();
            }, $this->localeService->getAllLocales())
        ];
    }
}