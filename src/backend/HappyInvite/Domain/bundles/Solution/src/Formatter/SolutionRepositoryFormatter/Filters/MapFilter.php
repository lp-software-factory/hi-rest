<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Filters;

final class MapFilter
{
    /** @var array */
    private $map;

    public function __construct(array $map)
    {
        $this->map = $map;
    }

    public function filter(array $input, array $map = null): array {
        if(! $map) {
            $map = $this->map;
        }

        $result = [];

        foreach($input as $key => $value) {
            if(is_numeric($key)) {
                $result[$key] = $this->filter($value, $map);
            }else{
                if(isset($map[$key])) {
                    if($map[$key] === '*') {
                        $result[$key] = $value;
                    }else if(is_array($value)) {
                        $result[$key] = $this->filter($value, $map[$key]);
                    }else{
                        $result[$key] = $value;
                    }
                }
            }
        }

        return $result;
    }
}