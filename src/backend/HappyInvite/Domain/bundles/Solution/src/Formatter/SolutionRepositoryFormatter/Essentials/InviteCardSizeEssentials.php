<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;
use HappyInvite\REST\Bundles\InviteCard\Bundles\CardSize\Formatter\InviteCardSizeFormatter;

final class InviteCardSizeEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var InviteCardSizeService */
    private $inviteCardSizeService;

    /** @var InviteCardSizeFormatter */
    private $inviteCardSizeFormatter;

    public function __construct(InviteCardSizeService $inviteCardSizeService, InviteCardSizeFormatter $inviteCardSizeFormatter)
    {
        $this->inviteCardSizeService = $inviteCardSizeService;
        $this->inviteCardSizeFormatter = $inviteCardSizeFormatter;
    }

    public function toJSON(array $solutions): array
    {
        return [
            'invite_card_sizes' => $this->inviteCardSizeFormatter->formatMany(
                $this->inviteCardSizeService->getAll()
            )
        ];
    }
}