<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Entity\InviteCardFamilyTemplatesEQ;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Template\Formatter\InviteCardTemplateFormatter;

final class InviteCardTemplateEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var InviteCardFamilyService */
    private $inviteCardFamilyService;

    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var InviteCardTemplateFormatter */
    private $inviteCardTemplateFormatter;

    public function __construct(
        InviteCardFamilyService $inviteCardFamilyService,
        InviteCardTemplateService $inviteCardTemplateService,
        InviteCardTemplateFormatter $inviteCardTemplateFormatter
    ) {
        $this->inviteCardFamilyService = $inviteCardFamilyService;
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->inviteCardTemplateFormatter = $inviteCardTemplateFormatter;
    }

    public function toJSON(array $solutions): array
    {
        $inviteCardTemplateIds = [];

        /** @var Solution $solution */
        foreach($solutions as $solution) {

            /** @var InviteCardTemplate $template */
            foreach($solution->getInviteCardTemplates() as $template) {
                /** @var InviteCardFamilyTemplatesEQ $eq */
                foreach($template->getFamily()->getTemplates() as $eq) {
                    $inviteCardTemplateIds[$eq->getInviteCardTemplate()->getId()] = true;
                }
            }
        }

        $json = $this->inviteCardTemplateFormatter->formatMany($this->inviteCardTemplateService->getByIds(new IdsCriteria(array_keys($inviteCardTemplateIds))), [
            InviteCardTemplateFormatter::OPTION_WITH_IDS => true,
        ]);

        return [
            'invite_card_templates' => $json
        ];
    }
}