<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\Designer\Entity\Designer;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Profile\Formatter\ProfileFormatter;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class ProfileEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var DesignerService */
    private $designerService;

    /** @var ProfileService */
    private $profileService;

    /** @var ProfileFormatter */
    private $profileFormatter;

    public function __construct(
        DesignerService $designerService,
        ProfileService $profileService,
        ProfileFormatter $profileFormatter
    ) {
        $this->designerService = $designerService;
        $this->profileService = $profileService;
        $this->profileFormatter = $profileFormatter;
    }

    public function toJSON(array $solutions): array
    {
        return [
            'profiles' => $this->profileFormatter->formatMany(
                $this->profileService->getProfilesByIds(new IdsCriteria($this->fetchProfileIds($solutions)))
            )
        ];
    }

    private function fetchProfileIds(array $solutions): array
    {
        $profileIds = [];

        /** @var Designer $designer */
        foreach($this->designerService->getAllDesigners() as $designer) {
            $profileIds[$designer->getProfile()->getId()] = true;
        }

        /** @var Solution $solution */
        foreach($solutions as $solution) {
            if(! $solution->isGlobal()) {
                $profileIds[$solution->getTarget()->getId()] = true;
            }

            /** @var InviteCardTemplate $inviteCardTemplate */
            foreach($solution->getInviteCardTemplates() as $inviteCardTemplate) {
                $profileIds[$inviteCardTemplate->getFamily()->getAuthor()->getId()] = true;
            }
        }

        return array_keys($profileIds);
    }
}