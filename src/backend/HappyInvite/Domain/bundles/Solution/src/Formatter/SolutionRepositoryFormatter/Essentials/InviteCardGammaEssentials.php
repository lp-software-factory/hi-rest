<?php
namespace HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\Essentials;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatterEssentials;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Gamma\Formatter\InviteCardGammaFormatter;

final class InviteCardGammaEssentials implements SolutionRepositoryFormatterEssentials
{
    /** @var InviteCardGammaService */
    private $inviteCardGammaService;

    /** @var InviteCardGammaFormatter */
    private $inviteCardGammaFormatter;

    public function __construct(InviteCardGammaService $inviteCardGammaService, InviteCardGammaFormatter $inviteCardGammaFormatter)
    {
        $this->inviteCardGammaService = $inviteCardGammaService;
        $this->inviteCardGammaFormatter = $inviteCardGammaFormatter;
    }

    public function toJSON(array $solutions): array
    {
        return [
            'invite_card_gammas' => $this->inviteCardGammaFormatter->formatMany(
                $this->inviteCardGammaService->getAll()
            )
        ];
    }
}