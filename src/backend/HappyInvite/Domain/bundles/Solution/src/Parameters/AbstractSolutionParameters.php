<?php
namespace HappyInvite\Domain\Bundles\Solution\Parameters;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

abstract class AbstractSolutionParameters
{
    /** @var Profile */
    private $target;

    /** @var InviteCardTemplate[] */
    private $inviteCardTemplates;

    /** @var EnvelopeTemplate[] */
    private $envelopeTemplates;

    /** @var EventLandingTemplate[] */
    private $landingTemplates;

    public function __construct(
        array $inviteCardTemplates,
        array $envelopeTemplates,
        array $landingTemplates
    ) {
        $this->inviteCardTemplates = $inviteCardTemplates;
        $this->envelopeTemplates = $envelopeTemplates;
        $this->landingTemplates = $landingTemplates;
    }

    public function assignTo(Profile $profile): self
    {
        $this->target = $profile;

        return $this;
    }

    public function isGlobal(): bool
    {
        return $this->target === null;
    }

    public function getTarget(): Profile
    {
        return $this->target;
    }

    public function getInviteCardTemplates(): array
    {
        return $this->inviteCardTemplates;
    }

    public function getEnvelopeTemplates(): array
    {
        return $this->envelopeTemplates;
    }

    public function getLandingTemplates(): array
    {
        return $this->landingTemplates;
    }
}