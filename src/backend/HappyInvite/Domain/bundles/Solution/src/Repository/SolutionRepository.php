<?php
namespace HappyInvite\Domain\Bundles\Solution\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Exceptions\SolutionNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class SolutionRepository extends EntityRepository
{
    public function createSolution(Solution $solution): int
    {
        while ($this->hasWithSID($solution->getSID())) {
            $solution->regenerateSID();
        }

        $this->getEntityManager()->persist($solution);
        $this->getEntityManager()->flush($solution);

        return $solution->getId();
    }

    public function saveSolution(Solution $solution)
    {
        $this->getEntityManager()->flush($solution);
    }

    public function saveSolutionEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveSolution($entity);
        }
    }

    public function deleteSolution(Solution $solution)
    {
        $this->getEntityManager()->remove($solution);
        $this->getEntityManager()->flush($solution);
    }

    public function listSolutions(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAllSolutionIds(): \Iterator
    {
        $result = $this->createQueryBuilder('s')
            ->select('s.id')
            ->getQuery()
            ->execute(null, AbstractQuery::HYDRATE_SCALAR);

        yield array_map(function(array $input) {
            return (int) $input['id'];
        }, $result);
    }

    public function getById(int $id): Solution
    {
        $result = $this->find($id);

        if ($result instanceof Solution) {
            return $result;
        } else {
            throw new SolutionNotFoundException(sprintf('Solution `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Solutions with IDs (%s) not found', array_diff($ids, array_map(function (Solution $solution) {
                return $solution->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): Solution
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof Solution) {
            return $result;
        } else {
            throw new SolutionNotFoundException(sprintf('Solution `SID(%s)` not found', $sid));
        }
    }
}