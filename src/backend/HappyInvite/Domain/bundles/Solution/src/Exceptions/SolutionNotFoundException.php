<?php
namespace HappyInvite\Domain\Bundles\Solution\Exceptions;

final class SolutionNotFoundException extends \Exception {}