<?php
namespace HappyInvite\Domain\Bundles\Solution\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;

final class SolutionDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Solution::class;
    }
}