<?php
namespace HappyInvite\Domain\Bundles\Solution;

use HappyInvite\Platform\Bundles\HIBundle;

final class SolutionDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}