<?php
namespace HappyInvite\Domain\Bundles\Solution\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Parameters\CreateSolutionParameters;
use HappyInvite\Domain\Bundles\Solution\Parameters\EditSolutionParameters;
use HappyInvite\Domain\Bundles\Solution\Repository\SolutionRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class SolutionService
{
    public const EVENT_CREATED = 'hi.domain.event-solution.solution.created';
    public const EVENT_EDITED = 'hi.domain.event-solution.solution.edited';
    public const EVENT_ASSIGNED_TO = 'hi.domain.event-solution.solution.solution.assigned-to';
    public const EVENT_USER_SPECIFIED = 'hi.domain.event-solution.solution.solution.user-specified';
    public const EVENT_UPDATED = 'hi.domain.event-solution.solution.updated';
    public const EVENT_BEFORE_DUPLICATE = 'hi.domain.event-solution.solution.before-duplicate';
    public const EVENT_DUPLICATED = 'hi.domain.event-solution.solution.duplicated';
    public const EVENT_DELETE = 'hi.domain.event-solution.solution.delete.before';
    public const EVENT_DELETED = 'hi.domain.event-solution.solution.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var SolutionRepository */
    private $solutionRepository;

    public function __construct(SolutionRepository $solutionRepository)
    {
        $this->solutionRepository = $solutionRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createSolution(CreateSolutionParameters $parameters): Solution
    {
        $solution = new Solution(
            $parameters->getInviteCardTemplates(),
            $parameters->getEnvelopeTemplates(),
            $parameters->getLandingTemplates()
        );

        $this->solutionRepository->createSolution($solution);

        if ($parameters->isGlobal()) {
            $solution->assignToGlobal();
        }else{
            $solution->assignTo($parameters->getTarget());
        }

        $this->eventEmitter->emit(self::EVENT_CREATED, [$solution]);

        return $solution;
    }

    public function duplicateSolution(Solution $origSolution, Profile $owner = null): Solution
    {
        $solution = new Solution(
            $origSolution->getInviteCardTemplates(),
            $origSolution->getEnvelopeTemplates(),
            $origSolution->getEventLandingTemplates()
        );

        $this->getEventEmitter()->emit(self::EVENT_BEFORE_DUPLICATE, [$origSolution, $solution, $owner]);
        $this->solutionRepository->createSolution($solution);
        $this->getEventEmitter()->emit(self::EVENT_DUPLICATED, [$origSolution, $solution, $owner]);

        return $solution;
    }

    public function editSolution(int $id, EditSolutionParameters $parameters): Solution
    {
        $solution = $this->solutionRepository->getById($id);
        $solution
            ->setInviteCardTemplates($parameters->getInviteCardTemplates())
            ->setEnvelopeTemplates($parameters->getEnvelopeTemplates())
            ->setLandingTemplates($parameters->getLandingTemplates());

        if ($parameters->isGlobal()) {
            $solution->assignToGlobal();
        }else{
            $solution->assignTo($parameters->getTarget());
        }

        $this->solutionRepository->saveSolution($solution);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$solution]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$solution]);

        return $solution;
    }

    public function assignTo(int $solutionId, Profile $newOwner): void
    {
        $solution = $this->solutionRepository->getById($solutionId);
        $solution->assignTo($newOwner);

        $this->solutionRepository->saveSolution($solution);

        $this->eventEmitter->emit(self::EVENT_ASSIGNED_TO, [$solution]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$solution]);

        $this->solutionRepository->saveSolution($solution);
    }

    public function markAsUserSpecified(int $id, Profile $newOwner): void
    {
        $solution = $this->solutionRepository->getById($id);
        $solution->setSolutionComponentOwner($newOwner);
        $solution->markSolutionAsUserSpecified();

        $this->solutionRepository->saveSolution($solution);

        $this->eventEmitter->emit(self::EVENT_USER_SPECIFIED, [$solution]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$solution]);

        $this->solutionRepository->saveSolution($solution);
    }

    public function deleteSolution(int $id): void
    {
        $solution = $this->getById($id);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$solution]);
        $this->solutionRepository->deleteSolution($solution);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$id, $solution]);
    }

    public function getById(int $id): Solution
    {
        return $this->solutionRepository->getById($id);
    }

    public function getByIds(IdsCriteria $ids): array
    {
        return $this->solutionRepository->getByIds($ids);
    }

    public function getAllSolutionIds(): \Iterator
    {
        return $this->solutionRepository->getAllSolutionIds();
    }
}