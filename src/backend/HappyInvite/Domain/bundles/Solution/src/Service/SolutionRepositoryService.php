<?php
namespace HappyInvite\Domain\Bundles\Solution\Service;

use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\QueryExecutor;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\QueryFactory;

final class SolutionRepositoryService
{
    /** @var QueryFactory */
    private $queryFactory;

    /** @var QueryExecutor */
    private $queryExecutor;

    public function __construct(QueryFactory $queryFactory, QueryExecutor $queryExecutor)
    {
        $this->queryFactory = $queryFactory;
        $this->queryExecutor = $queryExecutor;
    }

    public function getSolutions(string $source, array $json): array
    {
        /** @var Solution[] $result */
        $query = $this->queryFactory->createQueryFromJSON($source, $json['criteria']);
        $result = $this->queryExecutor->execute($query);

        return $result;
    }
}