<?php
namespace HappyInvite\Domain\Bundles\Solution\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany\SolutionEnvelopeTemplates;
use HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany\SolutionEventLandingTemplates;
use HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany\SolutionInviteCardTemplates;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntity;
use HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent\SolutionComponentEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Solution\Repository\SolutionRepository")
 * @Table(name="solution")
 */
class Solution implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity, SolutionComponentEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait, SolutionComponentEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="target_profile_id", referencedColumnName="id")
     * @var Profile|null
     */
    private $target;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany\SolutionInviteCardTemplates", mappedBy="solution", cascade={"all"})
     * @var Collection
     */
    private $inviteCardTemplates;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany\SolutionEnvelopeTemplates", mappedBy="solution", cascade={"all"})
     * @var Collection
     */
    private $envelopeTemplates;

    /**
     * @OneToMany(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany\SolutionEventLandingTemplates", mappedBy="solution", cascade={"all"})
     * @var Collection
     */
    private $landingTemplates;

    public function __construct(
        array $inviteCardTemplates,
        array $envelopeTemplates,
        array $landingTemplates
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->inviteCardTemplates = new ArrayCollection();
        $this->envelopeTemplates = new ArrayCollection();
        $this->landingTemplates = new ArrayCollection();

        $this->setInviteCardTemplates($inviteCardTemplates);
        $this->setEnvelopeTemplates($envelopeTemplates);
        $this->setLandingTemplates($landingTemplates);

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $json = [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'solution' => [
                'is_user_specified' => $this->isSolutionUserSpecified(),
                'owner' => [
                    'has' => $this->hasSolutionComponentOwner(),
                ]
            ],
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'invite_card_templates' => $this->inviteCardTemplates->map(function(SolutionInviteCardTemplates $eq) {
                return $eq->getInviteCardTemplate()->toJSON();
            }),
            'envelope_templates' => $this->envelopeTemplates->map(function(SolutionEnvelopeTemplates $eq) {
                return $eq->getEnvelopeTemplate()->toJSON();
            }),
            'landing_templates' => $this->landingTemplates->map(function(SolutionEventLandingTemplates $eq) {
                return $eq->getEventLandingTemplate()->toJSON();
            }),
        ];

        if($this->hasSolutionComponentOwner()) {
            $json['solution']['owner']['profile'] = $this->getSolutionComponentOwner()->toJSON($options);
        }

        if($this->isGlobal()) {
            $json['target'] = [
                'type' => 'global',
            ];
        }else{
            $json['target'] = [
                'type' => 'profile',
                'profile' => $this->getTarget()->toJSON($options),
            ];
        }

        return $json;
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function isGlobal(): bool
    {
        return $this->target === null;
    }

    public function assignTo(Profile $profile): self
    {
        $this->target = $profile;
        $this->markAsUpdated();

        return $this;
    }

    public function assignToGlobal(): self
    {
        $this->target = null;
        $this->markAsUpdated();

        return $this;
    }

    public function getTarget(): Profile
    {
        return $this->target;
    }

    public function getInviteCardTemplates(): array
    {
        return $this->inviteCardTemplates->map(function(SolutionInviteCardTemplates $eq) {
            return $eq->getInviteCardTemplate();
        })->toArray();
    }

    public function setInviteCardTemplates(array $inviteCardTemplates): self
    {
        $this->inviteCardTemplates->clear();

        array_map(function(InviteCardTemplate $template) {
            $this->inviteCardTemplates->add(new SolutionInviteCardTemplates($this, $template));
        }, $inviteCardTemplates);

        $this->markAsUpdated();

        return $this;
    }

    public function getEnvelopeTemplates(): array
    {
        return $this->envelopeTemplates->map(function(SolutionEnvelopeTemplates $eq) {
            return $eq->getEnvelopeTemplate();
        })->toArray();
    }

    public function setEnvelopeTemplates(array $envelopeTemplates): self
    {
        $this->envelopeTemplates->clear();

        array_map(function(EnvelopeTemplate $template) {
            $this->envelopeTemplates->add(new SolutionEnvelopeTemplates($this, $template));
        }, $envelopeTemplates);

        $this->markAsUpdated();

        return $this;
    }

    public function getEventLandingTemplates(): array
    {
        return $this->landingTemplates->map(function(SolutionEventLandingTemplates $eq) {
            return $eq->getEventLandingTemplate();
        })->toArray();
    }

    public function setLandingTemplates(array $landingTemplates): self
    {
        $this->landingTemplates->clear();

        array_map(function(EventLandingTemplate $template) {
            $this->landingTemplates->add(new SolutionEventLandingTemplates($this, $template));
        }, $landingTemplates);

        $this->markAsUpdated();

        return $this;
    }
}