<?php
namespace HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany;

use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;

/**
 * @Entity
 * @Table(name="event_landing_templates")
 */
class SolutionEventLandingTemplates implements IdEntity
{
    use IdEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\Solution")
     * @JoinColumn(name="solution_id", referencedColumnName="id")
     * @var Solution
     */
    private $solution;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate")
     * @JoinColumn(name="event_landing_template_id", referencedColumnName="id")
     * @var EventLandingTemplate
     */
    private $eventLandingTemplate;

    public function __construct(Solution $solution, EventLandingTemplate $eventLandingTemplate)
    {
        $this->solution = $solution;
        $this->eventLandingTemplate = $eventLandingTemplate;
    }

    public function getSolution(): Solution
    {
        return $this->solution;
    }

    public function getEventLandingTemplate(): EventLandingTemplate
    {
        return $this->eventLandingTemplate;
    }
}