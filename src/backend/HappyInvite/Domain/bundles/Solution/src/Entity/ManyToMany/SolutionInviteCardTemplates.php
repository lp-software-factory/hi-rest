<?php
namespace HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;

/**
 * @Entity
 * @Table(name="solution_invite_card_templates")
 */
class SolutionInviteCardTemplates implements IdEntity
{
    use IdEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\Solution")
     * @JoinColumn(name="solution_id", referencedColumnName="id")
     * @var Solution
     */
    private $solution;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate")
     * @JoinColumn(name="invite_card_template_id", referencedColumnName="id")
     * @var InviteCardTemplate
     */
    private $inviteCardTemplate;

    public function __construct(Solution $solution, InviteCardTemplate $inviteCardTemplate)
    {
        $this->solution = $solution;
        $this->inviteCardTemplate = $inviteCardTemplate;
    }

    public function getSolution(): Solution
    {
        return $this->solution;
    }

    public function getInviteCardTemplate(): InviteCardTemplate
    {
        return $this->inviteCardTemplate;
    }
}