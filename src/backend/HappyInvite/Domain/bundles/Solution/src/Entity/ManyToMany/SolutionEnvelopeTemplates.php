<?php
namespace HappyInvite\Domain\Bundles\Solution\Entity\ManyToMany;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;

/**
 * @Entity
 * @Table(name="solution_envelope_templates")
 */
class SolutionEnvelopeTemplates implements IdEntity
{
    use IdEntityTrait;

    /**
     * @ManyToOne(targetEntity="HappyInvite\Domain\Bundles\Solution\Entity\Solution")
     * @JoinColumn(name="solution_id", referencedColumnName="id")
     * @var Solution
     */
    private $solution;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate")
     * @JoinColumn(name="envelope_template_id", referencedColumnName="id")
     * @var EnvelopeTemplate
     */
    private $envelopeTemplate;

    public function __construct(Solution $solution, EnvelopeTemplate $envelopeTemplate)
    {
        $this->solution = $solution;
        $this->envelopeTemplate = $envelopeTemplate;
    }

    public function getSolution(): Solution
    {
        return $this->solution;
    }

    public function getEnvelopeTemplate(): EnvelopeTemplate
    {
        return $this->envelopeTemplate;
    }
}