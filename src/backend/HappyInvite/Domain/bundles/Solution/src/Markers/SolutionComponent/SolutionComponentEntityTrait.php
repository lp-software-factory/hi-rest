<?php
namespace HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

trait SolutionComponentEntityTrait
{
    /**
     * @Column(name="solution_component_user_specified", type="boolean")
     * @var bool
     */
    private $solutionComponentUserSpecified = false;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Profile\Entity\Profile")
     * @JoinColumn(name="solution_component_owner", referencedColumnName="id")
     * @var Profile
     */
    private $solutionComponentOwner;

    public function hasSolutionComponentOwner(): bool
    {
        return $this->solutionComponentOwner instanceof Profile;
    }

    public function getSolutionComponentOwner(): Profile
    {
        return $this->solutionComponentOwner;
    }

    public function setSolutionComponentOwner(Profile $profile): void
    {
        $this->solutionComponentOwner = $profile;
    }

    public function isSolutionUserSpecified(): bool
    {
        return $this->solutionComponentUserSpecified;
    }

    public function markSolutionAsUserSpecified(): void
    {
        $this->solutionComponentUserSpecified = true;
    }
}