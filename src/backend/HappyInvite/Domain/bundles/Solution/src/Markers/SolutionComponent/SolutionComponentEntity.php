<?php
namespace HappyInvite\Domain\Bundles\Solution\Markers\SolutionComponent;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;

interface SolutionComponentEntity
{
    public function hasSolutionComponentOwner(): bool;
    public function getSolutionComponentOwner(): Profile;
    public function setSolutionComponentOwner(Profile $profile): void;
    public function isSolutionUserSpecified(): bool;
    public function markSolutionAsUserSpecified(): void;
}