<?php
namespace HappyInvite\Domain\Bundles\Solution;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Solution\Factory\Doctrine\SolutionDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Solution\Repository\SolutionRepository;

return [
    SolutionRepository::class => factory(SolutionDoctrineRepositoryFactory::class),
];