<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Service;

use HappyInvite\Domain\Bundles\CompanyProfile\Entity\CompanyProfile;

final class CompanyProfileValidationService
{

    public function isEmailValid(CompanyProfile $companyProfile)
    {
        $Email = filter_var($companyProfile->getEmail(), FILTER_VALIDATE_EMAIL);

        if ($Email == false) {
            return filter_var($companyProfile->getEmail(), FILTER_VALIDATE_EMAIL);
        } else {
            return true;
        }
    }

    public function isINNValid(CompanyProfile $companyProfile)
    {
        $inn = $companyProfile->getInn();

        if (preg_match('#([\d]{10})#', $inn, $m)) {
            $inn = $m[0];
            $code10 = (($inn[0] * 2 + $inn[1] * 4 + $inn[2] * 10 + $inn[3] * 3 +
                        $inn[4] * 5 + $inn[5] * 9 + $inn[6] * 4 + $inn[7] * 6 +
                        $inn[8] * 8) % 11) % 10;
            return ($code10 == $inn[9]);
        } else {
            return false;
        }
    }

    public function isOGRNValid(CompanyProfile $companyProfile)
    {
        $ogrn = $companyProfile->getOgrn();
        $notIp = 13;
        $ip = 15;
        if (mb_strlen($ogrn) == $notIp) // если не ИП
        {
            $ogrn = trim($ogrn);

            if (preg_match('#([\d]{13})#', $ogrn, $m)) {
                $code1 = substr($m[1], 0, 12);
                $code2 = floor($code1 / 11) * 11;
                $code = ($code1 - $code2) % 10;
                return ($code == $m[1][12]);
            }
        } elseif (mb_strlen($ogrn) == $ip) // если ИП
        {
            $ogrn = trim($ogrn);
            //15ти значный код
            if (preg_match('#([\d]{15})#', $ogrn, $m)) {
                $code1 = substr($m[1], 0, 14);
                $code2 = floor($code1 / 13) * 13;
                $code = ($code1 - $code2) % 10;
                return ($code == $m[1][14]) ;
            }

        }
        return false;
    }

    public function isGIROValid(CompanyProfile $companyProfile)
    {
        $account = $companyProfile->getGiro();
        $bik = $companyProfile->getBankBik();

        if (preg_match('#([\d]{23})#', $account, $mAcc, $bik, $mBik)) {
            $account = $mAcc[0];
            $bik = $mBik[0];
            $code = (($bik[6] * 7 + $bik[7] * 1 + $bik[8] * 3) +
                ($account[0] * 7 + $account[1] * 1 + $account[2] * 3 + $account[3] * 7 + $account[4] * 1
                    + $account[5] * 3 + $account[6] * 7 + $account[7] * 1 + $account[8] * 3 + $account[9] * 7
                    + $account[10] * 1 + $account[11] * 3 + $account[12] * 7 + $account[13] * 1 + $account[14] * 3
                    + $account[15] * 7 + $account[16] * 1 + $account[17] * 3 + $account[18] * 7 + $account[19] * 1));
            $controlValue = $code % 10;
            return ($controlValue == 0);
          } else {
            return false;
        }
    }


    public function isBankCorrespondentAccountValid(CompanyProfile $companyProfile)

    {
        $account = $companyProfile->getBankCorrespondentAccount();
        $bik = $companyProfile->getBankBik();

        if (preg_match('#([\d]{23})#', $account, $mAcc, $bik, $mBik)) {
            $account = $mAcc[0];
            $bik = $mBik[0];
            $code = ((0 * 7 + $bik[4] * 1 + $bik[5] * 3) +
                ($account[0] * 7 + $account[1] * 1 + $account[2] * 3 + $account[3] * 7 + $account[4] * 1
                    + $account[5] * 3 + $account[6] * 7 + $account[7] * 1 + $account[8] * 3 + $account[9] * 7
                    + $account[10] * 1 + $account[11] * 3 + $account[12] * 7 + $account[13] * 1 + $account[14] * 3
                    + $account[15] * 7 + $account[16] * 1 + $account[17] * 3 + $account[18] * 7 + $account[19] * 1));
            $controlValue = $code % 10;
            return ($controlValue == 0);
            } else {
                return false;
        }
    }

    public function isOKPOValid(CompanyProfile $companyProfile)
    {
        $okpo = $companyProfile->getOkpo();
        $if8 = 8;
        $if9 = 9;
        $if10 = 10;
        if (mb_strlen($okpo) == $if8) {
            $code = $okpo[0] * 1 + $okpo[1] * 2 + $okpo[2] * 3 + $okpo[3] * 4 + $okpo[4] * 5 + $okpo[5] * 6 + $okpo[6] * 7;
            $controlValue = $code % 11;
            return ($controlValue == $okpo[7]);

        } elseif (mb_strlen($okpo) == $if9) {
            $code = $okpo[0] * 1 + $okpo[1] * 2 + $okpo[2] * 3 + $okpo[3] * 4 + $okpo[4] * 5 + $okpo[5] * 6 + $okpo[6] * 7 + $okpo[7] * 8;
            $controlValue = $code % 11;
            return ($controlValue == $okpo[8]);

        } elseif (mb_strlen($okpo) == $if10) {
            $code = $okpo[0] * 1 + $okpo[1] * 2 + $okpo[2] * 3 + $okpo[3] * 4 + $okpo[4] * 5 + $okpo[5] * 6 + $okpo[6] * 7 + $okpo[7] * 8 + $okpo[8] * 9;
            $controlValue = $code % 11;         // если остаток получается 10, то нужно сделать перерасчет необходимо провести повторный расчет,
            return ($controlValue == $okpo[9]);    //применяя вторую последовательность весов, сдвинутую на два разряда влево (3, 4, 5,…).
        } else {                                     // Если в случае повторного расчета остаток от деления вновь сохраняется равным 10,
            return false;                                     //то значение контрольного числа проставляется равным «0».
        }
    }

    public function isOKATOValid(CompanyProfile $companyProfile)
    {
        $okato = $companyProfile->getOkato();

        $if3 = 3;
        $if6 = 6;
        $if9 = 9;
        $if11 = 11;
        if (mb_strlen($okato) == $if3) {
            $code = $okato[0] * 1 + $okato[1] * 2;
            $controlValue = $code % 11;
            return ($controlValue == $okato[2]);
        } elseif (mb_strlen($okato) == $if6) {
            $code = $okato[0] * 1 + $okato[1] * 2 + $okato[2] * 3 + $okato[3] * 4 + $okato[4] * 5;
            $controlValue = $code % 11;
            return ($controlValue == $okato[5]);
        } elseif (mb_strlen($okato) == $if9) {
            $code = $okato[0] * 1 + $okato[1] * 2 + $okato[2] * 3 + $okato[3] * 4 + $okato[4] * 5 + $okato[5] * 6 + $okato[6] * 7 + $okato[7] * 8;
            $controlValue = $code % 11;
            return ($controlValue == $okato[8]);
        } elseif (mb_strlen($okato) == $if11) {
            $code = $okato[0] * 1 + $okato[1] * 2 + $okato[2] * 3 + $okato[3] * 4 + $okato[4] * 5 + $okato[5] * 6 + $okato[6] * 7 + $okato[7] * 8 + $okato[8] * 9 + $okato[9] * 1;
            $controlValue = $code % 11;
            return ($controlValue == $okato[10]);      // та же ситуация что и с ОКПО, что делать если остаток 10? Новый цикл для каждой?
            } else {
            return false;
        }
    }

    public function validate(CompanyProfile $companyProfile)
    {
        $results = [
            'email' => $this->isEmailValid($companyProfile->getEmail()),
            'inn' => $this->isINNValid($companyProfile->getInn()),
            'ogrn' => $this->isOGRNValid($companyProfile->getOgrn()),
            'giro' => $this->isGIROValid($companyProfile->getGiro()),
            'bank_correspondent_account' => $this->isBankCorrespondentAccountValid($companyProfile->getBankCorrespondentAccount()),
            'okpo' => $this->isOKPOValid($companyProfile->getOkpo()),
        ];

        return array_filter($results, function ($input) {
            return $input === false;
        });
    }

}
