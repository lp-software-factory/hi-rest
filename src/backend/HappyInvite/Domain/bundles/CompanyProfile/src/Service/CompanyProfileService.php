<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Service;


use HappyInvite\Domain\Bundles\CompanyProfile\Entity\CompanyProfile;
use HappyInvite\Domain\Bundles\CompanyProfile\Parameters\CompanyProfileParameters;
use HappyInvite\Domain\Bundles\CompanyProfile\Repository\CompanyProfileRepository;

final class CompanyProfileService
{
    /** @var CompanyProfileRepository */
    private $companyProfileRepository;

    public function __construct(CompanyProfileRepository $companyProfileRepository)
    {
        $this->companyProfileRepository = $companyProfileRepository;
    }

    public function createCompanyProfile (CompanyProfileParameters $parameters)
    {
        $companyProfile = new CompanyProfile
        (
            $parameters->getCompany(),
            $parameters->getInn(),
            $parameters->getKpp(),
            $parameters->getOgrn(),
            $parameters->getWhoGives(),
            $parameters->getBankBik(),
            $parameters->getGiro(),
            $parameters->getBankCorrespondentAccount(),
            $parameters->getOkpo(),
            $parameters->getOkato(),
            $parameters->getOkved(),
            $parameters->getLegalAddress(),
            $parameters->getRealAddress(),
            $parameters->getDirectorGeneral(),
            $parameters->getAccountingGeneral(),
            $parameters->getWhoGives(),
            $parameters->getFax(),
            $parameters->getEmail()

        );

        $this->companyProfileRepository->createCompanyProfile($companyProfile);

        return $companyProfile;
    }

    public function editCompanyProfile(int $companyId, CompanyProfileParameters $parameters)
    {
        $companyProfile = $this->companyProfileRepository->getCompanyProfileByCompanyId($companyId);
        $companyProfile->setInn($parameters->getInn());
        $companyProfile->setKpp($parameters->getKpp());
        $companyProfile->setOgrn($parameters->getOgrn());
        $companyProfile->setWhoGives($parameters);
        $companyProfile->setBankBik($parameters);
        $companyProfile->setGiro($parameters);
        $companyProfile->setBankCorrespondentAccount($parameters);
        $companyProfile->setOkpo($parameters);
        $companyProfile->setOkato($parameters);
        $companyProfile->setOkved($parameters);
        $companyProfile->setLegalAddress($parameters);
        $companyProfile->setRealAddress($parameters);
        $companyProfile->setDirectorGeneral($parameters);
        $companyProfile->setAccountingGeneral($parameters);
        $companyProfile->setPhone($parameters);
        $companyProfile->setFax($parameters);
        $companyProfile->setEmail($parameters);

        $this->companyProfileRepository->saveCompanyProfile($companyProfile);

        return $companyProfile;
    }

    public function getCompanyProfileById(int $companyProfileId)
    {
        return $this->companyProfileRepository->getCompanyProfileById($companyProfileId);
    }

    public function getCompanyProfileByCompanyId(int $companyId)
    {
        return $this->companyProfileRepository->getCompanyProfileByCompanyId($companyId);
    }

}