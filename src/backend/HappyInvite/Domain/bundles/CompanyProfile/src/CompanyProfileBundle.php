<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile;

use HappyInvite\Platform\Bundles\HIBundle;

final class CompanyProfileBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

}

