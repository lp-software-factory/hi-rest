<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Factory\Doctrine;

use HappyInvite\Domain\Bundles\CompanyProfile\Entity\CompanyProfile;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class CompanyProfileRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return CompanyProfile::class;
    }
}