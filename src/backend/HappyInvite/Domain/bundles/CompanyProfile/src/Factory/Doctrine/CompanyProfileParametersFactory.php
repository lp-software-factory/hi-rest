<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Company\Service\CompanyService;
use HappyInvite\Domain\Bundles\CompanyProfile\Parameters\CompanyProfileParameters;
use HappyInvite\Domain\Bundles\CompanyProfile\Service\CompanyProfileService;

final class CompanyProfileParametersFactory
{
    /** @var CompanyService */
    private $companyService;

     /** @var CompanyProfileService */
    private $companyProfileService;

    public function __construct(
        CompanyService $companyService,
        CompanyProfileService $companyProfileService
    )
    {
        $this->companyService = $companyService;
        $this->companyProfileService = $companyProfileService;
    }

    public function createCompanyProfileParameters (array $input): CompanyProfileParameters
    {
        return new CompanyProfileParameters(
            $this->companyService->getCompanyById($input['company_id']),
            $this->companyProfileService->createCompanyProfile($input['company']),
            $this->companyProfileService->createCompanyProfile($input['inn']),
            $this->companyProfileService->createCompanyProfile($input['kpp']),
            $this->companyProfileService->createCompanyProfile($input['ogrn']),
            $this->companyProfileService->createCompanyProfile($input['who_gives']),
            $this->companyProfileService->createCompanyProfile($input['bank_bik']),
            $this->companyProfileService->createCompanyProfile($input['bank_correspondent_account']),
            $this->companyProfileService->createCompanyProfile($input['okpo']),
            $this->companyProfileService->createCompanyProfile($input['okato']),
            $this->companyProfileService->createCompanyProfile($input['okved']),
            $this->companyProfileService->createCompanyProfile($input['legal_address']),
            $this->companyProfileService->createCompanyProfile($input['real_address']),
            $this->companyProfileService->createCompanyProfile($input['director_general']),
            $this->companyProfileService->createCompanyProfile($input['accounting_general']),
            $this->companyProfileService->createCompanyProfile($input['phone']),
            $this->companyProfileService->createCompanyProfile($input['fax']),
            $this->companyProfileService->createCompanyProfile($input['email'])
        );
    }
}

