<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Repository;


use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Exception\CompanyNotFoundException;
use HappyInvite\Domain\Bundles\CompanyProfile\Entity\CompanyProfile;
use HappyInvite\Domain\Bundles\CompanyProflie\Exceptions\CompanyProfileNotFoundException;

final class CompanyProfileRepository extends EntityRepository
{

    public function createCompanyProfile(CompanyProfile $companyProfile): int
    {
        while($this->hasWithSID($companyProfile->getSID())) {
            $companyProfile->regenerateSID();
        }

        $this->getEntityManager()->persist($companyProfile);
        $this->getEntityManager()->flush($companyProfile);

        return $companyProfile->getId();
    }

    public function saveCompanyProfile(CompanyProfile $companyProfile)
    {
        $this->getEntityManager()->flush($companyProfile);
    }

    public function getCompanyProfileById(int $companyProfileId): CompanyProfile
    {
        $result = $this->find($companyProfileId);

        if($result instanceof CompanyProfile) {
            return $result;
        }else{
            throw new CompanyProfileNotFoundException(sprintf('Company Profile `ID(%d)` not found', $companyProfileId));
        }
    }

    public function getCompanyProfileByCompanyId(int $companyId): CompanyProfile
    {
        $result = $this->find($companyId);

        if($result instanceof CompanyProfile) {
            return $result;
        }else{
            throw new CompanyNotFoundException(sprintf('Company  `ID(%d)` not found', $companyId));
        }
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

}