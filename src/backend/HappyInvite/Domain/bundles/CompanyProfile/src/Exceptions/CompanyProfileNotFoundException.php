<?php
namespace HappyInvite\Domain\Bundles\CompanyProflie\Exceptions;

final class CompanyProfileNotFoundException extends \Exception {}