<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Parameters;

use HappyInvite\Domain\Bundles\Company\Entity\Company;

final class EditCompanyProfileParameters
{

    public function __construct(Company $company, string $inn, string $kpp, string $ogrn)
    {
        $this->company = $company;
        $this->inn = $inn;
        $this->kpp = $kpp;
        $this->ogrn = $ogrn;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }
    /**
     * @return string
     */
    public function getKpp(): string
    {
        return $this->kpp;
    }

    /**
     * @param string $kpp
     */
    public function setKpp(string $kpp)
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string
     */
    public function getOgrn(): string
    {
        return $this->ogrn;
    }

    /**
     * @param string $ogrn
     */
    public function setOgrn(string $ogrn)
    {
        $this->ogrn = $ogrn;
    }
}
