<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Parameters;

use HappyInvite\Domain\Bundles\Company\Entity\Company;

final class CompanyProfileParameters
{

    public function __construct(
        Company $company,
        string $inn,
        string $kpp,
        string $ogrn,
        string $who_gives,
        string $bank_bik,
        string $giro,
        string $bank_correspondent_account,
        string $okpo,
        string $okato,
        string $okved,
        string $legal_address,
        string $real_address,
        string $director_general,
        string $accounting_general,
        string $phone,
        string $fax,
        string $email
    )

    {
        $this->company = $company;
        $this->inn = $inn;
        $this->kpp = $kpp;
        $this->ogrn = $ogrn;
        $this->who_gives = $who_gives;
        $this->bank_bik = $bank_bik;
        $this->giro = $giro;
        $this->bank_correspondent_account = $bank_correspondent_account;
        $this->okpo = $okpo;
        $this->okato = $okato;
        $this->okved = $okved;
        $this->legal_address = $legal_address;
        $this->real_address = $real_address;
        $this->director_general = $director_general;
        $this->accounting_general = $accounting_general;
        $this->phone = $phone;
        $this->fax = $fax;
        $this->email = $email;

    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }
    /**
     * @return string
     */
    public function getKpp(): string
    {
        return $this->kpp;
    }

    /**
     * @param string $kpp
     */
    public function setKpp(string $kpp)
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string
     */
    public function getOgrn(): string
    {
        return $this->ogrn;
    }

    /**
     * @param string $ogrn
     */
    public function setOgrn(string $ogrn)
    {
        $this->ogrn = $ogrn;
    }

    /**
     * @return string
     */
    public function getWhoGives(): string
    {
        return $this->who_gives;
    }

    /**
     * @param string $who_gives
     */
    public function setWhoGives(string $who_gives)
    {
        $this->who_gives = $who_gives;
    }

    /**
     * @return string
     */
    public function getBankBik(): string
    {
        return $this->bank_bik;
    }

    /**
     * @param string $bank_bik
     */
    public function setBankBik(string $bank_bik)
    {
        $this->bank_bik = $bank_bik;
    }
    /**
     * @return string
     */
    public function getGiro(): string
    {
        return $this->giro;
    }

    /**
     * @param string $giro
     */
    public function setGiro(string $giro)
    {
        $this->giro = $giro;
    }
    /**
     * @return string
     */
    public function getBankCorrespondentAccount(): string
    {
        return $this->bank_correspondent_account;
    }

    /**
     * @param string $bank_correspondent_account
     */
    public function setBankCorrespondentAccount(string $bank_correspondent_account)
    {
        $this->bank_correspondent_account = $bank_correspondent_account;
    }

    /**
     * @return string
     */
    public function getOkpo(): string
    {
        return $this->okpo;
    }

    /**
     * @param string $okpo
     */
    public function setOkpo(string $okpo)
    {
        $this->okpo = $okpo;
    }

    /**
     * @return string
     */
    public function getOkato(): string
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     */
    public function setOkato(string $okato)
    {
        $this->okato = $okato;
    }
    /**
     * @return string
     */
    public function getOkved(): string
    {
        return $this->okved;
    }

    /**
     * @param string $okved
     */
    public function setOkved(string $okved)
    {
        $this->okved = $okved;
    }

    /**
     * @return string
     */
    public function getLegalAddress(): string
    {
        return $this->legal_address;
    }

    /**
     * @param string $legal_address
     */
    public function setLegalAddress(string $legal_address)
    {
        $this->legal_address = $legal_address;
    }

    /**
     * @return string
     */
    public function getRealAddress(): string
    {
        return $this->real_address;
    }

    /**
     * @param string $real_address
     */
    public function setRealAddress(string $real_address)
    {
        $this->real_address = $real_address;
    }

    /**
     * @return string
     */
    public function getDirectorGeneral(): string
    {
        return $this->director_general;
    }

    /**
     * @param string $director_general
     */
    public function setDirectorGeneral(string $director_general)
    {
        $this->director_general = $director_general;
    }

    /**
     * @return string
     */
    public function getAccountingGeneral(): string
    {
        return $this->accounting_general;
    }

    /**
     * @param string $accounting_general
     */
    public function setAccountingGeneral(string $accounting_general)
    {
        $this->accounting_general = $accounting_general;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFax(): string
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax(string $fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

}