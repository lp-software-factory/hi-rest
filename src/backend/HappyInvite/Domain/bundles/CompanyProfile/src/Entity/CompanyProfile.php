<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile\Entity;


use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\CompanyProfile\Repository\CompanyProfileRepository")
 * @Table(name="company_profile")
 */
class CompanyProfile implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity,VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait;
    use SIDEntityTrait;
    use JSONMetadataEntityTrait;
    use ModificationEntityTrait;
    use VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Company\Entity\Company")
     * @JoinColumn(name="company_id", referencedColumnName="id")
     * @var Company
     */
    private $company;

    /**
     * @Column(type="string", name="ogrn")
     * @var string
     */
    private $ogrn = '';

    /**
     * @Column(type="string",name="who_gives")
     * @var string
     */
    private $who_gives = '';

    /**
     * @Column(type="string",name="inn")
     * @var string
     */
    private $inn = '';

    /**
     * @Column(type="string", name="kpp")
     * @var string
     */
    private $kpp = '';

    /**
     * @Column(type="string", name="bank_bik")
     * @var string
     */
    private $bank_bik = '';

    /**
     * @Column(type="string", name="giro")
     * @var string
     */
    private $giro = '';

    /**
     * @Column(type="string", name="bank_correspondent_account")
     * @var string
     */
    private $bank_correspondent_account = '';

    /**
     * @Column(type="string", name="okpo")
     * @var string
     */
    private $okpo = '';

    /**
     * @Column(type="string", name="okato")
     * @var string
     */
    private $okato = '';

    /**
     * @Column(type="string", name="okved")
     * @var string
     */
    private $okved = '';

    /**
     * @Column(type="string", name="legal_address")
     * @var string
     */
    private $legal_address = '';

    /**
     * @Column(type="string", name="real_address")
     * @var string
     */
    private $real_address = '';

    /**
     * @Column(type="string", name="director_general")
     * @var string
     */
    private $director_general = '';

    /**
     * @Column(type="string", name="accounting_general")
     * @var string
     */
    private $accounting_general = '';

    /**
     * @Column(type="string", name="phone")
     * @var string
     */
    private $phone = '';

    /**
     * @Column(type="string", name="fax")
     * @var string
     */
    private $fax = '';

    /**
     * @Column(type="string", name="email")
     * @var string
     */
    private $email = '';


    public function __construct(Company $company, string $inn, string $kpp, string $ogrn)
    {

        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->company = $company;
        $this->inn = $inn;
        $this->kpp = $kpp;
        $this->ogrn = $ogrn;
        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        $result = [
            'id' => $this->getId(),
            'sid' => $this->getSID(),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'metadata' => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),

            'company' => $this->getCompany(),
            'ogrn' => $this->getOgrn(),
            'who_gives' => $this->getWhoGives(),
            'inn' => $this->getInn(),
            'kpp' => $this->getKpp(),
            'bank_bik' => $this->getBankBik(),
            'giro' => $this->getGiro(),
            'bank_correspondent_account' => $this->getBankCorrespondentAccount(),
            'okpo' => $this->getOkpo(),
            'okato' => $this->getOkato(),
            'okved' => $this->getOkved(),
            'legal_address' => $this->getLegalAddress(),
            'real_address' => $this->getRealAddress(),
            'director_general' => $this->getDirectorGeneral(),
            'accounting_general' => $this->getAccountingGeneral(),
            'phone' => $this->getPhone(),
            'fax' => $this->getFax(),
            'email' => $this->getEmail(),

        ];


        return $result;
    }

    public function   getVersion(): string {
        return "1.0.0";
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self  ///CompanyProfile??
    {
        $this->company = $company;
        $this->markAsUpdated();

        return $this;
    }

    public function getOgrn(): string
    {
        return $this->ogrn;
    }

    public function setOgrn (string $ogrn): self
    {
        $this->ogrn = $ogrn;

        return $this;
    }

    public function getWhoGives(): string
    {
        return $this->who_gives;
    }

    public function setWhoGives (string $who_gives): self
    {
        $this->who_gives = $who_gives;

        return $this;
    }

    public function getInn(): string
    {
        return $this->inn;
    }

    public function setInn (string $inn): self
    {
        $this->inn = $inn;

        return $this;
    }

    public function getKpp(): string
    {
        return $this->inn;
    }

    public function setKpp (string $kpp): self
    {
        $this->kpp = $kpp;

        return $this;
    }

    public function getBankBik(): string
    {
        return $this->bank_bik;
    }

    public function setBankBik (string $bank_bik): self
    {
        $this->bank_bik = $bank_bik;

        return $this;
    }
    public function getBankCorrespondentAccount(): string
    {
        return $this->bank_correspondent_account;
    }

    public function setBankCorrespondentAccount (string $bank_correspondent_account): self
    {
        $this->bank_correspondent_account = $bank_correspondent_account;

        return $this;
    }

    public function getGiro(): string
    {
        return $this->giro;
    }

    public function setGiro (string $giro): self
    {
        $this->giro = $giro;

        return $this;
    }
    public function getOkpo(): string
    {
        return $this->okpo;
    }

    public function setOkpo (string $okpo): self
    {
        $this->okpo = $okpo;

        return $this;
    }

    public function getOkato(): string
    {
        return $this->okato;
    }

    public function setOkato (string $okato): self
    {
        $this->okato = $okato;

        return $this;
    }

    public function getOkved(): string
    {
        return $this->okved;
    }

    public function setOkved (string $okved): self
    {
        $this->okved = $okved;

        return $this;
    }

    public function getLegalAddress(): string
    {
        return $this->legal_address;
    }

    public function setLegalAddress (string $legal_address): self
    {
        $this->legal_address = $legal_address;

        return $this;
    }

    public function getRealAddress(): string
    {
        return $this->real_address;
    }

    public function setRealAddress (string $real_address): self
    {
        $this->real_address = $real_address;

        return $this;
    }

    public function getDirectorGeneral(): string
    {
        return $this->director_general;
    }

    public function setDirectorGeneral (string $director_general): self
    {
        $this->director_general = $director_general;

        return $this;
    }

    public function getAccountingGeneral(): string
    {
        return $this->accounting_general;
    }

    public function setAccountingGeneral (string $accounting_general): self
    {
        $this->accounting_general = $accounting_general;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone (string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getFax(): string
    {
        return $this->fax;
    }

    public function setFax (string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail (string $email): self
    {
        $this->email = $email;

        return $this;
    }

}