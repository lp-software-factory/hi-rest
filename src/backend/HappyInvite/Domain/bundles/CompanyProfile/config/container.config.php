<?php
namespace HappyInvite\Domain\Bundles\CompanyProfile;


use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\CompanyProfile\Repository\CompanyProfileRepository;
use HappyInvite\Domain\Bundles\CompanyProfile\Factory\Doctrine\CompanyProfileRepositoryFactory;

return [
    CompanyProfileRepository::class => factory(CompanyProfileRepositoryFactory::class),
];