<?php
namespace HappyInvite\Domain\Bundles\DressCode\Service;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use HappyInvite\Domain\Bundles\DressCode\Entity\DressCode;
use HappyInvite\Domain\Bundles\DressCode\Parameters\CreateDressCodeParameters;
use HappyInvite\Domain\Bundles\DressCode\Parameters\EditDressCodeParameters;
use HappyInvite\Domain\Bundles\DressCode\Repository\DressCodeRepository;

final class DressCodeService
{
    public const EVENT_CREATED = 'hi.domain.dress-code.entity.created';
    public const EVENT_EDITED = 'hi.domain.dress-code.entity.edited';
    public const EVENT_UPDATED = 'hi.domain.dress-code.entity.updated';
    public const EVENT_DELETE = 'hi.domain.dress-code.entity.delete.before';
    public const EVENT_DELETED = 'hi.domain.dress-code.entity.delete.after';

    /** @var EventEmitterInterface */
    private $eventEmitter;

    /** @var DressCodeRepository */
    private $entityRepository;

    public function __construct(DressCodeRepository $dressCodeRepository)
    {
        $this->entityRepository = $dressCodeRepository;
        $this->eventEmitter = new EventEmitter();
    }

    public function getEventEmitter(): EventEmitterInterface
    {
        return $this->eventEmitter;
    }

    public function createDressCode(CreateDressCodeParameters $parameters): DressCode
    {
        $entity = new DressCode(
            $parameters->getImage(),
            $parameters->getTitle(),
            $parameters->getDescription(),
            $parameters->getDescriptionMen(),
            $parameters->getDescriptionWomen()
        );

        $this->entityRepository->createEntity($entity);

        $this->eventEmitter->emit(self::EVENT_CREATED, [$entity]);

        return $entity;
    }

    public function editDressCode(int $entityId, EditDressCodeParameters $parameters): DressCode
    {
        $entity = $this->entityRepository->getById($entityId);
        $entity
            ->setImage($parameters->getImage())
            ->setTitle($parameters->getTitle())
            ->setDescriptionWomen($parameters->getDescriptionWomen())
            ->setDescriptionMen($parameters->getDescriptionMen())
            ->setDescription($parameters->getDescription());

        $this->entityRepository->saveEntity($entity);

        $this->eventEmitter->emit(self::EVENT_EDITED, [$entity]);
        $this->eventEmitter->emit(self::EVENT_UPDATED, [$entity]);

        return $entity;
    }

    public function deleteDressCode(int $entityId)
    {
        $entity = $this->getById($entityId);

        $this->eventEmitter->emit(self::EVENT_DELETE, [$entity]);
        $this->entityRepository->deleteEntity($entity);
        $this->eventEmitter->emit(self::EVENT_DELETED, [$entityId, $entity]);
    }

    public function getById(int $entityId): DressCode
    {
        return $this->entityRepository->getById($entityId);
    }

    public function getAll(): array
    {
        return $this->entityRepository->getAll();
    }
}