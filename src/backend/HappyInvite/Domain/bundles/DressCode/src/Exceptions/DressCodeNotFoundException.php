<?php
namespace HappyInvite\Domain\Bundles\DressCode\Exceptions;

final class DressCodeNotFoundException extends \Exception {}