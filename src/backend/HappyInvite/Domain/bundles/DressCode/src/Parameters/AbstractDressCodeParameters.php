<?php
namespace HappyInvite\Domain\Bundles\DressCode\Parameters;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;

abstract class AbstractDressCodeParameters
{
    /** @var Attachment */
    private $image;

    /** @var ImmutableLocalizedString */
    private $title;

    /** @var ImmutableLocalizedString */
    private $description;

    /** @var ImmutableLocalizedString */
    private $descriptionMen;

    /** @var ImmutableLocalizedString */
    private $descriptionWomen;

    public function __construct(Attachment $image, ImmutableLocalizedString $title, ImmutableLocalizedString $description, ImmutableLocalizedString $descriptionMen, ImmutableLocalizedString $descriptionWomen)
    {
        $this->image = $image;
        $this->title = $title;
        $this->description = $description;
        $this->descriptionMen = $descriptionMen;
        $this->descriptionWomen = $descriptionWomen;
    }

    public function getImage(): Attachment
    {
        return $this->image;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function getDescriptionMen(): ImmutableLocalizedString
    {
        return $this->descriptionMen;
    }

    public function getDescriptionWomen(): ImmutableLocalizedString
    {
        return $this->descriptionWomen;
    }
}