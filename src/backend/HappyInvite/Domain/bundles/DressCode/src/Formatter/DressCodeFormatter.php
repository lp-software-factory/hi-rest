<?php
namespace HappyInvite\Domain\Bundles\DressCode\Formatter;

use HappyInvite\Domain\Bundles\DressCode\Entity\DressCode;

final class DressCodeFormatter
{
    public function formatOne(DressCode $entity): array
    {
        return [
            'entity' => $entity->toJSON(),
        ];
    }

    public function formatMany(array $entities): array
    {
        return array_map(function(DressCode $entity) {
            return $this->formatOne($entity);
        }, $entities);
    }
}