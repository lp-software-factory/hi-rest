<?php
namespace HappyInvite\Domain\Bundles\DressCode\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntity;
use HappyInvite\Domain\Markers\SerialEntity\SerialEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\DressCode\Repository\DressCodeRepository")
 * @Table(name="dress_code")
 */
class DressCode implements JSONSerializable, IdEntity, SIDEntity, JSONMetadataEntity, ModificationEntity, VersionEntity, SerialEntity
{
    public const LATEST_VERSION = '1.0.0';

    use IdEntityTrait, SIDEntityTrait, JSONMetadataEntityTrait, VersionEntityTrait, ModificationEntityTrait, SerialEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="image_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $image;

    /**
     * @Column(name="title", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $title;

    /**
     * @Column(name="description", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $description;

    /**
     * @Column(name="description_men", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $descriptionMen;

    /**
     * @Column(name="description_women", type="i18n")
     * @var ImmutableLocalizedString
     */
    private $descriptionWomen;

    public function __construct(
        Attachment $image,
        ImmutableLocalizedString $title,
        ImmutableLocalizedString $description,
        ImmutableLocalizedString $descriptionMen,
        ImmutableLocalizedString $descriptionWomen
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->image = $image;
        $this->title = $title;
        $this->description = $description;
        $this->descriptionMen = $descriptionMen;
        $this->descriptionWomen = $descriptionWomen;

        $this->regenerateSID();
        $this->initModificationEntity();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'image' => $this->getImage()->toJSON($options),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'version' => $this->getVersion(),
            'position' => $this->getPosition(),
            'title' => $this->getTitle()->toJSON($options),
            'description' => $this->getDescription()->toJSON($options),
            'description_men' => $this->getDescriptionMen()->toJSON($options),
            'description_women' => $this->getDescriptionWomen()->toJSON($options),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getImage(): Attachment
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): ImmutableLocalizedString
    {
        return $this->title;
    }

    public function setTitle(ImmutableLocalizedString $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescription(): ImmutableLocalizedString
    {
        return $this->description;
    }

    public function setDescription(ImmutableLocalizedString $description): self
    {
        $this->description = $description;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescriptionMen(): ImmutableLocalizedString
    {
        return $this->descriptionMen;
    }

    public function setDescriptionMen(ImmutableLocalizedString $descriptionMen): self
    {
        $this->descriptionMen = $descriptionMen;
        $this->markAsUpdated();

        return $this;
    }

    public function getDescriptionWomen(): ImmutableLocalizedString
    {
        return $this->descriptionWomen;
    }

    public function setDescriptionWomen(ImmutableLocalizedString $descriptionWomen): self
    {
        $this->descriptionWomen = $descriptionWomen;
        $this->markAsUpdated();

        return $this;
    }
}