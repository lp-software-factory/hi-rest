<?php
namespace HappyInvite\Domain\Bundles\DressCode\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\DressCode\Entity\DressCode;
use HappyInvite\Domain\Bundles\DressCode\Exceptions\DressCodeNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class DressCodeRepository extends EntityRepository
{
    public function createEntity(DressCode $entity): int
    {
        while ($this->hasWithSID($entity->getSID())) {
            $entity->regenerateSID();
        }

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        return $entity->getId();
    }

    public function saveEntity(DressCode $entity)
    {
        $this->getEntityManager()->flush($entity);
    }

    public function saveEntityEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->saveEntity($entity);
        }
    }

    public function deleteEntity(DressCode $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function listFamilies(SeekCriteria $seek): array
    {
        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());

        return $query->getQuery()->execute();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function getById(int $id): DressCode
    {
        $result = $this->find($id);

        if ($result instanceof DressCode) {
            return $result;
        } else {
            throw new DressCodeNotFoundException(sprintf('Dress code `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if (count($ids) !== count($results)) {
            throw new \Exception('Dress codes with IDs (%s) not found', array_diff($ids, array_map(function (DressCode $entity) {
                return $entity->getId();
            }, $results)));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function getBySID(string $sid): DressCode
    {
        $result = $this->findOneBy(['sid' => $sid]);

        if ($result instanceof DressCode) {
            return $result;
        } else {
            throw new DressCodeNotFoundException(sprintf('Dress code `SID(%s)` not found', $sid));
        }
    }
}