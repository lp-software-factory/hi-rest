<?php
namespace HappyInvite\Domain\Bundles\DressCode\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\DressCode\Entity\DressCode;

final class DressCodeDoctrineRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return DressCode::class;
    }
}