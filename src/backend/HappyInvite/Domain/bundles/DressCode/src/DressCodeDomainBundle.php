<?php
namespace HappyInvite\Domain\Bundles\DressCode;

use HappyInvite\Platform\Bundles\HIBundle;

final class DressCodeDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}