<?php
namespace HappyInvite\Domain\Bundles\DressCode;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\DressCode\Factory\Doctrine\DressCodeDoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\DressCode\Repository\DressCodeRepository;

return [
    DressCodeRepository::class => factory(DressCodeDoctrineRepositoryFactory::class),
];