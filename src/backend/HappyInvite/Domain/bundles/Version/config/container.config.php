<?php
namespace HappyInvite\Domain\Bundles\Version;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Version\Service\VersionService;

return [
    VersionService::class => object()
        ->constructorParameter('composer', get('config.hi.rest.composer.json')),
];