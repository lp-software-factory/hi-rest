<?php
namespace HappyInvite\Domain\Bundles\Version;

use HappyInvite\Domain\Bundles\Version\Console\Command\CurrentVersionCommand;

return [
    'config.console' => [
        'commands' => [
            'Version' => [
                CurrentVersionCommand::class,
            ]
        ]
    ]
];