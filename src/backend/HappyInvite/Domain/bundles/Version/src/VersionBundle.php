<?php
namespace HappyInvite\Domain\Bundles\Version;

use HappyInvite\Platform\Bundles\HIBundle;

final class VersionBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}