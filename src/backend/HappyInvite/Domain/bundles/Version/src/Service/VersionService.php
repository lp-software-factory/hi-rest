<?php
namespace HappyInvite\Domain\Bundles\Version\Service;

final class VersionService
{
    /** @var string */
    private $current;

    public function __construct(array $composer)
    {
        $this->current = $composer['version'];
    }

    public function getCurrentVersion(): string
    {
        return $this->current;
    }
}