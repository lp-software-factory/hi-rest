<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Config;

final class ListAvailableLeagueProviders
{
    /** @var array[] */
    private $available;

    public function __construct(array $available)
    {
        $this->available = $available;
    }

    public function getAvailable(): array
    {
        return $this->available;
    }
}