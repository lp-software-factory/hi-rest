<?php
namespace HappyInvite\Domain\Bundles\OAuth2;

use HappyInvite\Platform\Bundles\HIBundle;

final class OAuth2DomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}