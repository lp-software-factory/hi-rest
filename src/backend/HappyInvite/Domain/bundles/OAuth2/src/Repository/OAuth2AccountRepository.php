<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\OAuth2\Entity\OAuth2Account;
use HappyInvite\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use HappyInvite\Domain\Bundles\OAuth2\Exceptions\OAuth2AccountNotFoundException;

final class OAuth2AccountRepository extends EntityRepository
{
    public function createOAuth2Account(OAuth2Account $oAuth2Account): int
    {
        while($this->hasWithSID($oAuth2Account->getSID())) {
            $oAuth2Account->regenerateSID();
        }

        $this->getEntityManager()->persist($oAuth2Account);
        $this->getEntityManager()->flush($oAuth2Account);

        return $oAuth2Account->getId();
    }

    public function saveOAuth2Account(OAuth2Account $oAuth2Account)
    {
        $this->getEntityManager()->flush($oAuth2Account);
    }

    public function deleteOAuth2Account(OAuth2Account $oAuth2Account)
    {
        $this->getEntityManager()->remove($oAuth2Account);
        $this->getEntityManager()->flush($oAuth2Account);
    }

    public function getById(int $id): OAuth2Account
    {
        $result = $this->find($id);

        if($result instanceof OAuth2Account) {
            return $result;
        }else{
            throw new OAuth2AccountNotFoundException(sprintf('OAuth2Account `ID(%d)` not found', $id));
        }
    }

    public function getByAccountId(int $accountId): array
    {
        return $this->findBy([
            'account' => $accountId
        ]);
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasOAuth2AccountWithResourceOwnerId(OAuth2Provider $provider, string $resourceOwnerId): bool
    {
        return $this->findOneBy([
            'provider' => $provider,
            'resourceOwnerId' => $resourceOwnerId
        ]) !== null;
    }

    public function getOAuth2AccountWithResourceOwnerId(OAuth2Provider $provider, string $resourceOwnerId): OAuth2Account
    {
        $result = $this->findOneBy([
            'provider' => $provider,
            'resourceOwnerId' => $resourceOwnerId
        ]);

        if($result instanceof OAuth2Account) {
            return $result;
        }else{
            throw new OAuth2AccountNotFoundException(sprintf('OAuth2Account not found'));
        }
    }
}