<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Parameters;

final class SetOAuth2ConfigParameters
{
    /** @var array */
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}