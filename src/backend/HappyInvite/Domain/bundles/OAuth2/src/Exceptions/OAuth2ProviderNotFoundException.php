<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Exceptions;

final class OAuth2ProviderNotFoundException extends \Exception
{}