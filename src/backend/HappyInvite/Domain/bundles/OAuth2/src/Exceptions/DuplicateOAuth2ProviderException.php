<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Exceptions;

final class DuplicateOAuth2ProviderException extends \Exception {}