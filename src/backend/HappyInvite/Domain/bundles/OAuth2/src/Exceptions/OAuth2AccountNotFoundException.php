<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Exceptions;

final class OAuth2AccountNotFoundException extends \Exception
{}