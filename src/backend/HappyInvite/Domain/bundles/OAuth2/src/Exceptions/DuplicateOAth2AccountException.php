<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Exceptions;

final class DuplicateOAth2AccountException extends \Exception {}