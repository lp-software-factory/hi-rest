<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Exceptions;

final class UnknownLeagueProviderException extends \Exception {}