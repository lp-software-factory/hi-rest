<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\OAuth2\Entity\OAuth2Provider;

final class OAuth2ProviderRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return OAuth2Provider::class;
    }
}