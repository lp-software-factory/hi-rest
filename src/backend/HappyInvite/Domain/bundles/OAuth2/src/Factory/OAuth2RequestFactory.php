<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Factory;

use Aego\OAuth2\Client\Provider\Yandex;
use HappyInvite\Domain\Bundles\OAuth2\Exceptions\UnknownLeagueProviderException;
use HappyInvite\Domain\Bundles\OAuth2\Request\OAuth2Request;
use HappyInvite\Domain\Bundles\OAuth2\Request\Types\FacebookOAuth2Request;
use HappyInvite\Domain\Bundles\OAuth2\Request\Types\GoogleOAuth2Request;
use HappyInvite\Domain\Bundles\OAuth2\Request\Types\VkontakteOAuth2Request;
use HappyInvite\Domain\Bundles\OAuth2\Request\Types\YandexOAuth2Request;
use HappyInvite\Domain\Bundles\OAuth2\Service\OAuth2ProviderService;
use J4k\OAuth2\Client\Provider\Vkontakte;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Token\AccessToken;

final class OAuth2RequestFactory
{
    /** @var OAuth2ProviderService */
    private $oAuth2ProviderRepository;

    public function __construct(OAuth2ProviderService $oAuth2ProviderRepository)
    {
        $this->oAuth2ProviderRepository = $oAuth2ProviderRepository;
    }

    public function createOAuth2Request(AbstractProvider $leagueProvider, AccessToken $accessToken): OAuth2Request
    {
        switch(get_class($leagueProvider)) {
            default:
                throw new UnknownLeagueProviderException(sprintf('No idea how to create request handler for `%s`', get_class($leagueProvider)));

            case Google::class:
                return new GoogleOAuth2Request($leagueProvider, $accessToken);

            case Yandex::class:
                return new YandexOAuth2Request($leagueProvider, $accessToken);

            case Facebook::class:
                return new FacebookOAuth2Request($leagueProvider, $accessToken);

            case Vkontakte::class:
                return new VkontakteOAuth2Request($leagueProvider, $accessToken);
        }
    }
}