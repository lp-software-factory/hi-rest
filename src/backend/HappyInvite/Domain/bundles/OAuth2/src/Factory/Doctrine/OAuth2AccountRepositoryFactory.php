<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Factory\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\OAuth2\Entity\OAuth2Account;

final class OAuth2AccountRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return OAuth2Account::class;
    }
}