<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Factory;

use HappyInvite\Domain\Bundles\OAuth2\Config\ListAvailableLeagueProviders;
use HappyInvite\Domain\Bundles\OAuth2\Service\OAuth2ProviderService;
use League\OAuth2\Client\Provider\AbstractProvider;

final class LeagueProviderFactory
{
    /** @var ListAvailableLeagueProviders */
    private $listAvailable;

    /** @var OAuth2ProviderService */
    private $oauth2ProviderService;

    public function __construct(
        ListAvailableLeagueProviders $listAvailable,
        OAuth2ProviderService $oauth2ProviderService
    ) {
        $this->listAvailable = $listAvailable;
        $this->oauth2ProviderService = $oauth2ProviderService;
    }

    public function createLeagueProviderFromCode(string $code): AbstractProvider
    {
        $oAuth2ProviderEntity = $this->oauth2ProviderService->getProviderByCode($code);

        return $this->createLeagueProvider($oAuth2ProviderEntity->getHandler(), $oAuth2ProviderEntity->getConfig());
    }

    public function createLeagueProvider(string $leagueProviderClassName, array $options = [], array $collaborators = []): AbstractProvider
    {
        if(is_subclass_of($leagueProviderClassName, AbstractProvider::class)) {
            return new $leagueProviderClassName($options, $collaborators);
        }else{
            throw new \Exception(sprintf('Class `%s` is not a League OAuth2 adapter', $leagueProviderClassName));
        }
    }

    public function listAvailable(): array
    {
        return $this->listAvailable->getAvailable();
    }
}