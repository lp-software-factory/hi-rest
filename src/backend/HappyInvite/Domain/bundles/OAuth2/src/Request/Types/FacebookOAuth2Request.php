<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Request\Types;

use HappyInvite\Domain\Bundles\OAuth2\Request\OAuth2RegistrationRequest;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Provider\FacebookUser;

final class FacebookOAuth2Request extends AbstractOAuth2Request
{
    public const PROVIDER_CODE = 'facebook';

    public function getCode(): string
    {
        return self::PROVIDER_CODE;
    }

    public function getLeagueProviderClassName(): string
    {
        return Facebook::class;
    }

    public static function createLeagueProvider(array $options = [], array $collaborators = []): AbstractProvider
    {
        return new Facebook($options, $collaborators);
    }

    public function createRegistrationRequest(): OAuth2RegistrationRequest
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof FacebookUser) {
            return new OAuth2RegistrationRequest($resourceOwner, $resourceOwner->getEmail());
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }

    public function setupProfile(Profile $profile)
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof FacebookUser) {
            $profile->changeName(
                $resourceOwner->getFirstName(),
                $resourceOwner->getLastName()
            );
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }
}