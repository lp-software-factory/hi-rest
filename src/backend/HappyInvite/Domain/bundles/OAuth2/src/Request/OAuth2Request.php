<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Request;

use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;

interface OAuth2Request
{
    public function __construct(AbstractProvider $leagueProvider, AccessToken $accessToken);
    public function getCode(): string;
    public function getLeagueProviderClassName(): string;
    public function getResourceOwner(): ResourceOwnerInterface;
    public function getResourceOwnerId(): string;
    public function getAccessToken(): AccessToken;
    public function getCreateProfileParameters(): CreateProfileParameters;
    public static function createLeagueProvider(array $options = [], array $collaborators = []);
    public function createRegistrationRequest(): OAuth2RegistrationRequest;
    public function setupProfile(Profile $profile);
}