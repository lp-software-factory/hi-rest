<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Request\Types;

use HappyInvite\Domain\Bundles\OAuth2\Request\OAuth2RegistrationRequest;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Provider\GoogleUser;

final class GoogleOAuth2Request extends AbstractOAuth2Request
{
    public const PROVIDER_CODE = 'google';

    public function getCode(): string
    {
        return self::PROVIDER_CODE;
    }

    public function getLeagueProviderClassName(): string
    {
        return Google::class;
    }

    public static function createLeagueProvider(array $options = [], array $collaborators = []): AbstractProvider
    {
        return new Google($options, $collaborators);
    }

    public function createRegistrationRequest(): OAuth2RegistrationRequest
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof GoogleUser) {
            return new OAuth2RegistrationRequest($resourceOwner, $resourceOwner->getEmail());
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }

    public function getCreateProfileParameters(): CreateProfileParameters
    {
        $resourceOwner = $this->getResourceOwner();

        if($resourceOwner instanceof GoogleUser) {
            return new CreateProfileParameters(
                $resourceOwner->getFirstName(),
                $resourceOwner->getLastName()
            );
        }else{
            throw new \Exception('Invalid resourceOwner');
        }
    }

    public function setupProfile(Profile $profile)
    {
    }
}