<?php
namespace HappyInvite\Domain\Bundles\OAuth2\Service;

use HappyInvite\Domain\Bundles\OAuth2\Entity\OAuth2Provider;
use HappyInvite\Domain\Bundles\OAuth2\Exceptions\DuplicateOAuth2ProviderException;
use HappyInvite\Domain\Bundles\OAuth2\Parameters\CreateOAuth2ProviderParameters;
use HappyInvite\Domain\Bundles\OAuth2\Parameters\SetOAuth2ConfigParameters;
use HappyInvite\Domain\Bundles\OAuth2\Repository\OAuth2ProviderRepository;

final class OAuth2ProviderService
{
    /** @var OAuth2ProviderRepository */
    private $oath2ProviderRepository;

    public function __construct(OAuth2ProviderRepository $oath2ProviderRepository)
    {
        $this->oath2ProviderRepository = $oath2ProviderRepository;
    }

    public function createOAuth2Provider(CreateOAuth2ProviderParameters $parameters): OAuth2Provider
    {
        if($this->hasProviderWithCode($parameters->getCode())) {
            throw new DuplicateOAuth2ProviderException(sprintf('OAuth2 provider with code `%s` already exists'));
        }

        $oauth2Provider = new OAuth2Provider(
            $parameters->getCode(),
            $parameters->getHandler(),
            $parameters->getConfig()
        );

        $this->oath2ProviderRepository->createOAuth2Provider($oauth2Provider);

        return $oauth2Provider;
    }

    public function deleteOAuth2Provider(string $code)
    {
        $this->oath2ProviderRepository->deleteOAuth2Provider(
            $this->oath2ProviderRepository->getByProviderCode($code)
        );
    }

    public function setConfigFor(string $code, SetOAuth2ConfigParameters $parameters)
    {
        $oAuth2Provider = $this->getProviderByCode($code);
        $oAuth2Provider->setConfig($parameters->getConfig());

        $this->oath2ProviderRepository->saveOAuth2Provider($oAuth2Provider);

        return $oAuth2Provider;
    }

    public function hasProviderWithCode(string $code): bool
    {
        return $this->oath2ProviderRepository->hasProviderWithCode($code);
    }

    public function getProviderByCode(string $code): OAuth2Provider
    {
        return $this->oath2ProviderRepository->getByProviderCode($code);
    }

    public function getAllProviders(): array
    {
        return $this->oath2ProviderRepository->getAll();
    }
}