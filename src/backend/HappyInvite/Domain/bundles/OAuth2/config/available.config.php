<?php
namespace HappyInvite\Domain\Bundles\OAuth2;

use Aego\OAuth2\Client\Provider\Yandex;
use J4k\OAuth2\Client\Provider\Vkontakte;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Provider\Google;

return [
    'config.oauth2.league.available' => [
        Google::class,
        Yandex::class,
        Facebook::class,
        Vkontakte::class,
    ]
];