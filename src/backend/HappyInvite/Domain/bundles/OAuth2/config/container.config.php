<?php
namespace HappyInvite\Domain\Bundles\Mail;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\OAuth2\Config\ListAvailableLeagueProviders;
use HappyInvite\Domain\Bundles\OAuth2\Factory\Doctrine\OAuth2AccountRepositoryFactory;
use HappyInvite\Domain\Bundles\OAuth2\Factory\Doctrine\OAuth2ProviderRepositoryFactory;
use HappyInvite\Domain\Bundles\OAuth2\Repository\OAuth2AccountRepository;
use HappyInvite\Domain\Bundles\OAuth2\Repository\OAuth2ProviderRepository;

return [
    ListAvailableLeagueProviders::class => object()->constructorParameter('available', get('config.oauth2.league.available')),
    OAuth2AccountRepository::class => factory(OAuth2AccountRepositoryFactory::class),
    OAuth2ProviderRepository::class => factory(OAuth2ProviderRepositoryFactory::class),
];