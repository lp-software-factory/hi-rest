<?php
namespace HappyInvite\Domain\Bundles\Fonts\Factory\Repository;

use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;
use HappyInvite\Domain\Bundles\Fonts\Entity\Font;

final class FontRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Font::class;
    }
}