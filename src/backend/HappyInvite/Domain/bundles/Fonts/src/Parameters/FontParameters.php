<?php
namespace HappyInvite\Domain\Bundles\Fonts\Parameters;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;

final class FontParameters
{
    /** @var Attachment */
    private $preview;

    /** @var string */
    private $title;

    /** @var int */
    private $supports;

    /** @var string */
    private $fontFace;

    /** @var string */
    private $license;

    /** @var int[] */
    private $files;

    /** @var bool */
    private $isActivated;

    public function __construct(
        Attachment $preview,
        string $title,
        int $supports,
        string $fontFace,
        string $license,
        array $files,
        bool $isActivated
    ) {
        $this->preview = $preview;
        $this->title = $title;
        $this->supports = $supports;
        $this->fontFace = $fontFace;
        $this->license = $license;
        $this->files = $files;
        $this->isActivated = $isActivated;
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSupports(): int
    {
        return $this->supports;
    }

    public function getFontFace(): string
    {
        return $this->fontFace;
    }

    public function getLicense(): string
    {
        return $this->license;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function isActivated(): bool
    {
        return $this->isActivated;
    }
}