<?php
namespace HappyInvite\Domain\Bundles\Fonts\Service;

use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\Domain\Bundles\Fonts\Parameters\FontParameters;
use HappyInvite\Domain\Bundles\Fonts\Repository\FontRepository;

final class FontService
{
    /** @var FontRepository */
    private $fontRepository;

    public function __construct(
        FontRepository $fontRepository
    ) {
        $this->fontRepository = $fontRepository;
    }

    public function createFont(FontParameters $parameters): Font
    {
        $font = new Font(
            $parameters->getPreview(),
            $parameters->getTitle(),
            $parameters->getSupports(),
            $parameters->getFontFace(),
            $parameters->getLicense(),
            $parameters->getFiles()
        );

        $this->fontRepository->createFont($font);

        return $font;
    }

    public function editFont(int $fontId, FontParameters $parameters): Font
    {
        $font = $this->fontRepository->getById($fontId);
        $font->setPreview($parameters->getPreview())
             ->setTitle($parameters->getTitle())
             ->setSupports($parameters->getSupports())
             ->setFontFace($parameters->getFontFace())
             ->setLicense($parameters->getLicense())
             ->setFiles($parameters->getFiles());

        $this->fontRepository->saveFont($font);

        return $font;
    }

    public function deleteFont(int $fontId)
    {
        $this->fontRepository->deleteFont(
            $this->fontRepository->getById($fontId)
        );
    }

    public function getAll(): array
    {
        return $this->fontRepository->getAll();
    }

    public function getFontById(int $id): Font
    {
        return $this->fontRepository->getById($id);
    }

    public function hasFontWithId(int $fontId): bool
    {
        return $this->fontRepository->hasFontWithId($fontId);
    }

}