<?php
namespace HappyInvite\Domain\Bundles\Fonts;

use HappyInvite\Platform\Bundles\HIBundle;

final class FontsDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}