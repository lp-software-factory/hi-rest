<?php
namespace HappyInvite\Domain\Bundles\Fonts\Entity;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntity;
use HappyInvite\Domain\Markers\ActivatedEntity\ActivatedEntityTrait;
use HappyInvite\Domain\Markers\IdEntity\IdEntity;
use HappyInvite\Domain\Markers\IdEntity\IdEntityTrait;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntity;
use HappyInvite\Domain\Markers\JSONMetadataEntity\JSONMetadataEntityTrait;
use HappyInvite\Domain\Markers\JSONSerializable;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntity;
use HappyInvite\Domain\Markers\ModificationEntity\ModificationEntityTrait;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntity;
use HappyInvite\Domain\Markers\SIDEntity\SIDEntityTrait;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntity;
use HappyInvite\Domain\Markers\VersionEntity\VersionEntityTrait;

/**
 * @Entity(repositoryClass="HappyInvite\Domain\Bundles\Fonts\Repository\FontRepository")
 * @Table(name="font")
 */
class Font implements JSONSerializable, IdEntity, SIDEntity, ModificationEntity, JSONMetadataEntity, ActivatedEntity, VersionEntity
{
    public const LATEST_VERSION = '1.0.0';

    public const MODE_ALL          = self::MODE_NORMAL | self::MODE_BOLD | self::MODE_ITALIC | self::MODE_BOLD_ITALIC;
    public const MODE_NORMAL       = 0b00000001;
    public const MODE_BOLD         = 0b00000010;
    public const MODE_ITALIC       = 0b00000100;
    public const MODE_BOLD_ITALIC  = 0b00001000;

    use IdEntityTrait, SIDEntityTrait, ModificationEntityTrait, JSONMetadataEntityTrait, ActivatedEntityTrait, VersionEntityTrait;

    /**
     * @OneToOne(targetEntity="HappyInvite\Domain\Bundles\Attachment\Entity\Attachment")
     * @JoinColumn(name="preview_attachment_id", referencedColumnName="id")
     * @var Attachment
     */
    private $preview;

    /**
     * @Column(name="title", type="string")
     * @var string
     */
    private $title;

    /**
     * @Column(name="supports", type="integer")
     * @var int
     */
    private $supports;

    /**
     * @Column(name="font_face", type="string")
     * @var string
     */
    private $fontFace;

    /**
     * @Column(name="license", type="string")
     * @var int
     */
    private $license;

    /**
     * @Column(name="files", type="json_array")
     * @var int[]
     */
    private $files = [];

    public function __construct(
        Attachment $preview,
        string $title,
        int $supports,
        string $fontFace,
        string $license,
        array $files
    ) {
        $this->setCurrentVersion(self::LATEST_VERSION);

        $this->title = $title;
        $this->preview = $preview;
        $this->supports = $supports;
        $this->fontFace = $fontFace;
        $this->license = $license;
        $this->files = $files;

        $this->initModificationEntity();
        $this->regenerateSID();
    }

    public function toJSON(array $options = []): array
    {
        return [
            "id" => $this->getIdNoFall(),
            "sid" => $this->getSID(),
            "metadata" => array_merge($this->getMetadata(), [
                'version' => $this->getMetadataVersion(),
            ]),
            'date_created_at' => $this->getDateCreatedAt()->format(\DateTime::RFC2822),
            'last_updated_on' => $this->getLastUpdatedOn()->format(\DateTime::RFC2822),
            'is_activated' => $this->isActivated(),
            'preview' => $this->getPreview()->toJSON($options),
            'title' => $this->getTitle(),
            'supports' => $this->getSupports(),
            'font_face' => $this->getFontFace(),
            'license' => $this->getLicense(),
            'files' => $this->getFiles(),
        ];
    }

    public function getMetadataVersion(): string
    {
        return "1.0.0";
    }

    public function getPreview(): Attachment
    {
        return $this->preview;
    }

    public function setPreview(Attachment $preview): self
    {
        $this->preview = $preview;
        $this->markAsUpdated();

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        $this->markAsUpdated();

        return $this;
    }

    public function getSupports(): int
    {
        return $this->supports;
    }

    public function setSupports(int $supports): self
    {
        $this->supports = $supports;
        $this->markAsUpdated();

        return $this;
    }

    public function getFontFace(): string
    {
        return $this->fontFace;
    }

    public function setFontFace(string $fontFace): self
    {
        $this->fontFace = $fontFace;
        $this->markAsUpdated();

        return $this;
    }

    public function getLicense(): string
    {
        return $this->license;
    }

    public function setLicense(string $license): self
    {
        $this->license = $license;
        $this->markAsUpdated();

        return $this;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function setFiles(array $files): self
    {
        $this->files = $files;
        $this->markAsUpdated();

        return $this;
    }
}