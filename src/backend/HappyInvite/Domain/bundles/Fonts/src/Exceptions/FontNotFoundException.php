<?php
namespace HappyInvite\Domain\Bundles\Fonts\Exceptions;

final class FontNotFoundException extends \Exception {}