<?php
namespace HappyInvite\Domain\Bundles\Fonts\Definitions;

use HappyInvite\Domain\Markers\JSONSerializable;

final class FontDefinition implements JSONSerializable
{
    /** @var string */
    private $title;

    /** @var string */
    private $css;

    public function __construct(string $title, string $css)
    {
        $this->title = $title;
        $this->css = $css;
    }

    public static function fromJSON(array $json): FontDefinition
    {
        return new self($json['title'], $json['css']);
    }

    public function toJSON(array $options = []): array
    {
        return [
            'title' => $this->getTitle(),
            'css' => $this->getCSS(),
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCSS(): string
    {
        return $this->css;
    }
}
