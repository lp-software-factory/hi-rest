<?php
namespace HappyInvite\Domain\Bundles\Fonts\Formatter;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Fonts\Entity\Font;

final class FontFormatter
{
    /** @var AttachmentService */
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function formatOne(Font $font): array
    {
        $json = $font->toJSON();

        $json['files'] = array_map(function(Attachment $attachment) {
            return $attachment->toJSON();
        }, $this->attachmentService->getManyByIds($json['files']));

        return [
            'entity' => $json,
        ];
    }

    public function formatMany(array $fonts): array
    {
        return array_map(function(Font $font) {
            return $this->formatOne($font);
        }, $fonts);
    }
}