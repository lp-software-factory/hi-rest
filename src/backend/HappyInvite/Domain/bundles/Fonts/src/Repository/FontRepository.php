<?php
namespace HappyInvite\Domain\Bundles\Fonts\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\Domain\Bundles\Fonts\Exceptions\FontNotFoundException;

final class FontRepository extends EntityRepository 
{
    public function createFont(Font $font): int
    {
        while($this->hasWithSID($font->getSID())) {
            $font->regenerateSID();
        }

        $this->getEntityManager()->persist($font);
        $this->getEntityManager()->flush($font);

        return $font->getId();
    }

    public function saveFont(Font $font)
    {
        $this->getEntityManager()->flush($font);
    }

    public function deleteFont(Font $font)
    {
        $this->getEntityManager()->remove($font);
        $this->getEntityManager()->flush($font);
    }

    public function getById(int $id): Font
    {
        $result = $this->find($id);

        if($result instanceof Font) {
            return $result;
        }else{
            throw new FontNotFoundException(sprintf('Font `ID(%d)` not found', $id));
        }
    }

    public function getAll(): array
    {
        return $this->findBy([], ['id' => 'asc']);
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new FontNotFoundException(sprintf(
                'Font with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(Font $font) {
                    return $font->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasFontWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): Font
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof Font) {
            return $result;
        }else{
            throw new FontNotFoundException(sprintf('Font `SID(%s)` not found', $sid));
        }
    }
}
