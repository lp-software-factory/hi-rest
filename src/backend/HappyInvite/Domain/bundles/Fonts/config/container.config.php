<?php
namespace HappyInvite\Domain\Bundles\Fonts;

use function DI\object;
use function DI\factory;
use function DI\get;

use HappyInvite\Domain\Bundles\Fonts\Factory\Repository\FontRepositoryFactory;
use HappyInvite\Domain\Bundles\Fonts\Repository\FontRepository;

return [
    FontRepository::class => (factory(FontRepositoryFactory::class)),
];