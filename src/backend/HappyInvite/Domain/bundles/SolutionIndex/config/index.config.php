<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex;

return [
    'config.hi.domain.solution-index.ensureIndexes' => [
        ['key' => ['id' => 1], 'unique' => true],
        ['key' => ['solution.id' => 1], 'unique' => true],
        ['key' => ['solution.date_created_at' => 1]],
        ['key' => ['solution.last_updated_on'=> 1]],
        ['key' => ['filter.event_type_ids' => 1]],
        ['key' => ['filter.has_photo' => 1]],
        ['key' => ['filter.gamma_ids' => 1]],
        ['key' => ['filter.size_ids' => 1]],
        ['key' => ['filter.style_ids' => 1]],
        ['key' => ['filter.is_public' => 1]],
        ['key' => ['filter.author_profile_ids' => 1]],
    ]
];