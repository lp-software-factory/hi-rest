<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexDatabase;

return [
    SolutionIndexDatabase::class => object()
        ->constructorParameter('indexes', get('config.hi.domain.solution-index.ensureIndexes')),
];