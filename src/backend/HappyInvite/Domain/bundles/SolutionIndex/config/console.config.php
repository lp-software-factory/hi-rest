<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex;

use HappyInvite\Domain\Bundles\SolutionIndex\Console\Command\SolutionIndexAllCommand;
use HappyInvite\Domain\Bundles\SolutionIndex\Console\Command\SolutionIndexClearCommand;
use HappyInvite\Domain\Bundles\SolutionIndex\Console\Command\SolutionIndexDeleteCommand;
use HappyInvite\Domain\Bundles\SolutionIndex\Console\Command\SolutionIndexEntityCommand;

return [
    'config.console' => [
        'commands' => [
            'Version' => [
                SolutionIndexAllCommand::class,
                SolutionIndexClearCommand::class,
                SolutionIndexDeleteCommand::class,
                SolutionIndexEntityCommand::class,
            ],
        ],
    ],
];