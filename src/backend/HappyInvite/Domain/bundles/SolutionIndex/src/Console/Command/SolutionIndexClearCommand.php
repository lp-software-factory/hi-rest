<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Console\Command;

use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SolutionIndexClearCommand extends Command
{
    /** @var SolutionService */
    private $solutionService;

    /** @var SolutionIndexService */
    private $solutionIndexService;

    public function __construct(
        SolutionService $solutionService,
        SolutionIndexService $solutionIndexService
    ) {
        $this->solutionService = $solutionService;
        $this->solutionIndexService = $solutionIndexService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('solution:index:clear')
            ->setDescription('[DANGER] Completely destroy index. You have to solution:index:all to restore solutions index')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Destroying index...');
        $this->solutionIndexService->clear();
        $output->writeln('Done. Run "hi-console.sh solution:index:all" to reindex solutions');
    }
}