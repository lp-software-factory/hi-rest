<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Console\Command;

use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SolutionIndexDeleteCommand extends Command
{
    /** @var SolutionService */
    private $solutionService;

    /** @var SolutionIndexService */
    private $solutionIndexService;

    public function __construct(
        SolutionService $solutionService,
        SolutionIndexService $solutionIndexService
    ) {
        $this->solutionService = $solutionService;
        $this->solutionIndexService = $solutionIndexService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('solution:index:index')
            ->setDescription('(Re)index solution with given id')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('id', InputArgument::REQUIRED, 'Solution ID'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $solutionId = (int) $input->getArgument('id');

        $output->writeln(sprintf('Deleting solution(ID: %d) from index...', $solutionId));
        $this->solutionIndexService->delete($solutionId);
        $output->writeln('Done');
    }
}