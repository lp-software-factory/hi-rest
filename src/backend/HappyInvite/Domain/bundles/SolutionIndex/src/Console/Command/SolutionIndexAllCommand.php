<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Console\Command;

use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SolutionIndexAllCommand extends Command
{
    /** @var SolutionService */
    private $solutionService;

    /** @var SolutionIndexService */
    private $solutionIndexService;

    public function __construct(
        SolutionService $solutionService,
        SolutionIndexService $solutionIndexService
    ) {
        $this->solutionService = $solutionService;
        $this->solutionIndexService = $solutionIndexService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('solution:index:all')
            ->setDescription('Reindex all existed solutions. Does not destroy old solutions!')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Reindexing...');

        $this->solutionIndexService->fullReindex(function(Solution $entity) use ($output) {
            $output->writeln(sprintf(' [*] Indexed: %d', $entity->getId()));
        });

        $output->writeln('Done');
    }
}