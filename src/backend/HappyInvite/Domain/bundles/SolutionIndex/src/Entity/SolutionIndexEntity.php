<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Entity;

use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use MongoDB\BSON\ObjectID;

final class SolutionIndexEntity
{
    /** @var ObjectID */
    private $_id;

    /** @var Solution */
    private $solution;

    /** @var array */
    private $filter;

    public function __construct(ObjectID $_id, array $filter, Solution $solution)
    {
        $this->_id = $_id;
        $this->filter = $filter;
        $this->solution = $solution;
    }

    public function getId(): ObjectID
    {
        return $this->_id;
    }

    public function getSolution(): Solution
    {
        return $this->solution;
    }

    public function getFilter(): array
    {
        return $this->filter;
    }
}