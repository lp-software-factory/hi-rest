<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\SolutionIdsCriteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

final class SolutionIdsCriteria implements Criteria
{
    public function isAvailable(array $json): bool
    {
        return isset($json['ids']);
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $filter['id'] = [
            '$in' => $query->getJson()['ids']
        ];
    }
}