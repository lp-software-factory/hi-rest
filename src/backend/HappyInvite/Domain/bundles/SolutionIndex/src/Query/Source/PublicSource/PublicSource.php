<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\PublicSource;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\Source;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexDatabase;
use MongoDB\Driver\Cursor;

final class PublicSource implements Source
{
    public static function getName(): string
    {
        return 'public';
    }

    public function execute(SolutionIndexDatabase $database, Query $query): Cursor
    {
        $filter = [];
        $options = [];

        array_map(function(Criteria $criteria) use (&$filter, &$options, $query) {
            $criteria->apply($query, $filter, $options);
        }, $query->getCriteria());

        $filter['filter.is_public'] = true;

        return $database->getIndexCollection()->find($filter, $options);
    }
}