<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\HasPhotoCriteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

final class HasPhotoCriteria implements Criteria
{
    public function isAvailable(array $json): bool
    {
        return isset($json['has_photo']);
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $filter['filter.has_photo'] = (bool) $query->getJson()['has_photo'];
    }
}