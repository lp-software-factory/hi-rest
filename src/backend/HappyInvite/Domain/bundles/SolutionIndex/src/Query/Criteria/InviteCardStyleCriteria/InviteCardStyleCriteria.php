<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\InviteCardStyleCriteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

final class InviteCardStyleCriteria implements Criteria
{
    public function isAvailable(array $json): bool
    {
        return isset($json['invite_card_style_ids']);
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $filter['filter.invite_card_style_ids'] = [
            '$in' => [(int) $query->getJson()['invite_card_style_ids']]
        ];
    }
}