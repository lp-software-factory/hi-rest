<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\AuthorProfileCriteria;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

final class AuthorProfileCriteria implements Criteria
{
    /** @var AuthToken */
    private $authToken;

    /** @var DesignerService */
    private $designerService;

    public function isAvailable(array $json): bool
    {
        return isset($json['author_profile_ids']);
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $authorProfileIds = $query->getJson()['author_profile_ids'];

        foreach($authorProfileIds as $authorProfileId) {
            if(! $this->hasAccess($authorProfileId)) {
                throw new \Exception(sprintf('You have no access to solutions of Profile(ID: %d)', $authorProfileId));
            }
        }

        $filter['filter.author_profile_ids'] = [
            '$in' => $authorProfileIds,
        ];
    }

    private function hasAccess(int $authorProfileId): bool
    {
        if($this->authToken->isSignedIn() && $this->authToken->getProfile()->getId() === $authorProfileId) {
            return true;
        }else if($this->authToken->isSignedIn() && $this->authToken->getProfile()->getAccount()->hasAccess('admin')) {
            return true;
        }else if($this->designerService->hasDesignerWithProfileId($authorProfileId)) {
            return true;
        }else{
            return false;
        }
    }
}