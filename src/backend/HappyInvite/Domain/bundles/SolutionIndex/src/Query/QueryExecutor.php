<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query;

use HappyInvite\Domain\Bundles\SolutionIndex\Converter\BSONDocumentToSolutionIndexEntityConverter;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexDatabase;
use MongoDB\Driver\Cursor;

final class QueryExecutor
{
    public const CURSOR_LIMIT = 1000;

    /** @var SolutionIndexDatabase */
    private $database;

    /** @var BSONDocumentToSolutionIndexEntityConverter */
    private $converter;

    public function __construct(
        SolutionIndexDatabase $database,
        BSONDocumentToSolutionIndexEntityConverter $converter
    ) {
        $this->database = $database;
        $this->converter = $converter;
    }

    public function execute(Query $query): array
    {
        $results = $query->getSource()->execute($this->database, $query);

        return $this->convertResults($results);
    }

    private function convertResults(Cursor $cursor): array
    {
        $numResults = 0;
        $documents = [];

        foreach($cursor as $document) {
            if(++$numResults > self::CURSOR_LIMIT) {
                throw new \Exception(sprintf('Limit %d exceeded', self::CURSOR_LIMIT));
            }

            $documents[] = $document;
        }

        return $this->converter->convertMany($documents);
    }
}