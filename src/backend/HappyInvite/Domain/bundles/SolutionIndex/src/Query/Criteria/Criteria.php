<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

interface Criteria
{
    public function isAvailable(array $json): bool;
    public function apply(Query $query, array &$filter, array &$options): void;
}