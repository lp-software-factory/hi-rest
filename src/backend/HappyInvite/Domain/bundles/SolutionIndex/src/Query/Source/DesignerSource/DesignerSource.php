<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\DesignerSource;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\Source;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexDatabase;
use MongoDB\Driver\Cursor;

final class DesignerSource implements Source
{
    /** @var int */
    private $designerId;

    public function __construct(int $designerId = null)
    {
        $this->designerId = $designerId;
    }

    public static function getName(): string
    {
        return 'designer';
    }

    public function withDesignerId(int $designerId)
    {
        return new self($designerId);
    }

    public function getDesignerId(): int
    {
        if($this->designerId === null) {
            throw new \Exception('Prototype is not a valid source');
        }

        return $this->designerId;
    }

    public function execute(SolutionIndexDatabase $database, Query $query): Cursor
    {
        $filter = [];
        $options = [];

        array_map(function(Criteria $criteria) use (&$filter, &$options, $query) {
            $criteria->apply($query, $filter, $options);
        }, $query->getCriteria());

        $filter['filter.author_profile_ids'] = [
            '$in' => [$this->getDesignerId()],
        ];

        return $database->getIndexCollection()->find($filter, $options);
    }
}