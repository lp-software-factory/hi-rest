<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\EventTypeCriteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

final class EventTypeCriteria implements Criteria
{
    public function isAvailable(array $json): bool
    {
        return isset($json['event_type_ids']);
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $filter['filter.event_type_ids'] = [
            '$in' => [(int) $query->getJson()['event_type_ids']]
        ];
    }
}