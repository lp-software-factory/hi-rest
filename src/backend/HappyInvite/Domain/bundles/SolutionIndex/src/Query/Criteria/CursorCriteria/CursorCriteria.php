<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\CursorCriteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;
use MongoDB\BSON\ObjectID;

final class CursorCriteria implements Criteria
{
    public const LIMIT_SORTS = 3;

    public const DEFAULT_LIMIT = 100;
    public const MAX_LIMIT = 1000;
    public const DEFAULT_SORT_ORDER = self::SORT_ORDER_DESC;
    public const DEFAULT_SORT_FIELD = '_id';
    public const DEFAULT_FROM_SIGN = '>';

    public const SORT_ORDER_ASC = 1;
    public const SORT_ORDER_DESC = -1;

    public function isAvailable(array $json): bool
    {
        return true;
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $json = $query->getJson();

        $hasCursor = isset($json['cursor']['from']) && isset($json['cursor']['from']['_id']);
        $hasSort = isset($json['cursor']['sort']);

        $options['sort'] = [];

        if($hasSort) {
            $numSorts = 0;

            foreach($json['cursor']['sort'] as $sortInstruction) {
                if(++$numSorts > self::LIMIT_SORTS) {
                    throw new \Exception(sprintf('Too many sorts, max %d allowed', self::LIMIT_SORTS));
                }

                $options['sort'][$sortInstruction['field'] ?? self::DEFAULT_SORT_FIELD] = $sortInstruction['order'] ?? self::DEFAULT_SORT_ORDER;
            }
        }else{
            $options['sort'][self::DEFAULT_SORT_FIELD] = self::DEFAULT_SORT_ORDER;
        }

        if($hasCursor) {
            $sign = $json['cursor']['from']['sign'] ?? self::DEFAULT_FROM_SIGN;
            $_id = new ObjectID($json['cursor']['from']['_id']);

            if($sign === '>') {
                $filter['_id'] = [
                    '$gt' => $_id,
                ];
            }else if($sign === '<') {
                $filter['_id'] = [
                    '$lt' => $_id,
                ];
            }else{
                throw new \Exception(sprintf('Invalid order "%s"', $sign));
            }
        }

        $limit = $json['cursor']['limit'] ?? self::DEFAULT_LIMIT;

        if($limit < 0 || $limit > self::MAX_LIMIT) {
            throw new \Exception(sprintf('Limit should be in range (0, %d)', self::MAX_LIMIT));
        }

        $options['limit'] = $limit;
    }
}