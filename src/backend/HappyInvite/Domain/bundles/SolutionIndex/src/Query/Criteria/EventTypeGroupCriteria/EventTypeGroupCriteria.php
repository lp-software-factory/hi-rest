<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\EventTypeGroupCriteria;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;

final class EventTypeGroupCriteria implements Criteria
{
    public function isAvailable(array $json): bool
    {
        return isset($json['event_type_group_ids']);
    }

    public function apply(Query $query, array &$filter, array &$options): void
    {
        $filter['filter.event_type_group_ids'] = [
            '$in' => [(int) $query->getJson()['event_type_group_ids']]
        ];
    }
}