<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Source;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerService;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\DesignerSource\DesignerSource;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\PersonalSource\PersonalSource;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\PublicSource\PublicSource;
use Interop\Container\ContainerInterface;

final class SourceFactory
{
    /** @var ContainerInterface */
    private $container;

    /** @var AuthToken */
    private $authToken;

    public function __construct(AuthToken $authToken, ContainerInterface $container)
    {
        $this->authToken = $authToken;
        $this->container = $container;
    }

    public function createSourceFromString(string $source): Source
    {
        return $this->getPrototype($source);
    }

    private function getPrototype(string $source): Source
    {
        if($source === PublicSource::getName()) {
            return $this->container->get(PublicSource::class);
        }else if($source === PersonalSource::getName()) {
            /** @var PersonalSource $designerSource */
            $personalSource = $this->container->get(PersonalSource::class);

            return $personalSource->withPersonalId($this->authToken->getProfile()->getId());
        }else if(preg_match_all(sprintf('/%s\-(\d+)/', PersonalSource::getName()), $source, $matches)) {
            /** @var PersonalSource $designerSource */
            $personalSource = $this->container->get(PersonalSource::class);

            return $personalSource->withPersonalId((int) $matches[1][0]);
        }else if($source === DesignerSource::getName()) {
            /** @var DesignerSource $designerSource */
            $designerSource = $this->container->get(DesignerSource::class);

            return $designerSource->withDesignerId($this->authToken->getProfile()->getId());
        }else if(preg_match_all(sprintf('/%s\-(\d+)/', DesignerSource::getName()), $source, $matches)) {
            /** @var DesignerSource $designerSource */
            $designerSource = $this->container->get(DesignerSource::class);

            return $designerSource->withDesignerId((int) $matches[1][0]);
        }else{
            throw new \Exception(sprintf('Invalid source "%s"', $source));
        }
    }
}