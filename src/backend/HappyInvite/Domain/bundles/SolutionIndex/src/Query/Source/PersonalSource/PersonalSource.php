<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\PersonalSource;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\Source;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexDatabase;
use MongoDB\Driver\Cursor;

final class PersonalSource implements Source
{
    /** @var int */
    private $profileId;

    public function __construct(int $profileId = null)
    {
        $this->profileId = $profileId;
    }

    public static function getName(): string
    {
        return 'personal';
    }

    public function withPersonalId(int $personalId)
    {
        return new self($personalId);
    }

    public function getProfileId(): int
    {
        if($this->profileId === null) {
            throw new \Exception('Prototype is not a valid source');
        }

        return $this->profileId;
    }

    public function execute(SolutionIndexDatabase $database, Query $query): Cursor
    {
        $filter = [];
        $options = [];

        array_map(function(Criteria $criteria) use (&$filter, &$options, $query) {
            $criteria->apply($query, $filter, $options);
        }, $query->getCriteria());

        $filter['filter.author_profile_ids'] = [
            '$in' => [$this->getProfileId()],
        ];

        return $database->getIndexCollection()->find($filter, $options);
    }
}