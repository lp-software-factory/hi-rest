<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query\Source;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Query;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexDatabase;
use MongoDB\Driver\Cursor;

interface Source
{
    public static function getName(): string;
    public function execute(SolutionIndexDatabase $database, Query $query): Cursor;
}