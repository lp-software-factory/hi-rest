<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query;

use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\Source;

final class Query
{
    /** @var Source */
    private $source;

    /** @var array */
    private $json;

    /** @var Criteria[] */
    private $criteria = [];

    public function __construct(Source $source, array $json, array $criteria)
    {
        $this->source = $source;
        $this->json = $json;

        foreach($criteria as $c) {
            $this->criteria[get_class($c)] = $c;
        }
    }

    public function getSource(): Source
    {
        return $this->source;
    }

    public function getCriteria(): array
    {
        return $this->criteria;
    }

    public function getJson(): array
    {
        return $this->json;
    }

    public function withCriteria(string $criteriaClassName, Callable $callback): bool
    {
        if(isset($this->criteria[$criteriaClassName])) {
            $callback($this->criteria[$criteriaClassName]);

            return true;
        }else{
            return false;
        }
    }
}