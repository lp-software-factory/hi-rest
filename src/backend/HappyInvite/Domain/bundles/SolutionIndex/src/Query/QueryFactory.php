<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Query;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\AuthorProfileCriteria\AuthorProfileCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\Criteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\CursorCriteria\CursorCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\EventTypeCriteria\EventTypeCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\EventTypeGroupCriteria\EventTypeGroupCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\HasPhotoCriteria\HasPhotoCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\SolutionIdsCriteria\SolutionIdsCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\InviteCardGammaCriteria\InviteCardGammaCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\InviteCardSizeCriteria\InviteCardSizeCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Criteria\InviteCardStyleCriteria\InviteCardStyleCriteria;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\SourceFactory;
use Interop\Container\ContainerInterface;

final class QueryFactory
{
    /** @var ContainerInterface */
    private $container;

    /** @var SourceFactory */
    private $sourceFactory;

    public function __construct(ContainerInterface $container, SourceFactory $sourceFactory)
    {
        $this->container = $container;
        $this->sourceFactory = $sourceFactory;
    }

    public function createQueryFromJSON(string $source, array $json): Query
    {
        $criteria = Chain::create($this->getCriteria())
            ->filter(function(Criteria $criteria) use ($json) {
                return $criteria->isAvailable($json);
            })
            ->array;

        return new Query($this->sourceFactory->createSourceFromString($source), $json, $criteria);
    }

    private function getCriteria(): array
    {
        return array_map(function(string $input): Criteria {
            return $this->container->get($input);
        }, [
            SolutionIdsCriteria::class,
            AuthorProfileCriteria::class,
            CursorCriteria::class,
            EventTypeCriteria::class,
            EventTypeGroupCriteria::class,
            InviteCardGammaCriteria::class,
            InviteCardSizeCriteria::class,
            InviteCardStyleCriteria::class,
            HasPhotoCriteria::class,
        ]);
    }
}