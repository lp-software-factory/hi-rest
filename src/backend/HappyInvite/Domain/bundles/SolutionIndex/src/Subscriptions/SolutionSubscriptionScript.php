<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Subscriptions;

use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class
SolutionSubscriptionScript implements SubscriptionScript
{
    /** @var SolutionService */
    private $solutionService;

    /** @var SolutionIndexService */
    private $solutionIndexService;

    public function __construct(
        SolutionService $solutionService,
        SolutionIndexService $solutionIndexService
    ) {
        $this->solutionService = $solutionService;
        $this->solutionIndexService = $solutionIndexService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_CREATED, function(Solution $solution) {
            $this->solutionIndexService->index([$solution]);
        });

        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_UPDATED, function(Solution $solution) {
            $this->solutionIndexService->index([$solution]);
        });

        $this->solutionService->getEventEmitter()->on(SolutionService::EVENT_DELETED, function(int $solutionId, Solution $solution) {
            $this->solutionIndexService->delete($solutionId);
        });
    }
}