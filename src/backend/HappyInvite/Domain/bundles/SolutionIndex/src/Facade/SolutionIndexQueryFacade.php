<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Facade;

use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Formatter\SolutionRepositoryFormatter\SolutionRepositoryFormatter;
use HappyInvite\Domain\Bundles\SolutionIndex\Entity\SolutionIndexEntity;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\QueryExecutor;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\QueryFactory;
use HappyInvite\Domain\Bundles\SolutionIndex\Service\SolutionIndexService;

final class SolutionIndexQueryFacade
{
    public const DEFAULT_LIMIT = 90;

    public const OPTION_FORMAT_MAP_FILTER = 'format_map_filter';
    public const OPTION_FORMAT_AS_REPOSITORY = 'format_as_repository';
    public const OPTION_FOR_AS_RESPONSE200 = 'format_as_response200';

    /** @var SolutionIndexService */
    private $solutionIndexService;

    /** @var QueryFactory */
    private $solutionQueryFactory;

    /** @var QueryExecutor */
    private $solutionQueryExecutor;

    /** @var SolutionRepositoryFormatter */
    private $solutionRepositoryFormatter;

    public function __construct(
        SolutionIndexService $solutionIndexService,
        QueryFactory $solutionQueryFactory,
        QueryExecutor $solutionQueryExecutor,
        SolutionRepositoryFormatter $solutionRepositoryFormatter
    ) {
        $this->solutionIndexService = $solutionIndexService;
        $this->solutionQueryFactory = $solutionQueryFactory;
        $this->solutionQueryExecutor = $solutionQueryExecutor;
        $this->solutionRepositoryFormatter = $solutionRepositoryFormatter;
    }

    public function query(string $source, array $criteria, array $options = []): array
    {
        $options = array_merge([
            self::OPTION_FORMAT_AS_REPOSITORY => true,
            self::OPTION_FOR_AS_RESPONSE200 => true,
            self::OPTION_FORMAT_MAP_FILTER => false,
        ], $options);

        if(! isset($criteria['cursor'])) $criteria['cursor'] = [];
        if(! isset($criteria['cursor']['limit'])) $criteria['cursor']['limit'] = self::DEFAULT_LIMIT;

        $query = $this->solutionQueryFactory->createQueryFromJSON($source, $criteria);
        $result = $this->solutionQueryExecutor->execute($query);

        $mongoIdsMap = [];
        $result = array_map(function(SolutionIndexEntity $indexEntity) use (&$mongoIdsMap): Solution {
            $mongoIdsMap[$indexEntity->getSolution()->getId()] = (string) $indexEntity->getId();

            return $indexEntity->getSolution();
        }, $result);

        if($options[self::OPTION_FORMAT_AS_REPOSITORY]) {
            $result = $this->solutionRepositoryFormatter->format($result, [
                SolutionRepositoryFormatter::OPTIONS_MAP_FILTER => $options[self::OPTION_FORMAT_MAP_FILTER],
            ]);

            if($options[self::OPTION_FOR_AS_RESPONSE200]) {
                $result = [
                    'success' => 200,
                    'mongo_ids' => $mongoIdsMap,
                    'results' => $result
                ];
            }
        }

        return $result;
    }
}