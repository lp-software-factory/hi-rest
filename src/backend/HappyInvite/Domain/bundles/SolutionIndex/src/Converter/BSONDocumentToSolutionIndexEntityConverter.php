<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Converter;

use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Entity\SolutionIndexEntity;
use HappyInvite\Domain\Criteria\IdsCriteria;
use MongoDB\Model\BSONDocument;

final class BSONDocumentToSolutionIndexEntityConverter
{
    /** @var SolutionService */
    private $solutionService;

    public function __construct(SolutionService $solutionService)
    {
        $this->solutionService = $solutionService;
    }

    public function convertMany(array $documents): array
    {
        $solutions = $this->fetchSolutionsMap($documents);

        return array_map(function(BSONDocument $document) use ($solutions): SolutionIndexEntity {
            return new SolutionIndexEntity($document['_id'], (array) $document['filter'], $solutions[(int) $document['id']]);
        }, $documents);
    }

    private function fetchSolutionsMap(array $documents): array
    {
        $solutions = [];
        $solutionIds = array_map(function(BSONDocument $document): int {
            return (int) $document['id'];
        }, $documents);

        array_map(function(Solution $solution) use (&$solutions) {
            $solutions[$solution->getId()] = $solution;
        }, $this->solutionService->getByIds(new IdsCriteria($solutionIds)));

        return $solutions;
    }
}