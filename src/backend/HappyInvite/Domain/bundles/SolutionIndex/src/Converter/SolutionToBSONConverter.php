<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Converter;

use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\SolutionIndex\Entity\SolutionIndexEntity;

/**
 * Class SolutionIndexEntityToMongoRecordFormatter
 * @package HappyInvite\Domain\Bundles\SolutionIndex\Formatter
 *
 * Использование форматтеров от Solution, InviteCardFamily и вообще каких-либо форматтеров СТРОГО ЗАПРЕЩЕНО. Каждый
 * из этих форматтеров имеет возможность включать пользователеспецифичные данные, которые не имеют смысла здесь.
 */
final class SolutionToBSONConverter
{
    public function convertMany(array $solutions): array
    {
        return array_map(function(Solution $solution): array {

            $bson = [
                'id' => $solution->getId(),
                'solution' => $solution->toJSON(),
                'filter' => $this->getFilterJSON($solution),
            ];

            foreach($bson['solution']['invite_card_templates'] as &$input) {
                $input['date_created_at'] = \DateTime::createFromFormat(\DateTime::RFC2822, $input['date_created_at']);
                $input['last_updated_on'] = \DateTime::createFromFormat(\DateTime::RFC2822, $input['last_updated_on']);
            }

            foreach($bson['solution']['envelope_templates'] as &$input) {
                $input['date_created_at'] = \DateTime::createFromFormat(\DateTime::RFC2822, $input['date_created_at']);
                $input['last_updated_on'] = \DateTime::createFromFormat(\DateTime::RFC2822, $input['last_updated_on']);
            }

            $bson['solution']['date_created_at'] = \DateTime::createFromFormat(\DateTime::RFC2822, $bson['solution']['date_created_at']);
            $bson['solution']['last_updated_on'] = \DateTime::createFromFormat(\DateTime::RFC2822, $bson['solution']['last_updated_on']);

            return $bson;
        }, $solutions);
    }

    private function getFilterJSON(Solution $solution): array
    {
        return [
            'is_public' => ($solution->isGlobal() && !$solution->hasSolutionComponentOwner()),
            'has_photo' => array_reduce($solution->getInviteCardTemplates(), function($carry, InviteCardTemplate $next) {
                return $carry || $next->isPhotoEnabled();
            }, false),
            'event_type_ids' => array_map(function(InviteCardTemplate $template) {
                return $template->getFamily()->getEventType()->getId();
            }, $solution->getInviteCardTemplates()),
            'event_type_group_ids' => array_map(function(InviteCardTemplate $template) {
                return $template->getFamily()->getEventType()->getGroup()->getId();
            }, $solution->getInviteCardTemplates()),
            'invite_card_style_ids' => array_map(function(InviteCardTemplate $template) {
                return $template->getFamily()->getStyle()->getId();
            }, $solution->getInviteCardTemplates()),
            'invite_card_size_ids' => array_map(function(InviteCardTemplate $template) {
                return $template->getFamily()->getCardSize()->getId();
            }, $solution->getInviteCardTemplates()),
            'invite_card_gamma_ids' => array_map(function(InviteCardTemplate $template) {
                return $template->getFamily()->getGamma()->getId();
            }, $solution->getInviteCardTemplates()),
            'author_profile_ids' => array_map(function(InviteCardTemplate $template) {
                return $template->getFamily()->getAuthor()->getId();
            }, $solution->getInviteCardTemplates()),
        ];
    }
}