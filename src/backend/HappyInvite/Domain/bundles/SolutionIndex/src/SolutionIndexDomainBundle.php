<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex;

use HappyInvite\Domain\Bundles\SolutionIndex\Subscriptions\SolutionSubscriptionScript;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Platform\Bundles\HIBundle;

final class SolutionIndexDomainBundle extends HIBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
          SolutionSubscriptionScript::class,
        ];
    }
}