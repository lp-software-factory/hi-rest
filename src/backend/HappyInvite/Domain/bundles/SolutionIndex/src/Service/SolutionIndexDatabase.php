<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Service;

use MongoDB\Collection;
use MongoDB\Database;

final class SolutionIndexDatabase
{
    public const MONGODB_COLLECTION_NAME = 'solution_index';

    /** @var Database */
    private $database;

    /** @var array */
    private $indexes;

    public function __construct(Database $database, array $indexes)
    {
        $this->database = $database;
        $this->indexes = $indexes;

        $this->ensureIndexes();
    }

    public function ensureIndexes(): void
    {
        $this->getIndexCollection()->createIndexes($this->indexes);
    }

    public function getIndexCollection(): Collection
    {
        return $this->database->selectCollection(self::MONGODB_COLLECTION_NAME);
    }
}