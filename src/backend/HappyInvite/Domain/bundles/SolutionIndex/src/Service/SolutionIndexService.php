<?php
namespace HappyInvite\Domain\Bundles\SolutionIndex\Service;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Domain\Bundles\SolutionIndex\Formatter\MongoResultToSolutionsFormatter;
use HappyInvite\Domain\Bundles\SolutionIndex\Converter\SolutionToBSONConverter;
use HappyInvite\Domain\Criteria\IdsCriteria;

final class SolutionIndexService
{
    /** @var SolutionIndexDatabase */
    private $database;

    /** @var EntityManager */
    private $em;

    /** @var SolutionToBSONConverter */
    private $converter;

    /** @var MongoResultToSolutionsFormatter */
    private $exporter;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(
        SolutionIndexDatabase $database,
        SolutionService $solutionService,
        SolutionToBSONConverter $converter,
        MongoResultToSolutionsFormatter $exporter,
        EntityManager $em
    ) {
        $this->database = $database;
        $this->converter = $converter;
        $this->exporter = $exporter;
        $this->solutionService = $solutionService;
        $this->em = $em;
    }

    public function index(array $solutions, Callable $done = null): void
    {
        $indexed = $this->converter->convertMany($solutions);

        array_map(function(array $bson) {
            $this->database->getIndexCollection()->updateOne(
                ['id' => $bson['id']],
                ['$set' => $bson],
                ['upsert' => true]
            );
        }, $indexed);

        if($done) {
            foreach($solutions as $solution) {
                $done($solution);
            }
        }
    }

    public function delete(int $solutionId): void
    {
        $this->database->getIndexCollection()->deleteOne([
            'id' => $solutionId,
        ]);
    }

    public function fullReindex(Callable $done = null): void
    {
        foreach($this->solutionService->getAllSolutionIds() as $solutionIds) {
            $solutions = $this->solutionService->getByIds(new IdsCriteria($solutionIds));

            $this->index($solutions, $done);

            array_map(function(Solution $entity) {
                $this->em->detach($entity);
            }, $solutions);
        }
    }

    public function clear(): void
    {
        $this->database->getIndexCollection()->drop();
    }
}