<?php
namespace HappyInvite\Domain\Bundles\Avatar;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Avatar\Config\AvatarConfig;
use HappyInvite\Domain\Bundles\Avatar\Factory\AvatarConfigFactory;

return [
    AvatarConfig::class => factory(AvatarConfigFactory::class),
];