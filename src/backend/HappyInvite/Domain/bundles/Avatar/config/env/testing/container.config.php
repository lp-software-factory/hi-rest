<?php
namespace HappyInvite\Domain\Bundles\Avatar;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Avatar\Service\AvatarService;
use HappyInvite\Domain\Bundles\Avatar\Service\Strategy\MockAvatarStrategy;

return [
    AvatarService::class => object()->constructorParameter('strategy', get(MockAvatarStrategy::class))
];