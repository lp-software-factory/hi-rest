<?php
namespace HappyInvite\Domain\Bundles\Avatar;

return [
    'config.hi.domain.avatar' => [
        'font' => '%s/fonts/Roboto/Roboto-Medium.ttf',
    ]
];