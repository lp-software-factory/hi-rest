<?php
namespace HappyInvite\Domain\Bundles\Avatar\Strategy;

use HappyInvite\Domain\Bundles\Avatar\Entity\ImageEntity;
use League\Flysystem\FilesystemInterface;

interface ImageStrategy
{
    public function getEntity(): ImageEntity;
    public function getEntityId(): string;
    public function getLetter(): string;
    public function getFilesystem(): FilesystemInterface;
    public function getPublicPath(): string;
    public function getDefaultSize(): int;
    public function getSizes(): array;
    public function getRatio(): float;
    public function validate(\Intervention\Image\Image $origImage);
}