<?php
namespace HappyInvite\Domain\Bundles\Avatar\Factory;

use HappyInvite\Domain\Bundles\Avatar\AvatarBundle;
use HappyInvite\Domain\Bundles\Avatar\Config\AvatarConfig;
use Interop\Container\ContainerInterface;

final class AvatarConfigFactory
{
    public function __invoke(AvatarBundle $bundle, ContainerInterface $container)
    {
        $config = $container->get('config.hi.domain.avatar');

        return new AvatarConfig(
            sprintf($config['font'], $bundle->getResourcesDir())
        );
    }
}