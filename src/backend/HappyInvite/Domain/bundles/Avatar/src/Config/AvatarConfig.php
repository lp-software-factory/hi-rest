<?php
namespace HappyInvite\Domain\Bundles\Avatar\Config;

final class AvatarConfig
{
    /** @var string */
    private $fontPath;

    public function __construct($fontPath)
    {
        $this->fontPath = $fontPath;
    }

    public function getFontPath(): string
    {
        return $this->fontPath;
    }
}