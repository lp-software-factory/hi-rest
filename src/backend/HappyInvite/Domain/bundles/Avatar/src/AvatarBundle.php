<?php
namespace HappyInvite\Domain\Bundles\Avatar;

use HappyInvite\Platform\Bundles\HIBundle;

final class AvatarBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}