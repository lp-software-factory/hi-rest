<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

final class FileNotUploadedException extends \Exception {}