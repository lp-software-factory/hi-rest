<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

abstract class ImageServiceException extends \Exception {}