<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

class InvalidRatioException extends ImageServiceException {}