<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

class InvalidCropException extends ImageServiceException {}