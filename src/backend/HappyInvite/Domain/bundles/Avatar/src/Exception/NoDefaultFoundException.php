<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

class NoDefaultFoundException extends ImageServiceException {}