<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

final class InvalidCoordinatesException extends \Exception {}