<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

class ImageTooBigException extends ImageServiceException {}