<?php
namespace HappyInvite\Domain\Bundles\Avatar\Exception;

class ImageTooSmallException extends ImageServiceException {}