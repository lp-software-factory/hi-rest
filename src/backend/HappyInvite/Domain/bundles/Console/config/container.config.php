<?php
namespace HappyInvite\Domain\Bundles\Company;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Console\Factory\ConsoleApplicationFactory;
use Symfony\Component\Console\Application;

return [
    Application::class => factory(new ConsoleApplicationFactory()),
];