<?php
namespace HappyInvite\Domain\Bundles\Console;

return [
    'config.console' => [
        'title' => 'HappyInvite Domain&Services (REST) Console',
        'version' => '1.0.0',
        'commands' => [],
    ],
];