<?php
namespace HappyInvite\Domain\Bundles\Console;

use HappyInvite\Platform\Bundles\HIBundle;

final class ConsoleBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}