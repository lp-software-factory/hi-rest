<?php
namespace HappyInvite\Domain\Bundles\Company\Parameters;

final class CreateCompanyParameters
{
    /** @var string */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}