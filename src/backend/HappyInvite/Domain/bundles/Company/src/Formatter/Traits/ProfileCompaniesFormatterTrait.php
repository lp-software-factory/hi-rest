<?php
namespace HappyInvite\Domain\Bundles\Company\Formatter\Traits;

use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Service\CompanyService;

trait ProfileCompaniesFormatterTrait
{
    /** @var CompanyService */
    private $companyService;

    protected function getProfileCompanies(array $profileIds)
    {
        return array_map(function(Company $company) {
            return $company->toJSON();
        }, $this->companyService->findCompaniesOfProfiles($profileIds));
    }
}