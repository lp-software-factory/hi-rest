<?php
namespace HappyInvite\Domain\Bundles\Company\Service;

use HappyInvite\Domain\Bundles\Company\Criteria\ListCompaniesCriteria;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Parameters\CreateCompanyParameters;
use HappyInvite\Domain\Bundles\Company\Parameters\EditCompanyParameters;
use HappyInvite\Domain\Bundles\Company\Repository\CompanyRepository;
use HappyInvite\Domain\Bundles\Profile\Entity\ProfileCompanies;
use HappyInvite\Domain\Bundles\Profile\Repository\ProfileCompaniesRepository;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class CompanyService
{
    /** @var CompanyRepository */
    private $companyRepository;

    /** @var ProfileCompaniesRepository */
    private $profileCompaniesRepository;

    public function __construct(
        CompanyRepository $companyRepository,
        ProfileCompaniesRepository $profileCompaniesRepository
    ) {
        $this->companyRepository = $companyRepository;
        $this->profileCompaniesRepository = $profileCompaniesRepository;
    }

    public function createCompany(CreateCompanyParameters $parameters): Company
    {
        $company = new Company($parameters->getName());

        $this->companyRepository->createCompany($company);

        return $company;
    }

    public function editCompany(int $companyId, EditCompanyParameters $parameters): Company
    {
        $company = $this->companyRepository->getById($companyId);
        $company->changeName($parameters->getName());

        $this->companyRepository->saveCompany($company);

        return $company;
    }

    public function deleteCompany(int $companyId)
    {
        $this->companyRepository->deleteCompany(
            $this->companyRepository->getById($companyId)
        );
    }

    public function getCompanyById(int $id): Company
    {
        return $this->companyRepository->getById($id);
    }

    public function getCompaniesByIds(IdsCriteria $idsCriteria): array
    {
        return $this->companyRepository->getByIds($idsCriteria);
    }

    public function listCompanies(ListCompaniesCriteria $criteria): array
    {
        return $this->companyRepository->listCompanies($criteria);
    }

    public function countCompanies(ListCompaniesCriteria $criteria): int
    {
        return $this->companyRepository->countCompanies($criteria);
    }

    public function findCompaniesOfProfiles(array $profileIds): array
    {
        return array_map(function(ProfileCompanies $eq) {
            return $eq->getCompany();
        }, $this->profileCompaniesRepository->findCompaniesOfProfiles($profileIds));
    }

    public function hasCompanyWithId(int $companyId): bool
    {
        return $this->companyRepository->hasCompanyWithId($companyId);
    }

}