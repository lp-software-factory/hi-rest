<?php
namespace HappyInvite\Domain\Bundles\Company\Exception;

final class CompaniesNotFoundException extends \Exception {}