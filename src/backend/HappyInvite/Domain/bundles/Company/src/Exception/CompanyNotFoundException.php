<?php
namespace HappyInvite\Domain\Bundles\Company\Exception;

final class CompanyNotFoundException extends \Exception {}