<?php
namespace HappyInvite\Domain\Bundles\Company;

use HappyInvite\Platform\Bundles\HIBundle;

final class CompanyBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}