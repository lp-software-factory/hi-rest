<?php
namespace HappyInvite\Domain\Bundles\Company\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Company\Criteria\ListCompaniesCriteria;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Exception\CompaniesNotFoundException;
use HappyInvite\Domain\Bundles\Company\Exception\CompanyNotFoundException;
use HappyInvite\Domain\Criteria\IdsCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;

final class CompanyRepository extends EntityRepository
{
    public function createCompany(Company $company): int
    {
        while($this->hasWithSID($company->getSID())) {
            $company->regenerateSID();
        }

        $this->getEntityManager()->persist($company);
        $this->getEntityManager()->flush($company);

        return $company->getId();
    }

    public function saveCompany(Company $company)
    {
        $this->getEntityManager()->flush($company);
    }

    public function deleteCompany(Company $company)
    {
        $this->getEntityManager()->remove($company);
        $this->getEntityManager()->flush($company);
    }

    public function listCompanies(ListCompaniesCriteria $criteria): array
    {
        $seek = $criteria->getSeek();

        $query = $this->createQueryBuilder('c');
        $query->setFirstResult($seek->getOffset());
        $query->setMaxResults($seek->getLimit());
        $query->orderBy('c.'.$criteria->getSort()->getField(), $criteria->getSort()->getDirection());

        return $query->getQuery()->execute();
    }

    public function countCompanies(ListCompaniesCriteria $criteria): int
    {
        $query = $this->createQueryBuilder('c');
        $query->select('count(c)');

        return (int) $query->getQuery()->getSingleScalarResult();
    }

    public function getById(int $id): Company
    {
        $result = $this->find($id);

        if($result instanceof Company) {
            return $result;
        }else{
            throw new CompanyNotFoundException(sprintf('Company `ID(%d)` not found', $id));
        }
    }

    public function getByIds(IdsCriteria $idsCriteria): array
    {
        $ids = $idsCriteria->getIds();
        $results = $this->findBy(['id' => $ids]);

        if(count($ids) !== count($results)) {
            throw new CompaniesNotFoundException(sprintf(
                'Companies with IDs (%s) not found',
                join(', ', array_diff($ids, array_map(function(Company $company) {
                    return $company->getId();
                }, $results)))
            ));
        }

        return $results;
    }

    public function hasWithSID(string $sid): bool
    {
        return $this->findOneBy(['sid' => $sid]) !== null;
    }

    public function hasCompanyWithId(string $id): bool
    {
        return $this->findOneBy(['id' => $id]) !== null;
    }

    public function getBySID(string $sid): Company
    {
        $result = $this->findBy(['sid' => $sid]);

        if($result instanceof Company) {
            return $result;
        }else{
            throw new CompanyNotFoundException(sprintf('Company `SID(%s)` not found', $sid));
        }
    }
}