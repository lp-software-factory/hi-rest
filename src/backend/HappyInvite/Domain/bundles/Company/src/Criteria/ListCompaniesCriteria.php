<?php
namespace HappyInvite\Domain\Bundles\Company\Criteria;

use HappyInvite\Domain\Criteria\SortCriteria;
use HappyInvite\Domain\Criteria\SeekCriteria;
use HappyInvite\Domain\Markers\JSONSerializable;

final class ListCompaniesCriteria implements JSONSerializable
{
    public const ORDER_BY_DEFAULT_FIELD = 'id';
    public const ORDER_BY_DEFAULT_DIRECTION = 'desc';
    public const ORDER_BY_FIELDS = ['id', 'name', 'date_created_at', 'last_updated_on'];

    /** @var SeekCriteria */
    private $seek;

    /** @var SortCriteria */
    private $sort;

    public function __construct(
        SeekCriteria $seek,
        SortCriteria $order
    ) {
        if(! in_array($order->getField(), self::ORDER_BY_FIELDS)) {
            throw new \Exception(sprintf('Invalid orderBy field `%s`, allowed: %s', $order->getField(), join(', ', self::ORDER_BY_FIELDS)));
        }

        $this->seek = $seek;
        $this->sort = $order;
    }

    public function toJSON(array $options = []): array
    {
        return [
            'seek' => $this->getSeek()->toJSON($options),
            'sort' => $this->getSort()->toJSON($options),
        ];
    }

    public function getSeek(): SeekCriteria
    {
        return $this->seek;
    }

    public function getSort(): SortCriteria
    {
        return $this->sort;
    }
}