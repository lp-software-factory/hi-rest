<?php
namespace HappyInvite\Domain\Bundles\Company;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Company\Factory\Doctrine\CompanyRepositoryFactory;
use HappyInvite\Domain\Bundles\Company\Repository\CompanyRepository;

return [
    CompanyRepository::class => factory(CompanyRepositoryFactory::class),
];