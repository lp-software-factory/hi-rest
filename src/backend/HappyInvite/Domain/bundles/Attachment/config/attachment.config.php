<?php
namespace HappyInvite\Domain\Bundles\Attachment;

return [
    'config.hi.domain.attachment' => [
        'dir' => '%s/entity/attachment',
        'www' => '%s/entity/attachment',
    ]
];