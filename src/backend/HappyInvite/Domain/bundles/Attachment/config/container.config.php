<?php
namespace HappyInvite\Domain\Bundles\Attachment;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Domain\Bundles\Attachment\Config\AttachmentConfig;
use HappyInvite\Domain\Bundles\Attachment\Factory\AttachmentConfigFactory;
use HappyInvite\Domain\Bundles\Attachment\Factory\Doctrine\AttachmentRepositoryFactory;
use HappyInvite\Domain\Bundles\Attachment\Repository\AttachmentRepository;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;

return [
    AttachmentConfig::class => factory(AttachmentConfigFactory::class),
    AttachmentRepository::class => factory(AttachmentRepositoryFactory::class),
    AttachmentService::class => object()
        ->constructorParameter('generatePreviews', true),
];