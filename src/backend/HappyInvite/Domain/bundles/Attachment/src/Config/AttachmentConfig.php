<?php
namespace HappyInvite\Domain\Bundles\Attachment\Config;

use HappyInvite\Domain\Service\StorageConfig;
use League\Flysystem\FilesystemInterface;

final class AttachmentConfig
{
    public const DEFAULT_STORAGE_DIR = StorageConfig::DEFAULT_STORAGE_DIR.'/storage';
    public const DEFAULT_STORAGE_WWW = StorageConfig::DEFAULT_STORAGE_WWW.'/storage';

    /** @var string */
    private $dir;

    /** @var string */
    private $www;

    /** @var FilesystemInterface */
    private $filesystem;

    public function __construct(string $dir, string $www, FilesystemInterface $filesystem)
    {
        $this->dir = $dir;
        $this->www = $www;
        $this->filesystem = $filesystem;
    }

    public function getDir(): string
    {
        if(! (is_dir($this->dir) && is_writable($this->dir))) {
            throw new \Exception(sprintf('Path `%s` is not dir or is not writable', $this->dir));
        }

        return $this->dir;
    }

    public function getWww(): string
    {
        return $this->www;
    }

    public function getFilesystem(): FilesystemInterface
    {
        return $this->filesystem;
    }
}