<?php
namespace HappyInvite\Domain\Bundles\Attachment\Source;

use HappyInvite\Domain\Markers\JSONSerializable;

interface Source extends JSONSerializable
{
    public function getCode(): string;
}