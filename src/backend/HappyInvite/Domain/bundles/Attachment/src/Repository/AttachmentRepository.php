<?php
namespace HappyInvite\Domain\Bundles\Attachment\Repository;

use Doctrine\ORM\EntityRepository;
use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Attachment\Exception\AttachmentFactoryException;
use HappyInvite\Domain\Bundles\Attachment\Exception\AttachmentNotFoundException;
use HappyInvite\Domain\Criteria\SeekCriteria;

class AttachmentRepository extends EntityRepository
{
    public function createAttachment(Attachment $attachment)
    {
        $this->getEntityManager()->persist($attachment);
        $this->getEntityManager()->flush($attachment);
    }

    public function saveAttachment(Attachment $attachment)
    {
        $this->getEntityManager()->flush($attachment);
    }

    public function deleteAttachment(array $attachments)
    {
        foreach($attachments as $attachment) {
            $this->getEntityManager()->remove($attachment);
        }
        $this->getEntityManager()->flush();
    }

    public function listAttachments(SeekCriteria $criteria): array
    {
        $qb = $this->createQueryBuilder('a');
        $qb->setFirstResult($criteria->getOffset());
        $qb->setMaxResults($criteria->getLimit());

        /** @var Attachment[] $result */
        $result = $qb->getQuery()->execute();

        return $result;
    }


    public function getById(int $attachmentId): Attachment
    {
        $result = $this->find($attachmentId);

        if($result instanceof Attachment) {
            return $result;
        }else{
            throw new AttachmentFactoryException(sprintf('Attachment with ID `%s` not found', $attachmentId));
        }
    }

    public function getBySID(string $attachmentSID): Attachment
    {
        $result = $this->findOneBy(['sid' => $attachmentSID]);

        if($result instanceof Attachment) {
            return $result;
        }else{
            throw new AttachmentFactoryException(sprintf('Attachment with SID `%s` not found', $attachmentSID));
        }
    }

    public function getManyByIds(array $ids): array
    {
        /** @var Attachment[] $result */
        $result = $this->findBy(['id' => array_filter($ids, function($input) {
            return is_int($input);
        })]);

        if(count($result) !== count($ids)) {
            throw new AttachmentNotFoundException(sprintf('Some of Attachment(ids: `%s`) not found',
                json_encode($ids)));
        }

        return $result;
    }

    public function preLoadAttachments(array $ids)
    {
        $this->getManyByIds($ids);
    }
}