<?php
namespace HappyInvite\Domain\Bundles\Attachment\Service;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;

interface AttachmentTypeExtension
{
    public function extend(Attachment $attachment): array;
}