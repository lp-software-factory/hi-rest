<?php
namespace HappyInvite\Domain\Bundles\Attachment\Service;

use HappyInvite\Domain\Bundles\Attachment\Config\AttachmentConfig;
use HappyInvite\Domain\Bundles\Attachment\LinkMetadata\LinkMetadata;
use HappyInvite\Domain\Bundles\Attachment\LinkMetadata\Types\WebmLinkMetadata;
use HappyInvite\Domain\Bundles\Attachment\Source\Source;

final class AttachmentPreviewService
{
    /** @var string */
    private $attachmentsRealPath;

    public function __construct(AttachmentConfig $config)
    {
        $this->attachmentsRealPath = $config->getDir();
    }

    public function generatePreview(
        string $dir,
        string $file,
        Source $source,
        LinkMetadata $metadata
    ): string {
        if($metadata instanceof WebmLinkMetadata) {
            return $this->generateWebmPreview($dir, $file);
        }else{
            throw new \Exception('Unknown how to generate preview');
        }
    }

    public function generateWebmPreview(string $dir, string $file)
    {
        $ffSource = sprintf('%s/%s/%s', $this->attachmentsRealPath, $dir, $file);
        $ffDestination = sprintf('%s/%s/preview.png', $this->attachmentsRealPath, $dir);

        exec(sprintf('ffmpeg -y  -i %s -f mjpeg -vframes 1 -ss 1 %s', $ffSource, $ffDestination));

        return 'preview.png';
    }
}