<?php
namespace HappyInvite\Domain\Bundles\Attachment\LinkMetadata;

use HappyInvite\Domain\Markers\JSONSerializable;

interface LinkMetadata extends JSONSerializable
{
    public function getTitle(): string;
    public function getDescription(): string;
    public function getVersion(): int;
    public function getURL(): string;
    public function getResourceType(): string;
    public function toJSON(array $options = []): array;
}