<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class NotFoundException extends \Exception
{
}