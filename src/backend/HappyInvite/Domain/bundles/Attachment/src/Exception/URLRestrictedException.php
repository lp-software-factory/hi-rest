<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class URLRestrictedException extends InvalidURLException
{
}