<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class EmptyURLException extends InvalidURLException
{
}