<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class AttachmentNotFoundException extends \Exception
{
}