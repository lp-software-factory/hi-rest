<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class AttachmentFactoryException extends \Exception
{
}