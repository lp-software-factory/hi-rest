<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class InvalidURLException extends \Exception
{
}