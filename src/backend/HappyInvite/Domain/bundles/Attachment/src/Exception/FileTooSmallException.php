<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class FileTooSmallException extends AttachmentFactoryException
{
}