<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class FileNotReadableException extends AttachmentFactoryException
{
}