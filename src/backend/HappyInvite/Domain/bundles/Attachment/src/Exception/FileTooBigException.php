<?php
namespace HappyInvite\Domain\Bundles\Attachment\Exception;

class FileTooBigException extends AttachmentFactoryException
{
}