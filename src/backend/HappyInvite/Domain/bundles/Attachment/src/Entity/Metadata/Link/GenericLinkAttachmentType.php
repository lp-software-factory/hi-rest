<?php
namespace HappyInvite\Domain\Bundles\Attachment\Entity\Metadata\Link;

use HappyInvite\Domain\Bundles\Attachment\Entity\Metadata\LinkAttachmentType;

class GenericLinkAttachmentType implements LinkAttachmentType
{
    public function getCode()
    {
        return 'link';
    }
}