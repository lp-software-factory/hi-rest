<?php
namespace HappyInvite\Domain\Bundles\Attachment\Entity\Metadata;

interface AttachmentType
{
    public function getCode();
}