<?php
namespace HappyInvite\Domain\Bundles\Attachment;

use HappyInvite\Platform\Bundles\HIBundle;

class AttachmentBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}