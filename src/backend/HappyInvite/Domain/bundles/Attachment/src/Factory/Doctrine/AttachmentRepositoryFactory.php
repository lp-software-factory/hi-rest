<?php
namespace HappyInvite\Domain\Bundles\Attachment\Factory\Doctrine;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineRepositoryFactory;

final class AttachmentRepositoryFactory extends DoctrineRepositoryFactory
{
    protected function getEntityClassName(): string
    {
        return Attachment::class;
    }
}