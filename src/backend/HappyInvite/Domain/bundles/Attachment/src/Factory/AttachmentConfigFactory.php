<?php
namespace HappyInvite\Domain\Bundles\Attachment\Factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Attachment\Config\AttachmentConfig;
use HappyInvite\Domain\Service\StorageConfig;
use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\Service\EnvironmentService;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\Memory\MemoryAdapter;

final class AttachmentConfigFactory
{
    public function __invoke(Container $container, StorageConfig $storageConfig, EnvironmentService $environmentService)
    {
        $config = $container->get('config.hi.domain.attachment');

        $dir = sprintf($config['dir'], $storageConfig->getDir());
        $www = sprintf($config['www'], $storageConfig->getWww());

        if($environmentService->getCurrent() === Environment::TESTING) {
            $adapter = new MemoryAdapter();
        }else{
            $adapter = new Local($dir);
        }

        return new AttachmentConfig($dir, $www, new Filesystem($adapter));
    }
}