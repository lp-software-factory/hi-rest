<?php
namespace HappyInvite\Domain\Bundles\Attachment\Formatter;

use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;

final class AttachmentFormatter
{
    public function format(Attachment $attachment)
    {
        return $attachment->toJSON();
    }
}