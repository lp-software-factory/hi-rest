<?php
namespace HappyInvite\Domain\Bundles\Doctrine;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\MongoDB\Factory\MongoDBFactory;
use MongoDB\Database;

return [
    Database::class => factory(MongoDBFactory::class),
];