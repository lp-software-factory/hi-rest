<?php
namespace HappyInvite\Domain\Bundles\MongoDB;

return [
    'config.hi.domain.mongodb.connection' => [
        'db_prefix' => 'hi',
        'server' => 'mongodb://127.0.0.1:27017',
        'options' => [
            'connect' => true
        ],
        'driver_options' => []
    ],
];