<?php
namespace HappyInvite\Domain\Bundles\MongoDB\Factory;

use HappyInvite\Platform\Service\EnvironmentService;
use MongoDB\Database;
use MongoDB\Client;
use Interop\Container\ContainerInterface;

final class MongoDBFactory
{
    public function __invoke(ContainerInterface $container, EnvironmentService $environmentService): Database
    {
        $config = $container->get('config.hi.domain.mongodb.connection');
        $env = $environmentService->getCurrent();

        $mongoClient = new Client(
            $config['server'] ?? 'mongodb://127.0.0.1:27017',
            $config['options'] ?? ['connect' => true],
            $config['driver_options'] ?? []
        );

        return $mongoClient->selectDatabase(sprintf('%s_%s', $config['db_prefix'] ?? 'hi', $env));
    }
}