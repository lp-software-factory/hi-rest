<?php
namespace HappyInvite\Domain\Bundles\MongoDB;

use HappyInvite\Platform\Bundles\HIBundle;

final class MongoDBDomainBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}