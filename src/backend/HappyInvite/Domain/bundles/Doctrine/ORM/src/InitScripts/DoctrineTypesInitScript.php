<?php
namespace HappyInvite\Domain\Bundles\DoctrineORM\InitScripts;

use Doctrine\DBAL\Types\Type;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class DoctrineTypesInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        foreach($container->get('hi.config.domain.doctrine.types') as $type => $className) {
            Type::addType($type, $className);
        }
    }
}