<?php
namespace HappyInvite\Domain\Bundles\DoctrineORM\Doctrine\Type\Point;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class PointType extends Type
{
    public const TYPE_NAME = 'point';

    public function getName()
    {
        return self::TYPE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if($value instanceof Point) {
            return json_encode($value->toJSON());
        }else{
            return '{"x": 0, "y": 0}';
        }
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return new Point(0, 0);
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;
        $json = json_decode($value, true);

        return new Point($json['x'], $json['y']);
    }
}