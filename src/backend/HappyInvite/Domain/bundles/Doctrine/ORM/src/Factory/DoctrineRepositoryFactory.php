<?php
namespace HappyInvite\Domain\Bundles\DoctrineORM\Factory;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use HappyInvite\Platform\Factory\ContainerFactoryInterface;
use Interop\Container\ContainerInterface;

abstract class
DoctrineRepositoryFactory implements ContainerFactoryInterface
{
    abstract protected function getEntityClassName(): string;

    public function __invoke(ContainerInterface $container): EntityRepository
    {
        $em = $container->get(EntityManager::class); /** @var EntityManager $em */

        return $em->getRepository($this->getEntityClassName());
    }
}