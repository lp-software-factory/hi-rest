<?php
namespace HappyInvite\Domain\Bundles\DoctrineORM;

use HappyInvite\Domain\Bundles\DoctrineORM\InitScripts\DoctrineTypesInitScript;
use HappyInvite\Platform\Bundles\HIBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScriptInjectableBundle;

final class DoctrineORMBundle extends HIBundle implements InitScriptInjectableBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getInitScripts(): array
    {
        return [
            -999 => new DoctrineTypesInitScript(),
        ];
    }
}