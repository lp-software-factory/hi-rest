<?php
namespace HappyInvite\Domain\Bundles\DoctrineORM;

return [
    'config.hi.domain.doctrine2orm' => [
        'is_dev_mode' => false,
        'connection_options'=> [
            'user' => 'hipgsql',
            'password' => '1234',
            'host' => '127.0.0.1',
            'port' => 5432,
            'dbname' => 'hi_testing',
            'driver'   => 'pdo_pgsql',
            'charset'  => 'utf8',
        ],
    ]
];