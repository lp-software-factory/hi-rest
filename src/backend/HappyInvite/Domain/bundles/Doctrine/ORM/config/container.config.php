<?php
namespace HappyInvite\Domain\Bundles\Doctrine;

use function DI\get;
use function DI\object;
use function DI\factory;

use Doctrine\ORM\EntityManager;
use HappyInvite\Domain\Bundles\DoctrineORM\Factory\DoctrineEntityManagerFactory;

return [
    EntityManager::class => factory(DoctrineEntityManagerFactory::class),
    'config.hi.domain.doctrine2orm' => [
        'is_dev_mode' => true,
        'connection_options'=> [
            'user' => 'hipgsql',
            'password' => '1234',
            'host' => '127.0.0.1',
            'port' => 5432,
            'dbname' => 'hi',
            'driver'   => 'pdo_pgsql',
            'charset'  => 'utf8',
        ],
    ]
];