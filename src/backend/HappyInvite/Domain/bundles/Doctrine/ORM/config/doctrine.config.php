<?php
namespace HappyInvite\Domain\Bundles\Doctrine;

use HappyInvite\Domain\Bundles\DoctrineORM\Doctrine\Type\Point\PointType;

return [
    'hi.config.domain.doctrine.types' => [
        'point' => PointType::class,
    ]
];