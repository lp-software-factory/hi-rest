<?php
namespace HappyInvite\Platform;

use function \DI\object;
use function \DI\factory;
use function \DI\get;

use HappyInvite\Platform\Service\EnvironmentService;

return [
    EnvironmentService::class => object()
        ->constructorParameter('current', get('config.environment')),
];