<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Bundle;

interface APIDocsBundle
{
    public function hasAPIDocs(): bool;
    public function getAPIDocsDir(): string;
}