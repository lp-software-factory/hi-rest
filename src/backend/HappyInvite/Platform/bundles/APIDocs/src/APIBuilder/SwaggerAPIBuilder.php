<?php
namespace HappyInvite\Platform\Bundles\APIDocs\APIBuilder;

interface SwaggerAPIBuilder
{
    public function build(): array;
}