<?php
namespace HappyInvite\Platform\Bundles\APIDocs;

use HappyInvite\Platform\Bundles\HIBundle;

final class APIDocsPlatformBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}