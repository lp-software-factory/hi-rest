<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Exceptions;

final class APIDocsDefinitionNotFoundException extends \Exception {}