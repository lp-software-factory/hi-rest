<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Schema\Exceptions;

class CachedSchemaNotFound extends \Exception
{
}