<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Schema\Exceptions;

final class InvalidJSONSchemaException extends \Exception {}