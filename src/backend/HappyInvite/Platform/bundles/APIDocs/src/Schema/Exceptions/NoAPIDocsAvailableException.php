<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Schema\Exceptions;

class NoAPIDocsAvailableException extends \Exception
{
}