<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Schema;

use JsonSchema\Validator;

class JSONSchema
{
    /** @var array */
    private $definition;

    /** @var Validator */
    private $validator;

    public function __construct($definition)
    {
        $this->definition = $definition;
        $this->validator = new Validator();
    }

    public function getDefinition()
    {
        return $this->definition;
    }

    public function getValidator(): Validator
    {
        return $this->validator;
    }

    public function validate($data): Validator
    {
        $this->validator->check(['foo' => 'bar'], $this->definition);

        return $this->validator;
    }
}