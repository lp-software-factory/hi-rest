<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Generator\Impl;

use Cocur\Chain\Chain;
use HappyInvite\Platform\Bundles\APIDocs\APIBuilder\LocalSwaggerAPIBuilder;
use HappyInvite\Platform\Bundles\APIDocs\Bundle\APIDocsBundle;
use HappyInvite\Platform\Bundles\APIDocs\Generator\APIDocsGeneratorInterface;
use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\Platform\Service\BundlesService;

final class BundlesAPIDocsGenerator implements APIDocsGeneratorInterface
{
    /** @var BundlesService */
    private $bundlesService;

    public function __construct(BundlesService $bundlesService)
    {
        $this->bundlesService = $bundlesService;
    }

    public function generateAPIDocs(): array
    {

        $sources = Chain::create($this->bundlesService->listBundles())
            ->filter(function(Bundle $bundle) {
                return $bundle instanceof APIDocsBundle;
            })
            ->filter(function(APIDocsBundle $bundle) {
                return $bundle->hasAPIDocs();
            })
            ->map(function(APIDocsBundle $bundle) {
                return $bundle->getAPIDocsDir();
            })
            ->array;

        return (new LocalSwaggerAPIBuilder($sources))->build();
    }
}