<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Generator;

interface APIDocsGeneratorInterface
{
    public function generateAPIDocs(): array;
}