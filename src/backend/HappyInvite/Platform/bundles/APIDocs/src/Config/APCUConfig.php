<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Config;

final class APCUConfig
{
    /** @var string */
    private $key;

    /** @var int */
    private $lifeTime;

    public function __construct(array $config)
    {
        $this->key = $config['api-docs']['key'];
        $this->lifeTime = $config['api-docs']['lifetime'];
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getLifeTime(): int
    {
        return $this->lifeTime;
    }
}