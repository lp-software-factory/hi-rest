<?php
namespace HappyInvite\Platform\Bundles\APIDocs\ResponseBuilder;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\Platform\Response\JSON\ResponseDecoratorsManager;

final class SwaggerResponseBuilder extends ResponseBuilder
{
    protected function initDecorators(ResponseDecoratorsManager $decoratorsManager)
    {
    }
}