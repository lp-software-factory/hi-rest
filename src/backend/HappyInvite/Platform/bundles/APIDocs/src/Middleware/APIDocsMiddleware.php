<?php
namespace HappyInvite\Platform\Bundles\APIDocs\Middleware;

use HappyInvite\Platform\Bundles\APIDocs\Service\APIDocsService;
use HappyInvite\Platform\Bundles\APIDocs\ResponseBuilder\SwaggerResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class APIDocsMiddleware implements MiddlewareInterface
{
    /** @var APIDocsService */
    private $apiDocsService;

    public function __construct(APIDocsService $apiDocsService)
    {
        $this->apiDocsService = $apiDocsService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new SwaggerResponseBuilder($response);

        return $responseBuilder
            ->setJSON($this->apiDocsService->getAPIDocs([
                'use_cache' => false,
            ]))
            ->setStatusSuccess()
            ->build();
    }
}