<?php
namespace HappyInvite\Platform\Bundles\APIDocs;

return [
    'config.hi.platform.api-docs.apcu' => [
        'api-docs' => [
            'key' => 'config.hi.platform.api-docs.json',
            'lifetime' => 3 /* hours */ * 60 /* minutes */ * 60 /* seconds */,
        ]
    ]
];