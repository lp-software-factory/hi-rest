<?php
namespace HappyInvite\Platform\Bundles\APIDocs;

use HappyInvite\Platform\Bundles\APIDocs\Middleware\APIDocsMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/api-docs.json',
                'middleware' => APIDocsMiddleware::class,
            ],
        ]
    ]
];