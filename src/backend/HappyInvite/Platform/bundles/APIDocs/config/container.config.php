<?php
namespace HappyInvite\Platform\Bundles\APIDocs;

use function DI\object;
use function DI\factory;
use function DI\get;

use DI\Container;
use HappyInvite\Platform\Bundles\APIDocs\Config\APCUConfig;

return [
    APCUConfig::class => object()->constructorParameter('config', get('config.hi.platform.api-docs.apcu')),
];