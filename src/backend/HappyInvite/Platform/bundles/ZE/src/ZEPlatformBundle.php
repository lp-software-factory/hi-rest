<?php
namespace HappyInvite\Platform\Bundles\ZE;

use HappyInvite\Platform\Bundles\HIBundle;

final class ZEPlatformBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}