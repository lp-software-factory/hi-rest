<?php
namespace HappyInvite\Platform\Bundles\ZE\Builder;

use DI\Container;
use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScriptInjectableBundle;
use Interop\Container\ContainerInterface;
use Zend\Diactoros\Response\SapiEmitter;
use Zend\Expressive\Application;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\RouterInterface;
use Zend\Stratigility\NoopFinalHandler;

final class ZEApplicationBuilder
{
    /** @var InitScript[] */
    private $initScripts = [];

    /** @var RouterInterface */
    private $router;

    /** @var SapiEmitter */
    private $SAPIEmitter;

    public function __construct()
    {
        $this->router = new FastRouteRouter();
        $this->SAPIEmitter = new SapiEmitter();
    }

    public function addInitScripts(array $initScripts): self
    {
        array_map(function(InitScript $script) {
            $this->initScripts[] = $script;
        }, $initScripts);

        return $this;
    }

    public function addInitScriptsFromBundles(array $bundles): self
    {
        array_map(function(Bundle $bundle) {
            if($bundle instanceof InitScriptInjectableBundle) {
                $this->addInitScripts($bundle->getInitScripts());
            }
        }, $bundles);

        return $this;
    }

    public function setRouter(RouterInterface $router): self
    {
        $this->router = $router;

        return $this;
    }

    public function setSAPIEmitter(SapiEmitter $SAPIEmitter): self
    {
        $this->SAPIEmitter = $SAPIEmitter;

        return $this;
    }

    public function build(ContainerInterface $container): Application
    {
        $application = new Application(
            $this->router,
            $container,
            new NoopFinalHandler(),
            $this->SAPIEmitter
        );

        array_map(function(InitScript $initScript) use ($application, $container) {
            $initScript->__invoke($application, $container);
        }, $this->initScripts);

        if($container instanceof Container) {
            $container->set(Application::class, $application);
        }

        return $application;
    }
}