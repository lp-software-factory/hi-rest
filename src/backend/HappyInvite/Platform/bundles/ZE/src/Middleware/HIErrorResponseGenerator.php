<?php
namespace HappyInvite\Platform\Bundles\ZE\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\Platform\Bundles\ZE\StatusExceptions\StatusException400;
use HappyInvite\Platform\Bundles\ZE\StatusExceptions\StatusException403;
use HappyInvite\Platform\Bundles\ZE\StatusExceptions\StatusException404;
use HappyInvite\Platform\Bundles\ZE\StatusExceptions\StatusException409;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;

final class HIErrorResponseGenerator
{
    public function __invoke(\Throwable $e, ServerRequestInterface $request, ResponseInterface $response)
    {
        $responseBuilder = new HIResponseBuilder(new Response());

        if($e instanceof StatusException400) {
            $responseBuilder->setStatusBadRequest();
        }else if($e instanceof StatusException403) {
            $responseBuilder->setStatusNotAllowed();
        }else if($e instanceof StatusException404) {
            $responseBuilder->setStatusNotFound();
        }else if($e instanceof StatusException409) {
            $responseBuilder->setStatusConflict();
        }else{
            $responseBuilder->setStatusInternalError();
        }

        return $responseBuilder
            ->setJSON([
                'trace' => $e->getTrace(),
                'last' => $e->getTrace()[0],
                'error' => $e->getMessage(),
            ])
            ->build();
    }
}