<?php
namespace HappyInvite\Platform\Bundles\ZE\Middleware;

use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class NotFoundMiddleware implements MiddlewareInterface
{
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        $responseBuilder
            ->setJSON([
                'error' => '404 NOT FOUND',
            ])
            ->setStatusNotFound();

        return $responseBuilder->build();
    }
}