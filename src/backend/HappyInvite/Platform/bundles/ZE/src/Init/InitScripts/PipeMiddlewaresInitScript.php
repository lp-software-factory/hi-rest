<?php
namespace HappyInvite\Platform\Bundles\ZE\Init\InitScripts;

use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class PipeMiddlewaresInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        $application->pipeRoutingMiddleware();
        $application->pipeDispatchMiddleware();
        $application->raiseThrowables();
    }
}