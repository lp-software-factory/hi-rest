<?php
namespace HappyInvite\Platform\Bundles\ZE\Init\InitScripts;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;
use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\Platform\Service\BundlesService;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class EventsInitScript implements InitScript
{
    /** @var DomainEventEmitter */
    private $domainEventEmitter;

    public function __invoke(Application $application, ContainerInterface $container)
    {
        $domainEventEmitter = $container->get(DomainEventEmitter::class);
        $bundlesService = $container->get(BundlesService::class);

        $this->domainEventEmitter = $domainEventEmitter;

        foreach($bundlesService->listBundles() as $bundle) {
            $this->bootstrapEvents($bundle, $container);
        }
    }

    private function bootstrapEvents(Bundle $bundle, ContainerInterface $container)
    {
        if($bundle instanceof DomainEventBundle) {
            Chain::create($bundle->getEventScripts())
                ->map(function(string $className) use ($container) {
                    return $container->get($className);
                })
                ->map(function(SubscriptionScript $script) {
                    $script->__invoke($this->domainEventEmitter);
                });
        }
    }
}