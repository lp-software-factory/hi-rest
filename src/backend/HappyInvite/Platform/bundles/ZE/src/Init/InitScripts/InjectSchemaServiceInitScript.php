<?php
namespace HappyInvite\Platform\Bundles\ZE\Init\InitScripts;

use HappyInvite\Platform\Bundles\APIDocs\Schema\SchemaRequest;
use HappyInvite\Platform\Bundles\APIDocs\Service\SchemaService;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class InjectSchemaServiceInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        SchemaRequest::injectSchemaService($container->get(SchemaService::class));
    }
}