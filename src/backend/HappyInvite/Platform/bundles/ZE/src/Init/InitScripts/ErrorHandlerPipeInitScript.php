<?php
namespace HappyInvite\Platform\Bundles\ZE\Init\InitScripts;

use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use HappyInvite\Platform\Bundles\ZE\Middleware\HIErrorResponseGenerator;
use Interop\Container\ContainerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Application;
use Zend\Stratigility\Middleware\ErrorHandler;
use Zend\Stratigility\Middleware\ErrorResponseGenerator;

final class ErrorHandlerPipeInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        $application->pipe(new ErrorHandler(new Response(), new HIErrorResponseGenerator()));
    }
}