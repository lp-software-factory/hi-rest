<?php
namespace HappyInvite\Platform\Bundles\ZE\Init\InitScripts;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Util\GenerateRandomString;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class RoutesInitScript implements InitScript
{
    /** @var string[] */
    private $groups = [];

    public function __construct(array $groups)
    {
        $this->groups = $groups;
    }

    public function __invoke(Application $application, ContainerInterface $container)
    {
        if(! $container->has('router')) {
            throw new \Exception('No router config available');
        }

        $allRoutes = array_map(function(array $input) {
            $method = $input['method'] ?? 'pipe';

            if($method === 'pipe') {
                return new PipeConfig($input);
            }else{
                return new RouteConfig($input);
            }
        }, $container->get('router')['routes']);

        foreach($this->groups as $group) {
            Chain::create($allRoutes)
                ->filter(function(RouteOrPipeConfig $config) use ($group) {
                    return $config->getGroup() === $group;
                })
                ->map(function(RouteOrPipeConfig $config) use ($application) {
                    if($config instanceof RouteConfig) {
                        $application->route(
                            $config->getPath(),
                            $config->getMiddleware(),
                            array_map(function(string $input) {
                                return mb_strtoupper($input);
                            }, $config->getMethods()),
                            $config->getName()
                        );
                    }else if($config instanceof PipeConfig) {
                        $application->pipe(
                            $config->getPath(),
                            $config->getMiddleware()
                        );
                    }else{
                        throw new \Exception(sprintf('No idea what to do with config `%s`, path `%s`', $config->getName(), $config->getPath()));
                    }
                });
        }
    }
}

interface RouteOrPipeConfig
{
    public function getName(): string;
    public function getGroup(): string;
    public function getPath(): string;
    public function getMiddleware(): string;
}

class RouteConfig implements RouteOrPipeConfig
{
    /** @var string */
    private $name;

    /** @var string */
    private $group;

    /** @var string[] */
    private $methods;

    /** @var string */
    private $path;

    /** @var string */
    private $middleware;

    public function __construct(array $config)
    {
        foreach(['path', 'middleware'] as $required) {
            if(! (isset($config[$required]) && is_string($config[$required]) && strlen($config[$required]))) {
                throw new \Exception(sprintf(
                    'RouteConfig `%s` is invalid, field `%s` in unavailable, empty or is not a string',
                    json_encode($config),
                    $required
                ));
            }
        }

        $this->group = $config['group'] ?? 'default';
        $this->methods = $config['method'] ?? 'get';
        $this->path = $config['path'] ?? '';
        $this->middleware = $config['middleware'] ?? '';
        $this->name = $config['name'] ?? GenerateRandomString::generate(32);

        if(is_string($this->methods)) {
            $this->methods = [$this->methods];
        }
    }

    public function getGroup(): string
    {
        return $this->group;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getMiddleware(): string
    {
        return $this->middleware;
    }

    public function getName(): string
    {
        return $this->name;
    }
}

class PipeConfig implements RouteOrPipeConfig
{
    /** @var string */
    private $name;

    /** @var string */
    private $group;

    /** @var string */
    private $path;

    /** @var string */
    private $middleware;

    public function __construct(array $config)
    {
        foreach(['group', 'path', 'middleware'] as $required) {
            if(! (isset($config[$required]) && is_string($config[$required]) && strlen($config[$required]))) {
                throw new \Exception(sprintf(
                    'PipeConfig `%s` is invalid, field `%s` in unavailable, empty or is not a string',
                    json_encode($config),
                    $required
                ));
            }
        }

        $this->name = GenerateRandomString::generate(32);
        $this->group = $config['group'];
        $this->path = $config['path'];
        $this->middleware = $config['middleware'];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGroup(): string
    {
        return $this->group;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getMiddleware(): string
    {
        return $this->middleware;
    }
}