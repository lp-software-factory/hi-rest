<?php
namespace HappyInvite\Platform\Bundles\ZE\Init;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

interface InitScript
{
    public function __invoke(Application $application, ContainerInterface $container);
}