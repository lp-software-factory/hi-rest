<?php
namespace HappyInvite\Platform\Bundles\ZE\Init\InitScripts;

use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use HappyInvite\Platform\Bundles\ZE\Middleware\NotFoundMiddleware;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class NotFoundPipeInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        $application->pipe(new NotFoundMiddleware());
    }
}