<?php
namespace HappyInvite\Platform\Bundles\ZE\Init;

interface InitScriptInjectableBundle
{
    public function getInitScripts(): array;
}