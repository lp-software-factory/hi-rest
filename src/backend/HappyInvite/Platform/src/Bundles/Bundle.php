<?php
namespace HappyInvite\Platform\Bundles;

interface Bundle
{
    /**
     * Возвращает название (идентификатор) бандла
     * Данный идентификатор должен быть уникальным в рамках всего приложения.
     * @return string
     */
    public function getName() : string;

    /**
     * Возвращает неймспейс данного бандла.
     * @return string
     */
    public function getNamespace() : string;

    /**
     * Возвращает корневую директорию бандла
     * Не путать корневую директорию бандла с директорией исходного кода
     * @return string
     */
    public function getDir() : string;

    /**
     * Возвращает дочерние бандлы
     * @return array
     */
    public function getBundles(): array ;

    /**
     * Возвращает true, если данный бандл имеет дочерние бандлы
     * @return bool
     */
    public function hasBundles();

    /**
     * Возвращает директорию, в которой потенциально могут храниться дочерние бандлы
     * @return string
     * @throws \Exception
     */
    public function getBundlesDir() : string;

    /**
     * Возвращает директорию конфигов бандла
     * @return string
     */
    public function getConfigDir() : string;

    /**
     * Возвращает true, если бандл имеет дополнительные ресурсы
     * @return bool
     */
    public function hasResources() : bool;

    /**
     * Возврашает директорию, в которой потенциально могут быть дополнительные ресурсы
     * @return string
     * @throws \Exception
     */
    public function getResourcesDir() : string;
}