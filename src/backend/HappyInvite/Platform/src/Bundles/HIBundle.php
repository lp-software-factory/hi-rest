<?php
namespace HappyInvite\Platform\Bundles;

abstract class HIBundle implements Bundle
{
    public final function getName(): string
    {
        return static::class;
    }

    public final function getNamespace(): string
    {
        return substr(get_called_class(), 0, strrpos(get_called_class(), "\\"));
    }

    abstract public function getDir(): string;

    public final function hasBundles()
    {
        return is_dir($this->getDir() . '/../bundles');
    }

    public function getBundles(): array
    {
        return []; // По умолчанию бандл не содержит дочерних бандлов
                   // Данный метод подразумевается быть переопределяемым.
    }

    public final function getBundlesDir(): string
    {
        if(! $this->hasBundles()) {
            throw new \Exception('No bundles available');
        }

        return $this->getDir() . '/../bundles';
    }

    public final function getConfigDir(): string
    {
        return sprintf("%s/../config", $this->getDir());
    }

    public final function hasResources(): bool
    {
        return is_dir($this->getDir() . '/../resources');
    }

    public final function getResourcesDir(): string
    {
        if(! $this->hasResources()) {
            throw new \Exception('No resources available');
        }

        return $this->getDir() . '/../resources';
    }
}