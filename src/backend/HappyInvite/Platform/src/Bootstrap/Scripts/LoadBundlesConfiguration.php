<?php
namespace HappyInvite\Platform\Bootstrap\Scripts;

use Cocur\Chain\Chain;
use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\Platform\Constants\Environment;
use HappyInvite\Platform\Service\BundlesService;
use HappyInvite\Platform\Service\ConfigService;

final class LoadBundlesConfiguration
{
    public function __invoke(
        ?string $environment,
        string $provideConfigDir,
        ConfigService $configService,
        BundlesService $bundlesService
    ) {
        $this->mergeBundleConfigs($configService, $bundlesService->listBundles());

        if($environment !== null) {
            if(!in_array($environment, Environment::LIST_ENVIRONMENTS)) {
                throw new \Exception(sprintf('Unknown environment `%s`', $environment));
            }

            $configService->merge([
                'config.environment' => $environment
            ]);
        }

        if(! $configService->has('config.environment')) {
            $configService->merge([
                'config.environment' => Environment::DEVELOPMENT
            ]);
        }

        $this->mergeEnvConfig($configService, $configService->get('config.environment'), $bundlesService->listBundles());
        $this->mergeProvideConfig($configService, $provideConfigDir);
    }

    private function mergeBundleConfigs(ConfigService $configService, array $bundles)
    {
        array_walk($bundles, function(Bundle $bundle) use ($configService) {
            $dir = $bundle->getConfigDir();

            if(is_dir($dir)) {
                Chain::create(scandir($dir))
                    ->filter(function($input) use ($dir) {
                        return is_file($dir . '/' . $input) && preg_match('/\.config.php$/', $input);
                    })
                    ->map(function($input) use ($dir) {
                        return require $dir . '/' . $input;
                    })
                    ->map(function(array $config) use ($configService) {
                        $configService->merge($config);

                        return null;
                    });
            }
        });
    }

    private function mergeProvideConfig(ConfigService $configService, string $path)
    {
        $provideConfigFileName = sprintf('%s/provide.config.php', $path);

        if(file_exists($provideConfigFileName)) {
            $provideConfig = require $provideConfigFileName;

            $configService->merge($provideConfig);
        }
    }

    private function mergeEnvConfig(ConfigService $configService, string $env, array $bundles)
    {
        array_walk($bundles, function(Bundle $bundle) use ($configService, $env) {
            foreach ([$env, 'default'] as $next) {
                $dir = $bundle->getConfigDir().'/env/'.$next;

                if(is_dir($dir)) {
                    Chain::create(scandir($dir))
                        ->filter(function($input) use ($dir) {
                            return is_file($dir . '/' . $input) && preg_match('/\.config.php$/', $input);
                        })
                        ->map(function($input) use ($dir) {
                            return require $dir . '/' . $input;
                        })
                        ->map(function(array $config) use ($configService) {
                            $configService->merge($config);

                            return null;
                        });

                    return; // Do not load "default" configuration
                }
            }
        });
    }
}