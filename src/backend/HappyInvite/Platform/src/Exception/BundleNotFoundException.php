<?php
namespace HappyInvite\Platform\Exception;

final class BundleNotFoundException extends \Exception {}