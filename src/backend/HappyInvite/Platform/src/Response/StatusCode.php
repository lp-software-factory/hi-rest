<?php
namespace HappyInvite\Platform\Response;

interface StatusCode
{
    public const HTTP_200_OK = 200;
    public const HTTP_400_BAD_REQUEST = 400;
    public const HTTP_403_NOT_ALLOWED = 403;
    public const HTTP_404_NOT_FOUND = 404;
    public const HTTP_409_CONFLICT = 409;
    public const HTTP_422_UN_PROCESSABLE = 422;
    public const HTTP_500_INTERNAL_SERVER_ERROR = 500;
}