<?php
namespace HappyInvite\Platform\Response\JSON;

use HappyInvite\Platform\Response\JSON\Decorators\Lib\ErrorResponseDecorator;
use HappyInvite\Platform\Response\JSON\Decorators\Lib\NotBadResponseDecorator;
use HappyInvite\Platform\Response\JSON\Decorators\Lib\SuccessResponseDecorator;
use HappyInvite\Platform\Response\JSON\Decorators\Lib\TimeResponseDecorator;

final class HIResponseBuilder extends ResponseBuilder
{
    protected function initDecorators(ResponseDecoratorsManager $decoratorsManager)
    {
        $decoratorsManager
            ->attach(new SuccessResponseDecorator())
            ->attach(new TimeResponseDecorator())
            ->attach(new ErrorResponseDecorator())
            ->attach(new NotBadResponseDecorator())
        ;
    }
}