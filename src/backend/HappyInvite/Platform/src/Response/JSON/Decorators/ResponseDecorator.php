<?php
namespace HappyInvite\Platform\Response\JSON\Decorators;

use HappyInvite\Platform\Response\JSON\ResponseBuilder;

interface ResponseDecorator
{
    public function decorate(ResponseBuilder $builder, array &$response): void;
}