<?php
namespace HappyInvite\Platform\Response\JSON\Decorators\Lib;

use HappyInvite\Platform\Response\JSON\Decorators\ResponseDecorator;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;

final class NotBadResponseDecorator implements ResponseDecorator
{
    public const THRESHOLD = (1024 /* bytes */ * 1024 /* kb */);

    public function decorate(ResponseBuilder $builder, array &$response): void
    {
        if(strlen(json_encode($response)) >= self::THRESHOLD) {
            $response['too_big'] = '¯\_(ツ)_/¯';
        }
    }
}