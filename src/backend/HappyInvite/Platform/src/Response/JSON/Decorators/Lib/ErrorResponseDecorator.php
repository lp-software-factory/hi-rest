<?php
namespace HappyInvite\Platform\Response\JSON\Decorators\Lib;

use HappyInvite\Platform\Response\JSON\Decorators\ResponseDecorator;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;

final class ErrorResponseDecorator implements ResponseDecorator
{
    public function decorate(ResponseBuilder $builder, array &$response): void
    {
        $error = $builder->getError();
        
        if($error) {
            if($error instanceof \Exception) {
                $errorMessage = $error->getMessage();
                $response['error_stack'] = $error->getTrace();
            }else if($error instanceof \TypeError) {
                $errorMessage = $error->getMessage();
                $response['error_stack'] = $error->getTrace();
            }else if(is_string($error)) {
                $errorMessage = $error;
            }else if($error === null){
                $errorMessage = 'No error message available';
            }else{
                $errorMessage = (string) $error;
            }

            $response['error'] = $errorMessage;
        }
    }
}