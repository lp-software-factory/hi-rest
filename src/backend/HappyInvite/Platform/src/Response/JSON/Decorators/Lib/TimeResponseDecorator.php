<?php
namespace HappyInvite\Platform\Response\JSON\Decorators\Lib;

use HappyInvite\Platform\Response\JSON\Decorators\ResponseDecorator;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;

final class TimeResponseDecorator implements ResponseDecorator
{
    public function decorate(ResponseBuilder $builder, array &$response): void
    {
        if(defined('APP_TIMER_START')) {
            $response['time'] = sprintf('%01.4f', (microtime(true) - APP_TIMER_START));
        }
    }
}