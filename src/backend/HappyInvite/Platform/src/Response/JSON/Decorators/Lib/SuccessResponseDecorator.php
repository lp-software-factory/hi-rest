<?php
namespace HappyInvite\Platform\Response\JSON\Decorators\Lib;

use HappyInvite\Platform\Response\JSON\Decorators\ResponseDecorator;
use HappyInvite\Platform\Response\JSON\ResponseBuilder;
use HappyInvite\Platform\Response\StatusCode;

final class SuccessResponseDecorator implements ResponseDecorator
{
    public function decorate(ResponseBuilder $builder, array &$response): void
    {
        $success =
                 $builder->getStatus() === StatusCode::HTTP_200_OK
            && (! $builder->hasError());

        $response['success'] = $success;
    }
}