<?php
namespace HappyInvite\Platform\Service;

final class ConfigService
{
    /** @var array */
    private $config = [];

    public function all()
    {
        return $this->config;
    }

    public function has(string $key)
    {
        return isset($this->config[$key]);
    }

    public function get(string $key)
    {
        if(! (isset($this->config[$key]))) {
            throw new \OutOfBoundsException(sprintf('Config key `%s` not found', $key));
        }

        return $this->config[$key];
    }

    public function merge($config)
    {
        $this->config = $this->array_merge_recursive_unique($this->config, $config);
    }

    private function array_merge_recursive_unique($array1, $array2)
    {

        $arrays = func_get_args();
        $narrays = count($arrays);

        for ($i = 0; $i < $narrays; $i++) {
            if (!is_array($arrays[$i])) {
                trigger_error('Argument #' . ($i + 1) . ' is not an array - trying to merge array with scalar! Returning null!', E_USER_WARNING);
                return;
            }
        }

        $ret = $arrays[0];

        for ($i = 1; $i < $narrays; $i++) {
            foreach ($arrays[$i] as $key => $value) {
                if (((string)$key) === ((string)intval($key))) {
                    $ret[] = $value;
                } else {
                    if (is_array($value) && isset($ret[$key])) {
                        $ret[$key] = $this->array_merge_recursive_unique($ret[$key], $value);
                    } else {
                        $ret[$key] = $value;
                    }
                }
            }
        }

        return $ret;
    }
}