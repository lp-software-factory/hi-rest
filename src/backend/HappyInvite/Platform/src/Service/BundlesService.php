<?php
namespace HappyInvite\Platform\Service;

use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\Platform\Exception\BundleNotFoundException;

final class BundlesService
{
    /** @var  \HappyInvite\Platform\Bundles\Bundle[] */
    private $bundles = [];

    public function addBundle(Bundle $bundle)
    {
        $this->bundles[get_class($bundle)] = $bundle;
    }

    public function addBundleRecursive(Bundle $bundle)
    {
        $this->addBundle($bundle);

        if($bundle->hasBundles()) {
            array_map(function(Bundle $bundle) {
                $this->addBundleRecursive($bundle);
            }, $bundle->getBundles());
        }
    }

    public function addBundlesRecursive(array $bundles)
    {
        foreach($bundles as $bundle) {
            $this->addBundleRecursive($bundle);
        }
    }

    public function hasBundle(Bundle $bundle): bool
    {
        return isset($this->bundles[get_class($bundle)]);
    }

    public function removeBundle(Bundle $bundle)
    {
        if($this->hasBundle($bundle)) {
            unset($this->bundles[get_class($bundle)]);
        }
    }

    public function removeBundleRecursive(Bundle $bundle)
    {
        $this->removeBundle($bundle);

        if($bundle->hasBundles()) {
            array_map(function(Bundle $bundle) {
                $this->removeBundleRecursive($bundle);
            }, $this->bundles);
        }
    }

    public function listBundles(): array
    {
        return $this->bundles;
    }

    public function getBundleWithClassName(string $bundleClassName): Bundle
    {
        if(isset($this->bundles[$bundleClassName])) {
            return $this->bundles[$bundleClassName];
        }else{
            throw new BundleNotFoundException(sprintf('Bundle with name `%s` not found'));
        }
    }
}