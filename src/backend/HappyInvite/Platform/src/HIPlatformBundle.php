<?php
namespace HappyInvite\Platform;

use HappyInvite\Platform\Bundles\APIDocs\APIDocsPlatformBundle;
use HappyInvite\Platform\Bundles\HIBundle;
use HappyInvite\Platform\Bundles\ZE\ZEPlatformBundle;

final class HIPlatformBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new APIDocsPlatformBundle(),
            new ZEPlatformBundle(),
        ];
    }
}