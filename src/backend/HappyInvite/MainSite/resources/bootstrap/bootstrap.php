<?php
namespace HappyInvite\REST;

use HappyInvite\Domain\HIDomainBundle;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigService;
use HappyInvite\MainSite\HIMainSiteBundle;
use HappyInvite\Platform\HIPlatformBundle;
use Zend\Expressive\Application;

$appBuilder = require __DIR__.'/../../../Platform/resources/bootstrap/bootstrap.php';

/** @var Application $application */
$application = $appBuilder([
    'bundles' => [
        new HIPlatformBundle(),
        new HIDomainBundle(),
        new HIMainSiteBundle(),
    ]
]);

$application->getContainer()->get(TwigService::class);

return $application;