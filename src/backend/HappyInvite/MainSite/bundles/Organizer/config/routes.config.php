<?php
namespace HappyInvite\MainSite\Bundles\Organizer;

use HappyInvite\MainSite\Bundles\Organizer\Controller\LandingController;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/organizer[/]',
                'middleware' => LandingController::class,
            ],
        ]
    ]
];