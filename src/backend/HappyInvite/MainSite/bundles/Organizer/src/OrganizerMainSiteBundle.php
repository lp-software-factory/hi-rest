<?php
namespace HappyInvite\MainSite\Bundles\Organizer;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class OrganizerMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}