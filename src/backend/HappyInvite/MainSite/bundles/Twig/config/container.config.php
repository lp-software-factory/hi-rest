<?php
namespace HappyInvite\MainSite\Bundles\Designer;

use function DI\get;
use function DI\object;
use function DI\factory;

use HappyInvite\MainSite\Bundles\Twig\Config\TwigConfig;

return [
    TwigConfig::class => object()->constructorParameter('config', get('config.hi.main-site.twig')),
];