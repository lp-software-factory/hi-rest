<?php
namespace HappyInvite\MainSite\Bundles\Twig;

use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use HappyInvite\Domain\Bundles\Locale\Service\SessionLocaleService;

return [
    'config.hi.main-site.twig' => [
        'exports' => [
            'SessionLocaleService' => SessionLocaleService::class,
            'FrontendService' => FrontendService::class,
        ],
        'paths' => [
            'main' => __DIR__.'/../../../../../../frontend/src/templates/twig',
        ],
        'options' => [
            'charset' => 'utf-8',
            'debug' => false,
            'auto_reload' => true,
            'strict_variables' => true,
            'autoescape' => true,
            'optimizations' => -1,
        ],
    ]
];