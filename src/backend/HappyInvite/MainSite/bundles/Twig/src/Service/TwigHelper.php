<?php
namespace HappyInvite\MainSite\Bundles\Twig\Service;

use HappyInvite\Platform\Response\HTML\HTMLResponseBuilder;
use HappyInvite\Platform\Response\StatusCode;
use Psr\Http\Message\ResponseInterface as Response;

final class TwigHelper
{
    /** @var TwigService */
    private $twigService;

    /** @var TwigHelper */
    private static $instance;

    public function __construct()
    {
        self::$instance = $this;
    }

    public function setTwigService(TwigService $twigService)
    {
        $this->twigService = $twigService;
    }

    public static function getInstance(): TwigHelper
    {
        return self::$instance;
    }

    public function htmlResponse(Response $response, string $template, array $context = [], $status = StatusCode::HTTP_200_OK): Response
    {
        $responseBuilder = new HTMLResponseBuilder($response);

        $result = $this->twigService->render($template, $context);
        $responseBuilder
            ->setStatus($status)
            ->setContent($result);

        return $responseBuilder->build();
    }

    public function html404Response(Response $response): Response
    {
        return self::getInstance()->htmlResponse($response, '@main/error/404/index.html.twig');
    }
}