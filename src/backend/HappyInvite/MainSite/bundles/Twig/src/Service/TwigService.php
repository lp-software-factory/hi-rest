<?php
namespace HappyInvite\MainSite\Bundles\Twig\Service;

use HappyInvite\MainSite\Bundles\Twig\Config\TwigConfig;
use Interop\Container\ContainerInterface;
use Twig_Environment;

final class TwigService
{
    /** @var TwigConfig */
    private $twigConfig;

    /** @var Twig_Environment */
    private $twigEnvironment;

    /** @var array */
    private $exportContext = [];

    /** @var ContainerInterface */
    private $container;

    public function __construct(
        TwigConfig $twigConfig,
        TwigHelper $twigHelper,
        ContainerInterface $container
    ) {
        $twigLoader = new \Twig_Loader_Filesystem();

        foreach($twigConfig->getPaths() as $id => $path) {
            $twigLoader->addPath($path, $id);
        }

        $this->container = $container;
        $this->twigConfig = $twigConfig;
        $this->twigEnvironment = new \Twig_Environment($twigLoader, $twigConfig->getOptions());

        $this->twigEnvironment->addFilter(new \Twig_SimpleFilter('addslashes', 'addslashes'));

        $twigHelper->setTwigService($this);
    }

    private function generateContext()
    {
        foreach($this->twigConfig->getExports() as $key => $service) {
            $this->exportContext[$key] = $this->container->get($service);
        }
    }

    public function getTwigConfig(): TwigConfig
    {
        return $this->twigConfig;
    }

    public function getTwigEnvironment(): Twig_Environment
    {
        return $this->twigEnvironment;
    }

    public function render(string $name, array $context): string
    {
        if(! $this->exportContext) {
            $this->generateContext();
        }

        return $this->twigEnvironment->render($name, array_merge($this->exportContext, $context));
    }
}