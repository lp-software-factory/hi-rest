<?php
namespace HappyInvite\MainSite\Bundles\Twig;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class TwigMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}