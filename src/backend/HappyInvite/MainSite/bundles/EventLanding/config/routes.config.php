<?php
namespace HappyInvite\MainSite\Bundles\EventLanding;

use HappyInvite\MainSite\Bundles\EventLanding\Middleware\EventLandingMiddleware;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/event/landing/{eventSID}/{recipient}[/]',
                'middleware' => EventLandingMiddleware::class,
            ],
            [
                'method' => 'get',
                'path' => '/event/landing/{eventSID}[/]',
                'middleware' => EventLandingMiddleware::class,
            ],
        ]
    ]
];