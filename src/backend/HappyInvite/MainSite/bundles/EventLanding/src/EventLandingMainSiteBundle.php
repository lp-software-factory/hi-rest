<?php
namespace HappyInvite\MainSite\Bundles\EventLanding;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class EventLandingMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}