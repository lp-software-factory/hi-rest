<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Middleware;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventNotFoundException;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\EventLandingResources;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class EventLandingMiddleware implements MiddlewareInterface
{
    /** @var EventService */
    private $eventService;

    /** @var EventLandingResources */
    private $eventLandingResources;

    public function __construct(EventService $eventService, EventLandingResources $eventLandingResources)
    {
        $this->eventService = $eventService;
        $this->eventLandingResources = $eventLandingResources;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        try {
            $event = $this->eventService->getBySID($eventSID = $request->getAttribute('eventSID'));

            $this->eventLandingResources->export($event, $request);

            return TwigHelper::getInstance()->htmlResponse($response, '@main/event-landing/views/index.html.twig', [
                'event' => $event,
                'eventSID' => $eventSID,
            ]);
        }catch(EventNotFoundException $e) {
            return TwigHelper::getInstance()->html404Response($response);
        }
    }
}