<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Fonts\Formatter\FontFormatter;
use HappyInvite\Domain\Bundles\Fonts\Service\FontService;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\EventLandingResource;
use Psr\Http\Message\ServerRequestInterface;

final class FontsResource implements EventLandingResource
{
    /** @var FontService */
    private $fontService;

    /** @var FontFormatter */
    private $fontFormatter;

    public function __construct(FontService $fontService, FontFormatter $fontFormatter)
    {
        $this->fontService = $fontService;
        $this->fontFormatter = $fontFormatter;
    }

    public function getKey(): string
    {
        return 'fonts';
    }

    public function fetch(Event $event, ServerRequestInterface $request): array
    {
        return $this->fontFormatter->formatMany(
            $this->fontService->getAll()
        );
    }
}