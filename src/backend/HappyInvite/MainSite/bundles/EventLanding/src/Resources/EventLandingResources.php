<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Resources;

use Cocur\Chain\Chain;
use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources\DressCodeResource;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources\EventRecipientsResource;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources\EventResource;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources\FontsResource;
use Psr\Http\Message\ServerRequestInterface;

final class EventLandingResources
{
    /** @var Container */
    private $container;

    /** @var FrontendService */
    private $frontendService;

    public function __construct(Container $container, FrontendService $frontendService)
    {
        $this->container = $container;
        $this->frontendService = $frontendService;
    }

    public function export(Event $event, ServerRequestInterface $request): void
    {
        Chain::create([
            EventResource::class,
            FontsResource::class,
            EventRecipientsResource::class,
            DressCodeResource::class,
        ])
            ->map(function(string $className): EventLandingResource {
                return $this->container->get($className);
            })
            ->map(function (EventLandingResource $resource) use ($event, $request) {
                $this->frontendService->export($resource->getKey(), $resource->fetch($event, $request));
            });
    }
}