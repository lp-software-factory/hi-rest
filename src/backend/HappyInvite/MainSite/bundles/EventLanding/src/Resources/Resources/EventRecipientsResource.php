<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Formatter\EventRecipientsFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipients\Service\EventRecipientsService;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\EventLandingResource;
use Psr\Http\Message\ServerRequestInterface;

final class EventRecipientsResource implements EventLandingResource
{
    /** @var EventRecipientsService */
    private $eventRecipientsService;

    /** @var EventRecipientsFormatter */
    private $eventRecipientsFormatter;

    public function __construct(
        EventRecipientsService $eventRecipientsService,
        EventRecipientsFormatter $eventRecipientsFormatter
    ) {
        $this->eventRecipientsService = $eventRecipientsService;
        $this->eventRecipientsFormatter = $eventRecipientsFormatter;
    }

    public function getKey(): string
    {
        return 'event_recipients';
    }

    public function fetch(Event $event, ServerRequestInterface $request): array
    {
        return $this->eventRecipientsFormatter->formatOne(
            $this->eventRecipientsService->getByEventId($event->getId())
        );
    }
}