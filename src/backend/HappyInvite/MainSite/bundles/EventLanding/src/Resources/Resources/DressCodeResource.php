<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources;

use HappyInvite\Domain\Bundles\DressCode\Formatter\DressCodeFormatter;
use HappyInvite\Domain\Bundles\DressCode\Service\DressCodeService;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\EventLandingResource;
use Psr\Http\Message\ServerRequestInterface;

final class DressCodeResource implements EventLandingResource
{
    /** @var DressCodeService */
    private $dressCodeService;

    /** @var DressCodeFormatter */
    private $dressCodeFormatter;

    public function __construct(
        DressCodeService $dressCodeService,
        DressCodeFormatter $dressCodeFormatter
    ) {
        $this->dressCodeService = $dressCodeService;
        $this->dressCodeFormatter = $dressCodeFormatter;
    }

    public function getKey(): string
    {
        return 'dress_codes';
    }

    public function fetch(Event $event, ServerRequestInterface $request): array
    {
        return $this->dressCodeFormatter->formatMany(
            $this->dressCodeService->getAll()
        );
    }
}