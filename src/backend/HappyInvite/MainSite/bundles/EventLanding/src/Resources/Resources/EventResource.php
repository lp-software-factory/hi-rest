<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Resources\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Formatter\EventFormatter;
use HappyInvite\MainSite\Bundles\EventLanding\Resources\EventLandingResource;
use Psr\Http\Message\ServerRequestInterface;

final class EventResource implements EventLandingResource
{
    /** @var EventFormatter */
    private $eventFormatter;

    public function __construct(EventFormatter $eventFormatter)
    {
        $this->eventFormatter = $eventFormatter;
    }

    public function getKey(): string
    {
        return 'event';
    }

    public function fetch(Event $event, ServerRequestInterface $request): array
    {
        return $this->eventFormatter->formatOne($event);
    }
}