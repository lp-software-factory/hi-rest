<?php
namespace HappyInvite\MainSite\Bundles\EventLanding\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use Psr\Http\Message\ServerRequestInterface;

interface EventLandingResource
{
    public function getKey(): string;
    public function fetch(Event $event, ServerRequestInterface $request): array ;
}