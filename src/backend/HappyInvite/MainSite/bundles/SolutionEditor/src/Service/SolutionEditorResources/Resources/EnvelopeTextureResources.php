<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Formatter\EnvelopeTextureFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class EnvelopeTextureResources implements SolutionEditorResource
{
    /** @var EnvelopeTextureService */
    private $service;

    /** @var EnvelopeTextureFormatter */
    private $formatter;

    public function __construct(EnvelopeTextureService $service, EnvelopeTextureFormatter $formatter)
    {
        $this->service = $service;
        $this->formatter = $formatter;
    }

    public function getKey(): string
    {
        return 'envelope_textures';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAllPublic()
        );
    }
}