<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\DressCode\Formatter\DressCodeFormatter;
use HappyInvite\Domain\Bundles\DressCode\Service\DressCodeService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class DressCodeResources implements SolutionEditorResource
{
    /** @var DressCodeService */
    private $service;

    /** @var DressCodeFormatter */
    private $formatter;

    public function __construct(DressCodeService $dressCodeService, DressCodeFormatter $dressCodeFormatter)
    {
        $this->service = $dressCodeService;
        $this->formatter = $dressCodeFormatter;
    }

    public function getKey(): string
    {
        return 'dress_codes';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAll()
        );
    }
}