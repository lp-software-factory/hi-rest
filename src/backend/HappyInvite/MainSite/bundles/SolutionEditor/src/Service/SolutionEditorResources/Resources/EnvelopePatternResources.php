<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Formatter\EnvelopePatternFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class EnvelopePatternResources implements SolutionEditorResource
{
    /** @var EnvelopePatternService */
    private $service;

    /** @var EnvelopePatternFormatter */
    private $formatter;

    public function __construct(EnvelopePatternService $envelopePatternService, EnvelopePatternFormatter $envelopePatternFormatter)
    {
        $this->service = $envelopePatternService;
        $this->formatter = $envelopePatternFormatter;
    }

    public function getKey(): string
    {
        return 'envelope_patterns';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAllPublic()
        );
    }
}