<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources;

interface SolutionEditorResource
{
    public function getKey(): string;
    public function fetch(): array ;
}