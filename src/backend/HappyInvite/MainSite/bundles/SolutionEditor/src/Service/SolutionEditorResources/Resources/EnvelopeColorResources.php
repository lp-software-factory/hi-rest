<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Formatter\EnvelopeColorFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service\EnvelopeColorService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class EnvelopeColorResources implements SolutionEditorResource
{
    /** @var EnvelopeColorService */
    private $service;

    /** @var EnvelopeColorFormatter */
    private $formatter;

    public function __construct(EnvelopeColorService $service, EnvelopeColorFormatter $formatter)
    {
        $this->service = $service;
        $this->formatter = $formatter;
    }

    public function getKey(): string
    {
        return 'envelope_colors';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAllPublic()
        );
    }
}