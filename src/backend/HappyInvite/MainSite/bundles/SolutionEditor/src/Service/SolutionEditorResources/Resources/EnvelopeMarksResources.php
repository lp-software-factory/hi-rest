<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Formatter\EnvelopeMarksFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarksService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class EnvelopeMarksResources implements SolutionEditorResource
{
    /** @var EnvelopeMarksService */
    private $service;

    /** @var EnvelopeMarksFormatter */
    private $formatter;

    public function __construct(EnvelopeMarksService $service, EnvelopeMarksFormatter $formatter)
    {
        $this->service = $service;
        $this->formatter = $formatter;
    }

    public function getKey(): string
    {
        return 'envelope_marks';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAll()
        );
    }
}