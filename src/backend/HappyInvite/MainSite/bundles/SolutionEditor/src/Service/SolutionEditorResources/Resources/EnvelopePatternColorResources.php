<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Formatter\EnvelopeColorFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service\EnvelopeColorService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class EnvelopePatternColorResources implements SolutionEditorResource
{
    /** @var EnvelopeColorService */
    private $service;

    /** @var EnvelopeColorFormatter */
    private $formatter;

    public function __construct(EnvelopeColorService $envelopePatternColorService, EnvelopeColorFormatter $envelopePatternColorFormatter)
    {
        $this->service = $envelopePatternColorService;
        $this->formatter = $envelopePatternColorFormatter;
    }

    public function getKey(): string
    {
        return 'envelope_pattern_colors';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAll()
        );
    }
}