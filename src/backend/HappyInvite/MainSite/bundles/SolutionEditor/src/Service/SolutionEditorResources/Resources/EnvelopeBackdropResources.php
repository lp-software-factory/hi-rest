<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Formatter\EnvelopeBackdropFormatter;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class EnvelopeBackdropResources implements SolutionEditorResource
{
    /** @var EnvelopeBackdropService */
    private $service;

    /** @var EnvelopeBackdropFormatter */
    private $formatter;

    public function __construct(EnvelopeBackdropService $envelopeBackdropService, EnvelopeBackdropFormatter $envelopeBackdropFormatter)
    {
        $this->service = $envelopeBackdropService;
        $this->formatter = $envelopeBackdropFormatter;
    }

    public function getKey(): string
    {
        return 'envelope_backdrops';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAllPublic()
        );
    }
}