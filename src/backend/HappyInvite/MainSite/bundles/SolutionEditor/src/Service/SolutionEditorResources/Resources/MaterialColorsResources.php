<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Palette\Entity\Color;
use HappyInvite\Domain\Bundles\Palette\Repository\ColorsRepository;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class MaterialColorsResources implements SolutionEditorResource
{
    /** @var ColorsRepository */
    private $repository;

    public function __construct(ColorsRepository $colorsRepository)
    {
        $this->repository = $colorsRepository;
    }

    public function getKey(): string
    {
        return 'material_colors';
    }

    public function fetch(): array
    {
        return array_map(function(Color $color) {
            return $color->toJSON();
        }, array_values($this->repository->getColors()));
    }
}