<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources;

use HappyInvite\Domain\Bundles\Fonts\Formatter\FontFormatter;
use HappyInvite\Domain\Bundles\Fonts\Service\FontService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResource;

final class FontResources implements SolutionEditorResource
{
    /** @var FontService */
    private $service;

    /** @var FontFormatter */
    private $formatter;

    public function __construct(FontService $fontsService, FontFormatter $fontsFormatter)
    {
        $this->service = $fontsService;
        $this->formatter = $fontsFormatter;
    }

    public function getKey(): string
    {
        return 'fonts';
    }

    public function fetch(): array
    {
        return $this->formatter->formatMany(
            $this->service->getAll()
        );
    }
}