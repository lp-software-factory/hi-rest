<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources;

use Cocur\Chain\Chain;
use DI\Container;
use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\DressCodeResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\EnvelopeBackdropResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\EnvelopeColorResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\EnvelopeMarksResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\EnvelopePatternColorResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\EnvelopePatternResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\EnvelopeTextureResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\FontResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\Resources\MaterialColorsResources;

final class SolutionEditorResourcesService
{
    /** @var Container */
    private $container;

    /** @var FrontendService */
    private $frontendService;

    public function __construct(Container $container, FrontendService $frontendService)
    {
        $this->container = $container;
        $this->frontendService = $frontendService;
    }

    public function export(): void
    {
        Chain::create([
            DressCodeResources::class,
            EnvelopeBackdropResources::class,
            EnvelopePatternResources::class,
            EnvelopePatternColorResources::class,
            EnvelopeTextureResources::class,
            EnvelopeMarksResources::class,
            EnvelopeColorResources::class,
            FontResources::class,
            MaterialColorsResources::class,
        ])
            ->map(function(string $className): SolutionEditorResource {
                return $this->container->get($className);
            })
            ->map(function (SolutionEditorResource $resource) {
                $this->frontendService->export($resource->getKey(), $resource->fetch());
            });
    }
}