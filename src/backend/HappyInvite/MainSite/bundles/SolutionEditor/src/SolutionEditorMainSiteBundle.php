<?php
namespace HappyInvite\MainSite\Bundles\SolutionEditor;

use HappyInvite\Platform\Bundles\HIBundle;

final class SolutionEditorMainSiteBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}