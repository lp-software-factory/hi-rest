<?php
namespace HappyInvite\MainSite\Bundles\Landing;

use HappyInvite\MainSite\Bundles\Landing\Controller\LandingController;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '[/]',
                'middleware' => LandingController::class,
            ],
            [
                'method' => 'get',
                'path' => '/landing[/]',
                'middleware' => LandingController::class,
            ],
            [
                'method' => 'get',
                'path' => '/landing/{eventTypeGroupURL}[/]',
                'middleware' => LandingController::class,
            ]
        ]
    ]
];