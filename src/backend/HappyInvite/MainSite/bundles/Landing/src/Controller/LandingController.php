<?php
namespace HappyInvite\MainSite\Bundles\Landing\Controller;

use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class LandingController implements MiddlewareInterface
{
    public function __invoke(Request $request, ResponseInterface $response, callable $out = null)
    {
        $eventTypeGroupURL = $request->getAttribute('eventTypeGroupURL', 'default');

        return TwigHelper::getInstance()->htmlResponse($response, sprintf('@main/landing/%s.html.twig', $eventTypeGroupURL));
    }
}