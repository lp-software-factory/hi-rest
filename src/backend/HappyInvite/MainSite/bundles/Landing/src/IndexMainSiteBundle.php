<?php
namespace HappyInvite\MainSite\Bundles\Landing;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class IndexMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}