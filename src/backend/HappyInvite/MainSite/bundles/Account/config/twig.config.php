<?php
namespace HappyInvite\MainSite\Bundles\Account;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Auth\Service\AuthService;

return [
    'config.hi.main-site.twig' => [
        'exports' => [
            'AuthService' => AuthService::class,
            'AuthToken' => AuthToken::class,
        ],
    ],
];