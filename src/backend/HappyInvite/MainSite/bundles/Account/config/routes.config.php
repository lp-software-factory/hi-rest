<?php
namespace HappyInvite\MainSite\Bundles\Account;

use HappyInvite\MainSite\Bundles\Account\Controller\ForgotPasswordController;
use HappyInvite\MainSite\Bundles\Account\Controller\ProceedController;
use HappyInvite\MainSite\Bundles\Account\Controller\RegisterController;
use HappyInvite\MainSite\Bundles\Account\Controller\ResetPasswordController;
use HappyInvite\MainSite\Bundles\Account\Controller\SignInController;
use HappyInvite\MainSite\Bundles\Account\Controller\SignOutController;
use HappyInvite\MainSite\Bundles\Account\Pipe\AuthPipe;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'pipe',
                'path' => '/',
                'group' => 'auth',
                'middleware' => AuthPipe::class,
            ],
            [
                'method' => ['get', 'post'],
                'path' => '/account/register[/]',
                'middleware' => RegisterController::class,
            ],
            [
                'method' => ['get', 'post'],
                'path' => '/account/sign-in[/]',
                'middleware' => SignInController::class,
                'name' => 'hi-account-sign-in',
            ],
            [
                'method' => 'get',
                'path' => '/account/reset-password[/]',
                'middleware' => ResetPasswordController::class,
            ],
            [
                'method' => 'get',
                'path' => '/account/forgot-password[/]',
                'middleware' => ForgotPasswordController::class,
            ],
            [
                'method' => 'get',
                'path' => '/account/sign-out[/]',
                'middleware' => SignOutController::class,
            ],
            [
                'method' => 'get',
                'path' => '/account/proceed[/]',
                'middleware' => ProceedController::class,
            ],
        ]
    ]
];