<?php
namespace HappyInvite\MainSite\Bundles\Account;

return [
    'config.hi.main-site.auth' => [
        'mail' => [
            'templates' => [
                'dir' => 'mail/auth'
            ]
        ]
    ],
    'config.hi.main-site.account' => [
        'mail' => [
            'templates' => [
                'dir' => 'mail/account'
            ]
        ]
    ]
];