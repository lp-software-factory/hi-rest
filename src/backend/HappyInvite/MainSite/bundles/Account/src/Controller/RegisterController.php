<?php
namespace HappyInvite\MainSite\Bundles\Account\Controller;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigService;
use HappyInvite\Platform\Response\HTML\HTMLResponseBuilder;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class RegisterController implements MiddlewareInterface
{
    /** @var AuthToken */
    private $authToken;

    public function __construct(TwigService $twig, AuthToken $authToken)
    {
        $this->authToken = $authToken;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        if($this->authToken->isSignedIn()) {
            header('Location: /solutions');
            exit();
        }

        if(strtolower($request->getMethod()) === 'get') {
            return $this->indexAction($request, $response);
        }else if(strtolower($request->getMethod()) === 'post') {
            return $this->sendAction($request, $response);
        }else{
            $responseBuilder = new HIResponseBuilder($response);

            $responseBuilder
                ->setError('404 NOT FOUND')
                ->setStatusNotFound();

            return $responseBuilder->build();
        }

    }

    private function indexAction(Request $request, Response $response, callable $out = null)
    {
        return TwigHelper::getInstance()->htmlResponse($response, '@main/account/views/register/index.html.twig');
    }

    private function sendAction(Request $request, Response $response, callable $out = null)
    {
    }
}