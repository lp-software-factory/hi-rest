<?php
namespace HappyInvite\MainSite\Bundles\Account\Controller;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Domain\Bundles\Auth\Exceptions\InvalidPasswordException;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Auth\Service\AuthService;
use HappyInvite\Domain\Bundles\Auth\Service\SessionJWTService;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use HappyInvite\Platform\Response\JSON\HIResponseBuilder;
use HappyInvite\REST\Bundles\Auth\Request\SignInRequest;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class SignInController implements MiddlewareInterface
{
    /** @var AuthService */
    private $authService;

    /** @var AuthToken */
    private $authToken;

    /** @var SessionJWTService */
    private $sessionJWTService;

    public function __construct(
        AuthService $authService,
        AuthToken $authToken,
        SessionJWTService $sessionJWTService
    ) {
        $this->authToken = $authToken;
        $this->authService = $authService;
        $this->sessionJWTService = $sessionJWTService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        if($this->authToken->isSignedIn()) {
            header('Location: /solutions');
            exit();
        }

        if(strtolower($request->getMethod()) === 'get') {
            return $this->indexAction($request, $response);
        }else if(strtolower($request->getMethod()) === 'post') {
            return $this->attemptSignInAction($request, $response);
        }else{
            $responseBuilder = new HIResponseBuilder($response);

            $responseBuilder
                ->setError('404 NOT FOUND')
                ->setStatusNotFound();

            return $responseBuilder->build();
        }
    }

    private function indexAction(Request $request, Response $response, callable $out = null)
    {
        return TwigHelper::getInstance()->htmlResponse($response, '@main/account/views/sign-in/index.html.twig');
    }

    private function attemptSignInAction(Request $request, Response $response, callable $out = null)
    {
        $responseBuilder = new HIResponseBuilder($response);

        try {
            $authToken = $this->authService->signIn((new SignInRequest($request))->getParameters());

            $responseBuilder
                ->setJSON([
                    'token' => $authToken->toJSON()
                ])
                ->setStatusSuccess();
        }catch(InvalidPasswordException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotAllowed();
        }catch(AccountNotFoundException $e) {
            $responseBuilder
                ->setError($e)
                ->setStatusNotFound();
        }

        return $responseBuilder->build();
    }
}