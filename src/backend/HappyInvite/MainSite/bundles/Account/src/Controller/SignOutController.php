<?php
namespace HappyInvite\MainSite\Bundles\Account\Controller;

use HappyInvite\Domain\Bundles\Auth\Service\SessionJWTService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class SignOutController implements MiddlewareInterface
{
    /** @var \HappyInvite\Domain\Bundles\Auth\Service\SessionJWTService */
    private $sessionJWTService;

    public function __construct(SessionJWTService $sessionJWTService)
    {
        $this->sessionJWTService = $sessionJWTService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        if($this->sessionJWTService->isAvailable()) {
            $this->sessionJWTService->destroyJWT();
        }

        header('Location: /');
        exit();
    }
}