<?php
namespace HappyInvite\MainSite\Bundles\Account\Pipe;

use HappyInvite\Domain\Bundles\Account\Exceptions\AccountNotFoundException;
use HappyInvite\Domain\Bundles\Auth\Exceptions\JWTTokenFailException;
use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Profile\Exceptions\ProfileNotFoundException;
use HappyInvite\Domain\Bundles\Auth\Service\SessionJWTService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class AuthPipe implements MiddlewareInterface
{
    /** @var AuthToken */
    private $authToken;

    /** @var \HappyInvite\Domain\Bundles\Auth\Service\SessionJWTService */
    private $sessionJWTService;

    public function __construct(AuthToken $authToken, SessionJWTService $sessionJWTService)
    {
        $this->authToken = $authToken;
        $this->sessionJWTService = $sessionJWTService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        try {
            if($this->sessionJWTService->isAvailable()) {
                $this->authToken->auth($this->sessionJWTService->getJWT());
            }
        }catch(AccountNotFoundException | JWTTokenFailException | ProfileNotFoundException $e) {
            $this->sessionJWTService->destroyJWT();

            header('Location: /');
            exit();
        }

        return $out($request, $response);
    }

}