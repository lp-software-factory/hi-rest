<?php
namespace HappyInvite\MainSite\Bundles\Account;

use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class AccountMainSiteBundle extends MainSiteBundle implements DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getEventScripts(): array
    {
        return [
        ];
    }
}