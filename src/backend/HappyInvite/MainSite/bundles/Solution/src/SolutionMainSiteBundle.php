<?php
namespace HappyInvite\MainSite\Bundles\Solution;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class SolutionMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}