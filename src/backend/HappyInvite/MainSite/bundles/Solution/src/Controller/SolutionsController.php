<?php
namespace HappyInvite\MainSite\Bundles\Solution\Controller;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use HappyInvite\Domain\Bundles\Locale\Service\SessionLocaleService;
use HappyInvite\Domain\Bundles\SolutionIndex\Facade\SolutionIndexQueryFacade;
use HappyInvite\Domain\Bundles\SolutionIndex\Query\Source\PublicSource\PublicSource;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class SolutionsController implements MiddlewareInterface
{
    /** @var array */
    private $options;

    /** @var AuthToken */
    private $authToken;

    /** @var FrontendService */
    private $frontendService;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    /** @var SolutionIndexQueryFacade */
    private $solutionIndexQueryFacade;

    public function __construct(
        array $options,
        AuthToken $authToken,
        FrontendService $frontendService,
        SessionLocaleService $sessionLocaleService,
        SolutionIndexQueryFacade $solutionIndexQueryFacade
    ) {
        $this->options = $options;
        $this->authToken = $authToken;
        $this->frontendService = $frontendService;
        $this->sessionLocaleService = $sessionLocaleService;
        $this->solutionIndexQueryFacade = $solutionIndexQueryFacade;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $options = $this->options;
        $source = $request->getAttribute('source', PublicSource::getName());

        $criteria = [
            'cursor' => [
                'limit' => $options['default_limit'],
            ]
        ];

        $this->frontendService
            ->export('solutions_source', $source)
            ->export('solutions_filter', $options['map_filter'])
            ->export('solutions', $this->solutionIndexQueryFacade->query($source, $criteria, [
                SolutionIndexQueryFacade::OPTION_FOR_AS_RESPONSE200 => true,
                SolutionIndexQueryFacade::OPTION_FORMAT_MAP_FILTER => $options['map_filter'],
            ]));

        return TwigHelper::getInstance()->htmlResponse($response, '@main/solution/index/index.html.twig');
    }
}