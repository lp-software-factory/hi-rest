<?php
namespace HappyInvite\MainSite\Bundles\Solution;

return [
    'config.hi.main-site.solution.options' => [
        'default_limit' => 90,
        'map_filter' => [
            'solutions' => '*',
            'essentials' => [
                'designers' => [
                    'id' => 1,
                    'profile_id' => 1,
                ],
                'event_type_groups' => '*',
                'event_types' => '*',
                'invite_card_families' => '*',
                'invite_card_templates' => [
                    'entity' => [
                        'id' => 1,
                        'family_id' => 1,
                        'preview' => '*',
                        'title' => '*',
                        'card_size_id' => 1,
                        'backdrop' => [
                            'id' => 1,
                            'link' => [
                                'url' => 1,
                            ]
                        ],
                        'svg_map' => [
                            'id' => 1,
                            'link' => [
                                'url' => 1,
                            ]
                        ],
                        'color' => [
                            'title' => '*',
                            'hex_code' => 1,
                        ],
                        'photo' => [
                            'enabled' => 1,
                            'placeholder' => [
                                'id' => 1,
                                'link' => [
                                    'url' => 1,
                                ]
                            ],
                            'container' => '*'
                        ]
                    ],
                ],
                'invite_card_sizes' => [
                    'entity' => [
                        'id' => 1,
                        'browser_preview' => '*',
                        'width' => 1,
                        'height' => 1,
                        'position' => 1,
                        'title' => '*',
                    ]
                ],
                'invite_card_styles' => [
                    'entity' => [
                        'id' => 1,
                        'position' => 1,
                        'title' => '*',
                    ]
                ],
                'invite_card_gammas' => [
                    'entity' => [
                        'id' => 1,
                        'position' => 1,
                        'title' => '*',
                    ]
                ],
                'locales' => '*',
                'profiles' => [
                    'entity' => [
                        'id' => 1,
                        'first_name' => 1,
                        'last_name' => 1,
                        'image' => '*',
                    ]
                ]
            ]
        ],
    ]
];