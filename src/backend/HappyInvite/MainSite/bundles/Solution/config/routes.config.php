<?php
namespace HappyInvite\MainSite\Bundles\Solution;

use HappyInvite\MainSite\Bundles\Solution\Controller\SolutionsController;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/solutions[/]',
                'middleware' => SolutionsController::class,
            ],
            [
                'method' => 'get',
                'path' => '/solutions/{source}[/]',
                'middleware' => SolutionsController::class,
            ],
        ]
    ]
];