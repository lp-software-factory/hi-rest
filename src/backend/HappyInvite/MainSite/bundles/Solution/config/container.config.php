<?php
namespace HappyInvite\MainSite\Bundles\Solution;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\MainSite\Bundles\Solution\Controller\SolutionsController;

return [
    SolutionsController::class => object()
        ->constructorParameter('options', get('config.hi.main-site.solution.options')),
];