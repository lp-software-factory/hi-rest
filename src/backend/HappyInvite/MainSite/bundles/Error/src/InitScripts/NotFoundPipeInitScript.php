<?php
namespace HappyInvite\MainSite\Bundles\Error\InitScripts;

use HappyInvite\MainSite\Bundles\Error\Controller\Error404Controller;
use HappyInvite\Platform\Bundles\ZE\Init\InitScript;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Application;

final class NotFoundPipeInitScript implements InitScript
{
    public function __invoke(Application $application, ContainerInterface $container)
    {
        $application->pipe('/', Error404Controller::class);
    }
}