<?php
namespace HappyInvite\MainSite\Bundles\Error;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class ErrorMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}