<?php
namespace HappyInvite\MainSite\Bundles\EventEditor;

use HappyInvite\MainSite\Bundles\EventEditor\Controller\EventEditorController;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/event/{eventSID}/editor[/]',
                'middleware' => EventEditorController::class,
            ],
        ]
    ]
];