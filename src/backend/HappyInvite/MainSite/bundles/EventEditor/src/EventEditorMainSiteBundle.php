<?php
namespace HappyInvite\MainSite\Bundles\EventEditor;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class EventEditorMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}