<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Resources;

use Cocur\Chain\Chain;
use DI\Container;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources\EventListResource;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources\EventRecipientsGroupsResource;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources\EventResource;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources\InviteCardFamiliesResource;

final class EventEditorResources
{
    /** @var Container */
    private $container;

    /** @var FrontendService */
    private $frontendService;

    public function __construct(Container $container, FrontendService $frontendService)
    {
        $this->container = $container;
        $this->frontendService = $frontendService;
    }

    public function export(Event $event): void
    {
        Chain::create([
            EventResource::class,
            EventListResource::class,
            EventRecipientsGroupsResource::class,
            InviteCardFamiliesResource::class,
        ])
            ->map(function(string $className): EventEditorResource {
                return $this->container->get($className);
            })
            ->map(function (EventEditorResource $resource) use ($event) {
                $this->frontendService->export($resource->getKey(), $resource->fetch($event));
            });
    }
}