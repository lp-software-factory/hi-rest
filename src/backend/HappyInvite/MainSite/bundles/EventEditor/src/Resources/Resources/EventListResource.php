<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Formatter\EventFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\EventEditorResource;

final class EventListResource implements EventEditorResource
{
    /** @var AuthToken */
    private $authToken;

    /** @var EventService */
    private $eventService;

    /** @var EventFormatter */
    private $eventFormatter;

    public function __construct(AuthToken $authToken, EventService $eventService, EventFormatter $eventFormatter)
    {
        $this->authToken = $authToken;
        $this->eventService = $eventService;
        $this->eventFormatter = $eventFormatter;
    }

    public function getKey(): string
    {
        return 'events';
    }

    public function fetch(Event $event): array
    {
        return $this->eventFormatter->formatList(
            $this->eventService->getByOwner($this->authToken->getProfile())
        );
    }
}