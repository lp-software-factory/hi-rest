<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\EventEditorResource;
use HappyInvite\REST\Bundles\InviteCard\Bundles\Family\Formatter\InviteCardFamilyFormatter;

final class InviteCardFamiliesResource implements EventEditorResource
{
    /** @var InviteCardFamilyFormatter */
    private $inviteCardFamilyFormatter;

    public function __construct(InviteCardFamilyFormatter $inviteCardFamilyFormatter)
    {
        $this->inviteCardFamilyFormatter = $inviteCardFamilyFormatter;
    }

    public function getKey(): string
    {
        return 'family';
    }

    public function fetch(Event $event): array
    {
        $originalTemplate = $event->getOriginalSolution()->getInviteCardTemplates()[0]; /** @var InviteCardTemplate $originalTemplate */
        $currentTemplate = $event->getCurrentSolution()->getInviteCardTemplates()[0]; /** @var InviteCardTemplate $currentTemplate */

        return [
            'original' => $this->inviteCardFamilyFormatter->formatOne($originalTemplate->getFamily()),
            'current' => $this->inviteCardFamilyFormatter->formatOne($currentTemplate->getFamily()),
        ];
    }
}