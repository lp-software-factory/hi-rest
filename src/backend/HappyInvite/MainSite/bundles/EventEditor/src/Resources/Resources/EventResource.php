<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Formatter\EventFormatter;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\EventEditorResource;

final class EventResource implements EventEditorResource
{
    /** @var EventFormatter */
    private $eventFormatter;

    public function __construct(EventFormatter $eventFormatter)
    {
        $this->eventFormatter = $eventFormatter;
    }

    public function getKey(): string
    {
        return 'event';
    }

    public function fetch(Event $event): array
    {
        return $this->eventFormatter->formatOne($event);
    }
}