<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Resources;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;

interface EventEditorResource
{
    public function getKey(): string;
    public function fetch(Event $event): array ;
}