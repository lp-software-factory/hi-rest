<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Resources\Resources;

use HappyInvite\Domain\Bundles\Auth\Inject\AuthToken;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Entity\Event;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Formatter\EventRecipientsGroupFormatter;
use HappyInvite\Domain\Bundles\Event\Bundles\EventRecipientsGroup\Service\EventRecipientsGroupService;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\EventEditorResource;

final class EventRecipientsGroupsResource implements EventEditorResource
{
    /** @var AuthToken */
    private $authToken;

    /** @var EventRecipientsGroupService */
    private $eventRecipientsGroupService;

    /** @var EventRecipientsGroupFormatter */
    private $eventRecipientsGroupFormatter;

    public function __construct(
        AuthToken $authToken,
        EventRecipientsGroupService $eventRecipientsGroupService,
        EventRecipientsGroupFormatter $eventRecipientsGroupFormatter
    ) {
        $this->authToken = $authToken;
        $this->eventRecipientsGroupService = $eventRecipientsGroupService;
        $this->eventRecipientsGroupFormatter = $eventRecipientsGroupFormatter;
    }


    public function getKey(): string
    {
        return 'event_recipients_groups';
    }

    public function fetch(Event $event): array
    {
        return $this->eventRecipientsGroupFormatter->formatMany(
            $this->eventRecipientsGroupService->getAllByOwner($this->authToken->getProfile())
        );
    }
}