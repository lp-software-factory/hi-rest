<?php
namespace HappyInvite\MainSite\Bundles\EventEditor\Controller;

use HappyInvite\Domain\Bundles\Event\Bundles\Event\Exceptions\EventNotFoundException;
use HappyInvite\Domain\Bundles\Event\Bundles\Event\Service\EventService;
use HappyInvite\MainSite\Bundles\EventEditor\Resources\EventEditorResources;
use HappyInvite\MainSite\Bundles\SolutionEditor\Service\SolutionEditorResources\SolutionEditorResourcesService;
use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use HappyInvite\MainSite\Service\RedirectService;
use HappyInvite\REST\Bundles\Access\Exceptions\AccessDenied\AccessDeniedException;
use HappyInvite\REST\Bundles\Access\Service\AccessService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class EventEditorController implements MiddlewareInterface
{
    /** @var RedirectService */
    private $redirectService;

    /** @var EventEditorResources */
    private $resources;

    /** @var EventService */
    private $eventService;


    /** @var SolutionEditorResourcesService */
    private $solutionEditorResourcesService;

    /** @var AccessService */
    private $accessService;

    public function __construct(
        RedirectService $redirectService,
        EventEditorResources $resources,
        EventService $eventService,
        SolutionEditorResourcesService $solutionEditorResourcesService,
        AccessService $accessService
    ) {
        $this->redirectService = $redirectService;
        $this->resources = $resources;
        $this->eventService = $eventService;
        $this->solutionEditorResourcesService = $solutionEditorResourcesService;
        $this->accessService = $accessService;
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        try {
            if(! $this->accessService->isAuthenticated()) {
                $this->redirectService->redirectByUrl('/account/sign-in');
            }

            $event = $this->eventService->getBySID($request->getAttribute('eventSID'));

            if(! $this->accessService->isAdmin()) {
                $this->accessService->requireOwner($event->getOwner()->getId());
            }

            $this->resources->export($event);
            $this->solutionEditorResourcesService->export();

            return TwigHelper::getInstance()->htmlResponse($response, '@main/event-editor/views/index.html.twig');
        }catch(AccessDeniedException $e) {
            return TwigHelper::getInstance()->html404Response($response);
        }catch(EventNotFoundException $e) {
            return TwigHelper::getInstance()->html404Response($response);
        }
    }
}