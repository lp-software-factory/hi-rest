<?php
namespace HappyInvite\MainSite\Bundles\Designer;

use HappyInvite\MainSite\Bundles\Designer\Controller\LandingController;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/designer[/]',
                'middleware' => LandingController::class,
            ],
        ]
    ]
];