<?php
namespace HappyInvite\MainSite\Bundles\Designer\Controller;

use HappyInvite\MainSite\Bundles\Twig\Service\TwigHelper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Stratigility\MiddlewareInterface;

final class LandingController implements MiddlewareInterface
{
    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        return TwigHelper::getInstance()->htmlResponse($response, '@main/designer/landing/index.html.twig');
    }
}