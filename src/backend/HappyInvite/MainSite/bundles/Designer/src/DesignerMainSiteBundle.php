<?php
namespace HappyInvite\MainSite\Bundles\Designer;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class DesignerMainSiteBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}