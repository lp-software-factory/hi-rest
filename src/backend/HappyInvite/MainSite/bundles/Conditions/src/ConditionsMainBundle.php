<?php
namespace HappyInvite\MainSite\Bundles\Conditions;

use HappyInvite\MainSite\Bundle\MainSiteBundle;

final class ConditionsMainBundle extends MainSiteBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}