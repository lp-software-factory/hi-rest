<?php
namespace HappyInvite\MainSite\Bundles\Conditions;

use HappyInvite\MainSite\Bundles\Conditions\Controller\ConditionsController;

return [
    'router' => [
        'routes' => [
            [
                'method' => 'get',
                'path' => '/conditions[/]',
                'middleware' => ConditionsController::class,
            ],
        ],
    ],
];