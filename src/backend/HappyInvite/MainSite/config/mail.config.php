<?php
namespace HappyInvite\Domain\Bundles\Mail;

return [
    'config.hi.main-site.mail' => [
        'dir' => __DIR__.'/../../../../frontend/src/mail',
        'host' => 'example.com',
        'no-reply' => [
            'from' => 'no-reply@example.com',
            'name' => 'HappyInvite No Reply',
        ]
    ]
];