<?php
namespace HappyInvite\MainSite;

use function DI\get;
use function DI\object;
use function DI\factory;

use DI\Container;
use HappyInvite\Domain\Bundles\Mail\Config\MailConfig;
use HappyInvite\MainSite\APIDocs\MainSiteAPIDocsGenerator;
use HappyInvite\Platform\Bundles\APIDocs\Generator\APIDocsGeneratorInterface;

return [
    APIDocsGeneratorInterface::class => get(MainSiteAPIDocsGenerator::class),
    MailConfig::class => object()->constructorParameter('config', get('config.hi.main-site.mail')),
];