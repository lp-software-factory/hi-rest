<?php
namespace HappyInvite\MainSite\Subscriptions;

use HappyInvite\Domain\Bundles\Frontend\Service\FrontendService;
use HappyInvite\Domain\Bundles\Locale\Service\SessionLocaleService;
use HappyInvite\Domain\Bundles\Translation\Formatter\TranslationFormatter;
use HappyInvite\Domain\Bundles\Translation\Repository\TranslationProjectRepository;
use HappyInvite\Domain\Bundles\Translation\Service\TranslationService;
use HappyInvite\Domain\Events\DomainEventEmitter;
use HappyInvite\Domain\Events\SubscriptionScript;

final class ExportTranslationSubscriptionScript implements SubscriptionScript
{
    /** @var FrontendService */
    private $frontendService;

    /** @var TranslationService */
    private $translationService;

    /** @var TranslationFormatter */
    private $translationFormatter;

    /** @var SessionLocaleService */
    private $sessionLocaleService;

    public function __construct(
        FrontendService $frontendService,
        TranslationService $translationService,
        TranslationFormatter $translationFormatter,
        SessionLocaleService $sessionLocaleService
    ) {
        $this->frontendService = $frontendService;
        $this->translationService = $translationService;
        $this->translationFormatter = $translationFormatter;
        $this->sessionLocaleService = $sessionLocaleService;
    }

    public function __invoke(DomainEventEmitter $eventEmitter)
    {
        $region = $this->sessionLocaleService->getCurrentRegion();
        $resources = $this->translationService->listTranslationsByRegion(TranslationProjectRepository::PROJECT_ID_HAPPY_INVITE_MAIN_FRONTEND, $region);

        $this->frontendService->export('translations', [
            'region' => $region,
            'fallback' => 'en_US',
            'resources' => $this->translationFormatter->formatManyRegion($resources)
        ]);
    }
}