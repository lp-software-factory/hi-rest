<?php
namespace HappyInvite\MainSite\Service;

final class RedirectService
{
    public function redirectByUrl(string $url): void {
        header('Location: '.$url);
        exit();
    }
}