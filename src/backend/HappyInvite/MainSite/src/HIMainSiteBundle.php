<?php
namespace HappyInvite\MainSite;

use HappyInvite\Domain\Events\DomainEventBundle;
use HappyInvite\MainSite\Bundles\Account\AccountMainSiteBundle;
use HappyInvite\MainSite\Bundles\Designer\DesignerMainSiteBundle;
use HappyInvite\MainSite\Bundles\Error\ErrorMainSiteBundle;
use HappyInvite\MainSite\Bundles\Error\InitScripts\NotFoundPipeInitScript;
use HappyInvite\MainSite\Bundles\EventEditor\EventEditorMainSiteBundle;
use HappyInvite\MainSite\Bundles\EventLanding\EventLandingMainSiteBundle;
use HappyInvite\MainSite\Bundles\Landing\IndexMainSiteBundle;
use HappyInvite\MainSite\Bundles\Organizer\OrganizerMainSiteBundle;
use HappyInvite\MainSite\Bundles\Solution\SolutionMainSiteBundle;
use HappyInvite\MainSite\Bundles\SolutionEditor\SolutionEditorMainSiteBundle;
use HappyInvite\MainSite\Bundles\Twig\TwigMainSiteBundle;
use HappyInvite\MainSite\Bundles\Conditions\ConditionsMainBundle;
use HappyInvite\MainSite\Subscriptions\ExportTranslationSubscriptionScript;
use HappyInvite\Platform\Bundles\HIBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScriptInjectableBundle;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\ErrorHandlerPipeInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\InjectSchemaServiceInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\PipeMiddlewaresInitScript;
use HappyInvite\Platform\Bundles\ZE\Init\InitScripts\RoutesInitScript;

final class HIMainSiteBundle extends HIBundle implements InitScriptInjectableBundle, DomainEventBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }

    public function getBundles(): array
    {
        return [
            new TwigMainSiteBundle(),
            new IndexMainSiteBundle(),
            new SolutionMainSiteBundle(),
            new AccountMainSiteBundle(),
            new ErrorMainSiteBundle(),
            new DesignerMainSiteBundle(),
            new OrganizerMainSiteBundle(),
            new ConditionsMainBundle(),
            new EventEditorMainSiteBundle(),
            new SolutionEditorMainSiteBundle(),
            new EventLandingMainSiteBundle(),
        ];
    }

    public function getInitScripts(): array
    {
        return [
            new ErrorHandlerPipeInitScript(),
            new RoutesInitScript([
                'auth',
                'locale',
                'pipes',
                'default',
                'final',
            ]),
            new InjectSchemaServiceInitScript(),
            new PipeMiddlewaresInitScript(),
            new NotFoundPipeInitScript(),
        ];
    }

    public function getEventScripts(): array
    {
        return [
            ExportTranslationSubscriptionScript::class,
        ];
    }
}