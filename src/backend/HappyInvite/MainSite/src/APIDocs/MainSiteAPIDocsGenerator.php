<?php
namespace HappyInvite\MainSite\APIDocs;

use Cocur\Chain\Chain;
use HappyInvite\Platform\Bundles\APIDocs\APIBuilder\LocalSwaggerAPIBuilder;
use HappyInvite\Platform\Bundles\APIDocs\Bundle\APIDocsBundle;
use HappyInvite\Platform\Bundles\APIDocs\Generator\APIDocsGeneratorInterface;
use HappyInvite\Platform\Bundles\Bundle;
use HappyInvite\REST\HIRESTBundle;

final class MainSiteAPIDocsGenerator implements APIDocsGeneratorInterface
{
    public function generateAPIDocs(): array
    {
        $bundles = [];

        $this->flattenBundle(new HIRESTBundle(), $bundles);

        $sources = Chain::create($bundles)
            ->filter(function(Bundle $bundle) {
                return $bundle instanceof APIDocsBundle;
            })
            ->filter(function(APIDocsBundle $bundle) {
                return $bundle->hasAPIDocs();
            })
            ->map(function(APIDocsBundle $bundle) {
                return $bundle->getAPIDocsDir();
            })
            ->array;

        return (new LocalSwaggerAPIBuilder($sources))->build();
    }

    private function flattenBundle(Bundle $bundle, array &$result)
    {
        $result[] = $bundle;

        if($bundle->hasBundles()) {
            foreach($bundle->getBundles() as $sub) {
                $this->flattenBundle($sub, $result);
            }
        }
    }
}