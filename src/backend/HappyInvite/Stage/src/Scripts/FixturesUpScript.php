<?php
namespace HappyInvite\Stage\Scripts;

use HappyInvite\Domain\Bundles\Mail\Service\MailService;
use HappyInvite\Stage\Fixtures\DressCodeStageFixture\DressCodeStageFixture;
use HappyInvite\Stage\Fixtures\EnvelopeBackdropStageFixture\EnvelopeBackdropStageFixture;
use HappyInvite\Stage\Fixtures\EnvelopeColorStageFixture\EnvelopeColorStageFixture;
use HappyInvite\Stage\Fixtures\EnvelopeMarksStageFixture\EnvelopeMarksStageFixture;
use HappyInvite\Stage\Fixtures\EnvelopePatternStageFixture\EnvelopePatternStageFixture;
use HappyInvite\Stage\Fixtures\AdminStageFixture\AdminStageFixture;
use HappyInvite\Stage\Fixtures\CurrencyStageFixture\CurrencyStageFixture;
use HappyInvite\Stage\Fixtures\EnvelopeTextureStageFixture\EnvelopeTextureStageFixture;
use HappyInvite\Stage\Fixtures\EventTypesStageFixture\EventTypesStageFixture;
use HappyInvite\Stage\Fixtures\FontStageFixture\FontStageFixture;
use HappyInvite\Stage\Fixtures\InviteCardGammaStageFixture\InviteCardGammaStageFixture;
use HappyInvite\Stage\Fixtures\InviteCardSizeStageFixture\InviteCardSizeStageFixture;
use HappyInvite\Stage\Fixtures\InviteCardStyleStageFixture\InviteCardStyleStageFixture;
use HappyInvite\Stage\Fixtures\LocalesStageFixture\LocalesStageFixture;
use HappyInvite\Stage\Fixtures\OAuth2StageFixture\OAuth2StageFixture;
use HappyInvite\Stage\Fixtures\SolutionStageFixture\SolutionStageFixture;
use HappyInvite\Stage\Fixtures\StageFixture;
use Interop\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class FixturesUpScript
{
    /** @var MailService */
    private $mailService;

    /** @var ContainerInterface */
    private $container;

    public function __construct(MailService $mailService, ContainerInterface $container)
    {
        $this->mailService = $mailService;
        $this->container = $container;
    }

    public function up(OutputInterface $output, InputInterface $input)
    {
        $this->mailService->disable();

        array_map(function(StageFixture $fixture) use ($output, $input) {
            $output->writeln(sprintf('[*] Up %s fixture', get_class($fixture)));

            $fixture->up($output, $input);
        }, $this->getFixtures());

        $output->writeln('');
        $output->writeln('Done!');
    }

    private function getFixtures(): array
    {
        return array_map(function(string $fixture) {
            return $this->container->get($fixture);
        }, [
            LocalesStageFixture::class,
            CurrencyStageFixture::class,
            AdminStageFixture::class,
            FontStageFixture::class,
            OAuth2StageFixture::class,
            DressCodeStageFixture::class,
            EventTypesStageFixture::class,
            InviteCardSizeStageFixture::class,
            InviteCardGammaStageFixture::class,
            InviteCardStyleStageFixture::class,
            EnvelopeColorStageFixture::class,
            EnvelopeMarksStageFixture::class,
            EnvelopeTextureStageFixture::class,
            EnvelopeBackdropStageFixture::class,
            EnvelopePatternStageFixture::class,
            SolutionStageFixture::class,
        ]);
    }
}