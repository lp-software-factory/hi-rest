<?php
namespace HappyInvite\Stage\Fixtures\InviteCardStyleStageFixture;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Parameters\CreateInviteCardStyleParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class InviteCardStyleStageFixture implements StageFixture
{
    /** @var InviteCardStyleService */
    private $inviteCardStyleService;

    public function __construct(InviteCardStyleService $inviteCardStyleService)
    {
        $this->inviteCardStyleService = $inviteCardStyleService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(CreateInviteCardStyleParameters $parameters) use ($output) {
            $output->writeln(sprintf('  - %s', $parameters->getTitle()->getTranslation('en_US')));

            $this->inviteCardStyleService->createStyle($parameters);
        }, $this->getInviteCardStyles());
    }

    private function getInviteCardStyles(): array
    {
        return [
            new CreateInviteCardStyleParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Классический'],
                    ['region' => 'en_US', 'value' => 'Classical'],
                    ['region' => 'en_GB', 'value' => 'Classical'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Классический'],
                    ['region' => 'en_US', 'value' => 'Classical'],
                    ['region' => 'en_GB', 'value' => 'Classical'],
                ])
            ),
            new CreateInviteCardStyleParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Винтажный'],
                    ['region' => 'en_US', 'value' => 'Vintage'],
                    ['region' => 'en_GB', 'value' => 'Vintage'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Винтажный'],
                    ['region' => 'en_US', 'value' => 'Vintage'],
                    ['region' => 'en_GB', 'value' => 'Vintage'],
                ])
            ),
            new CreateInviteCardStyleParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Минимализм'],
                    ['region' => 'en_US', 'value' => 'Minimalism'],
                    ['region' => 'en_GB', 'value' => 'Minimalism'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Минимализм'],
                    ['region' => 'en_US', 'value' => 'Minimalism'],
                    ['region' => 'en_GB', 'value' => 'Minimalism'],
                ])
            ),
            new CreateInviteCardStyleParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Цветочный'],
                    ['region' => 'en_US', 'value' => 'Floral'],
                    ['region' => 'en_GB', 'value' => 'Floral'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Цветочный'],
                    ['region' => 'en_US', 'value' => 'Floral'],
                    ['region' => 'en_GB', 'value' => 'Floral'],
                ])
            ),
            new CreateInviteCardStyleParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Акварельный'],
                    ['region' => 'en_US', 'value' => 'Watercolor'],
                    ['region' => 'en_GB', 'value' => 'Watercolor'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Акварельный'],
                    ['region' => 'en_US', 'value' => 'Watercolor'],
                    ['region' => 'en_GB', 'value' => 'Watercolor'],
                ])
            ),
            new CreateInviteCardStyleParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Морской'],
                    ['region' => 'en_US', 'value' => 'Nautical'],
                    ['region' => 'en_GB', 'value' => 'Nautical'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Морской'],
                    ['region' => 'en_US', 'value' => 'Nautical'],
                    ['region' => 'en_GB', 'value' => 'Nautical'],
                ])
            ),
        ];
    }
}