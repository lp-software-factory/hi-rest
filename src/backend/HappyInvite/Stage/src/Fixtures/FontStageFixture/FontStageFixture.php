<?php
namespace HappyInvite\Stage\Fixtures\FontStageFixture;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Fonts\Entity\Font;
use HappyInvite\Domain\Bundles\Fonts\Parameters\FontParameters;
use HappyInvite\Domain\Bundles\Fonts\Service\FontService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class FontStageFixture implements StageFixture
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var FontService */
    private $fontService;

    public function __construct(AttachmentService $attachmentService, FontService $fontService)
    {
        $this->attachmentService = $attachmentService;
        $this->fontService = $fontService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        foreach($this->getFontDefinitions() as $definition) {
            $output->writeln(sprintf('  - %s', $definition['title']));

            $files = array_map(function(string $input) {
                $path = __DIR__.'/resources/fonts/'.$input;

                $attachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($path, filesize($path), 0))->getStream()->getMetadata('uri'),
                    basename($input)
                );

                return $attachment->getId();
            }, $definition['files']);

            $preview = $this->attachmentService->uploadAttachment(
                (new UploadedFile(($previewPath = __DIR__.'/resources/preview/'.$definition['preview']), filesize($previewPath), 0))->getStream()->getMetadata('uri'),
                basename($definition['preview'])
            );

            $parameters = new FontParameters(
                $preview,
                $definition['title'],
                $definition['supports'],
                file_get_contents(__DIR__.'/resources/fonts/'.$definition['font_face']),
                $definition['license'],
                $files,
                true
            );

            $this->fontService->createFont($parameters);
        }
    }

    private function getFontDefinitions(): array
    {
        return [
            [
                'title' => 'Open Sans',
                'preview' => 'open_sans.png',
                'supports' => Font::MODE_ALL,
                'license' => './Open_Sans/LICENSE.txt',
                'font_face' => './Open_Sans/font-face.css',
                'files' => [
                    './Open_Sans/OpenSans-Bold.ttf',
                    './Open_Sans/OpenSans-BoldItalic.ttf',
                    './Open_Sans/OpenSans-Italic.ttf',
                    './Open_Sans/OpenSans-Regular.ttf',
                ]
            ],
            [
                'title' => 'PT Sans',
                'preview' => 'pt_sans.png',
                'supports' => Font::MODE_ALL,
                'license' => './PT_Sans/OFL.txt',
                'font_face' => './PT_Sans/font-face.css',
                'files' => [
                    './PT_Sans/PT_Sans-Web-Bold.ttf',
                    './PT_Sans/PT_Sans-Web-BoldItalic.ttf',
                    './PT_Sans/PT_Sans-Web-Italic.ttf',
                    './PT_Sans/PT_Sans-Web-Regular.ttf',
                ]
            ],
            [
                'title' => 'PT Serif',
                'preview' => 'pt_serif.png',
                'supports' => Font::MODE_ALL,
                'license' => './PT_Serif/OFL.txt',
                'font_face' => './PT_Serif/font-face.css',
                'files' => [
                    './PT_Serif/PT_Serif-Web-Bold.ttf',
                    './PT_Serif/PT_Serif-Web-BoldItalic.ttf',
                    './PT_Serif/PT_Serif-Web-Italic.ttf',
                    './PT_Serif/PT_Serif-Web-Regular.ttf',
                ]
            ],
            [
                'title' => 'Exo 2',
                'preview' => 'exo_2.png',
                'supports' => Font::MODE_ALL,
                'license' => './Exo_2/OFL.txt',
                'font_face' => './Exo_2/font-face.css',
                'files' => [
                    './Exo_2/Exo2-Bold.ttf',
                    './Exo_2/Exo2-BoldItalic.ttf',
                    './Exo_2/Exo2-Italic.ttf',
                    './Exo_2/Exo2-Regular.ttf',
                ]
            ],
            [
                'title' => 'Lora',
                'preview' => 'lora_.png',
                'supports' => Font::MODE_ALL,
                'license' => './Lora/OFL.txt',
                'font_face' => './Lora/font-face.css',
                   'files' => [
                    './Lora/Lora-Bold.ttf',
                    './Lora/Lora-BoldItalic.ttf',
                    './Lora/Lora-Italic.ttf',
                    './Lora/Lora-Regular.ttf',
                ]
            ],
            [
                'title' => 'Philosopher',
                'preview' => 'philosopher_.png',
                'supports' => Font::MODE_ALL,
                'license' => './Philosopher/OFL.txt',
                'font_face' => './Philosopher/font-face.css',
                'files' => [
                    './Philosopher/Philosopher-Bold.ttf',
                    './Philosopher/Philosopher-BoldItalic.ttf',
                    './Philosopher/Philosopher-Italic.ttf',
                    './Philosopher/Philosopher-Regular.ttf',
                ]
            ],
            [
                'title' => 'Comfortaa',
                'preview' => 'comfortaa_.png',
                'supports' => Font::MODE_NORMAL | Font::MODE_BOLD,
                'license' => './Comfortaa/OFL.txt',
                'font_face' => './Comfortaa/font-face.css',
                'files' => [
                    './Comfortaa/Comfortaa-Bold.ttf',
                    './Comfortaa/Comfortaa-Regular.ttf',
                ]
            ],
            [
                'title' => 'Marck Script',
                'preview' => 'marck_script.png',
                'supports' => Font::MODE_NORMAL,
                'license' => './Marck_Script/OFL.txt',
                'font_face' => './Marck_Script/font-face.css',
                'files' => [
                    './Marck_Script/MarckScript-Regular.ttf',
                ]
            ],
            [
                'title' => 'Nautilus',
                'preview' => 'nautilus_.png',
                'supports' => Font::MODE_NORMAL,
                'license' => './Nautilus/OFL.txt',
                'font_face' => './Nautilus/font-face.css',
                'files' => [
                    './Nautilus/Nautilus.otf',
                ]
            ],
            [
                'title' => 'Adine Kirnberg',
                'preview' => 'adine_kirnberg.png',
                'supports' => Font::MODE_NORMAL,
                'license' => './Adine_Kirnberg/OFL.txt',
                'font_face' => './Adine_Kirnberg/font-face.css',
                'files' => [
                    './Adine_Kirnberg/adine-kirnberg.ttf',
                ]
            ],
            [
                'title' => 'Anastasia Script',
                'preview' => 'anastasia_script.png',
                'supports' => Font::MODE_NORMAL,
                'license' => './Anastasia_Script/OFL.txt',
                'font_face' => './Anastasia_Script/font-face.css',
                'files' => [
                    './Anastasia_Script/anastasiascript.ttf',
                ]
            ],
            [
                'title' => 'Teddy Bear',
                'preview' => 'teddy_bear.png',
                'supports' => Font::MODE_NORMAL,
                'license' => './Teddy_Bear/OFL.txt',
                'font_face' => './Teddy_Bear/font-face.css',
                'files' => [
                    './Teddy_Bear/teddy-bear.ttf',
                ]
            ],
            [
                'title' => 'Vizit',
                'preview' => 'vizit_.png',
                'supports' => Font::MODE_NORMAL,
                'license' => './Vizit/OFL.txt',
                'font_face' => './Vizit/font-face.css',
                'files' => [
                    './Vizit/vizit.ttf',
                ]
            ],
        ];
    }
}