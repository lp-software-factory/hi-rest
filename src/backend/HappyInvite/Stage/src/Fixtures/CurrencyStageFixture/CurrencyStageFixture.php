<?php
namespace HappyInvite\Stage\Fixtures\CurrencyStageFixture;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Money\Entity\Currency;
use HappyInvite\Domain\Bundles\Money\Parameters\CreateCurrencyParameters;
use HappyInvite\Domain\Bundles\Money\Service\CurrencyService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CurrencyStageFixture implements StageFixture
{
    /** @var CurrencyService */
    private $currencyService;

    /** @var Currency[] */
    public static $currencies = [];

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(CreateCurrencyParameters $parameters) use ($output) {
            $output->writeln(sprintf('  - %s', $parameters->getCode()));

            self::$currencies[$parameters->getCode()] = $this->currencyService->createCurrency($parameters);
        }, $this->getCurrencies());
    }

    private function getCurrencies(): array
    {
        return [
            new CreateCurrencyParameters(
                'USD', '$', new ImmutableLocalizedString([
                    ['region' => 'en_US', 'value' => 'USD Currency']
                ])
            ),
            new CreateCurrencyParameters(
                'RUB', '₽', new ImmutableLocalizedString([
                    ['region' => 'en_US', 'value' => 'RUB Currency']
                ])
            )
        ];
    }
}