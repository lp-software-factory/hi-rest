<?php
namespace HappyInvite\Stage\Fixtures\EnvelopeBackdropStageFixture;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdropDefinition;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Entity\EnvelopeBackdrop;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\CreateEnvelopeBackdropParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Parameters\Definition\CreateEnvelopeBackdropDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeBackdrop\Service\EnvelopeBackdropService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Intervention\Image\ImageManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class EnvelopeBackdropStageFixture implements StageFixture
{
    /** @var EnvelopeBackdrop[] */
    public static $backdrops = [];

    /** @var ImageManager */
    private $imageManager;

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeBackdropService */
    private $envelopeBackdropService;

    /** @var EnvelopeBackdropDefinitionService */
    protected $envelopeBackdropDefinitionService;

    public function __construct(
        ImageManager $imageManager,
        AttachmentService $attachmentService,
        EnvelopeBackdropService $envelopeBackdropService,
        EnvelopeBackdropDefinitionService $envelopeBackdropDefinitionService
    ) {
        $this->imageManager = $imageManager;
        $this->attachmentService = $attachmentService;
        $this->envelopeBackdropService = $envelopeBackdropService;
        $this->envelopeBackdropDefinitionService = $envelopeBackdropDefinitionService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        foreach($this->getEnvelopeBackdrops() as $source) {
            $output->writeln(sprintf(' -- %s', $source['preview']));

            $base = basename($source['preview']);
            $backdrop = __DIR__.'/resources/textures/'.$base;
            $preview = __DIR__.'/resources/preview/'.$base;

            if(! file_exists($preview)) {
                $this->generatePreview($backdrop, $preview);
            }

            $previewAttachment = $this->attachmentService->uploadAttachment(
                (new UploadedFile($preview, filesize($preview), 0))->getStream()->getMetadata('uri'),
                $base
            );

            $parameters = new CreateEnvelopeBackdropParameters(
                $previewAttachment,
                new ImmutableLocalizedString($source['title']),
                new ImmutableLocalizedString($source['description'])
            );

            $backdrop = $this->envelopeBackdropService->createEnvelopeBackdrop($parameters);

            foreach($source['definitions'] as $definition) {
                $output->writeln(sprintf(' ---- %s', $definition['file']));

                $base = basename($definition['file']);
                $resource = __DIR__.'/resources/textures/'.$base;
                $preview = __DIR__.'/resources/preview/'.$base;

                if(! file_exists($preview)) {
                    $this->generatePreview($resource, $preview);
                }

                $backdropAttachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($resource, filesize($resource), 0))->getStream()->getMetadata('uri'),
                    $base
                );

                $previewAttachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($preview, filesize($preview), 0))->getStream()->getMetadata('uri'),
                    $base
                );

                $this->envelopeBackdropDefinitionService->createEnvelopeBackdropDefinition(new CreateEnvelopeBackdropDefinitionParameters(
                    $backdrop,
                    new ImmutableLocalizedString($definition['title']),
                    new ImmutableLocalizedString($definition['description']),
                    $previewAttachment,
                    $backdropAttachment
                ));
            }

            self::$backdrops[] = $backdrop;
        }
    }

    private function getEnvelopeBackdrops(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/source.json'), true);
    }

    private function generatePreview(string $path, string $newPath): void
    {
        $image = $this->imageManager->make($path);
        $image->crop(EnvelopeBackdrop::PREVIEW_WIDTH, EnvelopeBackdrop::PREVIEW_HEIGHT);
        $image->save($newPath);
    }
}