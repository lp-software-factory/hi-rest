<?php
namespace HappyInvite\Stage\Fixtures\OAuth2StageFixture;

use HappyInvite\Domain\Bundles\OAuth2\Parameters\CreateOAuth2ProviderParameters;
use HappyInvite\Domain\Bundles\OAuth2\Service\OAuth2ProviderService;
use HappyInvite\Stage\Fixtures\StageFixture;
use League\OAuth2\Client\Provider\Google;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class OAuth2StageFixture implements StageFixture
{
    /** @var OAuth2ProviderService */
    private $oauth2ProviderService;

    public function __construct(OAuth2ProviderService $oauth2ProviderService)
    {
        $this->oauth2ProviderService = $oauth2ProviderService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        $this->oauth2ProviderService->createOAuth2Provider(new CreateOAuth2ProviderParameters(
            'google',
            Google::class,
            [
                'clientId' => '42595752055-pc1tvd6k753ppi219d0iacg41f3958it.apps.googleusercontent.com',
                'clientSecret' => 'fnCZFW6A4xEwCqr-m1I26ayG',
                'redirectUri' => 'http://127.0.0.1:8080/backend/api/auth/oauth2/google/',
            ]
        ));
    }
}