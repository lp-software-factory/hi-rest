<?php
namespace HappyInvite\Stage\Fixtures\EnvelopeTextureStageFixture;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Entity\EnvelopeTexture;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\CreateEnvelopeTextureParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Parameters\Definition\CreateEnvelopeTextureDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTexture\Service\EnvelopeTextureService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Intervention\Image\ImageManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class EnvelopeTextureStageFixture implements StageFixture
{
    /** @var EnvelopeTexture[] */
    public static $textures = [];

    /** @var ImageManager */
    private $imageManager;

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeTextureService */
    private $envelopeTextureService;

    /** @var EnvelopeTextureDefinitionService */
    private $envelopeTextureDefinitionService;

    public function __construct(
        ImageManager $imageManager,
        AttachmentService $attachmentService,
        EnvelopeTextureService $envelopeTextureService,
        EnvelopeTextureDefinitionService $envelopeTextureDefinitionService
    ) {
        $this->imageManager = $imageManager;
        $this->attachmentService = $attachmentService;
        $this->envelopeTextureService = $envelopeTextureService;
        $this->envelopeTextureDefinitionService = $envelopeTextureDefinitionService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(array $source) use ($output) {
            $output->writeln(sprintf(' -- %s', $source['title'][0]['value']));

            $preview = __DIR__.'/resources/definitions/'.$source['preview'];
            $previewAttachment = $this->attachmentService->uploadAttachment(
                (new UploadedFile($preview, filesize($preview), 0))->getStream()->getMetadata('uri'),
                basename($preview)
            );

            $parameters = new CreateEnvelopeTextureParameters(
                $previewAttachment,
                new ImmutableLocalizedString($source['title']),
                new ImmutableLocalizedString($source['description'])
            );

            $texture = $this->envelopeTextureService->createEnvelopeTexture($parameters);

            foreach($source['definitions'] as $definition) {
                $output->writeln(sprintf(' ---- %s', $definition['preview']));

                $preview = __DIR__.'/resources/definitions/'.$definition['preview'];
                $previewAttachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($preview, filesize($preview), 0))->getStream()->getMetadata('uri'),
                    basename($preview)
                );
                $this->envelopeTextureDefinitionService->createEnvelopeTextureDefinition(new CreateEnvelopeTextureDefinitionParameters(
                    $texture,
                    new ImmutableLocalizedString($definition['title']),
                    $previewAttachment,
                    $this->getResources($definition['resources'])
                ));
            }

            self::$textures[] = $texture;
        }, $this->getEnvelopeTextures());
    }

    private function getEnvelopeTextures(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/source.json'), true);
    }

    private function generatePreview(string $path, string $newPath): void
    {
        $image = $this->imageManager->make($path);
        $image->crop(EnvelopeTexture::PREVIEW_WIDTH, EnvelopeTexture::PREVIEW_HEIGHT);
        $image->save($newPath);
    }

    private function getResources(array $resources): array
    {
        $results = [];

        foreach($resources as $resource) {
            $attachmentIds = [];

            foreach($resource['attachments'] as $key => $file) {
                $file = __DIR__.'/resources/definitions/'.$file ;
                $attachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($file, filesize($file), 0))->getStream()->getMetadata('uri'),
                    basename($file)
                );

                $attachmentIds[$key] = $attachment->getId();
            }

            $results[] = [
                'width' => $resource['width'],
                'height' => $resource['height'],
                'stamp' => $resource['stamp'],
                'attachment_ids' => $attachmentIds,
            ];
        }

        return $results;
    }
}