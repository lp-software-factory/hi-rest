<?php
namespace HappyInvite\Stage\Fixtures\InviteCardSizeStageFixture;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Parameters\CreateInviteCardSizeParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class InviteCardSizeStageFixture implements StageFixture
{
    /** @var InviteCardSizeService */
    private $inviteCardSizesService;

    public function __construct(InviteCardSizeService $inviteCardSizesService)
    {
        $this->inviteCardSizesService = $inviteCardSizesService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(CreateInviteCardSizeParameters $parameters) use ($output) {
            $output->writeln(sprintf('  - %s (%dx%d)', $parameters->getTitle()->getTranslation('en_US'), $parameters->getWidth(), $parameters->getHeight()));

            $this->inviteCardSizesService->createCardSize($parameters);
        }, $this->getInviteCardSizes());
    }

    private function getInviteCardSizes(): array
    {
        return [
            new CreateInviteCardSizeParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Книжный'],
                    ['region' => 'en_US', 'value' => 'Portrait'],
                    ['region' => 'en_GB', 'value' => 'Portrait'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Книжный'],
                    ['region' => 'en_US', 'value' => 'Portrait'],
                    ['region' => 'en_GB', 'value' => 'Portrait'],
                ]),
                570,
                810,
                260,
                369
            ),
            new CreateInviteCardSizeParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Альбомный'],
                    ['region' => 'en_US', 'value' => 'Album'],
                    ['region' => 'en_GB', 'value' => 'Album'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Альбомный'],
                    ['region' => 'en_US', 'value' => 'Album'],
                    ['region' => 'en_GB', 'value' => 'Album'],
                ]),
                810,
                570,
                260,
                183
            ),
            new CreateInviteCardSizeParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Квадратный'],
                    ['region' => 'en_US', 'value' => 'Square'],
                    ['region' => 'en_GB', 'value' => 'Square'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Квадратный'],
                    ['region' => 'en_US', 'value' => 'Square'],
                    ['region' => 'en_GB', 'value' => 'Square'],
                ]),
                570,
                570,
                260,
                260
            ),
        ];
    }
}