<?php
namespace HappyInvite\Stage\Fixtures\EnvelopeColorStageFixture;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Parameters\CreateEnvelopeColorParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Service\EnvelopeColorService;
use HappyInvite\Domain\Bundles\Palette\Entity\Palette;
use HappyInvite\Domain\Bundles\Palette\Repository\PaletteRepository;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EnvelopeColorStageFixture implements StageFixture
{
    /** @var EnvelopeColor[] */
    public static $colors = [];

    /** @var PaletteRepository */
    private $palettesRepository;

    /** @var EnvelopeColorService */
    private $envelopeColorService;

    public function __construct(PaletteRepository $palettesRepository, EnvelopeColorService $envelopeColorService)
    {
        $this->palettesRepository = $palettesRepository;
        $this->envelopeColorService = $envelopeColorService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(CreateEnvelopeColorParameters $parameters) use ($output) {
            $output->writeln(sprintf('  - %s', $parameters->getHexCode()));

            self::$colors[] = $this->envelopeColorService->createEnvelopeColor($parameters);
        }, $this->getEnvelopeColors());
    }

    private function getEnvelopeColors(): array
    {
        return array_map(function(array $input) {
            return new CreateEnvelopeColorParameters(
                new ImmutableLocalizedString($input['title']),
                new ImmutableLocalizedString($input['description']),
                $input['hex_code']
            );
        }, $this->getColorSource());
    }

    private function getColorSource(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/source.json'), true);
    }
}