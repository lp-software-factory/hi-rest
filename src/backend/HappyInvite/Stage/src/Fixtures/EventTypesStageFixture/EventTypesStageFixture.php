<?php
namespace HappyInvite\Stage\Fixtures\EventTypesStageFixture;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Parameters\CreateEventTypeParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Parameters\CreateEventTypeGroupParameters;
use HappyInvite\Domain\Bundles\Event\Bundles\EventTypeGroup\Service\EventTypeGroupService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class EventTypesStageFixture implements StageFixture
{
    /** @var EventTypeService */
    private $eventTypeService;

    /** @var EventTypeGroupService */
    private $eventTypeGroupService;

    public function __construct(EventTypeService $eventTypeService, EventTypeGroupService $eventTypeGroupService)
    {
        $this->eventTypeService = $eventTypeService;
        $this->eventTypeGroupService = $eventTypeGroupService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        foreach($this->getEventTypes() as $groupRequest) {
            /** @noinspection PhpUndefinedMethodInspection */
            $output->writeln(sprintf('  - %s', $groupRequest['group']['title']->getTranslation('en_US')));

            $group = $this->eventTypeGroupService->createEventTypeGroup(new CreateEventTypeGroupParameters(
                $groupRequest['group']['url'],
                $groupRequest['group']['title'],
                $groupRequest['group']['description']
            ));

            foreach($groupRequest['types'] as $typeRequest) {
                /** @noinspection PhpUndefinedMethodInspection */
                $output->writeln(sprintf('  --- %s', $typeRequest['title']->getTranslation('en_US')));

                $this->eventTypeService->createEventType(new CreateEventTypeParameters(
                    $typeRequest['url'],
                    $typeRequest['title'],
                    $typeRequest['description'],
                    $group->getId()
                ));
            }
        }
    }

    public function getEventTypes(): array
    {
        return [
            [
                'group' => [
                    'url' => 'wedding',
                    'title' => new ImmutableLocalizedString([
                        ['region' => 'ru_RU', 'value' => 'Свадьба'],
                        ['region' => 'en_US', 'value' => 'Wedding'],
                        ['region' => 'en_GB', 'value' => 'Wedding'],
                    ]),
                    'description' => new ImmutableLocalizedString([
                        ['region' => 'ru_RU', 'value' => 'Свадьба'],
                        ['region' => 'en_US', 'value' => 'Wedding'],
                        ['region' => 'en_GB', 'value' => 'Wedding'],
                    ])
                ],
                'types' => [
                    [
                        'url' => 'wedding',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Свадьба'],
                            ['region' => 'en_US', 'value' => 'Wedding'],
                            ['region' => 'en_GB', 'value' => 'Wedding'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Свадьба'],
                            ['region' => 'en_US', 'value' => 'Wedding'],
                            ['region' => 'en_GB', 'value' => 'Wedding'],
                        ])
                    ],
                    [
                        'url' => 'hen-party',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Девичник'],
                            ['region' => 'en_US', 'value' => 'Hen-party'],
                            ['region' => 'en_GB', 'value' => 'Hen-party'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Свадьба'],
                            ['region' => 'en_US', 'value' => 'Hen-party'],
                            ['region' => 'en_GB', 'value' => 'Hen-party'],
                        ])
                    ],
                    [
                        'url' => 'stap-party',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Мальчишник'],
                            ['region' => 'en_US', 'value' => 'Stag-party'],
                            ['region' => 'en_GB', 'value' => 'Stag-party'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Мальчишник'],
                            ['region' => 'en_US', 'value' => 'Stag-party'],
                            ['region' => 'en_GB', 'value' => 'Stag-party'],
                        ])
                    ],
                    [
                        'url' => 'engagement',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Помолвка'],
                            ['region' => 'en_US', 'value' => 'Engagement'],
                            ['region' => 'en_GB', 'value' => 'Engagement'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Помолвка'],
                            ['region' => 'en_US', 'value' => 'Engagement'],
                            ['region' => 'en_GB', 'value' => 'Engagement'],
                        ])
                    ],
                    [
                        'url' => 'save-the-date',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Сохрани дату'],
                            ['region' => 'en_US', 'value' => 'Save the date'],
                            ['region' => 'en_GB', 'value' => 'Save the date'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Сохрани дату'],
                            ['region' => 'en_US', 'value' => 'Save the date'],
                            ['region' => 'en_GB', 'value' => 'Save the date'],
                        ])
                    ],
                    [
                        'url' => 'wedding-anniversary',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Годовщина свадьбы'],
                            ['region' => 'en_US', 'value' => 'Wedding anniversary'],
                            ['region' => 'en_GB', 'value' => 'Wedding anniversary'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Годовщина свадьбы'],
                            ['region' => 'en_US', 'value' => 'Wedding anniversary'],
                            ['region' => 'en_GB', 'value' => 'Wedding anniversary'],
                        ])
                    ],
                ]
            ],
            [
                'group' => [
                    'url' => 'birthday',
                    'title' => new ImmutableLocalizedString([
                        ['region' => 'ru_RU', 'value' => 'День рождения'],
                        ['region' => 'en_US', 'value' => 'Birthday'],
                        ['region' => 'en_GB', 'value' => 'Birthday'],
                    ]),
                    'description' => new ImmutableLocalizedString([
                        ['region' => 'ru_RU', 'value' => 'День рождения'],
                        ['region' => 'en_US', 'value' => 'Birthday'],
                        ['region' => 'en_GB', 'value' => 'Birthday'],
                    ])
                ],
                'types' => [
                    [
                        'url' => 'gentlemen',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Джентельмены'],
                            ['region' => 'en_US', 'value' => 'Gentlemen'],
                            ['region' => 'en_GB', 'value' => 'Gentlemen'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Джентельмены'],
                            ['region' => 'en_US', 'value' => 'Gentlemen'],
                            ['region' => 'en_GB', 'value' => 'Gentlemen'],
                        ])
                    ],
                    [
                        'url' => 'lady',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Леди'],
                            ['region' => 'en_US', 'value' => 'Lady'],
                            ['region' => 'en_GB', 'value' => 'Lady'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Леди'],
                            ['region' => 'en_US', 'value' => 'Lady'],
                            ['region' => 'en_GB', 'value' => 'Lady'],
                        ])
                    ],
                    [
                        'url' => 'girl',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Девочка'],
                            ['region' => 'en_US', 'value' => 'Girl'],
                            ['region' => 'en_GB', 'value' => 'Girl'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Девочка'],
                            ['region' => 'en_US', 'value' => 'Girl'],
                            ['region' => 'en_GB', 'value' => 'Girl'],
                        ])
                    ],
                    [
                        'url' => 'boy',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Мальчик'],
                            ['region' => 'en_US', 'value' => 'Boy'],
                            ['region' => 'en_GB', 'value' => 'Boy'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Мальчик'],
                            ['region' => 'en_US', 'value' => 'Boy'],
                            ['region' => 'en_GB', 'value' => 'Boy'],
                        ])
                    ],
                    [
                        'url' => 'anniversary',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Юбилей'],
                            ['region' => 'en_US', 'value' => 'Anniversary'],
                            ['region' => 'en_GB', 'value' => 'Anniversary'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Юбилей'],
                            ['region' => 'en_US', 'value' => 'Anniversary'],
                            ['region' => 'en_GB', 'value' => 'Anniversary'],
                        ])
                    ],
                ]
            ],
            [
                'group' => [
                    'url' => 'business-events',
                    'title' => new ImmutableLocalizedString([
                        ['region' => 'ru_RU', 'value' => 'Бизнес-мероприятия'],
                        ['region' => 'en_US', 'value' => 'Business events'],
                        ['region' => 'en_GB', 'value' => 'Business events'],
                    ]),
                    'description' => new ImmutableLocalizedString([
                        ['region' => 'ru_RU', 'value' => 'Бизнес-мероприятия'],
                        ['region' => 'en_US', 'value' => 'Business events'],
                        ['region' => 'en_GB', 'value' => 'Business events'],
                    ])
                ],
                'types' => [
                    [
                        'url' => 'conference',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Конференция'],
                            ['region' => 'en_US', 'value' => 'Conference'],
                            ['region' => 'en_GB', 'value' => 'Conference'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Конференция'],
                            ['region' => 'en_US', 'value' => 'Conference'],
                            ['region' => 'en_GB', 'value' => 'Conference'],
                        ])
                    ],
                    [
                        'url' => 'corporate',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Корпоратив'],
                            ['region' => 'en_US', 'value' => 'Corporate'],
                            ['region' => 'en_GB', 'value' => 'Corporate'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Корпоратив'],
                            ['region' => 'en_US', 'value' => 'Corporate'],
                            ['region' => 'en_GB', 'value' => 'Corporate'],
                        ])
                    ],
                    [
                        'url' => 'business-breakfast',
                        'title' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Бизнес-завтрак'],
                            ['region' => 'en_US', 'value' => 'Business Breakfast'],
                            ['region' => 'en_GB', 'value' => 'Business Breakfast'],
                        ]),
                        'description' => new ImmutableLocalizedString([
                            ['region' => 'ru_RU', 'value' => 'Бизнес-завтрак'],
                            ['region' => 'en_US', 'value' => 'Business Breakfast'],
                            ['region' => 'en_GB', 'value' => 'Business Breakfast'],
                        ])
                    ],
                ]
            ],
        ];
    }
}