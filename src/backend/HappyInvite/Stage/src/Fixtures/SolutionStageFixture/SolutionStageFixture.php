<?php
namespace HappyInvite\Stage\Fixtures\SolutionStageFixture;

use Cocur\Chain\Chain;
use HappyInvite\Domain\Bundles\Avatar\Parameters\UploadImageParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Entity\EnvelopeTemplate;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeTemplate\Service\EnvelopeTemplatePresetService;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Entity\EventLandingTemplate;
use HappyInvite\Domain\Bundles\Event\Bundles\EventLandingTemplate\Service\EventLandingTemplatePresetService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Entity\InviteCardTemplate;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateImageService;
use HappyInvite\Domain\Bundles\Attachment\Entity\Attachment;
use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\DoctrineORM\Doctrine\Type\Point\Point;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Parameters\CreateInviteCardColorParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Color\Service\InviteCardColorService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Service\InviteCardFamilyService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Entity\InviteCardSize;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\CardSize\Service\InviteCardSizeService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Entity\InviteCardGamma;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Entity\InviteCardStyle;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Style\Service\InviteCardStyleService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Entity\EventType;
use HappyInvite\Domain\Bundles\Event\Bundles\EventType\Service\EventTypeService;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Family\Parameters\InviteCardFamilyParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Parameters\CreateInviteCardTemplateParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Template\Service\InviteCardTemplateService;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Solution\Entity\Solution;
use HappyInvite\Domain\Bundles\Solution\Parameters\CreateSolutionParameters;
use HappyInvite\Domain\Bundles\Solution\Service\SolutionService;
use HappyInvite\Stage\Fixtures\AdminStageFixture\AdminStageFixture;
use HappyInvite\Stage\Fixtures\StageFixture;
use HappyInvite\Domain\Bundles\Avatar\Definitions\Point as AvatarPoint;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class SolutionStageFixture implements StageFixture
{
    const NUM_RANDOMS = 1;

    /** @var Solution[] */
    public static $solutions = [];

    /** @var AttachmentService */
    private $attachmentService;

    /** @var LocaleService */
    private $localeService;

    /** @var EventTypeService */
    private $eventTypeService;

    /** @var InviteCardFamilyService */
    private $inviteCardFamilyService;

    /** @var InviteCardStyleService */
    private $inviteCardStyleService;

    /** @var InviteCardSizeService */
    private $inviteCardSizeService;

    /** @var InviteCardGammaService */
    private $inviteCardGammaService;

    /** @var InviteCardColorService */
    private $inviteCardColorService;

    /** @var InviteCardTemplateService */
    private $inviteCardTemplateService;

    /** @var InviteCardTemplateImageService */
    private $inviteCardTemplateImageService;

    /** @var EnvelopeTemplatePresetService */
    private $envelopeTemplatePresetService;

    /** @var EventLandingTemplatePresetService */
    private $eventLandingTemplatePresetService;

    /** @var SolutionService */
    private $solutionService;

    public function __construct(
        AttachmentService $attachmentService,
        LocaleService $localeService,
        EventTypeService $eventTypeService,
        InviteCardFamilyService $inviteCardFamilyService,
        InviteCardStyleService $inviteCardStyleService,
        InviteCardSizeService $inviteCardSizeService,
        InviteCardGammaService $inviteCardGammaService,
        InviteCardColorService $inviteCardColorService,
        InviteCardTemplateService $inviteCardTemplateService,
        InviteCardTemplateImageService $inviteCardTemplateImageService,
        EnvelopeTemplatePresetService $envelopeTemplatePresetService,
        EventLandingTemplatePresetService $eventLandingTemplatePresetService,
        SolutionService $solutionService
    ) {
        $this->attachmentService = $attachmentService;
        $this->localeService = $localeService;
        $this->eventTypeService = $eventTypeService;
        $this->inviteCardFamilyService = $inviteCardFamilyService;
        $this->inviteCardStyleService = $inviteCardStyleService;
        $this->inviteCardSizeService = $inviteCardSizeService;
        $this->inviteCardGammaService = $inviteCardGammaService;
        $this->inviteCardColorService = $inviteCardColorService;
        $this->inviteCardTemplateService = $inviteCardTemplateService;
        $this->inviteCardTemplateImageService = $inviteCardTemplateImageService;
        $this->envelopeTemplatePresetService = $envelopeTemplatePresetService;
        $this->eventLandingTemplatePresetService = $eventLandingTemplatePresetService;
        $this->solutionService = $solutionService;
    }


    public function up(OutputInterface $output, InputInterface $input): void
    {
        $dir = __DIR__.'/resources/invite_cards';
        $dirH = opendir($dir);

        $paths = [];

        while($path = readdir($dirH)) {
            if(($path !== '.') && ($path !== '..') && (is_dir($dir.'/'.$path))) {
                $paths[] = $dir.'/'.$path;
            }
        }

        $counter = 0;
        $repeats = $input->getArgument('repeat');

        do {
            $output->writeln(sprintf('[progress %d/%d]', $counter, $repeats));

            foreach($paths as $path) {
                $this->createRandomSolution($output, $path, json_decode(file_get_contents($path.'/definition.json'), true));
            }
        } while (++$counter < $repeats);
    }

    private function createRandomSolution(OutputInterface $output, string $resourcesDir, array $definition)
    {
        $output->writeln(sprintf('  - %s', basename($resourcesDir)));

        $family = $this->inviteCardFamilyService->createFamily(new InviteCardFamilyParameters(
            $this->localeService->getLocaleByRegion($definition['locale']),
            new ImmutableLocalizedString($definition['title']),
            new ImmutableLocalizedString($definition['description']),
            isset($definition['designer'])
                ? $this->findDesignerByName($definition['designer'])
                : AdminStageFixture::$designers[array_rand(AdminStageFixture::$designers)],
            $this->findEventType($definition['event_type']),
            $this->findInviteCardStyle($definition['style']),
            $this->findInviteCardGamma($definition['gamma']),
            $this->findInviteCardSize($definition['size']),
            $definition['has_photo']
        ));

        $templates = array_map(function(array $templateDefinition) use ($family, $output, $resourcesDir, $definition): InviteCardTemplate {
            $output->writeln(sprintf('    -- %s', $this->localizedString('en_GB', $templateDefinition['color']['title'])));

            $color = $this->inviteCardColorService->createColor(new CreateInviteCardColorParameters(
                new ImmutableLocalizedString($templateDefinition['color']['title']),
                $templateDefinition['color']['hex_code']
            ));

            $backdrop = $this->upload($resourcesDir.'/backdrop/'.$templateDefinition['backdrop']);

            $createTemplateParameters = new CreateInviteCardTemplateParameters(
                new ImmutableLocalizedString($definition['title']),
                new ImmutableLocalizedString($definition['description']),
                $family,
                $color,
                $backdrop,
                $templateDefinition['config'],
                []
            );

            $createTemplateParameters->bindToFamily();

            if($definition['has_photo']) {
                $placeholder = $this->upload($resourcesDir.'/placeholder/'.$templateDefinition['photo']['placeholder']);
                $containerStart = new Point($templateDefinition['photo']['container']['start']['x'], $templateDefinition['photo']['container']['start']['y']);
                $containerEnd = new Point($templateDefinition['photo']['container']['end']['x'], $templateDefinition['photo']['container']['end']['y']);

                $createTemplateParameters->withPhoto($placeholder, $containerStart, $containerEnd);
            }

            $template = $this->inviteCardTemplateService->createTemplate($createTemplateParameters);

            $this->inviteCardTemplateImageService->uploadImage($template->getId(), new UploadImageParameters(
                $resourcesDir.'/preview/'.$templateDefinition['preview'],
                new AvatarPoint(0, 0),
                new AvatarPoint(-1, -1)
            ));

            return $template;
        }, $definition['templates']);

        $envelopeTemplate = $this->createRandomEnvelopeTemplate();
        $eventLandingTemplate = $this->createEventLandingTemplate($definition['landing']);

        self::$solutions[] = $this->solutionService->createSolution(new CreateSolutionParameters(
            [$templates[0]],
            [$envelopeTemplate],
            [$eventLandingTemplate]
        ));
    }

    private function createRandomEnvelopeTemplate(): EnvelopeTemplate
    {
        return $this->envelopeTemplatePresetService->createRandomTemplate();
    }

    private function createEventLandingTemplate(array $definition): EventLandingTemplate
    {
        return $this->eventLandingTemplatePresetService->createBlackEventLandingTemplate();
    }

    private function upload(string $path): Attachment
    {
        return $this->attachmentService->uploadAttachment(
            (new UploadedFile($path, filesize($path), 0))->getStream()->getMetadata('uri'),
            basename($path)
        );
    }

    private function findDesignerByName(string $name): Profile
    {
        /** @var Profile $designer */
        foreach(AdminStageFixture::$designers as $designer) {
            if(sprintf('%s %s', $designer->getFirstName(), $designer->getLastName()) === $name) {
                return $designer;
            }
        }

        throw new \Exception(sprintf('Designer `%s` not found', $name));
    }

    private function findEventType(string $enCode): EventType
    {
        /** @var EventType $eventType */
        foreach($this->eventTypeService->getAll() as $eventType) {
            if($eventType->getTitle()->getTranslation('en_GB') === $enCode) {
                return $eventType;
            }
        }

        throw new \Exception(sprintf('Event type `%s` not found', $enCode));
    }

    private function findInviteCardStyle(string $enCode): InviteCardStyle
    {
        /** @var InviteCardStyle $style */
        foreach($this->inviteCardStyleService->getAll() as $style) {
            if($style->getTitle()->getTranslation('en_GB') === $enCode) {
                return $style;
            }
        }

        throw new \Exception(sprintf('Style`%s` not found', $enCode));
    }

    private function findInviteCardSize(string $enCode): InviteCardSize
    {
        /** @var InviteCardSize $size */
        foreach($this->inviteCardSizeService->getAll() as $size) {
            if($size->getTitle()->getTranslation('en_GB') === $enCode) {
                return $size;
            }
        }

        throw new \Exception(sprintf('Size`%s` not found', $enCode));
    }

    private function findInviteCardGamma(string $enCode): InviteCardGamma
    {
        /** @var InviteCardGamma $gamma */
        foreach($this->inviteCardGammaService->getAll() as $gamma) {
            if($gamma->getTitle()->getTranslation('en_GB') === $enCode) {
                return $gamma;
            }
        }

        throw new \Exception(sprintf('Gamma`%s` not found', $enCode));
    }
    
    private function localizedString(string $region, array $input): string
    {
        return Chain::create($input)
            ->reduce(function(string $carry, array $next) use ($region) {
                return ($next['region'] === $region)
                    ? $next['value']
                    : $carry;
            }, '');
    }
}