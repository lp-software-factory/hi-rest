<?php
namespace HappyInvite\Stage\Fixtures\EnvelopePatternStageFixture;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeColor\Entity\EnvelopeColor;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Entity\EnvelopePattern;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\CreateEnvelopePatternParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Parameters\Definition\CreateEnvelopePatternDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopePattern\Service\EnvelopePatternService;
use HappyInvite\Stage\Fixtures\EnvelopeColorStageFixture\EnvelopeColorStageFixture;
use HappyInvite\Stage\Fixtures\StageFixture;
use Intervention\Image\ImageManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class EnvelopePatternStageFixture implements StageFixture
{
    /** @var EnvelopePattern[] */
    public static $patterns = [];

    /** @var ImageManager */
    private $imageManager;

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopePatternService */
    private $envelopePatternService;

    /** @var EnvelopePatternDefinitionService */
    private $envelopePatternDefinitionService;

    public function __construct(
        ImageManager $imageManager,
        AttachmentService $attachmentService,
        EnvelopePatternService $envelopePatternService,
        EnvelopePatternDefinitionService $envelopePatternDefinitionService
    ) {
        $this->imageManager = $imageManager;
        $this->attachmentService = $attachmentService;
        $this->envelopePatternService = $envelopePatternService;
        $this->envelopePatternDefinitionService = $envelopePatternDefinitionService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(array $source) use ($output) {
            $output->writeln(sprintf(' -- %s', $source['title'][0]['value']));

            $current = $this->envelopePatternService->createEnvelopePattern(new CreateEnvelopePatternParameters(
                new ImmutableLocalizedString($source['title']),
                new ImmutableLocalizedString($source['description'])
            ));

            self::$patterns[] = $current;

            foreach ($source['definitions'] as $definition) {
                $color = $this->getColorByHexCode($definition['color']);

                $output->writeln(sprintf(' -- -- %s', $color->getHexCode()));

                $texture = sprintf('%s/resources/patterns/%s', __DIR__, $definition['attachment']);
                $preview = sprintf('%s/resources/patterns/%s', __DIR__, $definition['attachment']);

                $textureAttachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($texture, filesize($texture), 0))->getStream()->getMetadata('uri'),
                    basename($texture)
                );

                $previewAttachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($preview, filesize($preview), 0))->getStream()->getMetadata('uri'),
                    basename($preview)
                );


                $this->envelopePatternDefinitionService->createEnvelopePatternDefinition(new CreateEnvelopePatternDefinitionParameters(
                    $current,
                    $textureAttachment,
                    $previewAttachment,
                    new ImmutableLocalizedString($source['title']),
                    new ImmutableLocalizedString($source['description']),
                    $color,
                    $definition['width'],
                    $definition['height']
                ));
            }
        }, $this->getEnvelopePatterns());
    }

    private function getEnvelopePatterns(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/source.json'), true);
    }

    private function getColorByHexCode(string $hexColor): EnvelopeColor
    {
        foreach(EnvelopeColorStageFixture::$colors as $color) {
            if(strtolower($color->getHexCode()) === $hexColor) {
                return $color;
            }
        }

        throw new \Exception(sprintf('Color "%s" not found', $hexColor));
    }
}