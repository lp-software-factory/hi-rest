<?php
namespace HappyInvite\Stage\Fixtures\EnvelopeMarksStageFixture;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Entity\EnvelopeMarks;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\CreateEnvelopeMarksParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Parameters\Definition\CreateEnvelopeMarkDefinitionParameters;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarkDefinitionService;
use HappyInvite\Domain\Bundles\Envelope\Bundles\EnvelopeMarks\Service\EnvelopeMarksService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class EnvelopeMarksStageFixture implements StageFixture
{
    /** @var EnvelopeMarks[] */
    public static $marks = [];

    /** @var AttachmentService */
    private $attachmentService;

    /** @var EnvelopeMarksService */
    private $envelopeMarksService;

    /** @var EnvelopeMarkDefinitionService */
    private $envelopeMarkDefinitionService;

    public function __construct(
        AttachmentService $attachmentService,
        EnvelopeMarksService $envelopeMarksService,
        EnvelopeMarkDefinitionService $envelopeMarkDefinitionService
    ) {
        $this->attachmentService = $attachmentService;
        $this->envelopeMarksService = $envelopeMarksService;
        $this->envelopeMarkDefinitionService = $envelopeMarkDefinitionService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(array $source) use ($output) {
            $output->writeln(sprintf('  - %s', $source['title'][0]['value']));

            $parameters = new CreateEnvelopeMarksParameters(
                new ImmutableLocalizedString($source['title']),
                new ImmutableLocalizedString($source['description'])
            );

            $marks = $this->envelopeMarksService->createEnvelopeMarks($parameters);

            foreach($source['definitions'] as $definition) {
                $output->writeln(sprintf('   - * %s', $definition['attachment']));

                $file = $definition['attachment'];
                $file = __DIR__.'/resources/marks/'.$file;

                $attachment = $this->attachmentService->uploadAttachment(
                    (new UploadedFile($file, filesize($file), 0))->getStream()->getMetadata('uri'),
                    basename($file)
                );

                $this->envelopeMarkDefinitionService->createEnvelopeMarkDefinition(new CreateEnvelopeMarkDefinitionParameters(
                    $marks,
                    new ImmutableLocalizedString($definition['title']),
                    new ImmutableLocalizedString($definition['description']),
                    $attachment
                ));
            }

            self::$marks[] = $marks;
        }, $this->getEnvelopeMarks());
    }

    private function getEnvelopeMarks(): array
    {
        return json_decode(file_get_contents(__DIR__.'/resources/source.json'), true);
    }
}