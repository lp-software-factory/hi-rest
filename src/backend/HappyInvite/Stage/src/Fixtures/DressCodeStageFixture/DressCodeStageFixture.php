<?php
namespace HappyInvite\Stage\Fixtures\DressCodeStageFixture;

use HappyInvite\Domain\Bundles\Attachment\Service\AttachmentService;
use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\DressCode\Parameters\CreateDressCodeParameters;
use HappyInvite\Domain\Bundles\DressCode\Service\DressCodeService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Diactoros\UploadedFile;

final class DressCodeStageFixture implements StageFixture
{
    /** @var AttachmentService */
    private $attachmentService;

    /** @var DressCodeService */
    private $dressCodeService;

    public function __construct(
        AttachmentService $attachmentService,
        DressCodeService $dressCodeService
    ) {
        $this->attachmentService = $attachmentService;
        $this->dressCodeService = $dressCodeService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        $definitions = json_decode(file_get_contents(__DIR__.'/resources/definition.json'), true);

        foreach($definitions as $definition) {
            $output->writeln(sprintf(' -- %s', $definition['image']));

            $this->dressCodeService->createDressCode(new CreateDressCodeParameters(
                $this->upload(__DIR__.'/resources/image/'.$definition['image']),
                new ImmutableLocalizedString($definition['title']),
                new ImmutableLocalizedString($definition['description']),
                new ImmutableLocalizedString($definition['description_men']),
                new ImmutableLocalizedString($definition['description_women'])
            ));
        }
    }

    private function upload(string $path) {
        return $this->attachmentService->uploadAttachment(
            (new UploadedFile($path, filesize($path), 0))->getStream()->getMetadata('uri'),
            basename($path)
        );
    }
}