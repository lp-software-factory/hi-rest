<?php
namespace HappyInvite\Stage\Fixtures\InviteCardGammaStageFixture;

use HappyInvite\Domain\Bundles\Locale\Doctrine\Type\LocalizedString\Value\ImmutableLocalizedString;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Parameters\CreateInviteCardGammaParameters;
use HappyInvite\Domain\Bundles\InviteCard\Bundles\Gamma\Service\InviteCardGammaService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class InviteCardGammaStageFixture implements StageFixture
{
    /** @var InviteCardGammaService */
    private $inviteCardGammaService;

    public function __construct(InviteCardGammaService $inviteCardGammaService)
    {
        $this->inviteCardGammaService = $inviteCardGammaService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        array_map(function(CreateInviteCardGammaParameters $parameters) use ($output) {
            $output->writeln(sprintf('  - %s', $parameters->getTitle()->getTranslation('en_US')));

            $this->inviteCardGammaService->createGamma($parameters);
        }, $this->getInviteCardGammas());
    }

    private function getInviteCardGammas(): array
    {
        return [
            new CreateInviteCardGammaParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Пастельная'],
                    ['region' => 'en_US', 'value' => 'Pastel'],
                    ['region' => 'en_GB', 'value' => 'Pastel'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Пастельная'],
                    ['region' => 'en_US', 'value' => 'Pastel'],
                    ['region' => 'en_GB', 'value' => 'Pastel'],
                ])
            ),
            new CreateInviteCardGammaParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Яркая'],
                    ['region' => 'en_US', 'value' => 'Bright'],
                    ['region' => 'en_GB', 'value' => 'Bright'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Яркая'],
                    ['region' => 'en_US', 'value' => 'Bright'],
                    ['region' => 'en_GB', 'value' => 'Bright'],
                ])
            ),
            new CreateInviteCardGammaParameters(
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Ахроматическая'],
                    ['region' => 'en_US', 'value' => 'Achromatic'],
                    ['region' => 'en_GB', 'value' => 'Achromatic'],
                ]),
                new ImmutableLocalizedString([
                    ['region' => 'ru_RU', 'value' => 'Ахроматическая'],
                    ['region' => 'en_US', 'value' => 'Achromatic'],
                    ['region' => 'en_GB', 'value' => 'Achromatic'],
                ])
            ),
        ];
    }
}