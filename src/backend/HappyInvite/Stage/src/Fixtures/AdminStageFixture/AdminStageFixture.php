<?php
namespace HappyInvite\Stage\Fixtures\AdminStageFixture;

use HappyInvite\Domain\Bundles\Account\Service\AccountService;
use HappyInvite\Domain\Bundles\Avatar\Definitions\Point;
use HappyInvite\Domain\Bundles\Avatar\Parameters\UploadImageParameters;
use HappyInvite\Domain\Bundles\Company\Entity\Company;
use HappyInvite\Domain\Bundles\Company\Parameters\CreateCompanyParameters;
use HappyInvite\Domain\Bundles\Company\Service\CompanyService;
use HappyInvite\Domain\Bundles\Designer\Form\DesignerRequestForm;
use HappyInvite\Domain\Bundles\Designer\Service\DesignerFormService;
use HappyInvite\Domain\Bundles\Profile\Entity\Profile;
use HappyInvite\Domain\Bundles\Profile\Parameters\CreateProfileParameters;
use HappyInvite\Domain\Bundles\Profile\Service\ProfileService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class AdminStageFixture implements StageFixture
{
    /** @var Profile[] */
    public static $designers = [];

    /** @var AccountService */
    private $accountService;

    /** @var ProfileService */
    private $profileService;

    /** @var CompanyService */
    private $companyService;

    /** @var DesignerFormService */
    private $designerFormService;

    public function __construct(
        AccountService $accountService,
        ProfileService $profileService,
        CompanyService $companyService,
        DesignerFormService $designerFormService
    ) {
        $this->accountService = $accountService;
        $this->profileService = $profileService;
        $this->companyService = $companyService;
        $this->designerFormService = $designerFormService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        $company = $this->createHappyInviteCompany();

        foreach($this->getAccounts() as $request) {
            $output->writeln(sprintf('  - %s', $request['email']));

            $account = $this->accountService->createAccount($request['email'], $request['password'], array_diff($request['roles'], ['designer']));
            $profile = $this->profileService->createProfileForAccount($account, new CreateProfileParameters(
                $request['first_name'],
                $request['last_name'],
                $request['middle_name']
            ));

            $this->profileService->uploadProfileImage($profile->getId(), new UploadImageParameters(
                $request['avatar']['file'],
                new Point($request['avatar']['crop']['start'][0], $request['avatar']['crop']['start'][1]),
                new Point($request['avatar']['crop']['end'][0], $request['avatar']['crop']['end'][1])
            ));

            if(in_array('designer', $request['roles'])) {
                self::$designers[] = $profile;

                $form = $this->designerFormService->sendDesignerRequestForm(new DesignerRequestForm(
                    $profile->getFirstName(),
                    $profile->getLastName(),
                    $profile->getAccount()->getEmail(),
                    $profile->getAccount()->hasPhone() ? $profile->getAccount()->getPhone() : '',
                    '',
                    '',
                    true
                ));

                $this->designerFormService->acceptDesignerRequest($form->getId());
            }

            $this->profileService->setProfileCompanies($profile, [$company]);
        }
    }

    private function createHappyInviteCompany(): Company
    {
        $company = $this->companyService->createCompany(new CreateCompanyParameters(
            'Happy Invite'
        ));

        return $company;
    }

    private function getAccounts(): array
    {
        return [
            [
                'email' => 'root@example.com',
                'password' => '1234',
                'roles' => ['admin', 'designer'],
                'first_name' => 'Admin',
                'last_name' => 'Happy',
                'middle_name' => '',
                'avatar' => [
                    'file' => __DIR__.'/resources/root.png',
                    'crop' => [
                        'start' => [0, 0],
                        'end' => [390, 390],
                    ]
                ]
            ],
            [
                'email' => 'user@example.com',
                'password' => '1234',
                'roles' => [],
                'first_name' => 'User',
                'last_name' => 'Happy',
                'middle_name' => '',
                'avatar' => [
                    'file' => __DIR__.'/resources/root.png',
                    'crop' => [
                        'start' => [0, 0],
                        'end' => [390, 390],
                    ]
                ]
            ],
            [
                'email' => 'dborisenkowork@gmail.com',
                'password' => 'p2kiri966ng',
                'roles' => ['admin'],
                'first_name' => 'Дмитрий',
                'last_name' => 'Борисенко',
                'middle_name' => '',
                'avatar' => [
                    'file' => __DIR__.'/resources/dborisenkowork.png',
                    'crop' => [
                        'start' => [0, 0],
                        'end' => [319, 319],
                    ]
                ]
            ],
            [
                'email' => 'naumidium@mail.ru',
                'password' => 'erguih43987v',
                'roles' => ['admin', 'designer'],
                'first_name' => 'Ирина',
                'last_name' => 'Королева',
                'middle_name' => '',
                'avatar' => [
                    'file' => __DIR__.'/resources/naumidium.png',
                    'crop' => [
                        'start' => [0, 0],
                        'end' => [512, 512],
                    ]
                ]
            ],
            [
                'email' => 'dulepova@inbox.ru',
                'password' => '48ht3498vhdkj3',
                'roles' => ['admin', 'designer'],
                'first_name' => 'Елена',
                'last_name' => 'Дулепова',
                'middle_name' => '',
                'avatar' => [
                    'file' => __DIR__.'/resources/dulepova.png',
                    'crop' => [
                        'start' => [0, 0],
                        'end' => [519, 519],
                    ]
                ]
            ]
        ];
    }
}