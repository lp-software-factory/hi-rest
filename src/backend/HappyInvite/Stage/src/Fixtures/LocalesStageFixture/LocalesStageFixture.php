<?php
namespace HappyInvite\Stage\Fixtures\LocalesStageFixture;

use HappyInvite\Domain\Bundles\Locale\Parameters\CreateLocaleParameters;
use HappyInvite\Domain\Bundles\Locale\Service\LocaleService;
use HappyInvite\Stage\Fixtures\StageFixture;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class LocalesStageFixture implements StageFixture
{
    /** @var LocaleService */
    private $localeService;

    public function __construct(LocaleService $localeService)
    {
        $this->localeService = $localeService;
    }

    public function up(OutputInterface $output, InputInterface $input): void
    {
        foreach ($this->getLocales() as $request) {
            $output->writeln(sprintf('  - %s', $request['region']));

            $this->localeService->createLocale(new CreateLocaleParameters(
                $request['language'],
                $request['region']
            ));
        }
    }

    private function getLocales(): array
    {
        return [
            [
                'language' => 'ru',
                'region' => 'ru_RU'
            ],
            [
                'language' => 'en',
                'region' => 'en_GB'
            ],
            [
                'language' => 'en',
                'region' => 'en_US'
            ],
        ];
    }
}