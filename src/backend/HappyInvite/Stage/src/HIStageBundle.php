<?php
namespace HappyInvite\Stage;

use HappyInvite\Platform\Bundles\HIBundle;

final class HIStageBundle extends HIBundle
{
    public function getDir(): string
    {
        return __DIR__;
    }
}