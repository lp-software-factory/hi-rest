<?php
namespace HappyInvite\Stage\Console\Command;

use HappyInvite\Stage\Scripts\FixturesUpScript;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class StageUpCommand extends Command
{
    /** @var FixturesUpScript */
    private $script;

    public function __construct(FixturesUpScript $script)
    {
        $this->script = $script;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('stage:up')
            ->setDescription('Up stage')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('repeat', InputArgument::OPTIONAL, 'Solution random repeat', 1 ),
                ]))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->script->up($output, $input);
    }
}