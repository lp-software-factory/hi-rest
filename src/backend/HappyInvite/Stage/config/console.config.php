<?php
namespace HappyInvite\Stage;

use HappyInvite\Stage\Console\Command\StageUpCommand;

return [
    'config.console' => [
        'commands' => [
            'Stage' => [
                StageUpCommand::class,
            ],
        ],
    ],
];