import {HI_MAIN_MODULES} from "./modules/main/index";
import {HI_EDITOR_MODULE} from "./modules/editor/index";
import {HI_SOLUTION_MODULE} from "./modules/solution/index";
import {HI_UI_MODULE} from "./modules/main/module/ui/index";
import {HI_EVENT_LANDING_MODULE} from "./modules/event-landing/index";

export interface HIModule
{
    components?: ng.IComponentController[],
    factories?: any[],
    directives?: any[],
    routes?: ng.IComponentController[],
    services?: Function[],
    pipes?: Function[],
}

export const HI_MODULES = [
    HI_MAIN_MODULES,
    HI_EDITOR_MODULE,
    HI_SOLUTION_MODULE,
    HI_UI_MODULE,
    HI_EVENT_LANDING_MODULE,
];