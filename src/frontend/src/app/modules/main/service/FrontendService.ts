import {Service} from "../decorators/Service";

import {ReplaySubject} from "rxjs";
import {GetFrontendResponse200} from "../../../../../definitions/src/definitions/frontend/paths/frontend";

@Service({
    injects: [
        '$window',
    ]
})
export class FrontendService
{
    public static $instance: FrontendService;
    public static WINDOW_GLOBAL_KEY = 'happy-invite-frontend';

    private last: GetFrontendResponse200;
    public replay: ReplaySubject<GetFrontendResponse200> = new ReplaySubject<GetFrontendResponse200>();

    constructor(
        private $window
    ) {
        FrontendService.$instance = this;

        if($window[FrontendService.WINDOW_GLOBAL_KEY]) {
            this.next($window[FrontendService.WINDOW_GLOBAL_KEY]);
        }
    }

    private next(response: GetFrontendResponse200) {
        this.last = response;
        this.replay.next(response);
    }
}