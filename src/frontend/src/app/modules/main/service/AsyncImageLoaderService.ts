import {Service} from "../decorators/Service";

import {Observable} from "rxjs";

@Service()
export class AsyncImageLoaderService
{
    public static $name = 'hiAsyncImageLoaderService';

    private queue: string[] = [];

    isLoaded(url: string): boolean {
        return !!~this.queue.indexOf(url);
    }

    loadImages(images: string[]): Observable<string[]> {
        return Observable.forkJoin(images.map(url => this.loadImage(url)));
    }

    loadImage(url: string): Observable<string> {
        return Observable.create(observer => {
            if(this.isLoaded(url)) {
                observer.next();
                observer.complete();
            }else{
                let image = new Image();

                this.queue.push(url);

                image.src = url;
                image.onload = ev => {

                    observer.next(url);
                    observer.complete();
                };
            }
        }).share();
    }
}