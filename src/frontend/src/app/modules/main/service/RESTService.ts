import {Service} from "../decorators/Service";

import {Observable} from "rxjs";
import {MessageService, MESSAGE_LEVEL} from "../module/message/service/MessageService";
import {AuthService} from "../module/auth/service/AuthService";
import {RESTServiceInterface, RESTServiceInterfaceOptions, RESTServiceInterfaceHeader} from "../../../../../definitions/src/essentials/RESTServiceInterface";

@Service({
    injects: [
        '$http',
        AuthService,
        MessageService,
    ]
})
export class RESTService implements RESTServiceInterface
{
    constructor(
        public http: angular.IHttpService,
        public auth: AuthService,
        public messages: MessageService,
    ) {}

    public get(url: string, options: RESTServiceInterfaceOptions = {}): Observable<any> {
        if(this.auth.hasJWT()) {
            options['headers'] = this.getAuthHeaders();
        }

        return this.createHTTP('GET', url, options);
    }

    public put(url: string, json: any = '{}', options: RESTServiceInterfaceOptions = {}): Observable<any> {
        if(this.auth.hasJWT()) {
            options['headers'] = this.getAuthHeaders();
        }

        return this.createHTTP('PUT', url, options, json);
    }

    public post(url: string, json: any = '{}', options: RESTServiceInterfaceOptions = {}): Observable<any> {
        if(this.auth.hasJWT()) {
            options['headers'] = this.getAuthHeaders();
        }

        return this.createHTTP('POST', url, options, json);
    }

    public delete(url: string, options: RESTServiceInterfaceOptions = {}): Observable<any> {
        if(this.auth.hasJWT()) {
            options['headers'] = this.getAuthHeaders();
        }

        return this.createHTTP('DELETE', url, options);
    }

    public getAuthHeaders(): RESTServiceInterfaceHeader {
        let authHeaders = {};

        if(this.auth.hasJWT()) {
            authHeaders['Authorization'] = this.auth.getJWT();
        }

        return authHeaders;
    }

    private createHTTP(method: string, url: string, options: RESTServiceInterfaceOptions, data?: any): Observable<any> {
        let config: angular.IRequestConfig = {
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };


        if(options.search) {
            config.params = options.search;
        }

        if(data) {
            config.data = data;
        }

        if(options.headers) {
            for(let header in options.headers) {
                if(options.headers.hasOwnProperty(header)) {
                    config.headers[header] = options.headers[header];
                }
            }
        }

        let observable = Observable.create(observer => {
            this.http(config)
                .then(result => {
                    setTimeout(() => {
                        if(result.status === 200) {
                            observer.next(result.data);
                        }else{
                            observer.error(result.data);
                        }

                        observer.complete();
                    }, 0);
                })
                .catch(error => {
                    setTimeout(() => {
                        observer.error(this.handleError(error));
                        observer.complete();
                    }, 0);
                })
        });

        return observable.share();
    }

    private handleError(error) {
        let response = {
            status: 500,
            data: {
                success: false,
                error: 'Unknown error',
            }
        };

        if(typeof error === "string") {
            this.genericError(error);
        } else if(typeof error === null) {
            this.unknownError();
        } else if(typeof error === "object") {
            if(error.data && error.status) {
                try {
                    if(response.data.error === 'Expired token' && error.status === 403) {
                        this.auth.clearJWT();
                    }

                    response.data.error = error.data.error;
                    response.status = error.status;
                } catch(error) {
                    this.jsonParseError();
                }
            } else {
                this.unknownError();
            }
        } else {
            this.unknownError();
        }

        return response;
    }

    private unknownError() {
        this.messages.push(MESSAGE_LEVEL.Danger, ('common.http.unknown-error'))
    }

    private genericError(error: string) {
        this.messages.push(MESSAGE_LEVEL.Warning, error);
    }

    private jsonParseError() {
        this.messages.push(MESSAGE_LEVEL.Danger, ('common.http.parse-error'));
    }

    private httpError(error: string) {
        this.messages.push(MESSAGE_LEVEL.Danger, error);
    }
}