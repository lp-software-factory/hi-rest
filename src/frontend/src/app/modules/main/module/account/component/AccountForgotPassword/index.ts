import {Component} from "../../../../decorators/Component";
import {AccountRESTService} from "../../../../../../../../definitions/src/services/account/AccountRESTService";
import {RequestResetPasswordRequest} from "../../../../../../../../definitions/src/definitions/account/paths/request-reset-password";
import {httpErrorHandler} from "../../../../functions/httpErrorHandler";
import {ITranslateService, ITranslateServiceInterface} from "../../../i18n/service/TranslationService";

enum AccountForgotPasswordScreen {
    Form = <any>"form",
    Success = <any>"success",
}

@Component({
    selector: 'hi-account-forgot-password',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$window',
        AccountRESTService,
        ITranslateService,
    ],
})
export class AccountForgotPassword implements ng.IComponentController
{
    public error: string;
    public disabled: boolean = false;
    public screen: AccountForgotPasswordScreen = AccountForgotPasswordScreen.Form;

    public model: RequestResetPasswordRequest = {
        email: ''
    };

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private rest: AccountRESTService,
        private translation: ITranslateServiceInterface,
    ) {}

    ngSubmit($event) {
        this.disabled = true;
        this.error = undefined;

        this.rest.requestResetPassword({ email: this.model.email }).subscribe(
            response => {
                this.$timeout(() => {
                    this.screen = AccountForgotPasswordScreen.Success;
                    this.error = undefined;
                });
            },
            error => {
                this.$timeout(() => {
                    this.disabled = false;

                    setTimeout(() => {
                        $('#hiAccountForgotEmailInput').focus().select();
                    }, 0);

                    httpErrorHandler(error, {
                        unknown: () => {
                            this.translation(['main.account.forgot-password.error.unknown']).then(translations => {
                                this.error = translations['main.account.forgot-password.error.unknown'];
                            })
                        },
                        known: {
                            404: () => {
                                this.translation(['hi.main.account.forgot-password.errors.not-found']).then(translations => {
                                    this.error = translations['hi.main.account.forgot-password.errors.not-found'];
                                });
                            }
                        }
                    });
                })
            }
        );
    }

    cancel() {
        this.$window.location.href = '/account/sign-in';
    }
}