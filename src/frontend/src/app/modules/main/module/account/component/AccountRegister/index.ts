import {Component} from "../../../../decorators/Component";
import {AuthRESTService} from "../../../../../../../../definitions/src/services/auth/AuthRESTService";
import {SignUpRequest} from "../../../../../../../../definitions/src/definitions/auth/paths/sign-up";
import {withStatusCode} from "../../../../functions/withStatusCode";

let trim = require('trim');

enum AccountRegisterScreen {
    Form = <any>"form",
    Duplicate = <any>"duplicate",
    Success = <any>"success"
}

enum ProfileType {
    Personal = <any>"personal",
    Legal = <any>"legal"
}

enum ProfileSignUpType {
    Social = <any>"social",
    Email = <any>"email"
}

export interface AccountRegisterModel
{
    company: string;
    password: string;
    first_name: string;
    last_name: string;
    phone: string;
    email: string;
}

@Component({
    selector: 'hi-account-register',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$window',
        AuthRESTService,
    ]
})
export class AccountRegister implements ng.IComponentController
{
    private select: boolean;

    public error: string = '';
    public disabled: boolean = false;

    public screen: AccountRegisterScreen = AccountRegisterScreen.Form;
    public signUpType: ProfileSignUpType = ProfileSignUpType.Email;
    public type: ProfileType = ProfileType.Personal;
    public model: AccountRegisterModel = {
        company: '',
        password: '',
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
    };

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private rest: AuthRESTService,
    ) {}

    
    ngSubmit($event) {
        this.disabled = true;

        this.rest.signUp(this.createSignUpRequest()).subscribe(
            response => {
                this.$timeout(() => {
                    this.screen = AccountRegisterScreen.Success;
                    this.error = undefined;
                })
            },
            error => {
                this.disabled = false;

                this.$timeout(() => {
                    withStatusCode(409, error, () => {
                        this.screen = AccountRegisterScreen.Duplicate;
                    })
                });
            }
        );
    }

    oauth2(provider: string) {
        this.$window.location.href = `/backend/api/auth/oauth2/${provider}/`;
    }

    toggleSelect() {
        this.select = !this.select;
    }

    switchRegWay() {
        if(this.signUpType === ProfileSignUpType.Email)
            this.signUpType = ProfileSignUpType.Social;
        else if(this.signUpType === ProfileSignUpType.Social)
            this.signUpType = ProfileSignUpType.Email;
    }

    switchToLegal() {
        this.type = ProfileType.Legal;
        this.select = false;
    }

    switchToPersonal() {
        this.type = ProfileType.Personal;
        this.select = false;
    }

    createSignUpRequest(): SignUpRequest
    {
        let hasPhone = trim(this.model.phone).length > 0;
        let hasCompany = this.type === ProfileType.Legal;

        return {
            email: this.model.email,
            password: this.model.password,
            first_name: this.model.first_name,
            last_name: this.model.last_name,
            phone: {
                has: hasPhone,
                value: hasPhone ? this.model.phone : undefined
            },
            company: {
                has: hasCompany,
                value: hasCompany ? this.model.company : undefined
            }
        };
    }

    getProfileTypeValue(): string {
        switch(this.type) {
            default:
                return 'выберите';
            case ProfileType.Personal:
                return 'физическое лицо';
            case ProfileType.Legal:
                return 'юридическое лицо';
        }
    }
}