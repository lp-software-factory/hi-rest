import {Component} from "../../../../decorators/Component";

import {AuthService} from "../../../auth/service/AuthService";
import {AuthRESTService} from "../../../../../../../../definitions/src/services/auth/AuthRESTService";
import {withStatusCode} from "../../../../functions/withStatusCode";

let qs = require('querystring');

enum AccountRegisterProceedScreen {
    Sending = <any>"sending",
    Duplicate = <any>"duplicate",
    Success = <any>"success",
    Error = <any>"error"
}

enum ProfileType {
    Personal = <any>"personal",
    Legal = <any>"legal"
}

export interface AccountRegisterProceedModel
{
    company: string;
    password: string;
    first_name: string;
    last_name: string;
    phone: string;
    email: string;
}

@Component({
    selector: 'hiAccountRegisterProceed',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$window',
        AuthRESTService,
        AuthService,
    ],
})
export class AccountRegisterProceed implements ng.IComponentController
{
    public screen: AccountRegisterProceedScreen = AccountRegisterProceedScreen.Sending;

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private rest: AuthRESTService,
        private auth: AuthService,
    ) {}

    $onInit() {
        let parsed: { email: string; token: string } = qs.parse(this.$window.location.search.substr(1));

        if(! parsed.email || ! parsed.token) {
            this.screen = AccountRegisterProceedScreen.Error;
        }else{
            this.rest.signUpProceed(parsed.email, parsed.token).subscribe(
                response => {
                    this.$timeout(() => {
                        this.auth.signIn(response.token);
                        this.screen = AccountRegisterProceedScreen.Success;

                        setTimeout(() => {
                            this.$window.location.href = '/solutions';
                        }, 3000);
                    })
                },
                error => {
                    this.$timeout(() => {
                        this.screen = AccountRegisterProceedScreen.Error;

                        withStatusCode(409, error, () => {
                            this.screen = AccountRegisterProceedScreen.Duplicate;
                        });
                    })
                }
            )
        }
    }
}