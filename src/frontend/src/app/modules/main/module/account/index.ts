import {HIModule} from "../../../../modules";

import {AccountForgotPassword} from "./component/AccountForgotPassword/index";
import {AccountRegister} from "./component/AccountRegister/index";
import {AccountRegisterProceed} from "./component/AccountRegisterProceed/index";
import {AccountResetPassword} from "./component/AccountResetPassword/index";
import {AccountSignIn} from "./component/AccountSignIn/index";

require('./style.head.scss');

export const HI_MAIN_ACCOUNT_MODULE: HIModule = {
    components: [
        AccountForgotPassword,
        AccountRegister,
        AccountRegisterProceed,
        AccountResetPassword,
        AccountSignIn,
    ],
};