import {Component} from "../../../../decorators/Component";

import {AccountRESTService} from "../../../../../../../../definitions/src/services/account/AccountRESTService";
import {ResetPasswordRequest} from "../../../../../../../../definitions/src/definitions/account/paths/reset-password";
import {httpErrorHandler} from "../../../../functions/httpErrorHandler";
import {ITranslateService, ITranslateServiceInterface} from "../../../i18n/service/TranslationService";

enum AccountResetPasswordScreen {
    Form = <any>"form",
    Success = <any>"success",
}

@Component({
    selector: 'hi-account-reset-password',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$window',
        AccountRESTService,
        ITranslateService,
    ],
})
export class AccountResetPassword implements ng.IComponentController
{
    public screen: AccountResetPasswordScreen = AccountResetPasswordScreen.Form;

    public error: string;
    public disabled: boolean = false;
    public model: {
        accept_token: string;
        new_password: string;
        repeat: string;
    } = {
        accept_token: '',
        new_password: '',
        repeat: '',
    };

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private rest: AccountRESTService,
        private translate: ITranslateServiceInterface,
    ) {
        this.model.accept_token = $window.location.search;
    }

    createResetPasswordRequest(): ResetPasswordRequest
    {
        let url = require('url').parse(this.$window.location.href);
        let qs = require('querystring').parse(url.query);

        return {
            accept_token: qs.token,
            new_password: this.model.new_password,
        };
    }

    ngSubmit($event) {
        if(this.model.new_password !== this.model.repeat) {
            this.error = "password-not-matches";
        }else{
            this.disabled = true;
            this.error = undefined;

            this.rest.resetPassword(this.createResetPasswordRequest()).subscribe(
                response => {
                    this.$timeout(() => {
                        this.error = undefined;
                        this.screen = AccountResetPasswordScreen.Success;

                        setTimeout(() => {
                            this.$window.location.href = '/solutions'
                        }, 3000);
                    });
                },
                error => {
                    this.$timeout(() => {
                        this.disabled = false;

                        httpErrorHandler(error, {
                            unknown: error => this.error = error,
                            known: {
                                404: error => this.error = "link-expired",
                            }
                        });
                    });
                }
            );
        }
    }
}