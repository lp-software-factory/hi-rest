import {Component} from "../../../../decorators/Component";

import {AuthRESTService} from "../../../../../../../../definitions/src/services/auth/AuthRESTService";
import {AuthService} from "../../../auth/service/AuthService";
import {httpErrorHandler} from "../../../../functions/httpErrorHandler";

@Component({
    selector: 'hi-account-sign-in',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$window',
        AuthRESTService,
        AuthService,
    ],
})
export class AccountSignIn implements ng.IComponentController
{
    public error: string;
    public disabled: boolean = false;

    public model: AccountSignInModel = {
        email: '',
        password: ''
    };

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private rest: AuthRESTService,
        private auth: AuthService,
    ) {}

    ngSubmit($event) {
        this.disabled = true;

        this.rest.signIn(this.model).subscribe(
            response => {
                this.$timeout(() => {
                    this.auth.signIn(response.token);
                    this.$window.location.href = '/solutions';
                    this.error = undefined;
                });
            },
            error => {
                this.$timeout(() => {
                    this.disabled = false;

                    httpErrorHandler(error, {
                        unknown: error => this.error = error,
                        known: {
                            403: error => {
                                this.error = 'invalid-password';

                                setTimeout(() => $('#hiAccountSignInComponentEmail').select());
                            },
                            404: error => {
                                this.error = 'not-found';

                                setTimeout(() => $('#hiAccountSignInComponentPassword').select());
                            }
                        }
                    });
                });
            }
        );
    }
}

export interface AccountSignInModel
{
    email: string;
    password: string;
}