import {app} from "../../module";

import {ITranslateProvider, ITranslationTable} from "./service/TranslationService";

interface TranslationExports
{
    region: string,
    fallback: string,
    resources: ITranslationTable,
}

app.config(['$translateProvider', ($translateProvider: ITranslateProvider) => {
    if(window && window['happy-invite-frontend']) {
        let exports: TranslationExports = window['happy-invite-frontend'].exports.translations;

        $translateProvider
            .preferredLanguage(exports.region)
            .fallbackLanguage(exports.fallback)
            .translations(exports.region, exports.resources)
        ;
    }else{
        $translateProvider
            .preferredLanguage('en_US')
            .fallbackLanguage('en_US');
    }
}]);