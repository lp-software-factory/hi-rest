import {Component} from "../../../../../../../../../../../decorators/Component";

import {AbstractSvgToolbar, SvgToolbar} from "../AbstractSvgToolbar";
import {ListWindow} from "../../../../../../../../../../../helpers/ListWindow";
import {InviteCardEditorV1SvgEditorService} from "../../../../../../InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";

let color: Color.Color = require('color');

@Component({
    selector: 'hi-invite-card-editor-v1-svg-text-color-toolbar',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1SvgEditorService,
    ]
})
export class InviteCardEditorV1SvgEditorTextColorToolbar implements AbstractSvgToolbar
{
    public static TEXT_COLOR_LIST_WINDOW_LENGTH = 12;

    private window: ListWindow<string> = new ListWindow<string>(InviteCardEditorV1SvgEditorTextColorToolbar.TEXT_COLOR_LIST_WINDOW_LENGTH);

    constructor(
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    $onInit(): void {
        this.window.items = this.svg.listAvailableTextColors();
    }

    getId(): SvgToolbar {
        return SvgToolbar.TextColor;
    }

    isActive(textColor: string) {
        return this.svg.isTextColorActive(textColor);
    }

    click(textColor: string) {
        this.svg.setTextColor(textColor);
    }

    getColorSampleCSS(textColor: string) {
        return {
            "background-color": textColor,
            "border-color": color(textColor).darken(0.2),
        }
    }
}