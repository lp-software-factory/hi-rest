import {InviteCardEditorV1Tool} from "../../service";

export interface AbstractToolbar extends ng.IComponentController
{
    getId(): InviteCardEditorV1Tool;
}