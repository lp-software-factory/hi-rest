import {Component} from "../../../../../../decorators/Component"
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-invite-card-editor-v1-photo',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1Photo implements ng.IComponentController
{
    constructor(
        private service: InviteCardEditorV1Service,
    ) {}

    getCSS(): any {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        return {
            "top": container.start.y + "px",
            "left": container.start.x + "px",
            "width": Math.abs(container.end.x - container.start.x) + "px",
            "height": Math.abs(container.end.y - container.start.y) + "px",
            "background-image": "url(" + encodeURI(this.service.photo.getPhotoOrPlaceholderURL())  + ")",
        };
    }
}