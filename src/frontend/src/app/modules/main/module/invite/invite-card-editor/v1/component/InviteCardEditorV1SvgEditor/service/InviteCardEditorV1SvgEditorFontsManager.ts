import {Service} from "../../../../../../../decorators/Service";

import {Observable} from "rxjs";
import {InviteCardEditorV1SvgEditorService} from "./InviteCardEditorV1SvgEditorService";
import {FontService} from "../../../../../../font/service/FontService";
import {Font, FONT_SUPPORT_BOLD_ITALIC, FONT_SUPPORT_BOLD, FONT_SUPPORT_ITALIC} from "../../../../../../../../../../../definitions/src/definitions/font/entity/Font";
import {SvgMapInput, SvgMapElementType} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

@Service({
    injects: [
        '$http',
        FontService,
    ]
})
export class InviteCardEditorV1SvgEditorFontsManager
{
    public loaded: { [fontName: string]: boolean } = {};

    public svg: InviteCardEditorV1SvgEditorService;

    constructor(
        private $http: angular.IHttpService,
        private fonts: FontService,
    ) {}

    load(fontName: string): Observable<void> {
        return this.fonts.load(fontName);
    }

    getFontByName(fontName: string): Font {
        let result = this.fonts.available.filter(f => f.entity.title === fontName);

        if(result.length === 0) {
            throw new Error(`Font "${fontName}" not found`);
        }else if(result.length > 1) {
            throw new Error(`Font "${fontName}" has duplicates!`);
        }

        return result[0];
    }

    isSupported(fontName: string, feature: number): boolean {
        return !! (this.getFontByName(fontName).entity.supports & feature);
    }

    getDefaultFont(): Font {
        if(! this.fonts.available.length) {
            throw new Error("No fonts available yet.");
        }

        return this.fonts.available[0];
    }

    setBold(current: SvgMapInput): void {
        let isItalic = current.style.italic;

        let flag = isItalic ? FONT_SUPPORT_BOLD_ITALIC : FONT_SUPPORT_BOLD;

        if(this.isSupported(current.font.family, flag)) {
            current.style.bold = true;
        }else{
            throw new Error(`Unsupported font mode ${flag}`);
        }
    }

    unsetBold(current: SvgMapInput): void {
        current.style.bold = false;
    }

    setItalic(current: SvgMapInput): void {
        let isBold = current.style.bold;

        let flag = isBold ? FONT_SUPPORT_BOLD_ITALIC : FONT_SUPPORT_ITALIC;

        if(this.isSupported(current.font.family, flag)) {
            current.style.italic = true;
        }else{
            throw new Error(`Unsupported font mode ${flag}`);
        }
    }

    unsetItalic(current: SvgMapInput): void {
        current.style.italic = false;
    }

    setUnderline(current: SvgMapInput): void {
        current.style.underline = true;
    }

    unsetUnderline(current: SvgMapInput): void {
        current.style.underline = false;
    }

    isBoldEnabled(): boolean {
        if(! this.svg.cursor.has()) {
            return true;
        }else{
            let element = this.svg.cursor.getCurrent();

            if(element.type === SvgMapElementType.Text) {
                return this.fonts.isSupported((<SvgMapInput>element).font.family, FONT_SUPPORT_BOLD);
            }else{
                return false;
            }
        }
    }

    isBoldActive(): boolean {
        if(! this.svg.cursor.has()) {
            return false;
        }else{
            let element = this.svg.cursor.getCurrent();

            if(element.type === SvgMapElementType.Text) {
                return (<SvgMapInput>element).style.bold;
            }else{
                return false;
            }
        }
    }

    isItalicEnabled(): boolean {
        if(! this.svg.cursor.has()) {
            return true;
        }else{
            let element = this.svg.cursor.getCurrent();

            if(element.type === SvgMapElementType.Text) {
                return this.fonts.isSupported((<SvgMapInput>element).font.family, FONT_SUPPORT_ITALIC);
            }else{
                return false;
            }
        }
    }


    isItalicActive(): boolean {
        if(! this.svg.cursor.has()) {
            return false;
        }else{
            let element = this.svg.cursor.getCurrent();

            if(element.type === SvgMapElementType.Text) {
                return (<SvgMapInput>element).style.italic;
            }else{
                return false;
            }
        }
    }

    isUnderlineEnabled(): boolean {
        return true;
    }

    isUnderlineActive(): boolean {
        if(! this.svg.cursor.has()) {
            return false;
        }else{
            let element = this.svg.cursor.getCurrent();

            if(element.type === SvgMapElementType.Text) {
                return (<SvgMapInput>element).style.underline;
            }else{
                return false;
            }
        }
    }

    listAvailableFonts(): Font[] {
        return this.fonts.available;
    }
}