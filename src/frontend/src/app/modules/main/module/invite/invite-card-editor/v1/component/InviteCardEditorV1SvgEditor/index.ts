import {Component} from "../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {SvgEditorCreateStrategy} from "./strategy/SvgEditorCreateStrategy";
import {SvgEditorRotateStrategy} from "./strategy/SvgEditorRotateStrategy";
import {SvgEditorRemoveStrategy} from "./strategy/SvgEditorRemoveStrategy";
import {SvgEditorResizeStrategy} from "./strategy/SvgEditorResizeStrategy";
import {SvgEditorEditStrategy} from "./strategy/SvgEditorEditStrategy";
import {SvgEditorMoveStrategy} from "./strategy/SvgEditorMoveStrategy";
import {SvgEditorMode, InviteCardEditorV1SvgEditorService} from "./service/InviteCardEditorV1SvgEditorService";
import {SvgMapDocument, SvgMapInput, SvgMapElement} from "../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

let angular: ng.IAngularStatic = require('angular');

export interface ISvgEditorStrategy
{
    onElementClick?(event: MouseEvent, element: SvgMapElement): void;
    onMouseElementOut?(event: MouseEvent, element: SvgMapElement): void;
    onMouseElementOver?(event: MouseEvent, element: SvgMapElement): void;
    mouseUp?(event: MouseEvent): void;
    mouseDown?(event: MouseEvent, element?: SvgMapElement): void;
    mouseMove?(event: MouseEvent): void;
    mouseClick?(event: MouseEvent): void;
}

@Component({
    selector: 'hi-invite-card-editor-v1-svg-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$document',
        '$timeout',
        InviteCardEditorV1SvgEditorService,
    ],
    bindings: {
        'hiEditMode': '<',
    }
})
export class InviteCardEditorV1SvgEditor implements ng.IComponentController
{
    public static HTML_CONTAINER_ID = 'hiInviteCardEditorV1SvgEditorContainer';

    private strategy: ISvgEditorStrategy;
    private subscriptions: Subscription[] = [];

    private hovered: SvgMapElement;

    public hiEditMode: boolean = true;

    constructor(
        private $document: ng.IDocumentService,
        private $timeout: ng.ITimeoutService,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {
        this.subscriptions.push(svg.modeChangesSubject.subscribe(mode => {
            switch(mode) {
                default:
                    this.strategy = undefined;
                    break;

                case SvgEditorMode.Creating:
                    this.strategy = new SvgEditorCreateStrategy(this, this.svg);
                    break;

                case SvgEditorMode.Moving:
                    this.strategy = new SvgEditorMoveStrategy(this, this.svg);
                    break;

                case SvgEditorMode.Rotating:
                    this.strategy = new SvgEditorRotateStrategy(this, this.svg);
                    break;

                case SvgEditorMode.Removing:
                    this.strategy = new SvgEditorRemoveStrategy(this, this.svg);
                    break;

                case SvgEditorMode.Resizing:
                    this.strategy = new SvgEditorResizeStrategy(this, this.svg);
                    break;

                case SvgEditorMode.Editing:
                    this.strategy = new SvgEditorEditStrategy(this, this.svg);
                    break;
            }
        }))
    }

    $onDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    setMode(mode: SvgEditorMode): void {
        this.svg.mode = mode;
    }

    getContainerId(): string {
        return InviteCardEditorV1SvgEditor.HTML_CONTAINER_ID;
    }

    getContainerCSS(): any {
        let sizes = this.svg.size;
        let cursor = "default";

        switch(this.svg.mode) {
            case SvgEditorMode.Creating:
                cursor = 'crosshair';
                break;
        }

        return {
            "cursor": cursor,
            "width": sizes.width + "px",
            "height": sizes.height + "px",
        };
    }

    hasDocument(): boolean {
        return !!this.svg.document;
    }

    getDocument(): SvgMapDocument {
        if(! this.hasDocument()) {
            throw new Error('No document available!');
        }else{
            return this.svg.document;
        }
    }

    getInputs(): SvgMapInput[] {
        return this.getDocument().inputs;
    }

    isEditModeEnabled(): boolean {
        return (this.hiEditMode && this.svg.mode === SvgEditorMode.Editing);
    }


    onMouseDown(event: MouseEvent): void {
        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.mouseDown) {
                    this.strategy.mouseDown(event);
                }
            });
        }
    }

    onMouseUp(event: MouseEvent): void {
        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.mouseUp) {
                    this.strategy.mouseUp(event);
                }
            });
        }
    }

    onMouseMove(event: MouseEvent): void {
        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.mouseMove) {
                    this.strategy.mouseMove(event);
                }
            });
        }
    }

    onMouseClick(event: MouseEvent): void {
        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.mouseClick) {
                    this.strategy.mouseClick(event);
                }
            });
        }
    }
}