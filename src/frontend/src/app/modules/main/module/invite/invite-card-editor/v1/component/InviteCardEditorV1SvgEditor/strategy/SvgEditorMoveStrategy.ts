import {ISvgEditorStrategy, InviteCardEditorV1SvgEditor} from "../index";
import {SvgMapElement} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";
import {InviteCardEditorV1SvgEditorService} from "../service/InviteCardEditorV1SvgEditorService";

interface Point { x: number, y: number }

export class SvgEditorMoveStrategy implements ISvgEditorStrategy
{
    private dragEnabled: boolean = false;
    private hovered: SvgMapElement;
    private start: Point = { x: 0, y: 0 };
    private from: Point = { x: 0, y: 0 };

    constructor(
        private component: InviteCardEditorV1SvgEditor,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    getMouseDiff(event: MouseEvent): Point {
        return {
            x: this.start.x - event.offsetX,
            y: this.start.y - event.offsetY,
        }
    }

    onElementClick(event: MouseEvent, element: SvgMapElement): void {
        this.svg.cursor.select(element);
    }

    onMouseElementOut(event: MouseEvent, element: SvgMapElement): void { this.hovered = undefined; }
    onMouseElementOver(event: MouseEvent, element: SvgMapElement): void { this.hovered = element; }

    mouseDown(event: MouseEvent, element?: SvgMapElement): void {
        if(this.hovered) {
            this.dragEnabled = true;

            this.svg.cursor.select(this.hovered);

            this.start = { x: this.hovered.container.x, y: this.hovered.container.y  };
            this.from = { x: event.screenX, y: event.screenY };
        }
    }

    mouseMove(event: MouseEvent): void {
        if(this.svg.cursor.has() && this.dragEnabled) {
            this.svg.cursor.getCurrent().container.x = Math.abs(event.screenX - this.from.x + this.start.x);
            this.svg.cursor.getCurrent().container.y = Math.abs(event.screenY - this.from.y + this.start.y);
        }
    }

    mouseUp(event: MouseEvent): void {
        this.dragEnabled = false;
    }

    mouseClick(event: MouseEvent): void {}
}