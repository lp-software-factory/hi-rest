import {Component} from "../../../../../../../../../decorators/Component";

import {Color} from "color";
import {AbstractToolbar} from "../AbstractToolbar";
import {InviteCardEditorV1Tool} from "../../../service";
import {InviteCardTemplate} from "../../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {ListWindow} from "../../../../../../../../../helpers/ListWindow";
import {InviteCardEditorV1Service} from "../../../../../service/InviteCardEditorV1Service";

let color: Color = require('color');

@Component({
    selector: 'hi-invite-card-editor-v1-toolbars-template-color',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1TemplateColorToolbar implements AbstractToolbar
{
    public static PAGE_SIZE = 8;

    private lastEnabled: InviteCardTemplate;
    private window: ListWindow<InviteCardTemplate> = new ListWindow<InviteCardTemplate>(InviteCardEditorV1TemplateColorToolbar.PAGE_SIZE);

    constructor(
        private $timeout: ng.ITimeoutService,
        private service: InviteCardEditorV1Service,
    ) {
        this.window.items = this.service.model.listTemplates();
    }
    
    getId(): InviteCardEditorV1Tool {
        return InviteCardEditorV1Tool.TemplateColor;
    }

    getCSS(input: InviteCardTemplate): any {
        let hexColor = input.entity.color.hex_code;

        return {
            'background-color': hexColor,
            'border-color': color(hexColor).darken(0.15).string(),
        }
    }

    activate(input: InviteCardTemplate): void {
        let isCurrentlyNewTemplateLoading = this.service.view.status.isLoading();

        if(! (this.isActive(input) || isCurrentlyNewTemplateLoading)) {
            this.service.model.useTemplate(input).subscribe(() => {

                this.$timeout(() => {
                    this.lastEnabled = input;
                });
            });
        }
    }

    isActive(input: InviteCardTemplate): boolean {
        let isCurrentlyNewTemplateLoading = this.service.view.status.isLoading();

        let entity: InviteCardTemplate = (isCurrentlyNewTemplateLoading && !!this.lastEnabled)
            ? this.lastEnabled
            : this.service.model.model.template;

        return entity.entity.color.hex_code === input.entity.color.hex_code;
    }
}