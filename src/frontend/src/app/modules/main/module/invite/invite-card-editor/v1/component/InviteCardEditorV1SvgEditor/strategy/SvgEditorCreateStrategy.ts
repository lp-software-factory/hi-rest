import {ISvgEditorStrategy, InviteCardEditorV1SvgEditor} from "../index";
import {InviteCardEditorV1SvgEditorService, SvgEditorMode} from "../service/InviteCardEditorV1SvgEditorService";
import {SvgMapElement} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

export class SvgEditorCreateStrategy implements ISvgEditorStrategy
{
    private current: SvgMapElement;
    private start: { x: number, y: number } = { x: 0, y: 0 };
    private from: { x: number, y: number } = { x: 0, y: 0 };

    constructor(
        private component: InviteCardEditorV1SvgEditor,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    mouseMove(event: MouseEvent): void {
        if(this.current) {
            this.current.container.x = Math.abs(event.screenX - this.from.x + this.start.x);
            this.current.container.y = Math.abs(event.screenY - this.from.y + this.start.y);
        }
    }

    mouseClick(event: MouseEvent): void {
        this.current = this.svg.spawnTextInput(event.offsetX, event.offsetY);
        this.svg.cursor.select(this.current);
        this.svg.mode = SvgEditorMode.Moving;
        this.current = undefined;
    }
}