import {Component} from "../../../../../../decorators/Component";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

enum PhotoBoundaryMode
{
    None = <any>"none",
    Move = <any>"move",
    ResizeEnd = <any>"resize-end",
}

@Component({
    selector: 'hi-invite-card-editor-v1-photo-boundary',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1PhotoBoundary implements ng.IComponentController
{
    public static POINT_HIT_BOX = 20;
    public static MIN_WIDTH = 32;
    public static MIN_HEIGHT = 32;

    private mode: PhotoBoundaryMode;

    private initial: {
        down: Point,
        start: Point,
        end: Point,
    };

    private start: Point;
    private end: Point;

    constructor(
        private service: InviteCardEditorV1Service,
    ) {}

    $onInit(): void {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        this.start = { x: container.start.x, y: container.start.y };
        this.end = { x: container.end.x, y: container.end. y };
    }

    normalize(): void {
        if(this.mode === PhotoBoundaryMode.None) {
            let size = this.service.model.model.size.entity;
            let container = this.service.photo.getPlaceholderPhotoContainer();

            if(container.start.x < 0) container.start.x = 0;
            if(container.end.x > size.width) container.end.x = size.width;
            if(container.start.y < 0) container.start.y = 0;
            if(container.end.y > size.height) container.end.y = size.height;
        }
    }

    getBoxCSS(): any {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        return {
            "top": container.start.y + "px",
            "left": container.start.x + "px",
            "width": Math.abs(container.end.x - container.start.x) + "px",
            "height": Math.abs(container.end.y - container.start.y) + "px",
        };
    }

    getPointStartCSS(): any {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        return {
            "top": container.start.y - Math.floor(InviteCardEditorV1PhotoBoundary.POINT_HIT_BOX / 2) + "px",
            "left": container.start.x - Math.floor(InviteCardEditorV1PhotoBoundary.POINT_HIT_BOX / 2) + "px",
        }
    }

    getPointEndCSS(): any {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        return {
            "top": container.end.y - Math.floor(InviteCardEditorV1PhotoBoundary.POINT_HIT_BOX / 2) + "px",
            "left": container.end.x - Math.floor(InviteCardEditorV1PhotoBoundary.POINT_HIT_BOX / 2) + "px",
        }
    }

    onMouseUp($event: MouseEvent): void {
        this.mode = PhotoBoundaryMode.None;
        this.normalize();
    }

    onMouseMove($event: MouseEvent): void {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        if(this.mode === PhotoBoundaryMode.Move) {
            let offsetX = this.initial.down.x - $event.clientX;
            let offsetY = this.initial.down.y - $event.clientY;

            container.start = {
                x: this.initial.start.x - offsetX,
                y: this.initial.start.y - offsetY,
            };

            container.end = {
                x: this.initial.end.x - offsetX,
                y: this.initial.end.y - offsetY,
            };
        }else if(this.mode === PhotoBoundaryMode.ResizeEnd) {
            let offsetX = this.initial.down.x - $event.clientX;
            let offsetY = this.initial.down.y - $event.clientY;

            let x = this.initial.end.x - offsetX;
            let y = this.initial.end.y - offsetY;

            if((x > this.initial.start.x) && (y > this.initial.start.y)) {
                let width = Math.abs(this.initial.start.x - x);
                let height = Math.abs(this.initial.start.y - y);

                if((height >= InviteCardEditorV1PhotoBoundary.MIN_WIDTH) && (width >= InviteCardEditorV1PhotoBoundary.MIN_WIDTH)) {
                    container.end = {
                        x: x,
                        y: y,
                    };
                }
            }
        }

        this.normalize();
    }

    onMouseLeave($event: MouseEvent): void {
        this.mode = PhotoBoundaryMode.None;
    }

    startMove($event: MouseEvent): void {
        this.mode = PhotoBoundaryMode.Move;
        this.markInitial($event);
    }

    endMove($event: MouseEvent): void {
        this.mode = PhotoBoundaryMode.None;
    }

    startResizeEnd($event: MouseEvent): void {
        this.mode = PhotoBoundaryMode.ResizeEnd;
        this.markInitial($event);
    }

    endResizeEnd($event: MouseEvent): void {
        this.mode = PhotoBoundaryMode.None;
    }

    private markInitial($event: MouseEvent): void {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        this.initial = {
            down: { x: $event.clientX, y: $event.clientY },
            start: { x: container.start.x, y: container.start.y },
            end: { x: container.end.x, y: container.end.y },
        }
    }
}

interface Container {
    start: {
        x: number,
        y: number
    },
    end: {
        x: number,
        y: number,
    }
}

interface Point
{
    x: number;
    y: number;
}