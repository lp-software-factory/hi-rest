import {Subject} from "rxjs";
import {Service} from "../../../../../../decorators/Service";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

export enum InviteCardEditorV1Tool
{
    TemplateColor = <any>"template-color",
    SvgEditor = <any>"svg-editor",
    AddPhoto = <any>"add-photo",
    AddMusic = <any>"add-music",
    RevertChanges = <any>"revert-changes",
    ReplaceFamily = <any>"replace-family",
    Print = <any>"print",
    Submit = <any>"submit",
}

export interface InviteCardEditorV1ToolbarChangeEvent
{
    before: InviteCardEditorV1Tool,
    after: InviteCardEditorV1Tool,
}

@Service({
    injects: [
    ],
})
export class InviteCardEditorV1ToolbarService
{
    private current: InviteCardEditorV1Tool = InviteCardEditorV1Tool.TemplateColor;

    public editor: InviteCardEditorV1Service;
    public changeSubject: Subject<InviteCardEditorV1ToolbarChangeEvent> = new Subject<InviteCardEditorV1ToolbarChangeEvent>();

    change(newCurrent: InviteCardEditorV1Tool) {
        let before = this.current;

        this.current = newCurrent;
        this.changeSubject.next({
            before: before,
            after: newCurrent
        });
    }

    getCurrent(): InviteCardEditorV1Tool {
        return this.current;
    }

    isEnabled(id: InviteCardEditorV1Tool) {
        if(! this.editor) {
            return false;
        }

        let config = this.editor.editorConfig.toolbars;

        if(id === InviteCardEditorV1Tool.AddPhoto) {
            return config[<any>id].enabled === true
                && this.editor.model.model.template.entity.photo.enabled;
        }else{
            return config[<any>id].enabled === true;
        }
    }

    is(id: InviteCardEditorV1Tool) {
        if(! this.editor) {
            return false;
        }

        return this.current === id;
    }
}