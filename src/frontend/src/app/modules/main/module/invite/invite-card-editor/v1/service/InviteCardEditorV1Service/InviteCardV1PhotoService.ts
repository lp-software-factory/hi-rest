import {Service} from "../../../../../../decorators/Service";

import {Observable} from "rxjs";
import {InviteCardEditorV1Service} from "../InviteCardEditorV1Service";
import {UIAlertModalService} from "../../../../../ui/service/UIAlertModalService";
import {AttachmentRESTService} from "../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {AttachmentEntity} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {UploadObservableProgress} from "../../../../../../../../../../definitions/src/functions/upload";
import {UploadAttachmentResponse200} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/paths/upload";
import {ImageAttachmentMetadata} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {InviteCardTemplatePhotoContainer} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";

@Service({
    injects: [
        AttachmentRESTService,
        UIAlertModalService,
    ]
})
export class InviteCardV1PhotoService
{
    public services: InviteCardEditorV1Service;

    public static MIME_TYPES = ['image/png', 'image/jpeg', 'image/gif'];
    public static MAX_FILE_SIZE = 1024 /* b */ * 1024 /* kb */ * 2 /* Mb */;
    public static MIN_IMAGE_WIDTH = 100;
    public static MIN_IMAGE_HEIGHT = 100;
    public static MAX_IMAGE_WIDTH = 2096;
    public static MAX_IMAGE_HEIGHT = 2096;

    constructor(
        private rest: AttachmentRESTService,
        private alert: UIAlertModalService,
    ) {}

    uploadFiles($files: File[]): Observable<AttachmentEntity<ImageAttachmentMetadata>[]> {
        return Observable.create(uploadFilesObserver => {
            let observables = [];
            let validators: Observable<boolean>[] = [];

            if($files.length > 0) {
                for(let index = 0; index < $files.length; index++) {
                    validators.push(this.isValidFile($files[index]));
                }

                Observable.forkJoin(validators).subscribe(
                    success => {
                        for(let index = 0; index < $files.length; index++) {
                            observables.push(this.rest.upload($files[index]));
                        }

                        observables.forEach(o => o.subscribe(
                            response => {
                                if(! (response instanceof UploadObservableProgress)) {
                                    this.attachPhoto(response.entity);
                                    uploadFilesObserver.next(response.entity);
                                }
                            },
                            error => {
                                uploadFilesObserver.error(error);
                            }
                        ));

                        if(observables.length) {
                            Observable.forkJoin(observables).subscribe(
                                (responses: UploadAttachmentResponse200[]) => {
                                    uploadFilesObserver.complete();
                                },
                                error => {
                                    uploadFilesObserver.error(error);
                                }
                            )
                        }else{
                            uploadFilesObserver.complete();
                        }
                    },
                    error => {
                        this.raiseFileInputError();

                        uploadFilesObserver.error(error);
                    }
                );
            }else{
                uploadFilesObserver.complete();
            }
        }).share();
    }

    isValidFile(input: File): Observable<boolean> {
        return Observable.create(observer => {
            let size = input.size;
            let type = input.type;

            let result = (
                (!!~InviteCardV1PhotoService.MIME_TYPES.indexOf(type)) &&
                (size <= InviteCardV1PhotoService.MAX_FILE_SIZE)
            );

            if(result) {
                let image = new Image();
                image.src = URL.createObjectURL(input);

                image.onload = (ev) => {
                    let width = image.width;
                    let height = image.height;

                    let result = (
                        (width >= InviteCardV1PhotoService.MIN_IMAGE_WIDTH) &&
                        (width <= InviteCardV1PhotoService.MAX_IMAGE_WIDTH) &&
                        (height >= InviteCardV1PhotoService.MIN_IMAGE_HEIGHT) &&
                        (height <= InviteCardV1PhotoService.MAX_IMAGE_HEIGHT)
                    );

                    if(result) {
                        observer.next(result);
                        observer.complete();
                    }else{
                        observer.error();
                    }
                };
            }else{
                observer.error();
            }
        });
    }

    raiseFileInputError(): void {
        this.alert.open({
            title: {
                value: 'hi.main.editor.invite-card.errors.photo-validation.title',
                translate: true,
            },
            text: {
                value: 'hi.main.editor.invite-card.errors.photo-validation.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.main.editor.invite-card.errors.photo-validation.close',
                        translate: true,
                    },
                    click: (modal) => {
                        modal.close();
                    }
                }
            ]
        })
    }

    hasPhoto(): boolean {
        return this.services.model.model.photo.enabled;
    }

    getPhotos(): AttachmentEntity<ImageAttachmentMetadata>[] {
        return this.services.model.model.photo.uploaded;
    }

    getPlaceholderPhotoContainer(): InviteCardTemplatePhotoContainer {
        return this.services.model.model.photo.container;
    }

    getOriginalPlaceholderPhotoContainer(): InviteCardTemplatePhotoContainer {
        return this.services.model.model.photo.container;
    }

    getPhotoOrPlaceholderURL(): string {
        if(this.hasSelectedPhoto()) {
            return this.getSelectedPhoto().link.url;
        }else{
            return this.getPlaceholderPhoto();
        }
    }

    attachPhoto(attachment: AttachmentEntity<ImageAttachmentMetadata>): void {
        let config = this.services.model.model.photo;

        config.uploaded.push(attachment);

        if(config.uploaded.length && !config.attachment) {
            this.selectPhoto(config.uploaded[0]);
        }

    }

    hasSelectedPhoto(): boolean {
        return this.services.model.model.photo.attachment !== undefined;
    }

    getSelectedPhoto():  AttachmentEntity<ImageAttachmentMetadata> {
        if(! this.hasSelectedPhoto()) {
            throw new Error('No photo selected!');
        }else{
            return this.services.model.model.photo.attachment;
        }
    }

    getPlaceholderPhoto(): string {
        return this.services.model.model.photo.placeholder.link.url;
    }

    selectPhoto(attachment: AttachmentEntity<ImageAttachmentMetadata>): void {
        let config = this.services.model.model.photo;

        config.attachment = attachment;
    }

    detachPhoto(detach: AttachmentEntity<ImageAttachmentMetadata>): void {
        let config = this.services.model.model.photo;

        config.uploaded = config.uploaded.filter(a => a.id !== detach.id);

        if(config.attachment.id === detach.id) {
            config.attachment = undefined;
        }

        if(config.uploaded.length && !config.attachment) {
            this.selectPhoto(config.uploaded[0]);
        }
    }

    getListUploadedPhotos(): AttachmentEntity<ImageAttachmentMetadata>[] {
        return this.services.model.model.photo.uploaded;
    }

}