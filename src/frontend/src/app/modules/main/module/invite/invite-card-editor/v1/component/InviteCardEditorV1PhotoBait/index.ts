import {Component} from "../../../../../../decorators/Component";

import {InviteCardEditorV1ToolbarService, InviteCardEditorV1Tool} from "../InviteCardEditorV1Toolbar/service";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-invite-card-editor-v1-photo-bait',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1Service,
        InviteCardEditorV1ToolbarService,
    ]
})
export class InviteCardEditorV1PhotoBait implements ng.IComponentController
{
    constructor(
        private service: InviteCardEditorV1Service,
        private toolbar: InviteCardEditorV1ToolbarService,
    ) {}

    click() {
        this.toolbar.change(InviteCardEditorV1Tool.AddPhoto);
    }

    getCSS(): any {
        let container = this.service.photo.getPlaceholderPhotoContainer();

        return {
            "top": container.start.y + "px",
            "left": container.start.x + "px",
            "width": Math.abs(container.end.x - container.start.x) + "px",
            "height": Math.abs(container.end.y - container.start.y) + "px",
        };
    }
}