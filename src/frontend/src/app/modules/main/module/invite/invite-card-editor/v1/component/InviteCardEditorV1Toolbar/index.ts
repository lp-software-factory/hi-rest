import {InviteCardEditorV1ToolbarService, InviteCardEditorV1Tool} from "./service";

import {Component} from "../../../../../../decorators/Component";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-event-invite-card-editor-v1-toolbar',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$window',
        '$timeout',
        InviteCardEditorV1ToolbarService,
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1Toolbar implements ng.IComponentController
{
    constructor(
        private $window: ng.IWindowService,
        private $timeout: ng.ITimeoutService,
        private toolbars: InviteCardEditorV1ToolbarService,
        private service: InviteCardEditorV1Service,
    ) {}

    isActive(id: InviteCardEditorV1Tool): boolean {
        return this.toolbars.getCurrent() === id;
    }

    switchToolbar(id: InviteCardEditorV1Tool): void {
        this.toolbars.change(id);
    }

    hasAnyActivatedToolbar(): boolean {
        return this.toolbars.getCurrent() !== undefined;
    }

    isEnabled(id: InviteCardEditorV1Tool): boolean {
        return this.toolbars.isEnabled(id);
    }

    isActivated(id: InviteCardEditorV1Tool): boolean {
        return this.toolbars.is(id);
    }

    askForRevertChanges(): void {
        this.service.revert.askForRevertChanges();
    }

    preview(): void {
        this.service.createPreviewImage().subscribe(image => {
            this.service.preview.open(window.URL.createObjectURL(image));
        });
    }

    replaceFamily(): void {
        this.$window.location.href = '/solutions';
    }

    print(): void {
        this.service.createPreviewImage().subscribe(image => {
            let url = window.URL.createObjectURL(image);
            let windowObject = this.$window.open(url);

            windowObject.onload = () => {
                windowObject.print();
            }
        });
    }

    submit(): void {
        this.service.save().subscribe(() => {});
    }
}