import {Service} from "../../../../../../decorators/Service";

import {InviteCardEditorV1Service} from "../InviteCardEditorV1Service";
import {LoadingManager} from "../../../../../common/helpers/LoadingManager";

@Service()
export class InviteCardV1ViewPortService
{
    public id: string;

    public services: InviteCardEditorV1Service;
    public status: LoadingManager = new LoadingManager();
}