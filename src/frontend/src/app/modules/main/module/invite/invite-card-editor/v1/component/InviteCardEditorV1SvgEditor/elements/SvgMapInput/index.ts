import {Component} from "../../../../../../../../decorators/Component";
import {SvgMapInput, SvgMapElement} from "../../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";
import {SvgEditorMode, InviteCardEditorV1SvgEditorService} from "../../service/InviteCardEditorV1SvgEditorService";
import {ISvgEditorStrategy} from "../../index";
import {randomId} from "../../../../../../../../functions/randomId";

let angular: ng.IAngularStatic = require('angular');

@Component({
    selector: 'hi-invite-card-editor-v1-svg-map-input',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$window',
        '$sce',
        '$sanitize',
        '$document',
        '$timeout',
        InviteCardEditorV1SvgEditorService
    ],
    bindings: {
        'element': '<',
        'strategy': '<',
        'editMode': '<',
    }
})
export class InviteCardEditorV1SvgMapInput implements ng.IComponentController
{
    public id: string = 'svgMapInput_' + randomId(16);

    public element: SvgMapInput;
    public strategy: ISvgEditorStrategy;
    public editMode: boolean = true;

    constructor(
        private $window: ng.IWindowService,
        private $sce: ng.ISCEService,
        private $sanitize: Function,
        private $document: ng.IDocumentService,
        private $timeout: ng.ITimeoutService,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {
    }

    autoSizeInputElement($event: KeyboardEvent): void {
        let element = this.element;
        let target: HTMLTextAreaElement = <any>$event.target;

        let interval = setInterval(() => {
            if(target.clientHeight < target.scrollHeight) {
                let currentSize = element.font.size;
                let availableSizes = this.svg.listAvailableFontSizes()
                        .filter(size => size < currentSize)
                        .sort((a, b) => { return a + b })
                    ;

                if(availableSizes.length) {
                    element.font.size = availableSizes[0];

                    this.svg.events.autoResizeSubject.next(element.font.size);

                    clearInterval(interval);
                }
            }
        }, 10);
    }

    getCurrentComputedLineHeight(): number {
        if(this.$window && this.$window['getComputedStyle']) {
            let element = this.editMode
                ? document.querySelector(`#${this.id} .element-input-block textarea`)
                : document.querySelector(`#${this.id} .element-input-block span`);

            if(element) {
                return parseFloat(this.$window.getComputedStyle(element)["line-height"].replace("px", ""));
            }else{
                return this.element.lineHeight * this.element.font.size;
            }
        }else{
        }
    }

    getInputCSS(): any {
        let cursor = "default";

        if(this.svg.cursor.has() && this.svg.cursor.getCurrent() === this.element) {
            switch(this.svg.mode) {
                case SvgEditorMode.Creating:
                    cursor = 'pointer';
                    break;

                case SvgEditorMode.Moving:
                    cursor = 'move';
                    break;

                case SvgEditorMode.Removing:
                    cursor = 'not-allowed';
                    break;

                case SvgEditorMode.Rotating:
                    cursor = 'col-resize';
                    break;

                case SvgEditorMode.Resizing:
                    cursor = 'nesw-resize';
                    break;
            }
        }

        return {
            "cursor": cursor,
            "overflow": "hidden",
            "position": "absolute",
            "box-sizing": "border-box",
            "top": this.element.container.y + 'px',
            "left": this.element.container.x + 'px',
            "width": this.element.container.width + 'px',
            "height": this.element.container.height + 10 + 'px',
            "line-height": this.element.lineHeight,
            "color": this.element.textColor,
            "background-color": this.element.bgColor,
            "font-family": this.element.font.family,
            "font-size": this.element.font.size + 'px',
            "text-decoration": this.element.style.underline ? "underline" : "none",
            "font-style": this.element.style.italic ? "italic" : "normal",
            "font-weight": this.element.style.bold ? "bold" : "normal",
            "text-align": <any>this.element.align,
            "transform": "rotate(" + this.element.container.rotate + "deg)",
        }
    }

    getViewMaskCSS(): any {
        let size = this.element.font.size;
        let height = this.element.container.height;
        let lineHeight = this.getCurrentComputedLineHeight();

        let maxLines = Math.max(1, Math.floor(height / (lineHeight * size)));

        return {
            "overflow": "hidden",
            "height": maxLines * (lineHeight * size) + 0.5*(lineHeight * size) + 'px',
            "width": "100%"
        };
    }

    getTextElementCSS(): any {
        return {
            "width": this.element.container.width + "px",
            "height": this.element.container.height + "px",
        };
    }

    getInputHTML(): any {
        return this.$sce.getTrustedHtml(this.$sanitize(this.element.text).replace(/&#10;/g, "<br>"));
    }

    isSelectable(): boolean {
        let input = this.element;

        return this.svg.isSelectEnabled(input);
    }

    isSelected(): boolean {
        let input = this.element;

        return this.svg.isSelectEnabled(input) && this.svg.cursor.has() && this.svg.cursor.getCurrent() === input;
    }

    selectElement(): void {
        let element = this.element;

        this.svg.cursor.select(element);
    }

    forceEditElement($event: MouseEvent): void {
        let element = this.element;

        this.selectElement();
        this.svg.mode = SvgEditorMode.Editing;

        this.$timeout(() => {
            let idElement = angular.element(this.$document[0].querySelector('#' + element.id + ' textarea'));

            if(idElement[0] && (typeof idElement[0]['focus'] === "function")) {
                idElement[0]['focus']();
            }
        }, 50);
    }

    onElementClick(event: MouseEvent): void {
        let element = this.element;

        this.svg.cursor.select(element);

        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.onElementClick) {
                    this.strategy.onElementClick(event, element);
                }
            });
        }
    }

    onMouseElementOver(event: MouseEvent): void {
        let element = this.element;

        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.onMouseElementOver) {
                    this.strategy.onMouseElementOver(event, element);
                }
            });
        }
    }

    onMouseElementOut(event: MouseEvent): void {
        let element = this.element;

        event.preventDefault();
        event.stopPropagation();

        if(this.strategy) {
            this.$timeout(() => {
                if(!!this.strategy.onMouseElementOut) {
                    this.strategy.onMouseElementOut(event, element);
                }
            });
        }
    }
}