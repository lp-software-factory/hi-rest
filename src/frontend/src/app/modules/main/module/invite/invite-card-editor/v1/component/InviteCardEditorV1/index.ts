import {Component} from "../../../../../../decorators/Component";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-event-invite-card-editor-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        '$window',
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1 implements ng.IComponentController
{
    constructor(
        private $window: ng.IWindowService,
        private services: InviteCardEditorV1Service,
    ) {}

    $onInit(): void {
        if(this.$window && this.$window['$']) {
            setTimeout(() => {
                this.$window['$']('.hi-event-invite-card-editor-toolbars').sticky({
                    topSpacing: 0,
                });
            }, 500);
        }
    }

    isReady(): boolean {
        return this.services.isReady
    }
}