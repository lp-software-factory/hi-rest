import {InviteCardEditorV1} from "./component/InviteCardEditorV1/index";
import {InviteCardEditorV1AddMusicToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/AddMusicToobar/index";
import {InviteCardEditorV1AddPhotoToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/AddPhotoToolbar/index";
import {InviteCardEditorV1SvgEditorToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/SvgEditorToolbar/index";
import {InviteCardEditorV1TemplateColorToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/TemplateColorToolbar/index";
import {InviteCardEditorV1Toolbar} from "./component/InviteCardEditorV1Toolbar/index";
import {InviteCardEditorV1Viewport} from "./component/InviteCardEditorV1Viewport/index";
import {InviteCardEditorV1Photo} from "./component/InviteCardEditorV1Photo/index";
import {InviteCardEditorV1PhotoBoundary} from "./component/InviteCardEditorV1PhotoBoundary/index";
import {InviteCardEditorV1PhotoUploader} from "./component/InviteCardEditorV1PhotoUploader/index";
import {InviteCardEditorV1PhotoBait} from "./component/InviteCardEditorV1PhotoBait/index";
import {InviteCardEditorV1AudioUploader} from "./component/InviteCardEditorV1AudioUploader/index";
import {InviteCardEditorV1FileDropRouter} from "./component/InviteCardEditorV1FileDropRouter/index";
import {InviteCardEditorV1SvgEditor} from "./component/InviteCardEditorV1SvgEditor/index";
import {InviteCardEditorV1SvgEditorFontFamilyToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/SvgEditorToolbar/component/SvgFontFamilyToolbar/index";
import {InviteCardEditorV1SvgEditorFontSizeToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/SvgEditorToolbar/component/SvgFontSizeToolbar/index";
import {InviteCardEditorV1SvgEditorTextBackgroundToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/SvgEditorToolbar/component/SvgTextBackgroundToolbar/index";
import {InviteCardEditorV1SvgEditorTextColorToolbar} from "./component/InviteCardEditorV1Toolbar/component/toolbars/SvgEditorToolbar/component/SvgTextColorToolbar/index";
import {InviteCardEditorV1Service} from "./service/InviteCardEditorV1Service";
import {InviteCardV1AudioService} from "./service/InviteCardEditorV1Service/InviteCardV1AudioService";
import {InviteCardV1ModelService} from "./service/InviteCardEditorV1Service/InviteCardV1ModelService";
import {InviteCardV1PhotoService} from "./service/InviteCardEditorV1Service/InviteCardV1PhotoService";
import {InviteCardV1RevertService} from "./service/InviteCardEditorV1Service/InviteCardV1RevertService";
import {InviteCardV1ToolbarService} from "./service/InviteCardEditorV1Service/InviteCardV1ToolbarService";
import {InviteCardV1ViewPortService} from "./service/InviteCardEditorV1Service/InviteCardV1ViewPortService";
import {InviteCardEditorV1PreviewService} from "./component/InviteCardEditorV1Preview/service";
import {InviteCardEditorV1Preview} from "./component/InviteCardEditorV1Preview/index";
import {InviteCardEditorV1Unavailable} from "./component/InviteCardEditorV1Unavailble/index";
import {InviteCardEditorV1SvgMapInput} from "./component/InviteCardEditorV1SvgEditor/elements/SvgMapInput/index";

export const V1 = {
    component: [
        InviteCardEditorV1,
        InviteCardEditorV1AddMusicToolbar,
        InviteCardEditorV1AddPhotoToolbar,
        InviteCardEditorV1SvgEditorToolbar,
        InviteCardEditorV1TemplateColorToolbar,
        InviteCardEditorV1Toolbar,
        InviteCardEditorV1Viewport,
        InviteCardEditorV1Photo,
        InviteCardEditorV1PhotoBoundary,
        InviteCardEditorV1PhotoBait,
        InviteCardEditorV1PhotoUploader,
        InviteCardEditorV1AudioUploader,
        InviteCardEditorV1FileDropRouter,
        InviteCardEditorV1SvgEditor,
        InviteCardEditorV1SvgEditorFontFamilyToolbar,
        InviteCardEditorV1SvgEditorFontSizeToolbar,
        InviteCardEditorV1SvgEditorTextBackgroundToolbar,
        InviteCardEditorV1SvgEditorTextColorToolbar,
        InviteCardEditorV1Preview,
        InviteCardEditorV1Unavailable,
        InviteCardEditorV1SvgMapInput,
    ],
    services: [
        InviteCardEditorV1Service,
        InviteCardV1AudioService,
        InviteCardV1ModelService,
        InviteCardV1PhotoService,
        InviteCardV1RevertService,
        InviteCardV1ToolbarService,
        InviteCardV1ViewPortService,
        InviteCardEditorV1PreviewService,
    ],
};