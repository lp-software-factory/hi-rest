import {ISvgEditorStrategy, InviteCardEditorV1SvgEditor} from "../index";
import {InviteCardEditorV1SvgEditorService} from "../service/InviteCardEditorV1SvgEditorService";
import {SvgMapElement} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

export class SvgEditorRemoveStrategy implements ISvgEditorStrategy
{
    private current: SvgMapElement;
    private hovered: SvgMapElement;

    constructor(
        private component: InviteCardEditorV1SvgEditor,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    onMouseElementOut(event: MouseEvent, element: SvgMapElement): void { this.hovered = undefined; }
    onMouseElementOver(event: MouseEvent, element: SvgMapElement): void { this.hovered = element; }
    
    mouseDown(event: MouseEvent, element?: SvgMapElement): void {
        if(this.svg.cursor.has()) {
            this.current = this.svg.cursor.getCurrent();
            this.svg.destroyTextInput(this.current);
        }
    }
    
    mouseClick(event: MouseEvent): void {
        if(this.hovered && !this.svg.cursor.has()) {
            this.svg.cursor.select(this.hovered);
        }
    }
}