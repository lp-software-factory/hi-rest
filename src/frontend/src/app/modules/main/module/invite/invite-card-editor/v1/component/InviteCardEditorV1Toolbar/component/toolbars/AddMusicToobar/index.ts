import {Component} from "../../../../../../../../../decorators/Component";

import {AbstractToolbar} from "../AbstractToolbar";
import {InviteCardEditorV1Tool} from "../../../service";
import {InviteCardEditorV1Service} from "../../../../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-invite-card-editor-v1-toolbars-add-music',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1AddMusicToolbar implements AbstractToolbar
{
    constructor(
        private service: InviteCardEditorV1Service,
    ) {}
    
    getId(): InviteCardEditorV1Tool {
        return InviteCardEditorV1Tool.AddMusic;
    }
}