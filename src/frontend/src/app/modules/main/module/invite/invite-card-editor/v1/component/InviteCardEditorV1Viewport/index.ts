import {Component} from "../../../../../../decorators/Component";

import {InviteCardEditorV1ToolbarService, InviteCardEditorV1Tool} from "../InviteCardEditorV1Toolbar/service";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";
import {SvgEditorMode, InviteCardEditorV1SvgEditorService} from "../InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";
import {randomId} from "../../../../../../functions/randomId";

export enum ViewMode
{
    View = <any>"view",
    PhotoContainer = <any>"photo-container",
    TextEditor = <any>"text-editor",
}

@Component({
    selector: 'hi-invite-card-editor-v1-viewport',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1Service,
        InviteCardEditorV1ToolbarService,
        InviteCardEditorV1SvgEditorService,
    ]
})
export class InviteCardEditorV1Viewport implements ng.IComponentController
{
    public static SVG_MAP_ELEMENT_ID = 'hiInviteCardEditorV1ViewportSvgMapContainer';

    public viewMode: ViewMode = ViewMode.View;

    constructor(
        private service: InviteCardEditorV1Service,
        private toolbar: InviteCardEditorV1ToolbarService,
        private svgEditor: InviteCardEditorV1SvgEditorService,
    ) {
        this.update();
    }

    $onInit(): void {
        this.service.view.id = 'hiInviteCardEditorViewPort' + randomId(8);

        this.toolbar.changeSubject.subscribe(event => {
            this.update();
        });
    }

    $onDestroy(): void {
        this.service.view.id = undefined;
    }

    getContainerId(): string {
        return this.service.view.id;
    }

    update() {
        switch(this.toolbar.getCurrent()) {
            default:
                this.viewMode = ViewMode.View;
                break;

            case InviteCardEditorV1Tool.SvgEditor:
                this.viewMode = ViewMode.TextEditor;
                break;

            case InviteCardEditorV1Tool.AddPhoto:
                this.viewMode = ViewMode.PhotoContainer;
                break;
        }
    }

    isViewMode(viewMode: ViewMode): boolean {
        return this.viewMode === viewMode;
    }

    isLoading(): boolean {
        return this.service.view.status.isLoading();
    }

    isEditModeEnabled(): boolean {
        return this.toolbar.is(InviteCardEditorV1Tool.SvgEditor);
    }

    getBackdropImageURL(): string {
        return this.service.model.model.backdrop.link.url;
    }

    getContainerCSS(): any {
        let size = this.service.model.model.size.entity;

        return {
            width: size.width + 'px',
            height: size.height + 'px',
        };
    }

    hasPhoto(): boolean {
        return this.service.model.model.photo.enabled;
    }

    shouldShowPhotoContainerBoundary(): boolean {
        return this.svgEditor.mode === SvgEditorMode.Editing
            && this.toolbar.getCurrent() === InviteCardEditorV1Tool.AddPhoto;
    }

    getSvgMapContainerId(): string {
        return InviteCardEditorV1Viewport.SVG_MAP_ELEMENT_ID;
    }
}