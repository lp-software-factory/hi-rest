import {InviteCardTemplate} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {InviteCardFamily} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {SvgEditorConfig} from "../component/InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";

export interface InviteCardEditorV1EditorConfig
{
    source: {
        template: {
            current: InviteCardTemplate,
            original?: InviteCardTemplate,
        },
        family: {
            current: InviteCardFamily,
            original?: InviteCardFamily,
        },
    },
    toolbars: InviteCardEditorV1ToolbarConfig,
    svg_editor: SvgEditorConfig,
}

export interface InviteCardEditorV1ToolbarConfig
{
    "template-color": {
        enabled: boolean,
        features: {
            drag: boolean
            list: boolean,
            add: boolean,
        }
    },
    "svg-editor": {
        enabled: boolean,
    },
    "add-photo": {
        enabled: boolean,
    },
    "add-music": {
        enabled: boolean,
    },
    "revert-changes": {
        enabled: boolean,
    },
    "replace-family": {
        enabled: boolean,
    },
    "print": {
        enabled: boolean
    },
    "submit": {
        enabled: boolean
    },
    "preview": {
        enabled: boolean,
    }
}