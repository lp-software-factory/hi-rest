import {Service} from "../../../../../../decorators/Service";

import {LoadingManager} from "../../../../../common/helpers/LoadingManager";
import {AsyncImageLoaderService} from "../../../../../../service/AsyncImageLoaderService";

@Service({
    injects: [
        '$timeout',
        AsyncImageLoaderService,
    ]
})
export class InviteCardEditorV1PreviewService
{
    public status: LoadingManager = new LoadingManager();

    public error: string|undefined;
    public visible: boolean;
    public imageUrl: string;

    constructor(
        private $timeout: ng.ITimeoutService,
        private loader: AsyncImageLoaderService,
    ) {}

    open(imageUrl: string): void {
        this.error = undefined;
        this.visible = true;

        if(this.loader.isLoaded(imageUrl)) {
            this.imageUrl = imageUrl;
        }else{
            let loading = this.status.addLoading();

            this.loader.loadImage(imageUrl).subscribe(
                next => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.imageUrl = imageUrl;
                    });
                },
                error => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.error = '' + error;
                    });
                }
            );
        }
    }

    close(): void {
        this.visible = false;
    }
}