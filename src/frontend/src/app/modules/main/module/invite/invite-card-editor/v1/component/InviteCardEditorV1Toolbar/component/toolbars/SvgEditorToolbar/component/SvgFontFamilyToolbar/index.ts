import {Component} from "../../../../../../../../../../../decorators/Component";

import {AbstractSvgToolbar, SvgToolbar} from "../AbstractSvgToolbar";
import {ListWindow} from "../../../../../../../../../../../helpers/ListWindow";
import {Font} from "../../../../../../../../../../../../../../../definitions/src/definitions/font/entity/Font";
import {FontService} from "../../../../../../../../../../font/service/FontService";
import {InviteCardEditorV1SvgEditorService} from "../../../../../../InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";

@Component({
    selector: 'hi-invite-card-editor-v1-svg-font-family-toolbar',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1SvgEditorService,
        FontService,
    ]
})
export class InviteCardEditorV1SvgEditorFontFamilyToolbar implements AbstractSvgToolbar
{
    public static FONT_LIST_WINDOW_LENGTH = 6;

    private window: ListWindow<Font> = new ListWindow<Font>(InviteCardEditorV1SvgEditorFontFamilyToolbar.FONT_LIST_WINDOW_LENGTH);

    constructor(
        private svg: InviteCardEditorV1SvgEditorService,
        private fonts: FontService,
    ) {}

    $onInit(): void {
        this.window.items = this.fonts.available;
    }

    getId(): SvgToolbar {
        return SvgToolbar.FontFamily;
    }

    isActive(font: Font) {
        return this.svg.isFontFamilyActive(font.entity.title);
    }

    getFontPreview(font: Font): string {
        return font.entity.preview.link.url;
    }

    getFontPreviewAlt(font: Font): string {
        return font.entity.title;
    }

    click(font: Font) {
        this.svg.setFontFamily(font);
    }
}