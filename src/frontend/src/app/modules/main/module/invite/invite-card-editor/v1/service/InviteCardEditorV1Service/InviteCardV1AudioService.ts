import {Service} from "../../../../../../decorators/Service";

import {InviteCardEditorV1Service} from "../InviteCardEditorV1Service";
import {UIAlertModalService} from "../../../../../ui/service/UIAlertModalService";
import {AttachmentRESTService} from "../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {Observable} from "rxjs";
import {AttachmentEntity} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {UploadObservableProgress} from "../../../../../../../../../../definitions/src/functions/upload";
import {UploadAttachmentResponse200} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/paths/upload";
import {AudioAttachmentMetadata} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/AudioAttachmentMetadata";
import {basename} from "../../../../../common/functions/basename";

@Service({
    injects: [
        AttachmentRESTService,
        UIAlertModalService,
    ]
})
export class InviteCardV1AudioService
{
    public services: InviteCardEditorV1Service;


    public static MIME_TYPES = ['audio/mpeg', 'audio/mp3'];
    public static MAX_FILE_SIZE = 1024 /* b */ * 1024 /* kb */ * 10 /* Mb */;

    constructor(
        private rest: AttachmentRESTService,
        private alert: UIAlertModalService,
    ) {}

    uploadFiles($files: File[]): Observable<AttachmentEntity<AudioAttachmentMetadata>> {
        return Observable.create(uploadFilesObserver => {
            let observables = [];
            let validators: Observable<boolean>[] = [];

            if($files.length > 0) {
                for(let index = 0; index < $files.length; index++) {
                    validators.push(this.isValidFile($files[index]));
                }

                Observable.forkJoin(validators).subscribe(
                    success => {
                        if($files.length > 0) {
                            for(let index = 0; index < $files.length; index++) {
                                observables.push(this.rest.upload($files[index]));
                            }
                        }

                        observables.forEach(o => o.subscribe(
                            response => {
                                if(! (response instanceof UploadObservableProgress)) {
                                    this.attachAudio(response.entity);
                                    uploadFilesObserver.next(response.entity);
                                }
                            },
                            error => {
                                uploadFilesObserver.error(error);
                            }
                        ));

                        if(observables.length) {
                            Observable.forkJoin(observables).subscribe(
                                (responses: UploadAttachmentResponse200[]) => {
                                    uploadFilesObserver.complete();
                                },
                                error => {
                                    uploadFilesObserver.error(error);
                                }
                            )
                        }else{
                            uploadFilesObserver.complete();
                        }
                    },
                    error => {
                        this.raiseFileInputError();

                        uploadFilesObserver.error(error);
                    }
                );
            }else{
                uploadFilesObserver.complete();
            }
        }).share();
    }

    isValidFile(input: File):  Observable<boolean> {
        return Observable.create(observer => {
            let size = input.size;
            let type = input.type;

            let result = (
                (!!~InviteCardV1AudioService.MIME_TYPES.indexOf(type)) &&
                (size <= InviteCardV1AudioService.MAX_FILE_SIZE)
            );

            if(result) {
                observer.next(result);
                observer.complete();
            }else{
                observer.error();
            }
        });
    }

    raiseFileInputError() {
        this.alert.open({
            title: {
                value: 'hi.main.editor.invite-card.errors.audio-validation.title',
                translate: true,
            },
            text: {
                value: 'hi.main.editor.invite-card.errors.audio-validation.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.main.editor.invite-card.errors.audio-validation.close',
                        translate: true,
                    },
                    click: (modal) => {
                        modal.close();
                    }
                }
            ]
        })
    }

    getListUploadedAudios(): AttachmentEntity<AudioAttachmentMetadata>[] {
        return this.services.model.model.audio.uploaded;
    }

    getAudioTitle(attachment: AttachmentEntity<AudioAttachmentMetadata>): string {
        return basename(attachment.link.url);
    }

    attachAudio(attachment: AttachmentEntity<AudioAttachmentMetadata>): void {
        let config = this.services.model.model.audio;

        config.uploaded.push(attachment);

        if(config.uploaded.length && !config.attachment) {
            this.selectAudio(config.uploaded[0]);
        }
    }

    hasSelectedAudio(): boolean {
        return this.services.model.model.audio.attachment !== undefined;
    }

    getSelectedAudio():  AttachmentEntity<AudioAttachmentMetadata> {
        if(! this.hasSelectedAudio()) {
            throw new Error('No audio selected!');
        }else{
            return this.services.model.model.audio.attachment;
        }
    }

    selectAudio(attachment: AttachmentEntity<AudioAttachmentMetadata>): void {
        let config = this.services.model.model.audio;

        config.attachment = attachment;
    }

    detachAudio(detach: AttachmentEntity<AudioAttachmentMetadata>): void {
        let config = this.services.model.model.audio;

        config.uploaded = config.uploaded.filter(a => a.id !== detach.id);

        if(config.attachment.id === detach.id) {
            config.attachment = undefined;
        }

        if(config.uploaded.length && !config.attachment) {
            this.selectAudio(config.uploaded[0]);
        }
    }
}