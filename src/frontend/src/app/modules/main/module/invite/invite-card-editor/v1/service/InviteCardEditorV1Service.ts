import {Service} from "../../../../../decorators/Service";

import {ReplaySubject, Observable, Subject} from "rxjs";
import {InviteCardV1ModelService} from "./InviteCardEditorV1Service/InviteCardV1ModelService";
import {InviteCardV1RevertService} from "./InviteCardEditorV1Service/InviteCardV1RevertService";
import {InviteCardV1ViewPortService} from "./InviteCardEditorV1Service/InviteCardV1ViewPortService";
import {InviteCardV1AudioService} from "./InviteCardEditorV1Service/InviteCardV1AudioService";
import {InviteCardV1PhotoService} from "./InviteCardEditorV1Service/InviteCardV1PhotoService";
import {InviteCardEditorV1EditorConfig} from "./InviteCardEditorV1EditorConfig";
import {EditInviteCardTemplateRequest, EditInviteCardTemplateResponse200} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-template/paths/edit";
import {InviteCardTemplateRESTService} from "../../../../../../../../../definitions/src/services/invite-card/invite-card-template/InviteCardTemplateRESTService";
import {InviteCardTemplateGetResponse200} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-template/paths/get";
import {UploadImageInviteCardTemplateResponse200} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-template/paths/image-upload";
import {ImageEntityCollection} from "../../../../../../../../../definitions/src/definitions/avatar/entity/ImageEntityCollection";
import {UploadObservableLastOnlyFactory} from "../../../../../../../../../definitions/src/functions/upload";
import {InviteCardEditorV1Tool, InviteCardEditorV1ToolbarService} from "../component/InviteCardEditorV1Toolbar/service";
import {InviteCardEditorV1PreviewService} from "../component/InviteCardEditorV1Preview/service";

let html2canvas: Html2CanvasStatic = require('html2canvas');

@Service({
    injects: [
        '$timeout',
        InviteCardTemplateRESTService,
        InviteCardV1ModelService,
        InviteCardV1AudioService,
        InviteCardV1PhotoService,
        InviteCardV1RevertService,
        InviteCardEditorV1ToolbarService,
        InviteCardV1ViewPortService,
        InviteCardEditorV1PreviewService,
    ]
})
export class InviteCardEditorV1Service
{
    public editorConfig: InviteCardEditorV1EditorConfig;

    public isReady: boolean = false;

    public saveSubject: Subject<InviteCardTemplateGetResponse200> = new Subject<InviteCardTemplateGetResponse200>();
    public ready: ReplaySubject<InviteCardEditorV1Service> = new ReplaySubject<InviteCardEditorV1Service>();

    constructor(
        private $timeout: ng.ITimeoutService,
        private rest: InviteCardTemplateRESTService,

        public model: InviteCardV1ModelService,
        public audio: InviteCardV1AudioService,
        public photo: InviteCardV1PhotoService,
        public revert: InviteCardV1RevertService,
        public toolbar: InviteCardEditorV1ToolbarService,
        public view: InviteCardV1ViewPortService,
        public preview: InviteCardEditorV1PreviewService,
    ) {
        model.services = this;
        audio.services = this;
        photo.services = this;
        revert.services = this;
        toolbar.editor = this;
        view.services = this;

        this.isReady = true;
        this.ready.next(this);
    }

    setup(config: InviteCardEditorV1EditorConfig) {
        this.editorConfig = config;

        let template = config.source.template.current;
        let family = config.source.family.original;

        this.model.load(template, family, config);
    }

    save(): Observable<EditInviteCardTemplateResponse200> {
        return Observable.create(observer => {
            this.$timeout(() => {
                let familyId = this.model.current.entity.family_id;
                let templateId = this.model.current.entity.id;

                let preview: ImageEntityCollection;

                let imageObservable = this.uploadPreviewImage(familyId, templateId);

                imageObservable.subscribe(next => preview = next.image);

                Observable.forkJoin([imageObservable]).subscribe(
                    next => {},
                    error => {
                        this.$timeout(() => {
                            observer.error(error);
                            observer.complete();
                        });
                    },
                    () => {
                        this.$timeout(() => {
                            let viewBlock = this.view.status.addLoading();
                            let request = this.createEditInviteCardTemplateRequest();

                            this.rest.editInviteCardTemplate(familyId, templateId, request).subscribe(
                                next => {
                                    this.$timeout(() => {
                                        viewBlock.is = false;

                                        this.saveSubject.next(next);

                                        observer.next(next);
                                        observer.complete();
                                    });
                                },
                                error => {
                                    this.$timeout(() => {
                                        viewBlock.is = false;

                                        observer.error(error);
                                        observer.complete();
                                    });
                                }
                            );
                        });
                    }
                );
            });
        }).share();
    }

    uploadPreviewImage(familyId: number, templateId: number): Observable<UploadImageInviteCardTemplateResponse200> {
        return Observable.create(observer => {
            this.createPreviewImage().subscribe(
                file => {
                    let viewBlock = this.view.status.addLoading();

                    let upload = (new UploadObservableLastOnlyFactory<UploadImageInviteCardTemplateResponse200>()).create(
                        this.rest.uploadImageInviteCardTemplate(familyId, templateId, {
                            file: file,
                            crop: {
                                x1: 0,
                                y1: 0,
                                x2: -1,
                                y2: -1,
                            }
                        })
                    );

                    upload.subscribe(
                        next => {
                            viewBlock.is = false;

                            observer.next(next);
                            observer.complete();
                        },
                        error => {
                            viewBlock.is = false;

                            observer.error(error);
                            observer.complete();
                        }
                    )
                },
                error => {
                    observer.error(error);
                    observer.complete();
                }
            )
        });
    }

    createPreviewImage(): Observable<Blob> {
        return Observable.create(observer => {
            let prevToolbar = this.toolbar.getCurrent();

            this.toolbar.change(InviteCardEditorV1Tool.TemplateColor);

            this.$timeout(() => {
                let element = window.document.getElementById(this.view.id);

                html2canvas(element)
                    .then(data => {
                        if(data.toBlob) {
                            data.toBlob(result => {
                                this.$timeout(() => {
                                    this.toolbar.change(prevToolbar);

                                    observer.next(result);
                                    observer.complete();
                                });
                            })
                        }else{
                            this.$timeout(() => {
                                let imageUrl = data.toDataURL('image/png');

                                this.toolbar.change(prevToolbar);

                                observer.next(this.dataURItoBlob(imageUrl));
                                observer.complete();
                            });
                        }
                    })
                    .catch(error => {
                        this.$timeout(() => {
                            this.toolbar.change(prevToolbar);

                            observer.error(error);
                            observer.complete();
                        });
                    });
            });
        }).share();
    }

    /** http://stackoverflow.com/questions/12168909/blob-from-dataurl **/
    dataURItoBlob(dataURI): Blob {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        let byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

        // write the bytes of the string to an ArrayBuffer
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        for(let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // write the ArrayBuffer to a blob, and you're done
        let blob = new Blob([ab], {type: mimeString});
        return blob;
    }

    createEditInviteCardTemplateRequest(): EditInviteCardTemplateRequest {
        let model = this.model.model;
        let attachmentIds: number[] = [];

        let request: EditInviteCardTemplateRequest = {
            template: {
                title: model.template.entity.title,
                description: model.template.entity.description,
                color: {
                    title: model.template.entity.color.title,
                    hex_code: model.template.entity.color.hex_code,
                },
                photo: {
                    enabled: model.photo.enabled,
                },
                backdrop: model.backdrop.id,
                config: {
                    audio: {
                        attachment: model.audio.attachment,
                        uploaded: model.audio.uploaded,
                    },
                    document: model.document,
                },
                attachment_ids: attachmentIds
            }
        };

        model.audio.uploaded.forEach(a => attachmentIds.push(a.id));

        if(model.photo.enabled) {
            request.template.photo.placeholder = model.template.entity.photo.placeholder.id;
            request.template.photo.container = model.template.entity.photo.container;

            request.template.config.photo = {
                attachment: model.photo.attachment || model.photo.placeholder,
                uploaded: model.photo.uploaded,
                container: model.photo.container,
            };

            model.photo.uploaded.forEach(a => attachmentIds.push(a.id));
        }

        return request;
    }
}