import {ISvgEditorStrategy, InviteCardEditorV1SvgEditor} from "../index";
import {InviteCardEditorV1SvgEditorService} from "../service/InviteCardEditorV1SvgEditorService";

export class SvgEditorEditStrategy implements ISvgEditorStrategy
{
    constructor(
        private component: InviteCardEditorV1SvgEditor,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}
}