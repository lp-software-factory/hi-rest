import {Service} from "../../../../../../decorators/Service";

import {InviteCardEditorV1Service} from "../InviteCardEditorV1Service";
import {InviteCardEditorV1ToolbarService} from "../../component/InviteCardEditorV1Toolbar/service";

@Service()
export class InviteCardV1ToolbarService
{
    public services: InviteCardEditorV1Service;

    constructor(
        public toolbars: InviteCardEditorV1ToolbarService,
    ) {}
}