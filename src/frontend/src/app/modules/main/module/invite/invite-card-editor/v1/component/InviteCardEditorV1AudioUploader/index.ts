import {Component} from "../../../../../../decorators/Component";

import {LoadingManager} from "../../../../../common/helpers/LoadingManager";
import {ListWindow} from "../../../../../../helpers/ListWindow";
import {AttachmentEntity} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {AudioAttachmentMetadata} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/AudioAttachmentMetadata";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-invite-card-editor-v1-audio-uploader',
    template: require('./template.jade'),
    styles: [
        require('./resources/styles/style.shadow.scss'),
        require('./resources/styles/style.marquee.shadow.scss'),
    ],
    injects: [
        '$timeout',
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1AudioUploader implements ng.IComponentController
{
    public static MAX_WINDOW_AUDIOS = 5;

    private dragOver: boolean = false;
    private status: LoadingManager = new LoadingManager();
    private audios: ListWindow<AttachmentEntity<AudioAttachmentMetadata>> = new ListWindow<AttachmentEntity<AudioAttachmentMetadata>>(InviteCardEditorV1AudioUploader.MAX_WINDOW_AUDIOS);

    constructor(
        private $timeout: angular.ITimeoutService,
        private service: InviteCardEditorV1Service,
    ) {
        this.updateListWindow();
    }

    $onInit(): void {
        if(this.service.audio.hasSelectedAudio()) {
            this.audios.scrollTo(this.service.audio.getSelectedAudio(), (item, compare) => {
                return item.id === compare.id;
            });
        }

        this.updateListWindow();
    }

    $doCheck(): void {
        this.updateListWindow();
    }

    hasAnyAudio(): boolean {
        this.audios.items = this.service.audio.getListUploadedAudios();

        return this.audios.length() > 0;
    }

    isAudioActive(audio: AttachmentEntity<AudioAttachmentMetadata>): boolean {
        return this.service.audio.getSelectedAudio().id === audio.id;
    }

    selectAudio(audio: AttachmentEntity<AudioAttachmentMetadata>): void {
        this.service.audio.selectAudio(audio);
    }

    getAudioTitle(audio: AttachmentEntity<AudioAttachmentMetadata>): string {
        return this.service.audio.getAudioTitle(audio);
    }

    deleteAudio(audio: AttachmentEntity<AudioAttachmentMetadata>): void {
        this.service.audio.detachAudio(audio);
        this.updateListWindow();
    }

    updateListWindow(): void {
        this.audios.items = this.service.audio.getListUploadedAudios();
    }

    onDragOut(): void {
        this.$timeout(() => {
            this.dragOver = false;
        });
    }

    onDragOver(): void {
        this.$timeout(() => {
            this.dragOver = true;
        });
    }

    onDropFiles($files: File[]): void {
        this.dragOver = false;
        this.uploadFiles($files);
    }

    onFileChange($files: FileList): void {
        let files = [];

        for(let index = 0; index < $files.length; index++) {
            files.push($files[index]);
        }

        this.uploadFiles(files);
    }

    uploadFiles($files: File[]): void {
        this.$timeout(() => {
            let loading = this.status.addLoading();

            this.service.audio.uploadFiles($files).subscribe(
                success => {},
                error => {
                    this.$timeout(() => {
                        loading.is = false;
                    });
                },
                () => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.updateListWindow();
                    })
                }
            )
        });
    }
}