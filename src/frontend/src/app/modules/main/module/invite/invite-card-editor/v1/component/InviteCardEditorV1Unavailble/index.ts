import {Component} from "../../../../../../decorators/Component";

@Component({
    selector: 'hi-invite-card-editor-v1-unavailable',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ]
})
export class InviteCardEditorV1Unavailable implements ng.IComponentController
{

}