import {ISvgEditorStrategy, InviteCardEditorV1SvgEditor} from "../index";
import {SvgMapElement} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";
import {InviteCardEditorV1SvgEditorService} from "../service/InviteCardEditorV1SvgEditorService";

interface Point { x: number, y: number }

export class SvgEditorRotateStrategy implements ISvgEditorStrategy
{
    private hovered: SvgMapElement;
    private rotateEnabled: boolean = false;
    private from: Point = { x: 0, y: 0 };

    constructor(
        private component: InviteCardEditorV1SvgEditor,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    onElementClick(event: MouseEvent, element: SvgMapElement): void {
        this.svg.cursor.select(element);
    }

    onMouseElementOut(event: MouseEvent, element: SvgMapElement): void { this.hovered = undefined; }
    onMouseElementOver(event: MouseEvent, element: SvgMapElement): void { this.hovered = element; }

    mouseUp(event: MouseEvent): void {
        this.rotateEnabled = false;
    }

    mouseDown(event: MouseEvent, element?: SvgMapElement): void {
        if(this.hovered) {
            this.rotateEnabled = true;
            this.svg.cursor.select(this.hovered);
            this.from = { x: event.screenX, y: event.screenY }
        }
    }

    mouseMove(event: MouseEvent): void {
        if(this.svg.cursor.has() && this.rotateEnabled) {
            this.svg.cursor.getCurrent().container.rotate = Math.max(1, event.screenY - this.from.y);
        }
    }
}