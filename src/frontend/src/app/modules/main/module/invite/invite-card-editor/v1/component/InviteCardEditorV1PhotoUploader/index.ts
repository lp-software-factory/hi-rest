import {Component} from "../../../../../../decorators/Component";

import {ListWindow} from "../../../../../../helpers/ListWindow";
import {AttachmentEntity} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {LoadingManager} from "../../../../../common/helpers/LoadingManager";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";

@Component({
    selector: 'hi-invite-card-editor-v1-photo-uploader',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        InviteCardEditorV1Service,
    ]
})
export class InviteCardEditorV1PhotoUploader implements ng.IComponentController
{
    public static MAX_VIEW_IMAGES = 5;
    public static BASE_PREVIEW_HEIGHT = 56;

    private dragOver: boolean = false;
    private status: LoadingManager = new LoadingManager();
    private photos: ListWindow<AttachmentEntity<ImageAttachmentMetadata>> = new ListWindow<AttachmentEntity<ImageAttachmentMetadata>>(InviteCardEditorV1PhotoUploader.MAX_VIEW_IMAGES);

    constructor(
        private $timeout: ng.ITimeoutService,
        private service: InviteCardEditorV1Service,
    ) {
        this.updateListWindow();
    }

    $onInit(): void {
        if(this.service.photo.hasSelectedPhoto()) {
            this.photos.scrollTo(this.service.photo.getSelectedPhoto(), (item, compare) => {
                return item.id === compare.id;
            });
        }

        this.updateListWindow();
    }

    $doCheck(): void {
        this.updateListWindow();
    }

    hasManyPhotos(): boolean {
        this.photos.items = this.service.photo.getListUploadedPhotos();

        return this.photos.length() > 0;
    }

    isPhotoActive(photo: AttachmentEntity<ImageAttachmentMetadata>): boolean {
        return this.service.photo.getSelectedPhoto() === photo;
    }

    selectPhoto(photo: AttachmentEntity<ImageAttachmentMetadata>): void {
        this.service.photo.selectPhoto(photo);
    }

    deletePhoto(photo: AttachmentEntity<ImageAttachmentMetadata>) {
        this.service.photo.detachPhoto(photo);
        this.updateListWindow();
    }

    getPhotoPreviewCSS(photo: AttachmentEntity<ImageAttachmentMetadata>): any {
        let container = this.service.photo.getOriginalPlaceholderPhotoContainer();

        let width = Math.abs(container.end.x - container.start.x);
        let height = Math.abs(container.end.y - container.start.y);

        return {
            "background-image": `url("${encodeURI(photo.link.metadata.preview)}")`,
            "width": ((width / height) * InviteCardEditorV1PhotoUploader.BASE_PREVIEW_HEIGHT) + "px",
        };
    }

    updateListWindow(): void {
        this.photos.items = this.service.photo.getListUploadedPhotos();
    }

    onDragOut(): void {
        this.$timeout(() => {
            this.dragOver = false;
        });
    }

    onDragOver(): void {
        this.$timeout(() => {
            this.dragOver = true;
        });
    }

    onDropFiles($files: File[]): void {
        this.dragOver = false;
        this.uploadFiles($files);
    }

    onFileChange($files: FileList): void {
        let files = [];

        for(let index = 0; index < $files.length; index++) {
            files.push($files[index]);
        }

        this.uploadFiles(files);
    }

    uploadFiles($files: File[]): void {
        let loading = this.status.addLoading();

        this.service.photo.uploadFiles($files).subscribe(
            success => {},
            error => {
                this.$timeout(() => {
                    loading.is = false;
                });
            },
            () => {
                this.$timeout(() => {
                    loading.is = false;

                    this.updateListWindow();
                })
            }
        );
    }
}