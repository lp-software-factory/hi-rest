import {Service} from "../../../../../../decorators/Service";

import {InviteCardEditorV1Service} from "../InviteCardEditorV1Service";
import {UIAlertModalService} from "../../../../../ui/service/UIAlertModalService";

@Service({
    injects: [
        '$timeout',
        UIAlertModalService,
    ]
})
export class InviteCardV1RevertService
{
    public services: InviteCardEditorV1Service;

    constructor(
        private $timeout: ng.ITimeoutService,
        private alert: UIAlertModalService,
    ) {}

    askForRevertChanges(): void {
        this.alert.open({
            title: {
                value: "hi.main.editor.invite-card.revert-changes.confirm.title",
                translate: true,
            },
            text: {
                value: "hi.main.editor.invite-card.revert-changes.confirm.text",
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: "hi.main.editor.invite-card.revert-changes.confirm.buttons.ok",
                        translate: true,
                    },
                    click: modal => {
                        this.revertChanges();
                        modal.close();
                    }
                },
                {
                    title: {
                        value: "hi.main.editor.invite-card.revert-changes.confirm.buttons.cancel",
                        translate: true,
                    },
                    click: modal => {
                        modal.close();
                    }
                }
            ]
        });
    }

    revertChanges(): void {
        this.services.model.useTemplate(this.services.model.current).subscribe(() => {
            this.$timeout(() => {});
        });
    }
}