import {Component} from "../../../../../../decorators/Component";

import {InviteCardEditorV1PreviewService} from "./service";
import {LoadingManager} from "../../../../../common/helpers/LoadingManager";

@Component({
    selector: 'hi-invite-card-editor-v1-preview',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        InviteCardEditorV1PreviewService,
    ]
})
export class InviteCardEditorV1Preview
{
    constructor(
        public service: InviteCardEditorV1PreviewService,
    ) {}

    isVisible(): boolean {
        return this.service.visible;
    }

    hasImage(): boolean {
        return !!this.service.imageUrl;
    }

    getImageURL(): string {
        return this.service.imageUrl;
    }

    getStatus(): LoadingManager {
        return this.service.status;
    }

    close(): void {
        this.service.close();
    }
}