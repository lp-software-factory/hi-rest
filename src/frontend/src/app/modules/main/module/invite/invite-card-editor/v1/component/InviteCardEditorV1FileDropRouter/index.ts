import {Component} from "../../../../../../decorators/Component";

import {Observable, Subscription} from "rxjs";
import {InviteCardEditorV1ToolbarService, InviteCardEditorV1Tool} from "../InviteCardEditorV1Toolbar/service";
import {UIAlertModalService} from "../../../../../ui/service/UIAlertModalService";
import {LoadingManager} from "../../../../../common/helpers/LoadingManager";
import {isDragSourceExternalFile} from "../../../../../../functions/isDragSourceExternalFile";
import {InviteCardEditorV1Service} from "../../service/InviteCardEditorV1Service";
import {InviteCardV1AudioService} from "../../service/InviteCardEditorV1Service/InviteCardV1AudioService";
import {InviteCardV1PhotoService} from "../../service/InviteCardEditorV1Service/InviteCardV1PhotoService";

export interface InviteCardEditorV1FileDropRouterRequest
{
    photos: File[],
    audios: File[],
}

export enum InviteCardEditorV1FileDropRouterVariant
{
    Common = <any>"common",
    Audio = <any>"audio",
    Photo = <any>"photo",
}

@Component({
    selector: 'hi-invite-card-editor-v1-file-drop-router',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
        require('./loading.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$document',
        InviteCardEditorV1Service,
        InviteCardEditorV1ToolbarService,
        UIAlertModalService
    ]
})
export class InviteCardEditorV1FileDropRouter implements ng.IComponentController
{
    private bindings: {
        dragstart?: (ev: DragEvent) => void,
        dragover?: (ev: DragEvent) => void,
        dragleave?: (ev: DragEvent) => void,
        dragenter?: (ev: DragEvent) => void,
        dragend?: (ev: DragEvent) => void,
        dragexit?: (ev: DragEvent) => void,
        drop?: (ev: DragEvent) => void,
    };

    private error: boolean = false;
    private counter: number = 0;
    private status: LoadingManager = new LoadingManager();
    private dragging: boolean = false;
    private hovered: boolean = false;

    private total: number = 0;
    private uploaded: number = 0;

    private variant: InviteCardEditorV1FileDropRouterVariant = InviteCardEditorV1FileDropRouterVariant.Common;

    private subscription: Subscription;

    constructor(
        private $timeout: ng.ITimeoutService,
        private $document: ng.IDocumentService,
        private service: InviteCardEditorV1Service,
        private toolbars: InviteCardEditorV1ToolbarService,
        private alert: UIAlertModalService,
    ) {}

    update(): void {
        switch(this.toolbars.getCurrent()) {
            default:
                this.variant = InviteCardEditorV1FileDropRouterVariant.Common;
                break;

            case InviteCardEditorV1Tool.AddPhoto:
                this.variant = InviteCardEditorV1FileDropRouterVariant.Photo;
                break;

            case InviteCardEditorV1Tool.AddMusic:
                this.variant = InviteCardEditorV1FileDropRouterVariant.Audio;
                break;

        }
    }

    $onInit(): void {
        this.bindings = {
            dragenter: (ev: DragEvent) => {
                ev.preventDefault();
                ev.stopPropagation();

                if (!this.error && (this.counter++ === 0)) {
                    this.$timeout(() => {
                        this.dragging = true;
                        this.hovered = true;
                        this.update();
                    });
                }
            },
            dragleave: (ev: DragEvent) => {
                ev.preventDefault();
                ev.stopPropagation();

                if (! this.error && (--this.counter === 0)) {
                    this.$timeout(() => {
                        this.dragging = false;
                        this.hovered = false;
                    });
                }
            },
            dragover: (ev: DragEvent) => {
                ev.preventDefault();
                ev.stopPropagation();
            },
            drop: (evt: DragEvent) => {
                evt.preventDefault();
                evt.stopPropagation();

                if(! isDragSourceExternalFile(evt.dataTransfer)) {
                    this.$timeout(() => {
                        this.dragging = false;
                        this.hovered = false;
                        return;
                    });
                }

                let observables: Observable<File>[] = [];
                let loading = this.status.addLoading();

                let files = evt.dataTransfer.files;
                for(let i = 0, f; f = files[i]; i++) {
                    let reader = new FileReader();

                    observables.push(Observable.create(observer => {
                        reader.readAsArrayBuffer(f);
                        reader.onload = (function(theFile) {
                            return function(e) {
                                observer.next(theFile);
                                observer.complete();
                            };
                        })(f);
                    }));
                }

                if(observables.length) {
                    Observable.forkJoin(observables).subscribe((results: File[]) => {
                        this.$timeout(() => {
                            this.counter = 0;
                            this.dragging = false;
                            this.hovered = false;
                        });

                        if(this.toolbars.is(InviteCardEditorV1Tool.AddMusic)) {
                            this.$timeout(() => {
                                this.uploaded = 0;
                                this.total = results.length;
                            });

                            this.subscription = this.service.audio.uploadFiles(results).subscribe(
                                next => {
                                    this.$timeout(() => {
                                        ++this.uploaded;
                                    });
                                },
                                error => {
                                    this.$timeout(() => {
                                        loading.is = false;
                                    });
                                },
                                () => {
                                    this.$timeout(() => {
                                        loading.is = false;
                                    });
                                }
                            );
                        }else if(this.toolbars.is(InviteCardEditorV1Tool.AddPhoto)) {
                            this.$timeout(() => {
                                this.uploaded = 0;
                                this.total = results.length;
                            });

                            this.subscription = this.service.photo.uploadFiles(results).subscribe(
                                next => {
                                    this.$timeout(() => {
                                        ++this.uploaded;
                                    });
                                },
                                error => {
                                    this.$timeout(() => {
                                        loading.is = false;
                                    });
                                },
                                () => {
                                    this.$timeout(() => {
                                        loading.is = false;
                                    });
                                }
                            );
                        }else{
                            let request: InviteCardEditorV1FileDropRouterRequest = {
                                audios: results.filter(
                                    input => !!~InviteCardV1AudioService.MIME_TYPES.indexOf(input.type)),
                                photos: results.filter(
                                    input => !!~InviteCardV1PhotoService.MIME_TYPES.indexOf(input.type)),
                            };

                            let done = 0;
                            let total = 0;
                            for(let n in request) {
                                total += request[n].length;
                            }

                            if(total != results.length) {
                                this.error = true;
                                loading.is = false;

                                this.alert.open({
                                    title: {
                                        value: 'hi.main.editor.invite-card.drop-zone.error.title',
                                        translate: true,
                                    },
                                    text: {
                                        value: 'hi.main.editor.invite-card.drop-zone.error.text',
                                        translate: true,
                                    },
                                    buttons: [
                                        {
                                            title: {
                                                value: 'hi.main.editor.invite-card.drop-zone.error.close',
                                                translate: true,
                                            },
                                            click: modal => {
                                                modal.close();
                                                this.error = false;
                                            },
                                        }
                                    ]
                                })
                            }else{
                                let observables: Observable<any>[] = [
                                    this.service.audio.uploadFiles(request.audios),
                                    this.service.photo.uploadFiles(request.photos),
                                ];

                                this.$timeout(() => {
                                    this.uploaded = 0;
                                    this.total = total;
                                });

                                this.subscription = Observable.merge(...observables).subscribe(
                                    next => {
                                        this.$timeout(() => {
                                            ++this.uploaded;
                                        });
                                    },
                                    error => {
                                        this.$timeout(() => {
                                            loading.is = false;
                                        });
                                    },
                                    () => {
                                        this.$timeout(() => {
                                            loading.is = false;

                                            if((request.audios.length > 0) && (request.audios.length > request.photos.length)) {
                                                this.toolbars.change(InviteCardEditorV1Tool.AddMusic);
                                                this.$timeout(() => {}, 200);
                                            }else if((request.photos.length > 0) && (request.photos.length > request.audios.length)) {
                                                this.toolbars.change(InviteCardEditorV1Tool.AddPhoto);
                                                this.$timeout(() => {}, 200);
                                            }
                                        });
                                    }
                                )
                            }
                        }

                        this.$timeout(() => {});
                    });
                }else{
                    loading.is = false;
                }
            }
        };


        for(let ev in this.bindings) {
            window.document.addEventListener(ev, this.bindings[ev]);
        }
    }

    is(variant: InviteCardEditorV1FileDropRouterVariant): boolean {
        return this.variant === variant;
    }

    cancel(): void {
        this.status.clearLoading();

        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    $onDestroy(): void {
        for(let ev in this.bindings) {
            window.document.removeEventListener(ev, this.bindings[ev]);
        }
    }

    isHovered(): boolean {
        return this.hovered;
    }

    isDragging(): boolean {
        return this.dragging;
    }
}