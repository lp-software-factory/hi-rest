export enum SvgToolbar
{
    FontFamily = <any>"font-family",
    FontSize = <any>"font-size",
    TextColor = <any>"text-color",
    TextBackground = <any>"text-background",
}

export interface AbstractSvgToolbar extends ng.IComponentController
{
    getId(): SvgToolbar;
}