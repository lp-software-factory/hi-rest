import {Component} from "../../../../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {AbstractSvgToolbar, SvgToolbar} from "../AbstractSvgToolbar";
import {ListWindow} from "../../../../../../../../../../../helpers/ListWindow";
import {InviteCardEditorV1SvgEditorService} from "../../../../../../InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";
import {SvgMapElementType, SvgMapInput} from "../../../../../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

@Component({
    selector: 'hi-invite-card-editor-v1-svg-font-size-toolbar',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        InviteCardEditorV1SvgEditorService,
    ]
})
export class InviteCardEditorV1SvgEditorFontSizeToolbar implements AbstractSvgToolbar
{
    public static WINDOW_LENGTH = 11;

    public window: ListWindow<number> = new ListWindow<number>(InviteCardEditorV1SvgEditorFontSizeToolbar.WINDOW_LENGTH);

    private subscription: Subscription;

    constructor(
        private $timeout: ng.ITimeoutService,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    $onInit(): void {
        this.window.items = this.svg.listAvailableFontSizes();

        this.subscription = this.svg.events.autoResizeSubject.subscribe(next => {
            this.$timeout(() => {
                this.window.scrollTo(next, (item, compare) => item === compare);
            });
        });

        if(this.svg.cursor.has()) {
            let elem = <SvgMapInput>this.svg.cursor.getCurrent();

            if(elem.type === SvgMapElementType.Text) {
                this.window.scrollTo(elem.font.size, (item, compare) => item === compare);
            }
        }
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    getId(): SvgToolbar {
        return SvgToolbar.FontSize;
    }

    isActive(size: number) {
        return this.svg.isFontSizeActive(size);
    }

    click(size: number) {
        this.svg.setFontSize(size);
    }
}