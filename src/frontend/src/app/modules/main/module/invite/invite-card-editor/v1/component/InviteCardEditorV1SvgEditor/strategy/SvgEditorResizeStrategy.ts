import {ISvgEditorStrategy, InviteCardEditorV1SvgEditor} from "../index";
import {InviteCardEditorV1SvgEditorService} from "../service/InviteCardEditorV1SvgEditorService";
import {SvgMapElement} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

interface Size { x: number, y: number, width: number, height: number }
interface Point { x: number, y: number }

export class SvgEditorResizeStrategy implements ISvgEditorStrategy
{
    private hovered: SvgMapElement;
    private resizeEnabled: boolean = false;
    private start: Size = { x: 0, y: 0, width: 0, height: 0 };
    private from: Point = { x: 0, y: 0 };

    constructor(
        private component: InviteCardEditorV1SvgEditor,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    onElementClick(event: MouseEvent, element: SvgMapElement): void {
        this.svg.cursor.select(element);
    }

    onMouseElementOut(event: MouseEvent, element: SvgMapElement): void { this.hovered = undefined; }
    onMouseElementOver(event: MouseEvent, element: SvgMapElement): void { this.hovered = element; }

    mouseUp(event: MouseEvent): void {
        this.resizeEnabled = false;
    }

    mouseDown(event: MouseEvent, element?: SvgMapElement): void {
        if(this.hovered) {
            this.resizeEnabled = true;
            this.svg.cursor.select(this.hovered);
            this.start = { x: this.hovered.container.x, y: this.hovered.container.y, width: this.hovered.container.width, height: this.hovered.container.height };
            this.from = { x: event.screenX, y: event.screenY }
        }
    }

    mouseMove(event: MouseEvent): void {
        if(this.svg.cursor.has() && this.resizeEnabled) {
            this.svg.cursor.getCurrent().container.width = Math.max(1, event.screenX - this.from.x + this.start.width);
            this.svg.cursor.getCurrent().container.height = Math.max(1, event.screenY - this.from.y + this.start.height);
        }
    }
}