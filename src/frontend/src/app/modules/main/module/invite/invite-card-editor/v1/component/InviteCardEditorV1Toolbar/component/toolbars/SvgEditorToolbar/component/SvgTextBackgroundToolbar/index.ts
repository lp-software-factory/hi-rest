import {Component} from "../../../../../../../../../../../decorators/Component";

import {AbstractSvgToolbar, SvgToolbar} from "../AbstractSvgToolbar";
import {InviteCardEditorV1SvgEditorService} from "../../../../../../InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";

@Component({
    selector: 'hi-invite-card-editor-v1-svg-text-background-toolbar',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1SvgEditorService,
    ]
})
export class InviteCardEditorV1SvgEditorTextBackgroundToolbar implements AbstractSvgToolbar
{
    constructor(
        private service: InviteCardEditorV1SvgEditorService,
    ) {}

    getId(): SvgToolbar {
        return SvgToolbar.TextBackground;
    }
}