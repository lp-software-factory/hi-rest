import {Component} from "../../../../../../../../../decorators/Component";

import {AbstractToolbar} from "../AbstractToolbar";
import {InviteCardEditorV1Tool} from "../../../service";
import {InviteCardEditorV1SvgEditorService, SvgEditorMode, SvgEditorConfig, SvgEditorFeatures} from "../../../../InviteCardEditorV1SvgEditor/service/InviteCardEditorV1SvgEditorService";
import {InviteCardEditorV1Service} from "../../../../../service/InviteCardEditorV1Service";

export enum SvgEditorTools
{
    FontFamily = <any>"font-family",
    FontSize = <any>"font-size",
    TextColor = <any>"text-color",
    TextAlignLeft = <any>"text-align-left",
    TextAlignCenter = <any>"text-align-center",
    TextAlignRight = <any>"text-align-right",
    TextStyleBold = <any>"text-style-bold",
    TextStyleItalic = <any>"text-style-italic",
    TextStyleUnderline = <any>"text-style-underline",
}

export enum SvgEditorToolbars
{
    FontFamily = <any>"font-family",
    FontSize = <any>"font-size",
    TextColor = <any>"text-color",
    TextBackground = <any>"text-background",
}

@Component({
    selector: 'hi-invite-card-editor-v1-toolbars-svg-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
        require('./style-icons.shadow.scss'),
    ],
    injects: [
        InviteCardEditorV1Service,
        InviteCardEditorV1SvgEditorService,
    ]
})
export class InviteCardEditorV1SvgEditorToolbar implements AbstractToolbar
{
    private sub: SvgEditorToolbars = SvgEditorToolbars.FontFamily;

    constructor(
        private service: InviteCardEditorV1Service,
        private svg: InviteCardEditorV1SvgEditorService,
    ) {}

    $onDestroy(): void {
        this.svg.mode = SvgEditorMode.Editing;
    }
    
    getId(): InviteCardEditorV1Tool {
        return InviteCardEditorV1Tool.SvgEditor;
    }

    getConfig(): SvgEditorConfig {
        return this.service.editorConfig.svg_editor;
    }

    isToolEnabled(id: SvgEditorTools): boolean {
        return this.getConfig().tools[<any>id] === true;
    }

    isModeEnabled(id: SvgEditorMode): boolean {
        return this.svg.mode === id;
    }

    setMode(id: SvgEditorMode): void {
        this.svg.mode = id;
    }

    isToolbarEnabled(id: SvgEditorToolbars): boolean {
        return this.sub === id;
    }

    enableToolbar(id: SvgEditorToolbars) {
        this.sub = id;
    }

    hasFeature(id: SvgEditorFeatures) {
        return this.getConfig().features[<any>id] === true;
    }
}