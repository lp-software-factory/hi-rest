import {Service} from "../../../../../../../decorators/Service";

import {Subject} from "rxjs";
import {randomId} from "../../../../../../../functions/randomId";
import {Font, FONT_SUPPORT_BOLD_ITALIC, FONT_SUPPORT_BOLD, FONT_SUPPORT_ITALIC} from "../../../../../../../../../../../definitions/src/definitions/font/entity/Font";
import {ColorEntity} from "../../../../../../../../../../../definitions/src/definitions/colors/entity/ColorEntity";
import {FrontendService} from "../../../../../../../service/FrontendService";
import {InviteCardEditorV1Service} from "../../../service/InviteCardEditorV1Service";
import {InviteCardEditorV1SvgEditorFontsManager} from "./InviteCardEditorV1SvgEditorFontsManager";
import {SvgMapDocument, SvgMapElement, SvgMapInput, SvgMapElementType, SvgMapInputBlockAlign} from "../../../service/InviteCardEditorV1Service/InviteCardV1ModelService";

export enum SvgEditorFeatures
{
    Select = <any>"select",
    Create = <any>"create",
    Move = <any>"move",
    Resize = <any>"resize",
    Rotate = <any>"rotate",
    Remove = <any>"remove",
}

export enum SvgEditorMode
{
    None = <any>"none",
    Editing = <any>"editing",
    Creating = <any>"creating",
    Moving = <any>"moving",
    Resizing = <any>"resizing",
    Rotating = <any>"rotating",
    Removing = <any>"removing",
}

export interface SvgEditorConfig
{
    tools: {
        "add-text-input": boolean,
        "font-family": boolean,
        "font-size": boolean,
        "text-color": boolean,
        "text-align-left": boolean,
        "text-align-center": boolean,
        "text-align-right": boolean,
        "text-style-bold": boolean,
        "text-style-italic": boolean,
        "text-style-right": boolean,
        "text-style-underline": boolean,
    },
    features: {
        select: boolean;
        create: boolean,
        move: boolean,
        resize: boolean,
        rotate: boolean,
        remove: boolean,
    }
}

@Service({
    injects: [
        '$rootScope',
        FrontendService,
        InviteCardEditorV1Service,
        InviteCardEditorV1SvgEditorFontsManager,
    ]
})
export class InviteCardEditorV1SvgEditorService
{
    public static LAST_FONT_SIZE: number = 28;
    public static LAST_FONT_FAMILY: Font = undefined;
    public static LAST_TEXT_COLOR: string = '#000000';
    public static LAST_TEXT_BACKGROUND: string = 'transparent';

    get mode(): SvgEditorMode {
        return this._mode;
    }

    set mode(value: SvgEditorMode) {
        this._mode = value;
        this.modeChangesSubject.next(value);
    }

    private _mode: SvgEditorMode = SvgEditorMode.Editing;
    private colors: string[] = ['#000000', '#ffffff'];

    public size: { width: number, height: number } = { width: 0, height: 0 };
    public modeChangesSubject: Subject<SvgEditorMode> = new Subject<SvgEditorMode>();
    public cursor: Cursor = new Cursor();
    public document: SvgMapDocument = {
        inputs: [],
    };

    public events: {
        autoResizeSubject: Subject<number>;
    } = {
        autoResizeSubject: new Subject<number>()
    };

    constructor(
        private $rootScope: ng.IRootScopeService,
        private frontend: FrontendService,
        private services: InviteCardEditorV1Service,
        private fonts: InviteCardEditorV1SvgEditorFontsManager,
    ) {
        fonts.svg = this;

        InviteCardEditorV1SvgEditorService.LAST_FONT_FAMILY = this.fonts.getDefaultFont();

        frontend.replay.subscribe(next => {
            this.colors = ['#000000', '#ffffff'];

            next.exports['material_colors'].forEach((color: ColorEntity) => this.colors.push(color.hexCode));
        });

        services.model.readySubject.subscribe(next => {
            this.size.width = next.model.family.entity.card_size.entity.width;
            this.size.height = next.model.family.entity.card_size.entity.height;

            this.document = next.model.document;
        });

        services.model.documentSubject.subscribe(next => {
            this.document = next;
        });
    }

    isSelectEnabled(input: SvgMapElement): boolean {
        return !!~[
            SvgEditorMode.Moving,
            SvgEditorMode.Rotating,
            SvgEditorMode.Resizing,
            SvgEditorMode.Removing,
        ].indexOf(this.mode);
    }

    isSelected(input: SvgMapElement): boolean {
        return this.cursor.getCurrent() === input;
    }

    spawnTextInput(x: number, y: number): SvgMapInput {
        let sizes = this.services.model.model.size;
        let defaultLineHeight = 1.2;

        let input: SvgMapInput = {
            id: 'hiTextInput' + randomId(16),
            type: SvgMapElementType.Text,
            text: 'Text',
            variants: [],
            align: SvgMapInputBlockAlign.Center,
            textColor: InviteCardEditorV1SvgEditorService.LAST_TEXT_COLOR,
            bgColor: InviteCardEditorV1SvgEditorService.LAST_TEXT_BACKGROUND,
            lineHeight: defaultLineHeight,
            font: {
                family: InviteCardEditorV1SvgEditorService.LAST_FONT_FAMILY.entity.title,
                size: InviteCardEditorV1SvgEditorService.LAST_FONT_SIZE,
            },
            style: {
                bold: false,
                italic: false,
                underline: false,
            },
            container: {
                x: x,
                y: y,
                width: 120,
                height: defaultLineHeight * InviteCardEditorV1SvgEditorService.LAST_FONT_SIZE,
                rotate: 0
            }
        };

        this.fonts.load(input.font.family).subscribe(() => {
            this.document.inputs.push(input);
        });

        return input;
    }

    destroyTextInput(input: SvgMapInput | SvgMapElement): void {
        this.document.inputs = this.document.inputs.filter(compare => compare !== input);
    }

    alignLeft(): void {
        if(this.cursor.has()) {
            (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).align = SvgMapInputBlockAlign.Left;
        }
    }

    isAlignLeftActive(): boolean {
        if(this.cursor.has()) {
            return (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).align == SvgMapInputBlockAlign.Left;
        }else{
            return false;
        }
    }

    alignCenter(): void {
        if(this.cursor.has()) {
            (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).align =  SvgMapInputBlockAlign.Center;
        }
    }

    isAlignCenterActive(): boolean {
        if(this.cursor.has()) {
            return (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).align == SvgMapInputBlockAlign.Center;
        }else{
            return false;
        }
    }

    alignRight(): void {
        if(this.cursor.has()) {
            (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).align =  SvgMapInputBlockAlign.Right;
        }
    }

    isAlignRightActive(): boolean {
        if(this.cursor.has()) {
            return (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).align == SvgMapInputBlockAlign.Right;
        }else{
            return false;
        }
    }

    toggleBold(): void {
        if(this.cursor.has()) {
            let current = (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text));

            if(! current.style.bold) {
                this.fonts.setBold(current);
            }else{
                this.fonts.unsetBold(current);
            }
        }
    }

    isBoldEnabled(): boolean {
        return this.fonts.isBoldEnabled();
    }

    isBoldActive(): boolean {
        return this.fonts.isBoldActive();
    }

    toggleItalic(): void {
        if(this.cursor.has()) {
            let current = (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text));

            if(! current.style.italic) {
                this.fonts.setItalic(current);
            }else{
                this.fonts.unsetItalic(current);
            }
        }
    }

    isItalicActive(): boolean {
        return this.fonts.isItalicActive();

    }

    isItalicEnabled(): boolean {
        return this.fonts.isItalicEnabled();
    }

    toggleUnderline(): void {
        if(this.cursor.has()) {
            let current = (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text));

            if(! current.style.underline) {
                this.fonts.setUnderline(current);
            }else{
                this.fonts.unsetUnderline(current);
            }
        }
    }

    isUnderlineEnabled(): boolean {
        return this.fonts.isUnderlineEnabled();
    }

    isUnderlineActive(): boolean {
        return this.fonts.isUnderlineActive();
    }

    listAvailableFontFamilies(): Font[] {
        return this.fonts.listAvailableFonts();
    }

    isFontFamilyActive(fontName: string): boolean {
        if(this.cursor.has()) {
            return (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).font.family == fontName;
        }else{
            return fontName === InviteCardEditorV1SvgEditorService.LAST_FONT_FAMILY.entity.title;
        }
    }

    setFontFamily(font: Font): void {
        if(this.cursor.has()) {
            let fontName = font.entity.title;
            let current = (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text));

            this.fonts.load(font.entity.title).subscribe(() => {
                current.font.family = fontName;

                if(current.style.bold && current.style.italic && !(this.fonts.isSupported(fontName, FONT_SUPPORT_BOLD_ITALIC))) {
                    current.style.bold = false;
                    current.style.italic = false;
                }

                if(current.style.bold && !(this.fonts.isSupported(fontName, FONT_SUPPORT_BOLD))) {
                    current.style.bold = false;
                }

                if(current.style.italic && !(this.fonts.isSupported(fontName, FONT_SUPPORT_ITALIC))) {
                    current.style.italic = false;
                }
            });
        }else{
            InviteCardEditorV1SvgEditorService.LAST_FONT_FAMILY = font;
        }
    }

    listAvailableFontSizes(): number[] {
        let sizes = [
            10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 23, 26, 28, 30, 32, 35, 37, 39, 41, 43, 46, 48,
        ];

        for(let s = 50; s <= 256; s += 2) {
            sizes.push(s);
        }

        return sizes;
    }

    isFontSizeActive(size: number): boolean {
        if(this.cursor.has()) {
            return (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).font.size == size;
        }else{
            return size === InviteCardEditorV1SvgEditorService.LAST_FONT_SIZE;
        }
    }

    setFontSize(size: number): void {
        if(this.cursor.has()) {
            let current = (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text));
            let height = current.container.height;

            current.font.size = size;
        }else{
            InviteCardEditorV1SvgEditorService.LAST_FONT_SIZE = size;
        }
    }

    listAvailableTextColors(): string[] {
        return this.colors;
    }

    isTextColorActive(textColor: string): boolean {
        if(this.cursor.has()) {
            return (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).textColor == textColor;
        }else{
            return textColor === InviteCardEditorV1SvgEditorService.LAST_TEXT_COLOR;
        }
    }

    setTextColor(textColor: string): void {
        if(this.cursor.has()) {
            (<SvgMapInput>this.cursor.getCurrent(SvgMapElementType.Text)).textColor = textColor;
        }else{
            InviteCardEditorV1SvgEditorService.LAST_TEXT_COLOR = textColor;
        }
    }
}

class Cursor
{
    private current: SvgMapElement;

    has(): boolean {
        return this.current !== undefined;
    }

    getCurrent(type?: SvgMapElementType): SvgMapElement {
        if(this.has()) {
            if(type && this.current.type !== type) {
                throw new Error(`Required type ${type}, got ${this.current.type}`);
            }

            return this.current;
        }else{
            throw new Error('No selected element available!');
        }
    }

    select(element: SvgMapElement) {
        this.current = element;
    }

    blur(): void {
        this.current = undefined;
    }
}