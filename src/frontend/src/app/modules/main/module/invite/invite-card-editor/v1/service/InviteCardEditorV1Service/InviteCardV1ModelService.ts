import {Service} from "../../../../../../decorators/Service";

import {InviteCardEditorV1Service} from "../InviteCardEditorV1Service";
import {AttachmentEntity} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {AudioAttachmentMetadata} from "../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/AudioAttachmentMetadata";
import {InviteCardTemplate} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {ReplaySubject, Observable} from "rxjs";
import {InviteCardFamily} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {InviteCardSize} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {LocalizedString} from "../../../../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {InviteCardEditorV1EditorConfig} from "../InviteCardEditorV1EditorConfig";
import {FontService} from "../../../../../font/service/FontService";

export enum SvgMapElementType
{
    Text = <any>"text",
}

export enum SvgMapInputBlockAlign
{
    Left = <any>"left",
    Right = <any>"right",
    Center = <any>"center",
}

export interface SvgMapElement
{
    id: string,
    type: SvgMapElementType,
    container: {
        x: number,
        y: number,
        width: number,
        height: number,
        rotate: number,
    }
}

export interface SvgMapInput extends SvgMapElement
{
    text: string,
    type: SvgMapElementType,
    variants: LocalizedString[],
    align: SvgMapInputBlockAlign,
    textColor: string,
    bgColor: string,
    lineHeight: number,
    font: {
        family: string,
        size: number,
    },
    style: {
        bold: boolean,
        italic: boolean,
        underline: boolean,
    },
}

export interface SvgMapDocument
{
    inputs: SvgMapInput[];
}

export interface InviteCardV1Model
{
    template: InviteCardTemplate,
    family: InviteCardFamily,
    audio: {
        attachment: AttachmentEntity<AudioAttachmentMetadata>,
        uploaded: AttachmentEntity<AudioAttachmentMetadata>[],
    }
    photo: {
        enabled: boolean,
        placeholder: AttachmentEntity<ImageAttachmentMetadata>,
        attachment?: AttachmentEntity<ImageAttachmentMetadata>,
        uploaded: AttachmentEntity<ImageAttachmentMetadata>[],
        container: {
            start: {
                x: number,
                y: number,
            },
            end: {
                x: number,
                y: number,
            }
        }
    },
    size: InviteCardSize,
    backdrop: AttachmentEntity<ImageAttachmentMetadata>,
    document: SvgMapDocument,
}

@Service({
    injects: [
        '$timeout',
        FontService,
    ]
})
export class InviteCardV1ModelService
{
    public services: InviteCardEditorV1Service;
    public editorConfig: InviteCardEditorV1EditorConfig;

    public current: InviteCardTemplate;

    public isReady: boolean = false;

    public documentSubject: ReplaySubject<SvgMapDocument> = new ReplaySubject<SvgMapDocument>();
    public readySubject: ReplaySubject<{ model: InviteCardV1Model, editorConfig: InviteCardEditorV1EditorConfig }> = new ReplaySubject<{ model: InviteCardV1Model, editorConfig: InviteCardEditorV1EditorConfig }>();
    public resetSubject: ReplaySubject<{ model: InviteCardV1Model, editorConfig: InviteCardEditorV1EditorConfig }> = new ReplaySubject<{ model: InviteCardV1Model, editorConfig: InviteCardEditorV1EditorConfig }>();

    public model: InviteCardV1Model = {
        template: undefined,
        family: undefined,
        audio: {
            attachment: undefined,
            uploaded: [],
        },
        photo: {
            enabled: false,
            attachment: undefined,
            placeholder: undefined,
            uploaded: [],
            container: {
                start: {
                    x: undefined,
                    y: undefined,
                },
                end: {
                    x: undefined,
                    y: undefined,
                }
            }
        },
        size: undefined,
        backdrop: undefined,
        document: {
            inputs: [],
        }
    };

    constructor(
        private $timeout: ng.ITimeoutService,
        private fonts: FontService,
    ) {}

    load(input: InviteCardTemplate, family: InviteCardFamily, editorConfig: InviteCardEditorV1EditorConfig): void {
        this.isReady = false;
        this.current = input;
        
        let model = this.model;
        let config = input.entity.config;

        this.editorConfig = editorConfig;

        model.size = family.entity.card_size;
        model.template = input;
        model.family = family;

        if(config.audio) {
            if(config.audio.attachment) {
                model.audio.attachment = config.audio.attachment;
                model.audio.uploaded = [model.audio.attachment];
            }

            if(config.audio.uploaded) {
                model.audio.uploaded = config.audio.uploaded;
            }
        }else {
            model.audio.attachment = undefined;
            model.audio.uploaded = [];
        }

        if(model.template.entity.photo.enabled) {
            model.photo.enabled = true;
            model.photo.placeholder = input.entity.photo.placeholder;

            if(config.photo && config.photo.attachment) {
                model.photo.attachment = config.photo.attachment;
                model.photo.uploaded = [model.photo.attachment];
            }else{
                model.photo.attachment = undefined;
                model.photo.uploaded = [];
            }

            if(config.photo && config.photo.uploaded) {
                model.photo.uploaded = config.photo.uploaded;
            }else{
                model.photo.uploaded = [];
            }

            if(config.photo && config.photo.container.start.x !== undefined) {
                model.photo.container.start.x = config.photo.container.start.x;
                model.photo.container.start.y = config.photo.container.start.y;
                model.photo.container.end.x = config.photo.container.end.x;
                model.photo.container.end.y = config.photo.container.end.y;
            }else{
                model.photo.container.start.x = input.entity.photo.container.start.x;
                model.photo.container.start.y = input.entity.photo.container.start.y;
                model.photo.container.end.x = input.entity.photo.container.end.x;
                model.photo.container.end.y = input.entity.photo.container.end.y;
            }
        }else{
            model.photo.enabled = false;
            model.photo.placeholder = undefined;
            model.photo.attachment = undefined;
            model.photo.uploaded = [];
            model.photo.container.start.x = 0;
            model.photo.container.start.y = 0;
            model.photo.container.end.x = 0;
            model.photo.container.end.y = 0
        }

        if(config.document) {
            model.document = JSON.parse(JSON.stringify(config.document));
        }else{
            model.document = {
                inputs: [],
            };
        }

        this.useTemplate(input).subscribe(next => {
            this.readySubject.next({
                model: this.model,
                editorConfig: editorConfig,
            });
            this.isReady = true;
        });
    }

    useTemplate(template: InviteCardTemplate): Observable<InviteCardV1Model> {
        return Observable.create(observer => {
            let model = this.model;

            let viewBlock = this.services.view.status.addLoading();

            model.template = template;
            model.backdrop = template.entity.backdrop;

            if(template.entity.config.document) {
                model.document = JSON.parse(JSON.stringify(template.entity.config.document));
            }else{
                model.document = {
                    inputs: [],
                }
            }

            this.documentSubject.next(model.document);

            if(
                template.entity.config.document &&
                template.entity.config.document.inputs &&
                Array.isArray(template.entity.config.document.inputs) &&
                template.entity.config.document.inputs.length > 0
            ) {
                let observables = template.entity.config.document.inputs
                    .filter(input => {
                        return input.type === SvgMapElementType.Text;
                    })
                    .map((textInput: SvgMapInput) => {
                        return this.fonts.load(textInput.font.family);
                    });

                Observable.forkJoin(observables).subscribe(
                    next => {},
                    error => {
                        observer.error(error);
                    },
                    () => {
                        viewBlock.is = false;

                        observer.next(model);
                        observer.complete();
                    }
                )
            }else{
                viewBlock.is = false;

                observer.next(model);
                observer.complete();
            }
        }).share();
    }

    listTemplates(): InviteCardTemplate[] {
        if(this.isReady) {
            return this.model.family.entity.templates;
        }else{
            return [];
        }
    }

    reset(): void {
        this.model.audio.attachment = undefined;
        this.model.audio.uploaded = [];
        this.model.photo.attachment = undefined;
        this.model.photo.uploaded = [];
        this.model.photo.container.start.x = 0;
        this.model.photo.container.start.y = 0;
        this.model.photo.container.end.x = 0;
        this.model.photo.container.end.y = 0;
        this.model.backdrop = undefined;
        this.model.document = {
            inputs: [],
        };

        this.resetSubject.next({
            model: this.model,
            editorConfig: this.editorConfig
        });
    }
}