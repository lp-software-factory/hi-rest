import {HIModule} from "../../../../modules";

import {DesignerRequestForm} from "./component/DesignerRequestForm/index";

export const HI_MAIN_DESIGNER_MODULE: HIModule = {
    components: [
        DesignerRequestForm,
    ],
};