import {Component} from "../../../../decorators/Component";

import {httpErrorHandler} from "../../../../functions/httpErrorHandler";
import {DesignerRequestFormEntity} from "../../../../../../../../definitions/src/definitions/designer-requests/entity/DesignerRequestFormEntity";
import {urlSendDesignerRequest} from "../../../../../../../../definitions/src/definitions/designer-requests/paths/send";

enum DesignerRequestFormScreen {
    Form = <any>"form",
    Success = <any>"success",
}

interface DesignerRequestFormModel extends DesignerRequestFormEntity
{
}

@Component({
    selector: 'hi-designer-request-form',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$http',
        '$window'
    ]
})
export class DesignerRequestForm implements ng.IComponentController
{
    public error: string;
    public disabled: boolean = false;
    public screen: DesignerRequestFormScreen = DesignerRequestFormScreen.Form;

    public model: DesignerRequestFormModel = {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        link_portfolio: '',
        description: '',
        are_terms_accepted: false,
    };

    constructor(
        private $http: angular.IHttpService,
        private $window: angular.IWindowService,
    ) {}

    ngSubmit($event): void {
        this.disabled = true;
        this.error = undefined;

        if(! this.model.are_terms_accepted) {
            this.error = 'Вы не приняли условия оферты для дизайнера!';
            this.disabled = false;

            return;
        }else {
            this.$http.put(urlSendDesignerRequest, JSON.stringify({ form: this.model }))
                .then(response => {
                    this.screen = DesignerRequestFormScreen.Success;
                    this.error = undefined;
                })
                .catch(reason => {
                    this.disabled = false;

                    httpErrorHandler(reason, {
                        unknown: error => this.error = error,
                        known: {}
                    });
                });
        }
    }
}