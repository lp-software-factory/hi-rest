import {Component} from "../../../../decorators/Component";

import {UIAlertModalService, AlertModalButton, AlertModalString} from "../../service/UIAlertModalService";
import {LocaleService} from "../../../locale/service/LocaleService";
import {ITranslateService, ITranslateServiceInterface} from "../../../i18n/service/TranslationService";

@Component({
    selector: 'hi-main-ui-alert-modal',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        UIAlertModalService,
        LocaleService,
        ITranslateService,
    ]
})
export class UIAlertModal implements ng.IComponentController
{
    constructor(
        private $timeout: angular.ITimeoutService,
        private service: UIAlertModalService,
        private locale: LocaleService,
        private translate: ITranslateServiceInterface,
    ) {
        service.subjectOpen.subscribe(modal => {
            this.str(modal.title);
            this.str(modal.text);

            modal.buttons.forEach(button => {
                this.str(button.title);
            })
        });
    }

    getTitle(): string {
        let def = this.service.getModal().title;

        return this.service.getModal().title.translated;
    }

    getText(): string {
        return this.service.getModal().text.translated;
    }

    getButtons(): AlertModalButton[] {
        return this.service.getModal().buttons;
    }

    private str(input: AlertModalString) {
        if(input.translate) {
            this.translate(<string>input.value).then(t => {
                input.translated = t;
            });
        }else if(input.localize) {
            input.translated = this.locale.localize(<any>input.value);
        }else{
            input.translated = <string>input.value;
        }
    }
}