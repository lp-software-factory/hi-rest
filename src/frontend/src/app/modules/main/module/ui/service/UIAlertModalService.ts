import {Subject} from "rxjs";
import {Service} from "../../../decorators/Service";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";

export interface AlertModalString
{
    value: string|LocalizedString[];
    localize?: boolean;
    translate?: boolean;
    translated?: string;
}

export interface AlertModal
{
    title: AlertModalString;
    text: AlertModalString;
    buttons: AlertModalButton[];
}

export interface AlertModalButton
{
    title: AlertModalString;
    click: (modal: UIAlertModalCallback) => void;
}

@Service()
export class UIAlertModalService
{
    public subjectOpen: Subject<AlertModal> = new Subject<AlertModal>();
    public subjectClose: Subject<AlertModal> = new Subject<AlertModal>();

    private current: AlertModal;

    open(modal: AlertModal) {
        if(this.hasModal()) {
            this.close();
        }

        this.current = modal;
        this.subjectOpen.next(modal);
    }

    close(): void {
        if(this.hasModal()) {
            this.subjectClose.next(this.current);
            this.current = undefined;
        }
    }

    getModal(): AlertModal
    {
        if(! this.current) {
            throw new Error('No modal');
        }

        return this.current;
    }

    hasModal(): boolean {
        return this.current !== undefined;
    }
}

export interface UIAlertModalCallback
{
    close(): void;
}