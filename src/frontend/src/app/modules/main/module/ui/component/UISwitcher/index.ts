import {Component} from "../../../../decorators/Component";

@Component({
    selector: 'hi-ui-switcher',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        value: '=',
        title: '@',
        onChange: '&'
    }
})
export class UIHISwitcher implements ng.IComponentController
{
    private title: string;
    private _value: boolean;

    private onChange: { (value: boolean): any };

    isActive(): boolean {
        return this._value === true;
    }

    get value(): boolean {
        return this._value;
    }

    set value(value: boolean) {
        if(this.value !== value) {
            this._value = value;

            if(this.onChange) {
                this.onChange(this._value);
            }
        }
    }
}