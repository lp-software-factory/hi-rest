import {Component} from "../../../../decorators/Component";

export enum UIAjaxIndicatorStyle
{
    Spin = <any>"spin",
    SpinDouble = <any>"spin-double",
    Pulse = <any>"pulse",
}

@Component({
    selector: 'hi-ui-ajax-indicator',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        'uiStyle': '<'
    },
})
export class UIAjaxIndicator
{
    public style: UIAjaxIndicatorStyle = UIAjaxIndicatorStyle.Pulse;

    is(style: UIAjaxIndicatorStyle) {
        return style === this.style;
    }
}