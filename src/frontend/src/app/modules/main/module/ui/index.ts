import {HIModule} from "../../../../modules";

import {UIMainMenu} from "./component/UIMainMenu/index";
import {UIAlertModalService} from "./service/UIAlertModalService";
import {UIAlertModal} from "./component/UIAlertModal/index";
import {UIAjaxIndicator} from "./component/UIAjaxIndicator/index";
import {UILoadingTopIndicator} from "./component/UILoadingTopIndicator/index";
import {UIHISwitcher} from "./component/UISwitcher/index";

export const HI_UI_MODULE: HIModule = {
    services: [
        UIAlertModalService,
    ],
    components: [
        UIMainMenu,
        UIAlertModal,
        UIAjaxIndicator,
        UILoadingTopIndicator,
        UIHISwitcher,
    ]
};