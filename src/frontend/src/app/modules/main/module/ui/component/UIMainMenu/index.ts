import {Component} from "../../../../decorators/Component";
import {AuthService} from "../../../auth/service/AuthService";

@Component({
    selector: 'hi-ui-main-menu',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        AuthService,
    ]
})
export class UIMainMenu
{
    constructor(
        private auth: AuthService
    ) {}

    isDesignerCabinetAvailable(): boolean {
        return this.auth.isAdmin() || this.auth.isDesigner();
    }
}