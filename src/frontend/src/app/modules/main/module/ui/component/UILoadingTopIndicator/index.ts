import {Component} from "../../../../decorators/Component";

@Component({
    selector: 'hi-ui-loading-top-indicator',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        'lock': '<',
    }
})
export class UILoadingTopIndicator implements ng.IComponentController {}