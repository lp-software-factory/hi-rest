import {HIModule} from "../../../../modules";
import {MessageNotifications} from "./component/MessageNotifications/index";
import {MessageService} from "./service/MessageService";

export const HI_MAIN_MESSAGE_MODULE: HIModule = {
    components: [
        MessageNotifications,
    ],
    services: [
        MessageService,
    ]
};