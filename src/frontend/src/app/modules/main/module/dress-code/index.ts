import {HIModule} from "../../../../modules";

import {DressCodeService} from "./service/DressCodeService";

export const HI_MAIN_DRESS_CODE: HIModule = {
    components: [
    ],
    services: [
        DressCodeService,
    ]
};