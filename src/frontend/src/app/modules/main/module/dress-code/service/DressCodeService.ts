import {Service} from "../../../decorators/Service";

import {FrontendService} from "../../../service/FrontendService";
import {DressCode} from "../../../../../../../definitions/src/definitions/dress-code/entity/DressCode";

@Service({
    injects: [
        '$timeout',
        FrontendService,
    ]
})
export class DressCodeService
{
    public available: DressCode[] = [];

    constructor(
        private $timeout: ng.ITimeoutService,
        private frontend: FrontendService,
    ) {
        frontend.replay.subscribe(next => {
            if(next.exports['dress_codes']) {
                this.$timeout(() => {
                    this.available = next.exports['dress_codes'];
                });
            }
        })
    }

    getById(id: number): DressCode {
        let results = this.available.filter(d => d.entity.id === id);

        if(results.length === 0) {
            throw new Error(`No dress code with ID "${id}" found!`);
        }

        return results[0];
    }

    getDefaultDressCode(): DressCode {
        if(! this.available.length) {
            throw new Error('No dress codes available');
        }

        return this.available[0];
    }
}