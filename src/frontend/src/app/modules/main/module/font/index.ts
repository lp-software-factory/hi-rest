import {HIModule} from "../../../../modules";

import {FontService} from "./service/FontService";

export const HI_MAIN_MODULE_FONT: HIModule = {
    services: [
        FontService,
    ]
};