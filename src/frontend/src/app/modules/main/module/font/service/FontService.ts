import {Service} from "../../../decorators/Service";

import {Observable} from "rxjs";
import {Font, FONT_SUPPORT_BOLD, FONT_SUPPORT_ITALIC, FONT_SUPPORT_BOLD_ITALIC} from "../../../../../../../definitions/src/definitions/font/entity/Font";
import {cssLoader} from "../../../loaders/css-loader";

@Service({
    injects: [
        '$http',
    ]
})
export class FontService
{
    public loaded: { [fontName: string]: boolean } = {};

    public available: Font[] = [];

    constructor(
        private $http: angular.IHttpService,
    ) {}

    isLoaded(fontName: string): boolean {
        return !!this.loaded[fontName];
    }

    load(fontName: string): Observable<void> {
        return Observable.create(observer => {
            if(this.loaded[fontName]) {
                observer.next();
                observer.complete();
            }else{
                let result: RegExpExecArray;
                let urls = [];
                let font = this.getFontByName(fontName);
                let urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

                font.entity.files.forEach(file => urls.push(file.link.url));

                while(result = urlRegex.exec(font.entity.font_face)) {
                    urls.push(result[0]);
                }

                let essentials = urls
                    .filter(input => /^(.*)\.(ttf|otf|woff2|eot)$/.test(input))
                    .map(url => {
                        return Observable.create(essential => {
                            this.$http.get(url)
                                .then(response => essential.complete())
                                .catch(error => essential.error());
                        })
                    })
                    ;

                Observable.forkJoin(essentials).subscribe(() => {},
                    (error) => {
                        observer.error(error);
                    },
                    () => {
                        let css = font.entity.font_face;

                        font.entity.files.forEach((file, index) => {
                            css = css.replace(new RegExp(`\\$`+(index+1), 'g'), file.link.url);
                        });

                        cssLoader(css);

                        this.loaded[fontName] = true;

                        observer.next();
                        observer.complete();
                    }
                );
            }
        }).share();
    }

    getFontByName(fontName: string): Font {
        let result = this.available.filter(f => f.entity.title === fontName);

        if(result.length === 0) {
            throw new Error(`Font "${fontName}" not found`);
        }else if(result.length > 1) {
            throw new Error(`Font "${fontName}" has duplicates!`);
        }

        return result[0];
    }

    normalize(fontName: string, request: NormalizeRequest): void {
        let supportsBold = this.isSupported(fontName, FONT_SUPPORT_BOLD);
        let supportsItalic = this.isSupported(fontName, FONT_SUPPORT_ITALIC);
        let supportsBoldItalic = this.isSupported(fontName, FONT_SUPPORT_BOLD_ITALIC);

        if(! supportsBoldItalic && request.isBold && request.isItalic) {
            if(supportsBold) {
                request.isItalic = false;
            }else if(supportsItalic) {
                request.isBold = false;
            }else{
                request.isBold = false;
                request.isItalic = false;
            }
        }

        if(! supportsBold && request.isBold) {
            request.isBold = false;
        }

        if(! supportsItalic && request.isItalic) {
            request.isItalic = false
        }
    }

    isSupported(fontName: string, feature: number): boolean {
        return !! (this.getFontByName(fontName).entity.supports & feature);
    }

    getDefaultFont(): Font {
        if(! this.available.length) {
            throw new Error("No fonts available yet.");
        }

        return this.available[0];
    }
}

interface NormalizeRequest
{
    isItalic: boolean,
    isBold: boolean,
    isUnderline: boolean,
}