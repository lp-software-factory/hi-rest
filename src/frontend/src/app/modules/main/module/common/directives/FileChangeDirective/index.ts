import {Directive} from "../../../../decorators/Directive";

import {DirectiveFactoryInterface} from "../../../../../../angularized/decorators/Directive";

@Directive({
    name: 'ngFileChange',
})
    export class FileChangeDirectiveFactory implements DirectiveFactoryInterface
{
    factory(): angular.IDirectiveFactory {
        return function() {
            return {
                scope: {
                    ngFileChange: '&',
                },
                link: function(scope, element, attributes, ctrl) {
                    element.bind("change", function(changeEvent) {
                        let file = changeEvent.target['files'][0];

                        scope['ngFileChange']({
                            $event: changeEvent,
                            $file: file,
                            $files: changeEvent.target['files'],
                        });
                    });
                   }
            }
        };
    }
}