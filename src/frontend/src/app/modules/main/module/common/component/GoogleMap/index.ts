import {Component} from "../../../../decorators/Component";
import {timeout} from "../../../../../../../../../../../hi-admin/src/frontend/node_modules/rxjs/operator/timeout";

enum GMapType {
    Landing = <any>'landing',
    Editor = <any>'editor'
}

@Component({
    selector: 'hi-google-map',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        block: '<',
        type: '<'
    },
    injects: [
        '$timeout',
        '$exceptionHandler'
    ]
})
export class HiGoogleMapV1 implements ng.IComponentController
{
    constructor(
        private $timeout: angular.ITimeoutService,
        private $exceptionHandler: angular.IExceptionHandlerService
    ){}

    public block: any;
    public type: GMapType = GMapType.Landing;
    public searchStr: string = '';
    private searchBol: boolean = false;
    private currentPlace: any = 'current-location';

    getCurrentPlace() {
        if(this.isLanding()){
            this.currentPlace = `${this.block.markers[0].lat}, ${this.block.markers[0].lng}`;
        }
    }

    $onChanges(): void {
        this.getCurrentPlace();
    }
    
    changeSearchInput() {
        this.$timeout(() => {
            this.searchBol = true;
        }, 3000);
        if(this.searchBol){
            this.currentPlace = this.searchStr;
            this.searchBol = false;
        }
    }

    isLanding(): boolean {
        return this.type === GMapType.Landing;
    }

    addMarker() {
        if(!this.isLanding()){
            this.block.markers = [window['markerCoord']];
        }
    }

    getCoord(event) {
        window['markerCoord'] = {lat: event.latLng.lat(), lng: event.latLng.lng()};
    }
}