import {HIModule} from "../../../../modules";

import {FileChangeDirectiveFactory} from "./directives/FileChangeDirective/index";
import {DropZoneDirectiveFactory} from "./directives/DropZoneDirective/index";
import {CKEditorDirectiveFactory} from "./directives/CKEditorDirective/CKEditorDirective";
import {HIDoubleClickFactory} from "./directives/HIDoubleClick/index";

export const HI_MAIN_COMMON_MODULE: HIModule = {
    services: [],
    components: [],
    routes: [],
    directives: [
        FileChangeDirectiveFactory,
        DropZoneDirectiveFactory,
        CKEditorDirectiveFactory,
        HIDoubleClickFactory,
    ]
};