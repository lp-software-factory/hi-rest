import {Directive} from "../../../../decorators/Directive";

import {DirectiveFactoryInterface} from "../../../../../../angularized/decorators/Directive";

@Directive({
    name: 'hi-double-click',
    injects: [
        '$timeout',
    ],
})
export class HIDoubleClickFactory implements DirectiveFactoryInterface
{
    factory(): angular.IDirectiveFactory {
        return ($timeout: ng.ITimeoutService) => {
            return {
                restrict: 'A',
                    link: function($scope, $elm, $attrs) {
                        let clicks=0;

                        $elm.bind('click', function(evt) {
                            clicks++;
                            if (clicks == 1) {
                                $timeout(function(){
                                    if(clicks == 1) {
                                    } else {
                                        $scope.$apply(function() {
                                            console.log('clicked');
                                            $scope.$eval($attrs['hiDoubleClick'])
                                        });
                                    }
                                    clicks = 0;

                                },  300);}
                        });
                }
            };
        };
    }
}