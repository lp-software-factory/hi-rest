import {Observable} from "rxjs";

import {Directive} from "../../../../decorators/Directive";
import {DirectiveFactoryInterface} from "../../../../../../angularized/decorators/Directive";

@Directive({
    name: 'hiDropFiles',
})
export class DropZoneDirectiveFactory implements DirectiveFactoryInterface
{
    factory(): ng.IDirectiveFactory {
        return function() {
            return {
                scope: {
                    hiDropFiles: '&',
                    hiDragOut: '&',
                    hiDragOver: '&',
                },
                link: function(scope: ng.IScope, elem) {
                    elem.bind('dragenter', (e: Event) => {
                        e.stopPropagation();
                        e.preventDefault();
                    });

                    elem.bind('dragleave', (e: Event) => {
                        e.stopPropagation();
                        e.preventDefault();

                        scope['hiDragOut']({
                            $event: e,
                        })
                    });

                    elem.bind('dragover', (e: Event) => {
                        e.stopPropagation();
                        e.preventDefault();

                        scope['hiDragOver']({
                            $event: e,
                        })
                    });

                    elem.bind('drop', function(evt: any) {
                        evt.stopPropagation();
                        evt.preventDefault();

                        let observables: Observable<File>[] = [];

                        let files = evt.dataTransfer.files;
                        for(let i = 0, f; f = files[i]; i++) {
                            let reader = new FileReader();

                            observables.push(Observable.create(observer => {
                                reader.readAsArrayBuffer(f);
                                reader.onload = (function(theFile) {
                                    return function(e) {
                                        observer.next(theFile);
                                        observer.complete();
                                    };
                                })(f);
                            }));
                        }

                        Observable.forkJoin(observables).subscribe(results => {
                            scope['hiDropFiles']({
                                $files: results,
                            });
                        });
                    });
                }
            }
        }
    }
}