import {Directive} from "../../../../decorators/Directive";
import {DirectiveFactoryInterface} from "../../../../../../angularized/decorators/Directive";

declare let CKEDITOR;

export enum CKEDitorVariants
{
  Textarea = <any>"textarea",
  Inline = <any>"inline"
}

@Directive({
  name: 'hiCkEditor',
  injects: [
      '$rootScope',
  ]
})
export class CKEditorDirectiveFactory implements DirectiveFactoryInterface
{
  factory(): angular.IDirectiveFactory {
    return function($rootScope: ng.IRootScopeService) {
      return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel: ng.INgModelController) {
          let variant = CKEDitorVariants.Textarea;
          let config: any = {
            on: {
              'change': function() {
                ngModel.$setViewValue(this.getData());
              }
            }
          };

          if(attr['hiCkEditor']) {
            variant = attr['hiCkEditor'];
          }

          if(attr['hiCkEditorConfig']) {
            config.customConfig = attr['hiCkEditorConfig'];
          }

          let ck;

          switch(variant) {
            default:
              ck = CKEDITOR.replace(element[0], config);
              break;

            case CKEDitorVariants.Inline:
              ck = CKEDITOR.inline(element[0], config);
              break;
          }

          ngModel.$render = function() {
            ck.setData(ngModel.$modelValue);
          };

          ck.on('instanceReady', function() {
            let value = ngModel.$viewValue;

            if(! (value === undefined || value === null || value === [] || value === "")) {
              ck.setData(ngModel.$viewValue);
            }
          });

          ck.on('pasteState', function() {
            ngModel.$setViewValue(ck.getData());
          });
        }
      };
    }
  }
}