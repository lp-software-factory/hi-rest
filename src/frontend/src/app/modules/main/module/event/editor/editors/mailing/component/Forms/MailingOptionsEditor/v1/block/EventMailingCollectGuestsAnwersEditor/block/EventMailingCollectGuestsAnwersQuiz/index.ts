import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {EventMailingModel, CollectGuestAnswersStages, Quiz, QuizTarget, Answer} from "../../../../model";
import {randomId} from "../../../../../../../../../../../../../functions/randomId";

@Component({
    selector: 'hi-event-mailing-quiz-block',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        block: '<'
    },
    injects: [
        EventMailingModel
	]
})
export class EventMailingCollectGuestsAnwersQuizV1 implements ng.IComponentController
{
	constructor(private model: EventMailingModel){}
    
    public block: any;
    
    newQuiz: Quiz = {
        sid: randomId(32),
        target: QuizTarget.All,
        question: '',
        answers: []
    };
    
    newAnswer: Answer = {
        sid: randomId(32),
        title: ''
    };
    
    getQuizTarget(quiz): string {
        if(quiz.target === QuizTarget.Yes) {
            return 'Только те кто ответил "да"'
        } else if(quiz.target === QuizTarget.No) {
            return 'Только те кто ответил "нет"'
        } else return 'Все'
    }
    
    switchQuizTarget(inviteAnswer: string) {
        switch(inviteAnswer) {
            case 'yes':
                if(this.newQuiz.target !== QuizTarget.Yes)
                    this.newQuiz.target = QuizTarget.Yes; else this.newQuiz.target = QuizTarget.All;
                break;
            case 'no':
                if(this.newQuiz.target !== QuizTarget.No)
                    this.newQuiz.target = QuizTarget.No; else this.newQuiz.target = QuizTarget.All;
                break;
            default:
                this.newQuiz.target = QuizTarget.All;
        }
    }
    
    addNewAnswer() {
        this.newQuiz.answers.push(this.newAnswer);
        this.newAnswer = {
            sid: randomId(32),
            title: ''
        };
    }
    
    addNewQuiz() {
        this.model.CollectGuestAnswersEditor.blocks.quizzes.push(this.newQuiz);
        this.closeAddingNewQuizBlock();
    }
    
    openAddingNewQuizBlock() {
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.NewQuiz;
    }
    
    closeAddingNewQuizBlock() {
        this.newQuiz = {
            sid: randomId(32),
            target: QuizTarget.All,
            question: '',
            answers: []
        };
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.Quiz;
    }
    
    deleteQuiz(index) {
        this.model.CollectGuestAnswersEditor.blocks.quizzes.splice(index, 1);
    }
}