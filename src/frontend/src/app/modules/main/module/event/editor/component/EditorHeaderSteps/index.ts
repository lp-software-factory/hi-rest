import {Component} from "../../../../../decorators/Component";

import {EventEditorStage, EventEditorService} from "../../service/EventEditorService";
import {CurrentEvent} from "../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";

@Component({
    selector: 'hi-event-editor-header-steps',
    template: require('./template.jade'),
    styles: [
        require('./style.head.scss'),
    ],
    injects: [
        CurrentEvent,
        EventEditorService,
    ]
})
export class EditorHeaderSteps
{
    constructor(
        private event: CurrentEvent,
        private service: EventEditorService,
    ) {}

    isActive(stage: EventEditorStage): boolean {
        return this.service.stage === stage;
    }

    canGoEnvelopeStep(): boolean {
        return this.service.canGoEnvelopeStep();
    }

    canGoLandingStep(): boolean {
        return this.service.canGoLandingStep();
    }

    canGoMailingStep(): boolean {
        return this.service.canGoMailingStep();
    }
}