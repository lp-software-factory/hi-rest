import {Service} from "../../../../../../../../../decorators/Service";
import {EventRecipientsGroup} from "../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients-group/entity/EventRecipientsGroup";
import {randomId} from "../../../../../../../../../functions/randomId";
import {EventListEntity} from "../../../../../../../../../../../../../definitions/src/definitions/events/event/entity/EventListEntity";
import {EventRecipientsContact, ContactStatus} from "../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients/entity/EventRecipients";
import {UIAlertModalService} from "../../../../../../../../ui/service/UIAlertModalService";


export enum CollectGuestAnswersStages {
	BaseInfo = <any>'base_info',
	NewQuiz = <any>'new_quiz',	
	Quiz = <any>'quiz',
	Guests = <any>'guests',
	AdvancedSettings = <any>'advanced_settings',
	Preview = <any>'preview'
}

export enum ContactBookStages {
	ImportFromEvent = <any>'import_event',
	ImportFromFile = <any>'import_file',
	ImportFromMail = <any>'import_mail',
	ImportManual = <any>'import_manual',
	ContactBook = <any>'contact_book',
	Welcome = <any>'welcome'
}

export enum DeliveryStages {
	ContactBook = <any>'contact_book',
	CollectAnswers = <any>'collect_answers',
	ShareSocial = <any>'share_social'
}

export interface Answer {
	sid: string,
	title: string
}

export enum QuizTarget {
	Yes = <any>'yes',
	No = <any>'no',
	All = <any>'all'
}

export interface Quiz {
	sid: string;
	target: QuizTarget;
	question: string,
	answers: Array<Answer>
}

interface CollectGuestAnswersEditorInterface {
	sub: CollectGuestAnswersStages
	blocks: {
		baseInfo: {
			date: string;
			answersCollect: boolean;
			answersAfterDateOver: boolean;
			guestsMax: number;
			showEmptySpaces: boolean;
			description: string
		},
		quizzes: Array<Quiz>;
		guests: {
			additionalGuest: boolean;
			additionalGuestMax: number;
			collectInfo: {
				firstName: boolean;
				lastName: boolean;
				city: boolean;
				email: boolean;
				
				firstNameRequired: boolean;
				lastNameRequired: boolean;
				cityRequired: boolean;
				emailRequired: boolean;
			}
		},
		advancedSettings: {
			allowEditOnAnswerPage: boolean;
			allowEditAfterSend: boolean;
			allowSendMessagesWithAnswer: boolean;
			sendMailAfterAnswer: boolean;
			yesDescription: string;
			noDescription: string;
		}
	}
}

interface ContactBookManagerInterface {
	from: ContactBookStages;
	sub: ContactBookStages;
	manual: {
		isPair: boolean;
		plusPerson: boolean;
	}
	contacts: Array<EventRecipientsContact>,
	contactGroups: Array<EventRecipientsGroup>,
	events: Array<EventListEntity>
}


@Service({
	injects: [
		'$timeout',
		UIAlertModalService
	],
})
export class EventMailingModel
{
	constructor(
		private $timeout: angular.ITimeoutService,
		private alert: UIAlertModalService
	)
	{}

	public static EMAIL_REGEXP = /.+@.+\..+/i;
	public static PHONE_REGEXP = /^[\d+ -]+\d$/;
	
	isHeaderRowPresent = false;

	onImportFromFile($file) {
		let contacts: Array<EventRecipientsContact> = [];
		let firstNamePos = 0;
		let middleNamePos = 1;
		let lastNamePos = 2;
		let phonePositions = [];
		let isHeaderRowPresent = this.isHeaderRowPresent;
		if ($file !== undefined) {
			if($file.name.search(/\.csv/) !== -1) {
				let file = $file;
				let r = new FileReader();
				r.onload = (ev) => {
					this.$timeout(() =>
					{
						let linesArray = r.result.split("\n");
						if(isHeaderRowPresent) {
							let headers = linesArray[0].split(",");
							for(let i = 0; i < headers.length; i++) {
								let header = headers[i].toLowerCase();
								switch(header) {
									case "first name":
										firstNamePos = i;
										continue;
									case "middle name":
									case "additional name":
										middleNamePos = i;
										continue;
									case "last name":
										lastNamePos = i;
										continue;
								}
								if(header.match(".*phone.*")) {
									phonePositions.push(i)
								}
							}
							linesArray = linesArray.splice(1);
						}
						for(let line in linesArray) {
							let contact = {
								last_name: '', first_name: '', middle_name: '', email: '', phone: '', referral: false, sid: 'p' + randomId(32), with: '', group_id: undefined, status: ContactStatus.NotSent
							};
							let fields = linesArray[line].split(",");
							for(let i = 0; i < fields.length; i++) {
								let field = fields[i];
								if(i == firstNamePos) {
									contact.first_name = field;
								} else if(i == middleNamePos) {
									contact.middle_name = field;
								} else if(i == lastNamePos) {
									contact.last_name = field;
								} else if(field.match(EventMailingModel.PHONE_REGEXP)) {
									contact.phone = field;
								} else if(field.match(EventMailingModel.EMAIL_REGEXP)) {
									contact.email = field;
								}
							}
							contacts.push(contact);
						}
						this.ContactBookManager.contacts = this.ContactBookManager.contacts.concat(contacts);
						this.ContactBookManager.sub = ContactBookStages.ContactBook;
					})
				};
				r.readAsText(file);
			} else {
				this.raiseFileInputError();
			}
		}
	}

	raiseFileInputError(): void {
		this.alert.open({
			title: {
				value: 'hi.main.editor.invite-card.errors.photo-validation.title',
				translate: true,
			},
			text: {
				value: 'hi.main.editor.invite-card.errors.photo-validation.text',
				translate: true,
			},
			buttons: [
				{
					title: {
						value: 'hi.main.editor.invite-card.errors.photo-validation.close',
						translate: true,
					},
					click: (modal) => {
						modal.close();
					}
				}
			]
		})
	}
	
	GlobalStages: DeliveryStages = DeliveryStages.ContactBook;

	CollectGuestAnswersEditor: CollectGuestAnswersEditorInterface = {
		sub: CollectGuestAnswersStages.BaseInfo,
		blocks: {
			baseInfo: {
				date: '',
				answersCollect: false,
				answersAfterDateOver: false,
				guestsMax: 1,
				showEmptySpaces: false,
				description: ''
			},
			quizzes: [],
			guests: {
				additionalGuest: false,
				additionalGuestMax: 1,
				collectInfo: {
					firstName: false,
					lastName: false,
					city: false, 
					email: false,

					firstNameRequired: false,
					lastNameRequired: false,
					cityRequired: false,
					emailRequired: false,
				}
			},
			advancedSettings: {
				allowEditOnAnswerPage: false,
				allowEditAfterSend: false,
				allowSendMessagesWithAnswer: false,
				sendMailAfterAnswer: false,
				yesDescription: '',
				noDescription: ''
			}
		}
	};
	
	ContactBookManager: ContactBookManagerInterface = {
		from: ContactBookStages.Welcome,
		sub: ContactBookStages.Welcome,
		manual: {
			isPair: false,
			plusPerson: false
		},
		contacts: [],
		contactGroups: [],
		events: []
	};
}