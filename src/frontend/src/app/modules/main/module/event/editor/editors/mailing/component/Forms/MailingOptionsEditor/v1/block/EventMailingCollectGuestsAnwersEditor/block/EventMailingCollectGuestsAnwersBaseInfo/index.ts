import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {EventMailingModel} from "../../../../model";


@Component({
    selector: 'hi-event-mailing-base-info-block',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        block: '<'
    },
    injects: [
        EventMailingModel,
        
    ]
})
export class EventMailingCollectGuestsAnwersBaseInfoV1 implements ng.IComponentController
{
    constructor(
        private model: EventMailingModel
    ){}

    public block: any;
}