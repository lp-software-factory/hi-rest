import {Component} from "../../../../../../../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {CurrentEvent} from "../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {IComponentRouter} from "../../../../../../../../../../../../../../../typings-custom/angular-component-router";
import {EventMailingModel, CollectGuestAnswersStages, DeliveryStages, ContactBookStages} from "../../model";
import {EventMailingOptions} from "../../../../../../../../../../../../../../../definitions/src/definitions/events/event-mailing-options/entity/EventMailingOptions";
import {EventMailingOptionsRESTService} from "../../../../../../../../../../../../../../../definitions/src/services/event/event-mailing-options/EventMailingOptionsRESTService";
import {LoadingManager} from "../../../../../../../../../../common/helpers/LoadingManager";

@Component({
    selector: 'hi-event-mailing-collect-guests-answers-editor-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        event: '<',
        original: '<',
        current: '<',
    },
    injects: [
        '$timeout',
        '$rootRouter',
        EventMailingModel,
        CurrentEvent,
        EventMailingOptionsRESTService
    ]
})
export class EventMailingCollectGuestsAnswersEditorV1 implements ng.IComponentController
{
    public event: HIEvent;
    public original: EventMailingOptions;
    public current: EventMailingOptions;

    private status: LoadingManager = new LoadingManager();

    constructor(
        private $timeout: angular.ITimeoutService,
        private $router: IComponentRouter,
        private model: EventMailingModel,
        private currentEvent: CurrentEvent,
        private rest: EventMailingOptionsRESTService
    ){
        if(this.currentEvent.event.entity.event_mailing_options.entity.config.blocks !== undefined){
            this.model.CollectGuestAnswersEditor.blocks = this.currentEvent.event.entity.event_mailing_options.entity.config.blocks;  
        }
    }

    save() 
    {
        let loading = this.status.addLoading();
        let request = {
            config: {
                blocks: this.model.CollectGuestAnswersEditor.blocks
                }
            };
        this.rest.edit(this.currentEvent.event.entity.event_mailing_options.entity.event_id, request).subscribe(success => {
            this.$timeout(() => {
                loading.is = false;
            })
        });
    }

    goBackToContactBook() {
        this.model.GlobalStages = DeliveryStages.ContactBook;
    }

    showBaseInfo() {
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.BaseInfo;
    }
    
    showQuiz() {
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.Quiz;
    }
    
    showGuests() {
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.Guests;
    }
    
    showAdvancedSettings() {
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.AdvancedSettings;
    }
    
    showPreview() {
        this.model.CollectGuestAnswersEditor.sub = CollectGuestAnswersStages.Preview;
    }
}