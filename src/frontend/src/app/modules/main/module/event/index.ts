import {HIModule} from "../../../../modules";

import {EditorHeaderSteps} from "./editor/component/EditorHeaderSteps/index";
import {EventMailingOptionsEditorV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/index";
import {EventMailingCollectGuestsAnswersEditorV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingCollectGuestsAnwersEditor/index";
import {EventMailingCollectGuestsAnwersQuizV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingCollectGuestsAnwersEditor/block/EventMailingCollectGuestsAnwersQuiz/index";
import {EventMailingCollectGuestsAnwersPreviewV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingCollectGuestsAnwersEditor/block/EventMailingCollectGuestsAnwersPreview/index";
import {EventMailingCollectGuestsAnwersGuestsListV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingCollectGuestsAnwersEditor/block/EventMailingCollectGuestsAnwersGuestsList/index";
import {EventMailingCollectGuestsAnwersBaseInfoV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingCollectGuestsAnwersEditor/block/EventMailingCollectGuestsAnwersBaseInfo/index";
import {EventMailingCollectGuestsAnwersAdvancedSettingsV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingCollectGuestsAnwersEditor/block/EventMailingCollectGuestsAnwersAdvancedSettings/index";
import {EventEditorService} from "./editor/service/EventEditorService";
import {HiGoogleMapV1} from "../common/component/GoogleMap/index";
import {EditorTitle} from "./editor/component/EventTitle/index";
import {EventMailingContactBookImportFromFileV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingContactBookManager/variants/EventMailingContactBookImportFromFile/index";
import {EventMailingContactBookImportFromEventV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingContactBookManager/variants/EventMailingContactBookImportFromEvent/index";
import {EventMailingContactBookManagerV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingContactBookManager/index";
import {EventMailingContactBookImportManualV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingContactBookManager/variants/EventMailingContactBookImportManual/index";
import {EventMailingContactBookV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingContactBookManager/variants/EventMailingContactBook/index";
import {EventMailingWelcomeV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingContactBookManager/variants/EventMailingWelcome/index";
import {EventMailingShareSocialV1} from "./editor/editors/mailing/component/Forms/MailingOptionsEditor/v1/block/EventMailingShareSocial/index";
import {EventLandingTemplateConfigsService} from "./editor/service/EventLandingTemplateConfigsService";

export const HI_MAIN_EVENT_MODULE: HIModule = {
    components: [
        EditorHeaderSteps,
        EditorTitle,
        EventMailingOptionsEditorV1,
        EventMailingCollectGuestsAnwersQuizV1,
        EventMailingCollectGuestsAnwersPreviewV1,
        EventMailingCollectGuestsAnwersGuestsListV1,
        EventMailingCollectGuestsAnwersBaseInfoV1,
        EventMailingCollectGuestsAnwersAdvancedSettingsV1,
        EventMailingCollectGuestsAnswersEditorV1,
        EventMailingContactBookManagerV1,
        EventMailingContactBookV1,
        EventMailingContactBookImportFromEventV1,
        EventMailingContactBookImportFromFileV1,
        EventMailingContactBookImportManualV1,
        EventMailingWelcomeV1,
        EventMailingShareSocialV1,
        HiGoogleMapV1
    ],
    services: [
        EventEditorService,
        EventLandingTemplateConfigsService,
    ],
    routes: [],
};