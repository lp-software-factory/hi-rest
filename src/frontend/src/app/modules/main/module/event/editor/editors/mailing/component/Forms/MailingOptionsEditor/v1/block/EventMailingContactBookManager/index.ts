import {Component} from "../../../../../../../../../../../decorators/Component";

import {EventMailingModel, ContactBookStages} from "../../model";


@Component({
    selector: 'hi-event-mailing-contact-book-manager-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        stage: '<',
        original: '<',
        current: '<',
    },
    injects: [
        EventMailingModel
    ]
})
export class EventMailingContactBookManagerV1 implements ng.IComponentController
{
    constructor(
        private model: EventMailingModel
    ){}
    
    welcome(): boolean {
        return this.model.ContactBookManager.sub === ContactBookStages.Welcome;
    }
    
    contactBook(): boolean {
        return this.model.ContactBookManager.sub === ContactBookStages.ContactBook;
    }
    
    importManual(): boolean {
        return this.model.ContactBookManager.sub === ContactBookStages.ImportManual;
    }
    
    importFromEvent(): boolean {
        return this.model.ContactBookManager.sub === ContactBookStages.ImportFromEvent;
    }
    
    importFromFile(): boolean {
        return this.model.ContactBookManager.sub === ContactBookStages.ImportFromFile;
    }
    
    importFromEmail(): boolean {
        return this.model.ContactBookManager.sub === ContactBookStages.ImportFromMail;
    }
}