import {Component} from "../../../../../../../../../decorators/Component";
import {EventMailingModel} from "./model";

@Component({
	selector: 'hi-event-mailing-options-editor-v1', 
	template: require('./template.jade'), 
	styles: [require('./style.shadow.scss')],
	injects: [
		EventMailingModel
	]
})
export class EventMailingOptionsEditorV1 implements ng.IComponentController
{
	constructor(
		private model: EventMailingModel
	){}
}