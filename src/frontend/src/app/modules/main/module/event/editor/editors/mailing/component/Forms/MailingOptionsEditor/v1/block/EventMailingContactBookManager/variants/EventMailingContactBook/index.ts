import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {CurrentEvent} from "../../../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {EventMailingModel, DeliveryStages, ContactBookStages} from "../../../../model";
import {EventRecipientsGroup} from "../../../../../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients-group/entity/EventRecipientsGroup";
import {EventMailingContactBookImportManualV1} from "../EventMailingContactBookImportManual/index";
import {ITranslateServiceInterface, ITranslateService} from "../../../../../../../../../../../../i18n/service/TranslationService";
import {ListWindow} from "../../../../../../../../../../../../../helpers/ListWindow";
import {EventRecipientsContact, ContactStatus} from "../../../../../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients/entity/EventRecipients";
import {EventRecipientsRESTService} from "../../../../../../../../../../../../../../../../../definitions/src/services/event/event-recipients/EventRecipientsRESTService";
import {notDeepEqual} from "assert";
import {EventRESTService} from "../../../../../../../../../../../../../../../../../definitions/src/services/event/event/EventRESTService";
import {IComponentRouter} from "../../../../../../../../../../../../../../../../../typings-custom/angular-component-router";
import {UIAlertModalService} from "../../../../../../../../../../../../ui/service/UIAlertModalService";
import {LoadingManager} from "../../../../../../../../../../../../common/helpers/LoadingManager";

enum ContactBookSelectors {
    AddNewContactSelector = <any>'addNewContactSelector',
    SentSelector = <any>'sentSelector',
    ExportSelector = <any>'exportSelector',
    SortGroupSelector= <any>'sortGroupSelector',
    SortSentStatus = <any>'sortSentStatus',
    AllClosed = <any>'all_closed'
}

enum ContactBookSort {
    ByGroup = <any>'byGroup',
    ByStatus = <any>'byStatus',
    NoSort = <any>'noSort'
}

@Component({
    selector: 'hi-event-mailing-contact-book-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        stage: '<',
        original: '<',
        current: '<',
    },
    injects: [
        '$rootRouter',
        '$timeout',
        UIAlertModalService,
        ITranslateService,
        EventMailingModel,
        EventRecipientsRESTService,
        EventRESTService,
        CurrentEvent
    ]
})
export class EventMailingContactBookV1 implements ng.IComponentController
{
    constructor(
        private $router: IComponentRouter,
        private $timeout: angular.ITimeoutService,
        private alert: UIAlertModalService,
        private translate: ITranslateServiceInterface,
        private model: EventMailingModel,
        private rest: EventRecipientsRESTService,
        private restEvent: EventRESTService,
        private currentEvent: CurrentEvent)
    {
        let elements = document.getElementsByClassName('hi-input_contact');
        document.addEventListener('click', (ev) => {
            let elemq: boolean = false;
            for(let i = 0; i < elements.length; i++){
                if(elements[i] === ev.toElement) {
                    elemq = true;
                }
            }
            if(!elemq) {
                this.deactivateEditField();
            }
        });
        this.translate(['hi.main.event.recipients.groups.predefined.$predefined:family',
            'hi.main.event.recipients.groups.predefined.$predefined:friends',
            'hi.main.event.recipients.groups.predefined.$predefined:colleagues',]).then((data) =>
        {
            this.$timeout(() =>
            {
                this.titles['$predefined:family'] = data['hi.main.event.recipients.groups.predefined.$predefined:family'];
                this.titles['$predefined:friends'] = data['hi.main.event.recipients.groups.predefined.$predefined:friends'];
                this.titles['$predefined:colleagues'] = data['hi.main.event.recipients.groups.predefined.$predefined:colleagues'];
            });
        });
        this.window.items = this.model.ContactBookManager.contacts;
    }
    
    private status: LoadingManager = new LoadingManager();

    private activatedField = {index: undefined, field: undefined};
    private markedContacts: Array<EventRecipientsContact> = [];
    private titles: { [key: string]: string } = {};
    private contactManagementSelectorOpenedSid: string;
    private selector: ContactBookSelectors = ContactBookSelectors.AllClosed;
    private pageSize = 10;
    private pageSizeOptions = [10, 25, 50, 100];
    private sorting: ContactBookSort = ContactBookSort.NoSort;
    private groupSelectorTitle: string;
    private sentSelectorTitle: string;
    private searchContact: string;
    private sortType: string;
    private sortReverse: boolean;
    
    private window: ListWindow<EventRecipientsContact> = new ListWindow<EventRecipientsContact>(this.pageSize);

    choseSortType(type)
    {
        this.sortType = type
    }

    openSelector(selector)
    {
        switch(selector) {
            case 'addNewContactSelector':
                if(this.selector === ContactBookSelectors.AddNewContactSelector) {
                    this.selector = ContactBookSelectors.AllClosed;
                } else {
                    this.selector = ContactBookSelectors.AddNewContactSelector;
                }
                break;
            case 'exportSelector':
                if(this.selector === ContactBookSelectors.ExportSelector) {
                    this.selector = ContactBookSelectors.AllClosed;
                } else {
                    this.selector = ContactBookSelectors.ExportSelector;
                }
                break;
            case 'sentSelector':
                if(this.selector === ContactBookSelectors.SentSelector) {
                    this.selector = ContactBookSelectors.AllClosed;
                } else {
                    this.selector = ContactBookSelectors.SentSelector;
                }
                break;
            case 'sortGroupSelector':
                if(this.selector === ContactBookSelectors.SortGroupSelector) {
                    this.selector = ContactBookSelectors.AllClosed;
                } else {
                    this.selector = ContactBookSelectors.SortGroupSelector;
                }
                break;
            case 'sortSentStatus':
                if(this.selector === ContactBookSelectors.SortSentStatus) {
                    this.selector = ContactBookSelectors.AllClosed;
                } else {
                    this.selector = ContactBookSelectors.SortSentStatus;
                }
                break;
        }
    }

    activateEditField(index: number, field: string) {
        this.activatedField.index = index;
        this.activatedField.field = field;
    }

    isActivatedEditField(index: number, field: string):boolean {
        return !(this.activatedField.index === index && this.activatedField.field === field);
    }

    deactivateEditField() {
        this.activatedField.index = undefined;
        this.activatedField.field = undefined;
    }
    
    openContactManagementSelector(contact: EventRecipientsContact)
    {
        this.selector = ContactBookSelectors.AllClosed;
        if(this.contactManagementSelectorOpenedSid === contact.sid) {
            this.contactManagementSelectorOpenedSid = undefined;
        } else {
            this.contactManagementSelectorOpenedSid = contact.sid;
        }
    }
    
    getOpenedContactManagementSelector(contact: EventRecipientsContact)
    {
        return this.contactManagementSelectorOpenedSid === contact.sid
    }
    
    getGroupTitle(group: EventRecipientsGroup): string
    {
        if(group !== undefined) {
            if(!!~EventMailingContactBookImportManualV1.PREDEFINED_TITLES.indexOf(group.entity.title)) {
                return this.titles[group.entity.title];
            } else {
                return group.entity.title;
            }
        } else return 'Нет группы'
    }

    goToCollectAnswersSettings()
    {
        this.model.GlobalStages = DeliveryStages.CollectAnswers;
    }

    goToShareSocial()
    {
        this.model.GlobalStages = DeliveryStages.ShareSocial;
    }

    toggleSelectContact(contact: EventRecipientsContact)
    {
        for(let i = 0; i < this.markedContacts.length; i++) {
            if(this.markedContacts[i].sid === contact.sid) {
                return this.markedContacts.splice(i, 1);
            }
        }
        this.markedContacts.push(contact);
    }

    isChecked(contact: EventRecipientsContact): boolean
    {
        for(let i = 0; i < this.markedContacts.length; i++) {
            if(this.markedContacts[i].sid === contact.sid) {
                return true;
            }
        }
    }

    isAllChecked(): boolean
    {
        return this.markedContacts.length == this.model.ContactBookManager.contacts.length
    }

    selectAll()
    {
        if(this.isAllChecked()) {
            this.markedContacts = [];
        } else {
            this.markedContacts = JSON.parse(JSON.stringify(this.model.ContactBookManager.contacts));
        }
    }
    
    goToManualAdding()
    {
        this.model.ContactBookManager.from = ContactBookStages.ContactBook;
        this.model.ContactBookManager.sub = ContactBookStages.ImportManual;
    }

    goToImportFromEvent()
    {
        this.model.ContactBookManager.from = ContactBookStages.ContactBook;
        this.model.ContactBookManager.sub = ContactBookStages.ImportFromEvent;
    }

    goToImportFromFile()
    {
        this.model.ContactBookManager.from = ContactBookStages.ContactBook;
        this.model.ContactBookManager.sub = ContactBookStages.ImportFromFile;
    }
    
    deleteContact(index)
    {
        this.model.ContactBookManager.contacts.splice(index, 1);
    }
    
    deleteAllMarkedContacts()
    {
        for(let i = 0; i < this.markedContacts.length; i++) {
            for(let y = 0; y < this.model.ContactBookManager.contacts.length; y++) {
                if(this.model.ContactBookManager.contacts[y].sid === this.markedContacts[i].sid) {
                    this.model.ContactBookManager.contacts.splice(y, 1);
                }
            }
        }
        this.markedContacts = [];
    }
    
    getGroupById(contact: EventRecipientsContact)
    {
        for(let i = 0; i < this.model.ContactBookManager.contactGroups.length; i++) {
            if(this.model.ContactBookManager.contactGroups[i].entity.id === contact.group_id) {
                return this.model.ContactBookManager.contactGroups[i];
            }
        }
    }
    
    testContactBook(){
        let loading = this.status.addLoading();
        this.rest.edit(this.currentEvent.event.entity.event_recipients.entity.event_id, this.model.ContactBookManager).subscribe(success =>
        {
            this.restEvent.testEvent(this.currentEvent.event.entity.id, {}).subscribe(success =>
            {
                loading.is = false;
                window.location.href = '/my-events';
            })
        })
    }

    sendContactBook()
    {
        if(!(this.model.ContactBookManager.contacts.length > 0)) {
            this.alert.open({
                title: {
                    value: 'hi.main.event.recipients.send.error.title', translate: true,
                }, text: {
                    value: 'hi.main.event.recipients.send.error.text', translate: true,
                }, buttons: [{
                    title: {
                        value: 'hi.main.event.recipients.send.error.close', translate: true,
                    }, click: modal =>
                    {
                        this.$timeout(() =>
                        {
                            modal.close();
                        });
                    }
                }]
            });
        } else {
            let loading = this.status.addLoading();
            this.rest.edit(this.currentEvent.event.entity.event_recipients.entity.event_id, this.model.ContactBookManager).subscribe(success =>
            {
                this.restEvent.submitEvent(this.currentEvent.event.entity.id, {}).subscribe(success =>
                {
                    loading.is = false;
                    window.location.href = '/my-events';
                })
            })
        }
    }
    isLoading(): boolean {
        return this.status.isLoading();
    }
}