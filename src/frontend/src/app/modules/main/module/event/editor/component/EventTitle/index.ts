import {Component} from "../../../../../decorators/Component";

import {CurrentEvent} from "../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {EventRESTService} from "../../../../../../../../../definitions/src/services/event/event/EventRESTService";
import {randomId} from "../../../../../functions/randomId";

@Component({
	selector: 'hi-event-editor-title',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	injects: [
		'$timeout',
		CurrentEvent,
		EventRESTService,
	]
})
export class EditorTitle
{
	private id: string = 'et'+randomId(32);

	constructor(
		private $timeout: angular.ITimeoutService,
		private currentEvent: CurrentEvent,
		private service: EventRESTService
	) {}
}