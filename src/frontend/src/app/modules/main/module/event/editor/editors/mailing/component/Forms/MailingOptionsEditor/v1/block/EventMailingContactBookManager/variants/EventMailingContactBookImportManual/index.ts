import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {CurrentEvent} from "../../../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {EventMailingModel, ContactBookStages} from "../../../../model";
import {randomId} from "../../../../../../../../../../../../../functions/randomId";
import {EventRecipientsGroup} from "../../../../../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients-group/entity/EventRecipientsGroup";
import {ITranslateServiceInterface, ITranslateService} from "../../../../../../../../../../../../i18n/service/TranslationService";
import {EventRecipientsGroupCreateRequest} from "../../../../../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients-group/path/create";
import {EventRecipientsGroupRESTService} from "../../../../../../../../../../../../../../../../../definitions/src/services/event/event-recipients-group/EventRecipientsGroupRESTService";
import {getSymbolObservable} from "rxjs/symbol/observable";
import {EventRecipientsContact, ContactStatus} from "../../../../../../../../../../../../../../../../../definitions/src/definitions/events/event-recipients/entity/EventRecipients";



@Component({
    selector: 'hi-event-mailing-contact-book-import-manual-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        event: '<',
        original: '<',
        current: '<',
    },
    injects: [
        '$timeout',
        EventMailingModel,
        ITranslateService,
        EventRecipientsGroupRESTService
    ]
})
export class EventMailingContactBookImportManualV1 implements ng.IComponentController
{
    public static PREDEFINED_TITLES = [
        '$predefined:family',
        '$predefined:friends',
        '$predefined:colleagues',
    ];

    private titles: { [key: string]: string } = {};

    private sid: string = 'p'+randomId(32);
    private isGroupSelectorVisible: boolean = false;
    private currentGroup: EventRecipientsGroup;
    private newGroupTitle: string = '';

    private contacts: Array<EventRecipientsContact> = [
        {
            last_name: '',
            first_name: '',
            middle_name: '',
            email: '',
            phone: '',
            sid: 'p'+randomId(32),
            referral: false,
            with: '',
            group_id: undefined,
        }
    ];
    
    constructor(
        private $timeout: angular.ITimeoutService,
        private model: EventMailingModel,
        private translate: ITranslateServiceInterface,
        private restEventRecipientsGroup: EventRecipientsGroupRESTService
    ) {
        this.translate([
            'hi.main.event.recipients.groups.predefined.$predefined:family',
            'hi.main.event.recipients.groups.predefined.$predefined:friends',
            'hi.main.event.recipients.groups.predefined.$predefined:colleagues',
        ]).then((data) => {
            this.$timeout(() => {
                this.titles['$predefined:family'] = data['hi.main.event.recipients.groups.predefined.$predefined:family'];
                this.titles['$predefined:friends'] = data['hi.main.event.recipients.groups.predefined.$predefined:friends'];
                this.titles['$predefined:colleagues'] = data['hi.main.event.recipients.groups.predefined.$predefined:colleagues'];
            });
        });

        if(this.model.ContactBookManager.manual.isPair){
            this.initAdditionalContact();
        }
    }


    getCurrentGroupTitle(group: EventRecipientsGroup) {
        if(group !== undefined){
            return this.getGroupTitle(group);
        } else {
            return 'Добавить группу';
        }
    }

    getGroupTitle(group: EventRecipientsGroup): string {
        if(!!~EventMailingContactBookImportManualV1.PREDEFINED_TITLES.indexOf(group.entity.title)) {
            return this.titles[group.entity.title];
        }else{
            return group.entity.title;
        }
    }

    addNewGroup(){
        this.restEventRecipientsGroup.create({title: this.newGroupTitle}).subscribe(success => {
            this.$timeout(() => {
                this.model.ContactBookManager.contactGroups.push(success.event_recipients_group);
                this.newGroupTitle = '';
            });
        })
    }

    initAdditionalContact() {
        this.contacts.push({
            last_name: '',
            first_name: '',
            middle_name: '',
            email: '',
            phone: '',
            referral: false,
            sid: 'p'+randomId(32),
            with: this.contacts[0].sid,
            group_id: undefined,
        });

        this.contacts[0].with = this.contacts[1].sid;
    }
    
    changeContactsStatement(){
        if(this.model.ContactBookManager.manual.isPair) {
            this.initAdditionalContact();
        } else {
            this.contacts.splice(1, 1);
        }
    }
    
    changeCurrentGroup(group) {
        this.currentGroup = group;
        this.changeGroupSelectorStatement();
    }

    changeGroupSelectorStatement() {
        this.isGroupSelectorVisible = !this.isGroupSelectorVisible;
    }
    
    closeGroupSelector() {
        this.isGroupSelectorVisible = false;
    }
    
    goBack() {
        if(this.model.ContactBookManager.from === ContactBookStages.ContactBook){
            this.model.ContactBookManager.sub = ContactBookStages.ContactBook;
        } else {
            this.model.ContactBookManager.sub = ContactBookStages.Welcome;
        }
    }

    checkRequiresFields(): boolean {
        let blockPushButton: boolean = true;
        for(let i = 0; i < this.contacts.length; i++) {
            if(this.contacts[i].email.match(EventMailingModel.EMAIL_REGEXP) && this.contacts[i].first_name.length > 0 && this.contacts[i].last_name.length > 0) {
                blockPushButton = false;
            } else {
                blockPushButton = true;
            }
        }
        return blockPushButton;
    }

    pushContacts() {
        for(let i = 0; i < this.contacts.length; i++) {
            if(this.currentGroup !== undefined){
                this.contacts[i].group_id = this.currentGroup.entity.id;
            }
            this.model.ContactBookManager.contacts.push(JSON.parse(JSON.stringify(this.contacts[i])));
        }
        this.model.ContactBookManager.sub = ContactBookStages.ContactBook;
    }
}