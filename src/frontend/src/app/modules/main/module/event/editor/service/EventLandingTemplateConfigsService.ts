import {Service} from "../../../../decorators/Service";

import {EventLandingTemplateBlockConfig, EventLandingTemplateSwitcherConfigs, EventLandingTemplateBlock} from "../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@Service()
export class EventLandingTemplateConfigsService
{
    private configs: EventLandingTemplateBlockConfig[] = EventLandingTemplateSwitcherConfigs;

    all(): EventLandingTemplateBlockConfig[] {
        return this.configs;
    }

    getById(id: EventLandingTemplateBlock): EventLandingTemplateBlockConfig {
        let result = this.configs.filter(c => c.id === id);

        if(result.length === 0) {
            throw new Error(`Config for block type "${id}" doest not exists!`)
        }

        if(result.length !== 1) {
            throw new Error(`Config for block type "${id}" has duplicates.`)
        }

        return result[0];
    }

    getOrder(id: EventLandingTemplateBlock) {
        return this.getById(id).order;
    }

    getTitle(id: EventLandingTemplateBlock): string {
        return this.getById(id).title;
    }

    isToggleable(id: EventLandingTemplateBlock): boolean {
        return this.getById(id).toggle;
    }
}