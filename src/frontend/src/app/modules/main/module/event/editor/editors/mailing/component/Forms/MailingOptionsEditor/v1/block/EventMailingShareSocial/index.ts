import {Component} from "../../../../../../../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {CurrentEvent} from "../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {IComponentRouter} from "../../../../../../../../../../../../../../../typings-custom/angular-component-router";
import {EventMailingModel, CollectGuestAnswersStages, DeliveryStages} from "../../model";
import {EventMailingOptions} from "../../../../../../../../../../../../../../../definitions/src/definitions/events/event-mailing-options/entity/EventMailingOptions";
import {EventMailingOptionsRESTService} from "../../../../../../../../../../../../../../../definitions/src/services/event/event-mailing-options/EventMailingOptionsRESTService";

@Component({
    selector: 'hi-event-mailing-share-social-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        event: '<',
        original: '<',
        current: '<',
    },
    injects: [
        EventMailingModel
    ]
})
export class EventMailingShareSocialV1 implements ng.IComponentController
{
    constructor(private model: EventMailingModel) {}
    goBackToContactBook() {
        this.model.GlobalStages = DeliveryStages.ContactBook;
    }
}