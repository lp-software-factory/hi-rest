import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {CurrentEvent} from "../../../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {EventMailingModel, ContactBookStages} from "../../../../model";
import {FrontendService} from "../../../../../../../../../../../../../service/FrontendService";
import {EventRecipientsRESTService} from "../../../../../../../../../../../../../../../../../definitions/src/services/event/event-recipients/EventRecipientsRESTService";

@Component({
    selector: 'hi-event-mailing-welcome-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        event: '<',
        original: '<',
        current: '<',
    },
    injects: [
        EventMailingModel,
        FrontendService,
        EventRecipientsRESTService,
        '$timeout'
    ]
})
export class EventMailingWelcomeV1 implements ng.IComponentController
{
    eventsListVisible: boolean = false;
    pickedEventId: number;
    private dragOver: boolean = false;
    private username: string = '';
    
    constructor(
        private model: EventMailingModel,
        private frontend: FrontendService,
        private eventRecipientsRESTService: EventRecipientsRESTService,
        private $timeout: angular.ITimeoutService
    ){
        this.frontend.replay.subscribe(next => {
            this.model.ContactBookManager.contactGroups = next.exports['event_recipients_groups'];
            this.model.ContactBookManager.contacts = next.exports['event'].entity.event_recipients.entity.contacts;
            this.model.ContactBookManager.events = next.exports['events'];
            this.username = next.exports['event']['entity']['owner']['first_name'];
        });
        if(this.model.ContactBookManager.contacts.length > 0){
            this.model.ContactBookManager.from = ContactBookStages.ContactBook;
            this.model.ContactBookManager.sub = ContactBookStages.ContactBook;
        }
    }

    setManualContactAdding(isPair: boolean) {
        this.model.ContactBookManager.manual.isPair = isPair;
        this.model.ContactBookManager.sub = ContactBookStages.ImportManual;
    }
    
    hasPickedEvent():boolean {
        return !!this.pickedEventId;
    }

    getEventTitle() {
        if(this.hasPickedEvent()) {
            return this.model.ContactBookManager.events.filter(event => {return event.event_id === this.pickedEventId})[0].event_title;
        }
    }
    
    pickEvent(event_id: number){
        this.pickedEventId = event_id;
        this.eventsListVisible = false;
    }

    addContactsFromEvent() {
        this.eventRecipientsRESTService.get(this.pickedEventId).subscribe(success => {
            this.model.ContactBookManager.contacts = this.model.ContactBookManager.contacts.concat(success.event_recipients.entity.contacts);
            this.$timeout(() => {
                this.model.ContactBookManager.sub = ContactBookStages.ContactBook;
            });
        })
    }

    onDragOut(): void {
        this.$timeout(() => {
            this.dragOver = false;
        });
    }

    onDragOver(): void {
        this.$timeout(() => {
            this.dragOver = true;
        });
    }

    onDropFiles($file: File): void {
        this.dragOver = false;
        this.model.onImportFromFile($file);
    }
}