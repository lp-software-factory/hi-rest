import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {CurrentEvent} from "../../../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {EventMailingModel} from "../../../../model";
import {UIAlertModalService} from "../../../../../../../../../../../../ui/service/UIAlertModalService";

@Component({
    selector: 'hi-event-mailing-contact-book-import-from-file-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        event: '<',
        original: '<',
        current: '<',
    },
    injects: [
        '$timeout',
        EventMailingModel
    ]
})
export class EventMailingContactBookImportFromFileV1 implements ng.IComponentController
{
    constructor(
        private $timeout: angular.ITimeoutService,
        private model: EventMailingModel
    ) {}


    private dragOver: boolean = false;

    onDragOut(): void {
        this.$timeout(() => {
            this.dragOver = false;
        });
    }

    onDragOver(): void {
        this.$timeout(() => {
            this.dragOver = true;
        });
    }

    onDropFiles($file: File): void {
        this.dragOver = false;
        this.model.onImportFromFile($file);
    }
}