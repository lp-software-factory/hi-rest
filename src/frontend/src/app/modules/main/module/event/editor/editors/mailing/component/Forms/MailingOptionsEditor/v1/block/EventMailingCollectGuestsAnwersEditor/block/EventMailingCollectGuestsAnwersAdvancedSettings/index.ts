import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {EventMailingModel} from "../../../../model";

@Component({
    selector: 'hi-event-mailing-advanced-settings-block',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        block: '<',
    },
    injects: [
        EventMailingModel,
    ]
})
export class EventMailingCollectGuestsAnwersAdvancedSettingsV1 implements ng.IComponentController
{
    constructor(
        private model: EventMailingModel
    ){}
    
    public block: any;
}