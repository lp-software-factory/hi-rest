import {Component} from "../../../../../../../../../../../../../decorators/Component";

import {CurrentEvent} from "../../../../../../../../../../../../../../editor/module/event/editor/routes/EventEditorRoute/service";
import {EventMailingModel, ContactBookStages} from "../../../../model";
import {FrontendService} from "../../../../../../../../../../../../../service/FrontendService";
import {EventListEntity} from "../../../../../../../../../../../../../../../../../definitions/src/definitions/events/event/entity/EventListEntity";
import {EventRecipientsRESTService} from "../../../../../../../../../../../../../../../../../definitions/src/services/event/event-recipients/EventRecipientsRESTService";

@Component({
    selector: 'hi-event-mailing-contact-book-import-from-event-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        event: '<',
        original: '<',
        current: '<',
    },
    injects: [
        EventMailingModel,
        FrontendService,
        EventRecipientsRESTService
    ]
})
export class EventMailingContactBookImportFromEventV1 implements ng.IComponentController
{
    eventsListVisible: boolean;
    pickedEventId: number;
    
    constructor(
        private model: EventMailingModel,
        private frontend: FrontendService,
        private eventRecipientsRESTService: EventRecipientsRESTService
    ){
        frontend.replay.subscribe(next => {
            this.model.ContactBookManager.events = next.exports['events'];
        })
    }

    hasPickedEvent():boolean {
        return !!this.pickedEventId;
    }

    getEventTitle() {
        if(this.hasPickedEvent()) {
            return this.model.ContactBookManager.events.filter(event => {return event.event_id === this.pickedEventId})[0].event_title;
        }
    }

    pickEvent(event_id: number){
        this.pickedEventId = event_id;
        this.eventsListVisible = false;
    }

    addContactsFromEvent() {
        this.eventRecipientsRESTService.get(this.pickedEventId).subscribe(success => {
            this.model.ContactBookManager.contacts = this.model.ContactBookManager.contacts.concat(success.event_recipients.entity.contacts);
            this.model.ContactBookManager.sub = ContactBookStages.ContactBook;
        })
    }

    goBack() {
        this.model.ContactBookManager.sub = ContactBookStages.ContactBook;
    }
}