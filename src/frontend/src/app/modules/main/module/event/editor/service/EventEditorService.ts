import {Service} from "../../../../decorators/Service";

import {CurrentEvent} from "../../../../../editor/module/event/editor/routes/EventEditorRoute/service";

export enum EventEditorStage
{
    InviteCard = <any>"invite-card",
    Envelope = <any>"envelope",
    Landing = <any>"landing",
    Mailing = <any>"mailing",
}

@Service({
    injects: [
        '$timeout',
        CurrentEvent,
    ]
})
export class EventEditorService
{
    public stage: EventEditorStage = EventEditorStage.InviteCard;

    constructor(
        private $timeout: angular.ITimeoutService,
        private event: CurrentEvent,
    ) {}

    canGoEnvelopeStep(): boolean {
        return this.event.event.entity.solution.current.entity.invite_card_templates[0].entity.solution.is_user_specified;
    }

    canGoLandingStep(): boolean {
        return this.event.event.entity.solution.current.entity.envelope_templates[0].entity.solution.is_user_specified;
    }

    canGoMailingStep(): boolean {
        return this.event.event.entity.solution.current.entity.landing_templates[0].entity.solution.is_user_specified;
    }
}