import {Service} from "../../../../../../decorators/Service";

import {EnvelopeEditorV1Service} from "../EnvelopeEditorV1Service";
import {EnvelopeTexture} from "../../../../../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {InviteCardSizeEntity} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {FrontendService} from "../../../../../../service/FrontendService";
import {EnvelopeMarks} from "../../../../../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {FontService} from "../../../../../font/service/FontService";
import {EnvelopeBackdrop} from "../../../../../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";
import {EnvelopePattern} from "../../../../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopeColor} from "../../../../../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";
import {EnvelopeTextureDefinition} from "../../../../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";

@Service({
    injects: [
        '$timeout',
        FrontendService,
        FontService,
    ]
})
export class EnvelopeEditorV1ResourcesService
{
    public services: EnvelopeEditorV1Service;

    private _textures: EnvelopeTexture[] = [];
    private _backdrops: EnvelopeBackdrop[] = [];
    private _patterns: EnvelopePattern[] = [];
    private _marks: EnvelopeMarks[] = [];
    private _colors: EnvelopeColor[] = [];

    constructor(
        private $timeout: ng.ITimeoutService,
        private frontend: FrontendService,
        private fonts: FontService,
    ) {
        this.$timeout(() => {
            this.frontend.replay.subscribe(next => {
                this.fonts.available = next.exports['fonts'];
                this.textures = next.exports['envelope_textures'];
                this.backdrops = next.exports['envelope_backdrops'];
                this.marks = next.exports['envelope_marks'];
                this.colors = next.exports['envelope_colors'];
                this.patterns = next.exports['envelope_patterns'];
            });
        });
    }

    getEnvelopeColorById(id: number): EnvelopeColor {
        let result = this._colors.filter(input => input.entity.id === id);

        if(! result.length) {
            throw new Error(`Color with ID "${id}" not found`);
        }

        return result[0];
    }

    get textures(): EnvelopeTexture[] {
        return this._textures;
    }

    set textures(input: EnvelopeTexture[]) {
        this.services.model.ready.subscribe(next => {
            let icSize: InviteCardSizeEntity = this.services.model.getCurrentICSize();
            let cloned: EnvelopeTexture[] = JSON.parse(JSON.stringify(input));

            this._textures = cloned
                .filter(texture => {
                    texture.entity.definitions
                        .map((def): EnvelopeTextureDefinition => {
                            def.entity.resources = def.entity.resources.filter(resource => {
                                return (resource.width === icSize.width)
                                    && (resource.height === icSize.height);
                            });

                            return def;
                        })
                        .filter(definition => {
                            return definition.entity.resources.length > 0;
                        });

                    return texture.entity.definitions.length > 0;
                })
        });
    }

    get marks(): EnvelopeMarks[] {
        return this._marks;
    }

    set marks(value: EnvelopeMarks[]) {
        this._marks = value;
    }

    get backdrops(): EnvelopeBackdrop[] {
        return this._backdrops;
    }

    set backdrops(value: EnvelopeBackdrop[]) {
        this._backdrops = value;
    }

    get patterns(): EnvelopePattern[] {
        return this._patterns;
    }

    set patterns(value: EnvelopePattern[]) {
        this.services.model.ready.subscribe(next => {
            let icSize = this.services.model.getCurrentICSize();

            this._patterns = JSON.parse(JSON.stringify(value));
            this._patterns.forEach(pattern => {
                pattern.entity.definitions = pattern.entity.definitions.filter(def => {
                    return (def.entity.width === icSize.width)
                        && (def.entity.height === icSize.height);
                })
            })
        });
    }
    get colors(): EnvelopeColor[] {
        return this._colors;
    }

    set colors(value: EnvelopeColor[]) {
        this._colors = value;
    }
}