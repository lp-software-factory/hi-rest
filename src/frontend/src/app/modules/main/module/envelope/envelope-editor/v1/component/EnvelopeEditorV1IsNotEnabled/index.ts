import {Component} from "../../../../../../decorators/Component";

@Component({
    selector: 'hi-envelope-editor-v1-is-not-enabled',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ]
})
export class EnvelopeEditorV1IsNotEnabled
{
}