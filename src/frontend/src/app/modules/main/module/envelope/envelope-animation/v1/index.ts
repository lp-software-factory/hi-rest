import {EnvelopeAnimationService} from "./service/EnvelopeAnimationService";
import {EnvelopeAnimationPreview} from "./component/EnvelopeAnimationPreview/index";
import {EnvelopeAnimation} from "./component/EnvelopeAnimation/index";

export const V1 = {
    component: [
        EnvelopeAnimation,
        EnvelopeAnimationPreview,
    ],
    services: [
        EnvelopeAnimationService,
    ]
};