import {Component} from "../../../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../../../service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1ToolbarFontService} from "../../service";
import {ColorEntity} from "../../../../../../../../../../../../../../definitions/src/definitions/colors/entity/ColorEntity";
import {FrontendService} from "../../../../../../../../../../service/FrontendService";
import {ListWindow} from "../../../../../../../../../../helpers/ListWindow";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-text-color',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1ToolbarFontService,
        EnvelopeEditorV1Service,
        FrontendService,
    ],
})
export class EnvelopeEditorV1ToolbarTextColor implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 12;

    private subscription: Subscription;
    private window: ListWindow<string> = new ListWindow<string>(EnvelopeEditorV1ToolbarTextColor.LIST_WINDOW_LENGTH);

    constructor(
        private $timeout: angular.ITimeoutService,
        private toolbar: EnvelopeEditorV1ToolbarFontService,
        private service: EnvelopeEditorV1Service,
        private frontend: FrontendService,
    ) {}

    $onInit(): void {
        this.subscription = this.frontend.replay.subscribe(next => {
            this.window.items = next.exports['material_colors'].map((color: ColorEntity) => color.hexCode);
            this.window.scrollTo(this.service.model.config.recipient.textColor, (item, compare) => {
                return item == /* ! === */ compare;
            })
        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }


    isActive(color: string): boolean {
        return this.service.model.config.recipient.textColor == color;
    }

    getColorPreview(color: string): any {
        return {
            "background-color": color,
        }
    }

    click(color: string): void {
        this.service.model.config.recipient.textColor = color;
    }
}