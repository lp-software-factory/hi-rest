import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {EnvelopeTexture} from "../../../../../../../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-texture',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarTexture implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopeTexture> = new ListWindow<EnvelopeTexture>(EnvelopeEditorV1ToolbarTexture.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service,
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe(next => {
            this.$timeout(() => {
                this.window.items = this.services.resources.textures;
                this.window.scrollTo(next.texture, (item, compare) => {
                    return item.entity.id === compare.entity.id;
                })
            });
        })
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }
    }

    click(texture: EnvelopeTexture): void {
        this.services.model.texture = texture;
    }

    isActive(texture: EnvelopeTexture): boolean {
        return this.services.model.isAvailable() && this.services.model.texture.entity.id === texture.entity.id;
    }

    getTextureCSS(texture: EnvelopeTexture): any {
        return {
            'background-image': `url("${encodeURI(texture.entity.preview.link.url)}")`,
        }
    }
}