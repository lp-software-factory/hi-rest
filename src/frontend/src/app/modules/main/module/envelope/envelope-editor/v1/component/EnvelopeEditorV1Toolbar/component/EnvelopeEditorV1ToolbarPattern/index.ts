import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";
import {EnvelopePattern} from "../../../../../../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-pattern',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarPattern implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopePattern> = new ListWindow<EnvelopePattern>(EnvelopeEditorV1ToolbarPattern.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service,
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe(next => {
            this.$timeout(() => {
                this.window.items = this.services.resources.patterns;
                this.window.scrollTo(next.pattern, (item, compare) => {
                    return item.entity.id === compare.entity.id;
                })
            });
        })
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }
    }

    click(pattern: EnvelopePattern): void {
        this.services.model.pattern = pattern;
    }

    isActive(pattern: EnvelopePattern): boolean {
        return this.services.model.isAvailable() && this.services.model.pattern.entity.id === pattern.entity.id;
    }

    getPatternCSS(pattern: EnvelopePattern): any {
        if(this.services.model.isAvailable()) {
            let color = this.services.resources.colors.filter(color => {
                return this.services.model.patternVariant.entity.envelope_color_id === color.entity.id
            })[0];

            if(color) {
                let def = pattern.entity.definitions.filter(def => def.entity.envelope_color_id === color.entity.id)[0];

                if(def) {
                    return {
                        'background-image': `url("${encodeURI(def.entity.preview.link.url)}")`,
                    }
                }else{
                    return {};
                }
            }else{
                return {};
            }
        }else{
            return {};
        }
    }
}