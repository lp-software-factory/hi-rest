import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {EnvelopeMarkDefinition} from "../../../../../../../../../../../../definitions/src/definitions/envelope/mark-definition/entity/EnvelopeMarkDefinition";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-mark',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarMark implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopeMarkDefinition> = new ListWindow<EnvelopeMarkDefinition>(EnvelopeEditorV1ToolbarMark.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;
    private changeSubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe((model) => {
            let variants: EnvelopeMarkDefinition[] = [];

            this.services.resources.marks.forEach(marks => {
                marks.entity.definitions.forEach(attachment => variants.push(attachment));
            });

            this.window.items = variants;
            this.window.scrollTo(model.markVariant, (item, compare) => {
                return item.entity.id === compare.entity.id
            });

            if(this.changeSubscription) {
                this.changeSubscription.unsubscribe();
            }
        });
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }

        if(this.changeSubscription) {
            this.changeSubscription.unsubscribe();
        }
    }

    click(variant: EnvelopeMarkDefinition): void {
        this.services.model.markVariant = variant;
    }

    isActive(variant: EnvelopeMarkDefinition): boolean {
        return this.services.model.isAvailable() && this.services.model.markVariant.entity.id === variant.entity.id;
    }

    getMarkVariantCSS(variant: EnvelopeMarkDefinition): any {
        return {
            'background-image': `url("${encodeURI(variant.entity.attachment.link.url)}")`,
        }
    }
}