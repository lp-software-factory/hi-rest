import {Component} from "../../../../../../../../decorators/Component";

import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {UIAlertModalService} from "../../../../../../../ui/service/UIAlertModalService";
import {EnvelopeToolbar} from "../../../../service/EnvelopeEditorV1Service/EnvelopeEditorV1ToolbarService";

@Component({
    selector: 'hi-envelope-editor-v1-recipient',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        UIAlertModalService,
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1Recipient implements ng.IComponentController
{
    constructor(
        private alert: UIAlertModalService,
        private service: EnvelopeEditorV1Service,
    ) {}

    isRecipientAvailable(): boolean {
        return this.service.model.isAvailable()
            && this.service.model.config.recipient
            && !!this.service.model.config.recipient.fontFamily;
    }

    getCSS(): any {
        let config = this.service.model.config.recipient;

        return {
            "color": config.textColor,
            "font-family": config.fontFamily,
            "font-size": config.textSize + 'px',
            "font-weight": config.isBold ? 'bold' : 'normal',
            "font-style": config.isItalic ? 'italic' : 'normal',
            'text-decoration': config.isUnderline ? 'underline' : 'normal'
        };
    }

    click(): void {
        this.alert.open({
            title: {
                value: 'hi.main.envelope.editor.recipient.modal.bait.title',
                translate: true,
            },
            text: {
                value: 'hi.main.envelope.editor.recipient.modal.bait.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.main.envelope.editor.recipient.modal.bait.buttons.close',
                        translate: true,
                    },
                    click: (modal) => {
                        this.service.toolbar.switchToolbar(EnvelopeToolbar.Font);

                        modal.close();
                    }
                }
            ]
        });
    }
}