import {Component} from "../../../../../../decorators/Component";

import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EnvelopeEditorV1Service} from "../../service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1Switcher} from "../../service/EnvelopeEditorV1Service/EnvelopeEditorV1SwitcherService";

@Component({
    selector: 'hi-envelope-editor-v1',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        EnvelopeEditorV1Service,
    ],
    bindings: {
        switcher: '<',
        current: '<',
        original: '<',
    }
})
export class EnvelopeEditorV1 implements ng.IComponentController
{
    private switcher: EnvelopeEditorV1Switcher;
    private current: Solution;
    private original: Solution;

    constructor(
        private service: EnvelopeEditorV1Service,
    ) {}

    $onChanges?(onChangesObj: ng.IOnChangesObject): void {
        if(this.switcher) {
            this.service.switcher.enableSwitcher(this.switcher);
        }else{
            this.service.switcher.disableSwitcher();
        }

        this.service.read(this.current, this.original);
    }

    isReady(): boolean {
        return this.service.model.isAvailable();
    }

    isVisible(): boolean {
        return !this.service.switcher.isSwitcherEnabled() || this.service.switcher.getSwitcherValue();
    }
}