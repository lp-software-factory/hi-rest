import {Service} from "../../../../../decorators/Service";

import {Observable, Subject} from "rxjs";
import {EnvelopeEditorV1ModelService} from "./EnvelopeEditorV1Service/EnvelopeEditorV1ModelService";
import {EnvelopeEditorV1RevertService} from "./EnvelopeEditorV1Service/EnvelopeEditorV1RevertService";
import {EnvelopeEditorV1ToolbarService} from "./EnvelopeEditorV1Service/EnvelopeEditorV1ToolbarService";
import {EnvelopeEditorV1ViewPortService} from "./EnvelopeEditorV1Service/EnvelopeEditorV1ViewPortService";
import {EnvelopeEditorV1ResourcesService} from "./EnvelopeEditorV1Service/EnvelopeEditorV1ResourcesService";
import {Solution} from "../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EnvelopeTemplateEditResponse200} from "../../../../../../../../../definitions/src/definitions/envelope/template/path/edit";
import {EnvelopeTemplateRESTService} from "../../../../../../../../../definitions/src/services/envelope/envelope-template/EnvelopeTemplateRESTService";
import {UIAlertModalService} from "../../../../ui/service/UIAlertModalService";
import {EnvelopeAnimationService, EnvelopeAnimationRequest} from "../../../envelope-animation/v1/service/EnvelopeAnimationService";
import {EnvelopeEditorV1SwitcherService} from "./EnvelopeEditorV1Service/EnvelopeEditorV1SwitcherService";

@Service({
    injects: [
        '$timeout',
        UIAlertModalService,
        EnvelopeTemplateRESTService,
        EnvelopeEditorV1ModelService,
        EnvelopeEditorV1ToolbarService,
        EnvelopeEditorV1ViewPortService,
        EnvelopeEditorV1RevertService,
        EnvelopeEditorV1ResourcesService,
        EnvelopeAnimationService,
        EnvelopeEditorV1SwitcherService,
    ],
})
export class EnvelopeEditorV1Service
{
    public saveSubject: Subject<EnvelopeTemplateEditResponse200> = new Subject<EnvelopeTemplateEditResponse200>();

    public constructor(
        private $timeout: angular.ITimeoutService,
        private alert: UIAlertModalService,
        private rest: EnvelopeTemplateRESTService,

        public model: EnvelopeEditorV1ModelService,
        public toolbar: EnvelopeEditorV1ToolbarService,
        public view: EnvelopeEditorV1ViewPortService,
        public revert: EnvelopeEditorV1RevertService,
        public resources: EnvelopeEditorV1ResourcesService,
        public animation: EnvelopeAnimationService,
        public switcher: EnvelopeEditorV1SwitcherService,
    ) {
        model.services = this;
        toolbar.services = this;
        view.services = this;
        revert.services = this;
        resources.services = this;
        switcher.services = this;
    }

    preview(): Observable<string> {
        let request: EnvelopeAnimationRequest = {
            solution: this.model.current,
            envelope: this.model.current.entity.envelope_templates[0],
            invite: this.model.current.entity.invite_card_templates[0],
        };

        if(request.solution.entity.landing_templates.length) {
            request.landing = request.solution.entity.landing_templates[0];
        }

        return this.animation.open(request);
    }

    read(current: Solution, original?: Solution): void {
        this.model.current = current;

        if(original) {
            this.model.original = original;
        }
    }

    save(): Observable<EnvelopeTemplateEditResponse200> {
        let solution = this.model.current;

        let viewBlock = this.view.status.addLoading();
        let template = solution.entity.envelope_templates[0];

        let observable = this.rest.edit(template.entity.id, this.model.createEditEnvelopeTemplateRequest());

        observable.subscribe(
            next => {
                this.$timeout(() => {
                    this.saveSubject.next(next);
                });
            },
            error => {
                this.alert.open({
                    title: {
                        value: "hi.main.envelope.editor.save.error-modal.title",
                        translate: true,
                    },
                    text: {
                        value: "hi.main.envelope.editor.save.error-modal.text",
                        translate: true,
                    },
                    buttons: [
                        {
                            title: {
                                value: "hi.main.envelope.editor.save.error-modal.close",
                                translate: true,
                            },
                            click: modal => {
                                this.$timeout(() => {
                                    viewBlock.is = false;

                                    modal.close();
                                })
                            }
                        }
                    ]
                });
            },
            () => {
                this.$timeout(() => {
                    viewBlock.is = false;
                });
            }
        );

        return observable;
    }
}