import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {EnvelopeBackdropDefinition} from "../../../../../../../../../../../../definitions/src/definitions/envelope/backdrop-definition/entity/EnvelopeBackdropDefinition";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-backdrop-variant',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarBackdropVariant implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopeBackdropDefinition> = new ListWindow<EnvelopeBackdropDefinition>(EnvelopeEditorV1ToolbarBackdropVariant.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;
    private changeSubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe((model) => {
            if(this.changeSubscription) {
                this.changeSubscription.unsubscribe();
            }

            if(model.backdrop) {
                this.$timeout(() => {
                    this.window.items = model.backdrop.entity.definitions;
                });
            }

            this.changeSubscription = model.rChangeModel.subscribe(next => {
                if(next.backdrop) {
                    this.$timeout(() => {
                        this.window.items = next.backdrop.entity.definitions;
                    });
                }

                if(next.backdropVariant) {
                    this.$timeout(() => {
                        this.window.scrollTo(next.backdropVariant, (item, compare) => {
                            return item.entity.id === compare.entity.id;
                        });
                    });
                }
            });
        });
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }

        if(this.changeSubscription) {
            this.changeSubscription.unsubscribe();
        }
    }

    click(variant: EnvelopeBackdropDefinition): void {
        this.services.model.backdropVariant = variant;
    }

    isActive(variant: EnvelopeBackdropDefinition): boolean {
        return this.services.model.isAvailable() && this.services.model.backdropVariant.entity.sid === variant.entity.sid;
    }

    getBackdropVariantCSS(variant: EnvelopeBackdropDefinition): any {
        return {
            'background-image': `url("${encodeURI(variant.entity.preview.link.url)}")`,
        }
    }
}