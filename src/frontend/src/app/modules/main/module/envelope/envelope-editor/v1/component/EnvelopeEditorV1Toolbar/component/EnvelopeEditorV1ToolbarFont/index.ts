import {Component} from "../../../../../../../../decorators/Component";

import {EnvelopeEditorV1ToolbarFontService} from "./service";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-font',
    template: require('./template.jade'),
    styles: [
        require('./styles/style.shadow.scss'),
        require('./styles/style.icons.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1ToolbarFontService,
        EnvelopeEditorV1Service,
    ],
})
export class EnvelopeEditorV1ToolbarFont implements ng.IComponentController
{
    constructor(
        private $timeout: angular.ITimeoutService,
        private toolbar: EnvelopeEditorV1ToolbarFontService,
        private service: EnvelopeEditorV1Service,
    ) {}
}