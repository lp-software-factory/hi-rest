import {Component} from "../../../../../../decorators/Component";

import {EnvelopeAnimationService, EnvelopeAnimationRequest} from "../../service/EnvelopeAnimationService";
import {Subscription} from "rxjs";

@Component({
    selector: 'hi-envelope-animation-preview',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeAnimationService,
    ]
})
export class EnvelopeAnimationPreview implements ng.IComponentController
{
    private subscription: Subscription;

    public request: EnvelopeAnimationRequest;

    constructor(
        private $timeout: ng.ITimeoutService,
        private service: EnvelopeAnimationService,
    ) {}

    $onInit(): void {
        this.subscription = this.service.readySubject.subscribe(next => {
            this.$timeout(() => {
                this.request = next;
            });
        })
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    isVisible(): boolean {
        return this.service.current !== undefined;
    }

    isLoading(): boolean {
        return this.service.status.isLoading();
    }

    isReady(): boolean {
        return !this.isLoading() && this.isVisible() && this.service.ready;
    }

    getRequest(): EnvelopeAnimationRequest {
        return this.request;
    }

    close(): void {
        this.service.close();
    }
}