import {Service} from "../../../../../decorators/Service";

import {Solution} from "../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EnvelopeTemplate} from "../../../../../../../../../definitions/src/definitions/envelope/template/entity/EnvelopeTemplate";
import {InviteCardTemplate} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {EventLandingTemplate} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {AsyncImageLoaderService} from "../../../../../service/AsyncImageLoaderService";
import {LoadingManager} from "../../../../common/helpers/LoadingManager";
import {Observable, Subject} from "rxjs";

export interface EnvelopeAnimationRequest
{
    solution: Solution,
    envelope: EnvelopeTemplate,
    invite: InviteCardTemplate,
    landing?: EventLandingTemplate|undefined,
}

@Service({
    injects: [
        '$timeout',
        AsyncImageLoaderService,
    ]
})
export class EnvelopeAnimationService
{
    public status: LoadingManager = new LoadingManager();

    public ready: boolean;

    public openSubject: Subject<EnvelopeAnimationRequest> = new Subject<EnvelopeAnimationRequest>();
    public readySubject: Subject<EnvelopeAnimationRequest> = new Subject<EnvelopeAnimationRequest>();

    public current: Observable<EnvelopeAnimationRequest>;

    constructor(
        private $timeout: ng.ITimeoutService,
        private async: AsyncImageLoaderService,
    ) {}

    open(request: EnvelopeAnimationRequest): Observable<string> {
        this.ready = false;

        let observable = Observable.create(observer => {
            let loading = this.status.addLoading();

            this.async.loadImages(this.collectImagesToLoad(request)).subscribe(
                next => {
                    this.$timeout(() => {
                        observer.next(next);
                    });
                },
                error => {
                    this.$timeout(() => {
                        loading.is = false;

                        observer.error(error);
                        observer.complete();
                    });
                },
                () => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.ready = true;
                        this.readySubject.next(request);

                        observer.complete();
                    });
                }
            )
        }).share();

        observable.subscribe(next => {}, error => {}, () => { this.readySubject.next(request); });

        this.current = observable;
        this.openSubject.next(observable);

        return observable;
    }

    close() {
        this.$timeout(() => {
            this.ready = false;
            this.current = undefined;
        });
    }

    private collectImagesToLoad(input: EnvelopeAnimationRequest): string[] {
        let result = [];

        let icSize = input.invite.entity.card_size;
        let resources = input.envelope.entity.envelope_texture_definition.entity.resources.filter(t => {
            return (t.width === icSize.width)
                && (t.height === icSize.height);
        })[0].attachments;

        result.push(input.invite.entity.preview.variants['orig'].public_path);
        result.push(input.envelope.entity.envelope_backdrop_definition.entity.resource.link.url);
        result.push(input.envelope.entity.envelope_mark_definition.entity.attachment.link.url);
        result.push(input.envelope.entity.envelope_pattern_definition.entity.attachment.link.url);
        result.push(resources.back.link.url);
        result.push(resources.front.link.url);
        result.push(resources.cornerBack.link.url);
        result.push(resources.cornerFront.link.url);

        return result;
    }
}