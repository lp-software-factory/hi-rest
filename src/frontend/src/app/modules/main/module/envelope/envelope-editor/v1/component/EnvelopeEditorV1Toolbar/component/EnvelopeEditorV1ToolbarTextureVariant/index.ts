import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";
import {EnvelopeTextureDefinition} from "../../../../../../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-texture-variant',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarTextureVariant implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopeTextureDefinition> = new ListWindow<EnvelopeTextureDefinition>(EnvelopeEditorV1ToolbarTextureVariant.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;
    private changeSubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe((model) => {
            if(this.changeSubscription) {
                this.changeSubscription.unsubscribe();
            }

            if(model.texture) {
                this.$timeout(() => {
                    this.window.items = model.texture.entity.definitions;
                });
            }

            this.changeSubscription = model.rChangeModel.subscribe(next => {
                if(next.texture) {
                    this.$timeout(() => {
                        this.window.items = next.texture.entity.definitions;
                    });
                }

                if(next.textureVariant) {
                    this.$timeout(() => {
                        this.window.scrollTo(next.textureVariant, (item, compare) => {
                            return item.entity.id === compare.entity.id;
                        });
                    });
                }
            });
        });
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }

        if(this.changeSubscription) {
            this.changeSubscription.unsubscribe();
        }
    }

    click(variant: EnvelopeTextureDefinition): void {
        this.services.model.textureVariant = variant;
    }

    isActive(variant: EnvelopeTextureDefinition): boolean {
        return this.services.model.isAvailable() && this.services.model.textureVariant.entity.id === variant.entity.id;
    }

    getTextureVariantCSS(variant: EnvelopeTextureDefinition): any {
        return {
            'background-image': `url("${encodeURI(variant.entity.preview.link.url)}")`,
        }
    }
}