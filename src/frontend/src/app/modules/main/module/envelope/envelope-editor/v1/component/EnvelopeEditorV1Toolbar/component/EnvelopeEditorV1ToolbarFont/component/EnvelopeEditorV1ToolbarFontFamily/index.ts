import {Component} from "../../../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../../../service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1ToolbarFontService} from "../../service";
import {ListWindow} from "../../../../../../../../../../helpers/ListWindow";
import {Font} from "../../../../../../../../../../../../../../definitions/src/definitions/font/entity/Font";
import {FontService} from "../../../../../../../../../font/service/FontService";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-font-family',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1ToolbarFontService,
        EnvelopeEditorV1Service,
        FontService,
    ],
})
export class EnvelopeEditorV1ToolbarFontFamily implements ng.IComponentController
{
    public static FONT_LIST_WINDOW_LENGTH = 6;

    private subscription: Subscription;
    private window: ListWindow<Font> = new ListWindow<Font>(EnvelopeEditorV1ToolbarFontFamily.FONT_LIST_WINDOW_LENGTH);

    constructor(
        private $timeout: angular.ITimeoutService,
        private toolbar: EnvelopeEditorV1ToolbarFontService,
        private service: EnvelopeEditorV1Service,
        private fonts: FontService,
    ) {}

    $onInit(): void {
        this.subscription = this.service.model.ready.subscribe(next => {
            this.window.items = this.fonts.available;
            this.window.scrollTo(this.fonts.getFontByName(this.service.model.config.recipient.fontFamily), (item, compare) => {
                return item.entity.title === compare.entity.title;
            })
        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    getFontPreview(font: Font): string {
        return font.entity.preview.link.url;
    }

    getFontPreviewAlt(font: Font): string {
        return font.entity.title;
    }

    isActive(font: Font): boolean {
        return this.service.model.isAvailable()
            && (this.service.model.config.recipient.fontFamily === font.entity.title);
    }

    click(font: Font): void {
        let fontName: string = font.entity.title;

        if(this.fonts.isLoaded(fontName)) {
            this.service.model.config.recipient.fontFamily = fontName;
            this.fonts.normalize(fontName, this.service.model.config.recipient);
        }else{
            let loading = this.service.view.status.addLoading();

            this.fonts.load(fontName).subscribe(
                () => {},
                () => {
                    this.$timeout(() => {
                        loading.is = false;
                    });
                },
                () => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.service.model.config.recipient.fontFamily = fontName;
                        this.fonts.normalize(fontName, this.service.model.config.recipient)
                    });
                },
            )
        }
    }
}
