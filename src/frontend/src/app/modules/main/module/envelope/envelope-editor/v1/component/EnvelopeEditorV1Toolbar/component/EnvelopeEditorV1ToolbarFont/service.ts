import {Service} from "../../../../../../../../decorators/Service";

import {ReplaySubject} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {EnvelopeTemplateConfig} from "../../../../../../../../../../../../definitions/src/definitions/envelope/template/entity/EnvelopeTemplate";
import {FontService} from "../../../../../../../font/service/FontService";
import {FONT_SUPPORT_BOLD, FONT_SUPPORT_BOLD_ITALIC} from "../../../../../../../../../../../../definitions/src/definitions/font/entity/Font";

export enum EnvelopeEditorFontToolbars
{
    FontFamily = <any>"font-family",
    TextSize = <any>"text-size",
    TextColor = <any>"text-color",
}

@Service({
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
        FontService,
    ]
})
export class EnvelopeEditorV1ToolbarFontService
{
    private config: EnvelopeTemplateConfig;

    public replay: ReplaySubject<EnvelopeEditorFontToolbars> = new ReplaySubject<EnvelopeEditorFontToolbars>();
    public current: EnvelopeEditorFontToolbars = EnvelopeEditorFontToolbars.FontFamily;

    constructor(
        private $timeout: angular.ITimeoutService,
        private service: EnvelopeEditorV1Service,
        private fonts: FontService,
    ) {}

    isActive(id: EnvelopeEditorFontToolbars): boolean {
        return this.current === id;
    }

    setActive(id: EnvelopeEditorFontToolbars): void {
        this.current = id;
        this.replay.next(id);
    }

    isBoldEnabled(): boolean {
        return this.service.model.isAvailable() && this.service.model.config.recipient.isBold;
    }

    isItalicEnabled(): boolean {
        return this.service.model.isAvailable() && this.service.model.config.recipient.isItalic;
    }

    isUnderlineEnabled(): boolean {
        return this.service.model.isAvailable() && this.service.model.config.recipient.isUnderline;
    }

    isBoldActive(): boolean {
        if(this.service.model.isAvailable()) {
            let fontName = this.service.model.config.recipient.fontFamily;
            let isItalic = this.service.model.config.recipient.isItalic;

            if(isItalic) {
                return this.fonts.isSupported(fontName, FONT_SUPPORT_BOLD_ITALIC)
            }else{
                return this.fonts.isSupported(fontName, FONT_SUPPORT_BOLD)
            }
        }else{
            return false;
        }
    }

    isItalicActive(): boolean {
        if(this.service.model.isAvailable()) {
            let fontName = this.service.model.config.recipient.fontFamily;
            let isBold = this.service.model.config.recipient.isBold;

            if(isBold) {
                return this.fonts.isSupported(fontName, FONT_SUPPORT_BOLD_ITALIC)
            }else{
                return this.fonts.isSupported(fontName, FONT_SUPPORT_BOLD)
            }
        }else{
            return false;
        }
    }

    isUnderlineActive(): boolean {
        return this.service.model.isAvailable();
    }

    toggleBold(): void {
        if(this.service.model.isAvailable() && this.isBoldActive()) {
            this.service.model.config.recipient.isBold = !this.service.model.config.recipient.isBold;
        }
    }

    toggleItalic(): void {
        if(this.service.model.isAvailable() && this.isItalicActive()) {
            this.service.model.config.recipient.isItalic = !this.service.model.config.recipient.isItalic;
        }
    }

    toggleUnderline(): void {
        if(this.service.model.isAvailable() && this.isUnderlineActive()) {
            this.service.model.config.recipient.isUnderline = !this.service.model.config.recipient.isUnderline;
        }
    }
}