import {Service} from "../../../../../../decorators/Service";

import {ReplaySubject, Subject} from "rxjs";
import {EnvelopeEditorV1Service} from "../EnvelopeEditorV1Service";
import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EnvelopeTexture} from "../../../../../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {AsyncImageLoaderService} from "../../../../../../service/AsyncImageLoaderService";
import {InviteCardSizeEntity} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {EnvelopeTemplateConfig, EnvelopeTemplate} from "../../../../../../../../../../definitions/src/definitions/envelope/template/entity/EnvelopeTemplate";
import {EnvelopeBackdrop} from "../../../../../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";
import {EnvelopeMarks} from "../../../../../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {EnvelopePattern} from "../../../../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopePatternDefinition} from "../../../../../../../../../../definitions/src/definitions/envelope/pattern-definition/entity/EnvelopePattern";
import {EnvelopeColor} from "../../../../../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";
import {FontService} from "../../../../../font/service/FontService";
import {EnvelopeTemplateEditRequest} from "../../../../../../../../../../definitions/src/definitions/envelope/template/path/edit";
import {LocalizedString} from "../../../../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {EnvelopeMarkDefinition} from "../../../../../../../../../../definitions/src/definitions/envelope/mark-definition/entity/EnvelopeMarkDefinition";
import {EnvelopeTextureDefinitionResource, EnvelopeTextureDefinition} from "../../../../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";
import {EnvelopeBackdropDefinition} from "../../../../../../../../../../definitions/src/definitions/envelope/backdrop-definition/entity/EnvelopeBackdropDefinition";

export const ModelRecipientsDefaults = {
    textSize: 32,
    textColor: '#000000',
    isBold: false,
    isItalic: false,
    isUnderline: false,
};

@Service({
    injects: [
        '$timeout',
        AsyncImageLoaderService,
        FontService,
    ]
})
export class EnvelopeEditorV1ModelService
{
    private _current: Solution;
    private _original: Solution;

    public isReady: boolean = false;

    public services: EnvelopeEditorV1Service;

    public ready: ReplaySubject<EnvelopeEditorV1ModelService> = new ReplaySubject<EnvelopeEditorV1ModelService>();

    public rChangeModel: Subject<EnvelopeEditorV1ModelService> = new Subject<EnvelopeEditorV1ModelService>();
    public rCurrent: ReplaySubject<Solution> = new ReplaySubject<Solution>();
    public rOriginal: ReplaySubject<Solution> = new ReplaySubject<Solution>();

    private _card: string;
    private _title: LocalizedString[];
    private _description: LocalizedString[];
    private _backdropVariant: EnvelopeBackdropDefinition;
    private _textureVariant: EnvelopeTextureDefinition;
    private _markVariant: EnvelopeMarkDefinition;
    private _patternVariant: EnvelopePatternDefinition;

    public config: EnvelopeTemplateConfig;

    constructor(
        private $timeout: ng.ITimeoutService,
        private images: AsyncImageLoaderService,
        private fonts: FontService,
    ) {}

    createEditEnvelopeTemplateRequest(): EnvelopeTemplateEditRequest {
        return {
            title: this.title,
            description: this.description,
            envelope_backdrop_definition_id: this.backdropVariant.entity.id,
            envelope_mark_definition_id: this.markVariant.entity.id,
            envelope_pattern_definition_id: this.patternVariant.entity.id,
            envelope_texture_definition_id: this.textureVariant.entity.id,
            config: this.config
        };
    }

    get current(): Solution {
        return this._current;
    }

    set current(value: Solution) {
        this.isReady = false;
        this._current = value;
        this.initModel(value);
        this.ready.next(this);
        this.rCurrent.next(value);
        this.isReady = true;
    }

    set original(value: Solution) {
        this._original = value;
        this.rOriginal.next(value);
        this.rChangeModel.next(this);
    }

    isAvailable(): boolean {
        return this.isReady
            && this.current !== undefined
            && this._card !== undefined
            && this._textureVariant !== undefined
            && this._backdropVariant !== undefined
            && this._markVariant !== undefined
            && this._patternVariant !== undefined;
    }

    initModel(input: Solution): void {
        let template: EnvelopeTemplate = JSON.parse(JSON.stringify(input.entity.envelope_templates[0]));
        let config: EnvelopeTemplateConfig = JSON.parse(JSON.stringify(template.entity.config));

        if(config instanceof Array) {
            config = {};
        }

        if(! config.recipient) {
            config.recipient = {
                fontFamily: undefined,
                textSize: ModelRecipientsDefaults.textSize,
                textColor: ModelRecipientsDefaults.textColor,
                isBold: ModelRecipientsDefaults.isBold,
                isItalic: ModelRecipientsDefaults.isItalic,
                isUnderline: ModelRecipientsDefaults.isUnderline,
            };

            if(! config.recipient.fontFamily) {
                config.recipient.fontFamily = this.fonts.getDefaultFont().entity.title;
            }
        }

        this.config = config;
        this.card = input.entity.invite_card_templates[0].entity.preview.variants['orig'].public_path;
        this.title = template.entity.title;
        this.description = template.entity.description;
        this.backdropVariant = template.entity.envelope_backdrop_definition;
        this.textureVariant = template.entity.envelope_texture_definition;
        this.markVariant = template.entity.envelope_mark_definition;
        this.patternVariant = template.entity.envelope_pattern_definition;

        let viewBlock = this.services.view.status.addLoading();

        this.fonts.load(config.recipient.fontFamily).subscribe(
            () => {},
            () => { this.$timeout(() => viewBlock.is = false); },
            () => { this.$timeout(() => viewBlock.is = false); },
        );
    }

    getCurrentICSize(): InviteCardSizeEntity {
        if(! this._current) {
            throw new Error('Solution is not available yet.');
        }

        return this._current.entity.invite_card_templates[0].entity.card_size;
    }

    get card(): string {
        return this._card;
    }

    set card(value: string) {
        if(this.images.isLoaded(value)) {
            this._card = value;
        }else{
            let viewBlock = this.services.view.status.addLoading();

            this.images.loadImage(value).subscribe(() => {
                this.$timeout(() => {
                    this._card = value;

                    viewBlock.is = false;
                });
            })
        }
    }

    get description(): LocalizedString[] {
        return this._description;
    }

    set description(value: LocalizedString[]) {
        this._description = value;
    }
    get title(): LocalizedString[] {
        return this._title;
    }

    set title(value: LocalizedString[]) {
        this._title = value;
    }

    get texture(): EnvelopeTexture {
        if(this._current) {
            return this.services.resources.textures.filter(t => t.entity.id === this._textureVariant.entity.owner_envelope_texture_id)[0];
        }else{
            return undefined;
        }
    }

    set texture(texture: EnvelopeTexture) {
        this.textureVariant = texture.entity.definitions[0];

        this.rChangeModel.next(this);
    }

    get textureVariant(): EnvelopeTextureDefinition {
        return this._textureVariant;
    }

    set textureVariant(variant: EnvelopeTextureDefinition) {
        let viewBlock = this.services.view.status.addLoading();
        let resource = this.getTextureVariantOf(variant);

        this.images.loadImages([
            resource.attachments.front.link.url,
            resource.attachments.back.link.url,
            resource.attachments.cornerFront.link.url,
            resource.attachments.cornerBack.link.url,
        ]).subscribe(
            () => {
                this.$timeout(() => {
                    this._textureVariant = variant;
                    viewBlock.is = false;

                    this.rChangeModel.next(this);
                });
            },
        );
    }

    get textureVariantResource(): EnvelopeTextureDefinitionResource {
        return this.getTextureVariantOf(this.textureVariant);
    }

    private getTextureVariantOf(textureVariant: EnvelopeTextureDefinition): EnvelopeTextureDefinitionResource
    {
        let icSize = this.getCurrentICSize();
        let resource = textureVariant.entity.resources.filter(resource => {
            return resource.width === icSize.width
                && resource.height === icSize.height;
        })[0];

        if(! resource) {
            throw new Error("No resource available for current invite card size");
        }

        return resource;
    }

    get backdrop(): EnvelopeBackdrop {
        return this.services.resources.backdrops.filter(b => b.entity.id === this.backdropVariant.entity.owner_envelope_backdrop_id)[0];
    }

    set backdrop(value: EnvelopeBackdrop) {
        let viewBlock = this.services.view.status.addLoading();

        this.backdropVariant = value.entity.definitions[0];

        this.images.loadImages(value.entity.definitions.map(def => def.entity.preview.link.url)).subscribe(
            () => {},
            () => { this.$timeout(() => { viewBlock.is = false; })},
            () => {
                this.$timeout(() => {
                    viewBlock.is = false;

                    this.rChangeModel.next(this);
                });
            }
        )
    }


    get backdropVariant(): EnvelopeBackdropDefinition {
        return this._backdropVariant;
    }

    set backdropVariant(value: EnvelopeBackdropDefinition) {
        let viewBlock = this.services.view.status.addLoading();

        this._backdropVariant = value;

        this.images.loadImage(value.entity.resource.link.url).subscribe(
            () => {},
            () => { this.$timeout(() => { viewBlock.is = false; })},
            () => {
                this.$timeout(() => {
                    viewBlock.is = false;

                    this.rChangeModel.next(this);
                });
            }
        )
    }

    get markVariant(): EnvelopeMarkDefinition {
        return this._markVariant;
    }

    set markVariant(value: EnvelopeMarkDefinition) {

        this._markVariant = value;
        this.rChangeModel.next(this);
    }

    set mark(value: EnvelopeMarks) {
        this._markVariant = value.entity.definitions[0];
        this.rChangeModel.next(this);
    }

    get mark(): EnvelopeMarks {
        return this.services.resources.marks.filter(m => {
            return m.entity.definitions
                .map(d => d.entity.owner_envelope_marks_id)
                .filter(id => id === this._markVariant.entity.id)
                .length > 0;
        })[0];
    }

    get patternVariant(): EnvelopePatternDefinition {
        return this._patternVariant;
    }

    set patternVariant(value: EnvelopePatternDefinition) {
        let viewBlock = this.services.view.status.addLoading();

        this.images.loadImage(value.entity.attachment.link.url).subscribe(
            () => {},
            () => {
                this.$timeout(() => {
                    viewBlock.is = false;
                });
            },
            () => {
                this.$timeout(() => {
                    viewBlock.is = false;

                    this._patternVariant = value;
                    this.rChangeModel.next(this);
                });
            }
        )
    }

    get pattern(): EnvelopePattern {
        return this.services.resources.patterns.filter(p => p.entity.id === this.patternVariant.entity.envelope_pattern_id)[0];
    }

    set pattern(value: EnvelopePattern) {
        let viewBlock = this.services.view.status.addLoading();

        this.images.loadImages(value.entity.definitions.map(def => def.entity.preview.link.url)).subscribe(
            () => {},
            () => { //noinspection TypeScriptValidateTypes
                this.$timeout(() => { viewBlock.is = false; }); },
            () => {
                this.$timeout(() => {
                    let color: EnvelopeColor;
                    let icSize = this.getCurrentICSize();

                    if(this._patternVariant) {
                        color = this.services.resources.colors.filter(compare => {
                            return this.patternVariant.entity.envelope_color_id === compare.entity.id
                        })[0];
                    }

                    if(! color) {
                        color = this.services.resources.colors[0];
                    }

                    let guessVariant = value.entity.definitions.filter(def => {
                        return (def.entity.envelope_color_id === color.entity.id)
                            && (def.entity.width === icSize.width)
                            && (def.entity.height === icSize.height);
                    });

                    if(guessVariant.length) {
                        this.patternVariant = guessVariant[0];
                    }else{
                        this.patternVariant = value.entity.definitions[0];
                    }

                    viewBlock.is = false;
                    this.rChangeModel.next(this);
                });
            }
        )
    }
}