import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {EnvelopePatternDefinition} from "../../../../../../../../../../../../definitions/src/definitions/envelope/pattern-definition/entity/EnvelopePattern";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";
import {EnvelopeColor} from "../../../../../../../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-pattern-color',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarPatternColor implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopePatternDefinition> = new ListWindow<EnvelopePatternDefinition>(EnvelopeEditorV1ToolbarPatternColor.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;
    private changeSubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe((model) => {
            if(this.changeSubscription) {
                this.changeSubscription.unsubscribe();
            }

            this.$timeout(() => { this.update(); });

            this.changeSubscription = model.rChangeModel.subscribe(next => {
                this.$timeout(() => { this.update(); });
            });
        });
    }

    update(): void {
        if(this.services.model.patternVariant) {
            let icSize = this.services.model.getCurrentICSize();

            this.window.items = this.services.model.pattern.entity.definitions.filter(input => {
                return (input.entity.width === icSize.width)
                    && (input.entity.height === icSize.height);
            });

            this.window.scrollTo(this.services.model.patternVariant, (item, compare) => {
                return item.entity.sid === compare.entity.sid;
            });
        }
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }

        if(this.changeSubscription) {
            this.changeSubscription.unsubscribe();
        }
    }

    click(variant: EnvelopePatternDefinition): void {
        this.services.model.patternVariant = variant;
    }

    isActive(variant: EnvelopePatternDefinition): boolean {
        return this.services.model.isAvailable() && this.services.model.patternVariant.entity.sid === variant.entity.sid;
    }

    getPatternVariantCSS(variant: EnvelopePatternDefinition): any {
        return {
            'background-color': this.services.resources.getEnvelopeColorById(variant.entity.envelope_color_id).entity.hex_code,
        }
    }
}