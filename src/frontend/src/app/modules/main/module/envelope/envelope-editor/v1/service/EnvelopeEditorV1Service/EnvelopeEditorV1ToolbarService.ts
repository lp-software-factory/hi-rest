import {Service} from "../../../../../../decorators/Service";

import {EnvelopeEditorV1Service} from "../EnvelopeEditorV1Service";
import {EnvelopeSide} from "./EnvelopeEditorV1ViewPortService";

export enum EnvelopeToolbar {
    Texture = <any>"texture",
    TextureVariant = <any>"texture-variant",
    Mark = <any>"mark",
    Font = <any>"font",
    Backdrop = <any>"backdrop",
    BackdropVariant = <any>"backdrop-variant",
    Pattern = <any>"pattern",
    PatternColor = <any>"pattern-color",
}

export const ToolbarSidesMap = [
    { id: EnvelopeToolbar.Texture, side: EnvelopeSide.Front },
    { id: EnvelopeToolbar.TextureVariant, side: EnvelopeSide.Front },
    { id: EnvelopeToolbar.Font, side: EnvelopeSide.Front },
    { id: EnvelopeToolbar.Mark, side: EnvelopeSide.Front },
    { id: EnvelopeToolbar.Backdrop, side: EnvelopeSide.Back },
    { id: EnvelopeToolbar.BackdropVariant, side: EnvelopeSide.Back },
    { id: EnvelopeToolbar.Pattern, side: EnvelopeSide.Back },
    { id: EnvelopeToolbar.PatternColor, side: EnvelopeSide.Back },
];

@Service({
    injects: [
        '$timeout',
    ]
})
export class EnvelopeEditorV1ToolbarService
{
    public current: EnvelopeToolbar = EnvelopeToolbar.Texture;
    public services: EnvelopeEditorV1Service;

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {
        this.$timeout(() => {
            this.services.view.sideSwitchReplay.subscribe(next => {
                this.$timeout(() => {
                    this.current = this.getDefaultToolbarForSide(next);
                });
            });
        });
    }

    switchSide(): void {
        this.services.view.toggleSide();
    }

    switchToolbar(id: EnvelopeToolbar): void {
        this.current = id;
    }

    isActive(id: EnvelopeToolbar): boolean {
        return this.current === id;
    }

    shouldBeVisible(id: EnvelopeToolbar): boolean {
        return this.isToolbarForSide(id, this.services.view.side);
    }

    isToolbarForSide(id: EnvelopeToolbar, side: EnvelopeSide): boolean {
        return ToolbarSidesMap.filter(input => input.id === id)[0].side === side;
    }

    getDefaultToolbarForSide(side: EnvelopeSide): EnvelopeToolbar {
        switch(side) {
            default:
                throw new Error(`Unknown side "${side}"`);

            case EnvelopeSide.Front:
                return EnvelopeToolbar.Texture;

            case EnvelopeSide.Back:
                return EnvelopeToolbar.Backdrop;
        }
    }
}