import {Component} from "../../../../../../../../../../decorators/Component";

import {EnvelopeEditorV1Service} from "../../../../../../service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1ToolbarFontService} from "../../service";
import {ListWindow} from "../../../../../../../../../../helpers/ListWindow";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-text-size',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1ToolbarFontService,
        EnvelopeEditorV1Service,
    ],
})
export class EnvelopeEditorV1ToolbarTextSize implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 12;

    private window: ListWindow<number> = new ListWindow<number>(EnvelopeEditorV1ToolbarTextSize.LIST_WINDOW_LENGTH);

    constructor(
        private $timeout: angular.ITimeoutService,
        private toolbar: EnvelopeEditorV1ToolbarFontService,
        private service: EnvelopeEditorV1Service,
    ) {}

    $onInit(): void {
        this.window.items = this.listAvailableTextSizes();
        this.window.scrollTo(this.service.model.config.recipient.textSize, (item, compare) => item === compare);
    }

    listAvailableTextSizes(): number[] {
        let sizes = [
            10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 23, 26, 28, 30, 32, 35, 37, 39, 41, 43, 46, 48,
        ];

        for(let s = 50; s <= 72; s += 2) {
            sizes.push(s);
        }

        return sizes;
    }

    isActive(textSize: number): boolean {
        return this.service.model.config.recipient.textSize == textSize;
    }

    click(textSize: number): void {
        this.service.model.config.recipient.textSize = textSize;
    }
}