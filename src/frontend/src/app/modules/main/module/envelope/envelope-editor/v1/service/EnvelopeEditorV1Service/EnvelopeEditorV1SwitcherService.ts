import {Service} from "../../../../../../decorators/Service";

import {Subject} from "rxjs";
import {EnvelopeEditorV1Service} from "../EnvelopeEditorV1Service";

export interface EnvelopeEditorV1Switcher
{
    enabled: boolean,
}

@Service({
    injects: [],
})
export class EnvelopeEditorV1SwitcherService
{
    public current: EnvelopeEditorV1Switcher;
    public services: EnvelopeEditorV1Service;

    public switchSubject: Subject<boolean> = new Subject<boolean>();

    enableSwitcher(switcher: EnvelopeEditorV1Switcher): void {
        this.current = switcher;
        this.switchSubject.next(this.current.enabled);
    }

    disableSwitcher(): void {
        this.current = undefined;
    }

    isSwitcherEnabled(): boolean {
        return this.current !== undefined;
    }

    toggleSwitcher(): boolean {
        if(this.current === undefined) {
            throw new Error('Switcher is undefined');
        }else{
            this.current.enabled = !this.current.enabled;
            this.switchSubject.next(this.current.enabled);

            return this.current.enabled;
        }
    }

    getSwitcherValue(): boolean {
        if(this.current === undefined) {
            throw new Error('Switcher is undefined');
        }else{
            return this.current.enabled;
        }
    }
}