import {Component} from "../../../../../../decorators/Component";

import {EnvelopeToolbar} from "../../service/EnvelopeEditorV1Service/EnvelopeEditorV1ToolbarService";
import {EnvelopeEditorV1Service} from "../../service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1Switcher} from "../../service/EnvelopeEditorV1Service/EnvelopeEditorV1SwitcherService";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar',
    template: require('./template.jade'),
    styles: [
        require('./styles/style.icons.shadow.scss'),
        require('./styles/style.toolbar-flexbox-main.shadow.scss'),
        require('./styles/style.toolbar-flexbox-big-previews.shadow.scss'),
        require('./styles/style.toolbar-flexbox-previews-only.shadow.scss'),
        require('./styles/style.toolbar-flexbox-image-select.shadow.scss'),
    ],
    injects: [
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1Toolbar
{
    constructor(
        private services: EnvelopeEditorV1Service,
    ) {}

    switchSide(): void {
        this.services.toolbar.switchSide();
    }

    switchToolbar(id: EnvelopeToolbar): void {
        this.services.toolbar.switchToolbar(id);
    }

    isActive(id: EnvelopeToolbar): boolean {
        return this.services.toolbar.isActive(id);
    }

    isCurrent(id: EnvelopeToolbar): boolean {
        return this.services.toolbar.isActive(id);
    }

    shouldBeVisible(id: EnvelopeToolbar): boolean {
        return this.services.toolbar.shouldBeVisible(id);
    }

    isSwitcherEnabled(): boolean {
        return this.services.switcher.isSwitcherEnabled();
    }

    getSwitcher(): EnvelopeEditorV1Switcher {
        return this.services.switcher.current;
    }

    isSwitcherActive(): boolean {
        return this.services.switcher.getSwitcherValue();
    }

    isVisible(): boolean {
        return !this.services.switcher.isSwitcherEnabled() || this.services.switcher.getSwitcherValue();
    }

    toggleSwitcher(): void {
        this.services.switcher.toggleSwitcher();
    }

    preview(): void {
        this.services.preview();
    }

    submit(): void {
        this.services.save();
    }
}