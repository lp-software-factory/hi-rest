import {Component} from "../../../../../../decorators/Component";

import {EnvelopeAnimationRequest} from "../../service/EnvelopeAnimationService";
import {Observable} from "rxjs";

@Component({
    selector: 'hi-envelope-animation',
    template: require('./template.jade'),
    styles: [
        require('./styles/style.shadow.scss'),
        require('./styles/animation.style.shadow.scss'),
    ],
    bindings: {
        'request': '<',
    },
    injects: [
        '$timeout',
        '$window',
    ]
})
export class EnvelopeAnimation implements ng.IComponentController
{
    private ready: boolean = false;
    private request: EnvelopeAnimationRequest;

    private textureURL: string;
    private backdropURL: string;

    constructor(
        private $timeout: ng.ITimeoutService,
        private $window: ng.IWindowService,
    ) {}

    $onChanges(): void {
        this.ready = false;

        if(this.request) {
            let icSize = this.request.invite.entity.card_size;
            let resources = this.request.envelope.entity.envelope_texture_definition.entity.resources;

            this.backdropURL = this.request.envelope.entity.envelope_backdrop_definition.entity.resource.link.url;
        }
    }

    isReady(): boolean {
        return this.ready;
    }

    getBackdropCSS(): any {
        return {
            'background-image': `url("${encodeURI(this.backdropURL)}")`,
        };
    }
}