import {HIModule} from "../../../../../modules";

import {V1} from "./v1/index";

export const HI_MAIN_ENVELOPE_ANIMATION_MODULE: HIModule = {
    components: [],
    services: [],
};

V1.component.forEach(c => HI_MAIN_ENVELOPE_ANIMATION_MODULE.components.push(c));
V1.services.forEach(s => HI_MAIN_ENVELOPE_ANIMATION_MODULE.services.push(s));