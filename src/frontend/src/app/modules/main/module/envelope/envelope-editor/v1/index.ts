import {EnvelopeEditorV1} from "./component/EnvelopeEditorV1/index";

import {EnvelopeEditorV1Service} from "./service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1Toolbar} from "./component/EnvelopeEditorV1Toolbar/index";
import {EnvelopeEditorV1ToolbarBackdrop} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarBackdrop/index";
import {EnvelopeEditorV1ToolbarBackdropVariant} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarBackdropVariant/index";
import {EnvelopeEditorV1ToolbarFont} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarFont/index";
import {EnvelopeEditorV1ToolbarMark} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarMark/index";
import {EnvelopeEditorV1ToolbarPattern} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarPattern/index";
import {EnvelopeEditorV1ToolbarPatternColor} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarPatternColor/index";
import {EnvelopeEditorV1ToolbarTexture} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarTexture/index";
import {EnvelopeEditorV1ToolbarTextureVariant} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarTextureVariant/index";
import {EnvelopEditorV1ViewPort, EnvelopEditorV1ViewPortCSSHelper} from "./component/EnvelopeEditorV1ViewPort/index";
import {EnvelopeEditorV1Mark} from "./component/EnvelopeEditorV1ViewPort/component/EnvelopeEditorV1Mark/index";
import {EnvelopeEditorV1Recipient} from "./component/EnvelopeEditorV1ViewPort/component/EnvelopeEditorV1Recipient/index";
import {EnvelopeEditorV1ToolbarFontService} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarFont/service";
import {EnvelopeEditorV1ToolbarTextColor} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarFont/component/EnvelopeEditorV1ToolbarTextColor/index";
import {EnvelopeEditorV1ToolbarTextSize} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarFont/component/EnvelopeEditorV1ToolbarTextSize/index";
import {EnvelopeEditorV1ToolbarFontFamily} from "./component/EnvelopeEditorV1Toolbar/component/EnvelopeEditorV1ToolbarFont/component/EnvelopeEditorV1ToolbarFontFamily/index";
import {EnvelopeEditorV1SwitcherService} from "./service/EnvelopeEditorV1Service/EnvelopeEditorV1SwitcherService";
import {EnvelopeEditorV1ModelService} from "./service/EnvelopeEditorV1Service/EnvelopeEditorV1ModelService";
import {EnvelopeEditorV1ToolbarService} from "./service/EnvelopeEditorV1Service/EnvelopeEditorV1ToolbarService";
import {EnvelopeEditorV1ViewPortService} from "./service/EnvelopeEditorV1Service/EnvelopeEditorV1ViewPortService";
import {EnvelopeEditorV1ResourcesService} from "./service/EnvelopeEditorV1Service/EnvelopeEditorV1ResourcesService";
import {EnvelopeEditorV1IsNotEnabled} from "./component/EnvelopeEditorV1IsNotEnabled/index";

export const V1 = {
    component: [
        EnvelopeEditorV1,
        EnvelopeEditorV1Toolbar,
        EnvelopeEditorV1ToolbarBackdrop,
        EnvelopeEditorV1ToolbarBackdropVariant,
        EnvelopeEditorV1ToolbarFont,
        EnvelopeEditorV1ToolbarMark,
        EnvelopeEditorV1ToolbarPattern,
        EnvelopeEditorV1ToolbarPatternColor,
        EnvelopeEditorV1ToolbarTexture,
        EnvelopeEditorV1ToolbarTextureVariant,
        EnvelopEditorV1ViewPort,
        EnvelopeEditorV1Mark,
        EnvelopeEditorV1Recipient,
        EnvelopeEditorV1ToolbarFontFamily,
        EnvelopeEditorV1ToolbarTextColor,
        EnvelopeEditorV1ToolbarTextSize,
        EnvelopeEditorV1IsNotEnabled,
    ],
    services: [
        EnvelopeEditorV1Service,
        EnvelopEditorV1ViewPortCSSHelper,
        EnvelopeEditorV1ToolbarFontService,
        EnvelopeEditorV1SwitcherService,
        EnvelopeEditorV1ModelService,
        EnvelopeEditorV1ToolbarService,
        EnvelopeEditorV1ViewPortService,
        EnvelopeEditorV1ResourcesService,
    ],
};