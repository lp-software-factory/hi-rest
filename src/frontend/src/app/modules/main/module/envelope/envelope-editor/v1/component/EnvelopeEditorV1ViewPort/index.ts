import {Service} from "../../../../../../decorators/Service";
import {Component} from "../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../service/EnvelopeEditorV1Service";
import {EnvelopeSide} from "../../service/EnvelopeEditorV1Service/EnvelopeEditorV1ViewPortService";
import {EnvelopeEditorV1ModelService} from "../../service/EnvelopeEditorV1Service/EnvelopeEditorV1ModelService";
import {EnvelopeTextureDefinitionResource} from "../../../../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";

@Service({
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopEditorV1ViewPortCSSHelper
{
    private changeSubscription: Subscription;

    private images: {
        invite: string|undefined,
        front: string|undefined,
        back: string|undefined,
        cornerFront: string|undefined,
        cornerBack: string|undefined,
    } = {
        invite: undefined,
        front: undefined,
        back: undefined,
        cornerFront: undefined,
        cornerBack: undefined,
    };

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service,
    ) {
        this.services.model.ready.subscribe(() => {
            this.update();

            if(this.changeSubscription) {
                this.changeSubscription.unsubscribe();
            }

            this.changeSubscription = this.services.model.rChangeModel.subscribe((model: EnvelopeEditorV1ModelService) => {
                this.update();
            });
        });
    }

    update(): void {
        let model = this.services.model;

        if(model.isAvailable()) {
            this.$timeout(() => {
                if(model.textureVariant) {
                    let resource = model.textureVariantResource;

                    this.images = {
                        invite: this.services.model.current.entity.invite_card_templates[0].entity.preview.variants['orig'].public_path,
                        front: resource.attachments.front.link.url,
                        back: resource.attachments.back.link.url,
                        cornerFront: resource.attachments.cornerFront.link.url,
                        cornerBack: resource.attachments.cornerBack.link.url,
                    };
                }
            });
        }
    }

    isBack(): boolean {
        return this.services.view.side === EnvelopeSide.Back;
    }

    isFront(): boolean {
        return this.services.view.side === EnvelopeSide.Front;
    }

    getViewCSS(): any {
        let icSize = this.services.model.getCurrentICSize();

        if(this.isBack()) {
            return {
                "width": icSize.width + EnvelopEditorV1ViewPort.BACKDROP_MARGIN_W + "px",
                "height": icSize.height + Math.ceil(icSize.height / 2) +  EnvelopEditorV1ViewPort.BACKDROP_MARGIN_H + "px",
            };
        }else{
            return {
                "width": icSize.width + "px",
                "height": icSize.height + "px",
            };
        }
    }

    getConvertBackCSS(): any {
        let icSize = this.services.model.getCurrentICSize();

        return {
            "width": icSize.width + "px",
            "height": icSize.height + "px",
        };
    }

    getConvertCSS(): any {
        let icSize = this.services.model.getCurrentICSize();

        return {
            "width": icSize.width + "px",
            "height": icSize.height + "px",
        };
    }

    getBackdropCSS(): any {
        if(this.services.model.isAvailable() && this.services.model.backdropVariant) {
            return {
                'background-image': `url("${this.services.model.backdropVariant.entity.resource.link.url}")`,
            };
        }else{
            return {};
        }
    }

    getInviteCardImagePreview(): string {
        return this.images.invite;
    }
}

@Component({
    selector: 'hi-envelope-editor-v1-viewport',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        EnvelopeEditorV1Service,
        EnvelopEditorV1ViewPortCSSHelper,
    ]
})
export class EnvelopEditorV1ViewPort implements ng.IComponentController
{
    public static BACKDROP_MARGIN_W = 80;
    public static BACKDROP_MARGIN_H = 50;

    constructor(
        private services: EnvelopeEditorV1Service,
        private css: EnvelopEditorV1ViewPortCSSHelper,
    ) {}

    getCurrentTextureResource(): EnvelopeTextureDefinitionResource {
        return this.services.model.textureVariantResource;
    }

    getPatternImageURL(): string {
        return this.services.model.patternVariant.entity.attachment.link.url;
    }

    isTextureVariantAvailable(): boolean {
        return this.services.model.isAvailable() && !!this.services.model.textureVariant;
    }

    isBlocked(): boolean {
        return this.services.view.status.isLoading();
    }

    isBack(): boolean {
        return this.services.view.side === EnvelopeSide.Back;
    }

    isFront(): boolean {
        return this.services.view.side === EnvelopeSide.Front;
    }
}