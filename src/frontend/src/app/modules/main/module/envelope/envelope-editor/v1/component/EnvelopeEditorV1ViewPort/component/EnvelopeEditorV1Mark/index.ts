import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {AsyncImageLoaderService} from "../../../../../../../../service/AsyncImageLoaderService";
import {EnvelopeEditorV1ModelService} from "../../../../service/EnvelopeEditorV1Service/EnvelopeEditorV1ModelService";

@Component({
    selector: 'hi-envelope-editor-v1-mark',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        AsyncImageLoaderService,
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1Mark implements ng.IComponentController
{
    public static MARK_STAMP_URL = require('./resources/mark_stamp.png');
    public static MARK_HOLDER_URL = require('./resources/mark_holder.png');

    private current: string;
    private position: { x: number, y: number };

    private ready: boolean = false;
    private visible: boolean = false;

    private readySubscription: Subscription;
    private changeSubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private images: AsyncImageLoaderService,
        private services: EnvelopeEditorV1Service,
    ) {}

    $onInit(): void {
        if(! this.images.isLoaded(EnvelopeEditorV1Mark.MARK_STAMP_URL)) {
            let loading = this.services.view.status.addLoading();

            this.images.loadImages([
                EnvelopeEditorV1Mark.MARK_STAMP_URL,
                EnvelopeEditorV1Mark.MARK_HOLDER_URL,
            ]).subscribe(
                () => {},
                () => { this.$timeout(() => { loading.is = false; }) },
                () => { this.$timeout(() => { loading.is = false; this.ready = true;}) },
            );
        }

        this.readySubscription = this.services.model.ready.subscribe((model: EnvelopeEditorV1ModelService) => {
            if(this.changeSubscription) {
                this.changeSubscription.unsubscribe();
            }

            this.changeSubscription = model.rChangeModel.subscribe(() => {
                this.update();
            });
        });

        this.update();
    }

    update(): void {
        let model = this.services.model;

        if(model.isAvailable()) {
            if(model.markVariant) {
                let image = model.markVariant.entity.attachment.link.url;

                if(! this.images.isLoaded(image)) {
                    let loading = this.services.view.status.addLoading();

                    this.visible = false;

                    this.images.loadImage(image).subscribe(
                        () => {},
                        () => { this.$timeout(() => { loading.is = false; }) },
                        () => { this.$timeout(() => {
                            loading.is = false;

                            this.visible = true;
                            this.current = image;
                        })},
                    );
                }else{
                    this.ready = true;
                    this.visible = true;
                    this.current = image;
                }
            }

            if(model.textureVariant) {
                this.position = {
                    x: model.textureVariantResource.stamp.x,
                    y: model.textureVariantResource.stamp.y,
                }
            }
        }
    }


    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }

        if(this.changeSubscription) {
            this.changeSubscription.unsubscribe();
        }
    }

    getMarkContainerCSS(): any {
        if(this.position) {
            return {
                "top": this.position.y + 'px',
                "left": this.position.x + 'px',
            }
        }else{
            return {
                "display": "none",
            }
        }
    }

    getHappyInviteStamp(): string {
        return EnvelopeEditorV1Mark.MARK_STAMP_URL;
    }
}