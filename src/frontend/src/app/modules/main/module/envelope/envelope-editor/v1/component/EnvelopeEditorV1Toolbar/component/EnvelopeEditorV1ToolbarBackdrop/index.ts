import {Component} from "../../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EnvelopeEditorV1Service} from "../../../../service/EnvelopeEditorV1Service";
import {ListWindow} from "../../../../../../../../helpers/ListWindow";
import {EnvelopeBackdrop} from "../../../../../../../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";

@Component({
    selector: 'hi-envelope-editor-v1-toolbar-backdrop',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorV1ToolbarBackdrop implements ng.IComponentController
{
    public static LIST_WINDOW_LENGTH = 10;

    private window: ListWindow<EnvelopeBackdrop> = new ListWindow<EnvelopeBackdrop>(EnvelopeEditorV1ToolbarBackdrop.LIST_WINDOW_LENGTH);

    private readySubscription: Subscription;

    constructor(
        private $timeout: angular.ITimeoutService,
        private services: EnvelopeEditorV1Service,
    ) {}

    $onInit(): void {
        this.readySubscription = this.services.model.ready.subscribe(next => {
            //noinspection TypeScriptValidateTypes
            this.$timeout(() => {
                this.window.items = this.services.resources.backdrops;
                this.window.scrollTo(next.backdrop, (item, compare) => {
                    return item.entity.id === compare.entity.id;
                })
            });
        })
    }

    $onDestroy(): void {
        if(this.readySubscription) {
            this.readySubscription.unsubscribe();
        }
    }

    click(backdrop: EnvelopeBackdrop): void {
        this.services.model.backdrop = backdrop;
    }

    isActive(backdrop: EnvelopeBackdrop): boolean {
        return this.services.model.isAvailable() && this.services.model.backdrop.entity.id === backdrop.entity.id;
    }

    getBackdropCSS(backdrop: EnvelopeBackdrop): any {
        return {
            'background-image': `url("${encodeURI(backdrop.entity.preview.link.url)}")`,
        }
    }
}