import {Service} from "../../../../../../decorators/Service";

import {EnvelopeEditorV1Service} from "../EnvelopeEditorV1Service";

@Service({
    injects: [
    ]
})
export class EnvelopeEditorV1RevertService
{
    public services: EnvelopeEditorV1Service;
}