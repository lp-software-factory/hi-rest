import {Service} from "../../../../../../decorators/Service";

import {ReplaySubject} from "rxjs";
import {EnvelopeEditorV1Service} from "../EnvelopeEditorV1Service";
import {LoadingManager} from "../../../../../common/helpers/LoadingManager";

export enum EnvelopeSide {
    Front = <any>"front",
    Back = <any>"back",
}

@Service({
    injects: [
    ]
})
export class EnvelopeEditorV1ViewPortService
{
    public services: EnvelopeEditorV1Service;

    private _side: EnvelopeSide = EnvelopeSide.Front;
    public status: LoadingManager = new LoadingManager();

    public sideSwitchReplay: ReplaySubject<EnvelopeSide> = new ReplaySubject<EnvelopeSide>();

    constructor() {}

    toggleSide(): EnvelopeSide {
       if(this._side === EnvelopeSide.Front) {
           this.side = EnvelopeSide.Back;
       }else{
           this.side = EnvelopeSide.Front;
       }

       return this._side;
    }

    get side(): EnvelopeSide {
        return this._side;
    }

    set side(value: EnvelopeSide) {
        this._side = value;
        this.sideSwitchReplay.next(value);
    }
}