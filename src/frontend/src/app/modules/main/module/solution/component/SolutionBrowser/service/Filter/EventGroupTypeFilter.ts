import {Service} from "../../../../../../decorators/Service";

import {localizedToString} from "../../../../../locale/functions/localizedToString";
import {FilterChoice, FilterType, Filter} from "../FilterService";
import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EventTypeGroupEntity} from "../../../../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {SolutionRepository} from "../../../../repository/SolutionRepository";
import {ITranslateService, ITranslateServiceInterface} from "../../../../../i18n/service/TranslationService";
import {IComponentQueryParams} from "../../../../../../../../../../typings-custom/angular-component-router";
import {SolutionQuery} from "../../../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";

@Service({
    injects: [
        SolutionRepository,
        ITranslateService,
    ]
})
export class EventTypeGroupFilter implements Filter
{
    public id = FilterType.EventTypeGroup;

    private defaultTitle: string = 'Event Group Type';
    private current: EventTypeGroupEntity;
    private isActivatedFlag: boolean = false;

    constructor(
        public repository: SolutionRepository,
        private translate: ITranslateServiceInterface,
    ) {
        this.translate('hi.main.solution.browser.filters.event-group-type').then(res => this.defaultTitle = res);
    }

    getCurrent(): FilterChoice<any> {
        return {
            id: this.current.id,
            title: localizedToString(this.current.title),
            entity: this.current
        };
    }

    applyFromQueryParams(qp: IComponentQueryParams): boolean {
        if(qp.hasOwnProperty(this.id)) {
            let id = parseInt(qp[this.id], 10);
            let entity = this.repository.tables.event_type_groups.getById(id);

            this.isActivatedFlag = true;
            this.current = entity.entity;

            return true;
        }else{
            return false;
        }
    }

    applyCriteria(carry: SolutionQuery): boolean {
        if(this.isActivated()) {
            carry.criteria.event_type_group_ids = [
                this.getCurrent().id
            ];

            return true;
        }else{
            return false;
        }
    }

    activate(choice: FilterChoice<any>) {
        this.current = this.repository.tables.event_type_groups.getById(choice.id).entity;
        this.isActivatedFlag = true;
    }

    deactivate() {
        this.isActivatedFlag = false;
    }

    isActivated(): boolean {
        return this.isActivatedFlag;
    }

    getTitle(): string {
        if(this.isActivated()) {
            return localizedToString(this.current.title);
        }else{
            return this.defaultTitle;
        }
    }

    getIcon(): string {
        return 'icon icon-flag';
    }

    filter(input: Array<Solution>): Array<Solution> {
        return input.filter(entity => entity.entity.invite_card_templates[0].entity.family.event_type.group.id === this.current.id);
    }

    list(): FilterChoice<any>[] {
        return this.repository.tables.event_type_groups.getAll().map(group => {
            return {
                id: group.entity.id,
                title: localizedToString(group.entity.title),
                entity: group
            };
        })
    }
}