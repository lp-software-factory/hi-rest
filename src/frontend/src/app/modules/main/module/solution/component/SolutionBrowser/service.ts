import {Service} from "../../../../decorators/Service";

import {Subject, Observable} from "rxjs";
import {SolutionRepository} from "../../repository/SolutionRepository";
import {Solution} from "../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {SolutionRepositoryResponse200, SolutionRepositorySource, SolutionRepositoryRequest} from "../../../../../../../../definitions/src/definitions/solution/paths/repository";
import {InviteCardTemplate} from "../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {SolutionBrowserFilterService} from "./service/FilterService";
import {SolutionWindowService} from "./service/SolutionWindowService";
import {SolutionQueryFromSign, SolutionQuerySortOrder} from "../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";
import {SolutionRESTService} from "../../../../../../../../definitions/src/services/solution/SolutionRESTService";
import {LoadingManager} from "../../../common/helpers/LoadingManager";

@Service({
    injects: [
        SolutionRepository,
        SolutionBrowserFilterService,
        SolutionRESTService,
        SolutionWindowService,
    ]
})
export class SolutionBrowserService
{
    private source: SolutionRepositorySource = SolutionRepositorySource.Public;
    private filter: any;

    public static $name = 'hiSolutionBrowserService';
    public status: LoadingManager = new LoadingManager();

    public subjectOpenTemplate: Subject<InviteCardTemplate>;
    public subjectUpdates: Subject<Solution[]>;
    public subjectResponses: Subject<SolutionRepositoryResponse200>;

    public results: Solution[] = [];
    public eof: { [hash: string]: boolean } = {};

    constructor(
        public repository: SolutionRepository,
        public filters: SolutionBrowserFilterService,
        private rest: SolutionRESTService,
        public window: SolutionWindowService,
    ) {
        this.subjectResponses = new Subject<SolutionRepositoryResponse200>();
        this.subjectUpdates = new Subject<Solution[]>();
        this.subjectOpenTemplate= new Subject<InviteCardTemplate>();

        this.filters.subjectOnActivateFilter.subscribe(filter => {
            this.window.resetWindow();
        });

        this.filters.subjectOnDeactivateFilter.subscribe(filter => {
            this.window.resetWindow();
        })
    }

    setSource(source: SolutionRepositorySource): void {
        this.source = source;
    }

    getSource(): SolutionRepositorySource {
        return this.source;
    }

    setFilter(mapFilter: any): void {
        this.filter = mapFilter;
    }

    openTemplate(template: InviteCardTemplate): void  {
        this.subjectOpenTemplate.next(template);
    }

    next(): Observable<void> {
        let hash = this.filters.hash();

        let observable = Observable.create(observer => {
            if(this.hasNext()) {
                if(this.window.hasNextPage()) {
                    this.window.addPage();

                    observer.next();
                    observer.complete();
                }else if(this.eof[hash] === undefined) {
                    let from = this.repository.solution_mongo_ids[this.window.lastId()];
                    let limit = (SolutionWindowService.PAGE_SIZE*SolutionWindowService.PAGE_PRELOAD) + /* EOF DETECTOR! */ 1;

                    let status = this.status.addLoading();

                    this.rest.repository(this.getSource(), this.createRepositoryQuery(limit, from)).subscribe(response => {
                        if(response.results.solutions.length < limit) {
                            this.eof[hash] = true;
                        }

                        status.is = false;

                        response.results.solutions.pop();

                        if(response.results.solutions.length > 0) {
                            this.importResponse(response);
                            this.window.addPage();
                        }

                        observer.next();
                        observer.complete();
                    }, error => status.is = false)
                }
            }else{
                observer.next();
                observer.complete();
            }
        }).share();

        observable.subscribe(() => {});

        return observable;
    }

    hasNext(): boolean {
        return this.window.hasNextPage() || this.eof[this.filters.hash()] === undefined;
    }

    update() {
        this.results = this.filters.filter(this.repository.tables.solutions.getAll());

        this.window.setResults(this.results);

        setTimeout(() => {
            this.subjectUpdates.next(this.results);
        }, 0);
    }

    importResponse(response: SolutionRepositoryResponse200): void {
        this.repository.importSolutionRepositoryResponse(response);
        this.subjectResponses.next(response);
        this.update();
    }

    private createRepositoryQuery(limit: number, from: string|undefined): SolutionRepositoryRequest {
        let request: SolutionRepositoryRequest = {
            filter: this.filter,
            query: {
                criteria: {
                    cursor: {
                        limit: limit,
                        from: {
                            sign: SolutionQueryFromSign.LESSER_THAN,
                            _id: from,
                        },
                        sort: [
                            {
                                field: '_id',
                                order: SolutionQuerySortOrder.Desc,
                            }
                        ]
                    }
                }
            }
        };

        this.filters.forEach(filter => filter.applyCriteria(request.query));

        return request;
    }
}