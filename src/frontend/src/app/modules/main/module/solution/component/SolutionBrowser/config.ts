import {InviteCardTemplateBrowserStreamsConfig} from "./component/InviteCardTemplateBrowser/component/InviteCardTemplateBrowserStreams/index";

export interface SolutionBrowserConfiguration
{
    editMode: boolean;
    streams: InviteCardTemplateBrowserStreamsConfig;
}