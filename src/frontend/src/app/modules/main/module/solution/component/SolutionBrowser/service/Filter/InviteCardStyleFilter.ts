import {Service} from "../../../../../../decorators/Service";

import {Filter, FilterType, FilterChoice} from "../FilterService";
import {localizedToString} from "../../../../../locale/functions/localizedToString";
import {InviteCardStyleEntity} from "../../../../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {SolutionRepository} from "../../../../repository/SolutionRepository";
import {ITranslateService, ITranslateServiceInterface} from "../../../../../i18n/service/TranslationService";
import {IComponentQueryParams} from "../../../../../../../../../../typings-custom/angular-component-router";
import {SolutionQuery} from "../../../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";

@Service({
    injects: [
        SolutionRepository,
        ITranslateService,
    ]
})
export class InviteCardStyleFilter implements Filter
{
    public id = FilterType.InviteCardStyle;

    private defaultTitle: string = 'Style';
    private current: InviteCardStyleEntity;
    private isActivatedFlag: boolean = false;

    constructor(
        public repository: SolutionRepository,
        private translate: ITranslateServiceInterface,
    ) {
        this.translate('hi.main.solution.browser.filters.invite-card-style').then(res => this.defaultTitle = res);
    }

    getCurrent(): FilterChoice<any> {
        return {
            id: this.current.id,
            title: localizedToString(this.current.title),
            entity: this.current
        };
    }

    applyFromQueryParams(qp: IComponentQueryParams): boolean {
        if(qp.hasOwnProperty(this.id)) {
            let id = parseInt(qp[this.id], 10);
            let entity = this.repository.tables.invite_card_styles.getById(id);

            this.isActivatedFlag = true;
            this.current = entity.entity;

            return true;
        }else{
            return false;
        }
    }

    applyCriteria(carry: SolutionQuery): boolean {
        if(this.isActivated()) {
            carry.criteria.invite_card_style_ids = [
                this.getCurrent().id
            ];

            return true;
        }else{
            return false;
        }
    }

    activate(choice: FilterChoice<any>) {
        this.current = this.repository.tables.invite_card_styles.getAll().filter(style => style.entity.id === choice.id)[0].entity;
        this.isActivatedFlag = true;
    }

    deactivate() {
        this.isActivatedFlag = false;
    }

    isActivated(): boolean {
        return this.isActivatedFlag;
    }

    getTitle(): string {
        if(this.isActivated()) {
            return localizedToString(this.current.title);
        }else{
            return this.defaultTitle;
        }
    }

    getIcon(): string {
        return 'icon icon-Painting';
    }

    filter(input: Array<Solution>): Array<Solution> {
        return input.filter(entity => entity.entity.invite_card_templates[0].entity.family.style.entity.id === this.current.id);
    }

    list(): FilterChoice<any>[] {
        return this.repository.tables.invite_card_styles.getAll().map(style => {
            return {
                id: style.entity.id,
                title: localizedToString(style.entity.title),
                entity: style
            };
        })
    }
}