import {Component} from "../../../../../../decorators/Component";

import {FilterType, SolutionBrowserFilterService, Filter, FilterChoice} from "../../service/FilterService";

@Component({
    selector: 'hi-solution-browser-filters',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        SolutionBrowserFilterService,
    ]
})
export class SolutionBrowserFilters implements ng.IComponentController
{
    current: FilterType = FilterType.EventTypeGroup;
    listChoices: FilterChoice<any>[] = [];

    constructor(
        private $timeout: angular.ITimeoutService,
        private service: SolutionBrowserFilterService
    ) {
        service.subjectOnActivateFilter.subscribe(() => { setTimeout(() => this.$timeout(() => {
            this.updateListChoices();
        }), 50); });

        service.subjectOnDeactivateFilter.subscribe(() => { setTimeout(() => this.$timeout(() => {
            this.updateListChoices();
        }), 50); });
    }

    onActivateFilter(filter: Filter) {
        this.service.subjectOnActivateFilter.next(filter);
    }

    onDeactivateFilter(filter: Filter) {
        this.service.subjectOnDeactivateFilter.next(filter);
    }

    $onInit() {
        this.updateListChoices();
    }

    updateListChoices() {
        this.listChoices = this.getCurrentFilter().list();
    }

    isSelected(filter: Filter): boolean {
        return this.current === filter.id;
    }

    setAsCurrentFilter(filter: Filter) {
        this.current = filter.id;
        this.updateListChoices();
    }

    getCurrentFilter(): Filter {
        return this.service.filters[this.current];
    }

    listFilters(): Filter[] {
        return this.service.listFilters();
    }
}