import {Service} from "../../../../../decorators/Service";

import {Subject} from "rxjs";
import {EventTypeGroupFilter} from "./Filter/EventGroupTypeFilter";
import {EvenTypeFilter} from "./Filter/EventTypeFilter";
import {InviteCardGammaFilter} from "./Filter/InviteCardGammaFilter";
import {InviteCardStyleFilter} from "./Filter/InviteCardStyleFilter";
import {InviteCardSizeFilter} from "./Filter/InviteCardSizeFilter";
import {DesignerFilter} from "./Filter/DesignerFilter";
import {HasPhotoFilter} from "./Filter/HasPhotoFilter";
import {Solution} from "../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {IComponentQueryParams} from "../../../../../../../../../typings-custom/angular-component-router";
import {SolutionQuery} from "../../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";

export enum FilterType {
    EventTypeGroup = <any>"event_type_group",
    EventType = <any>"event_type",
    InviteCardGamma = <any>"invite_card_gamma",
    InviteCardStyle = <any>"invite_card_style",
    InviteCardSize = <any>"invite_card_size",
    HasPhoto = <any>"has_photo",
    Designer = <any>"designer",
}

@Service({
    injects: [
        DesignerFilter,
        EventTypeGroupFilter,
        EvenTypeFilter,
        HasPhotoFilter,
        InviteCardGammaFilter,
        InviteCardSizeFilter,
        InviteCardStyleFilter
    ]
})
export class SolutionBrowserFilterService
{
    public subjectOnActivateFilter: Subject<Filter> = new Subject<Filter>();
    public subjectOnDeactivateFilter: Subject<Filter> = new Subject<Filter>();

    public filters: { [id: string]: Filter} = {};

    constructor(
        private filterDesigner: DesignerFilter,
        private filterEventGroupType: EventTypeGroupFilter,
        private filterEventType: EvenTypeFilter,
        private filterHasPhotoFilter: HasPhotoFilter,
        private filterInviteCardGammaFilter: InviteCardGammaFilter,
        private filterInviteCardSizeFilter: InviteCardSizeFilter,
        private filterInviteCardStyleFilter: InviteCardStyleFilter,
    ) {
        this.filters[FilterType.EventTypeGroup] = filterEventGroupType;
        this.filters[FilterType.EventType] = filterEventType;
        this.filters[FilterType.InviteCardGamma] = filterInviteCardGammaFilter;
        this.filters[FilterType.InviteCardStyle] = filterInviteCardStyleFilter;
        this.filters[FilterType.InviteCardSize] = filterInviteCardSizeFilter;
        this.filters[FilterType.HasPhoto] = filterHasPhotoFilter;
        this.filters[FilterType.Designer] = filterDesigner;

        this.subjectOnActivateFilter.subscribe(filter => this.navigate());
        this.subjectOnDeactivateFilter.subscribe(filter => this.navigate());
    }

    hash(): string {
        let f = {};

        this.forEach(filter => {
            if(filter.isActivated()) {
                f[filter.id] = filter.getCurrent().id;
            }
        });

        return JSON.stringify(f);
    }

    filter(solutions: Solution[]) {
        this.forEach(filter => {
            if(filter.isActivated()) {
                solutions = filter.filter(solutions);
            }
        });

        return solutions;
    }

    forEach(callback: (filter: Filter) => any) {
        for(let id in this.filters) {
            if(this.filters.hasOwnProperty(id)) {
                callback(this.filters[id]);
            }
        }
    }

    listFilterTypes(): FilterType[] {
        return [
            FilterType.EventTypeGroup,
        ];
    }

    listFilters(): Filter[] {
        let result = [];

        for(let i in this.filters) {
            if(this.filters.hasOwnProperty(i)) {
                result.push(this.filters[i]);
            }
        }

        return result;
    }

    navigate() {
        this.sanitize();

        let qp = this.generateFilterQueryParams();
    }

    generateFilterQueryParams() {
        let result: { [filterId: string]: number; } = {};

        for(let id in this.filters) {
            if(this.filters.hasOwnProperty(id)) {
                let filter = this.filters[id];

                if(filter.isActivated()) {
                    result[filter.id] = filter.getCurrent().id;
                }
            }
        }

        return result;
    }

    sanitize() {
        let sanitizers: Function[] = [
            () => {
                let isFilterEventTypeActivated = this.filterEventType.isActivated();
                let isFilterEventTypeGroupActivated = this.filterEventGroupType.isActivated();

                if(isFilterEventTypeActivated && isFilterEventTypeGroupActivated) {

                    let et = this.filterEventType.getCurrent().entity;
                    let etg = this.filterEventGroupType.getCurrent().entity;

                    if(et.group.id !== etg.id) {
                        this.filterEventType.deactivate();

                        isFilterEventTypeActivated = false;
                    }
                }

                if(isFilterEventTypeGroupActivated) {
                    this.filterEventType.listsByEventTypeGroup(this.filterEventGroupType.getCurrent());
                }else{
                    this.filterEventType.listsAll();
                }
            }
        ];

        sanitizers.forEach(f => f());
    }
}

export interface Filter
{
    id: FilterType;
    getCurrent(): FilterChoice<any>;
    applyFromQueryParams(qp: IComponentQueryParams): boolean;
    applyCriteria(carry: SolutionQuery): boolean;
    activate(choice: FilterChoice<any>);
    deactivate();
    isActivated(): boolean;
    getTitle(): string;
    getIcon(): string;
    filter(carry: Array<Solution>): Array<Solution>;
    list(): FilterChoice<any>[];
}

export interface FilterChoice<T>
{
    id: any;
    title: string;
    entity: T;
}