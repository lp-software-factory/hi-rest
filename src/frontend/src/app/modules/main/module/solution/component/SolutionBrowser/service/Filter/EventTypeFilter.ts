import {Service} from "../../../../../../decorators/Service";

import {localizedToString} from "../../../../../locale/functions/localizedToString";
import {FilterChoice, Filter, FilterType} from "../FilterService";
import {EventTypeEntity, EventType} from "../../../../../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";
import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {SolutionRepository} from "../../../../repository/SolutionRepository";
import {ITranslateService, ITranslateServiceInterface} from "../../../../../i18n/service/TranslationService";
import {EventTypeGroup} from "../../../../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {IComponentQueryParams} from "../../../../../../../../../../typings-custom/angular-component-router";
import {SolutionQuery} from "../../../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";

@Service({
    injects: [
        SolutionRepository,
        ITranslateService,
    ]
})
export class EvenTypeFilter implements Filter
{
    public id = FilterType.EventType;

    private defaultTitle: string = 'Event Type';
    private current: EventTypeEntity;
    private isActivatedFlag: boolean = false;
    private etg: EventTypeGroup;

    constructor(
        public repository: SolutionRepository,
        private translate: ITranslateServiceInterface,
    ) {
        this.translate('hi.main.solution.browser.filters.event-type').then(res => this.defaultTitle = res);
    }

    getCurrent(): FilterChoice<any> {
        return {
            id: this.current.id,
            title: localizedToString(this.current.title),
            entity: this.current
        };
    }

    applyCriteria(carry: SolutionQuery): boolean {
        if(this.isActivated()) {
            carry.criteria.event_type_ids = [
                this.getCurrent().id
            ];

            return true;
        }else{
            return false;
        }
    }

    applyFromQueryParams(qp: IComponentQueryParams): boolean {
        if(qp.hasOwnProperty(this.id)) {
            let id = parseInt(qp[this.id], 10);
            let entity = this.repository.tables.event_types.getById(id);

            this.isActivatedFlag = true;
            this.current = entity.entity;

            return true;
        }else{
            return false;
        }
    }

    activate(choice: FilterChoice<any>) {
        let currentET = this.repository.tables.event_types.getById(choice.id);

        this.current = currentET.entity;

        this.isActivatedFlag = true;
    }

    deactivate() {
        this.isActivatedFlag = false;
    }

    listsByEventTypeGroup(etg: EventTypeGroup): void {
        this.etg = etg;
    }

    listsAll(): void {
        this.etg = undefined;
    }

    isActivated(): boolean {
        return this.isActivatedFlag;
    }

    getTitle(): string {
        if(this.isActivated()) {
            return localizedToString(this.current.title);
        }else{
            return this.defaultTitle;
        }
    }

    getIcon(): string {
        return 'icon icon-star';
    }

    filter(input: Array<Solution>): Array<Solution> {
        return input.filter(entity => entity.entity.invite_card_templates[0].entity.family.event_type.id === this.current.id);
    }

    list(): FilterChoice<any>[] {
        let results: Array<EventType> = this.repository.tables.event_types.getAll();

        if(this.etg) {
            results = results.filter(et => {
                return et.entity.group.id === this.etg.entity.id;
            });
        }

        return results.map(group => {
            return {
                id: group.entity.id,
                title: localizedToString(group.entity.title),
                entity: group
            };
        })
    }
}