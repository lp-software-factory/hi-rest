import {SolutionBrowserService} from "../../../../service";
import {Component} from "../../../../../../../../decorators/Component";
import {InviteCardTemplate} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {InviteCardFamily} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {localizedToString} from "../../../../../../../locale/functions/localizedToString";
import {queryImage, QueryTarget} from "../../../../../../../../../../../../definitions/src/definitions/avatar/functions/query";

@Component({
    selector: 'hi-invite-card-template-browser-card',
    template: require('./template.jade'),
    bindings: {
        family: '<',
        onChange: '&',
        onImageClick: '&',
    },
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$window',
        SolutionBrowserService,
    ]
})
export class InviteCardTemplateBrowserCard implements ng.IComponentController
{
    public static DEBUG = false;

    private cssSize: { width: string, height: string };

    public current: InviteCardTemplate;
    public family: InviteCardFamily;

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private service: SolutionBrowserService,
    ) {
        if(! $window['hiEnableInviteCardTemplateBrowserCardDebug']) {
            $window['hiEnableInviteCardTemplateBrowserCardDebug'] = () => {
                $timeout(() => {
                    InviteCardTemplateBrowserCard.DEBUG = true;
                }, 0);
            }
        }
    }

    $onInit(): void {
        let id = this.family.entity.card_size.entity.id;
        let size = this.service.repository.tables.invite_card_sizes.getById(id);

        this.cssSize = {
            "width": size.entity.browser_preview.width + 'px',
            "height": size.entity.browser_preview.height + 'px',
        }
    }

    $onChanges(onChangesObj: angular.IOnChangesObject): void {
        this.current = this.service.repository.tables.invite_card_templates.getById(this.family.entity.templates[0].entity.id);
    }

    isDebugEnabled(): boolean {
        return InviteCardTemplateBrowserCard.DEBUG;
    }

    getId(): number {
        return this.current.entity.id;
    }

    preview(template: InviteCardTemplate) {
        this.current = template;
    }

    getImageURL(): string {
        return this.current.entity.preview.variants['orig'].public_path;
    }

    getFamilyTitle(): string {
        return localizedToString(this.family.entity.title);
    }

    getImageCSS(): any {
        let id = this.family.entity.card_size.entity.id;
        let size = this.service.repository.tables.invite_card_sizes.getById(id);

        return {
            "min-height": size.entity.browser_preview.height + 'px',
        };
    }

    getImageBoxCSS(): any {
        return {
            "height": this.family.entity.card_size.entity.browser_preview.height + 'px',
        }
    }

    getDesignerName(): string {
        let authorProfile = this.family.entity.author.entity;

        return authorProfile.first_name + ' ' + authorProfile.last_name;
    }

    getDesignerProfileImage(): string {
        let authorProfile = this.family.entity.author.entity;

        return queryImage(QueryTarget.Avatar, authorProfile.image).public_path;
    }

    getTemplates(): InviteCardTemplate[] {
        return this.family.entity.templates;
    }

    getNgStyleBackground(): any {
        let image = this.getImageURL();

        return {
            "background-image": `url("${image}")`,
        };
    }

    getNgStyleFor(template: InviteCardTemplate) {
        return {
            'background-color': template.entity.color.hex_code
        };
    }
}