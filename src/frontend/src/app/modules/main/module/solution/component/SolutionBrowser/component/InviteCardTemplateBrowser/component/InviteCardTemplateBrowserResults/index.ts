import Timer = NodeJS.Timer;

import {Component} from "../../../../../../../../decorators/Component";
import {SolutionBrowserService} from "../../../../service";
import {SolutionRepository} from "../../../../../../repository/SolutionRepository";
import {InviteCardFamily} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {InviteCardTemplate} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {AsyncImageLoaderService} from "../../../../../../../../service/AsyncImageLoaderService";

interface InviteCardFamilyBrowserEntity extends InviteCardFamily
{
    loaded: boolean;
}

@Component({
    selector: 'hi-invite-card-template-browser-results',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        results: '<',
    },
    injects: [
        '$window',
        '$timeout',
        SolutionBrowserService,
        AsyncImageLoaderService,
    ]
})
export class InviteCardTemplateBrowserResults implements ng.IComponentController
{
    public visible: boolean = true;
    public results: InviteCardFamilyBrowserEntity[] = [];
    public preview: PreviewHelper;

    constructor(
        private $window: angular.IWindowService,
        private $timeout: angular.ITimeoutService,
        private service: SolutionBrowserService,
        private imagesLoader: AsyncImageLoaderService,
    ) {
        this.preview = new PreviewHelper(service.repository);
    }

    $onChanges(onChangesObj: angular.IOnChangesObject): void {
        let notLoaded = this.results.filter(entity => !entity.loaded);

        if(notLoaded.length > 0) {
            this.imagesLoader.loadImages(notLoaded.map(entity => entity.entity.templates[0].entity.preview.variants['preview'].public_path)).subscribe(() => {
                this.$timeout(() => {
                    notLoaded.forEach(entity => entity.loaded = true);
                }, 0);
            })
        }
    }

    open(template: InviteCardTemplate) {
        this.preview.open(template);
    }

    isValid(family: InviteCardFamily) {
        return family.entity.templates.length > 0;
    }

    getNgStyle() {
        return {
            opacity: this.visible ? 1 : 0
        };
    }
}

class PreviewHelper
{
    public enabled: boolean = false;
    public family: InviteCardFamily;
    public template: InviteCardTemplate;

    constructor(private repository: SolutionRepository)
    {}

    open(template: InviteCardTemplate) {
        this.enabled = true;
        this.template = template;
        this.family = this.repository.tables.invite_card_families.getById(this.template.entity.family.id);
    }

    close() {
        this.enabled = false;
        this.family = undefined;
        this.template = undefined;
    }
}