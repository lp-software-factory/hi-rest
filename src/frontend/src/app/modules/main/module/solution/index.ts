import {HIModule} from "../../../../modules";

import {SolutionBrowser} from "./component/SolutionBrowser/index";
import {SolutionBrowserFilterService} from "./component/SolutionBrowser/service/FilterService";
import {SolutionBrowserService} from "./component/SolutionBrowser/service";
import {DesignerFilter} from "./component/SolutionBrowser/service/Filter/DesignerFilter";
import {EventTypeGroupFilter} from "./component/SolutionBrowser/service/Filter/EventGroupTypeFilter";
import {EvenTypeFilter} from "./component/SolutionBrowser/service/Filter/EventTypeFilter";
import {HasPhotoFilter} from "./component/SolutionBrowser/service/Filter/HasPhotoFilter";
import {InviteCardGammaFilter} from "./component/SolutionBrowser/service/Filter/InviteCardGammaFilter";
import {InviteCardSizeFilter} from "./component/SolutionBrowser/service/Filter/InviteCardSizeFilter";
import {InviteCardStyleFilter} from "./component/SolutionBrowser/service/Filter/InviteCardStyleFilter";
import {InviteCardTemplateBrowserResults} from "./component/SolutionBrowser/component/InviteCardTemplateBrowser/component/InviteCardTemplateBrowserResults/index";
import {InviteCardTemplateBrowserCard} from "./component/SolutionBrowser/component/InviteCardTemplateBrowser/component/InviteCardTemplateBrowserCard/index";
import {InviteCardTemplateBrowserPreview} from "./component/SolutionBrowser/component/InviteCardTemplateBrowser/component/InviteCardTemplateBrowserPreview/index";
import {SolutionBrowserFilters} from "./component/SolutionBrowser/component/SolutionBrowserFilters/index";
import {InviteCardTemplateBrowserStreams} from "./component/SolutionBrowser/component/InviteCardTemplateBrowser/component/InviteCardTemplateBrowserStreams/index";

export const HI_MAIN_SOLUTION_MODULE: HIModule = {
    services: [
        SolutionBrowserFilterService,
        SolutionBrowserService,
        DesignerFilter,
        EventTypeGroupFilter,
        EvenTypeFilter,
        HasPhotoFilter,
        InviteCardGammaFilter,
        InviteCardSizeFilter,
        InviteCardStyleFilter,
        InviteCardTemplateBrowserCard,
        InviteCardTemplateBrowserPreview,
        InviteCardTemplateBrowserResults,
        InviteCardTemplateBrowserStreams,
        SolutionBrowserFilters,
    ],
    components: [
        SolutionBrowser,
    ],
    routes: [],
};