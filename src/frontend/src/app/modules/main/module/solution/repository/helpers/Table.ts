import {IdEntity} from "../../../../../../../../definitions/src/definitions/_common/markers/IdEntity";

export interface TableEntity {
    entity: IdEntity;
}

export class Table<T extends TableEntity>
{
    entities: T[] = [];
    index: { [id: number]: T } = {};

    constructor(
        public name: string,
    ) {}

    getAll(): T[] {
        return this.entities;
    }

    getById(id: number): T {
        if(this.index[id]) {
            return this.index[id];
        }else{
            let results = this.entities.filter(entity => entity.entity.id === id);

            if(results.length === 0) {
                throw new Error(`Entity "${this.name}:${id}" not found`);
            }else if(results.length > 1) {
                throw new Error(`There is duplicates for entity "${this.name}:${id}"`);
            }

            return results[0];
        }
    }

    hasWithId(id: number): boolean {
        return this.entities.filter(entity => entity.entity.id === id).length > 0;
    }

    insert(entity: T): void {
        if(this.hasWithId(entity.entity.id)) {
            throw new Error(`Entity "${this.name}:${entity.entity.id}" already exists`);
        }

        this.entities.push(entity);
        this.index[entity.entity.id] = entity;
    }

    insertMany(entities: T[]): void {
        entities.forEach(entity => this.insert(entity));
    }

    replace(replace: T): void {
        this.remove(replace);
        this.insert(replace);
    }

    replaceMany(entities: T[]): void {
        entities.forEach(entity => this.replace(entity));
    }

    removeById(id: number): void {
        this.entities = this.entities.filter(entity => entity.entity.id !== id);
        delete this.index[id];
    }

    remove(rm: T): void {
        this.removeById(rm.entity.id);
    }

    removeMany(entities: T[]): void {
        entities.forEach(entity => this.remove(entity));
    }

    touch(entity: T): void {
        if(! this.hasWithId(entity.entity.id)) {
            this.insert(entity);
        }
    }

    touchMany(entities: T[]):  void {
        entities.forEach(entity => this.touch(entity));
    }
}