import {FilterType, FilterChoice, Filter} from "../FilterService";

import {Service} from "../../../../../../decorators/Service";
import {DesignerEntity} from "../../../../../../../../../../definitions/src/definitions/designers/entity/DesignerEntity";
import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {ITranslateServiceInterface, ITranslateService} from "../../../../../i18n/service/TranslationService";
import {IComponentQueryParams} from "../../../../../../../../../../typings-custom/angular-component-router";
import {SolutionRepository} from "../../../../repository/SolutionRepository";
import {SolutionQuery} from "../../../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";

@Service({
    injects: [
        SolutionRepository,
        ITranslateService,
    ]
})
export class DesignerFilter implements Filter
{
    public id = FilterType.Designer;

    private defaultTitle: string = 'Designer';
    private current: DesignerEntity;
    private isActivatedFlag: boolean = false;

    constructor(
        public repository: SolutionRepository,
        private translate: ITranslateServiceInterface,
    ) {
        this.translate('hi.main.solution.browser.filters.designer').then(res => this.defaultTitle = res);
    }

    getCurrent(): FilterChoice<any> {
        return {
            id: this.current.id,
            title: `${this.current.profile.first_name} ${this.current.profile.last_name}`,
            entity: this.current
        };
    }

    applyFromQueryParams(qp: IComponentQueryParams): boolean {
        if(qp.hasOwnProperty(this.id)) {
            let id = parseInt(qp[this.id], 10);
            let entity = this.repository.tables.designers.getById(id);

            this.isActivatedFlag = true;
            this.current = entity.entity;

            return true;
        }else{
            return false;
        }
    }

    applyCriteria(carry: SolutionQuery): boolean {
        if(this.isActivated()) {
            carry.criteria.author_profile_ids = [
                this.getCurrent().id
            ];

            return true;
        }else{
            return false;
        }
    }

    activate(choice: FilterChoice<any>) {
        this.current = this.repository.tables.designers.getById(choice.id).entity;
        this.isActivatedFlag = true;
    }

    deactivate() {
        this.isActivatedFlag = false;
    }

    isActivated(): boolean {
        return this.isActivatedFlag;
    }

    getTitle(): string {
        if(this.isActivated()) {
            return `${this.current.profile.first_name} ${this.current.profile.last_name}`;
        }else{
            return this.defaultTitle;
        }
    }

    getIcon(): string {
        return 'icon icon-designer';
    }

    filter(input: Array<Solution>): Array<Solution> {
        let designer = this.repository.tables.designers.getById(this.current.id);
        return input.filter(entity => entity.entity.invite_card_templates[0].entity.family.author.entity.id === designer.entity.profile.id);
    }

    list(): FilterChoice<any>[] {
        return this.repository.tables.designers.getAll().map(designer => {
            return {
                id: designer.entity.id,
                title: `${designer.entity.profile.first_name} ${designer.entity.profile.last_name}`,
                entity: designer
            };
        })
    }
}