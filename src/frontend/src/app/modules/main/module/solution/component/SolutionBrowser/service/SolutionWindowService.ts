import {Service} from "../../../../../decorators/Service";
import {Solution} from "../../../../../../../../../definitions/src/definitions/solution/entity/Solution";

@Service()
export class SolutionWindowService
{
    public static PAGE_SIZE = 30;
    public static PAGE_PRELOAD = 3;

    private offset: number;
    private numPages: number = 1;
    private results: Array<Solution> = [];

    pushResults(results: Array<Solution>): void {
        results.forEach(r => this.results.push(r));
    }

    setResults(results: Array<Solution>) {
        this.results = results;

        if(this.maxPages() < this.numPages) {
            this.numPages = this.maxPages();
        }
    }

    view(): Array<Solution> {
        if(this.results.length) {
            let window: Array<Solution> = [];

            for(let index = 0; index < Math.min(this.results.length, (SolutionWindowService.PAGE_SIZE * this.numPages)); index++) {
                window.push(this.results[index]);
            }

            return window;
        }else{
            return [];
        }
    }

    firstId(): number|undefined {
        if(this.results.length) {
            return this.results[0].entity.id;
        }else{
            return undefined;
        }
    }

    lastId(): number|undefined {
        if(this.results.length) {
            return this.results[this.results.length - 1].entity.id;
        }else{
            return undefined;
        }
    }

    emit(results: Array<Solution>) {
        results.forEach(solution => this.results.push(solution));
    }

    resetWindow(): void {
        this.numPages = 1;
    }

    reset() {
        this.results = [];
        this.numPages = 1;
    }

    addPage(): boolean {
        if(this.hasNextPage()) {
            this.numPages++;

            return true;
        }else{
            return false;
        }
    }

    maxPages(): number {
        return Math.ceil(this.results.length / SolutionWindowService.PAGE_SIZE);
    }

    hasPrevPage(): boolean {
        return this.offset == 0;
    }

    hasNextPage(): boolean {
        return this.numPages < this.maxPages();
    }

    removePage(): boolean {
        if(this.numPages > 1) {
            this.numPages--;

            return true;
        }else{
            return false;
        }
    }
}

export interface SolutionWindowPage
{
    start: string|undefined;
    end: string|undefined;
    items: Array<Solution>;
}