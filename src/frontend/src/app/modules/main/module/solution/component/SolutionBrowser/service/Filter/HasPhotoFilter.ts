import {Filter, FilterType, FilterChoice} from "../FilterService";

import {Service} from "../../../../../../decorators/Service";
import {Solution} from "../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {SolutionRepository} from "../../../../repository/SolutionRepository";
import {ITranslateService, ITranslateServiceInterface} from "../../../../../i18n/service/TranslationService";
import {IComponentQueryParams} from "../../../../../../../../../../typings-custom/angular-component-router";
import {SolutionQuery} from "../../../../../../../../../../definitions/src/definitions/solution/request/SolutionQuery";

export enum HasPhoto
{
    None = <any>"none",
    True = <any>"has",
    False = <any>"without"
}

@Service({
    injects: [
        SolutionRepository,
        ITranslateService,
    ]
})
export class HasPhotoFilter implements Filter
{
    public static STR_DEFAULT = 'Has photo?';
    public static STR_HAS = 'With photo';
    public static STR_WITHOUT = 'Without photo';

    public id = FilterType.HasPhoto;

    private current: HasPhoto = HasPhoto.None;
    private isActivatedFlag: boolean = false;

    constructor(
        public repository: SolutionRepository,
        private translate: ITranslateServiceInterface,
    ) {

        let translations = [
            'hi.main.solution.browser.filters.has-photo.default',
            'hi.main.solution.browser.filters.has-photo.has',
            'hi.main.solution.browser.filters.has-photo.without',
        ];

        this.translate(translations).then((results) => {
            HasPhotoFilter.STR_DEFAULT = results[translations[0]];
            HasPhotoFilter.STR_HAS = results[translations[1]];
            HasPhotoFilter.STR_WITHOUT = results[translations[2]];
        });
    }

    applyFromQueryParams(qp: IComponentQueryParams): boolean {
        if(qp.hasOwnProperty(this.id)) {
            this.isActivatedFlag = true;
            this.current = <any>qp[this.id];

            return true;
        }else{
            return false;
        }
    }

    applyCriteria(carry: SolutionQuery): boolean {
        if(this.isActivated()) {
            carry.criteria.has_photo = this.getCurrent().id === HasPhoto.True;

            return true;
        }else{
            return false;
        }
    }

    getCurrent(): FilterChoice<any> {
        return {
            id: hasPhotoToId(this.current),
            title: hasPhotoToLocalizedString(this.current),
            entity: this.current
        };
    }

    activate(choice: FilterChoice<any>) {
        this.current = choice.entity;
        this.isActivatedFlag = true;
    }

    deactivate() {
        this.current = HasPhoto.None;
        this.isActivatedFlag = false;
    }

    isActivated(): boolean {
        return this.isActivatedFlag;
    }

    getTitle(): string {
        return hasPhotoToLocalizedString(this.current);
    }

    getIcon(): string {
        return 'icon icon-photo';
    }

    filter(input: Array<Solution>): Array<Solution> {
        return input.filter(entity => entity.entity.invite_card_templates[0].entity.family.has_photo === (this.current === HasPhoto.True));
    }

    list(): FilterChoice<any>[] {
        return [HasPhoto.True, HasPhoto.False].map(has => {
            return {
                id: hasPhotoToId(has),
                title: hasPhotoToLocalizedString(has),
                entity: has
            };
        })
    }
}

export function hasPhotoToId(hasPhoto: HasPhoto): string
{
    switch(hasPhoto) {
        default:
            return 'none';
        case HasPhoto.True:
            return 'has';
        case HasPhoto.False:
            return 'without';
    }
}

export function hasPhotoToLocalizedString(hasPhoto: HasPhoto): string
{
    switch(hasPhoto) {
        default:
            return HasPhotoFilter.STR_DEFAULT;
        case HasPhoto.True:
            return HasPhotoFilter.STR_HAS;
        case HasPhoto.False:
            return HasPhotoFilter.STR_WITHOUT;
    }
}