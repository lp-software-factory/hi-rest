import {Component} from "../../../../../../../../decorators/Component";

import {InviteCardFamily} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {InviteCardTemplate} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {SolutionRepository} from "../../../../../../repository/SolutionRepository";
import {SolutionBrowserService} from "../../../../service";
import {AsyncImageLoaderService} from "../../../../../../../../service/AsyncImageLoaderService";
import {randomId} from "../../../../../../../../functions/randomId";
import {InviteCardTemplateBrowserIsotope} from "./isotope";

export class InviteCardTemplateBrowserStreamsConfig
{
    public static DEFAULT_MAX_STREAMS = 4;
    public static DEFAULT_STREAM_WIDTH = 260;
    public static DEFAULT_MARGIN = 15;

    constructor(
        public max: number = InviteCardTemplateBrowserStreamsConfig.DEFAULT_MAX_STREAMS,
        public width:number = InviteCardTemplateBrowserStreamsConfig.DEFAULT_STREAM_WIDTH,
        public margin: number = InviteCardTemplateBrowserStreamsConfig.DEFAULT_MARGIN,
    ) {}
}

export interface InviteCardFamilyStreamEntity extends InviteCardFamily
{
    pushed: boolean;
    loaded: boolean;
}

@Component({
    selector: 'hi-invite-card-template-browser-streams',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        'config': '<',
        'results': '<',
    },
    injects: [
        '$window',
        '$interval',
        '$timeout',
        SolutionBrowserService,
        AsyncImageLoaderService,
    ]
})
export class InviteCardTemplateBrowserStreams implements ng.IComponentController
{
    private resultsContainerId: string = 'hiInviteCardTemplateBrowserStreamsContainer'+randomId(5);

    private interval: angular.IPromise<any>;
    private id;

    public config: InviteCardTemplateBrowserStreamsConfig;
    public results: InviteCardFamilyStreamEntity[] = [];
    public preview: PreviewHelper;
    public isotope: InviteCardTemplateBrowserIsotope;

    constructor(
        private $window: angular.IWindowService,
        private $interval: angular.IIntervalService,
        private $timeout: angular.ITimeoutService,
        private service: SolutionBrowserService,
        private imagesLoader: AsyncImageLoaderService,
    ) {
        this.id = 'hiInviteCardTemplateBrowserStreams' + randomId(12);
        this.preview = new PreviewHelper(service.repository);
    }

    $onInit(): void {
        this.$window.addEventListener('resize', () => {
            if(this.isotope.onResize()) {
                this.$timeout(() => {});
            }
        });

        this.interval = this.$interval(() => {
            for(let item of this.results) {
                if(! item.pushed) {
                    this.isotope.getStreams().map(stream => {
                        stream.bump = Math.max(stream.bump, this.$window.document.getElementById(stream.id).clientHeight);
                    });
                }
            }
        }, 500);
    }

    $onDestroy(): void {
        if(this.interval) {
            this.$interval.cancel(this.interval);
        }
    }

    $onChanges(onChangesObj: angular.IOnChangesObject): void {
        let notLoaded = this.results.filter(entity => !entity.loaded);

        if(notLoaded.length > 0) {
            this.imagesLoader.loadImages(notLoaded.map(entity => entity.entity.templates[0].entity.preview.variants['preview'].public_path)).subscribe(() => {
                this.$timeout(() => {
                    notLoaded.forEach(entity => entity.loaded = true);
                }, 0);
            })
        }

        let container = this.$window.document.getElementById(this.resultsContainerId);

        if(container) {
            this.isotope = new InviteCardTemplateBrowserIsotope(container, this.config);
        }

        if(this.isotope) {
            this.results.forEach(r => r.pushed = false);
            this.isotope.setItems(this.results);
        }
    }

    open(template: InviteCardTemplate) {
        this.preview.open(template);
    }

    isValid(family: InviteCardFamily) {
        return family.entity.templates.length > 0;
    }
}

class PreviewHelper
{
    public enabled: boolean = false;
    public family: InviteCardFamily;
    public template: InviteCardTemplate;

    constructor(private repository: SolutionRepository)
    {}

    open(template: InviteCardTemplate) {
        this.enabled = true;
        this.template = template;
        this.family = this.repository.tables.invite_card_families.getById(this.template.entity.family.id);
    }

    close() {
        this.enabled = false;
        this.family = undefined;
        this.template = undefined;
    }
}