interface IsotopeColumn
{
    bump: number;
}

export class IsotopeService
{
    static PADDING = 10;
    static DEFAULT_BASIS = 260;

    private maxColumns = 4;

    constructor(
        private basis: number = IsotopeService.DEFAULT_BASIS,
    ) {}

    update(container: HTMLElement, cards: HTMLElement[]) {
        if(cards.length === 0) return;

        let numColumns = Math.min(this.maxColumns, Math.floor((window.innerWidth - 2*IsotopeService.PADDING) / this.basis));

        if(numColumns === 1) {
            this.renderResponsiveSingleColumn(container, cards, [undefined, { bump: 0 }]);
        }else{
            if(cards.length < numColumns) {
                numColumns = cards.length;
            }

            this.renderMultipleColumns(numColumns, container, cards);
        }
    }

    renderMultipleColumns(numColumns: number, container: HTMLElement, cards: HTMLElement[]) {
        cards.forEach(item => {
            let newWidth = this.basis + 'px';
            let newDisplay = 'block';
            let newPosition = 'absolute';

            if(item.style.width !== newWidth) item.style.width = newWidth;
            if(item.style.display !== newDisplay) item.style.display = 'block';
            if(item.style.position !== newPosition) item.style.position = 'absolute';
        });

        let columns: IsotopeColumn[] = [];

        for(let i = 1; i <= numColumns; i++) {
            columns[i] = {
                bump: 0,
            };
        }

        let sorted: HTMLElement[] = [];
        let priorities: number[] = this.getPriority(numColumns);

        for(let card of cards) {
            let next: number;

            if(this.isPriorityRestRequired(columns)) {
                priorities = this.getPriority(numColumns);
            }

            if(priorities.length) {
                next = priorities.shift();
            }else{
                next = 1;
            }

            columns.forEach((column, index) => {
                if(columns[next].bump > column.bump) {
                    next = index;
                }
            });

            let x = (next-1) * this.basis + (next-1)*IsotopeService.PADDING;
            let y = columns[next].bump + IsotopeService.PADDING;
            let height = card.getBoundingClientRect().height;

            let newLeft = x + 'px';
            let newTop = y + 'px';

            if(card.style.left !== newLeft) card.style.left = x + "px";
            if(card.style.top !== newTop) card.style.top = y + "px";

            columns[next].bump += height + IsotopeService.PADDING;
        }

        this.updateContainerWidth(container, cards, columns);
    }

    renderResponsiveSingleColumn(container: HTMLElement, cards: HTMLElement[], columns: IsotopeColumn[]) {
        cards.forEach(item => {
            item['style'].width = '100%';
            item['style'].display = 'block';
            item['style'].position = 'static';

            let newWidth = '100%';
            let newDisplay = 'block';
            let newPosition = 'static';

            if(item.style.width !== newWidth) item.style.width = newWidth;
            if(item.style.display !== newDisplay) item.style.display = 'block';
            if(item.style.position !== newPosition) item.style.position = 'absolute';
        });

        if(container.style.width !== '100%') container.style.width = '100%';

        this.updateContainerWidth(container, cards, columns);
    }

    updateContainerWidth(container: HTMLElement, cards: HTMLElement[], columns: IsotopeColumn[]) {
        if((columns.length - 1) === 1) {
            let totalHeight = cards.map(element => element.getBoundingClientRect().height).reduce((a, b) => {
                return a + b;
            });

            let newWidth = '100%';
            let newHeight = 'auto';

            if(container.style.width !== newWidth) container.style.width = newWidth;
            if(container.style.height !== newHeight) container.style.height = newHeight;
        }else{
            let newWidth: string;
            let newHeight = Math.max.apply(Math, columns
                .filter(entity => entity !== undefined)
                .map(entity => entity.bump)
            ) + 'px';

            if(cards.length > (columns.length - 1)) {
                newWidth = (columns.length - 1) * this.basis + 'px';
            }else{
                newWidth = cards.length * this.basis + 'px';
            }

            if(container.style.width !== newWidth) container.style.width = newWidth;
            if(container.style.height !== newHeight) container.style.height = newHeight;
        }
    }

    isPriorityRestRequired(columns: IsotopeColumn[]): boolean {
        let firstValue = columns[1].bump;

        for(let column of columns.filter(c => c !== undefined)) {
            if(column.bump !== firstValue) {
                return false;
            }
        }

        return true;
    }

    getPriority(columns: number): number[] {
        switch(columns) {
            default: throw new Error('Unsupported :/');
            case 1: return [1];
            case 2: return [1, 2];
            case 3: return [2, 1, 3];
            case 4: return [2, 3, 1, 4];
        }
    }
}
