import {Component} from "../../../../decorators/Component";

import {SolutionBrowserConfiguration} from "./config";
import {InviteCardFamily} from "../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {SolutionBrowserService} from "./service";

@Component({
    selector: 'hi-solution-browser',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        config: '=',
    },
    injects: [
        '$timeout',
        '$window',
        SolutionBrowserService,
    ]
})
export class SolutionBrowser implements ng.IComponentController
{
    private disabledLoadMore: boolean = false;

    public results: InviteCardFamily[] = [];
    public config: SolutionBrowserConfiguration;

    constructor(
        private $timeout: angular.ITimeoutService,
        private $window: angular.IWindowService,
        private service: SolutionBrowserService,
    ) {
        this.update();
    }

    $onInit(): void {
        this.service.subjectUpdates.subscribe(solutions => {
            this.$timeout(() => {
                this.update();
            });
        })
    }

    update() {
        this.results = this.service.window.view().map(solution => {
            return this.service.repository.tables.invite_card_families.getById(
                solution.entity.invite_card_templates[0].entity.family.id
            )
        });
    }

    next($event: angular.IAngularEvent) {
        $event.preventDefault();

        this.disabledLoadMore = true;

        this.service.next().subscribe(() => {
            setTimeout(() => {
                this.$timeout(() => {
                    this.update();
                    this.disabledLoadMore = false;
                })
            });
        });
    }

    isLoadMoreButtonEnabled(): boolean {
        return (this.service.results.length > 0) && this.service.hasNext() && !this.disabledLoadMore;
    }
}