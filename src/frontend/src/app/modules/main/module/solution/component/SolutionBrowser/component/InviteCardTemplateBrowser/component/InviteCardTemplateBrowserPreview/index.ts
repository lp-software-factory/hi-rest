import {Component} from "../../../../../../../../decorators/Component";

import {SolutionBrowserService} from "../../../../service";
import {InviteCardFamily} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {InviteCardTemplate} from "../../../../../../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";

@Component({
    selector: 'hi-invite-card-template-browser-preview',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        family: '<',
        current: '<',
        onClose: '&',
        onChange: '&',
    },
    injects: [
        SolutionBrowserService,
    ]
})
export class InviteCardTemplateBrowserPreview implements ng.IComponentController
{
    public family: InviteCardFamily;
    public current: InviteCardTemplate;

    constructor(
        private service: SolutionBrowserService,
    ) {}

    select(template: InviteCardTemplate) {
        this.current = template;
    }

    getNgStyleFor(template: InviteCardTemplate) {
        return {
            'background-color': template.entity.color.hex_code,
        };
    }

    getTemplate(): InviteCardTemplate[] {
        return this.family.entity.templates;
    }

    getImageURL(): string {
        let sizes = [];

        for(let size in this.current.entity.preview.variants) {
            if(this.current.entity.preview.variants.hasOwnProperty(size)) {
                if(! isNaN(parseInt(size))) {
                    sizes.push(parseInt(size), 10);
                }
            }
        }

        let maxSize = Math.max.apply(Math, sizes);

        return this.current.entity.preview.variants[maxSize.toString()].public_path;
    }
}