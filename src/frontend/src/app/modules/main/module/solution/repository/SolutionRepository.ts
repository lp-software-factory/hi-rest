import {Service} from "../../../decorators/Service";

import {Table} from "./helpers/Table";
import {Locale} from "../../../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {Profile} from "../../../../../../../definitions/src/definitions/profile/entity/Profile";
import {Designer} from "../../../../../../../definitions/src/definitions/designers/entity/DesignerEntity";
import {EventType, EventTypeEntity} from "../../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";
import {EventTypeGroup, EventTypeGroupEntity} from "../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {InviteCardGamma} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {InviteCardSize} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {InviteCardStyle} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {InviteCardFamily} from "../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {InviteCardTemplate} from "../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {Solution, SolutionTarget} from "../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {SolutionRepositoryResponse200} from "../../../../../../../definitions/src/definitions/solution/paths/repository";

export interface SolutionMongoIds { [id: number]: string }

@Service()
export class SolutionRepository
{
    public solution_mongo_ids: SolutionMongoIds = {};

    public tables: {
        locales: Table<Locale>;
        profiles: Table<Profile>;
        designers: Table<Designer>;
        event_types: Table<EventType>;
        event_type_groups: Table<EventTypeGroup>;
        invite_card_gammas: Table<InviteCardGamma>;
        invite_card_sizes: Table<InviteCardSize>;
        invite_card_styles: Table<InviteCardStyle>;
        invite_card_families: Table<InviteCardFamily>;
        invite_card_templates: Table<InviteCardTemplate>;
        solutions: Table<Solution>;
    };

    constructor() {
        this.initTables();
    }

    reset() {
        this.initTables();
    }

    private initTables() {
        this.solution_mongo_ids = {};

        this.tables = {
            locales: new Table<Locale>('locale'),
            profiles: new Table<Profile>('profile'),
            designers: new Table<Designer>('designer'),
            event_types: new Table<EventType>('event_type'),
            event_type_groups: new Table<EventTypeGroup>('event_type_group'),
            invite_card_gammas: new Table<InviteCardGamma>('invite_card_gamma'),
            invite_card_sizes: new Table<InviteCardSize>('invite_card_size'),
            invite_card_styles: new Table<InviteCardStyle>('invite_card_size'),
            invite_card_families: new Table<InviteCardFamily>('invite_card_family'),
            invite_card_templates: new Table<InviteCardTemplate>('invite_card_template'),
            solutions: new Table<Solution>('solution'),
        };
    }

    importSolutionRepositoryResponse(response: SolutionRepositoryResponse200) {
        let results = response.results;

        for(let id in response.mongo_ids) {
            if(response.mongo_ids.hasOwnProperty(id)) {
                this.solution_mongo_ids[id] = response.mongo_ids[id];
            }
        }

        this.tables.locales.touchMany(results.essentials.locales.map(entity => {
            return {
                entity: entity,
            }
        }));
        this.tables.profiles.touchMany(results.essentials.profiles);
        this.tables.invite_card_gammas.touchMany(results.essentials.invite_card_gammas);
        this.tables.invite_card_sizes.touchMany(results.essentials.invite_card_sizes);
        this.tables.invite_card_styles.touchMany(results.essentials.invite_card_styles);

        this.tables.event_type_groups.touchMany(results.essentials.event_type_groups.map((withIds): EventTypeGroup => {
            let eventTypeGroup: EventTypeGroupEntity = {
                id: withIds.id,
                sid: withIds.sid,
                url: withIds.url,
                position: withIds.position,
                metadata: withIds.metadata,
                title: withIds.title,
                description: withIds.description,
                event_types: [],
            };

            eventTypeGroup.event_types = results.essentials.event_types
                .filter(et => et.event_type_group_id === eventTypeGroup.id)
                .map((et): EventTypeEntity => {
                    if(! this.tables.event_types.hasWithId(et.id)) {
                        this.tables.event_types.insert({
                            entity: {
                                id: et.id,
                                sid: et.sid,
                                url: et.url,
                                position: et.position,
                                metadata: et.metadata,
                                title: et.title,
                                description: et.description,
                                group: {
                                    id: et.event_type_group_id,
                                    title: eventTypeGroup.title,
                                    description: eventTypeGroup.description
                                }
                            }
                        });
                    }

                    return this.tables.event_types.getById(et.id).entity;
                });

            return {
                entity: eventTypeGroup,
            };
        }));

        this.tables.designers.touchMany(results.essentials.designers.map((withIds): Designer => {
            return {
                entity: {
                    id: withIds.id,
                    sid: withIds.sid,
                    date_created_at: withIds.date_created_at,
                    last_updated_on: withIds.last_updated_on,
                    metadata: withIds.metadata,
                    profile: this.tables.profiles.getById(withIds.profile_id).entity
                }
            };
        }));

        this.tables.invite_card_families.touchMany(results.essentials.invite_card_families.map((withIds): InviteCardFamily => {
            let family: InviteCardFamily = {
                entity: {
                    id: withIds.entity.id,
                    sid: withIds.entity.sid,
                    position: withIds.entity.position,
                    date_created_at: withIds.entity.date_created_at,
                    last_updated_on: withIds.entity.last_updated_on,
                    metadata: withIds.entity.metadata,
                    title: withIds.entity.title,
                    description: withIds.entity.description,
                    locale: this.tables.locales.getById(withIds.entity.locale_id).entity,
                    author: this.tables.profiles.getById(withIds.entity.author_profile_id),
                    event_type: this.tables.event_types.getById(withIds.entity.event_type_id).entity,
                    style: this.tables.invite_card_styles.getById(withIds.entity.style_id),
                    card_size: this.tables.invite_card_sizes.getById(withIds.entity.card_size_id),
                    gamma: this.tables.invite_card_gammas.getById(withIds.entity.gamma_id),
                    has_photo: withIds.entity.has_photo,
                    is_activated: withIds.entity.is_activated,
                    template_ids: [],
                    templates: []
                }
            };

            family.entity.templates = results.essentials.invite_card_templates
                .filter(template => template.entity.family_id === family.entity.id)
                .map((templateWithIds): InviteCardTemplate => {
                    if(! this.tables.invite_card_templates.hasWithId(templateWithIds.entity.id)) {
                        this.tables.invite_card_templates.insert({
                            entity: {
                                id: templateWithIds.entity.id,
                                sid: templateWithIds.entity.sid,
                                position: templateWithIds.entity.position,
                                date_created_at: templateWithIds.entity.date_created_at,
                                last_updated_on: templateWithIds.entity.last_updated_on,
                                is_activated: templateWithIds.entity.is_activated,
                                title: templateWithIds.entity.title,
                                description: templateWithIds.entity.description,
                                family_id: family.entity.id,
                                family: family.entity,
                                card_size: this.tables.invite_card_sizes.getById(templateWithIds.entity.card_size_id).entity,
                                color: templateWithIds.entity.color,
                                preview: templateWithIds.entity.preview,
                                backdrop: templateWithIds.entity.backdrop,
                                photo: templateWithIds.entity.photo,
                                config: templateWithIds.entity.config,
                                solution: templateWithIds.entity.solution,
                                attachments: [],
                            }
                        });
                    }

                    return this.tables.invite_card_templates.getById(templateWithIds.entity.id);
                });

            return family;
        }));


        this.tables.solutions.touchMany(results.solutions.map(withIds => {
            let result: Solution = {
                entity: {
                    id: withIds.entity.id,
                    sid: withIds.entity.sid,
                    date_created_at: withIds.entity.date_created_at,
                    last_updated_on: withIds.entity.last_updated_on,
                    version: withIds.entity.version,
                    is_user_specified: withIds.entity.is_user_specified,
                    owner: withIds.entity.owner,
                    target: {
                        type: withIds.entity.target.type,
                    },
                    invite_card_templates: withIds.entity.invite_card_template_ids.map(id => {
                        return this.tables.invite_card_templates.getById(id);
                    }),
                    envelope_templates: [],
                    landing_templates: []
                },
                families: withIds.entity.invite_card_template_ids.map(icId => {
                    let template = this.tables.invite_card_templates.getById(icId);

                    return this.tables.invite_card_families.getById(template.entity.family_id);
                })
            };

            if(result.entity.target.type === SolutionTarget.Profile) {
                result.entity.target.profile = this.tables.profiles.getById(withIds.entity.target.profile_id).entity;
            }

            return result;
        }));
    }
}