import {InviteCardFamilyStreamEntity, InviteCardTemplateBrowserStreamsConfig} from "./index";

import {randomId} from "../../../../../../../../functions/randomId";

export interface InviteCardTemplateBrowserStream
{
    id: string;
    css: any;
    position: number;
    items: InviteCardFamilyStreamEntity[],
    bump: number;
}

export class InviteCardTemplateBrowserIsotope
{
    private items: InviteCardFamilyStreamEntity[] = [];
    private streams: InviteCardTemplateBrowserStream[] = [];

    constructor(
        private container: HTMLElement,
        private config: InviteCardTemplateBrowserStreamsConfig
    ) {}

    setItems(items: InviteCardFamilyStreamEntity[]) {
        this.items = items;
        this.setupColumns();
        this.allocateItems(this.items);
    }

    pushItems(items: InviteCardFamilyStreamEntity[]) {
        items.forEach(item => this.items.push(item));
        this.allocateItems(items);
    }

    onResize(): boolean {
        if(this.calculateMaxStreams() !== this.streams.length) {
            this.setupColumns();
            this.allocateItems(this.items);

            return true;
        }else{
            return false;
        }
    }

    setupColumns() {
        let cssColumn: any;
        let maxStreams = this.calculateMaxStreams();

        this.streams = [];

        if(maxStreams > 1) {
            cssColumn = {
                "flex-shrink": "0",
                "flex-basis": this.config.width + "px",
            }
        }else{
            cssColumn = {
                "flex-basis": "100%",
            }
        }

        for(let n = 0; n < maxStreams; n++) {
            this.streams.push({
                id: randomId(32),
                css: cssColumn,
                position: n+1,
                items: [],
                bump: 0
            });
        }
    }

    allocateItems(items: InviteCardFamilyStreamEntity[]) {
        this.streams.forEach(s => s.items = []);

        items.forEach(item => {
            let next = this.getCurrentMinColumn();

            this.streams[next].items.push(item);
            this.streams[next].bump += item.entity.card_size.entity.browser_preview.height;
            this.streams[next].bump += Math.ceil((item.entity.templates.length-1) / 8)*25;
        });
    }

    getCurrentMinColumn(): number {
        if(this.streams.length === 1) {
            return 0;
        }else{
            let currentMinColumn = 0;
            let currentMin = this.streams[0].bump;

            for(let index = 0; index < this.streams.length; index++) {
                let s = this.streams[index];

                if(s.bump < currentMin) {
                    currentMin = s.bump;
                    currentMinColumn = index;
                }
            }

            return currentMinColumn;
        }
    }

    getStreams(): InviteCardTemplateBrowserStream[] {
        return this.streams;
    }

    getCSSContainer(): any {
        return {
            "display": "flex",
            "justify-content": "center",
        }
    }

    private calculateMaxStreams(): number {
        let width = this.container.clientWidth;

        let numColumns = Math.max(1, Math.min(this.config.max, Math.floor(width / this.config.width)));
        let minWidthWithMargins = (numColumns*this.config.width) + ((numColumns-1)*this.config.margin)

        if(width < minWidthWithMargins) {
            return Math.max(1, numColumns - 1);
        }else{
            return numColumns;
        }
    }
}