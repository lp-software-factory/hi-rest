import {HIModule} from "../../../../modules";

import {AuthService} from "./service/AuthService";

export const HI_MAIN_AUTH_MODULE: HIModule = {
    services: [
        AuthService,
    ]
};