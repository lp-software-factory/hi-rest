import {Service} from "../../../decorators/Service";

import {JWTAuthToken} from "../../../../../../../definitions/src/definitions/auth/entity/JWTAuthToken";
import {FrontendService} from "../../../service/FrontendService";

@Service({
    injects: [
        '$window',
        FrontendService,
    ]
})
export class AuthService
{
    private current: JWTAuthToken;

    constructor(
        private $window: angular.IWindowService,
        private frontend: FrontendService,
    ) {
        frontend.replay.subscribe(response => {
            this.current = response.auth;
        });
    }

    public hasJWT(): boolean {
        return !!this.current;
    }

    public getJWT(): string {
        return this.current.jwt;
    }

    public clearJWT() {
        this.current = undefined;
    }

    public getToken(): JWTAuthToken {
        if(!this.isSignedIn()) {
            throw new Error("You're not signed in");
        }

        return this.current;
    }

    public isSignedIn(): boolean {
        return !!this.current;
    }

    public isAdmin(): boolean {
        return this.hasJWT() && !!~this.current.account.app_access.indexOf("admin");
    }

    public isDesigner(): boolean {
        return this.hasJWT() && !!~this.current.account.app_access.indexOf("designer");
    }

    public signIn(token: JWTAuthToken) {
        this.current = token;
    }

    public signOut() {
        this.current = null;
        this.clearJWT();
    }
}