import {Service} from "../../../decorators/Service";

import {FrontendService} from "../../../service/FrontendService";
import {LocaleEntity} from "../../../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {localizedToString} from "../functions/localizedToString";

@Service({
    injects: [
        FrontendService,
    ]
})
export class LocaleService
{
    public current: LocaleEntity;
    public available: LocaleEntity[];

    private fallback = ['en_US', 'en_GB'];

    constructor(private frontend: FrontendService) {
        frontend.replay.subscribe(response => {
            this.current = response.locale.current;
            this.available = response.locale.available;
        })
    }

    localize(input: LocalizedString[]): string {
        let locales = [this.current.region];

        this.fallback.forEach(region => locales.push(region));

        return localizedToString(input, locales);
    }
}