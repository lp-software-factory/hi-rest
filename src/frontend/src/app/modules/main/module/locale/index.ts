import {LocaleService} from "./service/LocaleService";
import {LocalizePipe} from "./pipe/LocalizePipe";

export const HI_MAIN_LOCALE_MODULE = {
    services: [
        LocaleService,
    ],
    components: [],
    routes: [],
    pipes: [
        LocalizePipe,
    ]
};