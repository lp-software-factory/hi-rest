import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";

export function localizedToString(input: LocalizedString[], localesStack = [
    'ru_RU',
    'en_GB',
    'en_US'
]): string
{
    if(!Array.isArray(input) || (input.length === 0)) {
        return '<???>';
    }else{
        for(let region of localesStack) {
            if(input.filter(i => i.region === region).length) {
                return input.filter(i => i.region === region)[0].value;
            }
        }

        return input[0].value;
    }
}