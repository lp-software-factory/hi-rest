import {Pipe} from "../../../decorators/Pipe";

import {PipeFactoryInterface} from "../../../../../angularized/decorators/Pipe";
import {LocaleService} from "../service/LocaleService";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";

@Pipe({
    name: 'localize',
    injects: [
        LocaleService,
    ]
})
export class LocalizePipe implements PipeFactoryInterface
{
    factory(): angular.Injectable<Function> {
        return (service: LocaleService) => {
            return function(input: LocalizedString[]) {
                return service.localize(input);
            }
        };
    }
}