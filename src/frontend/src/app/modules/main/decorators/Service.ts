import {app} from "../module"

import {ServiceDefinition, ServiceDefinitions} from "../../../angularized/decorators/Service";

export function Service(definition: ServiceDefinition = {}) {
    return function(target: Function) {
        ServiceDefinitions.$instance.register(app, target, definition);
    };
}