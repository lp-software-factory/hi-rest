export function cssLoader(input: string) {
    if(window && window.document) {
        let css: HTMLStyleElement;

        css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = input;

        document.body.appendChild(css);
    }
}