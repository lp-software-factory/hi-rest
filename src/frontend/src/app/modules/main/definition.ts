import {app} from "./module";

import {LIST_SERVICES} from "../../../../definitions/src/services/list";
import {ServiceDefinitions} from "../../angularized/decorators/Service";
import {RESTService} from "./service/RESTService";

export function bootstrapRESTServices() {
    LIST_SERVICES.forEach((service: Function) => {
        let name: string = service.name
            ? service.name
            : randomId(32);

        ServiceDefinitions.$instance.register(app, service, {
            name: name,
            injects: [
                RESTService,
            ]
        });
    });
}

function randomId(length: number) {
    let text = "";
    let possible = "abcdef0123456789";

    for(let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}