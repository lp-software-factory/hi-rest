export class ListWindow<T>
{
    private _page: number = 1;

    public items: T[] = [];

    constructor(private pageSize: number) {}

    get page(): number {
        return Math.max(1, this._page);
    }

    set page(value: number) {
        this._page = Math.min(value, this.maxPages());
    }

    scrollTo(item: T, locate: { (item: T, compare: T): boolean }): void {
        let index = 0;

        for(let input of this.items) {
            if(locate(item, input)) {
                this.page = Math.floor(index / this.pageSize) + 1;

                return;
            }else{
                index++;
            }
        }
    }

    length(): number {
        return this.items.length;
    }

    hasNext(): boolean {
        return (this.items.length > 0) && (this.page !== this.maxPages());
    }

    goToPage(page: number) {
        this.page = page;
    }

    next(): boolean {
        if(this.hasNext()) {
            this.page++;

            return true;
        }else{
            return false;
        }
    }

    hasPrev(): boolean {
        return (this.items.length > 0) && (this.page > 1);
    }
    
    updatePageSize(number: number) {
        this.pageSize = number;
    }

    prev(): boolean {
        if(this.hasPrev()) {
            this.page--;

            return true;
        }else{
            return false;
        }
    }

    maxPages(): number {
        return Math.ceil(this.items.length / this.pageSize);
    }


    tableList(): T[] {
        let to, from;
        let result: T[] = [];

        if(this.maxPages() === this.page) {
            from = this.pageSize*(this.page-1);
            to = this.items.length;
        }else{
            from = this.pageSize*(this.page-1);
            to = this.pageSize*this.page;
        }


        for(let index = from; index < to; index++) {
            if(this.items[index]) {
                result.push(this.items[index]);
            }
        }

        return result;
    }

    list(): T[] {
        let to, from;
        let result: T[] = [];

        if(this.maxPages() === this.page) {
            from = Math.max(0, this.items.length - this.pageSize);
            to = this.items.length;
        }else{
            from = this.pageSize*(this.page-1);
            to = this.pageSize*this.page;
        }
        

        for(let index = from; index < to; index++) {
            if(this.items[index]) {
                result.push(this.items[index]);
            }
        }
        
        return result;
    }
}