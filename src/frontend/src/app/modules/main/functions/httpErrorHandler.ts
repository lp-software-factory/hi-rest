import {UNKNOWN_BACKEND_ERROR} from "../../../../messages";

export interface ErrorHandlerDefinition
{
    known: { [status: number]: (error: string) => void },
    unknown(error: string);
}

export function httpErrorHandler(reason, definition: ErrorHandlerDefinition)
{
    if(! (reason && reason.status && reason.data && reason.data.error)) {
        definition.unknown(UNKNOWN_BACKEND_ERROR);
    }

    let status = reason.status.toString();

    if(definition.known.hasOwnProperty(status)) {
        definition.known[status](reason.data.error);
    }else{
        definition.unknown(UNKNOWN_BACKEND_ERROR);
    }
}