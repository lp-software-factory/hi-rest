export function withStatusCode(code: number, error, callback: Function) {
    if(error.status === code) {
        callback();
    }
}
