export function ucfirst(str) {
    var f = str.charAt(0).toUpperCase();

    return f + str.substr(1, str.length - 1);
}