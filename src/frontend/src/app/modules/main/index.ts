import {HIModule} from "../../modules";

import {FrontendService} from "./service/FrontendService";
import {AsyncImageLoaderService} from "./service/AsyncImageLoaderService";
import {RESTService} from "./service/RESTService";

import {HI_MAIN_ACCOUNT_MODULE} from "./module/account/index";
import {HI_MAIN_COMMON_MODULE} from "./module/common/index";
import {HI_MAIN_DESIGNER_MODULE} from "./module/designer/index";
import {HI_MAIN_EVENT_MODULE} from "./module/event/index";
import {HI_MAIN_LOCALE_MODULE} from "./module/locale/index";
import {HI_MAIN_SOLUTION_MODULE} from "./module/solution/index";
import {HI_MAIN_AUTH_MODULE} from "./module/auth/index";
import {HI_MAIN_MESSAGE_MODULE} from "./module/message/index";
import {HI_MAIN_I18N_MODULE} from "./module/i18n/index";
import {HI_UI_MODULE} from "./module/ui/index";
import {HI_MAIN_INVITE_CARD_EDITOR_MODULE} from "./module/invite/invite-card-editor/index";
import {HI_MAIN_ENVELOPE_EDITOR_MODULE} from "./module/envelope/envelope-editor/index";
import {HI_MAIN_MODULE_FONT} from "./module/font/index";
import {HI_MAIN_ENVELOPE_ANIMATION_MODULE} from "./module/envelope/envelope-animation/index";
import {HI_MAIN_ATTACHMENT_MODULE} from "./module/attachment/index";
import {HI_MAIN_DRESS_CODE} from "./module/dress-code/index";

export const HI_MAIN_MODULE: HIModule = {
    factories: [
    ],
    services: [
        FrontendService,
        AsyncImageLoaderService,
        RESTService,
    ],
};

export const HI_MAIN_MODULES: HIModule[] = [
    HI_MAIN_MODULE,
    HI_MAIN_I18N_MODULE,
    HI_MAIN_ACCOUNT_MODULE,
    HI_MAIN_COMMON_MODULE,
    HI_MAIN_DESIGNER_MODULE,
    HI_MAIN_EVENT_MODULE,
    HI_MAIN_LOCALE_MODULE,
    HI_MAIN_SOLUTION_MODULE,
    HI_MAIN_AUTH_MODULE,
    HI_MAIN_MESSAGE_MODULE,
    HI_UI_MODULE,
    HI_MAIN_INVITE_CARD_EDITOR_MODULE,
    HI_MAIN_ENVELOPE_EDITOR_MODULE,
    HI_MAIN_ENVELOPE_ANIMATION_MODULE,
    HI_MAIN_MODULE_FONT,
    HI_MAIN_ATTACHMENT_MODULE,
    HI_MAIN_DRESS_CODE,
];