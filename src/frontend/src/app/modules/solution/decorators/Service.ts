import {appSolution} from "../module"

import {ServiceDefinition, ServiceDefinitions} from "../../../angularized/decorators/Service";

export function SolutionService(definition: ServiceDefinition = {}) {
    return function(target: Function) {
        ServiceDefinitions.$instance.register(appSolution, target, definition);
    };
}
