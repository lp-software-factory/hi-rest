import {appSolution} from "../module"

import {ComponentDefinitions, ComponentDefinition} from "../../../angularized/decorators/Component";

export function SolutionComponent(definition: ComponentDefinition) {
    return function(target: Function) {
        ComponentDefinitions.$instance.register(appSolution, target, definition);
    };
}
