import {HIModule} from "../../modules";

import {SolutionBrowserRoute} from "./browser/routes/SolutionBrowserRoute/index";
import {SolutionBrowserRootRoute} from "./browser/routes/SolutionBrowserRootRoute/index";

export const HI_SOLUTION_MODULE: HIModule = {
    components: [
        SolutionBrowserRootRoute,
        SolutionBrowserRoute,
    ],
};