import {SolutionComponent} from "../../../decorators/Component";

import {SolutionBrowserService} from "../../../../main/module/solution/component/SolutionBrowser/service";
import {SolutionBrowserConfiguration} from "../../../../main/module/solution/component/SolutionBrowser/config";
import {EventRESTService} from "../../../../../../../definitions/src/services/event/event/EventRESTService";
import {InviteCardTemplate} from "../../../../../../../definitions/src/definitions/invites/invite-card-template/entity/InviteCardTemplate";
import {Solution} from "../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {AuthService} from "../../../../main/module/auth/service/AuthService";
import {SolutionBrowserFilterService} from "../../../../main/module/solution/component/SolutionBrowser/service/FilterService";
import {IComponentRouter, IComponentRouterInstructions} from "../../../../../../../typings-custom/angular-component-router";
import {InviteCardTemplateBrowserStreamsConfig} from "../../../../main/module/solution/component/SolutionBrowser/component/InviteCardTemplateBrowser/component/InviteCardTemplateBrowserStreams/index";

@SolutionComponent({
    selector: 'hi-solution-browser-route',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$rootRouter',
        '$window',
        SolutionBrowserService,
        SolutionBrowserFilterService,
        EventRESTService,
        AuthService,
    ],
})
export class SolutionBrowserRoute implements ng.IComponentController
{
    public config: SolutionBrowserConfiguration;

    constructor(
        private $timeout: angular.ITimeoutService,
        private $router: IComponentRouter,
        private $window: angular.IWindowService,
        private service: SolutionBrowserService,
        private filters: SolutionBrowserFilterService,
        private rest: EventRESTService,
        private auth: AuthService,
    ) {}

    $routerOnActivate(next: IComponentRouterInstructions, previous: IComponentRouterInstructions) {
        this.filters.forEach(filter => filter.applyFromQueryParams(next.params));
        this.service.update();

        if(this.service.results.length === 0) {
            this.service.next();
        }
    }

    $onInit(): void {
        this.config = {
            editMode: false,
            streams: new InviteCardTemplateBrowserStreamsConfig(),
        };

        this.service.subjectOpenTemplate.subscribe(template => {
            let solution = this.locateSolution(template);

            if(this.auth.isSignedIn()) {
                this.rest.createEvent({
                    solution_id: solution.entity.id,
                    invite_card_template_id: template.entity.id,
                }).subscribe(response => {
                    window.location.href = `/event/${response.event.entity.sid}/editor`;
                });
            }else{
                window.location.href = '/account/register';
            }
        });

        this.service.subjectUpdates.subscribe(() => { this.$timeout() });

        this.filters.subjectOnActivateFilter.subscribe(() => this.navigate());
        this.filters.subjectOnDeactivateFilter.subscribe(() => this.navigate());
    }

    private navigate() {
        this.$router.navigate(['/HISolutionBrowse', this.filters.generateFilterQueryParams()]);
    }

    private locateSolution(template: InviteCardTemplate): Solution {
        let inviteCardFamilyId: number = template.entity.family_id;

        let solutions = this.service.repository.tables.solutions.getAll().filter(solution => {
            let familyIds = [];

            solution.entity.invite_card_templates.forEach(template => {
                familyIds.push(template.entity.family_id);
            });

            return !!~familyIds.indexOf(inviteCardFamilyId);
        });

        if(solutions.length !== 1) {
            throw new Error('Unsupported');
        }

        return solutions[0];
    }
}