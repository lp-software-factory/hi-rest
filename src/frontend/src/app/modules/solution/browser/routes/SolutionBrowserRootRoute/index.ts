import {SolutionComponent} from "../../../decorators/Component";

import {SolutionBrowserRoute} from "../SolutionBrowserRoute/index";
import {FrontendService} from "../../../../main/service/FrontendService";
import {SolutionBrowserService} from "../../../../main/module/solution/component/SolutionBrowser/service";

@SolutionComponent({
    selector: 'hi-solution-browser-root-route',
    template: require('./template.jade'),
    injects: [
        FrontendService,
        SolutionBrowserService,
    ],
    routes: [
        {
            path: '/browse',
            name: 'HISolutionBrowse',
            component: SolutionBrowserRoute,
            useAsDefault: true,
        }
    ]
})
export class SolutionBrowserRootRoute
{
    constructor(
        private frontend: FrontendService,
        private service: SolutionBrowserService,
    ) {
        frontend.replay.subscribe(response => {
            this.service.setSource(response.exports['solutions_source']);
            this.service.setFilter(response.exports['solutions_filter']);
            this.service.importResponse(response.exports['solutions']);
        })
    }
}