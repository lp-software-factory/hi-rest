let angular = require('angular');

export const appSolution: angular.IModule = angular.module('hi-solution');

appSolution.config(function($locationProvider) {
    $locationProvider
        .html5Mode(false)
        .hashPrefix('!')
    ;
});
appSolution.value('$routerRootComponent', 'hiSolutionBrowserRootRoute');