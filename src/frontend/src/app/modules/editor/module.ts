var angular = require('angular');

export const appEditor: angular.IModule = angular.module('hi-editor');

appEditor.config(['$locationProvider', function($locationProvider) {
    $locationProvider
        .html5Mode(false)
        .hashPrefix('!')
    ;
}]);
appEditor.value('$routerRootComponent', 'hiEventRouteEventEditor');