import {EditorComponent} from "../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EventLandingEditorV1Service} from "../../../../service/EventLandingEditorV1Service";
import {EventLandingTemplateBlockGuestsList} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-block-guests-list',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        '$timeout',
        EventLandingEditorV1Service,
    ],
    bindings: {
        block: '<'
    }
})
export class EventLandingEditorV1BlockGuestsList implements ng.IComponentController
{
    private subscription: Subscription;

    public block: EventLandingTemplateBlockGuestsList;

    constructor(
        private $timeout: ng.ITimeoutService,
        private services: EventLandingEditorV1Service,
    ) {}

    $onInit(): void {
        this.subscription = this.services.model.readySubject.subscribe(next => {

        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}