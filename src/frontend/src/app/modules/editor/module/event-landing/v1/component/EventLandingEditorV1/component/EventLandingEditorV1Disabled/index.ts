import {EditorComponent} from "../../../../../../../decorators/Component";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-disabled',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ]
})
export class EventLandingEditorV1Disabled implements ng.IComponentController
{}