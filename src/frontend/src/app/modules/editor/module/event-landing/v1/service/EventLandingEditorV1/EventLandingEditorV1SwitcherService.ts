import {EditorService} from "../../../../../decorators/Service";

import {EventLandingEditorV1Service} from "../EventLandingEditorV1Service";
import {Subject} from "rxjs";

export interface EventLandingSwitcher
{
    enabled: boolean,
}

@EditorService({
    injects: [
        '$timeout',
    ]
})
export class EventLandingEditorV1SwitcherService
{
    public current: EventLandingSwitcher;
    public services: EventLandingEditorV1Service;

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {}

    public switchSubject: Subject<boolean> = new Subject<boolean>();

    enableSwitcher(switcher: EventLandingSwitcher): void {
        this.current = switcher;
        this.switchSubject.next(this.current.enabled);
    }

    disableSwitcher(): void {
        this.current = undefined;
    }

    isSwitcherEnabled(): boolean {
        return this.current !== undefined;
    }

    toggleSwitcher(): boolean {
        if(this.current === undefined) {
            throw new Error('Switcher is undefined');
        }else{
            this.current.enabled = !this.current.enabled;
            this.switchSubject.next(this.current.enabled);

            return this.current.enabled;
        }
    }

    getSwitcherValue(): boolean {
        if(this.current === undefined) {
            throw new Error('Switcher is undefined');
        }else{
            return this.current.enabled;
        }
    }
}
