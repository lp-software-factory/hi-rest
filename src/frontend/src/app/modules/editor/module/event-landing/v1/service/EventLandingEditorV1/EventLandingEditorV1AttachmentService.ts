import {EditorService} from "../../../../../decorators/Service";

import {AttachmentRepository} from "../../../../../../main/module/attachment/repository/AttachmentRepository";
import {EventLandingEditorV1Service} from "../EventLandingEditorV1Service";

@EditorService({
    injects: [
        '$timeout',
    ]
})
export class EventLandingEditorV1AttachmentService
{
    public services: EventLandingEditorV1Service;
    public repository: AttachmentRepository;

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {}
}