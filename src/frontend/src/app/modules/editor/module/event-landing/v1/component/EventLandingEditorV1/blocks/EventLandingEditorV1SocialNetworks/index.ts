import {EditorComponent} from "../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EventLandingEditorV1Service} from "../../../../service/EventLandingEditorV1Service";
import {EventLandingTemplateBlockSocialNetworks, EventLandingTemplateSocialNetworks, EventLandingTemplateSocialNetworksList} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-block-social-networks',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        '$timeout',
        EventLandingEditorV1Service,
    ],
    bindings: {
        block: '<'
    }
})
export class EventLandingEditorV1BlockSocialNetworks implements ng.IComponentController
{
    private subscription: Subscription;

    public block: EventLandingTemplateBlockSocialNetworks;

    public list: EventLandingTemplateSocialNetworks[] = EventLandingTemplateSocialNetworksList;

    constructor(
        private $timeout: ng.ITimeoutService,
        private services: EventLandingEditorV1Service,
    ) {}

    $onInit(): void {
        this.subscription = this.services.model.readySubject.subscribe(next => {});
    }

    $onChanges(): void {
        this.list.forEach(id => {
            if(! this.block.enabled.hasOwnProperty(id)) {
                this.block.enabled[id] = false;
            }
        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    toggle(id: EventLandingTemplateSocialNetworks): void {
        this.block.enabled[id] = !this.block.enabled[id];
    }

    getIcon(id: EventLandingTemplateSocialNetworks): string {
        switch(id) {
            default: throw new Error(`No icon available for "${id}"`);
            case EventLandingTemplateSocialNetworks.Facebook: return 'icon icon-social-facebook';
            case EventLandingTemplateSocialNetworks.Vkontakte: return 'icon icon-vk';
            case EventLandingTemplateSocialNetworks.Twitter: return 'icon icon-twitter';
            case EventLandingTemplateSocialNetworks.Pinterest: return 'icon icon-pinterest2';
            case EventLandingTemplateSocialNetworks.LinkedIn: return 'icon icon-linkedin2';
            case EventLandingTemplateSocialNetworks.Odnoklassniki: return 'icon icon-ok';
            case EventLandingTemplateSocialNetworks.Email: return 'icon icon-envelope';
        }
    }
}