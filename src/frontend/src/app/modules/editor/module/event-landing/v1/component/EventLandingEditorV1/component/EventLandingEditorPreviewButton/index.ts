import {EditorComponent} from "../../../../../../../decorators/Component";

import {EventLandingEditorV1Service} from "../../../../service/EventLandingEditorV1Service";
import {LoadingManager} from "../../../../../../../../main/module/common/helpers/LoadingManager";
import {UIAlertModalService} from "../../../../../../../../main/module/ui/service/UIAlertModalService";

@EditorComponent({
    selector: 'hi-event-landing-editor-preview-button',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        eventSid: '<',
    },
    injects: [
        '$timeout',
        '$window',
        EventLandingEditorV1Service,
        UIAlertModalService,
    ]
})
export class EventLandingEditorPreviewButton implements ng.IComponentController
{
    public status: LoadingManager = new LoadingManager();

    public eventSid: string;

    constructor(
        private $timeout: ng.ITimeoutService,
        private $window: ng.IWindowService,
        private service: EventLandingEditorV1Service,
        private alert: UIAlertModalService,
    ) {}

    isAvailable(): boolean {
        return this.service.canSave();
    }

    preview(): void {
        if(this.service.canSave()) {
            let loading = this.status.addLoading();

            this.service.save(true).last().subscribe(
                next => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.$window.open(`/event/landing/${this.eventSid}/`);
                    });
                },
                error => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.raiseSaveError();
                    });
                }
            );
        }
    }

    raiseSaveError(): void {
        this.alert.open({
            title: {
                value: 'hi.editor.event-landing.finish.error.title',
                translate: true,
            },
            text: {
                value: 'hi.editor.event-landing.finish.error.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.editor.event-landing.finish.error.close',
                        translate: true,
                    },
                    click: modal => {
                        this.$timeout(() => {
                            modal.close();
                        });
                    }
                }
            ]
        })
    }
}