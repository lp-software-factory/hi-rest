import {EditorComponent} from "../../../../../../../decorators/Component";
import {randomId} from "../../../../../../../../main/functions/randomId";

export enum EventLandingEditorV1TextInputType
{
    Single = <any>"single",
    MultiLine = <any>"multiline",
}

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-text-input-temp',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        type: '@',
        placeholder: '@',
        value: '=',
        fontSize: '@',
    },
    injects: [
        '$window',
    ]
})
export class EventLandingEditorV1TextInput implements ng.IComponentController
{
    public id: string = 'textInput' + randomId(8);

    public type: EventLandingEditorV1TextInputType = EventLandingEditorV1TextInputType.Single;
    public placeholder: string;
    public value: string;
    public fontSize: number = 24;

    constructor(
        private $window: ng.IWindowService,
    ) {}

    focus(): void {
        this.$window.document.getElementById(this.id).focus();
    }

    isSingleLine(): boolean {
        return this.type === EventLandingEditorV1TextInputType.Single;
    }

    isMultiLine(): boolean {
        return this.type === EventLandingEditorV1TextInputType.MultiLine;
    }

    getInputCSS(): any {
        return {
            'font-size': this.fontSize + 'px',
        };
    }

    getTextAreaCSS(): any {
        return {
            'font-size': this.fontSize + 'px',
        };
    }
}