import {EventDirective} from "../../../../../../../decorators/Directive";

import {DirectiveFactoryInterface} from "../../../../../../../../../angularized/decorators/Directive";

declare let CKEDITOR;

@EventDirective({
    name: 'hi-event-landing-edit-button',
    injects: [
        '$timeout'
    ]
})
export class EventLandingEditButtonDirective implements DirectiveFactoryInterface
{
    factory(): angular.IDirectiveFactory {
        return function($timeout: ng.ITimeoutService) {
            return {
                link: function(scope: ng.IScope, element, attr, ngModel: ng.INgModelController) {
                    element.on('click', function() {
                        let source = this.parentElement.getElementsByTagName('textarea')[0];

                        for(let i in CKEDITOR.instances) {
                            if(CKEDITOR.instances.hasOwnProperty(i)) {
                                if(CKEDITOR.instances[i].element.$ === source) {
                                    setTimeout(() => {
                                        CKEDITOR.instances[i].focus();
                                    });
                                }
                            }
                        }
                    })
                }
            }
        }
    }
}