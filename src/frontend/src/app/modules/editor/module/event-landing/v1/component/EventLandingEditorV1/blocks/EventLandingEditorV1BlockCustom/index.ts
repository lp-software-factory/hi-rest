import {EditorComponent} from "../../../../../../../decorators/Component";

import {EventLandingTemplateBlockCustom, EventLandingTemplateBlock} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {AttachmentRESTService} from "../../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {UIAlertModalService} from "../../../../../../../../main/module/ui/service/UIAlertModalService";
import {LoadingManager} from "../../../../../../../../main/module/common/helpers/LoadingManager";
import {AttachmentEntity} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {UploadObservableProgress} from "../../../../../../../../../../../definitions/src/functions/upload";

enum FileType {
    File = <any>'file',
    Photo = <any>'photo'
}

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-block-custom',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        '$timeout',
        AttachmentRESTService,
        UIAlertModalService
    ],
    bindings: {
        block: '<'
    }
})
export class EventLandingEditorV1BlockCustom implements ng.IComponentController
{
    constructor(
        private $timeout: angular.ITimeoutService,
        private attachment: AttachmentRESTService,
        private alert: UIAlertModalService
    ){}

    public block: EventLandingTemplateBlockCustom;
    public status: LoadingManager = new LoadingManager();

    hasPhoto(): boolean {
        return this.block.photo.url !== undefined;
    }

    getUploadedPhotoCSS(): any {
        if(this.hasPhoto()) {
            return {
                'background-image': `url("${encodeURI(this.block.photo.url)}")`,
            };
        }else{
            return {};
        }
    }

    uploadFile(file: File, prop: FileType): void {
        let loading = this.status.addLoading();
        let lastResult: AttachmentEntity<ImageAttachmentMetadata>;

        this.attachment.upload(file).subscribe(
            next => {
                if(! (next instanceof UploadObservableProgress)) {
                    lastResult = <any>next.entity;
                }
            },
            error => {
                this.$timeout(() => {
                    loading.is = false;

                    this.alert.open({
                        title: {
                            value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.title',
                            translate: true,
                        },
                        text: {
                            value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.text',
                            translate: true,
                        },
                        buttons: [
                            {
                                title: {
                                    value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.close',
                                    translate: true,
                                },
                                click: modal => {
                                    this.$timeout(() => {
                                        loading.is = false;

                                        modal.close();
                                    });
                                }
                            }
                        ]
                    });
                });
            },
            () => {
                if(prop === FileType.File) {
                    this.$timeout(() => {
                        loading.is = false;

                        this.block.file = {
                            $attachment_id: lastResult.id,
                            url: lastResult.link.url,
                        };
                    });
                } else if(prop === FileType.Photo) {
                    this.$timeout(() => {
                        loading.is = false;

                        this.block.photo = {
                            $attachment_id: lastResult.id,
                            url: lastResult.link.url,
                        };
                    });
                }
            }
        )
    }
   
}