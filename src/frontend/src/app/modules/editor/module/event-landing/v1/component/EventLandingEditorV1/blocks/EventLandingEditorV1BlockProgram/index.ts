import {Component} from "../../../../../../../../main/decorators/Component";
import {AttachmentRESTService} from "../../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EventLandingTemplateBlockProgram, EventLandingTemplateBlock} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {UploadObservableProgress} from "../../../../../../../../../../../definitions/src/functions/upload";
import {AttachmentEntity} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {LoadingManager} from "../../../../../../../../main/module/common/helpers/LoadingManager";
import {UIAlertModalService} from "../../../../../../../../main/module/ui/service/UIAlertModalService";
import {EventLandingEditorV1ModelService} from "../../../../service/EventLandingEditorV1/EventLandingEditorV1ModelService";
import {randomId} from "../../../../../../../../main/functions/randomId";

enum FileType {
    File = <any>'file',
    Photo = <any>'photo'
}

@Component({
    selector: 'hi-event-landing-editor-v1-block-program',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        index: '<',
        block: '<',
    },
    injects: [
        '$timeout',
        AttachmentRESTService,
        UIAlertModalService,
    ]
})
export class EventLandingEditorV1BlockProgram implements ng.IComponentController
{
    constructor(
        private $timeout: angular.ITimeoutService,
        private attachment: AttachmentRESTService,
        private alert: UIAlertModalService
    ){}

    public mapId = `googleMap_${randomId(8)}`;
    public mapVisible = false;

    public index: number;
    public block: EventLandingTemplateBlockProgram;
    public status: LoadingManager = new LoadingManager();

    hasTitle(): boolean {
        return this.index === 0;
    }

    changeMapVisibleStatement() {
        this.mapVisible = !this.mapVisible;
    }

    hasPhoto(): boolean {
        return this.block.photo.url !== undefined;
    }

    getUploadedPhotoCSS(): any {
        if(this.hasPhoto()) {
            return {
                'background-image': `url("${encodeURI(this.block.photo.url)}")`,
            };
        }else{
            return {};
        }
    }

    uploadFile(file: File, prop: FileType): void {
        let loading = this.status.addLoading();
        let lastResult: AttachmentEntity<ImageAttachmentMetadata>;

        this.attachment.upload(file).subscribe(
            next => {
                if(! (next instanceof UploadObservableProgress)) {
                    lastResult = <any>next.entity;
                }
            },
            error => {
                this.$timeout(() => {
                    loading.is = false;

                    this.alert.open({
                        title: {
                            value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.title',
                            translate: true,
                        },
                        text: {
                            value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.text',
                            translate: true,
                        },
                        buttons: [
                            {
                                title: {
                                    value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.close',
                                    translate: true,
                                },
                                click: modal => {
                                    this.$timeout(() => {
                                        loading.is = false;

                                        modal.close();
                                    });
                                }
                            }
                        ]
                    });
                });
            },
            () => {
                if(prop === FileType.File) {
                    this.$timeout(() => {
                        loading.is = false;

                        this.block.file = {
                            $attachment_id: lastResult.id,
                            url: lastResult.link.url,
                        };
                    });
                } else if(prop === FileType.Photo) {
                    this.$timeout(() => {
                        loading.is = false;

                        this.block.photo = {
                            $attachment_id: lastResult.id,
                            url: lastResult.link.url,
                        };
                    });
                }
            }
        )
    }
}