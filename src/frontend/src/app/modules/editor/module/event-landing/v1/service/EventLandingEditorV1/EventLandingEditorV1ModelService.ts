import {EditorService} from "../../../../../decorators/Service";

import {ReplaySubject, Observable} from "rxjs";
import {EventLandingEditorV1Service} from "../EventLandingEditorV1Service";
import {EventLandingTemplate, EventLandingTemplateModel, EventLandingTemplateBlock, IEventLandingTemplateBlock} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {Solution} from "../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EventLandingTemplateConfigsService} from "../../../../../../main/module/event/editor/service/EventLandingTemplateConfigsService";
import {EventLandingTemplateBlocksFactory} from "../../factory/EventLandingTemplateBlocksFactory";
import {EventLandingSwitcher} from "./EventLandingEditorV1SwitcherService";

interface SetupModelQuery
{
    template: EventLandingTemplate;
    current: Solution;
    original: Solution;
}

@EditorService({
    injects: [
        '$timeout',
        EventLandingTemplateConfigsService,
        EventLandingTemplateBlocksFactory,
    ]
})
export class EventLandingEditorV1ModelService
{
    public services: EventLandingEditorV1Service;

    public config: EventLandingTemplateModel;
    public template: EventLandingTemplate;
    public current: Solution;
    public original: Solution;

    public isReady: boolean = false;

    public readySubject: ReplaySubject<EventLandingEditorV1ModelService> = new ReplaySubject<EventLandingEditorV1ModelService>();
    public changeSubject: ReplaySubject<EventLandingTemplate> = new ReplaySubject<EventLandingTemplate>();

    constructor(
        private $timeout: ng.ITimeoutService,
        private configs: EventLandingTemplateConfigsService,
        private factory: EventLandingTemplateBlocksFactory,
    ) {}

    
    addNewBlock(type: EventLandingTemplateBlock): void {
       this.config.blocks.sources.push(this.factory.createBlockOf(type)); 
    }
    
    deleteBlock(block: IEventLandingTemplateBlock) {
        this.config.blocks.sources = this.config.blocks.sources.filter(compare => {
            return compare.sid !== block.sid;
        });
    }
    
    setup(query: SetupModelQuery): Observable<string> {
        let observable = Observable.create(observer => {
            this.isReady = false;

            this.config = JSON.parse(JSON.stringify(query.template.entity.json));
            this.template = query.template;
            this.current = query.current;
            this.original = query.original;

            if(this.config instanceof Array) {
                this.config = <any>{};
            }

            this.setupConfig(this.config);

            this.loadResource().subscribe(
                next => {
                    this.$timeout(() => {
                        observer.next();
                    });
                },
                error => {
                    this.$timeout(() => {
                        observer.next();
                    });
                },
                () => {
                    observer.complete();
                }
            )
        }).share();

        observable.subscribe(undefined, undefined, () => {
            this.$timeout(() => {
                this.isReady = true;
                this.readySubject.next(this);
            })
        });

        return observable;
    }

    setupConfig(input: EventLandingTemplateModel): void {
        if(! input.import_attachment_ids) {
            input.import_attachment_ids = [];
        }

        if(! input.blocks) {
            input.blocks = {
                enabled: {},
                sources: [],
            };

            this.configs.all().forEach(c => {
                if(c.auto) {
                    input.blocks.sources.push(this.factory.createBlockOf(c.id));
                }
            });
        }

        this.configs.all().forEach(c => {
            if(! input.blocks.enabled.hasOwnProperty(c.id)) {
                input.blocks.enabled[c.id] = true;
            }
        });
    }

    loadResource(): Observable<string> {
        return Observable.create(observer => {
            observer.complete();
        });
    }

    toggleBlock(id: EventLandingTemplateBlock): void {
        if(!this.isDisabledBySwitcher() && this.configs.isToggleable(id)) {
            if(this.isBlockTypeEnabled(id)) {
                this.config.blocks.enabled[id] = false;
            }else{
                this.config.blocks.enabled[id] = true;

                if(this.config.blocks.sources.filter(b => b.type === id).length === 0) {
                    this.config.blocks.sources.push(this.factory.createBlockOf(id));
                }
            }
        }
    }

    isSwitcherEnabled(): boolean {
        return this.services.switcher.isSwitcherEnabled();
    }

    getSwitcher(): EventLandingSwitcher {
        if(this.isSwitcherEnabled()) {
            return this.services.switcher.current;
        }else{
            throw new Error("Switcher is not enabled");
        }
    }

    isDisabledBySwitcher(): boolean {
        return !this.isSwitcherEnabled() || !this.getSwitcher().enabled;
    }

    isBlockTypeEnabled(id: EventLandingTemplateBlock): boolean {
        if(this.isDisabledBySwitcher()) {
            return false;
        }else{
            if(this.configs.isToggleable(id)) {
                return this.services.model.config.blocks.enabled[id]
                    && this.services.model.config.blocks.sources.filter(b => b.type === id).length > 0;
            }else{
                return false;
            }
        }
    }

    numBlockOfTypes(id: EventLandingTemplateBlock): number {
        return this.config.blocks.sources.filter(b => b.type === id).length;
    }
}