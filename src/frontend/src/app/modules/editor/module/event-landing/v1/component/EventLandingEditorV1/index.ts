import {EditorComponent} from "../../../../../decorators/Component";

import {EventLandingEditorV1Service} from "../../service/EventLandingEditorV1Service";
import {Solution} from "../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EventLandingSwitcher} from "../../service/EventLandingEditorV1/EventLandingEditorV1SwitcherService";
import {EventLandingTemplate, IEventLandingTemplateBlock, EventLandingTemplateBlockHeader, EventLandingTemplateBlock, EventLandingTemplateBlockProgram} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplateConfigsService} from "../../../../../../main/module/event/editor/service/EventLandingTemplateConfigsService";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1',
    template: require('./resources/template.jade'),
    styles: [
        require('./resources/styles/style.shadow.scss'),
    ],
    bindings: {
        switcher: '<',
        template: '<',
        current: '<',
        original: '<'
    },
    injects: [
        '$timeout',
        EventLandingEditorV1Service,
        EventLandingTemplateConfigsService,
    ]
})
export class EventLandingEditorV1 implements ng.IComponentController
{
    switcher: EventLandingSwitcher;
    template: EventLandingTemplate;
    current: Solution;
    original: Solution;

    private status: LoadingManager = new LoadingManager();
    private ready: boolean = false;

    constructor(
        private $timeout: ng.ITimeoutService,
        private services: EventLandingEditorV1Service,
        private configs: EventLandingTemplateConfigsService,
    ) {}

    $onChanges(): void {
        this.ready = false;

        let loading = this.status.addLoading();

        if(this.switcher) {
            this.services.switcher.enableSwitcher(this.switcher);
        }else{
            this.services.switcher.disableSwitcher();
        }

        this.services.model.services.model.setup({
            current: this.current,
            original: this.original,
            template: this.template,
        }).subscribe(undefined, undefined, () => {
            this.$timeout(() => {
                loading.is = false;

                this.ready = true;
            });
        });
    }

    isLoading(): boolean {
        return this.status.isLoading() || this.services.status.isLoading();
    }

    isDisabled(): boolean {
        return this.services.switcher.isSwitcherEnabled() && !this.services.switcher.getSwitcherValue();
    }

    getBlocks(): IEventLandingTemplateBlock[] {
        let enabled = this.services.model.config.blocks.enabled;

        return this.services.model.config.blocks.sources
            .filter(item => {
                return !~[
                    EventLandingTemplateBlock.Header,
                    EventLandingTemplateBlock.Program,
                ].indexOf(item.type);
            })
            .filter(item => {
                return enabled[item.type];
            })
            .sort((a, b) => {
                return this.configs.getOrder(a.type) - this.configs.getOrder(b.type);
            });
    }

    getHeaderBlock(): EventLandingTemplateBlockHeader {
        return <EventLandingTemplateBlockHeader> this.services.model.config.blocks.sources.filter(b => {
            return b.type === EventLandingTemplateBlock.Header;
        })[0];
    }

    getProgramBlocks(): EventLandingTemplateBlockProgram[] {
        return <EventLandingTemplateBlockProgram[]> this.services.model.config.blocks.sources
            .filter(b => {
                return b.type === EventLandingTemplateBlock.Program;
            })
            .sort((a, b) => {
                return a.zIndex - b.zIndex;
            })
        ;
    }
}