import {EditorComponent} from "../../../../../../../decorators/Component";

import {EventLandingTemplateBlockPhotoGallery, EventLandingTemplateBlock, EventLandingTemplateAttachment} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {AttachmentRESTService} from "../../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {LoadingManager} from "../../../../../../../../main/module/common/helpers/LoadingManager";
import {UIAlertModalService} from "../../../../../../../../main/module/ui/service/UIAlertModalService";
import {AttachmentEntity} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {UploadObservableProgress} from "../../../../../../../../../../../definitions/src/functions/upload";
import {ListWindow} from "../../../../../../../../main/helpers/ListWindow";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-block-photo-gallery',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        '$timeout',
        AttachmentRESTService,
        UIAlertModalService
    ],
    bindings: {
        block: '<'
    }
})
export class EventLandingEditorV1BlockPhotoGallery implements ng.IComponentController
{
    public static LIST_SIZE = 3;

    public window: ListWindow<EventLandingTemplateAttachment> = new ListWindow<EventLandingTemplateAttachment>(EventLandingEditorV1BlockPhotoGallery.LIST_SIZE);

    public block: EventLandingTemplateBlockPhotoGallery;
    public status: LoadingManager = new LoadingManager();

    private dragOver: boolean = false;

    constructor(
        private $timeout: angular.ITimeoutService,
        private attachment: AttachmentRESTService,
        private alert: UIAlertModalService
    ){}

    $onChanges(): void {
        this.normalize();

        if(this.block.chosenPhoto) {
            this.window.scrollTo(this.block.chosenPhoto, item => {
                return item.$attachment_id === this.block.chosenPhoto.$attachment_id;
            });
        }
    }

    uploadFile(file: File): void {
        if (file !== undefined) {
            if(file.name.search(/\.jpg|\.png|\.gif/) !== -1) {
                let loading = this.status.addLoading();
                let lastResult: AttachmentEntity<ImageAttachmentMetadata>;

                this.attachment.upload(file).subscribe(
                    next => {
                        if(! (next instanceof UploadObservableProgress)) {
                            lastResult = <any>next.entity;
                        }
                    },
                    error => {
                        this.$timeout(() => {
                            loading.is = false;

                            this.alert.open({
                                title: {
                                    value: 'hi.editor.event-landing.blocks.resources.photo-gallery.upload-error.title',
                                    translate: true,
                                },
                                text: {
                                    value: 'hi.editor.event-landing.blocks.resources.photo-gallery.upload-error.text',
                                    translate: true,
                                },
                                buttons: [
                                    {
                                        title: {
                                            value: 'hi.editor.event-landing.blocks.resources.photo-gallery.upload-error.close',
                                            translate: true,
                                        },
                                        click: modal => {
                                            this.$timeout(() => {
                                                loading.is = false;

                                                modal.close();
                                            });
                                        }
                                    }
                                ]
                            });
                        });
                    },
                    () => {
                        this.$timeout(() => {
                            loading.is = false;

                            this.block.photos.push({
                                $attachment_id: lastResult.id,
                                url: lastResult.link.url,
                            });

                            this.normalize();
                        });
                    }
                )
            } else {
                this.raiseImageInputError();
            }
        }
    }

    raiseImageInputError(): void {
        this.alert.open({
            title: {
                value: 'hi.editor.event-landing.blocks.resources.photo-gallery.photo-validation.title',
                translate: true,
            },
            text: {
                value: 'hi.editor.event-landing.blocks.resources.photo-gallery.photo-validation.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.editor.event-landing.blocks.resources.photo-gallery.photo-validation.close',
                        translate: true,
                    },
                    click: (modal) => {
                        modal.close();
                    }
                }
            ]
        })
    }

    normalize(): void {
        if(this.block.chosenPhoto === undefined) {
            if(this.block.photos.length) {
                this.block.chosenPhoto = this.block.photos[0];
            }
        }

        if(this.block.chosenPhoto) {
            if(this.block.photos.filter(p => p.$attachment_id === this.block.chosenPhoto.$attachment_id).length === 0) {
                this.block.chosenPhoto = undefined;
            }
        }

        this.window.items = this.block.photos;
    }

    onDragOut(): void {
        this.$timeout(() => {
            this.dragOver = false;
        });
    }

    onDragOver(): void {
        this.$timeout(() => {
            this.dragOver = true;
        });
    }

    onDropFiles($file: File): void {
        this.dragOver = false;
        this.uploadFile($file);
    }

    isSelected(photo: EventLandingTemplateAttachment): boolean {
        return this.block.chosenPhoto && this.block.chosenPhoto.$attachment_id === photo.$attachment_id;
    }

    selectPhoto(photo: EventLandingTemplateAttachment) {
        this.block.chosenPhoto = photo;
        this.normalize();
    }

    deletePhoto(photo: EventLandingTemplateAttachment) {
        this.block.photos = this.block.photos.filter(p => p.$attachment_id !== photo.$attachment_id);
        this.normalize();
    }

    getPhotoCSS(photo: EventLandingTemplateAttachment): any {
        return {
            'background-image': `url(${encodeURI(photo.url)})`,
        };
    }
}