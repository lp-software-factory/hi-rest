import {EditorService} from "../../../../decorators/Service";

import {randomId} from "../../../../../main/functions/randomId";
import {EventLandingTemplateBlock, IEventLandingTemplateBlock, EventLandingTemplateBlockHeader, EventLandingTemplateSIDLength, EventLandingTemplateBlockProgram, EventLandingTemplateBlockDressCode, EventLandingTemplateBlockGuestsList, EventLandingTemplateBlockWishes, EventLandingTemplateBlockPhotoGallery, EventLandingTemplateBlockCustom, EventLandingTemplateBlockSocialNetworks, EventLandingTemplateSocialNetworksList} from "../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {DressCodeService} from "../../../../../main/module/dress-code/service/DressCodeService";

@EditorService({
    injects: [
        DressCodeService,
    ]
})
export class EventLandingTemplateBlocksFactory
{
    constructor(
        private dressCodes: DressCodeService,
    ) {}

    createBlockOf(id: EventLandingTemplateBlock): IEventLandingTemplateBlock {
        switch(id) {
            default:
                throw new Error(`No idea how to create empty block for "${id}"`);
            case EventLandingTemplateBlock.Header:
                return this.createBlockHeader();
            case EventLandingTemplateBlock.Program:
                return this.createBlockProgram();
            case EventLandingTemplateBlock.DressCode:
                return this.createBlockDressCode();
            case EventLandingTemplateBlock.GuestsList:
                return this.createBlockGuestsList();
            case EventLandingTemplateBlock.Wishes:
                return this.createBlockWishes();
            case EventLandingTemplateBlock.PhotoGallery:
                return this.createBlockPhotoGallery();
            case EventLandingTemplateBlock.Custom:
                return this.createBlockCustom();
            case EventLandingTemplateBlock.SocialNetworks:
                return this.createBlockSocialNetworks();

        }
    }

    createBlockHeader(): EventLandingTemplateBlockHeader {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.Header,
            isOrganizerEnabled: true,
            isDateEnabled: true,
            date: '',
            eventTitle: '',
            organizer: '',
            color: '',
            description: '',
            images: [],
            musics: [],
            chosenImage: {
                $attachment_id: undefined,
                url: undefined
            },
            chosenMusic: {
                $attachment_id: undefined,
                url: undefined
            }
        };
    }

    createBlockProgram(): EventLandingTemplateBlockProgram {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.Program,
            programTitle: '',
            programDescription: '',
            time: {
                start: '',
                end: ''
            },
            markers: [],
            file: {
                $attachment_id: undefined,
                url: undefined
            },
            photo: {
                $attachment_id: undefined,
                url: undefined
            }
        };
    }

    createBlockDressCode(): EventLandingTemplateBlockDressCode {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.DressCode,
            useId: this.dressCodes.getDefaultDressCode().entity.id,
            custom: {
                image: {
                    $attachment_id: undefined,
                    url: undefined,
                },
                men: undefined,
                women: undefined,
            },
            title: undefined
        };
    }

    createBlockGuestsList(): EventLandingTemplateBlockGuestsList {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.GuestsList,
            guestListTitle: ''
        };
    }

    createBlockWishes(): EventLandingTemplateBlockWishes {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.Wishes,
            title: '',
            wishList: []
        };
    }

    createBlockPhotoGallery(): EventLandingTemplateBlockPhotoGallery {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.PhotoGallery,
            galleryTitle: '',
            chosenPhoto: {
                $attachment_id: undefined,
                url: undefined
            },
            photos: []
        };
    }

    createBlockCustom(): EventLandingTemplateBlockCustom {
        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.Custom,
            customTitle: '',
            customDescription: '',
            file: {
                $attachment_id: undefined,
                url: undefined
            },
            photo: {
                $attachment_id: undefined,
                url: undefined
            }
        };
    }

    createBlockSocialNetworks(): EventLandingTemplateBlockSocialNetworks {
        let enabled = {};

        for(let network of EventLandingTemplateSocialNetworksList) {
            enabled[network] = false;
        }

        return {
            sid: randomId(EventLandingTemplateSIDLength),
            zIndex: + new Date(),
            type: EventLandingTemplateBlock.SocialNetworks,
            enabled: enabled,
        };
    }
}