import {HIModule} from "../../../../../modules";
import {EventLandingEditorV1Service} from "./service/EventLandingEditorV1Service";
import {EventLandingEditorV1ModelService} from "./service/EventLandingEditorV1/EventLandingEditorV1ModelService";
import {EventLandingEditorV1} from "./component/EventLandingEditorV1/index";
import {EventLandingEditorV1Unavailable} from "./component/EventLandingEditorV1/component/EventLandingEditorV1Unavailable/index";
import {EventLandingEditorV1AttachmentService} from "./service/EventLandingEditorV1/EventLandingEditorV1AttachmentService";
import {EventLandingEditorV1SwitcherService} from "./service/EventLandingEditorV1/EventLandingEditorV1SwitcherService";
import {EventLandingEditorV1Disabled} from "./component/EventLandingEditorV1/component/EventLandingEditorV1Disabled/index";
import {EventLandingEditorV1SwitcherBlock} from "./component/EventLandingEditorV1/component/EventLandingEditorV1Switchers/index";
import {EventLandingEditorV1BlockCustom} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockCustom/index";
import {EventLandingEditorV1BlockDressCode} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockDressCode/index";
import {EventLandingEditorV1BlockGuestsList} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockGuestsList/index";
import {EventLandingEditorV1BlockPhotoGallery} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockPhotoGallery/index";
import {EventLandingEditorV1BlockProgram} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockProgram/index";
import {EventLandingEditorV1BlockWishes} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockWishes/index";
import {EventLandingEditorV1BlockSocialNetworks} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1SocialNetworks/index";
import {EventLandingEditorV1BlockHeader} from "./component/EventLandingEditorV1/blocks/EventLandingEditorV1BlockHeader/index";
import {EventLandingTemplateBlocksFactory} from "./factory/EventLandingTemplateBlocksFactory";
import {EventLandingEditorV1TextInput} from "./component/EventLandingEditorV1/component/EventLandingEditorV1TextInput/index";
import {EventLandingEditorV1ControlButtonsBlock} from "./component/EventLandingEditorV1/component/EventLandingEditorV1ControlButtonsBlock/index";
import {HiGoogleMapV1} from "../../../../main/module/common/component/GoogleMap/index";
import {EventLandingEditButtonDirective} from "./component/EventLandingEditorV1/directive/EventLandingEditButtonDirective/index";
import {EventLandingWishListDirective} from "./component/EventLandingEditorV1/directive/EventLandingWishListDirective/index";
import {EventLandingEditorPreviewButton} from "./component/EventLandingEditorV1/component/EventLandingEditorPreviewButton/index";

export const HI_EVENT_LANDING_EDITOR_V1: HIModule = {
    components: [
        HiGoogleMapV1,
        EventLandingEditorV1,
        EventLandingEditorV1SwitcherBlock,
        EventLandingEditorV1Unavailable,
        EventLandingEditorV1Disabled,
        EventLandingEditorV1TextInput,
        EventLandingEditorV1BlockCustom,
        EventLandingEditorV1BlockDressCode,
        EventLandingEditorV1BlockGuestsList,
        EventLandingEditorV1BlockPhotoGallery,
        EventLandingEditorV1BlockProgram,
        EventLandingEditorV1BlockWishes,
        EventLandingEditorV1BlockSocialNetworks,
        EventLandingEditorV1BlockHeader,
        EventLandingEditorV1ControlButtonsBlock,
        EventLandingEditorPreviewButton,
    ],
    directives: [
        EventLandingEditButtonDirective,
        EventLandingWishListDirective,
    ],
    services: [
        EventLandingEditorV1Service,
        EventLandingEditorV1ModelService,
        EventLandingEditorV1AttachmentService,
        EventLandingEditorV1SwitcherService,
        EventLandingTemplateBlocksFactory,
    ]
};