import {EditorService} from "../../../../decorators/Service";

import {EventLandingEditorV1ModelService} from "./EventLandingEditorV1/EventLandingEditorV1ModelService";
import {EventLandingEditorV1SwitcherService} from "./EventLandingEditorV1/EventLandingEditorV1SwitcherService";
import {EventLandingEditorV1AttachmentService} from "./EventLandingEditorV1/EventLandingEditorV1AttachmentService";
import {LoadingManager} from "../../../../../main/module/common/helpers/LoadingManager";
import {Observable, Subject} from "rxjs";
import {EventLandingTemplateEditResponse200, EventLandingTemplateEditRequest} from "../../../../../../../../definitions/src/definitions/events/event-landing/paths/edit";
import {EventLandingTemplateRESTService} from "../../../../../../../../definitions/src/services/event/event-landing-template/EventLandingTemplateRESTService";

@EditorService({
    injects: [
        '$timeout',
        EventLandingTemplateRESTService,
        EventLandingEditorV1ModelService,
        EventLandingEditorV1SwitcherService,
        EventLandingEditorV1AttachmentService,
    ]
})
export class EventLandingEditorV1Service
{
    public status: LoadingManager = new LoadingManager();

    public saveSubject: Subject<EventLandingTemplateEditResponse200> = new Subject<EventLandingTemplateEditResponse200>();

    constructor(
        private $timeout: ng.ITimeoutService,
        private rest: EventLandingTemplateRESTService,

        public model: EventLandingEditorV1ModelService,
        public switcher: EventLandingEditorV1SwitcherService,
        public attachments: EventLandingEditorV1AttachmentService,
    ) {
        model.services = this;
        switcher.services = this;
        attachments.services = this;
    }

    canSave(): boolean {
        return true;
    }

    save(silence: boolean = false): Observable<EventLandingTemplateEditResponse200> {
        if(! this.canSave()) {
            throw new Error('Unable to save event landing template at this moment');
        }

        return Observable.create(observer => {
            this.createEditEventLandingRequest().subscribe(
                request => {
                    let loading = this.status.addLoading();
                    let observable = this.rest.edit(this.model.template.entity.id, request);

                    observable.subscribe(
                        next => {
                            this.$timeout(() => {
                                loading.is = false;

                                if(! silence) {
                                    this.saveSubject.next(next);
                                }

                                observer.next(observable);
                                observer.complete();
                            })
                        },
                        error => {
                            this.$timeout(() => {
                                loading.is = false;

                                observer.error(error);
                                observer.complete();
                            });
                        }
                    );
                },
                error => {
                    this.$timeout(() => {
                        observer.error(error);
                        observer.complete();
                    })
                }
            );
        });
    }

    createEditEventLandingRequest(): Observable<EventLandingTemplateEditRequest> {
        return Observable.create(observer => {
            let request: EventLandingTemplateEditRequest = {
                json: this.model.config,
            };

            observer.next(request);
            observer.complete();
        });
    }
}