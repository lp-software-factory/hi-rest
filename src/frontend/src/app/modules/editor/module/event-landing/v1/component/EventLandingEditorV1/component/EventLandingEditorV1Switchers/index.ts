import {EditorComponent} from "../../../../../../../decorators/Component";

import {EventLandingEditorV1Service} from "../../../../service/EventLandingEditorV1Service";
import {EventLandingSwitcher} from "../../../../service/EventLandingEditorV1/EventLandingEditorV1SwitcherService";
import {EventLandingTemplateBlockConfig, EventLandingTemplateBlock} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {EventLandingTemplateConfigsService} from "../../../../../../../../main/module/event/editor/service/EventLandingTemplateConfigsService";
import {EventLandingTemplateBlocksFactory} from "../../../../factory/EventLandingTemplateBlocksFactory";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-switchers',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        EventLandingEditorV1Service,
        EventLandingTemplateConfigsService,
        EventLandingTemplateBlocksFactory,
    ]
})
export class EventLandingEditorV1SwitcherBlock implements ng.IComponentController
{
    private switchers: EventLandingTemplateBlockConfig[] = [];

    constructor(
        private $timeout: ng.ITimeoutService,
        private services: EventLandingEditorV1Service,
        private configs: EventLandingTemplateConfigsService,
        private factory: EventLandingTemplateBlocksFactory,
    ) {
        this.switchers = this.configs.all().sort((a, b) => { return a.order - b.order });
    }

    save(): void {
        this.services.save().subscribe(() => {});
    }

    isSwitcherEnabled(): boolean {
        return this.services.switcher.isSwitcherEnabled();
    }

    getSwitcher(): EventLandingSwitcher {
        if(this.isSwitcherEnabled()) {
            return this.services.switcher.current;
        }else{
            throw new Error("Switcher is not enabled");
        }
    }

    toggleSwitcher(): void {
        if(this.isSwitcherEnabled()) {
            this.services.switcher.toggleSwitcher();
        }else{
            throw new Error("Switcher is not enabled");
        }
    }

    isDisabledBySwitcher(): boolean {
        return !this.isSwitcherEnabled() || !this.getSwitcher().enabled;
    }

    isHeaderSwitcherEnabled(): boolean {
        return !this.isDisabledBySwitcher();
    }

    blockHasSwitcher(id: EventLandingTemplateBlock): boolean {
        return this.configs.isToggleable(id);
    }

    toggleBlock(id: EventLandingTemplateBlock): void {
        this.services.model.toggleBlock(id);
    }

    isBlockEnabled(id: EventLandingTemplateBlock): boolean {
        return this.services.model.isBlockTypeEnabled(id);
    }
}