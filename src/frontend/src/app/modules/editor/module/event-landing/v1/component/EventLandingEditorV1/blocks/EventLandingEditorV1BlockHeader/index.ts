import {EditorComponent} from "../../../../../../../decorators/Component";

import {AttachmentRESTService} from "../../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EventLandingTemplateBlockHeader, EventLandingTemplateAttachment} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../../../main/module/common/helpers/LoadingManager";
import {UploadObservableProgress} from "../../../../../../../../../../../definitions/src/functions/upload";
import {AttachmentEntity} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {UIAlertModalService} from "../../../../../../../../main/module/ui/service/UIAlertModalService";
import {ITranslateServiceInterface} from "../../../../../../../../main/module/i18n/service/TranslationService";
import {ListWindow} from "../../../../../../../../main/helpers/ListWindow";

enum FileType {
    Music = <any>'music',
    Image = <any>'image'
}

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-block-header',
    template: require('./template.jade'),
    styles: [
        require('./resources/styles/style.shadow.scss'),
        require('./resources/styles/style.uploaded-list.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
        '$translate',
        AttachmentRESTService,
        UIAlertModalService
    ]
})

export class EventLandingEditorV1BlockHeader implements ng.IComponentController
{
    public static LIST_WINDOW_SIZE = 2;

    public status: LoadingManager = new LoadingManager();
    public block: EventLandingTemplateBlockHeader;

    public windowPhotos: ListWindow<EventLandingTemplateAttachment> = new ListWindow<EventLandingTemplateAttachment>(EventLandingEditorV1BlockHeader.LIST_WINDOW_SIZE);
    public windowMusics: ListWindow<EventLandingTemplateAttachment> = new ListWindow<EventLandingTemplateAttachment>(EventLandingEditorV1BlockHeader.LIST_WINDOW_SIZE);

    private sub: {
        imageBlockVisible: boolean,
        musicBlockVisible: boolean,
        otherBlockVisible: boolean,
        datePickerVisible: boolean,
        colorPickerVisible: boolean,
        haveDateInfo: boolean
    } = {
        imageBlockVisible: false,
        musicBlockVisible: false,
        otherBlockVisible: false,
        datePickerVisible: false,
        colorPickerVisible: false,
        haveDateInfo: true
    };

    private dragOver: boolean = false;
    
    private monthNames = [
        'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
    ];

    constructor(
        private $timeout: angular.ITimeoutService,
        private $translate: ITranslateServiceInterface,
        private attachment: AttachmentRESTService,
        private alert: UIAlertModalService,
    ) {
        let translation = this.monthNames.map(m => 'hi.common.months.' + m);

        this.$translate(translation).then(data => {
            this.$timeout(() => {
                this.monthNames = [];

                translation.forEach(m => {
                    this.monthNames.push(data[m]);
                })
            });
        });
    }

    $onChanges(): void {
        this.normalizeFiles();

        if(this.block.chosenImage) {
            this.windowPhotos.scrollTo(this.block.chosenImage, item => {
                return item.$attachment_id === this.block.chosenImage.$attachment_id;
            });
        }

        if(this.block.chosenMusic) {
            this.windowMusics.scrollTo(this.block.chosenMusic, item => {
                return item.$attachment_id === this.block.chosenMusic.$attachment_id;
            });
        }
    }
    
    normalizeFiles(): void {
        this.windowPhotos.items = this.block.images;
        this.windowMusics.items = this.block.musics;

        if(this.block.chosenImage === undefined || this.block.chosenImage.$attachment_id === undefined) {
            if(this.block.images.length) {
                this.block.chosenImage = this.block.images[0];
            }
        }

        if(this.block.chosenImage) {
            if(this.block.images.filter(p => p.$attachment_id === this.block.chosenImage.$attachment_id).length === 0) {
                this.block.chosenImage = {
                    $attachment_id: undefined,
                    url: undefined
                };
            }
        }

        if(this.block.chosenMusic === undefined || this.block.chosenMusic.$attachment_id === undefined) {
            if(this.block.musics.length) {
                this.block.chosenMusic = this.block.musics[0];
            }
        }

        if(this.block.chosenMusic) {
            if(this.block.musics.filter(p => p.$attachment_id === this.block.chosenMusic.$attachment_id).length === 0) {
                this.block.chosenMusic = {
                    $attachment_id: undefined,
                    url: undefined
                };
            }
        }
    }

    uploadFile(file: File, prop: FileType): void {
        if(! file) return;

        if(!!~['image/jpeg', 'image/png', 'image/gif', 'audio/mpeg', 'audio/mp3'].indexOf(file.type)) {
            let loading = this.status.addLoading();
            let lastResult: AttachmentEntity<ImageAttachmentMetadata>;

            this.attachment.upload(file).subscribe(
                next => {
                    if(! (next instanceof UploadObservableProgress)) {
                        lastResult = <any>next.entity;
                    }
                },
                error => {
                    this.$timeout(() => {
                        loading.is = false;

                        this.alert.open({
                            title: {
                                value: 'hi.editor.event-landing.blocks.resources.header.upload-error.title',
                                translate: true,
                            },
                            text: {
                                value: 'hi.editor.event-landing.blocks.resources.header.upload-error.text',
                                translate: true,
                            },
                            buttons: [
                                {
                                    title: {
                                        value: 'hi.editor.event-landing.blocks.resources.header.upload-error.close',
                                        translate: true,
                                    },
                                    click: modal => {
                                        this.$timeout(() => {
                                            loading.is = false;

                                            modal.close();
                                        });
                                    }
                                }
                            ]
                        });
                    });
                },
                () => {
                    if(prop === FileType.Image) {
                        this.$timeout(() => {
                            loading.is = false;

                            this.block.images.push({
                                $attachment_id: lastResult.id,
                                url: lastResult.link.url,
                            });

                            this.normalizeFiles();
                        });
                    } else if(prop === FileType.Music) {
                        this.$timeout(() => {
                            loading.is = false;

                            this.block.musics.push({
                                $attachment_id: lastResult.id,
                                url: lastResult.link.url,
                            });
                        });
                    }
                }
            )
        }
    }

    raiseMusicInputError(): void {
        this.alert.open({
            title: {
                value: 'hi.editor.event-landing.blocks.resources.header.audio-validation.title',
                translate: true,
            },
            text: {
                value: 'hi.editor.event-landing.blocks.resources.header.audio-validation.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.editor.event-landing.blocks.resources.header.audio-validation.close',
                        translate: true,
                    },
                    click: (modal) => {
                        modal.close();
                    }
                }
            ]
        })
    }
    
    raiseImageInputError(): void {
        this.alert.open({
            title: {
                value: 'hi.editor.event-landing.blocks.resources.header.photo-validation.title',
                translate: true,
            },
            text: {
                value: 'hi.editor.event-landing.blocks.resources.header.photo-validation.text',
                translate: true,
            },
            buttons: [
                {
                    title: {
                        value: 'hi.editor.event-landing.blocks.resources.header.photo-validation.close',
                        translate: true,
                    },
                    click: (modal) => {
                        modal.close();
                    }
                }
            ]
        })
    }

    hasHeader(): boolean {
        return this.block.isDateEnabled || this.block.isOrganizerEnabled;
    }

    onDragOut(): void {
        this.$timeout(() => {
            this.dragOver = false;
        });
    }

    onDragOver(): void {
        this.$timeout(() => {
            this.dragOver = true;
        });
    }

    onDropFiles($file: File, prop: FileType): void {
        this.dragOver = false;
        this.uploadFile($file, prop);
    }
    
    getColorCSS() {
        return {'background': `linear-gradient(transparent, ${this.getCurrentBGColor()})`}
    }

    getMonth() {
        let date = new Date(this.block.date);
        return this.monthNames[date.getMonth()];
    }

    getDate() {
        let date = new Date(this.block.date);
        return date.getDate();
    }

    getYear() {
        let date = new Date(this.block.date);
        return date.getFullYear();
    }

    getCurrentBGColor() {
        return this.block.color;
    }

    getCurrentImage() {
        if(this.block.chosenImage.url !== undefined) {
            return this.block.chosenImage.url;
        } else return '/dist/img/event-img.jpg';
    }

    haveMusics(): boolean {
        return this.block.musics.length > 0
    }

    hasDate(): boolean {
        return this.block.date.length > 0;
    }

    chooseMusic(index) {
        this.block.chosenMusic = this.block.musics[index];
    }

    getButtonCSS(index): any {
        return {
            'background-color': `whitesmoke`,
            'color': 'rgba(54, 54, 54, 0.3)',
            'border-color': '#dbdbdb',
            'cursor': 'not-allowed',
            'pointer-events': 'none'
        };
    }

    getPhotoCSS(photo: EventLandingTemplateAttachment): any {
        return {
            'background-image': `url(${encodeURI(photo.url)})`,
        };
    }

    hasImages(): boolean {
        return this.block.images.length > 0;
    }

    isPhotoSelected(photo: EventLandingTemplateAttachment): boolean {
        return this.block.chosenImage && this.block.chosenImage.$attachment_id === photo.$attachment_id;
    }

    selectPhoto(photo: EventLandingTemplateAttachment) {
        this.block.chosenImage = photo;
        this.normalizeFiles();
    }

    canDeletePhoto(photo: EventLandingTemplateAttachment): boolean {
        return this.block.images.length > 1;
    }

    deletePhoto(photo: EventLandingTemplateAttachment) {
        this.block.images = this.block.images.filter(p => p.$attachment_id !== photo.$attachment_id);
        this.normalizeFiles();
    }

    isMusicSelected(music: EventLandingTemplateAttachment): boolean {
        return this.block.chosenMusic && this.block.chosenMusic.$attachment_id === music.$attachment_id;
    }

    selectMusic(music: EventLandingTemplateAttachment) {
        this.block.chosenMusic = music;
        this.normalizeFiles();
    }

    deleteMusic(music: EventLandingTemplateAttachment) {
        this.block.musics = this.block.musics.filter(p => p.$attachment_id !== music.$attachment_id);
        this.normalizeFiles();
    }

    changeColorPickerBlockStatement(){
        this.sub.colorPickerVisible = !this.sub.colorPickerVisible;
    }

    changeDatePickerBlockStatement(){
        this.sub.imageBlockVisible = false;
        this.sub.musicBlockVisible = false;
        this.sub.otherBlockVisible = false;

        this.sub.datePickerVisible = !this.sub.datePickerVisible;
    }

    changeImageBlockVisibleStatement(){
        this.sub.musicBlockVisible = false;
        this.sub.otherBlockVisible = false;
        this.sub.datePickerVisible = false;

        this.sub.imageBlockVisible = !this.sub.imageBlockVisible;
    }

    changeMusicBlockVisibleStatement(){
        this.sub.imageBlockVisible = false;
        this.sub.otherBlockVisible = false;
        this.sub.datePickerVisible = false;

        this.sub.musicBlockVisible = !this.sub.musicBlockVisible;
    }

    changeOtherBlockVisibleStatement(){
        this.sub.imageBlockVisible = false;
        this.sub.musicBlockVisible = false;
        this.sub.datePickerVisible = false;

        this.sub.otherBlockVisible = !this.sub.otherBlockVisible;
    }
}