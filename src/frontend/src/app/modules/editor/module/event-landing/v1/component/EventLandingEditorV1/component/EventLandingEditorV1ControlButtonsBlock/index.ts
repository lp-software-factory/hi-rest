import {EditorComponent} from "../../../../../../../decorators/Component";

import {EventLandingEditorV1Service} from "../../../../service/EventLandingEditorV1Service";
import {IEventLandingTemplateBlock} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

enum BooleanValue
{
    True = <any>"true",
    False = <any>"false",
}

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-control-buttons-block',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<',
        addBlock: '@',
    },
    injects: [
        '$timeout',
        EventLandingEditorV1Service,
    ]
})
export class EventLandingEditorV1ControlButtonsBlock implements ng.IComponentController
{
    public block: IEventLandingTemplateBlock;

    public addBlock: BooleanValue = BooleanValue.True;

    constructor(
        private $timeout: ng.ITimeoutService,
        private services: EventLandingEditorV1Service,
    ) {}

    deleteBlock(): void {
        if(this.services.model.numBlockOfTypes(this.block.type) > 1) {
            this.services.model.deleteBlock(this.block);
        }else{
            this.services.model.toggleBlock(this.block.type);
        }
    }

    isAddBlockFeatureEnabled(): boolean {
        return this.addBlock === BooleanValue.True;
    }
    
    addNewBlock() {
        if(this.isAddBlockFeatureEnabled()) {
            this.services.model.addNewBlock(this.block.type);
        }
    }
}