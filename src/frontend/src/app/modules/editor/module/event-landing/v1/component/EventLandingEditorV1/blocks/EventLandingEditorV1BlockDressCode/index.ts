import {EditorComponent} from "../../../../../../../decorators/Component";

import {Subscription} from "rxjs";
import {EventLandingEditorV1Service} from "../../../../service/EventLandingEditorV1Service";
import {EventLandingTemplateBlockDressCode, EventLandingTemplateBlock} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {DressCodeService} from "../../../../../../../../main/module/dress-code/service/DressCodeService";
import {LocaleService} from "../../../../../../../../main/module/locale/service/LocaleService";
import {LoadingManager} from "../../../../../../../../main/module/common/helpers/LoadingManager";
import {AttachmentRESTService} from "../../../../../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {UIAlertModalService} from "../../../../../../../../main/module/ui/service/UIAlertModalService";
import {AttachmentEntity} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {UploadObservableProgress} from "../../../../../../../../../../../definitions/src/functions/upload";
import {DressCode} from "../../../../../../../../../../../definitions/src/definitions/dress-code/entity/DressCode";

@EditorComponent({
    selector: 'hi-event-landing-editor-v1-block-dress-code',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    injects: [
        '$timeout',
        EventLandingEditorV1Service,
        AttachmentRESTService,
        DressCodeService,
        LocaleService,
        UIAlertModalService,
    ],
    bindings: {
        block: '<'
    }
})
export class EventLandingEditorV1BlockDressCode implements ng.IComponentController
{
    private subscription: Subscription;

    private isListOpened: boolean = false;
    public status: LoadingManager = new LoadingManager();
    public block: EventLandingTemplateBlockDressCode;

    private _menDescription: string;
    private _womenDescription: string;

    constructor(
        private $timeout: ng.ITimeoutService,
        private services: EventLandingEditorV1Service,
        private attachment: AttachmentRESTService,
        private dressCodes: DressCodeService,
        private locale: LocaleService,
        private alert: UIAlertModalService
    ) {}

    $onInit(): void {
        this.$timeout(() => {
            this._menDescription = this.getMenDescription();
            this._womenDescription = this.getWomenDescription();
        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    isAvailable(): boolean {
        return this.dressCodes.available.length > 0;
    }

    getUsedDressCodeTitle(): string {
        if(this.block.useId) {
            let dressCode = this.dressCodes.getById(this.block.useId);

            return this.locale.localize(dressCode.entity.title);
        }else{
            return '–';
        }
    }

    toggleDressCodeVariants(): void {
        this.isListOpened = !this.isListOpened;
    }

    listDressCodes(): DressCode[] {
        return this.dressCodes.available.sort((a, b) => { return a.entity.position - b.entity.position });
    }

    useDressCode(dressCode: DressCode): void {
        this.isListOpened = false;

        this.block.useId = dressCode.entity.id;
        this._menDescription = this.locale.localize(dressCode.entity.description_men);
        this._womenDescription = this.locale.localize(dressCode.entity.description_women);
        this.block.custom.image.$attachment_id = undefined;
        this.block.custom.image.url = undefined;
        this.block.custom.men = undefined;
        this.block.custom.women = undefined;
    }

    getDressCodeTitle(dressCode: DressCode): string {
        return this.locale.localize(dressCode.entity.title);
    }

    uploadFile(file: File): void {
        let loading = this.status.addLoading();
        let lastResult: AttachmentEntity<ImageAttachmentMetadata>;

        this.attachment.upload(file).subscribe(
            next => {
                if(! (next instanceof UploadObservableProgress)) {
                    lastResult = <any>next.entity;
                }
            },
            error => {
                this.$timeout(() => {
                    loading.is = false;

                    this.alert.open({
                        title: {
                            value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.title',
                            translate: true,
                        },
                        text: {
                            value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.text',
                            translate: true,
                        },
                        buttons: [
                            {
                                title: {
                                    value: 'hi.main.event.landing.blocks.resources.dress-code.upload.error.close',
                                    translate: true,
                                },
                                click: modal => {
                                    this.$timeout(() => {
                                        loading.is = false;

                                        modal.close();
                                    });
                                }
                            }
                        ]
                    });
                });
            },
            () => {
                this.$timeout(() => {
                    loading.is = false;

                    this.block.custom.image = {
                        $attachment_id: lastResult.id,
                        url: lastResult.link.url,
                    };
                });
            }
        )
    }

    getMenPlaceholder(): string {
        if(this.block.useId) {
            let dressCode = this.dressCodes.getById(this.block.useId);

            return this.locale.localize(dressCode.entity.description_men);
        }else{
            return '';
        }
    }

    getWomenPlaceholder(): string {
        if(this.block.useId) {
            let dressCode = this.dressCodes.getById(this.block.useId);

            return this.locale.localize(dressCode.entity.description_women);
        }else{
            return '';
        }
    }

    getMenDescription(): string {
        if(this.block.custom.men) {
            return this.block.custom.men;
        }else{
            let dressCode = this.dressCodes.getById(this.block.useId);

            return this.locale.localize(dressCode.entity.description_men);
        }
    }

    getWomenDescription(): string {
        if(this.block.custom.women) {
            return this.block.custom.women;
        }else{
            let dressCode = this.dressCodes.getById(this.block.useId);

            return this.locale.localize(dressCode.entity.description_women);
        }
    }

    getImageCSS(): any {
        return {
            'background-image': `url(${encodeURI(this.getImageURL())})`,
        };
    }

    getImageURL(): string {
        if(this.block.custom.image.url) {
            return this.block.custom.image.url;
        }else{
            let dressCode = this.dressCodes.getById(this.block.useId);

            return dressCode.entity.image.link.url;
        }
    }

    get womenDescription(): string {
        return this._womenDescription;
    }

    set womenDescription(value: string) {
        this._womenDescription = value;
        
        if(this.block.useId) {
            let dressCode = this.dressCodes.getById(this.block.useId);
            
            if(this._womenDescription.length === 0) {
                this.block.custom.women = undefined;
            }else{
                this.block.custom.women = this._womenDescription;
            }
        }else{
            this.block.custom.women = this._womenDescription;
        }
    }

    get menDescription(): string {
        return this._menDescription;
    }

    set menDescription(value: string) {
        this._menDescription = value;

        if(this.block.useId) {
            let dressCode = this.dressCodes.getById(this.block.useId);

            if(this._menDescription.length === 0) {
                this.block.custom.men = undefined;
            }else{
                this.block.custom.men = this._menDescription;
            }
        }else{
            this.block.custom.men = this._menDescription;
        }
    }
}