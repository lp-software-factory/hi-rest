import {EventDirective} from "../../../../../../../decorators/Directive";

import {DirectiveFactoryInterface} from "../../../../../../../../../angularized/decorators/Directive";

require('./select2.min.css');

@EventDirective({
    name: 'hi-event-landing-wish-list',
    injects: [
        '$timeout',
    ]
})
export class EventLandingWishListDirective implements DirectiveFactoryInterface
{
    factory(): angular.IDirectiveFactory {
        return function($timeout: ng.ITimeoutService) {
            return {
                require: 'ngModel',
                priority: 1,
                link: function(scope: ng.IScope, element, attr, ngModel: ng.INgModelController) {
                    let selector = window['$'](element[0]);
                    let rendered = false;

                    ngModel.$render = function() {
                        if(! rendered) {
                            let initialValue = ngModel.$modelValue;

                            selector.select2({
                                tags: true,
                                multiple: true,
                            });

                            selector.on('change', function() {
                                ngModel.$setViewValue(selector.val());
                            });

                            if(Array.isArray(initialValue)) {
                                initialValue.forEach(value => {
                                    window['$']('<option>')
                                        .val(value)
                                        .text(value)
                                        .attr('selected', 'selected')
                                        .appendTo(selector);
                                });
                            }

                            selector.trigger('change');
                        }

                        rendered = true;
                    };
                }
            };
        };
    }
}