import {EditorComponent} from "../../../../../decorators/Component";

@EditorComponent({
    selector: 'hi-event-header',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
})
export class EventHeader implements ng.IComponentController
{
}