import {EditorService} from "../../../../../decorators/Service";

import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {InviteCardFamily} from "../../../../../../../../../definitions/src/definitions/invites/invite-card-family/entity/InviteCardFamilyEntity";
import {EnvelopeEditorV1Service} from "../../../../../../main/module/envelope/envelope-editor/v1/service/EnvelopeEditorV1Service";
import {InviteCardEditorV1Service} from "../../../../../../main/module/invite/invite-card-editor/v1/service/InviteCardEditorV1Service";
import {ReplaySubject} from "rxjs";
import {EventLandingEditorV1Service} from "../../../../event-landing/v1/service/EventLandingEditorV1Service";

@EditorService({
    injects: [
        '$timeout',
        InviteCardEditorV1Service,
        EnvelopeEditorV1Service,
        EventLandingEditorV1Service,
    ]
})
export class CurrentEvent
{
    public static $name = 'hiEventEditorCurrentEventService';

    public ready: boolean = false;
    public changeSubject: ReplaySubject<HIEvent> = new ReplaySubject<HIEvent>();

    private _event: HIEvent;
    public family: {
        original: InviteCardFamily,
        current: InviteCardFamily,
    } = {
        original: undefined,
        current: undefined,
    };

    constructor(
        private $timeout: ng.ITimeoutService,
        private invite: InviteCardEditorV1Service,
        private envelope: EnvelopeEditorV1Service,
        private landing: EventLandingEditorV1Service,
    ) {
        invite.saveSubject.subscribe(next => {
            this.$timeout(() => {
                this._event.entity.solution.current.entity.invite_card_templates[0] = next.template;
            });
        });

        envelope.saveSubject.subscribe(next => {
            this.$timeout(() => {
                this._event.entity.solution.current.entity.envelope_templates[0] = next.envelope_template;
            });
        });

        envelope.switcher.switchSubject.subscribe(next => {
            this.$timeout(() => {
                this._event.entity.event_features.envelope = next;
            });
        });

        landing.saveSubject.subscribe(next => {
            this.$timeout(() => {
                this._event.entity.solution.current.entity.landing_templates[0] = next.event_landing_template;
            });
        });

        landing.switcher.switchSubject.subscribe(next => {
            this.$timeout(() => {
                this._event.entity.event_features.landing = next;
            });
        })
    }

    setup(event: HIEvent, currentFamily: InviteCardFamily, originalFamily: InviteCardFamily): void {
        this.family.current = currentFamily;
        this.family.original = originalFamily;
        this.event = event;
    }

    getOriginalFamily(): InviteCardFamily {
        return this.family.original;
    }

    getCurrentFamily(): InviteCardFamily {
        return this.family.current;
    }

    get event(): HIEvent {
        return this._event;
    }

    set event(value: HIEvent) {
        if(!!value) {
            this._event = value;
            this.ready = true;
            this.changeSubject.next(value);
        }else{
            this._event = undefined;
            this.ready = false;
        }
    }
}