import {EditorComponent} from "../../../../../../../decorators/Component";

import {CurrentEvent} from "../../service";
import {EventEditorService, EventEditorStage} from "../../../../../../../../main/module/event/editor/service/EventEditorService";
import {EventLandingTemplate} from "../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {Solution} from "../../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EventLandingSwitcher} from "../../../../../../event-landing/v1/service/EventLandingEditorV1/EventLandingEditorV1SwitcherService";

@EditorComponent({
    selector: 'hi-event-route-landing-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        $router: '<',
    },
    injects: [
        CurrentEvent,
        EventEditorService,
    ]
})
export class LandingEditorRoute implements ng.IComponentController
{
    public switcher: EventLandingSwitcher;

    public template: EventLandingTemplate;
    public current: Solution;
    public original: Solution;

    constructor(
        public currentEvent: CurrentEvent,
        private eventEditorService: EventEditorService,
    ) {
        currentEvent.changeSubject.subscribe(next => {
            this.current = this.currentEvent.event.entity.solution.current;
            this.original = this.currentEvent.event.entity.solution.original;
            this.template = this.currentEvent.event.entity.solution.current.entity.landing_templates[0];
            this.switcher = {
                enabled: this.currentEvent.event.entity.event_features.landing,
            };
        });
    }

    isReady(): boolean {
        return this.template !== undefined
            && this.current !== undefined
            && this.original !== undefined;
    }

    $routerOnActivate(next: any) {
        this.eventEditorService.stage = EventEditorStage.Landing;
    }

    getEventSID(): string {
        return this.currentEvent.event.entity.sid;
    }

    getCurrentTemplate(): EventLandingTemplate {
        return this.template;
    }

    getCurrentSolution(): Solution {
        return this.current;
    }

    getOriginalSolution(): Solution {
        return this.original;
    }
}
