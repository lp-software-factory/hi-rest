import {EditorComponent} from "../../../../../../../decorators/Component";

import {CurrentEvent} from "../../service";
import {EventEditorService, EventEditorStage} from "../../../../../../../../main/module/event/editor/service/EventEditorService";
import {AuthService} from "../../../../../../../../main/module/auth/service/AuthService";
import {InviteCardEditorV1EditorConfig} from "../../../../../../../../main/module/invite/invite-card-editor/v1/service/InviteCardEditorV1EditorConfig";
import {InviteCardEditorV1Service} from "../../../../../../../../main/module/invite/invite-card-editor/v1/service/InviteCardEditorV1Service";

@EditorComponent({
    selector: 'hi-event-route-invite-card-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        $router: '<',
    },
    injects: [
        AuthService,
        CurrentEvent,
        InviteCardEditorV1Service,
        EventEditorService,
    ],
})
export class InviteCardEditorRoute implements ng.IComponentController
{
    public config: InviteCardEditorV1EditorConfig;

    constructor(
        private auth: AuthService,
        private current: CurrentEvent,
        private service: InviteCardEditorV1Service,
        private eventEditorService: EventEditorService,
    ) {
        let isAdmin = this.auth.isAdmin();
        let isDesigner = this.auth.isDesigner();

        let config: InviteCardEditorV1EditorConfig = {
            source: {
                template: {
                    current: this.current.event.entity.solution.current.entity.invite_card_templates[0],
                    original: this.current.event.entity.solution.original.entity.invite_card_templates[0],
                },
                family: {
                    current: this.current.getCurrentFamily(),
                    original: this.current.getOriginalFamily(),
                },
            },
            svg_editor: {
                tools: {
                    "add-text-input": isAdmin || isDesigner,
                    "font-family": true,
                    "font-size": true,
                    "text-color": true,
                    "text-align-left": true,
                    "text-align-center": true,
                    "text-align-right": true,
                    "text-style-bold": true,
                    "text-style-italic": true,
                    "text-style-right": true,
                    "text-style-underline": true,
                },
                features: {
                    select: true,
                    create: isAdmin || isDesigner,
                    move: isAdmin || isDesigner,
                    resize: isAdmin || isDesigner,
                    rotate: isAdmin || isDesigner,
                    remove: isAdmin || isDesigner,
                }
            },
            toolbars: {
                "template-color": {
                    enabled: true,
                    features: {
                        drag: true,
                        list: isAdmin || isDesigner,
                        add: isAdmin || isDesigner,
                    }
                },
                "svg-editor": {
                    enabled: true,
                },
                "add-photo": {
                    enabled: true,
                },
                "add-music": {
                    enabled: true,
                },
                "revert-changes": {
                    enabled: true,
                },
                "replace-family": {
                    enabled: true,
                },
                "print": {
                    enabled: true,
                },
                "submit": {
                    enabled: true,
                },
                "preview": {
                    enabled: true,
                }
            }
        };

        service.setup(config);
    }

    $routerOnActivate(next: any) {
        this.eventEditorService.stage = EventEditorStage.InviteCard;
    }

    isReady(): boolean {
        return this.service.isReady && this.service.model.isReady;
    }
}
