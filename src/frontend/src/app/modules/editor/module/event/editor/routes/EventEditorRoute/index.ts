import {EditorComponent} from "../../../../../decorators/Component";

import {CurrentEvent} from "./service";
import {InviteCardEditorRoute} from "./routes/InviteCardEditorRoute/index";
import {EnvelopeEditorRoute} from "./routes/EnvelopeEditorRoute/index";
import {LandingEditorRoute} from "./routes/LandingEditorRoute/index";
import {MailingOptionsEditorRoute} from "./routes/MailingOptionsEditorRoute/index";
import {EventEditorService} from "../../../../../../main/module/event/editor/service/EventEditorService";
import {FontService} from "../../../../../../main/module/font/service/FontService";
import {FrontendService} from "../../../../../../main/service/FrontendService";
import {Subscription, Observable} from "rxjs";
import {IComponentRouter} from "../../../../../../../../../typings-custom/angular-component-router";
import {InviteCardEditorV1Service} from "../../../../../../main/module/invite/invite-card-editor/v1/service/InviteCardEditorV1Service";
import {EnvelopeEditorV1Service} from "../../../../../../main/module/envelope/envelope-editor/v1/service/EnvelopeEditorV1Service";
import {EventRESTService} from "../../../../../../../../../definitions/src/services/event/event/EventRESTService";
import {LoadingManager} from "../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingEditorV1Service} from "../../../../event-landing/v1/service/EventLandingEditorV1Service";
import {DressCodeService} from "../../../../../../main/module/dress-code/service/DressCodeService";

@EditorComponent({
    selector: 'hi-event-route-event-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        '$rootRouter',
        CurrentEvent,
        EventEditorService,
        EventRESTService,
        FontService,
        FrontendService,
        InviteCardEditorV1Service,
        EnvelopeEditorV1Service,
        EventLandingEditorV1Service,
        DressCodeService,
    ],
    routes: [
        {
            path: '/invite-card',
            name: 'HIEventInviteCardEditorRoute',
            component: InviteCardEditorRoute,
            useAsDefault: true,
        },
        {
            path: '/envelope',
            name: 'HIEventEnvelopeEditorRoute',
            component: EnvelopeEditorRoute,
        },
        {
            path: '/landing',
            name: 'HIEventEditorLandingRoute',
            component: LandingEditorRoute,
        },
        {
            path: '/mailing',
            name: 'HIEventEditorMailingOptionsRoute',
            component: MailingOptionsEditorRoute,
        },
    ]
})
export class EventEditorRoute implements ng.IComponentController
{
    public status: LoadingManager = new LoadingManager();

    private subscriptions: {
        invite: Subscription,
        envelope: Subscription,
        landing: Subscription,
    } = {
        invite: undefined,
        envelope: undefined,
        landing: undefined,
    };

    constructor(
        private $timeout: ng.ITimeoutService,
        private $router: IComponentRouter,
        private current: CurrentEvent,
        private service: EventEditorService,
        private rest: EventRESTService,
        private fonts: FontService,
        private frontend: FrontendService,
        private invite: InviteCardEditorV1Service,
        private envelope: EnvelopeEditorV1Service,
        private landing: EventLandingEditorV1Service,
        private dressCodes: DressCodeService,
    ) {
        frontend.replay.subscribe(next => {
            fonts.available = next.exports['fonts'];
        });

        frontend.replay.subscribe(next => {
            dressCodes.available = next.exports['dress_codes'];
        });

        frontend.replay.subscribe(response => {
            this.current.setup(
                response.exports['event'],
                response.exports['family']['original'],
                response.exports['family']['current'],
            );
        });
    }

    $onInit(): void {
        this.subscriptions.invite = this.invite.saveSubject.subscribe(next => {
            this.$timeout(() => {
                let loading = this.status.addLoading();

                this.savePrerequisites().subscribe(
                    next => {},
                    error => {
                        this.$timeout(() => {
                            loading.is = false;
                        });
                    },
                    () => {
                        this.$timeout(() => {
                            loading.is = false;

                            this.$router.navigate(['/HIEventEnvelopeEditorRoute']);
                        });
                    }
                );
            });
        });

        this.subscriptions.envelope = this.envelope.saveSubject.subscribe(next => {
            this.$timeout(() => {
                this.$timeout(() => {
                    let loading = this.status.addLoading();

                    this.savePrerequisites().subscribe(
                        next => {},
                        error => {
                            this.$timeout(() => {
                                loading.is = false;
                            });
                        },
                        () => {
                            this.$timeout(() => {
                                loading.is = false;

                                this.$router.navigate(['/HIEventEditorLandingRoute']);
                            });
                        }
                    );
                });
            });
        });

        this.subscriptions.landing = this.landing.saveSubject.subscribe(next => {
            this.$timeout(() => {
                this.$timeout(() => {
                    let loading = this.status.addLoading();

                    this.savePrerequisites().subscribe(
                        next => {},
                        error => {
                            this.$timeout(() => {
                                loading.is = false;
                            });
                        },
                        () => {
                            this.$timeout(() => {
                                loading.is = false;

                                this.$router.navigate(['/HIEventEditorMailingOptionsRoute']);
                            });
                        }
                    );
                });
            });
        });
    }

    $onDestroy(): void {
        if(this.subscriptions.invite) {
            this.subscriptions.invite.unsubscribe();
        }

        if(this.subscriptions.envelope) {
            this.subscriptions.envelope.unsubscribe();
        }

        if(this.subscriptions.landing) {
            this.subscriptions.landing.unsubscribe();
        }
    }

    isReady(): boolean {
        return this.current.ready;
    }

    savePrerequisites(): Observable<any[]> {
        let observables: Observable<any>[] = [];

        observables.push(this.rest.setTitle(this.current.event.entity.id, {
            title: this.current.event.entity.title,
        }));

        observables.push(this.rest.setFeatures(
            this.current.event.entity.id,
            this.current.event.entity.event_features,
        ));

        return Observable.forkJoin(observables);
    }
}