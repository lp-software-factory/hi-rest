import {EditorComponent} from "../../../../../../../decorators/Component";

import {CurrentEvent} from "../../service";
import {EventEditorService, EventEditorStage} from "../../../../../../../../main/module/event/editor/service/EventEditorService";

@EditorComponent({
    selector: 'hi-event-route-mailing-options-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        $router: '<',
    },
    injects: [
        CurrentEvent,
        EventEditorService,
    ]
})
export class MailingOptionsEditorRoute implements ng.IComponentController
{
    constructor(
        public current: CurrentEvent,
        private eventEditorService: EventEditorService,
    ) {}

    $routerOnActivate(next: any) {
        this.eventEditorService.stage = EventEditorStage.Mailing;
    }
}
