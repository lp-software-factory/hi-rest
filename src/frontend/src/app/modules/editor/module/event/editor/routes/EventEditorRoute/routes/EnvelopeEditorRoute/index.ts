import {EditorComponent} from "../../../../../../../decorators/Component";

import {CurrentEvent} from "../../service";
import {EventEditorService, EventEditorStage} from "../../../../../../../../main/module/event/editor/service/EventEditorService";
import {Solution} from "../../../../../../../../../../../definitions/src/definitions/solution/entity/Solution";
import {EnvelopeEditorV1Service} from "../../../../../../../../main/module/envelope/envelope-editor/v1/service/EnvelopeEditorV1Service";
import {EnvelopeEditorV1Switcher} from "../../../../../../../../main/module/envelope/envelope-editor/v1/service/EnvelopeEditorV1Service/EnvelopeEditorV1SwitcherService";

@EditorComponent({
    selector: 'hi-event-route-envelope-editor',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
    bindings: {
        $router: '<',
    },
    injects: [
        CurrentEvent,
        EventEditorService,
        EnvelopeEditorV1Service,
    ]
})
export class EnvelopeEditorRoute implements ng.IComponentController
{
    public switcher: EnvelopeEditorV1Switcher;

    constructor(
        public current: CurrentEvent,
        private eventEditorService: EventEditorService,
        private envelopeEditorService: EnvelopeEditorV1Service,
    ) {
        this.switcher = {
            enabled: current.event.entity.event_features.envelope,
        };
    }

    $routerOnActivate(next: any) {
        this.eventEditorService.stage = EventEditorStage.Envelope;
    }

    getCurrentSolution(): Solution {
        return this.current.event.entity.solution.current;
    }

    getOriginalSolution(): Solution {
        return this.current.event.entity.solution.original;
    }
}
