import {appEditor} from "../module"

import {DirectiveDefinition, DirectiveDefinitions} from "../../../angularized/decorators/Directive";

export function EventDirective(definition: DirectiveDefinition) {
    return function(target: Function) {
        DirectiveDefinitions.$instance.register(appEditor, <any>target, definition);
    };
}
