import {appEditor} from "../module"

import {FactoryDefinition, FactoryDefinitions, FactoryInterface} from "../../../angularized/decorators/Factory";

export function Factory(definition: FactoryDefinition) {
    return function(target: Function) {
        FactoryDefinitions.$instance.register(appEditor, <any>target, definition);
    };
}
