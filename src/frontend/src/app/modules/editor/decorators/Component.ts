import {appEditor} from "../module"

import {ComponentDefinitions, ComponentDefinition} from "../../../angularized/decorators/Component";

export function EditorComponent(definition: ComponentDefinition) {
    return function(target: Function) {
        ComponentDefinitions.$instance.register(appEditor, target, definition);
    };
}
