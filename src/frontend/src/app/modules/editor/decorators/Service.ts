import {appEditor} from "../module"

import {ServiceDefinition, ServiceDefinitions} from "../../../angularized/decorators/Service";

export function EditorService(definition: ServiceDefinition = {}) {
    return function(target: Function) {
        ServiceDefinitions.$instance.register(appEditor, target, definition);
    };
}
