import {HIModule} from "../../modules";

import {MailingOptionsEditorRoute} from "./module/event/editor/routes/EventEditorRoute/routes/MailingOptionsEditorRoute/index";
import {LandingEditorRoute} from "./module/event/editor/routes/EventEditorRoute/routes/LandingEditorRoute/index";
import {InviteCardEditorRoute} from "./module/event/editor/routes/EventEditorRoute/routes/InviteCardEditorRoute/index";
import {EnvelopeEditorRoute} from "./module/event/editor/routes/EventEditorRoute/routes/EnvelopeEditorRoute/index";
import {EventEditorRoute} from "./module/event/editor/routes/EventEditorRoute/index";
import {EventHeader} from "./module/event/editor/component/EventHeader/index";
import {HI_EVENT_LANDING_EDITOR_V1} from "./module/event-landing/v1/index";

export const HI_EDITOR_MODULE: HIModule = {
    components: [
        EventHeader,
    ],
    routes: [
        EventEditorRoute,
        EnvelopeEditorRoute,
        InviteCardEditorRoute,
        LandingEditorRoute,
        MailingOptionsEditorRoute,
    ]
};

export const HI_EDITOR_MODULES = [
    HI_EVENT_LANDING_EDITOR_V1,
];