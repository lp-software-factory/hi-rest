import {appEventLanding} from "../module"

import {ComponentDefinitions, ComponentDefinition} from "../../../angularized/decorators/Component";

export function EventLandingComponent(definition: ComponentDefinition) {
    return function(target: Function) {
        ComponentDefinitions.$instance.register(appEventLanding, target, definition);
    };
}
