import {appEventLanding} from "../module"

import {FactoryDefinition, FactoryDefinitions, FactoryInterface} from "../../../angularized/decorators/Factory";

export function EventLandingFactory(definition: FactoryDefinition) {
    return function(target: Function) {
        FactoryDefinitions.$instance.register(appEventLanding, <any>target, definition);
    };
}
