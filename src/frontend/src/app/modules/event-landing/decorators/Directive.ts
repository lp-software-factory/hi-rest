import {appEventLanding} from "../module"

import {DirectiveDefinition, DirectiveDefinitions} from "../../../angularized/decorators/Directive";

export function EventLandingDirective(definition: DirectiveDefinition) {
    return function(target: Function) {
        DirectiveDefinitions.$instance.register(appEventLanding, <any>target, definition);
    };
}
