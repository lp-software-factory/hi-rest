import {appEventLanding} from "../module"

import {ServiceDefinition, ServiceDefinitions} from "../../../angularized/decorators/Service";

export function EventLandingService(definition: ServiceDefinition = {}) {
    return function(target: Function) {
        ServiceDefinitions.$instance.register(appEventLanding, target, definition);
    };
}
