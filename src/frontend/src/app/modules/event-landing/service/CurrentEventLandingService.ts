import {EventLandingService} from "../decorators/Service";

import {ReplaySubject, Observable} from "rxjs";
import {HIEvent} from "../../../../../definitions/src/definitions/events/event/entity/Event";
import {LoadingManager} from "../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplate, EventLandingTemplateBlock} from "../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {EventRecipientsContact, EventRecipients} from "../../../../../definitions/src/definitions/events/event-recipients/entity/EventRecipients";

export const CODE_ERROR_NO_EVENT_LANDING_AVAILABLE = -1;
export const CODE_ERROR_RESOURCES_FAIL = -2;

export interface EventLandingNext
{
    event: HIEvent,
    landing: EventLandingTemplate,
    recipient: EventRecipientsContact|undefined,
    recipients: EventRecipients,
}

export interface CurrentEventLandingServiceSetupQuery
{
    event: HIEvent;
    recipient: EventRecipientsContact|undefined,
    recipients: EventRecipients,
}

@EventLandingService({
    injects: [
        '$timeout',
    ]
})
export class CurrentEventLandingService
{
    public current: EventLandingNext;

    public isReady: boolean = false;
    public ready: ReplaySubject<EventLandingNext> = new ReplaySubject<EventLandingNext>();
    public error: number|undefined;

    public status: LoadingManager = new LoadingManager();

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {}

    setup(query: CurrentEventLandingServiceSetupQuery): Observable<EventLandingNext> {
        let loading = this.status.addLoading();

        let event = query.event;

        this.isReady = false;
        this.error = undefined;

        return Observable.create(observer => {
            this.loadResources().subscribe(
                next => {
                    this.$timeout(() => {
                        observer.next(next);
                    });
                },
                error => {
                    this.$timeout(() => {
                        observer.error({
                            code: CODE_ERROR_RESOURCES_FAIL,
                            error: error,
                        });

                        // we can skip loading resources error
                    });
                },
                () => {
                    this.$timeout(() => {
                        loading.is = false;

                        let landing = event.entity.solution.current.entity.landing_templates[0];

                        if(event.entity.event_features.landing === false || landing.entity.solution.is_user_specified === false) {
                            this.error = CODE_ERROR_NO_EVENT_LANDING_AVAILABLE;

                            observer.error({
                                code: CODE_ERROR_NO_EVENT_LANDING_AVAILABLE,
                                error: 'This event has no landing at all!'
                            });

                            observer.complete();
                        }else{
                            this.current = {
                                event: event,
                                landing: landing,
                                recipient: query.recipient,
                                recipients: query.recipients,
                            };

                            this.isReady = true;
                            this.ready.next(this.current);

                            observer.next(this.current);
                            observer.complete();
                        }
                    });
                }
            )
        }).share();
    }

    loadResources(): Observable<any> {
        return Observable.create(observer => {
            let loading = this.status.addLoading();

            // do something about loading resources, then:
            this.$timeout(() => {
                loading.is = false;

                observer.complete();
            });
        }).share();
    }

    isPhotoGalleryAvailable(): boolean {
        if(this.current) {
            return this.current.landing.entity.json.blocks.enabled[EventLandingTemplateBlock.PhotoGallery];
        }else{
            return false;
        }
    }

    isAnswerAvailable(): boolean {
        return this.isCurrentRecipientAvailable();
    }

    isInviteCardAvailable(): boolean {
        return this.isCurrentRecipientAvailable();
    }

    isCurrentRecipientAvailable(): boolean {
        if(this.current) {
            return this.current.recipient !== undefined;
        }else{
            return false;
        }
    }

    isEventRecipientsListAvailable(): boolean {
        if(this.current) {
            return this.current.recipients.entity.contacts.length > 0;
        }else{
            return false;
        }
    }
}