import {EventLandingService} from "../decorators/Service";
import {CurrentEventLandingService} from "./CurrentEventLandingService";
import {IEventLandingTemplateBlock, EventLandingTemplate, EventLandingTemplateBlock, EventLandingTemplateBlockPhotoGallery, EventLandingTemplateBlockWishes} from "../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EventLandingService({
    injects: [
        CurrentEventLandingService,
    ]
})
export class IsEventLandingBlockAvailableService
{
    private last: EventLandingTemplate;

    constructor(
        private current: CurrentEventLandingService,
    ) {
        this.current.ready.subscribe(next => {
            this.last = next.landing;
        });
    }

    isPhotoBlockAvailable(): boolean {
        if(! this.last) {
            return false;
        }

        if(this.last.entity.json.blocks.enabled[EventLandingTemplateBlock.PhotoGallery]) {
            let photoBlocks = this.last.entity.json.blocks.sources.filter(b => b.type === EventLandingTemplateBlock.PhotoGallery);

            if(photoBlocks.length) {
                for(let photoBlock of photoBlocks) {
                    if(this.isBlockEnabled(photoBlock)) {
                        return true;
                    }

                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    isWishListBlockAvailable(): boolean {
        if(! this.last) {
            return false;
        }

        if(this.last.entity.json.blocks.enabled[EventLandingTemplateBlock.Wishes]) {
            let wishListBlocks = this.last.entity.json.blocks.sources.filter(b => b.type === EventLandingTemplateBlock.Wishes);

            if(wishListBlocks.length > 0) {
                for(let block of wishListBlocks) {
                    if(this.isBlockEnabled(block)) {
                        return true;
                    }
                }

                return false;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    isBlockEnabled(block: IEventLandingTemplateBlock): boolean {
        if(! this.last) {
            return false;
        }

        switch(block.type) {
            default:
                return true;

            case EventLandingTemplateBlock.PhotoGallery:
                let typescriptPlease = <EventLandingTemplateBlockPhotoGallery>block;

                return typescriptPlease.photos.length > 0;

            case EventLandingTemplateBlock.Wishes:
                let typescriptPlease2 = <EventLandingTemplateBlockWishes>block;

                return Array.isArray(typescriptPlease2.wishList) && typescriptPlease2.wishList.length > 0;
        }
    }
}