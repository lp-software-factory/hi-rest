import {RouteConfig} from "../../angularized/decorators/Component";
import {EventLandingEventRoute} from "./routes/EventLandingRootRoute/routes/EventLandingEventRoute/index";
import {EventLandingAnswerRoute} from "./routes/EventLandingRootRoute/routes/EventLandingAnswerRoute/index";
import {EventLandingInviteRoute} from "./routes/EventLandingRootRoute/routes/EventLandingInviteRoute/index";
import {EventLandingNotAvailableRoute} from "./routes/EventLandingRootRoute/routes/EventLandingNotAvailableRoute/index";
import {EventLandingPhotoGalleryRoute} from "./routes/EventLandingRootRoute/routes/EventLandingPhotoGalleryRoute/index";

export const APP_EVENT_LANDING_ROUTES: RouteConfig[] = [
    {
        path: '/event',
        name: 'HIEventLandingEventRoute',
        component: EventLandingEventRoute,
        useAsDefault: true,
    },
    {
        path: '/answer',
        name: 'HIEventLandingAnswerRoute',
        component: EventLandingAnswerRoute,
    },
    {
        path: '/invite',
        name: 'HIEventLandingInviteRoute',
        component: EventLandingInviteRoute,
    },
    {
        path: '/not-available',
        name: 'HIEventLandingNotAvailableRoute',
        component: EventLandingNotAvailableRoute,
    },
    {
        path: '/photo-gallery',
        name: 'HIEventLandingPhotoGalleryRoute',
        component: EventLandingPhotoGalleryRoute,
    },
];