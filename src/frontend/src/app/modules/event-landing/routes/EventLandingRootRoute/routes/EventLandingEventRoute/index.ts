import {EventLandingComponent} from "../../../../decorators/Component";

import {Subscription} from "rxjs";
import {CurrentEventLandingService, CODE_ERROR_NO_EVENT_LANDING_AVAILABLE} from "../../../../service/CurrentEventLandingService";
import {IComponentRouter} from "../../../../../../../../typings-custom/angular-component-router";
import {EventLandingTemplate} from "../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {HIEvent} from "../../../../../../../../definitions/src/definitions/events/event/entity/Event";

@EventLandingComponent({
    selector: 'hi-event-landing-event-route',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$rootRouter',
        '$timeout',
        CurrentEventLandingService,
    ]
})
export class EventLandingEventRoute implements ng.IComponentController
{
    private subscription: Subscription;

    private _event: HIEvent;
    private _landing: EventLandingTemplate;
    
    constructor(
        private $router: IComponentRouter,
        private $timeout: ng.ITimeoutService,
        private current: CurrentEventLandingService,
    ) {}

    get landing(): EventLandingTemplate {
        if(this._landing === undefined) {
            throw new Error('Landing is not ready');
        }

        return this._landing;
    }

    set landing(value: EventLandingTemplate) {
        this._landing = value;
    }
    get event(): HIEvent {
        if(this._event === undefined) {
            throw new Error('Event is not ready');
        }

        return this._event;
    }

    set event(value: HIEvent) {
        this._event = value;
    }

    $onInit(): void {
        this.subscription = this.current.ready.subscribe(
            next => {
                this.$timeout(() => {
                    this.event = next.event;
                    this.landing = next.landing;

                    // do initial
                });
            },
            error => {
                if(error.code && error.code === CODE_ERROR_NO_EVENT_LANDING_AVAILABLE) {
                    this.$router.navigate(['/HIEventLandingNotAvailableRoute']);
                }
            }
        );
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    isReady(): boolean {
        return this.current.isReady
            && this._event !== undefined
            && this._landing !== undefined;
    }
}