import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {EventLandingTemplateBlockProgram} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {stripTags} from "../../../../../../../../../main/functions/stripTags";
import * as moment from 'moment'

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-program',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        index: '<',
        block: '<',
        total: '<',
    },
    injects: [
        '$timeout',
        '$sce'
    ],
})
export class EventLandingV1BlockProgram implements ng.IComponentController
{
    public index: number = 0;
    public total: number = 0;
    public block: EventLandingTemplateBlockProgram;
    public status: LoadingManager = new LoadingManager();

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService
    ) {}

    $onChanges(): void {
        let loading = this.status.addLoading();

        this.$timeout(() => {
            loading.is = false;
        });
    }

    has(input: string): boolean {
        return stripTags(input).length > 0;
    }

    isTitleEnabled(): boolean {
        return this.index === 0;
    }
    
    getTrustedHTML(htmlString): any {
        return this.$sce.trustAsHtml(htmlString);
    }

    hasPhoto(): boolean {
        if(this.block.photo !== undefined) {
            return this.block.photo.url !== undefined;
        }
    }

    isSeparatorAvailable(): boolean {
        return (this.index + 1 !== this.total) && ! this.hasMap();
    }

    getPhotoCSS(): any {
        if(this.hasPhoto()) {
            return {
                'background-image': `url(${encodeURI(this.block.photo.url)})`,
            };
        }else{
            return {};
        }
    }

    getStartTime(): string {
        if(this.block.time.start) {
            return moment(this.block.time.start).format('HH:mm');
        }else{
            return '';
        }
    }

    getEndTime(): string {
        if(this.block.time.end) {
            return moment(this.block.time.end).format('HH:mm');
        }else{
            return '';
        }
    }

    hasMap(): boolean {
        return Array.isArray(this.block.markers) && this.block.markers.length > 0;
    }
}