import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplateBlockDressCode} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {stripTags} from "../../../../../../../../../main/functions/stripTags";
import {DressCodeService} from "../../../../../../../../../main/module/dress-code/service/DressCodeService";
import {LocaleService} from "../../../../../../../../../main/module/locale/service/LocaleService";

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-dress-code',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
        '$sce',
        DressCodeService,
        LocaleService,
    ],
})
export class EventLandingV1BlockDressCode implements ng.IComponentController
{
    public block: EventLandingTemplateBlockDressCode;
    public status: LoadingManager = new LoadingManager();

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService,
        private dressCodes: DressCodeService,
        private locale: LocaleService,
    ) {}

    $onChanges(): void {
        let loading = this.status.addLoading();

        this.$timeout(() => {
            loading.is = false;
        });
    }

    getTrustedHTML(htmlString): any {
        return this.$sce.trustAsHtml(htmlString);
    }

    has(input: string): boolean {
        return stripTags(input).length > 0;
    }

    getPhotoCSS(): any {
        let url: string;

        if(this.block.custom && this.block.custom.image && this.block.custom.image.url) {
            url = this.block.custom.image.url;
        }else{
            url = this.dressCodes.getById(this.block.useId).entity.image.link.url;
        }

        return {
            'background-image': `url(${encodeURI(url)})`,
        };
    }

    getMenDescription(): any {
        let result: string;

        if(this.block.custom && this.block.custom.men) {
            return this.block.custom.men;
        }else{
            result = this.locale.localize(this.dressCodes.getById(this.block.useId).entity.description_men);
        }

        return this.getTrustedHTML(result);
    }

    getWomenDescription(): any {
        let result: string;

        if(this.block.custom && this.block.custom.women) {
            return this.block.custom.women;
        }else{
            result = this.locale.localize(this.dressCodes.getById(this.block.useId).entity.description_women);
        }

        return this.getTrustedHTML(result);
    }
}