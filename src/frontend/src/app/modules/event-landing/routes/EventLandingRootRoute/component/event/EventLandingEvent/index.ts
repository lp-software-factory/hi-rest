import {EventLandingComponent} from "../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate, IEventLandingTemplateBlock, EventLandingTemplateBlock, EventLandingTemplateBlockHeader, EventLandingTemplateBlockProgram} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {EventLandingTemplateConfigsService} from "../../../../../../main/module/event/editor/service/EventLandingTemplateConfigsService";
import {LoadingManager} from "../../../../../../main/module/common/helpers/LoadingManager";
import {IsEventLandingBlockAvailableService} from "../../../../../service/IsEventLandingBlockAvailableService";

@EventLandingComponent({
    selector: 'hi-event-landing-event',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        event: '<',
        landing: '<',
    },
    injects: [
        '$timeout',
        EventLandingTemplateConfigsService,
        IsEventLandingBlockAvailableService,
    ]
})
export class EventLandingEvent implements ng.IComponentController
{
    public status: LoadingManager = new LoadingManager();

    public event: HIEvent;
    public landing: EventLandingTemplate;

    constructor(
        private $timeout: ng.ITimeoutService,
        private configs: EventLandingTemplateConfigsService,
        private isEventLandingBlockAvailableService: IsEventLandingBlockAvailableService,
    ) {}

    $onChanges(): void {
        let loading = this.status.addLoading();
        
        this.$timeout(() => {
            loading.is = false;
        });
    }

    getBlocks(): IEventLandingTemplateBlock[] {
        let enabled = this.landing.entity.json.blocks.enabled;

        return this.landing.entity.json.blocks.sources
            .filter(item => {
                return !~[
                    EventLandingTemplateBlock.Header,
                    EventLandingTemplateBlock.Program,
                ].indexOf(item.type);
            })
            .filter(item => {
                return enabled[item.type];
            })
            .sort((a, b) => {
                return this.configs.getOrder(a.type) - this.configs.getOrder(b.type);
            });
    }

    getHeaderBlock(): EventLandingTemplateBlockHeader {
        return <EventLandingTemplateBlockHeader> this.landing.entity.json.blocks.sources.filter(b => {
            return b.type === EventLandingTemplateBlock.Header;
        })[0];
    }

    hasProgramBlocks(): boolean {
        return this.getProgramBlocks().length > 0;
    }

    getProgramBlocks(): EventLandingTemplateBlockProgram[] {
        return <EventLandingTemplateBlockProgram[]> this.landing.entity.json.blocks.sources
            .filter(b => {
                return b.type === EventLandingTemplateBlock.Program;
            })
            .sort((a, b) => {
                return a.zIndex - b.zIndex;
            });
    }

    getTotalProgramBlocks(): number {
        return this.getProgramBlocks().length;
    }

    isBlockAvailable(block: IEventLandingTemplateBlock): boolean {
        return this.isEventLandingBlockAvailableService.isBlockEnabled(block);
    }
}