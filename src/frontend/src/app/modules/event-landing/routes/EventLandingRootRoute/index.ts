import {EventLandingComponent} from "../../decorators/Component";

import {Subscription} from "rxjs";
import {FrontendService} from "../../../main/service/FrontendService";
import {FontService} from "../../../main/module/font/service/FontService";
import {CurrentEventLandingService} from "../../service/CurrentEventLandingService";
import {APP_EVENT_LANDING_ROUTES} from "../../routes";
import {HIEvent} from "../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate} from "../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EventLandingComponent({
    selector: 'hi-event-landing-root-route',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
        CurrentEventLandingService,
        FrontendService,
        FontService,
        CurrentEventLandingService,
    ],
    routes: APP_EVENT_LANDING_ROUTES,
})
export class EventLandingRootRoute implements ng.IComponentController
{
    private subscription: Subscription;

    public event: HIEvent;
    public landing: EventLandingTemplate;

    constructor(
        private $timeout: ng.ITimeoutService,
        private current: CurrentEventLandingService,
        private frontend: FrontendService,
        private fontService: FontService,
    ) {}

    $onInit(): void {
        this.subscription = this.frontend.replay.subscribe(next => {
            if(next.exports['fonts']) {
                this.fontService.available = next['fonts'];
            }

            if(next.exports['event']) {
                this.current.setup({
                    event: next.exports['event'],
                    recipient: next.exports['recipient'],
                    recipients: next.exports['event_recipients'],
                }).subscribe(
                    next => {
                        this.event = next.event;
                        this.landing = next.landing;
                    },
                    error => {
                        console.log(error);
                    },
                    () => {
                        console.log('Event landing ready');
                    }
                );
            }
        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    isReady(): boolean {
        return !this.current.status.isLoading() && this.current.isReady && this.event !== undefined;
    }

    isLoading(): boolean {
        return this.current.status.isLoading();
    }
}