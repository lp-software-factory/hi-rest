import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {EventLandingTemplateBlockWishes} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {stripTags} from "../../../../../../../../../main/functions/stripTags";

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-wishes',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
        '$sce',
    ],
})
export class EventLandingV1BlockWishes implements ng.IComponentController
{
    public block: EventLandingTemplateBlockWishes;
    public status: LoadingManager = new LoadingManager();

    public visible: boolean = false;
    public listLeft: string[];
    public listRight: string[];

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService,
    ) {}

    $onChanges(): void {
        if(Array.isArray(this.block.wishList)) {
            this.visible = true;

            this.listLeft = this.block.wishList.filter((w, index) => {
                return ! (index % 2 === 0);
            });

            this.listRight = this.block.wishList.filter((w, index) => {
                return index % 2 === 0;
            })
        }else{
            this.visible = false;

            this.listLeft = [];
            this.listRight = [];
        }
    }

    getTrustedHTML(htmlString): any {
        return this.$sce.trustAsHtml(htmlString);
    }

    has(input: string): boolean {
        return stripTags(input).length > 0;
    }
}