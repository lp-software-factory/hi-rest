import {EventLandingComponent} from "../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../main/module/common/helpers/LoadingManager";

@EventLandingComponent({
    selector: 'hi-event-landing-answer',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        event: '<',
        landing: '<',
    },
    injects: [
        '$timeout',
    ]
})
export class EventLandingAnswer implements ng.IComponentController
{
    public status: LoadingManager = new LoadingManager();

    public event: HIEvent;
    public landing: EventLandingTemplate;

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {}

    $onChanges(): void {
        let loading = this.status.addLoading();

        // load some resources, then:
        this.$timeout(() => {
            loading.is = false;
        });
    }
}