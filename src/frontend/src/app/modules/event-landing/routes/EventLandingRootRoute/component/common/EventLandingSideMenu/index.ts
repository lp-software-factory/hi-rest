import {EventLandingComponent} from "../../../../../decorators/Component";

import {IsEventLandingBlockAvailableService} from "../../../../../service/IsEventLandingBlockAvailableService";
import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate, EventLandingTemplateBlock, EventLandingTemplateBlockCustom} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {Subscription} from "rxjs";
import {CurrentEventLandingService} from "../../../../../service/CurrentEventLandingService";
import {stripTags} from "../../../../../../main/functions/stripTags";

@EventLandingComponent({
    selector: 'hi-event-landing-side-menu',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        event: '<',
        landing: '<',
    },
    injects: [
        '$timeout',
        IsEventLandingBlockAvailableService,
        CurrentEventLandingService,
    ]
})
export class EventLandingSideMenu implements ng.IComponentController
{
    public event: HIEvent;
    public landing: EventLandingTemplate;

    public customBlockTitle: string = '';
    public hasCustomBlockTitle: boolean = false;

    private subscription: Subscription;

    constructor(
        private $timeout: ng.ITimeoutService,
        private enabled: IsEventLandingBlockAvailableService,
        private current: CurrentEventLandingService,
    ) {}

    $onInit(): void {
        this.subscription = this.current.ready.subscribe(next => {
            this.$timeout(() => {
                this.customBlockTitle = '';
                this.hasCustomBlockTitle = false;

                let customBlocks: EventLandingTemplateBlockCustom[] = <any>next.landing.entity.json.blocks.sources.filter(b => b.type === EventLandingTemplateBlock.Custom);

                if(customBlocks.length > 0) {
                    for(let block of customBlocks) {
                        if(stripTags(block.customTitle).length > 0) {
                            this.customBlockTitle = stripTags(block.customTitle);
                            this.hasCustomBlockTitle = true;

                            break;
                        }
                    }
                }
            });
        });
    }

    $onDestroy(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    scrollTo(elementClass: string) {
        document.getElementsByClassName(elementClass)[0].scrollIntoView();
    }
    
    hasElement(elementClass: string) {
        return document.getElementsByClassName(elementClass)[0] !== undefined;
    }
}