import {EventLandingComponent} from "../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate, IEventLandingTemplateBlock} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplateConfigsService} from "../../../../../../main/module/event/editor/service/EventLandingTemplateConfigsService";
import {IsEventLandingBlockAvailableService} from "../../../../../service/IsEventLandingBlockAvailableService";

@EventLandingComponent({
    selector: 'hi-event-landing-photo-gallery',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        event: '<',
        landing: '<',
    },
    injects: [
        '$timeout',
        EventLandingTemplateConfigsService,
        IsEventLandingBlockAvailableService
    ]
})
export class EventLandingPhotoGallery implements ng.IComponentController
{
    public status: LoadingManager = new LoadingManager();

    public event: HIEvent;
    public landing: EventLandingTemplate;

    constructor(
        private $timeout: ng.ITimeoutService,
        private configs: EventLandingTemplateConfigsService,
        private isEventLandingBlockAvailableService: IsEventLandingBlockAvailableService,
    ) {}
    
    getBlocks(): IEventLandingTemplateBlock[] {
        let enabled = this.landing.entity.json.blocks.enabled;
        
        return this.landing.entity.json.blocks.sources
            .filter(item => {
                return enabled[item.type];
            })
            .sort((a, b) => {
                return this.configs.getOrder(a.type) - this.configs.getOrder(b.type);
            });
    }

    isBlockAvailable(block: IEventLandingTemplateBlock): boolean {
        return this.isEventLandingBlockAvailableService.isBlockEnabled(block);
    }
    
    $onChanges(): void {
        let loading = this.status.addLoading();

        // load some resources, then:
        this.$timeout(() => {
            loading.is = false;
        });
    }
}