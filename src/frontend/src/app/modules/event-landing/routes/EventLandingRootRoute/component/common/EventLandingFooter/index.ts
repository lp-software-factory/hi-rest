import {EventLandingComponent} from "../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EventLandingComponent({
    selector: 'hi-event-landing-footer',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        event: '<',
        landing: '<',
    }
})
export class EventLandingFooter implements ng.IComponentController
{
    public event: HIEvent;
    public landing: EventLandingTemplate;

    getBgCSS(): any {
        return {
            'background-image': `url(${encodeURI(this.event.entity.solution.current.entity.envelope_templates[0].entity.envelope_backdrop_definition.entity.resource.link.url)})`,
        };
    }
}