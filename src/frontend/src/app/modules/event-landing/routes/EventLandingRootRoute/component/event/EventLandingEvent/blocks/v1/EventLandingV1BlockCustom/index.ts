import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {EventLandingTemplateBlockCustom} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {stripTags} from "../../../../../../../../../main/functions/stripTags";

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-custom',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
        '$sce',
    ],
})
export class EventLandingV1BlockCustom implements ng.IComponentController
{
    public block: EventLandingTemplateBlockCustom;
    public status: LoadingManager = new LoadingManager();

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService,
    ) {}

    $onChanges(): void {
        let loading = this.status.addLoading();

        // do some initial stuff, than:
        this.$timeout(() => {
            loading.is = false;
        });
    }

    hasTitle(): boolean {
        return stripTags(this.block.customTitle).length > 0;
    }

    getTitle(): any {
        return this.$sce.trustAsHtml(this.block.customTitle);
    }

    hasDescription(): boolean {
        return stripTags(this.block.customDescription).length > 0;
    }

    getDescription(): any {
        return this.$sce.trustAsHtml(this.block.customDescription);
    }

    hasPhoto(): boolean {
        return !!this.block.photo && !!this.block.photo.url;
    }

    getPhotoCSS(): any {
        if(this.hasPhoto()) {
            return {
                'background-image': `url(${encodeURI(this.block.photo.url)})`,
            };
        }else{
            return {};
        }
    }
}