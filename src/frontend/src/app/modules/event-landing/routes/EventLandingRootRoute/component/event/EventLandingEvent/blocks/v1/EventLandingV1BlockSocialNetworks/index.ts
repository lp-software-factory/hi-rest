import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplateBlockSocialNetworks, EventLandingTemplateSocialNetworks} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-social-networks',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
    ],
})
export class EventLandingV1BlockSocialNetworks implements ng.IComponentController
{
    public block: EventLandingTemplateBlockSocialNetworks;
    public status: LoadingManager = new LoadingManager();

    public visible: boolean = false;

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {}

    $onChanges(): void {
        this.visible = this.hasAnyEnabledSocialNetworks();
    }

    hasAnyEnabledSocialNetworks(): boolean {
        for(let sn in this.block.enabled) {
            if(this.block.enabled.hasOwnProperty(sn) && this.block.enabled[sn] === true) {
                return true;
            }
        }

        return false;
    }

    isEnabled(network: EventLandingTemplateSocialNetworks): boolean {
        return !! this.block.enabled[network];
    }

    click(network: EventLandingTemplateSocialNetworks): void {}
}