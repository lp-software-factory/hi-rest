import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {EventLandingTemplateBlockHeader} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {ITranslateServiceInterface} from "../../../../../../../../../main/module/i18n/service/TranslationService";
import {stripTags} from "../../../../../../../../../main/functions/stripTags";

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-header',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
        '$sce',
        '$translate'
    ],
})
export class EventLandingV1BlockHeader implements ng.IComponentController
{
    public block: EventLandingTemplateBlockHeader;
    public status: LoadingManager = new LoadingManager();

    private monthNames = [
        'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
    ];

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService,
        private $translate: ITranslateServiceInterface,
    ) {
        let translation = this.monthNames.map(m => 'hi.common.months-short.' + m);

        this.$translate(translation).then(data => {
            this.$timeout(() => {
                this.monthNames = [];

                translation.forEach(m => {
                    this.monthNames.push(data[m]);
                })
            });
        });
    }

    $onChanges(): void {
        let loading = this.status.addLoading();

        this.$timeout(() => {
            loading.is = false;
        });
    }

    getCurrentBGColor(): string {
        return (
               this.block.color !== undefined
            && this.block.color !== null
            && typeof this.block.color === "string"
            && this.block.color.length >= 6
        ) ? this.block.color : '#000000';
    }

    getColorCSS(): any {
        return {
            'color': '#ffffff',
            'background': `linear-gradient(transparent, ${this.getCurrentBGColor()})`,
        }
    }

    hasHeader(): boolean {
        return this.block.isDateEnabled || this.block.isOrganizerEnabled;
    }

    hasSubTitle(): boolean {
        return stripTags(this.block.description).length > 0;
    }

    hasDate(): boolean {
        return this.block.date.length > 0;
    }

    getMonth() {
        let date = new Date(this.block.date);
        return this.monthNames[date.getMonth()];
    }

    getDate() {
        let date = new Date(this.block.date);
        return date.getDate();
    }

    getYear() {
        let date = new Date(this.block.date);
        return date.getFullYear();
    }

    getCurrentImage(): string {
        if(this.block.chosenImage.url !== undefined) {
            return this.block.chosenImage.url;
        } else return '/dist/img/event-img.jpg';
    }
    
    getTrustedHTML(htmlString): any {
        return this.$sce.trustAsHtml(htmlString);
    }

    hasBg(): boolean {
        return this.hasHeader();
    }
}