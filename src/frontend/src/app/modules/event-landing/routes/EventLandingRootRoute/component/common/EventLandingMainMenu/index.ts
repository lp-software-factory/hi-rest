import {EventLandingComponent} from "../../../../../decorators/Component";

import {HIEvent} from "../../../../../../../../../definitions/src/definitions/events/event/entity/Event";
import {EventLandingTemplate} from "../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {IsEventLandingBlockAvailableService} from "../../../../../service/IsEventLandingBlockAvailableService";
import {CurrentEventLandingService} from "../../../../../service/CurrentEventLandingService";

@EventLandingComponent({
    selector: 'hi-event-landing-main-menu',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        event: '<',
        landing: '<',
    },
    injects: [
        CurrentEventLandingService,
        IsEventLandingBlockAvailableService,
    ]
})
export class EventLandingMainMenu implements ng.IComponentController
{
    public event: HIEvent;
    public landing: EventLandingTemplate;

    constructor(
        private current: CurrentEventLandingService,
        private isEventLandingBlockAvailableService: IsEventLandingBlockAvailableService,
    ) {}

    isPhotoBlockAvailable(): boolean {
        return this.isEventLandingBlockAvailableService.isPhotoBlockAvailable();
    }

    isInviteMenuItemAvailable(): boolean {
        return this.current.isInviteCardAvailable();
    }

    ifPhotoGalleryMenuItemAvailable(): boolean {
        return this.current.isPhotoGalleryAvailable();
    }

    isAnswerMenuItemAvailable(): boolean {
        return this.current.isAnswerAvailable();
    }
}