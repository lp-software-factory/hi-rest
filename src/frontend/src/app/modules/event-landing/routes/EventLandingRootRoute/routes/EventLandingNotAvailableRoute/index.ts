import {EventLandingComponent} from "../../../../decorators/Component";

import {Subscription} from "rxjs";

@EventLandingComponent({
    selector: 'hi-event-landing-not-available-route',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    injects: [
        '$timeout',
    ]
})
export class EventLandingNotAvailableRoute implements ng.IComponentController
{
    private subscription: Subscription;

    constructor(
        private $timeout: ng.ITimeoutService,
    ) {}
}