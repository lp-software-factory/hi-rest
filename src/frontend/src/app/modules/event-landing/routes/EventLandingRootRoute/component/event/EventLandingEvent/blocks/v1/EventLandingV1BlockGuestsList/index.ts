import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplateBlockGuestsList} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";
import {EventRecipientsContact, EventRecipients} from "../../../../../../../../../../../../definitions/src/definitions/events/event-recipients/entity/EventRecipients";
import {CurrentEventLandingService} from "../../../../../../../../service/CurrentEventLandingService";
import {stripTags} from "../../../../../../../../../main/functions/stripTags";
import {ITranslateServiceInterface} from "../../../../../../../../../main/module/i18n/service/TranslationService";

enum GuestsListEntryStatus
{
    Accepted = <any>"accepted",
    Waiting = <any>"waiting",
    Rejected = <any>"rejected",
}

interface GuestsListEntry
{
    name: string,
    status: GuestsListEntryStatus,
}

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-guests-list',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<',
        eventId: '<'
    },
    injects: [
        '$timeout',
        '$sce',
        '$translate',
        CurrentEventLandingService,
    ],
})
export class EventLandingV1BlockGuestsList implements ng.IComponentController
{
    public block: EventLandingTemplateBlockGuestsList;
    public event: any;
    public status: LoadingManager = new LoadingManager();

    private recipients: EventRecipients;
    private contacts: GuestsListEntry[] = [];

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService,
        private $translate: ITranslateServiceInterface,
        private currentEventLandingService: CurrentEventLandingService,
    ) {
        this.currentEventLandingService.ready.subscribe(next => {
            this.recipients = next.recipients;

            if(this.currentEventLandingService.isCurrentRecipientAvailable() && this.currentEventLandingService.isEventRecipientsListAvailable()) {
                this.buildContactsFromRecipients();
            }
        })
    }
    
    $onChanges(): void {
        let loading = this.status.addLoading();

        if(this.currentEventLandingService.isCurrentRecipientAvailable() && this.currentEventLandingService.isEventRecipientsListAvailable()) {
            this.$timeout(() => {
                loading.is = false;

                this.buildContactsFromRecipients();
            });
        }else{
            this.$translate(['hi.main.event.landing.blocks.resources.guests-list.guest']).then(t => {
                this.$timeout(() => {
                    loading.is = false;

                    this.buildSampleContacts();
                });
            });
        }
    }

    buildContactsFromRecipients(): void
    {
        this.contacts = this.recipients.entity.contacts.map(contact => {
            return {
                name: contact.first_name + ' ' + contact.last_name,
                status: GuestsListEntryStatus.Accepted,
            };
        });
    }

    buildSampleContacts(): void {
        this.$translate(['hi.main.event.landing.blocks.resources.guests-list.guest']).then(t => {
            this.$timeout(() => {
                this.contacts = [
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 1', status: GuestsListEntryStatus.Accepted },
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 2', status: GuestsListEntryStatus.Waiting },
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 3', status: GuestsListEntryStatus.Accepted },
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 4', status: GuestsListEntryStatus.Rejected },
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 5', status: GuestsListEntryStatus.Accepted },
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 6', status: GuestsListEntryStatus.Waiting },
                    { name: t['hi.main.event.landing.blocks.resources.guests-list.guest'] + ' 7', status: GuestsListEntryStatus.Rejected },
                ];
            });
        });
    }

    getContacts(): EventRecipientsContact[] {
        return this.currentEventLandingService.current.event.entity.event_recipients.entity.contacts;
    }

    has(input: string): boolean {
        return stripTags(input).length > 0;
    }

    getTrustedHTML(htmlString) {
        return this.$sce.trustAsHtml(htmlString);
    }
}