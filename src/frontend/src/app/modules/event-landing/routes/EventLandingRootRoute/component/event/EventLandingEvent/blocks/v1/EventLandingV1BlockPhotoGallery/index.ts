import {EventLandingComponent} from "../../../../../../../../decorators/Component";

import {LoadingManager} from "../../../../../../../../../main/module/common/helpers/LoadingManager";
import {EventLandingTemplateBlockPhotoGallery, EventLandingTemplateAttachment} from "../../../../../../../../../../../../definitions/src/definitions/events/event-landing/entity/EventLandingTemplate";

@EventLandingComponent({
    selector: 'hi-event-landing-v1-block-photo-gallery',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
    bindings: {
        block: '<'
    },
    injects: [
        '$timeout',
        '$sce'
    ],
})
export class EventLandingV1BlockPhotoGallery implements ng.IComponentController
{
    public block: EventLandingTemplateBlockPhotoGallery;
    public status: LoadingManager = new LoadingManager();

    constructor(
        private $timeout: ng.ITimeoutService,
        private $sce: ng.ISCEService
    ) {}

    openFullSize(photo: EventLandingTemplateAttachment){
        console.log(photo.url);
    }

    getTrustedHTML(htmlString) {
        return this.$sce.trustAsHtml(htmlString);
    }
    
    $onChanges(): void {
        let loading = this.status.addLoading();
        // do some initial stuff, than:
        this.$timeout(() => {
            loading.is = false;
        });
    }
}