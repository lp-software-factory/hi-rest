import {HIModule} from "../../modules";

import {EventLandingRootRoute} from "./routes/EventLandingRootRoute/index";
import {EventLandingEventRoute} from "./routes/EventLandingRootRoute/routes/EventLandingEventRoute/index";
import {EventLandingAnswerRoute} from "./routes/EventLandingRootRoute/routes/EventLandingAnswerRoute/index";
import {EventLandingInviteRoute} from "./routes/EventLandingRootRoute/routes/EventLandingInviteRoute/index";
import {EventLandingNotAvailableRoute} from "./routes/EventLandingRootRoute/routes/EventLandingNotAvailableRoute/index";
import {EventLandingPhotoGalleryRoute} from "./routes/EventLandingRootRoute/routes/EventLandingPhotoGalleryRoute/index";
import {CurrentEventLandingService} from "./service/CurrentEventLandingService";
import {EventLandingAnswer} from "./routes/EventLandingRootRoute/component/answer/EventLandingAnswer/index";
import {EventLandingFooter} from "./routes/EventLandingRootRoute/component/common/EventLandingFooter/index";
import {EventLandingMainMenu} from "./routes/EventLandingRootRoute/component/common/EventLandingMainMenu/index";
import {EventLandingInvite} from "./routes/EventLandingRootRoute/component/invite/EventLandingInvite/index";
import {EventLandingEvent} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/index";
import {EventLandingPhotoGallery} from "./routes/EventLandingRootRoute/component/photo-gallery/EventLandingPhotoGallery/index";
import {EventLandingV1BlockCustom} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockCustom/index";
import {EventLandingV1BlockDressCode} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockDressCode/index";
import {EventLandingV1BlockGuestsList} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockGuestsList/index";
import {EventLandingV1BlockHeader} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockHeader/index";
import {EventLandingV1BlockPhotoGallery} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockPhotoGallery/index";
import {EventLandingV1BlockProgram} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockProgram/index";
import {EventLandingV1BlockSocialNetworks} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockSocialNetworks/index";
import {EventLandingV1BlockWishes} from "./routes/EventLandingRootRoute/component/event/EventLandingEvent/blocks/v1/EventLandingV1BlockWishes/index";
import {IsEventLandingBlockAvailableService} from "./service/IsEventLandingBlockAvailableService";
import {EventLandingSideMenu} from "./routes/EventLandingRootRoute/component/common/EventLandingSideMenu/index";

export const HI_EVENT_LANDING_MODULE: HIModule = {
    components: [
        EventLandingAnswer,
        EventLandingFooter,
        EventLandingMainMenu,
        EventLandingSideMenu,
        EventLandingInvite,
        EventLandingEvent,
        EventLandingPhotoGallery,
        EventLandingV1BlockCustom,
        EventLandingV1BlockDressCode,
        EventLandingV1BlockGuestsList,
        EventLandingV1BlockHeader,
        EventLandingV1BlockPhotoGallery,
        EventLandingV1BlockProgram,
        EventLandingV1BlockSocialNetworks,
        EventLandingV1BlockWishes,
    ],
    routes: [
        EventLandingRootRoute,
        EventLandingEventRoute,
        EventLandingAnswerRoute,
        EventLandingInviteRoute,
        EventLandingNotAvailableRoute,
        EventLandingPhotoGalleryRoute,
    ],
    services: [
        CurrentEventLandingService,
        IsEventLandingBlockAvailableService,
    ],
};

export const HI_EVENT_LANDING_MODULES = [
];