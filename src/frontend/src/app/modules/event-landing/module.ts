var angular = require('angular');

export const appEventLanding: angular.IModule = angular.module('hi-event-landing');

appEventLanding.config(['$locationProvider', function($locationProvider) {
    $locationProvider
        .html5Mode(false)
        .hashPrefix('!')
    ;
}]);
appEventLanding.value('$routerRootComponent', 'hiEventLandingRootRoute');