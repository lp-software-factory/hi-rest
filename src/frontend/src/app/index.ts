let angular: ng.IAngularStatic = require('angular');

window['moment'] = require('moment');

require('angular-sanitize');
require('angular-translate');
require('angular-messages');
require('reflect-metadata');
require('moment');
require('angular-moment-picker');
require('../../node_modules/angular-moment-picker/dist/angular-moment-picker.css');
require('angular-color-picker');
require('../../node_modules/angular-color-picker/angular-color-picker.css');
require('@angular/router/angular1/angular_1_router.js');
require('../../node_modules/font-awesome/css/font-awesome.min.css');
require('./modules/main/module/common/directives/NgMap/ng-map');
require('./modules/main/styles/moment-js.head.scss');

angular.module('hi-main', [
    'pascalprecht.translate',
    'ngMessages',
    'ngComponentRouter',
    'mp.colorPicker',
    'moment-picker',
    'ngMap',
    'ngSanitize'
]);

angular.module('hi-editor', ['hi-main', 'ngMessages', 'ngComponentRouter']);
angular.module('hi-solution', ['hi-main', 'ngMessages', 'ngComponentRouter']);
angular.module('hi-event-landing', ['hi-main', 'ngMessages', 'ngComponentRouter']);

import {bootstrapRESTServices} from "./modules/main/definition";

bootstrapRESTServices();

import {HI_MODULES} from "./modules";

(() => {
    //noinspection BadExpressionStatementJS
    HI_MODULES;
})(); // Do not tree shaking that