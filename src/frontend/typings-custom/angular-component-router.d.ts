export interface IComponentRouter
{
    navigate(linkParams: Array<any>);
    generate(linkParams: Array<any>);
}

export interface IComponentRouterInstructions
{
    componentType: string;
    params: any;
    reuse: boolean;
}

export interface IComponentQueryParams { [key: string]: string }