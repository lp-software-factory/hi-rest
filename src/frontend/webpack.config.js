var webpack = require("webpack");

function WebpackConfigBuilder() {
  this.publicPath = __dirname + '/../../www/site/dist';
  this.wwwPath = '/dist';
  this.bundlesDir = '/bundles';
}

WebpackConfigBuilder.prototype = {
  build: function() {
    return {
      node: {
        fs: "empty"
      },
      entry: {
        bootstrap: './src/bootstrap/index.ts',
        main: './src/app/index.ts',
      },
      output: {
        filename: '[name].js',
        path: this.publicPath + '/' + this.bundlesDir,
        publicPath: this.wwwPath + '/' + this.bundlesDir + '/'
      },
      resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
      },
      watchOptions: {
        poll: true
      },
      module: {
        loaders: [
          {
            test: /\.jpe?g$|\.gif$|\.png$/i,
            loader: "file"
          },
          {
            test: /\.css$/,
            loader: "style-loader!css-loader"
          },
          {
            test: /\.ts$/,
            loader: 'ts-loader',
            exclude: [
              /\.(spec|e2e)\.ts$/,
              /node_modules\/(?!(ng2-.+))/
            ]
          },
          {
            test: /\.json$/,
            loader: 'json-loader'
          },
          {
            test: /\.html$/,
            loader: 'raw-loader'
          },
          {
            test: /\.help.md$/,
            loader: 'raw-loader'
          },
          {
            test: /\.shadow\.scss$/,
            loaders: ["style", "css", "sass"]
          },
          {
            test: /\.head\.scss$/,
            loaders: ["style", "css", "sass"]
          },
          {
            test: /\.woff(2)?(\?[a-z0-9]+)?$/,
            loader: "url-loader?limit=10000&mimetype=application/font-woff"
          }, {
            test: /\.(ttf|eot|svg)(\?[a-z0-9]+)?$/,
            loader: "file-loader"
          },
          {
            test: /\.jade$/,
            loaders: ['raw-loader', 'jade-html']
          }
        ]
      },
      plugins: [
        new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          Promise: 'bluebird'
        })
      ]
    }
  }
};

module.exports = (new WebpackConfigBuilder()).build();